import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfGeneraterComponent } from './pdf-generater.component';

describe('PdfGeneraterComponent', () => {
  let component: PdfGeneraterComponent;
  let fixture: ComponentFixture<PdfGeneraterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdfGeneraterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfGeneraterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
