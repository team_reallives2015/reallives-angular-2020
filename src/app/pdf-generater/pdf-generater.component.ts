import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import jsPDF from 'jspdf';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';

@Component({
  selector: 'app-pdf-generater',
  templateUrl: './pdf-generater.component.html',
  styleUrls: ['./pdf-generater.component.css']
})
export class PdfGeneraterComponent implements OnInit {
  single: any[];
  // view: any[] = [350, 200];
    view: any[] = [330, 150];
   multi: any[];
  // // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Country';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA'],
  };

  constructor() {
    this.multi= [
      {
        name: 'Germany',
        series: [
          {
            name: '2010',
            value: 10,
          },
          {
            name: '2011',
            value: 80,
          },
          {
            name: '2012',
            value: 10,
          },
        ],
      },
    
      {
        name: 'USA',
        series: [
          {
            name: '2010',
            value: 50,
          },
          {
            name: '2011',
            value: 30,
          },
          {
            name: '2012',
            value: 20,
          },
        ],
      },
    ];
    
    Object.assign(this.multi);
  }

  // onSelect(event) {
  //   console.log(event);
  // }
  // options
  // showXAxis: boolean = true;
  // showYAxis: boolean = true;
  // gradient: boolean = false;
  // showLegend: boolean = false;
  // showXAxisLabel: boolean = true;
  // yAxisLabel: string = 'Country';
  // showYAxisLabel: boolean = true;
  // xAxisLabel: string = 'Population';
  
  // colorScheme = {
  //   domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  // };

 // constructor() {
  //   this.single = [
  //     {
  //       name: 'Germany',
  //       value: 8940000,
  //     },
  //     {
  //       name: 'USA',
  //       value: 5000000,
  //     },
  //   ];
    
  //   Object.assign(this.single);
// }

  onSelect(data): void {
  }

  onActivate(data): void {
  }

  onDeactivate(data): void {
  }

  ngOnInit(): void {
  }
  
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  
}
