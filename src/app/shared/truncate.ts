import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform{
  transform(value: string, limit: number): string {
    return value.length < limit
      ? value
      : value.slice(0, limit) + '...';
  }
  // transform(value: string, args: any) : string {
  //   let limit = args.length > 0 ? parseInt(args, args) : args;
  //   let trail = args.length > 1 ? args : '...';
  //   return value.length > limit ? value.substring(0, limit) + trail : value;
  // }
}
