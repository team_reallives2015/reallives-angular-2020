import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { DOCUMENT } from '@angular/common';
import { HostListener, Inject, Injectable } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CarrerService } from '../home/action-carrer/carrer.service';
import { FinanceService } from '../home/action-finance/finance.service';
import { ConstantService } from './constant.service';
import { NumberFormatePipe } from './number-formate.pipe';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  audio;
  //url for local
  //  url = "http://localhost:3000/";

  // url for live version
  url = " https://api.reallivesworld.com/"
  logOutUrl = "https://reallivesworld.com/login"
  buyNowUrl = "https://reallivesworld.com/purchase_license_list"
    renewUrl = "https://reallivesworld.com/licenses"

  // for dashbord
  // url = "https://devapi.reallivesworld.com/";
  // logOutUrl = "https://dev.reallivesworld.com/login"
  // buyNowUrl = "https://dev.reallivesworld.com/purchase_license_list"
  // renewUrl = "https://dev.reallivesworld.com/licenses"
  checkFormat;
  openedWindow;
  elem: any;
  fullScreenFlag: boolean;
  currentStatus: string;
  currentStatusValue: number;
  avgWealth: number;
  meanWealth: number;
  firstValueW: number;
  secondValueW: number;
  thirdValueW: number;
  fourthValueW: number;
  firstValueI: number;
  secondValueI: number;
  thirdValueI: number;
  fourthValueI: number;
  bCfirstValueI: number;
  bCsecondValueI: number;
  bCthirdValueI: number;
  bCfourthValueI: number;
  wealthStatus: number;
  wealthValue: number;
  incomeStatus: number;
  incomeValue: number;
  sdgName;
  sdgColour;
  sdgTooltipe
  sdgChaName;
  education;
  sdgSubGoalId;
  finalSdg;
  statusValue = ""
  statusString="";
  //  private document: any
  constructor(@Inject(DOCUMENT) private document: any,
    public translate: TranslateService,
    public constantService: ConstantService,
    public numberPipe: NumberFormatePipe,
    public route: ActivatedRoute,
    private router: Router
  ) {
    this.audio = new Audio();
  }

  soundOnOff(value) {
    this.audio.muted = value;
  }

  getRandomValue(min, max) {
    let random = Math.random() * (max - min) + min;
    return random;
  }

  getRandomValueint(min, max) {
    let random = Math.floor(Math.random() * (max - min) + min);
    return random;

  }

  fullScreen() {
    this.elem = document.documentElement;
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
  }

  removeFullScreen() {
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }

  checkNumberFormat(number) {
    let newNumber
    if (number != undefined) {

      let abs = Math.abs(number);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        newNumber = (number / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = " T";
        number = newNumber + this.checkFormat;
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        newNumber = (number / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = " B";
        number = newNumber + this.checkFormat;
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        newNumber = (number / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = " M";
        number = newNumber + this.checkFormat;
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        newNumber = (number / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = " K";
        number = newNumber + this.checkFormat;
        // else{
        //   number=parseInt(number)  
        //   if(number>0){  
        //     number = this.numberPipe.transform(number);   
        //   }
        // }
      }
      else {
        number = parseInt(number);
        if (number > 0) {
          number = this.numberPipe.transform(number);
        }
      }
    }
    return number;
  }

  
  getSatusString(value) {
    if (value === 0) {
      this.currentStatus = this.translate.instant('gameLabel.Extremely_poor');
    }
    else if (value === 1) {
      this.currentStatus = this.translate.instant('gameLabel.poor');
    }
    else if (value === 2) {
      this.currentStatus = this.translate.instant('gameLabel.Middle_income');
    }
    else if (value === 3) {
      this.currentStatus = this.translate.instant('gameLabel.Rich');
    }
    else if (value === 4) {
      this.currentStatus = this.translate.instant('gameLabel.Super_rich');
    }
    return this.currentStatus;

  }

  getStatusValue(value) {
    if (value === this.constantService.Extreme_Poor) {
      this.currentStatusValue = 0;
    }
    else if (value === this.constantService.Poor) {
      this.currentStatusValue = 1;
    }
    else if (value === this.constantService.Well_To_Do) {
      this.currentStatusValue = 2;
    }
    else if (value === this.constantService.rich) {
      this.currentStatusValue = 3;
    }
    else if (value === this.constantService.super_rich) {
      this.currentStatusValue = 4;
    }
    return this.currentStatusValue;
  }

  calculateWealthStatus(welathMeataData, allPerson) {
    welathMeataData.wealth = (welathMeataData.wealth);
    let value1 = welathMeataData.per20 * welathMeataData.wealth
    let value2 = (welathMeataData.per50 - welathMeataData.per20) * welathMeataData.wealth
    let value3 = (welathMeataData.per90 - welathMeataData.per50) * welathMeataData.wealth
    let value4 = (welathMeataData.per99 - welathMeataData.per90) * welathMeataData.wealth
    let value5 = (welathMeataData.per100 - welathMeataData.per99) * welathMeataData.wealth
    let totalWealth1 = value1 + value2 + value3 + value4 + value5;
    let totalWealth2 = totalWealth1 / welathMeataData.pop2021;
    this.avgWealth = (totalWealth2 * (10 ** 9));
    let wealthDeviation = welathMeataData.stdDeviation
    let meanIncomeSeq = wealthDeviation * wealthDeviation / 2
    let logWealth = Math.log(this.avgWealth * allPerson[this.constantService.FAMILY_SELF].country.AverageFamilyCount);
    this.meanWealth = (logWealth - meanIncomeSeq)
    let f1 = (this.meanWealth + (this.constantService.Economically_Challenged_Wealth) * wealthDeviation)
    let f3 = (Math.exp(f1))
    this.firstValueW = (f3);
    this.secondValueW = (Math.exp(this.meanWealth + (this.constantService.Bearly_Managed_Wealth * wealthDeviation)));
    this.thirdValueW = (Math.exp(this.meanWealth + (this.constantService.Well_to_do__Wealth * wealthDeviation)));
    this.fourthValueW = (Math.exp(this.meanWealth + (this.constantService.Rich_Wealth * wealthDeviation)));
    let networthInDoller = (allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth / allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate);
    if (networthInDoller < this.firstValueW) {
      this.wealthStatus = 0;
    }
    else if (networthInDoller > this.firstValueW && networthInDoller < this.secondValueW) {
      this.wealthStatus = 1;
    }
    else if (networthInDoller > this.secondValueW && networthInDoller < this.thirdValueW) {
      this.wealthStatus = 2;
    }
    else if (networthInDoller > this.thirdValueW && networthInDoller < this.fourthValueW) {
      this.wealthStatus = 3;
    }
    else if (networthInDoller > this.fourthValueW) {
      this.wealthStatus = 4;
    }
    let log = Math.log(networthInDoller)
    let l = (log - this.meanWealth);
    let l2 = l / wealthDeviation;
    let exp = l2 + 10;
    let d1 = Math.max(Math.round(exp * 5), 0);
    this.wealthValue = Math.min(d1, 100);
  }

  calculateIncameStatus(incomeMataData, allPerson) {
    let meanIncome = incomeMataData.meanIncome
    let incomeDeviation = incomeMataData.incomeDeviation
    let exchangeRate = incomeMataData.exchangeRate;
    let f1 = (meanIncome + (this.constantService.Economically_Challenged) * incomeDeviation)
    let f3 = (Math.exp(f1))
    this.firstValueI = (f3);
    this.secondValueI = (Math.exp(meanIncome + (this.constantService.Bearly_Managed * incomeDeviation)));
    this.thirdValueI = (Math.exp(meanIncome + (this.constantService.Well_to_do * incomeDeviation)));
    this.fourthValueI = (Math.exp(meanIncome + (this.constantService.Rich * incomeDeviation)));
    let hIncome = (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome * 12) / exchangeRate;
    let avgHouseholdIncome = hIncome;
    if (avgHouseholdIncome < this.firstValueI) {
      this.incomeStatus = 0;
    }
    else if (avgHouseholdIncome > this.firstValueI && avgHouseholdIncome < this.secondValueI) {
      this.incomeStatus = 1;
    }
    else if (avgHouseholdIncome > this.secondValueI && avgHouseholdIncome < this.thirdValueI) {
      this.incomeStatus = 2;
    }
    else if (avgHouseholdIncome > this.thirdValueI && avgHouseholdIncome < this.fourthValueI) {
      this.incomeStatus = 3;
    }
    else if (avgHouseholdIncome > this.fourthValueI) {
      this.incomeStatus = 4;
    }
    let log = Math.log(avgHouseholdIncome)
    let l = (log - meanIncome);
    let l2 = l / incomeDeviation;
    let exp = l2 + 10;
    let d1 = Math.max(Math.round(exp * 5), 0);
    this.incomeValue = Math.min(d1, 100);

  }


  getSdgNameFromId(sdgId) {
    if (sdgId === 1) {
      this.sdgName = "one";
    }
    else if (sdgId === 2) {
      this.sdgName = "two";
    }
    else if (sdgId === 3) {
      this.sdgName = "three";
    }
    else if (sdgId === 4) {
      this.sdgName = "four";
    }
    else if (sdgId === 5) {
      this.sdgName = "five";
    }
    else if (sdgId === 6) {
      this.sdgName = "six";
    }
    else if (sdgId === 1) {
      this.sdgName = "first";
    }
    else if (sdgId === 7) {
      this.sdgName = "seven";
    }
    else if (sdgId === 8) {
      this.sdgName = "eight";
    }
    else if (sdgId === 9) {
      this.sdgName = "nine";
    }
    else if (sdgId === 10) {
      this.sdgName = "ten";
    }
    else if (sdgId === 11) {
      this.sdgName = "eleven";
    }
    else if (sdgId === 12) {
      this.sdgName = "twelve";
    }
    else if (sdgId === 13) {
      this.sdgName = "thirteen";
    }
    else if (sdgId === 14) {
      this.sdgName = "fourteen";
    }
    else if (sdgId === 15) {
      this.sdgName = "fifteen";
    }
    else if (sdgId === 16) {
      this.sdgName = "sixteen";
    }
    else if (sdgId === 17) {
      this.sdgName = "seventeen";
    }
    return this.sdgName;
  }


  setEducationForAll(person) {
    let education = person.education;
    if (person.age < 6 && !education.school.inSchool) {
      this.education = this.translate.instant('gameLabel.Not_in_school_yet');
    }
    else if (person.age === 6 && !education.school.inSchool) {
      this.education = this.translate.instant('gameLabel.Not_in_school_yet');
    }
    else {
      if (person.age >= 6 && education.school.inSchool) {
        this.education = this.translate.instant('gameLabel.Attending_school');
      }
      else if (education.school.highSchoolGraduate && !education.school.schoolDropOut) {
        this.education = this.translate.instant('gameLabel.School_completed');
      }
      else if (!education.school.highSchoolGraduate && education.school.schoolDropOut && !education.school.inSchool && education.school.schoolCompletedYears > 0) {
        this.translate.get('gameLabel.School_drop_out', { years: education.school.schoolCompletedYears }).subscribe(
          (s: String) => {
            this.education = s;
          })
        // this.education="School drop out (" +education.school.schoolCompletedYears +" years completed)"
      }
      else if (!education.school.highSchoolGraduate && education.school.schoolDropOut && !education.school.inSchool && education.school.schoolCompletedYears === 0) {
        this.education = this.translate.instant('gameLabel.Wasn_able_to_enroll_for_school')
      }

      if (education.college.inCollege) {
        this.education = this.translate.instant('gameLabel.Attending_college');
      }
      else if (education.college.collegeGraduate) {
        this.education = this.translate.instant('gameLabel.College_completed');
      }
      else if (education.college.collegeDropOut && !education.college.collegeGraduate) {
        this.translate.get('gameLabel.College_dropOut', { years: education.college.collegeCompletedYears }).subscribe(
          (s: String) => {
            this.education = s;
          })
        // this.education="College dropOut (" +education.college.collegeCompletedYears +" years completed)"
      }


      if (education.graduate.inGraduate) {
        this.education = this.translate.instant('gameLabel.Attending_graduate');
      }
      else if (education.graduate.graduateDegree && !education.graduate.graduateDropOut) {
        this.education = this.translate.instant('gameLabel.Completed_graduate');
      }
      else if (!education.graduate.graduateDegree && education.graduate.graduateDropOut) {
        this.translate.get('gameLabel.Graduate_dropOut', { years: education.graduate.graduateCompletedYears }).subscribe(
          (s: String) => {
            this.education = s;
          })
        // this.education="Graduate dropOut (" +education.graduate.graduateCompletedYears +" years completed)"
      }
    }
    return this.education;
  }
  public removeParamFromUrl(paramMap: ParamMap, keysToRemove: string[]) {
    const queryParams = {};
    const keysToKeep = paramMap.keys.filter(k => !keysToRemove.includes(k));
    keysToKeep.forEach(k => (queryParams[k] = paramMap.get(k)));

    this.router.navigate([], { queryParams, replaceUrl: true, relativeTo: this.route });
  }
  public filter(paramMap: ParamMap, key: string) {
    if (!paramMap.has(key)) { return; }
  }


  close() {
    this.constantService.removeToken();
    window.location.href = this.logOutUrl;
  }
  getSdgStringAndClour(id) {
    if (id === 0) {
      this.sdgChaName = this.translate.instant('gameLabel.major_challenges');
      this.sdgColour = "text-red";
    }
    else if (id === 1) {
      this.sdgChaName = this.translate.instant('gameLabel.significant_challenges');
      this.sdgColour = "text-orange";
    }
    else if (id === 2) {
      this.sdgChaName = this.translate.instant('gameLabel.challenges_remain');
      this.sdgColour = "text-yellow";
    }
    else if (id === 3) {
      this.sdgChaName = this.translate.instant('gameLabel.goal_achivments');
      this.sdgColour = "text-success";
    }
    else if (id === 4) {
      this.sdgChaName = this.translate.instant('gameLabel.No_data');
      this.sdgColour = "text-secondary ";
    }

  }

  getSdgStringSplite(string) {
    var splits = string.split("_")
    this.sdgSubGoalId = splits[0]
    let colorId = splits[1]
    this.getSdgStringAndClour(parseInt(colorId));
    this.sdgTooltipe = this.sdgChaName + ":" + splits[2]
    this.finalSdg = this.sdgSubGoalId + " " + this.sdgChaName;
  }

  public randomArrayShuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }


  public getColurForStatus(id) {
    let cValue = "";
    if (id === 0) {
      cValue = "colourRed"
    }
    else if (id === 1) {
      cValue = "colourYellow"
    }
    else if (id === 2) {
      cValue = "colourOrange"
    } else if (id === 3) {
      cValue = "colourGreen"
    } else if (id === 4) {
      cValue = "colourBlue"
    }
    return cValue
  }

  public calculateWealthStatusForTool(welathMeataData: any, country: any) {
    welathMeataData.wealth = (welathMeataData.wealth);
    let value1 = welathMeataData.per20 * welathMeataData.wealth
    let value2 = (welathMeataData.per50 - welathMeataData.per20) * welathMeataData.wealth
    let value3 = (welathMeataData.per90 - welathMeataData.per50) * welathMeataData.wealth
    let value4 = (welathMeataData.per99 - welathMeataData.per90) * welathMeataData.wealth
    let value5 = (welathMeataData.per100 - welathMeataData.per99) * welathMeataData.wealth
    let totalWealth1 = value1 + value2 + value3 + value4 + value5;
    let totalWealth2 = totalWealth1 / welathMeataData.pop2021;
    this.avgWealth = (totalWealth2 * (10 ** 9));
    let wealthDeviation = welathMeataData.stdDeviation
    let meanIncomeSeq = wealthDeviation * wealthDeviation / 2
    let logWealth = Math.log(this.avgWealth * country.AverageFamilyCount);
    this.meanWealth = (logWealth - meanIncomeSeq)
    let f1 = (this.meanWealth + (this.constantService.Economically_Challenged_Wealth) * wealthDeviation)
    let f3 = (Math.exp(f1))
    this.firstValueW = (f3);
    this.secondValueW = (Math.exp(this.meanWealth + (this.constantService.Bearly_Managed_Wealth * wealthDeviation)));
    this.thirdValueW = (Math.exp(this.meanWealth + (this.constantService.Well_to_do__Wealth * wealthDeviation)));
    this.fourthValueW = (Math.exp(this.meanWealth + (this.constantService.Rich_Wealth * wealthDeviation)));
  }

  public calculateIncameStatusForTool(incomeMataData: any, country: any) {
    let meanIncome = incomeMataData.meanIncome
    let incomeDeviation = incomeMataData.incomeDeviation
    let exchangeRate = incomeMataData.exchangeRate;
    let f1 = (meanIncome + (this.constantService.Economically_Challenged) * incomeDeviation)
    let f3 = (Math.exp(f1))
    this.firstValueI = (f3);
    this.secondValueI = (Math.exp(meanIncome + (this.constantService.Bearly_Managed * incomeDeviation)));
    this.thirdValueI = (Math.exp(meanIncome + (this.constantService.Well_to_do * incomeDeviation)));
    this.fourthValueI = (Math.exp(meanIncome + (this.constantService.Rich * incomeDeviation)));

  }


  calculateIncameStatusForChangedBornCountry(incomeMataData: any, country: any) {
    let meanIncome = incomeMataData.meanIncome
    let incomeDeviation = incomeMataData.incomeDeviation
    let exchangeRate = incomeMataData.exchangeRate;
    let f1 = (meanIncome + (this.constantService.Economically_Challenged) * incomeDeviation)
    let f3 = (Math.exp(f1))
    this.bCfirstValueI = (f3);
    this.bCsecondValueI = (Math.exp(meanIncome + (this.constantService.Bearly_Managed * incomeDeviation)));
    this.bCthirdValueI = (Math.exp(meanIncome + (this.constantService.Well_to_do * incomeDeviation)));
    this.bCfourthValueI = (Math.exp(meanIncome + (this.constantService.Rich * incomeDeviation)));

  }

  getColor(status) {
    switch (status) {
      case 0:
        return '#e30000';
      case 1:
        return '#FFF600';
      case 2:
        return '#e76b00';
      case 3:
        return '#0000FF';
      case 4:
        return '#34db00';
    }
  }
  checkPopulationFormat(Population) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = (Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }

  checkBusinessTyoeOldOrNew(business) {
    let oldBusinessFlag: boolean = false;
    if (business.businessesId === 130) {
      oldBusinessFlag = false
    }
    else if (business.businessesId !== 130) {
      if ((business).hasOwnProperty('businessType')) {
        oldBusinessFlag = false
      }
      else {
        oldBusinessFlag = true
      }
    }
    return oldBusinessFlag
  }

  getAge17StatusString(ecoStatusId){
    if (ecoStatusId === 0) {
      this.statusString = this.translate.instant('gameLabel.Status1');
    }
    else if (ecoStatusId === 1) {
      this.statusString = this.translate.instant('gameLabel.Status2');
    }
    else if (ecoStatusId === 2) {
      this.statusString = this.translate.instant('gameLabel.Status3');
    }
    else if (ecoStatusId === 3) {
      this.statusString = this.translate.instant('gameLabel.Status4');
    }
    else if (ecoStatusId === 4) {
      this.statusString = this.translate.instant('gameLabel.Status5');
    }
    return this.statusString;

  }

  licenExpireClick(){
    this.constantService.removeToken();
    window.location.href = this.renewUrl;
  }
  }


