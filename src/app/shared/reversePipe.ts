/**
 * Created by purva.deshpande on 17-01-2017.
 */
 import { Pipe, PipeTransform } from '@angular/core';

 @Pipe({
   name: 'displayReverse',
   pure: false
 })
 export class DisplayReversePipe {
   transform(value) {
     return value.slice().reverse();
   }
 }
 