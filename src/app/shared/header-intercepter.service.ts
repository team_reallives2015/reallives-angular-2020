import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeaderIntercepterService implements HttpInterceptor{

  constructor() { }

intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
     const token: string = localStorage.getItem('token');
  if (request.headers.get('Anonymous') == '') {
    const newHeaders = request.headers.delete('Anonymous')
    request = request.clone({ headers: newHeaders });
  } 
  else if (token) {
      request = request.clone({ headers: request.headers.set('Authorization',  token) });
      if (!request.headers.has('Content-Type')) {
        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }
  } 
  request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

  return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
          }
          return event;
      }));
}
}