import { DOCUMENT } from '@angular/common';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CommonService } from './common.service';
import { ConstantService } from './constant.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingInterceptorService implements HttpInterceptor {

  constructor( private constantService :ConstantService,
               public commonService:CommonService,
               private router :Router,
               @Inject(DOCUMENT) private document: Document) { }
  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(httpRequest)
     .pipe(retry(1),
       catchError((error: HttpErrorResponse) => {
        if (error.status === 0 || error.status === 410) {
          alert(this.constantService.MESSAGE_ACCESSTOKEN_EXPIRED);
          this.constantService.removeToken();
          this.document.location.href = this.commonService.logOutUrl;
        } else if (error.status === 500) {
          this.router.navigate(['/language']);
          alert(this.constantService.MESSAGE_INTERNAL_SERVER_ERROR);
        }else if(error.status===409){
          this.router.navigate(['/language']);
          alert(this.constantService.MESSAGE_USER_ALREADY_EXITS);
        }
        else if(error.status===404)
        {
          alert("User does not exits");
         this.constantService.removeToken();
         this.document.location.href = this.commonService.logOutUrl;
        }
        else if(error.status===400){
          alert("Bad Request Error!!");
        }
        else if(error.status===403){
          alert(this.constantService.MESSAGE_ACCESSTOKEN_EXPIRED);
          this.constantService.removeToken();
          this.document.location.href = this.commonService.logOutUrl;
        }
        else if(error.status===402){
           alert(this.constantService.MESSAGE_ACCESSTOKEN_EXPIRED);
          this.constantService.removeToken();
          this.document.location.href = this.commonService.logOutUrl;
        }
        else if(error.status===401){
          alert("A multiple login has been detected! Re-login to continue.");
          this.constantService.removeToken();
          this.document.location.href = this.commonService.logOutUrl;
        }
        else{
          alert("Error StatusText: " + error.statusText);
        }
        return throwError(error);
       })
     )
 }
}
