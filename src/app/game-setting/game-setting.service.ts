import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { CommonService } from '../shared/common.service';
import { ConstantService } from '../shared/constant.service';

@Injectable({
  providedIn: 'root'
})
export class GameSettingService {

  constructor(private http: HttpClient,
    private commonService: CommonService,
    private constantService: ConstantService) { }

  getAllGameSetting() {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getGameSettings`);
  }

  updateAllGameSetting(settings) {
    return this.http.post<boolean>(`${this.commonService.url}game/gameSettingsUpdate`, { settings });

  }

}



