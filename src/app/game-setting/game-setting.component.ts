import { Component, DoCheck, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from '../game-summery/game-summery.service';
import { modalWindowShowHide } from '../modal-window-show-hide.service';
import { TranslationService } from '../translation/translation.service';
import { GameSettingService } from './game-setting.service';

@Component({
  selector: 'app-game-setting',
  templateUrl: './game-setting.component.html',
  styleUrls: ['./game-setting.component.css']
})
export class GameSettingComponent implements OnInit, DoCheck {
  gameSettings;
  checkIsPersonDead: boolean = false;
  //disasters
  allDisasters: boolean;
  Naturaldiseases: boolean;
  CONSCIENCE1: boolean;
  FamiliyandSexuality: boolean;
  Governmenthumanrights: boolean;
  CountryandCulture: boolean;
  CrimeandAbuse: boolean;
  Health: boolean;
  Discrimination: boolean;
  Discrimination1: boolean;
  MessageQuestion;

  //conscience

  CONSCIENCE = false;
  CHARITY = false;
  CRIME = false;
  GREED = false;
  SELFSACRIFICE = false;
  TRUTHFULNESS = false;
  TAKINGASTAND = false;
  VOLUNTEERING = false;

  //Family and Sexuality
  HOMOSEXUALITY = false;
  INFERTILITY = false;
  OUTOFWEDLOCKCHILDERN = false;
  PREGNANCYCHILDREN = false;
  ROMANTICRELATIONSHIPS = false;
  SEXUALLYTRANSMITTEDDISEASES = false;

  //Government/Human Roghts
  CONSCRIPTION = false; GOVERMENTCORRUPTION = false; HUMANRIGHTABUSES = false; WAR = false; WELFAREGOVERMENTASSISTANCE = false;

  //Country and culture
  CUSTOMS = false; CLIMATE = false; HOUSING = false; CUISINE = false; ARTS = false; SPORTS = false; HISTORY = false; NATURE = false; TRADITIONS = false; RELIGION = false;


  //Crime and abuses
  ASSULT = false; BURGLARY = false; EMOTIONALABUSE = false; MOTORVEHICLETHEFT = false; MURDER = false; PHYSICALABUSE = false; RAPE = false; ROBBERY = false; SEXUALABUSE = false; WORKPLACECORRUPTION = false;

  //Health
  ACCIDENTSANDTRAUMA = false; BIRTHDEFECTS = false; CANCERS = false; CARDIOVASCULARDISEASES = false; DEGENERATIVECONDITION = false; INFECTIOUSDISSEASE = false; MALNUTRITION = false; PARASITICDISEASES = false; PSYCHOLOGICALDISORDERS = false; SUBSTANCEADUSE = false; OTHERCONDITIONS = false;

  //Discrimination
  AGEDICRIMINATION = false; CULTURALRACIALDISCRIMINATION = false; SEXUALDISCRIMINATIONS = false; ALCOHOLUSE = false; DRUGUSE = false; SMOKING = false; LOVE = false; CORRUPTION = false;

  gameId;
  successMsgFlag: boolean = false;
  messageText;
  constructor(public modalWindowShowHide: modalWindowShowHide,
    private gameSettingService: GameSettingService,
    private translater: TranslateService,
    public translationService: TranslationService,
    public gameSummaryService: GameSummeryService) { }


  ngDoCheck(): void {
    if (this.CONSCIENCE === true && this.CHARITY == true && this.CRIME === true && this.VOLUNTEERING === true && this.GREED === true && this.SELFSACRIFICE === true && this.TRUTHFULNESS === true && this.TAKINGASTAND === true) {
      this.CONSCIENCE1 = true;
    }
    else if (this.CONSCIENCE === false && this.CHARITY === false && this.CRIME === false && this.VOLUNTEERING === false && this.GREED === false && this.SELFSACRIFICE === false && this.TRUTHFULNESS === false && this.TAKINGASTAND === false) {
      this.CONSCIENCE1 = false;
    }

    if (this.Naturaldiseases === true) {
      this.allDisasters = true;
    }
    else if (this.Naturaldiseases === false) {
      this.allDisasters = false;
    }


    if (this.HOMOSEXUALITY === true && this.INFERTILITY == true && this.OUTOFWEDLOCKCHILDERN === true && this.PREGNANCYCHILDREN === true && this.ROMANTICRELATIONSHIPS === true && this.SEXUALLYTRANSMITTEDDISEASES === true && this.LOVE === true) {
      this.FamiliyandSexuality = true;
    } else if (this.HOMOSEXUALITY === false && this.INFERTILITY == false && this.OUTOFWEDLOCKCHILDERN === false && this.PREGNANCYCHILDREN === false && this.ROMANTICRELATIONSHIPS === false && this.SEXUALLYTRANSMITTEDDISEASES === false && this.LOVE === false) {
      this.FamiliyandSexuality = false;
    }

    if (this.CONSCRIPTION === true && this.GOVERMENTCORRUPTION == true && this.HUMANRIGHTABUSES === true && this.WAR === true && this.WELFAREGOVERMENTASSISTANCE === true && this.CORRUPTION === true) {
      this.Governmenthumanrights = true;
    } else if (this.CONSCRIPTION === false && this.GOVERMENTCORRUPTION === false && this.HUMANRIGHTABUSES === false && this.WAR === false && this.WELFAREGOVERMENTASSISTANCE === false && this.CORRUPTION === false) {
      this.Governmenthumanrights = false;
    }

    if (this.CUSTOMS === true && this.CLIMATE == true && this.ARTS === true && this.HOUSING === true && this.CUISINE === true && this.SPORTS === true && this.HISTORY === true && this.NATURE === true && this.TRADITIONS === true && this.RELIGION === true) {
      this.CountryandCulture = true;
    } else if (this.CUSTOMS === false && this.CLIMATE == false && this.ARTS === false && this.HOUSING === false && this.CUISINE === false && this.SPORTS === false && this.HISTORY === false && this.NATURE === false && this.TRADITIONS === false && this.RELIGION === false) {
      this.CountryandCulture = false;
    }
    if (this.ASSULT === true && this.BURGLARY == true && this.EMOTIONALABUSE === true && this.MOTORVEHICLETHEFT === true && this.MURDER === true && this.PHYSICALABUSE === true && this.RAPE === true && this.ROBBERY === true && this.SEXUALABUSE === true && this.WORKPLACECORRUPTION === true) {
      this.CrimeandAbuse = true;
    } else if (this.ASSULT === false && this.BURGLARY == false && this.EMOTIONALABUSE === false && this.MOTORVEHICLETHEFT === false && this.MURDER === false && this.PHYSICALABUSE === false && this.RAPE === false && this.ROBBERY === false && this.SEXUALABUSE === false && this.WORKPLACECORRUPTION === false) {
      this.CrimeandAbuse = false;
    }

    if (this.ACCIDENTSANDTRAUMA === true && this.BIRTHDEFECTS == true && this.CANCERS === true && this.CARDIOVASCULARDISEASES === true && this.DEGENERATIVECONDITION === true && this.INFECTIOUSDISSEASE === true && this.MALNUTRITION === true && this.PARASITICDISEASES === true && this.PSYCHOLOGICALDISORDERS === true && this.SUBSTANCEADUSE === true && this.OTHERCONDITIONS === true) {
      this.Health = true;
    } else if (this.ACCIDENTSANDTRAUMA === false && this.BIRTHDEFECTS == false && this.CANCERS === false && this.CARDIOVASCULARDISEASES === false && this.DEGENERATIVECONDITION === false && this.INFECTIOUSDISSEASE === false && this.MALNUTRITION === false && this.PARASITICDISEASES === false && this.PSYCHOLOGICALDISORDERS === false && this.SUBSTANCEADUSE === false && this.OTHERCONDITIONS === false) {
      this.Health = false;
    }

    if (this.AGEDICRIMINATION === true && this.CULTURALRACIALDISCRIMINATION == true && this.SEXUALDISCRIMINATIONS === true) {
      this.Discrimination = true;
    } else if (this.AGEDICRIMINATION === false && this.CULTURALRACIALDISCRIMINATION == false && this.SEXUALDISCRIMINATIONS === false) {
      this.Discrimination = false;
    }
    if (this.ALCOHOLUSE === true && this.DRUGUSE == true && this.SMOKING === true) {
      this.Discrimination1 = true;
    } else if (this.ALCOHOLUSE === false && this.DRUGUSE == false && this.SMOKING === false) {
      this.Discrimination1 = false;
    }

  }

  onChange(value: boolean, casedata: string) {
    if (casedata == '1') {
      if (value === true) {
        this.CONSCIENCE = false;
        this.CHARITY = false;
        this.CRIME = false
          ;
        this.VOLUNTEERING = false;
        this.GREED = false;
        this.SELFSACRIFICE = false;
        this.TRUTHFULNESS = false;
        this.TAKINGASTAND = false;
      }
      else {
        this.CONSCIENCE = true;
        this.CHARITY = true;
        this.CRIME = true
          ;
        this.VOLUNTEERING = true;
        this.GREED = true;
        this.SELFSACRIFICE = true;
        this.TRUTHFULNESS = true;
        this.TAKINGASTAND = true;
      }
    }
    else if (casedata == '2') {
      if (value === true) {
        this.Naturaldiseases = false;
      }
      else {
        this.Naturaldiseases = true;
      }
    }
    else if (casedata === '3') {
      if (value === true) {
        this.HOMOSEXUALITY = false;
        this.INFERTILITY = false;
        this.OUTOFWEDLOCKCHILDERN = false;
        this.PREGNANCYCHILDREN = false;
        this.ROMANTICRELATIONSHIPS = false;
        this.SEXUALLYTRANSMITTEDDISEASES = false;
        this.LOVE = false;

      }
      else {
        this.HOMOSEXUALITY = true;
        this.INFERTILITY = true;
        this.OUTOFWEDLOCKCHILDERN = true;
        this.PREGNANCYCHILDREN = true;
        this.ROMANTICRELATIONSHIPS = true;
        this.SEXUALLYTRANSMITTEDDISEASES = true;
        this.LOVE = true;
      }
    }
    else if (casedata === '4') {
      if (value === true) {
        this.CONSCRIPTION = false;
        this.GOVERMENTCORRUPTION = false;
        this.HUMANRIGHTABUSES = false;
        this.WAR = false;
        this.WELFAREGOVERMENTASSISTANCE = false;
        this.CORRUPTION = false;
      }
      else if (value === false) {
        this.CONSCRIPTION = true;
        this.GOVERMENTCORRUPTION = true;
        this.HUMANRIGHTABUSES = true;
        this.WAR = true;
        this.WELFAREGOVERMENTASSISTANCE = true;
        this.CORRUPTION = true;
      }
    }
    else if (casedata === '5') {
      if (value === true) {
        this.CUSTOMS = false;
        this.CLIMATE = false;
        this.ARTS = false;
        this.HOUSING = false;
        this.CUISINE = false;
        this.SPORTS = false;
        this.HISTORY = false;
        this.NATURE = false;
        this.TRADITIONS = false;
        this.RELIGION = false;

      }
      else {
        this.CUSTOMS = true;
        this.CLIMATE = true;
        this.ARTS = true;
        this.HOUSING = true;
        this.CUISINE = true;
        this.SPORTS = true;
        this.HISTORY = true;
        this.NATURE = true;
        this.TRADITIONS = true;
        this.RELIGION = true;
      }
    }
    else if (casedata == '6') {
      if (value === true) {
        this.ASSULT = false;
        this.BURGLARY = false;
        this.EMOTIONALABUSE = false;
        this.MOTORVEHICLETHEFT = false;
        this.MURDER = false;
        this.PHYSICALABUSE = false;
        this.RAPE = false;
        this.ROBBERY = false;
        this.SEXUALABUSE = false;
        this.WORKPLACECORRUPTION = false;


      }
      else {
        this.ASSULT = true;
        this.BURGLARY = true;
        this.EMOTIONALABUSE = true;
        this.MOTORVEHICLETHEFT = true;
        this.MURDER = true;
        this.PHYSICALABUSE = true;
        this.RAPE = true;
        this.ROBBERY = true;
        this.SEXUALABUSE = true;
        this.WORKPLACECORRUPTION = true;
      }
    }
    else if (casedata === '7') {
      if (value === true) {
        this.ACCIDENTSANDTRAUMA = false;
        this.BIRTHDEFECTS = false;
        this.CANCERS = false;
        this.CARDIOVASCULARDISEASES = false;
        this.DEGENERATIVECONDITION = false;
        this.INFECTIOUSDISSEASE = false;
        this.MALNUTRITION = false;
        this.PARASITICDISEASES = false;
        this.PSYCHOLOGICALDISORDERS = false;
        this.SUBSTANCEADUSE = false;
        this.OTHERCONDITIONS = false;

      }
      else {
        this.ACCIDENTSANDTRAUMA = true;
        this.BIRTHDEFECTS = true;
        this.CANCERS = true;
        this.CARDIOVASCULARDISEASES = true;
        this.DEGENERATIVECONDITION = true;
        this.INFECTIOUSDISSEASE = true;
        this.MALNUTRITION = true;
        this.PARASITICDISEASES = true;
        this.PSYCHOLOGICALDISORDERS = true;
        this.SUBSTANCEADUSE = true;
        this.OTHERCONDITIONS = true;
      }
    }
    else if (casedata === '8') {
      if (value === true) {
        this.AGEDICRIMINATION = false;
        this.CULTURALRACIALDISCRIMINATION = false;
        this.SEXUALDISCRIMINATIONS = false;
      }
      else {
        this.AGEDICRIMINATION = true;
        this.CULTURALRACIALDISCRIMINATION = true;
        this.SEXUALDISCRIMINATIONS = true;
      }
    }
    else if (casedata === '9') {
      if (value === true) {
        this.ALCOHOLUSE = false;
        this.DRUGUSE = false;
        this.SMOKING = false;
      }
      else {
        this.ALCOHOLUSE = true;
        this.DRUGUSE = true;
        this.SMOKING = true;
      }
    }

  }

  ngOnInit(): void {
    this.modalWindowShowHide.showGameSeetingsFlag = true;
    this.getGameSeetings();
  }


  getGameSeetings() {
    this.gameSettingService.getAllGameSetting().subscribe(
      res => {
        this.gameSettings = res;
        this.CONSCIENCE = this.gameSettings.CONSCIENCE;
        this.ALCOHOLUSE = this.gameSettings.ALCOHOLUSE;
        this.DRUGUSE = this.gameSettings.DRUGUSE;
        this.SMOKING = this.gameSettings.SMOKING;
        this.CHARITY = this.gameSettings.CHARITY;
        this.CRIME = this.gameSettings.CRIME;
        this.GREED = this.gameSettings.GREED;
        this.SELFSACRIFICE = this.gameSettings.SELFSACRIFICE;
        this.TRUTHFULNESS = this.gameSettings.TRUTHFULNESS;
        this.TAKINGASTAND = this.gameSettings.TAKINGASTAND;
        this.VOLUNTEERING = this.gameSettings.VOLUNTEERING;
        this.LOVE = this.gameSettings.LOVE;
        this.CORRUPTION = this.gameSettings.CORRUPTION;
        this.Naturaldiseases = this.gameSettings.NATUREALDIASTER;
        this.HOMOSEXUALITY = this.gameSettings.HOMOSEXUALITY;
        this.INFERTILITY = this.gameSettings.INFERTILITY;
        this.OUTOFWEDLOCKCHILDERN = this.gameSettings.OUTOFWEDLOCKCHILDERN;
        this.PREGNANCYCHILDREN = this.gameSettings.PREGNANCYCHILDREN;
        this.ROMANTICRELATIONSHIPS = this.gameSettings.ROMANTICRELATIONSHIPS;
        this.SEXUALLYTRANSMITTEDDISEASES = this.gameSettings.SEXUALLYTRANSMITTEDDISEASES;
        this.allDisasters = this.gameSettings.NATURALDISASTER;
        this.CONSCRIPTION = this.gameSettings.CONSCRIPTION;
        this.GOVERMENTCORRUPTION = this.gameSettings.GOVERMENTCORRUPTION;
        this.HUMANRIGHTABUSES = this.gameSettings.HUMANRIGHTABUSES;
        this.WAR = this.gameSettings.WAR;
        this.WELFAREGOVERMENTASSISTANCE = this.gameSettings.WELFAREGOVERMENTASSISTANCE;

        this.ACCIDENTSANDTRAUMA = this.gameSettings.ACCIDENTSANDTRAUMA;
        this.ASSULT = this.gameSettings.ASSULT;
        this.BURGLARY = this.gameSettings.BURGLARY;
        this.EMOTIONALABUSE = this.gameSettings.EMOTIONALABUSE;
        this.MOTORVEHICLETHEFT = this.gameSettings.MOTORVEHICLETHEFT;
        this.MURDER = this.gameSettings.MURDER;
        this.PHYSICALABUSE = this.gameSettings.PHYSICALABUSE;
        this.RAPE = this.gameSettings.RAPE;
        this.ROBBERY = this.gameSettings.ROBBERY;
        this.SEXUALABUSE = this.gameSettings.SEXUALABUSE;
        this.WORKPLACECORRUPTION = this.gameSettings.WORKPLACECORRUPTION;

        this.ARTS = this.gameSettings.ARTS;
        this.CUSTOMS = this.gameSettings.CUSTOMS;
        this.CLIMATE = this.gameSettings.CLIMATE;
        this.HOUSING = this.gameSettings.HOUSING;
        this.CUISINE = this.gameSettings.CUISINE;
        this.SPORTS = this.gameSettings.SPORTS;
        this.HISTORY = this.gameSettings.HISTORY;
        this.NATURE = this.gameSettings.NATURE;
        this.TRADITIONS = this.gameSettings.TRADITIONS;
        this.RELIGION = this.gameSettings.RELIGION;

        this.BIRTHDEFECTS = this.gameSettings.BIRTHDEFECTS;
        this.CANCERS = this.gameSettings.CANCERS;
        this.CARDIOVASCULARDISEASES = this.gameSettings.CARDIOVASCULARDISEASES;
        this.DEGENERATIVECONDITION = this.gameSettings.DEGENERATIVECONDITION;
        this.INFECTIOUSDISSEASE = this.gameSettings.INFECTIOUSDISSEASE;
        this.MALNUTRITION = this.gameSettings.MALNUTRITION;
        this.PARASITICDISEASES = this.gameSettings.PARASITICDISEASES;
        this.PSYCHOLOGICALDISORDERS = this.gameSettings.PSYCHOLOGICALDISORDERS;
        this.SUBSTANCEADUSE = this.gameSettings.SUBSTANCEADUSE;
        this.OTHERCONDITIONS = this.gameSettings.OTHERCONDITIONS;
      }
    );
  }


  updateGameSetting() {
    let gameSettingObject = {
      'CRIME': this.CRIME,
      'HUMANRIGHTABUSES': this.HUMANRIGHTABUSES,
      'TRUTHFULNESS': this.TRUTHFULNESS,
      'CUSTOMS': this.CUSTOMS,
      'VOLUNTEERING': this.VOLUNTEERING,
      'ACCIDENTSANDTRAUMA': this.ACCIDENTSANDTRAUMA,
      'ROMANTICRELATIONSHIPS': this.ROMANTICRELATIONSHIPS,
      'CLIMATE': this.CLIMATE,
      'CHARITY': this.CHARITY,
      'ROBBERY': this.ROBBERY,
      'HOMOSEXUALITY': this.HOMOSEXUALITY,
      'CARDIOVASCULARDISEASES': this.CARDIOVASCULARDISEASES,
      'CORRUPTION': this.CORRUPTION,
      'DRUGUSE': this.DRUGUSE,
      'PHYSICALABUSE': this.PHYSICALABUSE,
      'PARASITICDISEASES': this.PARASITICDISEASES,
      'BIRTHDEFECTS': this.BIRTHDEFECTS,
      'PREGNANCYCHILDREN': this.PREGNANCYCHILDREN,
      'SUBSTANCEADUSE': this.SUBSTANCEADUSE,
      'CONSCIENCE': this.CONSCIENCE,
      'RELIGION': this.RELIGION,
      'SEXUALABUSE': this.SEXUALABUSE,
      'BURGLARY': this.BURGLARY,
      'SEXUALLYTRANSMITTEDDISEASES': this.SEXUALLYTRANSMITTEDDISEASES,
      'MURDER': this.MURDER,
      'ASSULT': this.ASSULT,
      'SELFSACRIFICE': this.SELFSACRIFICE,
      'RAPE': this.RAPE,
      'CANCERS': this.CANCERS,
      'WAR': this.WAR,
      'SMOKING': this.SMOKING,
      'OTHERCONDITIONS': this.OTHERCONDITIONS,
      'EMOTIONALABUSE': this.EMOTIONALABUSE,
      'MALNUTRITION': this.MALNUTRITION,
      'SPORTS': this.SPORTS,
      'LOVE': this.LOVE,
      'GREED': this.GREED,
      'INFERTILITY': this.INFERTILITY,
      'HOUSING': this.HOUSING,
      'TRADITIONS': this.TRADITIONS,
      'CUISINE': this.CUISINE,
      'NATUREALDIASTER': this.Naturaldiseases,
      'MOTORVEHICLETHEFT': this.MOTORVEHICLETHEFT,
      'WORKPLACECORRUPTION': this.WORKPLACECORRUPTION,
      'TAKINGASTAND': this.TAKINGASTAND,
      'GOVERMENTCORRUPTION': this.GOVERMENTCORRUPTION,
      'INFECTIOUSDISSEASE': this.INFECTIOUSDISSEASE,
      'CONSCRIPTION': this.CONSCRIPTION,
      'ARTS': this.ARTS,
      'NATURE': this.NATURE,
      'WELFAREGOVERMENTASSISTANCE': this.WELFAREGOVERMENTASSISTANCE,
      'ALCOHOLUSE': this.ALCOHOLUSE,
      'PSYCHOLOGICALDISORDERS': this.PSYCHOLOGICALDISORDERS,
      'DEGENERATIVECONDITION': this.DEGENERATIVECONDITION,
      'OUTOFWEDLOCKCHILDERN': this.OUTOFWEDLOCKCHILDERN,
      'HISTORY': this.HISTORY


    };
    this.gameSettingService.updateAllGameSetting(gameSettingObject).subscribe(
      res => {
        this.successMsgFlag = true;
        this.messageText = (this.translater.instant('gameLabel.Save_Settings_Succesfully'));
      }

    )

  }

  successMsgWindowClose() {
    this.successMsgFlag = false;
    this.modalWindowShowHide.showGameSeetingsFlag = false;
  }

  closeSetting() {
    this.modalWindowShowHide.showGameSeetingsFlag = false;
  }


}
