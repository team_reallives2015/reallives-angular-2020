import { Inject, Injectable } from '@angular/core'

@Injectable()
export class modalWindowShowHide {

    showEducationModalWindow = false;
    showcharacterDesignFlag = false;
    showchearacterDesignSdgFlag = false
    showSdg = false;
    sdgCountrySelectedObject;
    showGroupCountry = false;
    groupIdSelectedCountryObject;
    showGameSeetingsFlag = false;
    showLoadGameFlag = false;
    showLoadCompleteLifeFlag = false;
    showFamilyCapsuleWindow = false;
    showCountryCapsule = false;
    showLifeSummery = false;
    showSdgIndicator = false;
    showActionEducation = false;
    showActionResidence = false;
    showActionLeisure = false;
    showActionRelation = false;
    showActionFinanace = false;
    showActionCareer = false;
    showWaitScreen: boolean = false;
    showGotoHome: boolean = false;
    showObituary: boolean = false;
    showGameSummary: boolean = false;
    showLetter: boolean = false;
    showFeddback: boolean = false;
    showBugReport: boolean = false;
    showAgeAYear: boolean = false;
    showWellBeingIndicatorWindow: boolean = false;
    showLanguageWindow: boolean = false;
    showLangSelectionFromIconFlag: boolean = false;
    showOrganBloodDonationFlag: boolean = false;
    showDecisionFlag: boolean = false;
    showEvent: boolean = false;
    showObituaryPdf: boolean = false;
    showCharacterDesighWithGropuFlag: boolean = false
    showAssignment: boolean = false
    showLifeExpectancyGraph: boolean = false;
    showOrganDonationCertificate: boolean = false
    showFinanceStatus: boolean = false;
    gotoHome: boolean = false;
    showSdgWithAssignMent: boolean = false;
    showBornSummary: boolean = false;
    showWellBeingIndicator: boolean = false
    showLangFromIcon: boolean = false;
    showSiblingFamily: boolean = false;
    showChildrenFamily: boolean = false;
    showGrandChildrenFamily: boolean = false;
    bornScreenSeetingsFlag: boolean = false;
    showKonwMoreFlag: boolean = false
    showRegisterCOuntrySelectFlag: boolean = false;
    showDocumentFlag: boolean = false;
    showCountryLearningTool: boolean = false;
    showDeletedLifeList: boolean = false;
    showDairy: boolean = false;
    showBusinessTabFlag: boolean = false;
    showPetFlag: boolean = false;
    ShowOldLifeFlag: boolean = false;
    showCharity: boolean = false;
}