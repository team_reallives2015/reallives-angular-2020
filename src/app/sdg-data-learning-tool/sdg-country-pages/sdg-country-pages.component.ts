import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { Router } from '@angular/router';
import { SdgToolServiceService } from '../sdg-tool-service.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { ConstantService } from 'src/app/shared/constant.service';

@Component({
  selector: 'app-sdg-country-pages',
  templateUrl: './sdg-country-pages.component.html',
  styleUrls: ['./sdg-country-pages.component.css']
})
export class SdgCountryPagesComponent implements OnInit {
  selectedCountryName = null;
  gameId;
  selectedcountry = [];
  designALifeObject = {};
  sdgUrl = "https://dashboards.sdgindex.org/profiles/"
  closHome: boolean = false;
  regSdgInfo: any[] = [];
  data: boolean = false
  colourCode: am4core.Color | undefined;
  sdgCountryListAllLast: any[] = [];
  modelClass = " modal fade new_cmn_modal md-overlay";
  sdgId: any
  countryList: any
  mapTitle: any;
  countryListSdg: any
  sdgMapTitle: any;
  firstWindow: boolean = false;
  secondWindow: boolean = false;
  sdgInfo: any;
  sdgTargets: any;
  sdgDetails: any;
  sdgImage: any
  sdgName: any
  sdgTitle: any
  sdgDescription: any
  sdgTargetCode: any;
  values: any[] = []
  regSdgSubGoalDeatails = [];
  disabledButton: boolean = false;
  sdgGreenCountryList: any[] = [];
  sdgYellowCountryList: any[] = [];
  sdgOrangeCountryList: any[] = [];
  sdgGrayCountryList: any[] = [];
  sdgRedCountryList: any[] = [];
  sdgGreenCountryScoreList: any[] = [];
  sdgYellowCountryScoreList: any[] = [];
  sdgOrangeCountryScoreList: any[] = [];
  sdgGrayCountryScoreList: any[] = [];
  sdgRedCountryScoreList: any[] = [];
  sdgCountryListAll: any[] = [];
  grey: any;
  sdgTargetDesc: any
  green: string | number | any[] = [];
  red: string | number | any[] = [];
  organge: string | number | any[] = [];
  gray: any;
  yellow: string | number | any[] = [];
  score: any;
  redMin: number | undefined;
  graphShow: boolean = true;
  redMax: any;
  redMediun!: number; redTotal!: number;
  greenMin!: number; greenMax: any; greenMediun!: number; greenTotal!: number;
  orangeMin!: number; orangeMax: any; orangeMediun!: number; orangeTotal!: number;
  yellowMin!: number; yellowMax: any; yellowMediun!: number; yellowTotal!: number;
  grayMin: any; grayMax: any; grayMediun: any; grayTotal: any
  disabledButtonMap: boolean = false;
  showAlertFlag: boolean = false;
  moreInfoFlag: boolean = false;
  moreInforStm: any
  msgText: any;
  sdgOrigninalName: any;
  changeGraphFlag: boolean = false;
  flagForSort: boolean = false;
  allMaxValue: any[] = [];
  allMinValue = [];
  allMediunValue = [];
  redMinCountry = [];
  redMaxCountry = [];
  greenMinCountry = [];
  greenMaxCountry = [];
  yellowMinCountry = [];
  yellowMaxCountry = [];
  orangeMaxCountry = [];
  orangeMinCountry = [];
  sdgSubGoalValue = [];
  bornSdgInfo: any[] = [];
  windowOne: boolean = false;
  windowTwo: boolean = false;
  windowThree: boolean = false;
  windowFour: boolean = false;
  windowFive: boolean = false;
  divShow: boolean = false;
  graphTitle: any
  sdgBornCountry: any;
  sdgRegisterCountry: any;
  sdgCountryMapList: string | any[] = [];
  valuesR: any[] = [];
  // values:any[]=[];
  //graph
  chartReady: boolean = false;
  bornSdgSubGoalDeatails = [];
  public chartColors: any[] = [];
  firstCopy = false;
  state: any;
  // data
  // data
  public lineChartData: Array<any> = [];
  // labels
  // labels
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any
  public lineChartType: any = 'line';
  public labelMFL: Array<any>
    //multiple line chart
    = [];
  //multiple line chart
  //multiple line chart
  public lineChartColors: am4core.Color[] = [];
  public lineChartLegend = true;
  public lineChartPlugins = [];
  dispalySdgArray = []
  mainPageDispaly: boolean = false;
  class = "new_cmn_modal sdg_modal md-overlay"
  constructor(
    public gameSummary: GameSummeryService,
    public sdgToolService: SdgToolServiceService,
    public constantService: ConstantService,
    public gameSummeryService: GameSummeryService,
    public modalWindowService: modalWindowShowHide,
    public translation: TranslateService,
    public commonService: CommonService,
    public phpToolService: PhpToolService,
    public router: Router,
    public translationService: TranslationService) { }
  @ViewChild('decisionBoxWindow') decisionBoxWindow!: ModalDirective;

  ngOnInit(): void {
    this.mainPageDispaly = true;
    this.state = 'goal';
    this.windowOne = true;
  }


  sdgMapSelect(sdgId: any) {
    this.state = "mapShow"
    this.data = false;
    this.sdgId = sdgId;
    this.translation.get('gameLabel.sdg_map_title', { sdgName: "SDG's goal " + sdgId }).subscribe(
      (str) => {
        this.mapTitle = str;
      })
    let sdg = this.commonService.getSdgNameFromId(sdgId);
    this.sdgToolService.getsdgGoalColorData(sdg).subscribe(
      res => {
        this.countryListSdg = res;
        this.sdgToolService.sdgGoalCountryList = this.countryListSdg;
        this.sdgToolService.sdgId = this.sdgId;
        this.data = true;
        this.translation.get('gameLabel.View_countries_reflecting_SDG_Goal_Score_for_the_Goal', { goalName: sdgId }).subscribe(
          (str) => {
            this.sdgMapTitle = str;

          })
      })
  }
  sdgSelect(sdgId: number) {
    this.state = "subGoalShow"
    this.bornSdgInfo = [];
    this.regSdgInfo = [];
    am4core.disposeAllCharts();
    this.sdgInfo = [];
    this.sdgTargets = [];
    var index = sdgId;
    this.sdgId = index;
    this.sdgToolService.getsdgSubGolaDataWithColourByCountryId(this.phpToolService.selectedBornCountryObject.countryid, true, this.translationService.selectedLang.code).subscribe(
      (res: any) => {
        this.sdgDetails = res['goals'];
        this.bornSdgSubGoalDeatails = res['targets'];
        this.sdgToolService.getsdgSubGolaDataWithColourByCountryId(this.phpToolService.selectedRegisterCountryObject.countryid, true, this.translationService.selectedLang.code).subscribe(
          (res1: any) => {
            this.regSdgSubGoalDeatails = res1['targets'];
            this.sdgInfo = this.bornSdgSubGoalDeatails;
            this.values = Object.values(this.sdgInfo)
            for (let i = 0; i < this.values.length; i++) {
              if (this.values[i].SDG_Id === sdgId) {
                if (this.values[i].score !== 'NA') {
                  this.values[i].score = parseFloat(this.values[i].score);
                  this.values[i].score = this.values[i].score.toFixed(2);
                }
                else {
                  this.values[i].score = this.values[i].score;
                }
                this.bornSdgInfo.push(this.values[i]);
              }
            }
            this.valuesR = Object.values(this.regSdgSubGoalDeatails)
            for (let i = 0; i < this.valuesR.length; i++) {
              if (this.valuesR[i].SDG_Id === sdgId) {
                this.regSdgInfo.push(this.valuesR[i]);
              }
            }
            this.sdgImage = "assets/images/sdgicon/sdg" + sdgId + "_" + this.translationService.selectedLang.code + ".png";
            this.sdgDescription = this.sdgDetails[sdgId - 1].SDG_discription;
            this.sdgName = this.sdgDetails[sdgId - 1].SDG_title;
            this.sdgOrigninalName = this.sdgDetails[sdgId].SDG_name;
          })
      })
  }
  radioButtonValue(event: any, targetId: any, i: any) {
    this.chartReady = false;
    if (event.target.name = 'sdg_selection') {
      if (targetId !== 0) {
        this.sdgTargetCode = targetId;
        this.sdgSubGoalValue = this.sdgTargets[i].id;
        this.sdgTargetDesc = this.sdgTargets[i].value;
        //

        this.sdgToolService.sdgSubGoalId = this.sdgSubGoalValue;
        this.sdgToolService.sdgSubGoalString = this.sdgTargetDesc;
        //
      }

      this.disabledButton = true;
    }
  }

  getRegisterCountrySdgScore(i: any) {
    let v
    v = this.regSdgInfo[i].score;
    return v;
  }
  getRegisterCountrySdgColur(i: any) {
    let v
    v = this.regSdgInfo[i].color;
    return v;
  }
  getNewSdgColor(colour: any) {
    let classvalue = "";
    if (colour === "green") {
      classvalue = "clr_code bg-success d-inline-block mr-2"
    }
    else if (colour === "red") {
      classvalue = "clr_code bg-red d-inline-block mr-2"
    }
    else if (colour === "orange") {
      classvalue = "clr_code bg-oragne d-inline-block mr-2"
    }
    else if (colour === "yellow") {
      classvalue = "clr_code bg-yellow d-inline-block mr-2"
    }
    else if (colour === "grey") {
      classvalue = "clr_code bg-secondary d-inline-block mr-2"
    }
    return classvalue;
  }


  closeAllMap() {
    this.secondWindow = false;
    am4core.disposeAllCharts();
  }
  sdgCountryList() {
    this.windowOne = false;
    this.windowTwo = false;
    this.windowThree = false;
    this.windowFour = true;
    this.windowFive = false;
    this.countryList = []
    this.sdgGreenCountryList = [];
    this.sdgRedCountryList = [];
    this.sdgYellowCountryList = [];
    this.sdgGrayCountryList = [];
    this.sdgOrangeCountryList = [];
    this.sdgToolService.getsdgsubGoalColorData(this.sdgTargetCode).subscribe((res: any) => {
      this.sdgGreenCountryList = res['green'];
      this.sdgRedCountryList = res['red'];
      this.sdgOrangeCountryList = res['orange'];
      this.sdgYellowCountryList = res['yellow'];
      this.sdgGrayCountryList = res['grey'];
      this.disabledButton = false;


    })
  }
  sdgIfram() {
    this.state = "iframeShow"
    if (this.phpToolService.selectedBornCountryObject.countryid === 136) {
      this.sdgBornCountry = this.phpToolService.selectedBornCountryObject.country.split(" ").join('-').toLowerCase();
      this.sdgBornCountry = "russian-federation"
      this.sdgRegisterCountry = this.phpToolService.selectedRegisterCountryObject.country.split(" ").join('-').toLowerCase();
    }
    else if (this.phpToolService.selectedRegisterCountryObject.countryid === 136) {
      this.sdgRegisterCountry = this.phpToolService.selectedRegisterCountryObject.country.split(" ").join('-').toLowerCase();
      this.sdgRegisterCountry = "russian-federation"
      this.sdgBornCountry = this.phpToolService.selectedBornCountryObject.country.split(" ").join('-').toLowerCase();

    }
    else {
      this.sdgBornCountry = this.phpToolService.selectedBornCountryObject.country.split(" ").join('-').toLowerCase();
      this.sdgRegisterCountry = this.phpToolService.selectedRegisterCountryObject.country.split(" ").join('-').toLowerCase();
    }
  }
  close() {
    this.router.navigate(['/countryMap'])
  }

  backClick(state: any) {

    if (state === 'iframeShow') {
      this.state = "subGoalShow";
      this.sdgSelect(this.sdgId)
    }
    else if (state === 'subGoalShow') {
      this.state = 'mapShow'
      this.sdgMapSelect(this.sdgId)
    }
    else if (state === 'subGoalShow') {
      this.state = 'mapShow'
      this.sdgMapSelect(this.sdgId)
    }
    else if (state === 'mapShow') {
      // this.decisionBoxWindow.hide();
      // this.windowOne = false;
      this.state = "goal"
    }
    else if (state === 'goal') {
      this.decisionBoxWindow.hide();
      this.windowOne = false;
      this.state = "";
      this.router.navigate(['/sdgSummary'])
    }
  }

  clickToExitButton() {
    if (this.gameSummary.countrySdgTool) {
      // this.gameSummary.countrySdgTool = false
      this.phpToolService.flowFromSdg = false;
      this.sdgToolService.flowFromWorldSdg = false;
      this.sdgToolService.flowFromSdgStmt = false;
      this.sdgToolService.flowFromCountrySdg = false;
      this.router.navigate(['/sdgSummary']);
    }
    else {
      this.sdgToolService.flowFromWorldSdg = false;
      this.sdgToolService.flowFromSdgStmt = false;
      this.sdgToolService.flowFromCountrySdg = false;
      this.commonService.close();
    }
  }


  playReallives(selectedcountry) {
    if (this.selectedcountry === null) {
      this.showAlertFlag = true;
      this.msgText = this.translation.instant('gameLabel.live_life_warr')
    }
    else {
      this.windowOne = false;
      this.windowTwo = false;
      this.windowThree = false;
      this.windowFour = false;
      this.windowFive = false
      this.modalWindowService.showCharacterDesighWithGropuFlag = true;
      this.modalWindowService.showWaitScreen = true
      this.designALifeObject["country"] = selectedcountry;
      this.designALifeObject["onlyCountry"] = false;
      this.designALifeObject['city'] = null;
      this.designALifeObject["religion"] = null;
      this.designALifeObject["traits"] = null;
      this.designALifeObject['first_name'] = null;
      this.designALifeObject['last_name'] = null;
      this.designALifeObject['nameGroupId'] = null;
      this.designALifeObject['sex'] = null
      this.designALifeObject['urban_rural'] = null;
      this.gameSummeryService.generateCharacterDesignLife(this.designALifeObject, false, false, 0, this.translationService.selectedLang.code, true, 0).subscribe(
        res => {
          this.gameId = res;
          localStorage.removeItem("gameid");
          localStorage.setItem("gameid", this.gameId);
          this.modalWindowService.showWaitScreen = false;
          this.router.navigate(['/createlife']);
        })
    }
  }
  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }

  clickOk() {
    this.showAlertFlag = false
  }
}
