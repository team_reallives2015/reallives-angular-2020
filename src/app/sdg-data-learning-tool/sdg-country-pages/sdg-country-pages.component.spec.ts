import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgCountryPagesComponent } from './sdg-country-pages.component';

describe('SdgCountryPagesComponent', () => {
  let component: SdgCountryPagesComponent;
  let fixture: ComponentFixture<SdgCountryPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgCountryPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgCountryPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
