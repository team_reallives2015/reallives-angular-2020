import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { CommonService } from 'src/app/shared/common.service';
import { SdgToolServiceService } from '../sdg-tool-service.service';

@Component({
  selector: 'app-sdg-summary-page',
  templateUrl: './sdg-summary-page.component.html',
  styleUrls: ['./sdg-summary-page.component.css']
})
export class SdgSummaryPageComponent implements OnInit {
  sdgGoalCountryList: any
  sdgId: any;
  sdgSubGoalId: any;
  sdgSubGoalString: any
  subGoalMapData: any
  flowFromWorldSdg: boolean = false;
  flowFromCountrySdg: boolean = false;
  flowFromSdgStmt: boolean = false;
  sdgQuetionFlag: boolean = false;
  sdgQuetions: any[] = [];
  sdgStmtData: any[] = [];
  sdgSubGoalMapData: any[] = [];
  constructor(public SdgToolService: SdgToolServiceService, public phpToolService: PhpToolService,
    public router: Router, public commonService: CommonService, public gameSummary: GameSummeryService) { }

  ngOnInit(): void {
    this.phpToolService.flowFromSdg = true;
  }
  public sdgCountrySelection() {
    this.SdgToolService.flowFromCountrySdg = true;
    this.SdgToolService.flowFromWorldSdg = false;
    this.SdgToolService.flowFromSdgStmt = false;
    this.router.navigate(['/sdgCountrySelection']);
  }

  sdgHome() {
    this.SdgToolService.flowFromWorldSdg = true;
    this.SdgToolService.flowFromSdgStmt = false;
    this.SdgToolService.flowFromCountrySdg = false;
    this.router.navigate(['/sdgPages'])
  }

  sdgStatement() {
    this.SdgToolService.flowFromWorldSdg = false;
    this.SdgToolService.flowFromSdgStmt = true;
    this.SdgToolService.flowFromCountrySdg = false;
    this.router.navigate(['/sdgCountrySelection'])

  }

  clickToExitButton() {
    if (this.gameSummary.countrySdgTool) {
      this.gameSummary.countrySdgTool = false
      this.phpToolService.flowFromSdg = false;
      this.SdgToolService.flowFromWorldSdg = false;
      this.SdgToolService.flowFromSdgStmt = false;
      this.SdgToolService.flowFromCountrySdg = false;
      this.router.navigate(['/action-page']);
    }
    else {
      this.phpToolService.flowFromSdg = false;
      this.SdgToolService.flowFromWorldSdg = false;
      this.SdgToolService.flowFromSdgStmt = false;
      this.SdgToolService.flowFromCountrySdg = false;
      this.commonService.close();
    }
  }
}
