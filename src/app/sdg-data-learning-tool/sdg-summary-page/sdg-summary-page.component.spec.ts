import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgSummaryPageComponent } from './sdg-summary-page.component';

describe('SdgSummaryPageComponent', () => {
  let component: SdgSummaryPageComponent;
  let fixture: ComponentFixture<SdgSummaryPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgSummaryPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgSummaryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
