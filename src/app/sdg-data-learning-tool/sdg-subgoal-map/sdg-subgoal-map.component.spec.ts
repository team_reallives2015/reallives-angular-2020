import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgSubgoalMapComponent } from './sdg-subgoal-map.component';

describe('SdgSubgoalMapComponent', () => {
  let component: SdgSubgoalMapComponent;
  let fixture: ComponentFixture<SdgSubgoalMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgSubgoalMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgSubgoalMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
