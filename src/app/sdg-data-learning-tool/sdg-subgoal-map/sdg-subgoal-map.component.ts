import { Component, OnInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { SdgToolServiceService } from '../sdg-tool-service.service';
@Component({
  selector: 'app-sdg-subgoal-map',
  templateUrl: './sdg-subgoal-map.component.html',
  styleUrls: ['./sdg-subgoal-map.component.css']
})
export class SdgSubgoalMapComponent implements OnInit {

  sdgCountryMapList: any[] = []
  colourCode: any;
  constructor(public SdgToolService: SdgToolServiceService) { }

  ngOnInit(): void {
    this.sdgCountryMapList = this.SdgToolService.sdgSubGoalMapData
    this.getSdgCountryListMap();
  }


  getSdgCountryListMap() {
    let countryId, country;
    let map = am4core.create("mapDiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    map.projection = new am4maps.projections.Mercator();
    am4core.options.autoDispose = true;
    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";

    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];

    polygonTemplate.events.on("hit", function (ev) {
      if (this.SdgToolService.flowFromSdgSunGoalPlayLife) {
        let country, colorId;
        let selectedObj = ev.target.dataItem.dataContext;
        for (let i = 0; i < this.sdgCountryMapList.length; i++) {
          if (this.sdgCountryMapList[i].code === selectedObj["id"]) {
            countryId = this.sdgCountryMapList[i].countryid;
            country = this.sdgCountryMapList[i].country;
            this.SdgToolService.colorId = selectedObj["value"]
            country = {
              "countryid": countryId,
              "country": country
            }
            this.SdgToolService.selectedCountry = country
            break
          }
        }
        this.SdgToolService.showPlayLifeWindow = true
      } else {
        let cnt = 0;
        // zoom to an object
        ev.target.series.chart.zoomToMapObject(ev.target);
        let object: any;
        object = ev.target.dataItem.dataContext;
        for (let i = 0; i < this.sdgCountryMapList.length; i++) {
          if (this.sdgCountryMapList[i].code === object["id"]) {
            cnt = -1
            if (this.sdgCountryMapList[i].score !== null) {
              countryId = this.sdgCountryMapList[i].countryid;
              country = this.sdgCountryMapList[i].country;
            }
          }

        }
      }
    }, this);
    let county1 = [];
    let value;
    for (let i = 0; i < this.sdgCountryMapList.length; i++) {
      if (this.sdgCountryMapList[i].color === "red") {
        this.colourCode = am4core.color("#d3270d")
        value = 0;
      }
      else if (this.sdgCountryMapList[i].color === "green") {
        this.colourCode = am4core.color("#36AD1F")
        value = 3;

      }
      else if (this.sdgCountryMapList[i].color === "orange") {
        this.colourCode = am4core.color("#FFA019")
        value = 1;

      }
      else if (this.sdgCountryMapList[i].color === "yellow") {
        this.colourCode = am4core.color("#ffff00")
        value = 2;

      }
      else if (this.sdgCountryMapList[i].color === '') {
        this.colourCode = am4core.color("#808080")
        value = 4;
      }
      county1.push({
        "id": this.sdgCountryMapList[i].code,
        "name": this.sdgCountryMapList[i].country,
        "value": value,
        "fill": this.colourCode
      })
    }
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }


}
