import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PhpToolService } from '../data-learning-tool-php/php-tool.service';
import { CommonService } from '../shared/common.service';
import { SdgToolServiceService } from './sdg-tool-service.service';

@Component({
  selector: 'app-sdg-data-learning-tool',
  templateUrl: './sdg-data-learning-tool.component.html',
  styleUrls: ['./sdg-data-learning-tool.component.css']
})
export class SdgDataLearningToolComponent implements OnInit {
  token;
  constructor(public translate: TranslateService,
    public phpToolService: PhpToolService,
    public router: Router,
    public commonService: CommonService,
    public sdgToolService: SdgToolServiceService,
    public activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.phpToolService.flowFromSdg = true;
    this.token = (this.activatedRouter.snapshot.queryParamMap.get('token') || 0);
    // this.token = "63ad1fb893312510221887f1"
    localStorage.setItem('token', this.token);
    this.router.navigate(['/language']);
  }

}
