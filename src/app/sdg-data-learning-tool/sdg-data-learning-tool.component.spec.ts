import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgDataLearningToolComponent } from './sdg-data-learning-tool.component';

describe('SdgDataLearningToolComponent', () => {
  let component: SdgDataLearningToolComponent;
  let fixture: ComponentFixture<SdgDataLearningToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgDataLearningToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgDataLearningToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
