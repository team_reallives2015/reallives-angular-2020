import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { CommonService } from 'src/app/shared/common.service';
import { SdgToolServiceService } from '../sdg-tool-service.service';

@Component({
  selector: 'app-country-selection',
  templateUrl: './country-selection.component.html',
  styleUrls: ['./country-selection.component.css']
})
export class CountrySelectionComponent implements OnInit {

  constructor(public router: Router,
    public commonService: CommonService,
    public gameSummary: GameSummeryService,
    public SdgToolService: SdgToolServiceService,
    public phpToolService: PhpToolService) { }

  ngOnInit(): void {
  }


  clickFromMap() {
    this.router.navigate(['/countryMap'])
  }
  clickFromList() {
    this.router.navigate(['/countryList'])

  }

  backClick() {
    this.router.navigate(['/sdgSummary'])

  }

  clickToExitButton() {
    if (this.gameSummary.countrySdgTool) {
      // this.gameSummary.countrySdgTool = false
      this.phpToolService.flowFromSdg = false;
      this.SdgToolService.flowFromWorldSdg = false;
      this.SdgToolService.flowFromSdgStmt = false;
      this.SdgToolService.flowFromCountrySdg = false;
      this.router.navigate(['/sdgSummary']);
    }
    else {
      this.phpToolService.flowFromSdg = false;
      this.SdgToolService.flowFromWorldSdg = false;
      this.SdgToolService.flowFromSdgStmt = false;
      this.SdgToolService.flowFromCountrySdg = false;
      this.commonService.close();
    }
  }
}
