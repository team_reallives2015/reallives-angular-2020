import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgStatementsComponent } from './sdg-statements.component';

describe('SdgStatementsComponent', () => {
  let component: SdgStatementsComponent;
  let fixture: ComponentFixture<SdgStatementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgStatementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgStatementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
