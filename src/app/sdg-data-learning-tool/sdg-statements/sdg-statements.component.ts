import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { QuestionnaireService } from 'src/app/data-learning-tool/questionnaire/questionnaire.service';
import { CommonService } from 'src/app/shared/common.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { SdgToolServiceService } from '../sdg-tool-service.service';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { Series } from '@amcharts/amcharts4/.internal/charts/series/Series';
@Component({
  selector: 'app-sdg-statements',
  templateUrl: './sdg-statements.component.html',
  styleUrls: ['./sdg-statements.component.css']
})
export class SdgStatementsComponent implements OnInit {
  valueOfSelectedSubGoal = 0;
  data: boolean = false;
  showAlertFlag: boolean = false;
  sdgSubGoalValue;
  sdgTargetDesc = "";
  color;
  selectedCountryName = 'Null';
  gameId;
  selectedcountry = null;
  designALifeObject = {};
  classForCountryOne = ""
  classForCountryTwo = '';
  msgText;
  class: any;
  className: any = "sdg_amenity_modal container-fluid sdg using_sdg";
  keys: any[] = [];
  allSunGoalStmtArray: any[] = [];
  sdgDescription: any
  sdgTitle: any;
  sdgImage: any
  sdgDetails: any;
  bornSdgSubGoalDeatails: any;
  sdgId: any;
  sdgName: any;
  sdgOrigninalName: any
  colourCode: any
  sdgCountryMapList: any
  sdgNameArray: any[] = [];
  redCount: any = 0;
  greenCount: any = 0;
  orangeCount: any = 0;
  yellowCount: any = 0;
  grayCount: any = 0;
  bornCountrySdgColor: any;
  registerCountrySdgColor: any;
  stmt1: any = '';
  stmt2: any = '';
  stmt3: any = '';
  stmt: any = '';
  openColseWindowFlag: boolean = false;
  sdgInfo: any[] = []
  sdgTargets: any[] = []
  regSdgSubGoalDeatails: any
  bornSdgInfo: any[] = [];
  regSdgInfo: any[] = [];
  values: any
  valuesR: any;
  state: any;
  subGoalStm1: any;
  subGoalStm2: any;
  subGoalStm3: any;
  quetionsData: any[] = [];
  sdgGoalStamtArray: any[] = [];
  constructor(public translationService: TranslationService,
    public translate: TranslateService,
    public phpToolService: PhpToolService,
    public sdgToolService: SdgToolServiceService,
    public commonService: CommonService,
    public questionnaireService: QuestionnaireService,
    public router: Router,
    public gameSummeryService: GameSummeryService,
    public modalWindowService: modalWindowShowHide,
    public constantService: ConstantService) { }

  ngOnInit(): void {
    this.openColseWindowFlag = true;
    this.sdgArray();
    this.ClickData();
    this.state = "sdgGoal"
  }

  ClickData() {
    this.sdgCountryMapList = []
    let born = this.phpToolService.selectedBornCountryObject;
    let register = this.phpToolService.selectedRegisterCountryObject;
    this.sdgId = 1;
    this.sdgToolService.getsdgSubGolaDataWithColourByCountryId(born.countryid, true, this.translationService.selectedLang.code).subscribe(
      (res: any) => {
        this.sdgDetails = res['goals'];
        this.bornSdgSubGoalDeatails = res['targets'];
        let sdg = this.commonService.getSdgNameFromId(this.sdgId);
        this.sdgToolService.getsdgGoalColorData(sdg).subscribe(
          res => {
            this.sdgCountryMapList = res;
            this.sdgImage = "assets/images/sdgicon/sdg" + this.sdgId + "_" + this.translationService.selectedLang.code + ".png";
            this.sdgDescription = this.sdgDetails[this.sdgId - 1].SDG_discription;
            this.sdgName = this.sdgDetails[this.sdgId - 1].SDG_title;
            this.sdgOrigninalName = this.sdgDetails[this.sdgId - 1].SDG_name;
            //map
            this.mapFunction();
            this.stmt1 = this.getStatementOneTwo(this.bornCountrySdgColor, this.phpToolService.selectedBornCountryObject.country);
            this.stmt2 = this.getStatementOneTwo(this.registerCountrySdgColor, this.phpToolService.selectedRegisterCountryObject.country)
            this.stmt3 = this.globalStatement(this.greenCount)

          })

      })
  }
  option(target: any) {
    this.sdgCountryMapList = [];
    this.redCount = 0;
    this.greenCount = 0;
    this.orangeCount = 0;
    this.yellowCount = 0;
    this.grayCount = 0;
    am4core.disposeAllCharts();
    this.sdgId = target;
    let id = target;
    let sdg = this.commonService.getSdgNameFromId(id);
    for (let i = 0; i < this.sdgNameArray.length; i++) {
      if (this.sdgNameArray[i].sdgId === target) {
        this.sdgNameArray[i].class = "sdg-img-selected"
      }
      else {
        this.sdgNameArray[i].class = ""
      }
    } this.sdgToolService.getsdgGoalColorData(sdg).subscribe(
      res => {
        this.sdgCountryMapList = res;
        this.sdgImage = "assets/images/sdgicon/sdg" + id + "_" + this.translationService.selectedLang.code + ".png";
        this.sdgDescription = this.sdgDetails[id - 1].SDG_discription;
        this.sdgName = this.sdgDetails[id - 1].SDG_title;
        this.sdgOrigninalName = this.sdgDetails[id - 1].SDG_name;
        //map
        this.mapFunction();
        this.stmt1 = this.getStatementOneTwo(this.bornCountrySdgColor, this.phpToolService.selectedBornCountryObject.country);
        this.stmt2 = this.getStatementOneTwo(this.registerCountrySdgColor, this.phpToolService.selectedRegisterCountryObject.country)
        this.stmt3 = this.globalStatement(this.greenCount)
      })
  }
  getStatementOneTwo(color: any, country: any) {
    this.stmt = ''
    let keys: any[] = [];
    let red = this.translate.instant('gameLabel.major_challenges');
    let green = this.translate.instant('gameLabel.goal_achivments');
    let orange = this.translate.instant('gameLabel.significant_challenges');
    let yellow = this.translate.instant('gameLabel.challenges_remain');
    let gray = this.translate.instant('gameLabel.No_data');

    if (color === 'red') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: this.redCount, color: color, name: red, type: this.translate.instant('gameLabel.Goal'), sdgName: this.sdgName }).subscribe(
        (str) => {
          this.stmt = str;
          keys.push({
            country: country,
            color: color,
            count: this.redCount,
          })
          this.sdgGoalStamtArray.push({
            key: "sdg_born_country_stmt",
            value: keys,
            goalName: this.sdgName,
            type: this.translate.instant('gameLabel.Goal')

          })
        })
    }
    else if (color === 'green') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: this.greenCount, color: color, name: green, type: this.translate.instant('gameLabel.Goal'), sdgName: this.sdgName }).subscribe(
        (str) => {
          this.stmt = str;
          keys.push({
            country: country,
            color: color,
            count: this.greenCount,
          })
          this.sdgGoalStamtArray.push({
            key: "sdg_born_country_stmt",
            value: keys,
            goalName: this.sdgName,
            type: this.translate.instant('gameLabel.Goal')

          })
        })

    }
    else if (color === 'orange') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: this.orangeCount, color: color, name: orange, type: this.translate.instant('gameLabel.Goal'), sdgName: this.sdgName }).subscribe(
        (str) => {
          this.stmt = str;
          keys.push({
            country: country,
            color: color,
            count: this.orangeCount,
          })
          this.sdgGoalStamtArray.push({
            key: "sdg_born_country_stmt",
            value: keys,
            goalName: this.sdgName,
            type: this.translate.instant('gameLabel.Goal')

          })
        })

    }
    else if (color === 'yellow') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: this.yellowCount, color: color, name: yellow, type: this.translate.instant('gameLabel.Goal'), sdgName: this.sdgName }).subscribe(
        (str) => {
          this.stmt = str;
          keys.push({
            country: country,
            color: color,
            count: this.yellowCount,
          })
          this.sdgGoalStamtArray.push({
            key: "sdg_born_country_stmt",
            value: keys,
            goalName: this.sdgName,
            type: this.translate.instant('gameLabel.Goal')
          })
        })
    }
    else if (color === 'grey' || color === '' || color === undefined) {
      this.stmt = this.translate.instant('gameLabel.No_data');
      keys.push({
        country: country,
        color: color,
        count: this.grayCount,
      })
      this.sdgGoalStamtArray.push({
        key: "sdg_born_country_stmt",
        value: keys,
        goalName: this.sdgName,
        type: this.translate.instant('gameLabel.Goal')
      })
    }
    return this.stmt
  }
  globalStatement(greenCount: any) {
    this.stmt = ''
    let keys: any[] = []
    this.translate.get('gameLabel.sdgStmt6', { greenCount: this.greenCount, type: this.translate.instant('gameLabel.Goal'), sdgName: this.sdgName }).subscribe(
      (str) => {
        this.stmt = str;
        keys.push({
          count: this.greenCount,
        })
        this.sdgGoalStamtArray.push({
          key: "sdg_global_Stmt",
          value: keys,
          goalName: this.sdgName,
          type: this.translate.instant('gameLabel.Goal')
        })
      })
    return this.stmt

  }
  sdgArray() {
    this.sdgNameArray = [
      {
        "sdgId": 1,
        "sdgName": "No Poverty",
        "image": "assets/images/sdgicon/sdg1_" + this.translationService.selectedLang.code + ".png",
        "class": "sdg-img-selected"
      },
      {
        "sdgId": 2,
        "sdgName": "Zero Hunger",
        "image": "assets/images/sdgicon/sdg2_" + this.translationService.selectedLang.code + ".png",
        "class": ""
      },
      {
        "sdgId": 3,
        "sdgName": "Good Health and Well-being",
        "image": "assets/images/sdgicon/sdg3_" + this.translationService.selectedLang.code + ".png",
        "class": ""
      },
      {
        "sdgId": 4,
        "sdgName": "Quality Equality",
        "image": "assets/images/sdgicon/sdg4_" + this.translationService.selectedLang.code + ".png",
        "class": ""
      },
      {
        "sdgId": 5,
        "sdgName": "Gender Equality",
        "image": "assets/images/sdgicon/sdg5_" + this.translationService.selectedLang.code + ".png",
        "class": ""
      },
      {
        "sdgId": 6,
        "sdgName": "Clean Water and Sanitation",
        "image": "assets/images/sdgicon/sdg6_" + this.translationService.selectedLang.code + ".png",
        "class": ""
      },
      {
        "sdgId": 7,
        "sdgName": "Affordable and Clean Energy",
        "image": "assets/images/sdgicon/sdg7_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 8,
        "sdgName": "Decent Work and Economic Growth",
        "image": "assets/images/sdgicon/sdg8_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 9,
        "sdgName": " Industry, Innovation and Infrastructure",
        "image": "assets/images/sdgicon/sdg9_" + this.translationService.selectedLang.code + ".png"

      },
      {
        "sdgId": 10,
        "sdgName": "Reduce Inequalities",
        "image": "assets/images/sdgicon/sdg10_" + this.translationService.selectedLang.code + ".png",
        "class": ""
      },
      {
        "sdgId": 11,
        "sdgName": "Sustainable Cities and Communities",
        "image": "assets/images/sdgicon/sdg11_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 12,
        "sdgName": "Responsible Consumption and Production",
        "image": "assets/images/sdgicon/sdg12_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 13,
        "sdgName": "Climate Action",
        "image": "assets/images/sdgicon/sdg13_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 14,
        "sdgName": " Life Below Water",
        "image": "assets/images/sdgicon/sdg14_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 15,
        "sdgName": " Life On Land",
        "image": "assets/images/sdgicon/sdg15_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 16,
        "sdgName": "Peace, Justice and Strong Institutions",
        "image": "assets/images/sdgicon/sdg16_" + this.translationService.selectedLang.code + ".png",
        "class": ""

      },
      {
        "sdgId": 17,
        "sdgName": "Partnerships for the Goals",
        "image": "assets/images/sdgicon/sdg17_" + this.translationService.selectedLang.code + ".png",
        "class": ""
      }
    ]
  }

  sdgSelect(sdgId: any) {
    this.state = "sdgsubGoal"
    this.className = ""
    this.sdgInfo = [];
    this.sdgTargets = [];
    this.bornSdgInfo = [];
    this.regSdgInfo = [];
    var index = sdgId;
    this.sdgId = index;
    this.sdgToolService.getsdgSubGolaDataWithColourByCountryId(this.phpToolService.selectedBornCountryObject.countryid, true, this.translationService.selectedLang.code).subscribe(
      (res: any) => {
        this.sdgDetails = res['goals'];
        this.bornSdgSubGoalDeatails = res['targets'];
        this.sdgToolService.getsdgSubGolaDataWithColourByCountryId(this.phpToolService.selectedRegisterCountryObject.countryid, true, this.translationService.selectedLang.code).subscribe(
          (res1: any) => {
            this.regSdgSubGoalDeatails = res1['targets'];
            this.sdgInfo = this.bornSdgSubGoalDeatails;
            this.keys = am4core.object.keys(res1['targets']);
            this.values = Object.values(this.sdgInfo)
            for (let i = 0; i < this.values.length; i++) {
              if (this.values[i].SDG_Id === sdgId) {
                if (this.values[i].score !== 'NA') {
                  this.values[i].score = parseFloat(this.values[i].score);
                  this.values[i].score = this.values[i].score.toFixed(2);
                }
                else {
                  this.values[i].score = this.values[i].score;
                }
                this.values[i].id = this.keys[i]
                this.bornSdgInfo.push(this.values[i]);
              }
            }
            this.bornSdgInfo[0].SDG_Id = "highlight"
            this.sdgTargetDesc = this.bornSdgInfo[0].target_desc
            this.sdgSubGoalValue = this.bornSdgInfo[0].SDG_target
            this.valueOfSelectedSubGoal = 0;
            this.valuesR = Object.values(this.regSdgSubGoalDeatails)
            for (let i = 0; i < this.valuesR.length; i++) {
              if (this.valuesR[i].SDG_Id === sdgId) {
                this.valuesR[i].id = this.keys[i]
                this.regSdgInfo.push(this.valuesR[i]);
              }
            }
            this.sdgImage = "assets/images/sdgicon/sdg" + sdgId + "_" + this.translationService.selectedLang.code + ".png";
            this.sdgName = this.sdgDetails[sdgId - 1].SDG_title;
            this.sdgOrigninalName = this.sdgDetails[sdgId - 1].SDG_name;
            this.getSubGoalStmt(sdgId);
          })
      })
  }
  getRegisterCountrySdgScore(i: any) {
    let v
    v = this.regSdgInfo[i].score;
    return v;
  }
  getRegisterCountrySdgColur(i: any) {
    let v
    v = this.regSdgInfo[i].color;
    return v;
  }
  getNewSdgColor(colour: any) {
    let classvalue = "";
    if (colour === "green") {
      classvalue = "clr_code bg-success d-inline-block mr-2"
    }
    else if (colour === "red") {
      classvalue = "clr_code bg-red d-inline-block mr-2"
    }
    else if (colour === "orange") {
      classvalue = "clr_code bg-oragne d-inline-block mr-2"
    }
    else if (colour === "yellow") {
      classvalue = "clr_code bg-yellow d-inline-block mr-2"
    }
    else if (colour === "grey") {
      classvalue = "clr_code bg-secondary d-inline-block mr-2"
    }
    return classvalue;
  }
  backClick(state: any) {
    this.valueOfSelectedSubGoal = 0;
    //  this.sdgArray();
    if (state === 'sdgGoal') {
      this.router.navigate(['/sdgCountrySelection'])
    }
    else if (state === 'sdgsubGoal') {
      this.state = 'sdgGoal'
      this.className = "sdg_amenity_modal container-fluid sdg using_sdg"
      this.ClickData();
    }
    else if (state === 'reallives') {
      this.state = 'sdgsubGoal'
      this.className = "sdg_amenity_modal container-fluid sdg using_sdg"
      this.sdgSelect(1);
    }
    else {
      this.state = "sdgGoal"
      this.className = "sdg_amenity_modal container-fluid sdg using_sdg"
      this.ClickData();
    }
  }
  close() {

    am4core.disposeAllCharts();
    this.openColseWindowFlag = false
  }
  getSubGoalStmt(sdgId: any) {
    let id = "sdg" + sdgId;
    this.allSunGoalStmtArray = [];
    let subGoal: any[] = [];
    let subGoalBColour: any[] = []
    let data: any[] = []
    let subGoalRColour: any[] = []
    let valuesArray: any[] = [];
    let bKeys: any[] = []
    let rKeys: any[] = []
    let gKeys: any[] = []
    let subGoalName: any[] = []
    this.sdgToolService.geSdgSubGolaDataFOrStmt(id, this.phpToolService.selectedBornCountryObject.countryid, this.phpToolService.selectedRegisterCountryObject.countryid).subscribe(
      res => {
        data = res
        for (let i = 0; i < this.keys.length; i++) {
          if (this.keys[i].includes(id + "_")) {
            subGoal.push(this.keys[i]);
            subGoalName.push(this.values[i]);
          }
        }
        for (let i = 0; i < this.sdgNameArray.length; i++) {
          if (this.sdgNameArray[i].sdgId === sdgId) {
            this.sdgNameArray[i].class = "sdg-img-selected"
          }
          else {
            this.sdgNameArray[i].class = ""
          }
        }
        for (let j = 0; j < subGoal.length; j++) {
          rKeys = [];
          bKeys = [];
          gKeys = [];
          let stametName = subGoal[j];
          let subgoalid = subGoal[j]
          if (data.hasOwnProperty(subgoalid)) {
            let subGoalObject = data[subgoalid]
            subGoalBColour = data[this.phpToolService.selectedBornCountryObject.countryid]
            let bColor = subGoalBColour[subgoalid]
            let bcount = subGoalObject[bColor]
            subGoalRColour = data[this.phpToolService.selectedRegisterCountryObject.countryid]
            let rColor = subGoalRColour[subgoalid]
            let rcount = subGoalObject[rColor]
            let stm1 = this.getStatementForSubGoalOneTwo(bColor, this.phpToolService.selectedBornCountryObject.country, bcount, subGoalName[j].target_desc)
            let stm2 = this.getStatementForSubGoalOneTwo(rColor, this.phpToolService.selectedRegisterCountryObject.country, rcount, subGoalName[j].target_desc)
            let stm3 = this.globalForSubGoalStatement(subGoalObject['green'], subGoalName[j].target_desc)
            this.allSunGoalStmtArray.push(
              {
                key: stametName,
                value: stm1
              }, {
              key: stametName,
              value: stm2
            },
              {
                key: stametName,
                value: stm3
              })
            bKeys.push({
              country: this.phpToolService.selectedBornCountryObject.country,
              color: bColor,
              count: bcount,
            })
            rKeys.push({
              country: this.phpToolService.selectedRegisterCountryObject.country,
              color: rColor,
              count: rcount
            })
            gKeys.push({
              count: subGoalObject['green']
            })
            valuesArray.push(
              {
                key: "sdg_born_country_stmt",
                value: bKeys,
                goalName: subGoalName[j].target_desc,
                type: this.translate.instant('gameLabel.Subgoal')
              },
              {
                key: "sdg_register_country_stmt",
                value: rKeys,
                goalName: subGoalName[j].target_desc,
                type: this.translate.instant('gameLabel.Subgoal')
              },
              {
                key: "sdg_globel_stmt",
                value: gKeys,
                goalName: subGoalName[j].target_desc,
                type: this.translate.instant('gameLabel.Subgoal')
              }
            )

          }
        }

        this.subGoalStm1 = this.allSunGoalStmtArray[0].value
        this.subGoalStm2 = this.allSunGoalStmtArray[1].value
        this.subGoalStm3 = this.allSunGoalStmtArray[2].value
        for (let g = 0; g < this.sdgGoalStamtArray.length; g++) {
          valuesArray.push(this.sdgGoalStamtArray[g]);
        }
        this.sdgToolService.sdgStmtData = valuesArray;
        this.sdgToolService.sendDataAndGetQue(valuesArray).subscribe(
          res => {
            this.quetionsData = res;
            this.questionnaireService.quetionMeatadata = this.quetionsData
            this.sdgToolService.sdgQuetions = this.quetionsData;
          })
      })
  }


  getStatementForSubGoalOneTwo(color: any, country: any, count: any, sdgName: any) {
    this.stmt = '';
    let red = this.translate.instant('gameLabel.major_challenges');
    let green = this.translate.instant('gameLabel.goal_achivments');
    let orange = this.translate.instant('gameLabel.significant_challenges');
    let yellow = this.translate.instant('gameLabel.challenges_remain');
    let gray = this.translate.instant('gameLabel.No_data');

    if (color === 'red') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: count, color: color, name: red, type: this.translate.instant('gameLabel.Subgoal'), sdgName: sdgName }).subscribe(
        (str) => {
          this.stmt = str;
        })
    }
    else if (color === 'green') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: count, color: color, name: green, type: this.translate.instant('gameLabel.Subgoal'), sdgName: sdgName }).subscribe(
        (str) => {
          this.stmt = str;
        })

    }
    else if (color === 'orange') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: count, color: color, name: orange, type: this.translate.instant('gameLabel.Subgoal'), sdgName: sdgName }).subscribe(
        (str) => {
          this.stmt = str;
        })

    }
    else if (color === 'yellow') {
      this.translate.get('gameLabel.sdgStmt1', { country: country, count: count, color: color, name: yellow, type: this.translate.instant('gameLabel.Subgoal'), sdgName: sdgName }).subscribe(
        (str) => {
          this.stmt = str;
        })
    }
    else if (color === '' || color === '' || color === undefined) {
      this.stmt = this.translate.instant('gameLabel.No_data');
      // this.translate.get('gameLabel.sdgStmt1', { country: country, count: this.grayCount, color: color, name: gray, type: "subgoal", sdgName: sdgName }).subscribe(
      //   (str) => {
      //     this.stmt = str;
      //   })
    }
    return this.stmt
  }
  globalForSubGoalStatement(count: any, sdgName: any) {
    this.stmt = '';
    this.translate.get('gameLabel.sdgStmt6', { greenCount: count, type: this.translate.instant('gameLabel.Subgoal'), sdgName: sdgName }).subscribe(
      (str) => {
        this.stmt = str;
      })
    return this.stmt

  }

  radioButtonValue(value: any, i) {
    this.valueOfSelectedSubGoal = i;
    for (let k = 0; k < this.bornSdgInfo.length; k++) {
      if (this.bornSdgInfo[k].target_code === value) {
        this.sdgSubGoalValue = this.bornSdgInfo[k].SDG_target;
        this.sdgTargetDesc = this.bornSdgInfo[k].target_desc;
        this.bornSdgInfo[k].SDG_Id = "highlight"
      }
      else {
        this.bornSdgInfo[k].SDG_Id = ""
      }
    }
    let values: any[] = [];
    for (let i = 0; i < this.allSunGoalStmtArray.length; i++) {
      if (this.allSunGoalStmtArray[i].key === value) {
        values.push(this.allSunGoalStmtArray[i].value);
      }
    }
    this.subGoalStm1 = values[0]
    this.subGoalStm2 = values[1]
    this.subGoalStm3 = values[2]

  }


  setQuetion() {
    if (this.translationService.checkGameBuyOrNot) {
      this.msgText = this.translate.instant('gameLabel.live_life')
    }
    else {
      this.msgText = this.translate.instant('gameLabel.buy')

    }
    this.state = "reallives"
    // this.sdgToolService.sdgQuetionFlag = true;
  }


  goBack() {
    this.state = 'sdgsubGoal'
    this.sdgToolService.sdgQuetionFlag = false;
  }

  clickToExitButton() {
    if (this.gameSummeryService.countrySdgTool) {
      // this.gameSummeryService.countrySdgTool = false
      this.phpToolService.flowFromSdg = false;
      this.sdgToolService.flowFromWorldSdg = false;
      this.sdgToolService.flowFromSdgStmt = false;
      this.sdgToolService.flowFromCountrySdg = false;
      this.router.navigate(['/sdgSummary']);
    }
    else {
      this.phpToolService.flowFromSdg = false;
      this.sdgToolService.flowFromWorldSdg = false;
      this.sdgToolService.flowFromSdgStmt = false;
      this.sdgToolService.flowFromCountrySdg = false;
      this.commonService.close();
    }
  }

  countrySelectionToPlayLifeFirst() {
    this.classForCountryOne = "country_map_log mx-auto countrySelectionClass"
    this.classForCountryTwo = "country_map_log mx-auto"
    this.selectedcountry = this.phpToolService.selectedBornCountryObject
    this.selectedCountryName = this.phpToolService.selectedBornCountryObject.country

  }


  countrySelectionToPlayLifeSecond() {
    this.classForCountryOne = "country_map_log mx-auto"
    this.classForCountryTwo = " country_map_log mx-auto countrySelectionClass"
    this.selectedcountry = this.phpToolService.selectedRegisterCountryObject
    this.selectedCountryName = this.phpToolService.selectedRegisterCountryObject.country

  }

  playReallives() {
    if (this.selectedcountry === null) {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.live_life_warr')
    }
    else {
      if (this.selectedcountry.countryid === this.phpToolService.selectedBornCountryObject.countryid) {
        // console.log(this.bornSdgInfo[this.valueOfSelectedSubGoal]);
        let color = this.bornSdgInfo[this.valueOfSelectedSubGoal].color;
        this.color = this.getColorCode(color)
      }
      else if (this.selectedcountry.countryid === this.phpToolService.selectedRegisterCountryObject.countryid) {
        let color = this.getRegisterCountrySdgColur(this.valueOfSelectedSubGoal);
        this.color = this.getColorCode(color)
      }
      this.valueOfSelectedSubGoal = 0;
      this.phpToolService.flowFromSdg = false;
      this.sdgToolService.flowFromWorldSdg = false;
      this.sdgToolService.flowFromSdgStmt = false;
      this.sdgToolService.flowFromCountrySdg = false;
      this.modalWindowService.showCharacterDesighWithGropuFlag = true;
      this.modalWindowService.showWaitScreen = true
      // this.designALifeObject["country"] = this.selectedcountry;
      // this.designALifeObject["onlyCountry"] = false;
      // this.designALifeObject['city'] = null;
      // this.designALifeObject["religion"] = null;
      // this.designALifeObject["traits"] = null;
      // this.designALifeObject['first_name'] = null;
      // this.designALifeObject['last_name'] = null;
      // this.designALifeObject['nameGroupId'] = null;
      // this.designALifeObject['sex'] = null
      // this.designALifeObject['urban_rural'] = null;
      let sdg;
      sdg = {
        "sdgId": this.sdgId,
        "goalString": this.sdgTargetDesc,
        "challengeId": this.color,
        "subGoal": this.sdgSubGoalValue
      }
      //
      let country;
      country = {
        "countryid": this.selectedcountry.countryid
      }
      this.designALifeObject["sdg"] = sdg;
      this.designALifeObject["country"] = country;
      this.sdgToolService.createLifeWithSdg(this.translationService.selectedLang.code, this.designALifeObject).subscribe(
        res => {
          this.gameId = res;
          localStorage.removeItem("gameid");
          localStorage.setItem("gameid", this.gameId);
          this.modalWindowService.showWaitScreen = false;
          this.router.navigate(['/createlife']);
        })
    }
  }

  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }

  clickOk() {
    this.showAlertFlag = false;
  }


  getColorCode(color) {
    if (color === 'red') {
      return 0
    }
    else if (color === 'orange') {
      return 1

    }
    else if (color === 'yellow') {
      return 2

    } else if (color === 'green') {
      return 3


    } else if (color === 'grey') {
      return 4


    }
  }

  mapFunction() {
    let map = am4core.create("mapDiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    map.projection = new am4maps.projections.Mercator();
    am4core.options.autoDispose = true;
    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";

    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    let county1 = [];
    let value;
    for (let i = 0; i < this.sdgCountryMapList.length; i++) {
      if (this.sdgCountryMapList[i].color === "red") {
        this.redCount = this.redCount + 1;
        this.colourCode = am4core.color("#d3270d")
        value = 0;
      }
      else if (this.sdgCountryMapList[i].color === "green") {
        this.greenCount = this.greenCount + 1;
        this.colourCode = am4core.color("#36AD1F")
        value = 3;

      }
      else if (this.sdgCountryMapList[i].color === "orange") {
        this.orangeCount = this.orangeCount + 1;
        this.colourCode = am4core.color("#FFA019")
        value = 1;

      }
      else if (this.sdgCountryMapList[i].color === "yellow") {
        this.yellowCount = this.yellowCount + 1;
        this.colourCode = am4core.color("#ffff00")
        value = 2;

      }
      else if (this.sdgCountryMapList[i].color === 'grey') {
        this.grayCount = this.grayCount + 1;
        this.colourCode = am4core.color("#808080")
        value = 4;
      }
      if (this.phpToolService.selectedBornCountryObject.countryid === this.sdgCountryMapList[i].countryid) {
        this.bornCountrySdgColor = this.sdgCountryMapList[i].color
        county1.push({
          "id": this.sdgCountryMapList[i].code,
          "name": this.sdgCountryMapList[i].countryname,
          "value": value,
          "fill": this.colourCode
        })
      }
      else if (this.phpToolService.selectedRegisterCountryObject.countryid === this.sdgCountryMapList[i].countryid) {
        this.registerCountrySdgColor = this.sdgCountryMapList[i].color
        county1.push({
          "id": this.sdgCountryMapList[i].code,
          "name": this.sdgCountryMapList[i].countryname,
          "value": value,
          "fill": this.colourCode
        })
      }

    }
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";

  }
}
