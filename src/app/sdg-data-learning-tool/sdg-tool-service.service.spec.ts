import { TestBed } from '@angular/core/testing';

import { SdgToolServiceService } from './sdg-tool-service.service';

describe('SdgToolServiceService', () => {
  let service: SdgToolServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SdgToolServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
