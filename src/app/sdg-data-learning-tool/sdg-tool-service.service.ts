import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from '../shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class SdgToolServiceService {
  flagForSdgTool: boolean = false;
  flowFromSdgSunGoalPlayLife: boolean = false
  colorId;
  sdgGoalCountryList: any
  sdgId: any;
  sdgSubGoalId: any;
  sdgSubGoalString: any
  subGoalMapData: any
  flowFromWorldSdg: boolean = false;
  flowFromCountrySdg: boolean = false;
  flowFromSdgStmt: boolean = false;
  sdgQuetionFlag: boolean = false;
  showPlayLifeWindow: boolean = false;
  selectedCountry;
  sdgQuetions: any[] = [];
  sdgStmtData: any[] = [];
  sdgSubGoalMapData: any[] = [];
  constructor(public http: HttpClient,
    public commonService: CommonService) { }

  getsdgGoalColorData(subgoal: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tools/actionbar/DesignALifeSDG/allSDGDataColor/${subgoal}`);

  }


  getsdgGoalSubGoalMetaData(langCode: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tools/actionbar/DesignALifeSDG/getSDGMetaData/${langCode}`);

  }

  getsdgsubGoalColorData(sdgTargetCode: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tools/actionbar/DesignALifeSDG/SDGDetailsByCountry/${sdgTargetCode}`);

  }

  getSubGoalGraphData(sdgTargetId: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tools/actionbar/DesignALifeSDG/SDGDetailsMap/${sdgTargetId}`);
  }


  getSubGoalCountryList(sdgTargetCode: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tools/actionbar/DesignALifeSDG/SDGDetailsByCountry/${sdgTargetCode}`);
  }

  getsdgSubGolaDataWithColourByCountryId(countryId: any, registeredFlag: any, langCode: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tools/SDGDetails/${countryId}/${registeredFlag}/${langCode}`);

  }

  geSdgSubGolaDataFOrStmt(sdgGoal: any, countryid1: any, countryid2: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tools/actionbar/getSDGDetails/${sdgGoal}/${countryid1}/${countryid2}`);

  }


  sendDataAndGetQue(questions: any) {
    return this.http.post<any>(`${this.commonService.url}tools/sdgData/sdgQuiz `, { questions });

  }


  createLifeWithSdg(langCode, designALifeObject) {
    return this.http.post<any>(`${this.commonService.url}game/actionbar/SDGTool/designANewLife/${langCode} `, { designALifeObject });



  }
}
