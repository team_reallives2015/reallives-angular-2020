import { Component, OnInit, ViewChild } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { TranslationService } from 'src/app/translation/translation.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { SdgToolServiceService } from '../sdg-tool-service.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-sdg-pages',
  templateUrl: './sdg-pages.component.html',
  styleUrls: ['./sdg-pages.component.css']
})
export class SdgPagesComponent implements OnInit {
  selectedCountryName = 'Null';
  gameId;
  sdgImageLast;
  selectedcountry = [];
  designALifeObject = {};
  classForCountryOne = ""
  classForCountryTwo = '';
  plyLifeFlag: boolean = false;
  closHome: boolean = false;
  data: boolean = false
  colourCode: am4core.Color | undefined;
  sdgCountryListAllLast: any[] = [];
  modelClass = " modal fade new_cmn_modal md-overlay";
  sdgId: any
  countryList: any
  mapTitle: any;
  countryListSdg: any
  sdgMapTitle: any;
  firstWindow: boolean = false;
  secondWindow: boolean = false;
  sdgInfo: any;
  sdgTargets: any;
  sdgDetails: any;
  sdgImage: any
  sdgName: any
  sdgTitle: any
  sdgDescription: any
  sdgTargetCode: any;
  values: any
  disabledButton: boolean = false;
  sdgGreenCountryList: any[] = [];
  sdgYellowCountryList: any[] = [];
  sdgOrangeCountryList: any[] = [];
  sdgGrayCountryList: any[] = [];
  sdgRedCountryList: any[] = [];
  sdgGreenCountryScoreList: any[] = [];
  sdgYellowCountryScoreList: any[] = [];
  sdgOrangeCountryScoreList: any[] = [];
  sdgGrayCountryScoreList: any[] = [];
  sdgRedCountryScoreList: any[] = [];
  sdgCountryListAll: any[] = [];
  grey: any;
  sdgTargetDesc: any
  green: string | number | any[] = [];
  red: string | number | any[] = [];
  organge: string | number | any[] = [];
  gray: any;
  yellow: string | number | any[] = [];
  score: any;
  redMin: number | undefined;
  graphShow: boolean = true;
  redMax: any;
  redMediun!: number; redTotal!: number;
  greenMin!: number; greenMax: any; greenMediun!: number; greenTotal!: number;
  orangeMin!: number; orangeMax: any; orangeMediun!: number; orangeTotal!: number;
  yellowMin!: number; yellowMax: any; yellowMediun!: number; yellowTotal!: number;
  grayMin: any; grayMax: any; grayMediun: any; grayTotal: any
  disabledButtonMap: boolean = false;
  showAlertFlag: boolean = false;
  moreInfoFlag: boolean = false;
  moreInforStm: any
  msgText: any;
  changeGraphFlag: boolean = false;
  flagForSort: boolean = false;
  allMaxValue: any[] = [];
  allMinValue = [];
  allMediunValue = [];
  redMinCountry = [];
  redMaxCountry = [];
  greenMinCountry = [];
  greenMaxCountry = [];
  yellowMinCountry = [];
  yellowMaxCountry = [];
  orangeMaxCountry = [];
  orangeMinCountry = [];
  sdgSubGoalValue = [];
  windowOne: boolean = false;
  windowTwo: boolean = false;
  windowThree: boolean = false;
  windowFour: boolean = false;
  windowFive: boolean = false;
  divShow: boolean = false;
  graphTitle: any
  sdgCountryMapList: string | any[] = [];
  //graph
  chartReady: boolean = false;
  public chartColors: any[] = [];
  firstCopy = false;
  // data
  // data
  public lineChartData: Array<any> = [];
  // labels
  // labels
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any
  public lineChartType: any = 'line';
  public labelMFL: Array<any>
    //multiple line chart
    = [];
  //multiple line chart
  //multiple line chart
  public lineChartColors: am4core.Color[] = [];
  public lineChartLegend = true;
  public lineChartPlugins = [];
  dispalySdgArray = []
  state = 'goal';
  goalName = '';
  constructor(public translationService: TranslationService,
    public phpToolService: PhpToolService,
    public translation: TranslateService,
    public SdgToolService: SdgToolServiceService,
    public commonService: CommonService,
    public modalWindowService: modalWindowShowHide,
    public constantService: ConstantService,
    public translate: TranslateService,
    public router: Router,
    public gameSummeryService: GameSummeryService) { }
  @ViewChild('decisionForSimpleYesNo') decisionForSimpleYesNo: ModalDirective;

  ngOnInit(): void {
    this.closHome = true;
    this.windowOne = true;
    this.SdgToolService.flowFromSdgSunGoalPlayLife = true;
  }
  sdgMapSelect(sdgId: any) {
    this.state = "goalMap"
    this.chartReady = false;
    this.data = false;
    this.sdgId = sdgId;
    this.translation.get('gameLabel.sdg_map_title', { sdgName: "SDG's goal " + sdgId }).subscribe(
      (str) => {
        this.mapTitle = str;
      })
    let sdg = this.commonService.getSdgNameFromId(sdgId);
    this.SdgToolService.getsdgGoalColorData(sdg).subscribe(
      res => {
        this.countryListSdg = res;
        this.SdgToolService.sdgGoalCountryList = this.countryListSdg;
        this.SdgToolService.sdgId = this.sdgId;
        this.data = true;
        this.translation.get('gameLabel.View_countries_reflecting_SDG_Goal_Score_for_the_Goal', { goalName: sdgId }).subscribe(
          (str) => {
            this.sdgMapTitle = str;

          })
      })
  }

  sdgSelect(sdgId: number) {
    this.state = "subGoalList"
    am4core.disposeAllCharts();
    this.sdgInfo = [];
    this.sdgTargets = [];
    var index = sdgId;
    this.sdgId = index;
    let keys: string | any[];
    let values: any[];
    this.SdgToolService.getsdgGoalSubGoalMetaData(this.translationService.selectedLang.code).subscribe(res => {
      this.sdgDetails = res;
      this.values = Object.values(this.sdgDetails)
      for (let i = 0; i < this.values.length; i++) {
        if (this.values[i].SDG_Id === this.sdgId) {
          this.sdgInfo.push(this.values[i]);
          keys = Object.keys(this.values[i].targets);
          values = Object.values(this.values[i].targets)
        }
      }
      for (let j = 0; j < keys.length; j++) {
        let cnt = j + 1
        this.sdgTargets.push({
          key: keys[j],
          value: values[j],
          id: sdgId + "." + cnt
        });
      }
      this.sdgImage = "assets/images/sdgicon/sdg" + sdgId + "_" + this.translationService.selectedLang.code + ".png"
      this.sdgDescription = this.sdgInfo[0].SDG_discription
      this.sdgTitle = this.sdgInfo[0].SDG_title
      this.sdgName = this.sdgInfo[0].SDG_name

    })


  }

  sdgGraphAndTableData() {
    this.state = "subGoalGraph"
    this.translation.get('gameLabel.sdg_graph_title', { subgoalName: this.sdgSubGoalValue + " " + this.sdgTargetDesc }).subscribe(
      (str) => {
        this.graphTitle = str;
      })
    this.sdgRedCountryList = [];
    this.sdgGreenCountryList = [];
    this.sdgYellowCountryList = [];
    this.sdgOrangeCountryList = [];
    this.sdgGreenCountryScoreList = [];
    this.sdgOrangeCountryScoreList = [];
    this.sdgYellowCountryScoreList = [];
    this.sdgRedCountryScoreList = [];
    this.green = 0, this.greenMax = 0; this.greenMin = 0; this.greenMediun = 0;
    this.red = 0, this.redMax = 0; this.redMin = 0; this.redMediun = 0
    this.organge = 0, this.orangeMax = 0; this.orangeMin = 0; this.orangeMediun = 0
    this.yellow = 0, this.yellowMax = 0; this.yellowMin = 0; this.yellowMediun = 0
    this.SdgToolService.getSubGoalGraphData(this.sdgTargetCode).subscribe((res: any) => {
      this.sdgCountryMapList = res;
      this.SdgToolService.sdgSubGoalMapData = res;
      this.dotPlotGraph();
      for (let i = 0; i < this.sdgCountryMapList.length; i++) {
        if (this.sdgCountryMapList[i].color === "red") {
          this.sdgRedCountryList.push(this.sdgCountryMapList[i])
        }

        else if (this.sdgCountryMapList[i].color === "orange") {
          this.sdgOrangeCountryList.push(this.sdgCountryMapList[i])

        }
        else if (this.sdgCountryMapList[i].color === "yellow") {
          this.sdgYellowCountryList.push(this.sdgCountryMapList[i])
        }
        else if (this.sdgCountryMapList[i].color === "green") {
          this.sdgGreenCountryList.push(this.sdgCountryMapList[i])
        }
      }
      for (let j = 0; j < this.sdgGreenCountryList.length; j++) {
        this.sdgGreenCountryScoreList.push(parseFloat(this.sdgGreenCountryList[j].score))
      }
      for (let j = 0; j < this.sdgOrangeCountryList.length; j++) {
        this.sdgOrangeCountryScoreList.push(parseFloat(this.sdgOrangeCountryList[j].score))
      }
      for (let j = 0; j < this.sdgYellowCountryList.length; j++) {
        this.sdgYellowCountryScoreList.push(parseFloat(this.sdgYellowCountryList[j].score))
      }
      for (let j = 0; j < this.sdgRedCountryList.length; j++) {
        this.sdgRedCountryScoreList.push(parseFloat(this.sdgRedCountryList[j].score))
      }
      //for green

      this.green = Object.values(this.sdgGreenCountryScoreList)
      if (this.green.length > 0) {
        this.greenMin = (Math.min(...this.green));
        this.greenMax = (Math.max(...this.green));
        this.greenTotal = this.sdgGreenCountryList.length;
      }
      else {
        this.greenMin = 0;
        this.greenMax = 0
        this.greenMediun = 0;
        this.greenTotal = 0;
      }
      //for orange
      this.organge = Object.values(this.sdgOrangeCountryScoreList)
      if (this.organge.length > 0) {
        this.orangeMin = (Math.min(...this.organge));
        this.orangeMax = (Math.max(...this.organge))
        this.orangeTotal = this.sdgOrangeCountryList.length;
      }
      else {
        this.orangeMin = 0;
        this.orangeMax = 0
        this.orangeTotal = 0;
      }
      //for yellow
      this.yellow = Object.values(this.sdgYellowCountryScoreList)
      if (this.yellow.length > 0) {
        this.yellowMin = (Math.min(...this.yellow));
        this.yellowMax = (Math.max(...this.yellow))
        this.yellowTotal = this.sdgYellowCountryList.length;
      }
      else {
        this.yellowMin = 0;
        this.yellowMax = 0
        this.yellowTotal = 0
      }



      //for red
      this.red = Object.values(this.sdgRedCountryScoreList)
      if (this.red.length > 0) {
        this.redMin = (Math.min(...this.red))
        this.redMax = (Math.max(...this.red))
        this.redTotal = this.sdgRedCountryList.length;
      }
      else {
        this.redMin = 0
        this.redMax = 0
        this.redTotal = 0
      }

      //
      this.allMaxValue = [];
      this.flagForSort = false;
      this.allMaxValue.push(parseFloat(this.redMax));
      this.allMaxValue.push(parseFloat(this.orangeMax));
      this.allMaxValue.push(parseFloat(this.yellowMax));
      this.allMaxValue.push(parseFloat(this.greenMax));

      let max = Math.max(...this.allMaxValue)
      if (parseFloat(this.redMax) === max) {
        this.flagForSort = true
      }
      else {
        this.flagForSort = false;
      }

      for (let i = 0; i < this.sdgCountryMapList.length; i++) {
        if (this.sdgCountryMapList[i].score) {
          if (parseFloat(this.sdgCountryMapList[i].score) === (this.redMax)) {
            this.redMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.redMin)) {
            this.redMinCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.greenMax)) {
            this.greenMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.greenMin)) {
            this.greenMinCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.orangeMax)) {
            this.orangeMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.orangeMin)) {
            this.orangeMinCountry = this.sdgCountryMapList[i].country
          } else if (parseFloat(this.sdgCountryMapList[i].score) === (this.yellowMax)) {
            this.yellowMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.yellowMin)) {
            this.yellowMinCountry = this.sdgCountryMapList[i].country
          }
        }
      }

    })
  }
  radioButtonValue(event: any, targetId: any, i: any, country, color) {
    this.chartReady = false;
    if (event.target.name === 'sdg_selection') {
      if (targetId !== 0) {
        this.sdgTargetCode = targetId;
        this.sdgSubGoalValue = this.sdgTargets[i].id;
        this.sdgTargetDesc = this.sdgTargets[i].value;
        //

        this.SdgToolService.sdgSubGoalId = this.sdgSubGoalValue;
        this.SdgToolService.sdgSubGoalString = this.sdgTargetDesc;
        //
      }

      this.disabledButton = true;
    }
    else if (event.target.name === 'sdg_country_list') {
      this.SdgToolService.selectedCountry = country
      this.SdgToolService.colorId = color
      this.goalName = this.getSdgGoalName(this.sdgId)
      this.translate.get
      this.plyLifeFlag = true;
    }
  }

  dotPlotGraph() {
    this.chartReady = true
    this.chartColors = [];
    this.lineChartData = [];
    this.lineChartLabels = [];
    this.chartColors[0] = {}
    let graphData = [];
    let i = 0
    this.chartColors[0]['pointBackgroundColor'] = []
    graphData = JSON.parse(JSON.stringify(this.sdgCountryMapList))
    let graphFinalData = [];
    for (let j = 0; j < graphData.length; j++) {
      if (graphData[j].hasOwnProperty("score") && graphData[j].hasOwnProperty("color")) {
        if (graphData[j].score || graphData[j].color) {
          graphData[j].score = (parseFloat(graphData[j].score))
          graphFinalData.push(graphData[j]);
        }
      }
    }
    let dummy
    if (this.flagForSort) {
      dummy = graphFinalData.sort((first, second) => 0 - (first.score < second.score ? -1 : 1));
    }
    else {
      dummy = graphFinalData.sort((first, second) => 0 - (first.score > second.score ? -1 : 1));
    }
    for (let sdg of dummy) {
      if (sdg.color == 'red') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = "#d3270d"
        this.lineChartData[i] = (sdg.score)
      }
      else if (sdg.color == 'orange') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = "#FFA019"
        this.lineChartData[i] = (sdg.score)
      }
      else if (sdg.color == 'yellow') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = " #ffff00"
        this.lineChartData[i] = (sdg.score)
      }
      else if (sdg.color == 'green') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = "#36AD1F"
        this.lineChartData[i] = (sdg.score)
      }
      i++

    }
    this.labelMFL = [
      {
        data: this.lineChartData,
        label: this.translation.instant('gameLabel.SDG')
      }
    ];
    this.lineChartOptions = {
      responsive: true,
      scales: {
        yAxes: [{
          ticks: {

          },
          scaleLabel: {
            display: true,
            labelString: this.translation.instant('gameLabel.Subgoal_score')
          }
        }],
        xAxes: [{
          display: false,
          ticks: {

          }
        }],
      },
    };

  }

  closeAllMap() {
    this.secondWindow = false;
    am4core.disposeAllCharts();
  }


  sdgCountryList() {
    this.state = "subGoalCountryList"
    this.countryList = []
    this.sdgGreenCountryList = [];
    this.sdgRedCountryList = [];
    this.sdgYellowCountryList = [];
    this.sdgGrayCountryList = [];
    this.sdgOrangeCountryList = [];
    this.SdgToolService.getsdgsubGoalColorData(this.sdgTargetCode).subscribe((res: any) => {
      this.sdgGreenCountryList = res['green'];
      this.sdgRedCountryList = res['red'];
      this.sdgOrangeCountryList = res['orange'];
      this.sdgYellowCountryList = res['yellow'];
      this.sdgGrayCountryList = res['grey'];
      this.disabledButton = false;


    })
  }

  getSdgSubGoalMapData() {
    this.state = "subGoalCountryMap"
    this.divShow = true;
    this.disabledButtonMap = false;
  }

  public chartClicked(e: any): void {
  }
  public chartHovered(e: any): void {
  }
  clickMoreInfo() {
    this.moreInfoFlag = true
    let s1 = this.translation.instant('gameLabel.Minimum_info');
    let s2 = this.translation.instant('gameLabel.Maximum_info');
    let s3 = this.translation.instant('gameLabel.Median_info');
    this.moreInforStm = s1 + "<br><br>" + s2 + "<br><br>" + s3;
  }


  public close() {
    this.router.navigate(['/sdgSummary'])
  }
  // stateChange() {
  //   am4core.disposeAllCharts();
  //   this.state = 'city';
  //   this.modalWindowService.showchearacterDesignSdgFlag = false;
  //   this.modalWindowService.showSdg = true;
  //   this.modalWindowService.sdgCountrySelectedObject = this.selectedCountryObject;
  //   this.gameSummeryService.sdgId = this.sdgId;
  //   this.modalWindowService.showcharacterDesignFlag = true;
  // }


  backClick(state: any) {
    if (state === "subGoalList") {
      this.state = "goalMap"
    }
    else if (state === "subGoalGraph") {
      this.state = "subGoalList"
    }
    else if (state === "subGoalCountryList") {
      this.state = "subGoalGraph"
    }
    else if (state === "subGoalCountryMap") {
      this.state = "subGoalGraph"
    }
    else if (state === 'goalMap') {
      this.state = "goal";
    }
    else if (state === 'goal') {
      this.windowOne = false;
      this.router.navigate(['/sdgSummary'])
    }
  }


  clickToExitButton() {
    if (this.gameSummeryService.countrySdgTool) {
      // this.gameSummeryService.countrySdgTool = false
      this.phpToolService.flowFromSdg = false;
      this.SdgToolService.flowFromWorldSdg = false;
      this.SdgToolService.flowFromSdgStmt = false;
      this.SdgToolService.flowFromCountrySdg = false;
      this.router.navigate(['/sdgSummary']);
    }
    else {
      this.phpToolService.flowFromSdg = false;
      this.SdgToolService.flowFromWorldSdg = false;
      this.SdgToolService.flowFromSdgStmt = false;
      this.SdgToolService.flowFromCountrySdg = false;
      this.commonService.close();
    }
  }

  clickCancel() {
    this.plyLifeFlag = false;
    this.SdgToolService.showPlayLifeWindow = false;
  }

  playReallives() {
    this.phpToolService.flowFromSdg = false;
    this.SdgToolService.flowFromWorldSdg = false;
    this.SdgToolService.flowFromSdgStmt = false;
    this.SdgToolService.flowFromCountrySdg = false;
    this.modalWindowService.showcharacterDesignFlag = true;
    this.modalWindowService.showWaitScreen = true
    let sdg;
    sdg = {
      "sdgId": this.sdgId,
      "goalString": this.sdgTargetDesc,
      "challengeId": this.SdgToolService.colorId,
      "subGoal": this.sdgSubGoalValue
    }
    //
    let country;
    country = {
      "countryid": this.SdgToolService.selectedCountry.countryid
    }
    this.designALifeObject["sdg"] = sdg;
    this.designALifeObject["country"] = country;
    this.SdgToolService.createLifeWithSdg(this.translationService.selectedLang.code, this.designALifeObject).subscribe(
      res => {
        this.gameId = res;
        localStorage.removeItem("gameid");
        localStorage.setItem("gameid", this.gameId);
        this.modalWindowService.showWaitScreen = false;
        this.router.navigate(['/createlife']);
      })
  }


  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }

  getSdgGoalName(sdgId) {
    let name;
    if (sdgId === '1') {
      name = this.translate.instant('gameLabel.sdg1')
    }
    else if (sdgId === '2') {
      name = this.translate.instant('gameLabel.sdg2')
    }
    else if (sdgId === '3') {
      name = this.translate.instant('gameLabel.sdg3')
    }
    else if (sdgId === '4') {
      name = this.translate.instant('gameLabel.sdg4')
    }
    else if (sdgId === '5') {
      name = this.translate.instant('gameLabel.sdg5')
    }
    else if (sdgId === '6') {
      name = this.translate.instant('gameLabel.sdg6')
    }
    else if (sdgId === '7') {
      name = this.translate.instant('gameLabel.sdg7')
    }
    else if (sdgId === '8') {
      name = this.translate.instant('gameLabel.sdg8')
    }
    else if (sdgId === '9') {
      name = this.translate.instant('gameLabel.sdg9')
    }
    else if (sdgId === '10') {
      name = this.translate.instant('gameLabel.sdg10')
    }
    else if (sdgId === '11') {
      name = this.translate.instant('gameLabel.sdg11')
    }
    else if (sdgId === '12') {
      name = this.translate.instant('gameLabel.sdg12')
    }
    else if (sdgId === '13') {
      name = this.translate.instant('gameLabel.sdg13')
    }
    else if (sdgId === 'q4') {
      name = this.translate.instant('gameLabel.sdg14')
    }
    else if (sdgId === '15') {
      name = this.translate.instant('gameLabel.sdg15')
    }
    else if (sdgId === '16') {
      name = this.translate.instant('gameLabel.sdg16')
    }
    else if (sdgId === '17') {
      name = this.translate.instant('gameLabel.sdg17')
    }
    return name;
  }


}



