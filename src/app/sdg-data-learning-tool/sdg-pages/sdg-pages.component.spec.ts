import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgPagesComponent } from './sdg-pages.component';

describe('SdgPagesComponent', () => {
  let component: SdgPagesComponent;
  let fixture: ComponentFixture<SdgPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
