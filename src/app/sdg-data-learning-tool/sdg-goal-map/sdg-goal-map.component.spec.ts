import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgGoalMapComponent } from './sdg-goal-map.component';

describe('SdgGoalMapComponent', () => {
  let component: SdgGoalMapComponent;
  let fixture: ComponentFixture<SdgGoalMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgGoalMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgGoalMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
