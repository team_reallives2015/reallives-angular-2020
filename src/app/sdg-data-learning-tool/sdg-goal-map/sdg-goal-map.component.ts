import { Component, OnInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { SdgToolServiceService } from '../sdg-tool-service.service';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
@Component({
  selector: 'app-sdg-goal-map',
  templateUrl: './sdg-goal-map.component.html',
  styleUrls: ['./sdg-goal-map.component.css']
})
export class SdgGoalMapComponent implements OnInit {
  sdgMapTitle: any;
  colourCode: any;
  constructor(public SdgToolService: SdgToolServiceService,
    public phpToolService: PhpToolService) { }

  ngOnInit(): void {
    if (this.SdgToolService.flowFromWorldSdg) {
      this.getSdgMap(this.SdgToolService.sdgId, this.SdgToolService.sdgGoalCountryList);
    }
    else if (this.SdgToolService.flowFromCountrySdg) {
      this.getSdgMapCountry(this.SdgToolService.sdgId, this.SdgToolService.sdgGoalCountryList);
    }

  }
  getSdgMap(sdgId: any, countryListSdg: any) {
    let map = am4core.create("sdgchartdiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.events.on("hit", function (ev) {
      // zoom to an object
    }, this);
    let county1 = [];
    for (let i = 0; i < countryListSdg.length; i++) {
      if (countryListSdg[i].color === "red") {
        this.colourCode = am4core.color("#d3270d")
      }
      else if (countryListSdg[i].color === "green") {
        this.colourCode = am4core.color("#36AD1F")

      }
      else if (countryListSdg[i].color === "orange") {
        this.colourCode = am4core.color("#FFA019")

      }
      else if (countryListSdg[i].color === "yellow") {
        this.colourCode = am4core.color("#ffff00")

      }
      else if (countryListSdg[i].color === "grey" || countryListSdg[i].color === "NA") {
        this.colourCode = am4core.color("#808080")
      }
      county1.push({
        "id": countryListSdg[i].code,
        "name": countryListSdg[i].countryname,
        "value": 100,
        "fill": this.colourCode
      })
    }
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }

  getSdgMapCountry(sdgId: any, countryListSdg: any) {
    let map = am4core.create("sdgchartdiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.events.on("hit", function (ev) {
      // zoom to an object
      ev.target.series.chart.zoomToMapObject(ev.target);
    });
    let county1 = [];
    for (let i = 0; i < countryListSdg.length; i++) {
      if (this.phpToolService.selectedBornCountryObject.countryid === countryListSdg[i].countryid || this.phpToolService.selectedRegisterCountryObject.countryid === countryListSdg[i].countryid) {
        if (countryListSdg[i].color === "red") {
          this.colourCode = am4core.color("#d3270d")
        }
        else if (countryListSdg[i].color === "green") {
          this.colourCode = am4core.color("#36AD1F")

        }
        else if (countryListSdg[i].color === "orange") {
          this.colourCode = am4core.color("#FFA019")

        }
        else if (countryListSdg[i].color === "yellow") {
          this.colourCode = am4core.color("#ffff00")

        }
        else if (countryListSdg[i].color === "grey" || countryListSdg[i].color === "NA") {
          this.colourCode = am4core.color("#808080")
        }
        county1.push({
          "id": countryListSdg[i].code,
          "name": countryListSdg[i].countryname,
          "value": 100,
          "fill": this.colourCode
        })
      }

    }
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }
}

