import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgQuizeMapComponent } from './sdg-quize-map.component';

describe('SdgQuizeMapComponent', () => {
  let component: SdgQuizeMapComponent;
  let fixture: ComponentFixture<SdgQuizeMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgQuizeMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgQuizeMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
