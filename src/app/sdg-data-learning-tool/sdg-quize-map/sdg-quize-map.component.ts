import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { SdgToolServiceService } from '../sdg-tool-service.service';

@Component({
  selector: 'app-sdg-quize-map',
  templateUrl: './sdg-quize-map.component.html',
  styleUrls: ['./sdg-quize-map.component.css']
})
export class SdgQuizeMapComponent implements OnInit {
  quetions: any[] = [];
  queLen: any;
  firstArr: any[] = [];
  secondArr: any[] = [];
  constructor(public SdgToolService: SdgToolServiceService,
    public commonService: CommonService) { }

  ngOnInit(): void {
    this.quetions = this.SdgToolService.sdgQuetions;
    this.setQuetions();
  }

  setQuetions() {
    let indexArray: any[] = [];
    let random: any;
    this.queLen = this.quetions.length;
    for (let k = 0; k < this.quetions.length; k++) {
      random = this.commonService.getRandomValueint(0, this.queLen)
      if (k > 0) {
        if (!indexArray.includes(random)) {
          indexArray.push(
            {
              key: random,
              que: this.quetions[random].question,
              ans: this.quetions[random].answer
            }
          )
        }
      }
      else {
        indexArray.push(
          {
            key: random,
            que: this.quetions[random].question,
            ans: this.quetions[random].answer
          }
        )
      }
    }
    for (let i = 0; i < indexArray.length; i++) {
      if (i <= 4) {
        this.firstArr.push({
          "key": indexArray[i].key,
          "quetion": indexArray[i].que,
          "ans": indexArray[i].ans,
          "selectedAns": '',
          "class": '',

        })
      }
      else if (i > 4) {
        this.secondArr.push({
          "key": indexArray[i].key,
          "quetion": indexArray[i].que,
          "ans": indexArray[i].ans,
          "selectedAns": '',
          "class": '',

        })
      }
    }
  }

  redioButtonClick(value: any) {
    alert(value)

  }
}