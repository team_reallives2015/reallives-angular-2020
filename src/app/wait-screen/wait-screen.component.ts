import { Component, OnInit } from '@angular/core';
import { modalWindowShowHide } from '../modal-window-show-hide.service';

@Component({
  selector: 'app-wait-screen',
  templateUrl: './wait-screen.component.html',
  styleUrls: ['./wait-screen.component.css']
})
export class WaitScreenComponent implements OnInit {

  constructor(public modalWindowShowHide: modalWindowShowHide) { }

  ngOnInit(): void {
  }

}
