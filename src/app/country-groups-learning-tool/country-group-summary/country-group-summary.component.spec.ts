import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryGroupSummaryComponent } from './country-group-summary.component';

describe('CountryGroupSummaryComponent', () => {
  let component: CountryGroupSummaryComponent;
  let fixture: ComponentFixture<CountryGroupSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryGroupSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryGroupSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
