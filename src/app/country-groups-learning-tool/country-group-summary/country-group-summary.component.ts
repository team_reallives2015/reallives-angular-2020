import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-country-group-summary',
  templateUrl: './country-group-summary.component.html',
  styleUrls: ['./country-group-summary.component.css']
})
export class CountryGroupSummaryComponent implements OnInit {

  constructor(public router: Router,
    public gameSummary: GameSummeryService,
    public phpToolService: PhpToolService,
    public commonService: CommonService) { }

  ngOnInit(): void {
  }

  goToPages() {
    this.router.navigate(['/countryGroupPages'])
  }


  clickToExitButton() {
    if (this.gameSummary.countryGroupTool) {
      this.gameSummary.countryGroupTool = false;
      this.phpToolService.folwFromCountryGroup = false;
      this.router.navigate(['/action-page']);
    }
    else {
      this.phpToolService.folwFromCountryGroup = false;
      this.commonService.close();
    }
  }
}
