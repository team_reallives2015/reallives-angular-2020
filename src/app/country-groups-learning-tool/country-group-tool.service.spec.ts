import { TestBed } from '@angular/core/testing';

import { CountryGroupToolService } from './country-group-tool.service';

describe('CountryGroupToolService', () => {
  let service: CountryGroupToolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountryGroupToolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
