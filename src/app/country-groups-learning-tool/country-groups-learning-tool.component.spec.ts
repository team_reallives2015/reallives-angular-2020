import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryGroupsLearningToolComponent } from './country-groups-learning-tool.component';

describe('CountryGroupsLearningToolComponent', () => {
  let component: CountryGroupsLearningToolComponent;
  let fixture: ComponentFixture<CountryGroupsLearningToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryGroupsLearningToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryGroupsLearningToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
