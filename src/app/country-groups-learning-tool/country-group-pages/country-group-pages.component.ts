import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';

@Component({
  selector: 'app-country-group-pages',
  templateUrl: './country-group-pages.component.html',
  styleUrls: ['./country-group-pages.component.css']
})
export class CountryGroupPagesComponent implements OnInit {

  constructor(public summary: GameSummeryService,
    public phpToolService: PhpToolService,
    public modalWindowService: modalWindowShowHide,
    public commonService: CommonService,
    public constantService: ConstantService,
    public router: Router,
    public gameSummary: GameSummeryService,
    public translate: TranslateService,
    public translationService: TranslationService) { }
  playLifeWarrFlag: boolean = false;
  classOne = 'borderRed';
  classTwo = '';
  classThree = '';
  classFour = '';
  classFive = '';
  classSix = '';
  single = [];
  msgText = '';
  gameId;
  selectedcountry = [];
  designALifeObject = {};
  classForCountryOne = ""
  classForCountryTwo = ""
  compaireCountryFlag: boolean = false;
  mySelect1 = '1';
  mySelect2 = '1';
  countryInfo;
  lat: number;
  lng: number;
  markers = [];
  mapZoom = 0;
  localUrl;
  dispalyOnlyOneLegent: boolean = false
  countryGroupCatergory = [];
  disableButton: boolean = false;
  countryGroupList = [];
  countryList = [];
  selectedCategoryId;
  selectedCategoryName;
  group = [];
  groupList = [];
  groupName;
  webLink;
  selectedGroupId;
  //bar char
  view: any[] = [1000, 600];
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = false;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = this.translate.instant('gameLabel.Country');
  showYAxisLabel: boolean = true;
  xAxisLabel: string = this.translate.instant('gameLabel.Population');
  colorScheme = {
    domain: []
  };
  flagForResolution: boolean = false;
  displayLegentAll: boolean = false;
  groupTitle

  state = "country-group-category"
  ngOnInit(): void {
    this.getAllCountryGroupsCategoryList();
  }


  getAllCountryGroupsCategoryList() {
    this.summary.getCountryGroupCatergory(this.translationService.selectedLang.code).subscribe(
      res => {
        this.countryGroupCatergory = res;
      })
  }

  selectCat(cId, cName) {
    this.selectedCategoryId = cId;
    this.selectedCategoryName = cName
    this.translate.get('gameLabel.View_group_name', { cName: this.selectedCategoryName }).subscribe(
      (str) => {
        this.groupTitle = str;
      })
    this.getGroupMetaData();
    this.state = "group"

  }


  getGroupMetaData() {
    this.summary.getCountryGropuListByCategories(this.translationService.selectedLang.code, this.selectedCategoryId).subscribe(
      res => {
        this.group = res;
        this.groupList = JSON.parse(JSON.stringify(this.group));
        this.groupName = this.groupList[0].Group_display_name
        this.webLink = this.groupList[0].Wiki_Link;
        this.selectedGroupId = this.groupList[0].Group_ID;
      });
  }


  clickBack(state) {
    if (state === 'group') {
      this.state = "country-group-category"
    }
    else if (state === 'country') {
      this.groupList = JSON.parse(JSON.stringify(this.group));
      this.state = "group"
    }
    else if (state === 'graph') {
      this.state = "country"
      this.clickPopulation() ;
    }
    else if (state === 'reallives') {
      this.state = "graph"
    }
  }
  allRadioButtonValue(event: any) {
    this.disableButton = false;
    this.selectedGroupId = this.groupList[event.target.value].Group_ID;
    this.webLink = this.groupList[event.target.value].Wiki_Link;
    this.groupName = this.groupList[event.target.value].Group_display_name

  }


  shuffleCountry() {
    this.commonService.randomArrayShuffle(this.groupList);
    this.groupName = this.groupList[0].Group_display_name
    this.webLink = this.groupList[0].Wiki_Link;
    this.selectedGroupId = this.groupList[0].Group_ID;
  }

  aToz() {
    this.groupList = JSON.parse(JSON.stringify(this.group));
  }

  clickToExitButton() {
    if (this.gameSummary.countryGroupTool) {
      this.gameSummary.countryGroupTool = false;
      this.phpToolService.folwFromCountryGroup = false;
      this.router.navigate(['/action-page']);
    }
    else {
      this.phpToolService.folwFromCountryGroup = false;
      this.commonService.close();
    }
  }


  clickNext(state) {
    if (state === 'country') {
      this.state = 'graph'
    }
    else if (state = 'graph') {
      this.state = 'reallives'
      if (this.translationService.checkGameBuyOrNot) {
        this.translate.get('gameLabel.groupPlaylife', { group: this.groupName }).subscribe(
          (str) => {
            this.msgText = str;
          })
      }
      else {
        this.msgText = this.translate.instant('gameLabel.buy')

      }
    }
  }


  selectCountry() {
    this.single = [];
    this.disableButton = true;
    this.dispalyOnlyOneLegent = true;
    this.summary.getCountryListFromGroupId(this.selectedGroupId).subscribe(
      res => {
        this.countryList = res;
        for (var i = 0; i < this.countryList.length; i++) {
          this.single.push({
            "name": this.countryList[i].country,
            "value": this.countryList[i].population
          })
          this.colorScheme.domain.push('#567caa')
        }
        if (this.single.length % 2 !== 0) {
          this.single.sort((a, b) => a.value - b.value);
          let index: any;
          index = (this.single.length / 2);
          index = parseInt(index);
          index = index - 1;
          index = index + 1;
          this.colorScheme.domain[index] = ('#D1B000')
        }
        else {
          this.single = this.single.sort((a, b) => a.value - b.value);
          let index: any;
          index = this.single.length / 2;
          index = parseInt(index);
          index = index - 1;
          let v1 = this.single[index].value
          index = index + 1;
          let v2 = this.single[index].value
          let v3 = (v1 + v2) / 2
          this.single.push({
            "name": this.translate.instant('gameLabel.Median'),
            "value": v3
          })
          this.colorScheme.domain.push('#567caa');
          this.single = this.single.sort((a, b) => a.value - b.value);
          for (let j = 0; j < this.single.length; j++) {
            if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
              this.colorScheme.domain[j] = ('#D1B000');
              break;
            }
          }
        }
        Object.assign(this.single);
        this.fillMarkerCountry();
        this.translate.get('gameLabel.view_a_country_in_a_group', { gName: (this.groupName.toUpperCase()) }).subscribe(
          (s: String) => {
            this.countryInfo = s;
          })
        this.state = "country";
      });

  }

  fillMarkerCountry() {
    for (let i = 0; i < this.countryList.length; i++) {
      this.markers.push(
        {
          lat: this.countryList[i].lat,
          lng: this.countryList[i].lng,
          label: this.countryList[i].country,
          code: this.countryList[i].Code

        }
      );
    }
  }

  clickGini() {
    this.classOne = ""
    this.classThree = "borderRed"
    this.classFive = ""
    this.classFour = ""
    this.classTwo = ""
    this.classSix=""
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].gini
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.GINI');

  }

  clickHdi() {
    this.classOne = ""
    this.classThree = ""
    this.classFive = ""
    this.classFour = ""
    this.classSix = "borderRed"
    this.classTwo = ""
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].HDI
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.HDI');

  }

  clickHappiness() {
    this.classOne = ""
    this.classThree = ""
    this.classFive = ""
    this.classFour = "borderRed"
    this.classSix = ""
    this.classTwo = ""
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": parseFloat(this.countryList[i].HappinessScore)
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.Happiness');

  }

  clickPopulation() {
    this.classOne = "borderRed"
    this.classThree = ""
    this.classFive = ""
    this.classFour = ""
    this.classTwo = ""
    this.classSix=""
    this.colorScheme = {
      domain: []
    };
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = true;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].population
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000')
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      let v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
      for (let j = 0; j < this.single.length; j++) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
          break;
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.Population');

  }
  clickppp() {
    this.classOne = ""
    this.classThree = ""
    this.classFive = "borderRed"
    this.classFour = ""
    this.classSix = ""
    this.classTwo = ""
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].ppp
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.PPP');
  }

  clickSdg() {
    this.classOne = ""
    this.classThree = ""
    this.classFive = ""
    this.classFour = ""
    this.classSix = ""
    this.classTwo = "text-center countryGroupCommon borderRed"
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      if (this.countryList[i].SdgiScore === "NA") {
        this.countryList[i].SdgiScore = 0;
      }
      this.single.push({
        "name": this.countryList[i].country,
        "value": parseFloat(this.countryList[i].SdgiScore)
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.SDG_Score');

  }

  countryMarkerClick(countryObject) {
    this.disableButton = false;
    this.mapZoom = 4;
    this.lat = countryObject.lat;
    this.lng = countryObject.lng;
  }

  allRadioButtonValueCountrySelect(event: any) {

    this.selectedcountry = this.countryList[event.target.value]
  }

  // countrySelectionToPlayLifeFirst() {
  //   this.classForCountryOne = "country_map_log mx-auto countrySelectionClass"
  //   this.classForCountryTwo = "country_map_log mx-auto"
  //   // this.selectedcountry = this.phpToolService.selectedBornCountryObject
  // }


  // countrySelectionToPlayLifeSecond() {
  //   this.classForCountryOne = "country_map_log mx-auto"
  //   this.classForCountryTwo = " country_map_log mx-auto countrySelectionClass"
  //   // this.selectedcountry = this.phpToolService.selectedRegisterCountryObject
  // }

  playReallives() {
    if (this.selectedcountry.length === 0) {
      this.playLifeWarrFlag = true;
      this.msgText = this.translate.instant('gameLabel.live_life_warr')
    }
    else {
      this.phpToolService.folwFromCountryGroup = false;
      this.modalWindowService.showCharacterDesighWithGropuFlag = true;
      this.modalWindowService.showWaitScreen = true
      this.designALifeObject["country"] = this.selectedcountry;
      this.designALifeObject["onlyCountry"] = false;
      this.designALifeObject['city'] = null;
      this.designALifeObject["religion"] = null;
      this.designALifeObject["traits"] = null;
      this.designALifeObject['first_name'] = null;
      this.designALifeObject['last_name'] = null;
      this.designALifeObject['nameGroupId'] = null;
      this.designALifeObject['sex'] = null
      this.designALifeObject['urban_rural'] = null;
      this.summary.generateCharacterDesignLife(this.designALifeObject, false, false, 0, this.translationService.selectedLang.code, true, 0).subscribe(
        res => {
          this.gameId = res;
          localStorage.removeItem("gameid");
          localStorage.setItem("gameid", this.gameId);
          this.modalWindowService.showWaitScreen = false;
          this.router.navigate(['/createlife']);
        })
    }
  }

  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }

  warrWindowClose() {
    this.playLifeWarrFlag = false;
  }
}
