import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryGroupPagesComponent } from './country-group-pages.component';

describe('CountryGroupPagesComponent', () => {
  let component: CountryGroupPagesComponent;
  let fixture: ComponentFixture<CountryGroupPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryGroupPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryGroupPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
