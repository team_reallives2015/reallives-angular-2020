import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PhpToolService } from '../data-learning-tool-php/php-tool.service';
import { CommonService } from '../shared/common.service';

@Component({
  selector: 'app-country-groups-learning-tool',
  templateUrl: './country-groups-learning-tool.component.html',
  styleUrls: ['./country-groups-learning-tool.component.css']
})
export class CountryGroupsLearningToolComponent implements OnInit {
  token;
  constructor(public router: Router, public translate: TranslateService,
    public phpToolService: PhpToolService,
    public commonService: CommonService,
    public activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.phpToolService.folwFromCountryGroup = true;
    this.token = (this.activatedRouter.snapshot.queryParamMap.get('token') || 0);
    // this.token = "63ad1fb893312510221887f1"
    localStorage.setItem('token', this.token);
    this.router.navigate(['/language']);
  }

}
