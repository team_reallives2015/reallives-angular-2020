import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GameSummeryService } from '../game-summery/game-summery.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {
  token: any;
  constructor(private router: Router,
    private activatedRouter: ActivatedRoute,
    public gameSummeryService: GameSummeryService) { }

  ngOnInit(): void {
    this.token = (this.activatedRouter.snapshot.queryParamMap.get('token') || 0);
        // this.token = "659e7e7b3f1e9741b71ca8e9"
     localStorage.setItem('token', this.token);
    this.gameSummeryService.getValidToken().subscribe(
      res => {
        if (res === false) {
           this.router.navigate(['/language']);
        }
        else {
          this.token = res.data.result.token;
          localStorage.removeItem('token');
          localStorage.setItem('token', this.token);
           this.router.navigate(['/language']);
        }
      })
   }

}
