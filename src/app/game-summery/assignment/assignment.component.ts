import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.css']
})
export class AssignmentComponent implements OnInit {

  constructor(public modalWindowService: modalWindowShowHide,
    public gameSummeryService: GameSummeryService,
    public commonService: CommonService,
    public constantService: ConstantService,
    private router: Router,
    public sanitizer: DomSanitizer,
    public homeService: HomeService,
    public translate: TranslateService,
    public translationService: TranslationService) { }
    correctAnsFlag:boolean=false;
  groupList;
  correctCountry;
  assignMentList;
  gameId;
  assId;
  showGruopList: boolean = false;
  showAssignMentList: boolean = false;
  successMsgFlag: boolean = false;
  messageText;
  country: string = '';
  subjectName;
  showSuccessMsgFlag: boolean = false;
  moreInfoTitle;
  moreInforStm;
  showSuccessMsgFlagLoadLife: boolean = false;
  ngOnInit(): void {
    this.modalWindowService.showAssignment = true;
    this.showGruopList = true;
    this.showAssignMentList = false;
    this.gameSummeryService.getAllAssignmentGroupList(this.translationService.selectedLang.code).subscribe(
      res => {
        this.groupList = res;
      })
  }

  closeGroupList() {
    this.showGruopList = false;
    this.showAssignMentList = true;
    this.modalWindowService.showAssignment = false;
    this.router.navigate(['/summary'])
  }

  clickOnGroup(subjectName, groupId: any) {
    this.subjectName = subjectName;
    this.gameSummeryService.getAllAssignMentListByGroupId(groupId, this.translationService.selectedLang.code).subscribe(
      res => {
        this.assignMentList = res;
        this.showGruopList = false;
        this.showAssignMentList = true;
      })

  }

  clickOnGenarate(assignmentId,correctCountry) {
    this.correctCountry=correctCountry;
    this.messageText = this.translate.instant('gameLabel.Guess_the_country');
    this.assId = assignmentId;
    this.successMsgFlag = true;
  }
  generateLife() {
    this.correctAnsFlag=false
    if ((this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.moreInforStm = this.translate.instant('gameLabel.demo_warr_for_other');
      this.showSuccessMsgFlag = true;
    }
    else if ((this.gameSummeryService.totalPayedLifeCount >= this.gameSummeryService.maxLifeCount)) {
      this.moreInforStm = this.translate.instant('gameLabel.licences_exide_warr');
      this.showSuccessMsgFlagLoadLife = true;
    }
    else {
      this.successMsgFlag = false;
      this.modalWindowService.showWaitScreen = true;
      this.gameSummeryService.createLifeWithAssignment(this.assId, true, this.translationService.selectedLang.code).subscribe(
        res => {
          this.gameId = res['gameId'];
          localStorage.removeItem("gameid");
          localStorage.setItem("gameid", this.gameId);
          this.modalWindowService.showWaitScreen = false;
          this.router.navigate(['/createlife']);
        }
      )
    }
  }


  ansClick(){
    if(this.areStringsEqualIgnoreCase(this.country, this.correctCountry)){
      this.correctAnsFlag=true;
      this.messageText=this.translate.instant('gameLabel.corrcet_ans')
    }
    else{
      this.correctAnsFlag=true;
      this.translate.get('gameLabel.wrong_Ans', { countryname:this.correctCountry }).subscribe(
        (str1: String) => {
                this.messageText=str1
        })
    }
  }
   


  back() {
    this.successMsgFlag = false;
  }
  goBack() {
    this.showGruopList = true;
    this.showAssignMentList = false;
  }

  goHome() {
    this.showGruopList = false;
    this.modalWindowService.showAssignment = false;

    this.router.navigate(['/summary']);
  }

  successWindowOk() {
    this.showSuccessMsgFlag = false;
    this.modalWindowService.showAssignment = false;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.showGroupCountry = false;
    this.modalWindowService.showCharacterDesighWithGropuFlag = false;
    this.router.navigate(['/summary']);
  }

  loadIncompleteLife() {
    this.router.navigate(['/loadincompletegame']);

  }

  successWindowOkLoadLife() {
    this.showSuccessMsgFlagLoadLife = false;

  }

   areStringsEqualIgnoreCase(str1: string, str2: string): boolean {
    return str1.toLowerCase() === str2.toLowerCase();
  }
  
}
