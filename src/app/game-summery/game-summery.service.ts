import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { catchError } from 'rxjs/operators';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from '../shared/constant.service';
import { TranslationService } from '../translation/translation.service';

@Injectable({
  providedIn: 'root'
})
export class GameSummeryService {
  //tool
  countryLearningTool: boolean = false;
  countryDisaparityTool: boolean = false;
  countryGroupTool: boolean = false;
  countrySdgTool: boolean = false;
  //
  licencesExpiryFlag:boolean=false;
  newFetureOnceFlag:boolean=false;
  newFeatureFlag:boolean=false;
  mainPerson;
  allData;
  sdgId: number = 0;
  country;
  registerCountry;
  city;
  sdgLogoClass;
  sdgLogo;
  sdgLogoOrNotFlag: boolean = false;
  ecoStatusData;
  bornCountryWelathMetadata;
  selectedCountryObjectForSdg;
  mapDisplay: boolean = true;
  UserRole: String;
  studentAssignmentCountryGroupData;
  displayCountryGroupData: boolean = false;
  studentAssId = 0;
  bornSummary;
  sdgSubGoalId;
  sdgSubGoalString;
  sdgChallengeId;
  flagForStudentAssignMent: boolean = false;
  flowFromStudent: boolean = false;
  generateLifeWithDemo: boolean = false;
  maxLifeCount;
  licenceId;
  totalPayedLifeCount;
  registerCountryName;
  chagedRegisterCountryName;
  regCountryId;
  changeRegCountryId;
  selectedSdgArray = [];
  gameWithOnlySdgFlag: boolean = false;
  gameId;
  showLifeDiaryListFlag: boolean = false;
  showLifeDataListFlag: boolean = false;
  showLifeBookListFlag: boolean = false;
createLifenextEventFlag:boolean=false;
createlifeNextEventId;
createlifeNextParameter;
public paginationArr = [30,40,50];
  constructor(private http: HttpClient,
    private commonService: CommonService,
    private constantService: ConstantService,
    public translate: TranslateService,
    public translationServics: TranslationService) { }

  getPersonFromId(id) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getperson/${id}`);
  }

  getCreateLifeData(characterDesignFlag, sdgForCreateLife, sdgId, selectedLang, charAge) {
    return this.http.post<Number>(`${this.commonService.url}game/createLife/${characterDesignFlag}/${sdgForCreateLife}/${sdgId}/${selectedLang}`, { Random: true, type: 'Random', charAge: charAge })
  }
  getReligionForWorld(countryId, lang) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/residence/religion/${countryId}/${lang}`)
  }

  getAllReligion(regionId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/residence/regionreligion/${regionId}`)
  }

  getCountryList() {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/residence/country/false`);
  }

  getCotinentList(){
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getAllContinents`);

  }

  getCountryListByContinenet(continentName){
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getCountryByContinent/${continentName}`);

  }

  getCity(countryId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/residence/city/${countryId}`);
  }

  getNameGroupId(regionId) {// this has two apis : api for india and api for rest of the world...
    return this.http.get<Number>(`${this.commonService.url}game/actionbar/character-design/nameGroupId/${regionId}`);
  }

  getIndianNameGroupId(regionId) {// this has two apis : api for india and api for rest of the world...
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/character-design/nameGroupId/Indian/${regionId}`);
  }

  getFirstNameList(countryid, genderFlag, nameType, religionId, nameGroupId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/character-design/FirstNameList/${countryid}/${genderFlag}/${nameType}/${religionId}/${nameGroupId}`);
  }

  getLastNameList(countryid, genderFlag, nameType, religionId, nameGroupId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/character-design/LastNameList/${countryid}/${genderFlag}/${nameType}/${religionId}/${nameGroupId}`);
  }

  generateCharacterDesignLife(designALifeObject, characterDesignFlag, sdgForCreateLife, sdgId, selectedLang, lifeCreatedByGroups, studentAssId) {
    return this.http.post<Number>(`${this.commonService.url}game/actionbar/character-design/designANewLife/${characterDesignFlag}/${sdgForCreateLife}/${sdgId}/${selectedLang}/${lifeCreatedByGroups}/${studentAssId}`, { designALifeObject });
  }

  getSDGInfo(countryId, flagForGoalsInfo, lang) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/SDGDetails/${countryId}/${flagForGoalsInfo}/${lang}`);
  }

  getSDGDetailsForDesignALife(lang) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/DesignALifeSDG/getSDGMetaData/${lang}`);
  }

  getSDGDetailsByCountryForDesignALife(sdgTargetCode) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/DesignALifeSDG/SDGDetailsByCountry/${sdgTargetCode}`);
  }

  getLoadGameData() {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/getGameList`);
  }
  getCompletedLifeData() {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/getCompletedGameList`);
  }
  getAllEventList(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}gameEvent/getEvent/${gameId}`);
  }

  getActionNextEvent(nextEventObject) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/actionEventNext`, { nextEventObject });
  }

  getRegisterCountryCapitalCity(cityName, countryId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getLatLong/${cityName}/${countryId}`);
  }

  getCountryGropuList(lang,) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getAllCountryGroups/${lang}`);

  }

  getCountryGropuListByCategories(lang, cId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getGroupByCat/${cId}/${lang}`);

  }

  getCountryListFromGroupId(groupId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getCountryByGroup/${groupId}`);
  }

  getAllAssignmentGroupList(langCode) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getAllSubjects/true/${langCode}`);
  }

  getAllAssignMentListByGroupId(subjectId, langCode) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getAllStudentAssignmentsBySubject/true/${subjectId}/${langCode}`);
  }


  createLifeWithAssignment(assignmentId, flag, langCode) {
    return this.http.get<Number>(`${this.commonService.url}game/createLifeByStudentAssignment/${assignmentId}/${flag}/${langCode}`);
  }

  getWelathMetaData(countryId) {
    return this.http.get<any>(`${this.commonService.url}game/getWealth/${countryId}`);

  }

  getSdgGoalNewData(countryId) {
    return this.http.get<any>(`${this.commonService.url}game/getAllSDGData/${countryId}`);
  }

  getAllCountryListForSdgLifeDesign() {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getAllNewSDG`);
  }

  getCountryGroupListForBornAndRegCountry(bornId, regId, lang) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getCountryGroupInfo/${bornId}/${regId}/${lang}`);
  }

  getPlayerRole() {
    return this.http.get<any>(`${this.commonService.url}game/determineRole`);
  }
  saveBornSummary(bornSummary, economicStatusData, wealthStatusData, gameId) {
    return this.http.post<boolean>(`${this.commonService.url}game/saveBornData/${gameId}`, { bornSummary, economicStatusData, wealthStatusData });
  }

  getBornSummary(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getBornSummary/${gameId}`);
  }


  loadLoadGameLife(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/loadGame/${gameId}`);

  }


  createorLoadDemoLife(langCode) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/generateDemoLifeApp/${langCode}`);

  }

  getRegisterCountry() {
    return this.http.get<any>(`${this.commonService.url}game/getUserCountries`);

  }

  setRegisterCountry(countryName) {
    return this.http.get<any>(`${this.commonService.url}game/changeCountry/${countryName}`);
  }

  getValidToken() {
    return this.http.post<any>(`${this.commonService.url}endpoint/session/createAndValidate`, {});
  }

  getSdgCountryColour(sdgId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/DesignALifeSDG/allSDGDataColor/${sdgId}`);
  }

  getSdgSubGoalCountryColour(sdgTargetId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/DesignALifeSDG/SDGDetailsMap/${sdgTargetId}`);
  }

  getCountryAllLanguages(countryId) {
    return this.http.get<any>(`${this.commonService.url}game/getCountryLang/${countryId}`);
  }

  getReligionByCountry(regCountryId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getCountryReligion/${regCountryId}`);
  }
  getSdgGoalCountryColor(country1, country2) {
    return this.http.post<any>(`${this.commonService.url}game/getCountrySDG`, { country1, country2 });
  }

  updateSoftDelateGameFlag(gameId, isComplete) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/updateGameEntry/${gameId}/${isComplete}`);

  }

  getDeletedLifeList() {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getDeletedLives`);

  }

  updateDeletedLife(gameId) {
    return this.http.get<any>(`${this.commonService.url}game/recoverLife/${gameId}`);

  }

  getCountryGroupCatergory(langCode) {
    return this.http.get<any>(`${this.commonService.url}game/getGroupCat/${langCode}`)
  }

  getAllPassportData() {
    return this.http.get<any>(`${this.commonService.url}game/getlivesByCountry`)
  }
  getCountryCoveredCount() {
    return this.http.get<any>(`${this.commonService.url}game/getCountriesCovered`)
  }
  getLivesCount() {
    return this.http.get<any>(`${this.commonService.url}game/userTotalLivesCount`)

  }
  setNextEventForCreateLife(gameId,eventId){
    return this.http.post<any>(`${this.commonService.url}reallives/event/setNextEvent/${gameId}`, { eventId });
  }
  getIncompleteListForPagination(lenghtFlag,limit,pageNo) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/getGameListPagination`,{lenghtFlag,limit,pageNo});
  }

  getCompleteListForPagination(lenghtFlag,limit,pageNo) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/getCompleteListPagination`,{lenghtFlag,limit,pageNo});
  }

  getDeletedListForPagination(lenghtFlag,limit,pageNo) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/getDeletedListPagination`,{lenghtFlag,limit,pageNo});
  }

  orpaheStringArray(orphanId) {
    let orphanString = ""
    if (orphanId === 1) {
      orphanString = this.translate.instant('gameLabel.takenCareString_1')
    }
    else if (orphanId === 2) {
      orphanString = this.translate.instant('gameLabel.takenCareString_2')

    }
    else if (orphanId === 3) {
      orphanString = this.translate.instant('gameLabel.takenCareString_3')

    }
    else if (orphanId === 4) {
      orphanString = this.translate.instant('gameLabel.takenCareString_4')
    }
    return orphanString
  }

  BornAtAnyAgeNextEvent(allPerson){
    this.createLifenextEventFlag=true;
    if(allPerson[this.constantService.FAMILY_SELF].orphanIndex>0){
   if(allPerson[this.constantService.FAMILY_SELF].orphanIndex<=3){
     this.createlifeNextEventId="COMMON_BORN_DYNAMIC_AGE_GUARDIAN"
   }
   else{
    this.createlifeNextEventId="COMMON_BORN_DYNAMIC_AGE_ORPHANAGE"
   }
  }
  else{
this.createlifeNextEventId="COMMON_BORN_DYNAMIC_BORN_SIBLING_DEAD"
  }
  }

  checkSiblingDeadAtAge17(allPerson){
    let mainPerson=allPerson[this.constantService.FAMILY_SELF];
    let siblings,sibString,sib;
    let deadSibCount=0;
    if (mainPerson[this.constantService.FAMILY_SIBLING] != null) {
      siblings = mainPerson[this.constantService.FAMILY_SIBLING];
      for (var i = 0; i < siblings.length; i++) {
        sibString = mainPerson.siblings[i];
      sib = allPerson[sibString];
        if (sib.dead) {
          deadSibCount=deadSibCount+1;
  }
}
    }
    else{
      deadSibCount=0
    }
    return deadSibCount;

}
}









