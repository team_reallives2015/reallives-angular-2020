import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadGameAnimationComponent } from './load-game-animation.component';

describe('LoadGameAnimationComponent', () => {
  let component: LoadGameAnimationComponent;
  let fixture: ComponentFixture<LoadGameAnimationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadGameAnimationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadGameAnimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
