import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HomeService } from 'src/app/home/home.service';
import { PostGameService } from 'src/app/home/post-game.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-load-game-animation',
  templateUrl: './load-game-animation.component.html',
  styleUrls: ['./load-game-animation.component.css']
})
export class LoadGameAnimationComponent implements OnInit {
  countryChallengesArray = [];
  challengeHeading;
  challengeInfo;
  bornSummaryTitle;
  classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
  moreInfoTitle;
  moreInfoStm;
  chanceOfPoorR;
  bOne: number = 0;
  bTwo: number = 0;
  bThree: number = 0;
  bFour: number = 0;
  rOne: number = 0;
  rTwo: number = 0;
  rThree: number = 0;
  rFour: number = 0;
  firstR
  moreInfoFlag: boolean = false;
  bornCountry;
  registerCountry;
  //
  gameId;
  allPerson;
  mainPerson;
  showBornVideo = false;
  gender;
  boyOrGirl;
  religions;
  state = "born-screen"
  showAnimationFlag: boolean = false;
  showCountryInfoFlag: boolean = false;
  showBornInfoFlag: boolean = false;
  showHomeData: boolean = false;
  checkFormat: string;
  bornScreenArray = [];
  bornScreenButtonArray = [];
  buttonText = this.translate.instant('gameLabel.View_Born_Info');
  //dummyCountry
  dummyCountry;
  registeredCountry;
  registerCountrysdgInfo
  //end
  classForhealth;
  classForHealthStatus;
  sdgDetails
  bornSdgSubGoalDeatails;
  bornSdgInfo: any
  regSdgInfo: any
  registerSdgInfo: any;
  regSdgSubGoalDeatails
  registerCountrySdgName
  sdgDescription
  sdgInfo
  sdgName
  sdgOrigninalName;
  registeredCountrySdgDeatails;
  sdgNewData;
  sdgReagisterData;
  bornGoalColur;
  regiGoalColue
  values: any;
  valuesR: any;
  //countrySpecificInfo
  countryInfo
  population
  countryFlag
  classvalue;
  //person info variables
  familyMembers
  active = "clr_code bg-success d-inline-block mr-2"
  nonActive
  selectedLanguage: String;
  senntenceGeneratedFlag: boolean = false;
  //map
  longitude;
  latitude;
  mapZoom = 4;
  cityName;
  sex;
  sibCount;
  map
  previous;
  moveOut;
  //amenities
  computerAvailability = "clr_code bg-red d-inline-block mr-2"
  refrigeratorAvailability = "clr_code bg-red d-inline-block mr-2"
  safeWaterAvailability = "clr_code bg-red d-inline-block mr-2"
  basicSanitationAvailability = "clr_code bg-red d-inline-block mr-2"
  healthServiceAvailability = "clr_code bg-red d-inline-block mr-2"
  televisionAvailability = "clr_code bg-red d-inline-block mr-2"
  radioAvailability = "clr_code bg-red d-inline-block mr-2"
  telephoneAvailability = "clr_code bg-red d-inline-block mr-2"
  mobileAvailability = "clr_code bg-red d-inline-block mr-2"
  vehicleAvailability = "clr_code bg-red d-inline-block mr-2"
  internetAvailability = "clr_code bg-red d-inline-block mr-2"
  lat;
  lng;
  zoom = 2;
  labelName;
  start_end_mark = [];
  latlng = [];
  sdgImage = ""
  //tree
  religion;
  currencyName;
  diet;
  shelter;
  options;
  selfIncome: number;
  countryIncome;
  currencyCode;
  education;
  self;
  mainPersonName;
  mainPersonAge;
  mother;
  motherString;
  father;
  fatherString;
  wife;
  wifeString;
  husband;
  husbandString;
  siblings;
  siblingString;
  sibString;
  sib;
  children;
  childrenString;
  childString;
  child;
  grandChildren;
  grandChildrenString;
  grandChildString;
  grandChild;
  livingAtHomeArr;
  livingAwayArr = [];
  level1Arr = [];
  level2Arr = [];
  level3Arr = [];
  level4Arr = [];
  identity;
  image;
  width;
  class;
  householdIncome;
  householdExpenses;
  householdNetWorth;
  language;
  classForStatusOne;
  classForStatusTwo;
  classForStatusThree
  classForStatusFour;
  classForStatusFive;
  classForStatusBorderOne;
  classForStatusBorderTwo;
  classForStatusBorderThree
  classForStatusBorderFour;
  classForStatusBorderFive;
  //graph
  showGraphWindow: boolean = false;
  maxLenght: number = 40;
  yTrimValue: boolean = true;

  single: any[];
  view: any[] = [320, 200];
  view1: any[] = [320, 200];
  view2: any[] = [700, 400];
  view3 = [1000, 200];
  view4 = [600, 200];

  multi: any[];
  multi1: any[];
  multi2: any[];
  multi3: any[];
  multi4: any[];
  multi5: any[];
  deadClass;
  showDataLabel = true;
  colorScheme2 = {
    domain: ['#00FFFF', '#567caa', '#567caa']
  };
  schemeType: string = 'linear';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  maxXAxisTickLength: number = 10
  gradient: boolean = false;
  showLegend: boolean = false;
  showLegend1: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Country';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = 'Population';
  wealthGraphSentenceBorn1;
  wealthGraphSentenceBorn2;
  wealthGraphSentenceBorn3;
  wealthGraphSentenceReg1;
  wealthGraphSentenceReg2;
  wealthGraphSentenceReg3;
  registerCountryArray;
  poupulationValue;
  bornCountryArray
  bornCountryGroupList;
  groupIndex;
  registerCountryGroupList;
  countryGroupTitle;
  countryGroupIframeFlag: boolean = false
  webLink;
  colorScheme = {
    domain: ['#00FFFF', '#00FFFF']
  };

  colorScheme1 = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA'],
  };
  colorScheme3 = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#0000FF']
  };
  formatDataLabel(value) {
    return "$" + value;
  }
  jobName = '';
  stringReturnValue;
  stringRetutnText;
  activeClassForDemo = "nav-item nav-link active"
  activeClassForPerCapita = "nav-item nav-link";
  activeClassForLFO = "nav-item nav-link";
  activeClassForIndex = "nav-item nav-link";
  activeClassForSdg = "nav-item nav-link";
  onClickDemoGraphic: boolean = true;
  onClickPerCapita: boolean = false;
  onCLickLabourForce: boolean = false;
  onCLickSdgGroup: boolean = false;
  onClickIndexRating: boolean = false;
  onClickSdgData: boolean = false;
  sentenceTextWindow1: string = "";
  bithString: string = "";
  deathString: string = "";
  primarySchoolString: string = "";
  happinessString: string = "";
  valurForHappiAndCorr;
  giniIndexString: string = "";
  labourForceString1: string = "";
  labourForceString2: string = "";
  sexRatio: string = "";
  accesEleString = "";
  valueForHdi;
  sexRatioValue;
  sdgScoreBorn;
  sdgScoreRegister;
  populationString;
  bornCountryWelathMetaData: any;
  registerCountryWelathMetaData: any;
  bornCountryWelathMetaData1: any;
  registerCountryWelathMetaData1: any;
  welthGraphDataNotGenrateFlag: boolean = false;
  status;
  statusValue;
  firstValue;
  secondValue;
  thirdValue;
  fourthValue;
  firstValueReg;
  secondValueReg;
  thirdValueReg;
  fourthValueReg;
  statusMetadata
  oldStatusValue;
  registerStatusData;
  incomeGraphStringOne;
  incomeGraphStringTwo;
  incomeGraphStringThree;
  incomeGraphStringFour;
  registerCountryExchangeRate;
  registerCountryCode; nextEventObject;
  bornSummary;
  loadGameString;
  messageText;
  successMsgFlag;
  friendMailShowFlag: boolean = false;
  emailId;
  token;
  flagForLoadAssignments;
  finalStatement;
  finalStatementReg;
  constantGraphValueArray = [
    { 'name': '10%', 'value': 10 / 100 },
    { 'name': '20%', 'value': 20 / 100 },
    { 'name': '30%', 'value': 30 / 100 },
    { 'name': '40%', 'value': 40 / 100 },
    { 'name': '50%', 'value': 50 / 100 },
    { 'name': '60%', 'value': 60 / 100 },
    { 'name': '70%', 'value': 70 / 100 },
    { 'name': '80%', 'value': 80 / 100 },
    { 'name': '90%', 'value': 90 / 100 },
    { 'name': '100%', 'value': 100 / 100 }
  ]
  constructor(public commonService: CommonService,
    public gameSummeryService: GameSummeryService,
    public modalWindowService: modalWindowShowHide,
    private router: Router,
    public homeService: HomeService,
    public translationService: TranslationService,
    public translate: TranslateService,
    public constantService: ConstantService,
    public postGameService: PostGameService, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.homeService.clicktoGameSummary = false;
    this.loadLife();
  }

  loadLife() {
    this.flagForLoadAssignments = this.route.snapshot.queryParamMap.get('flag')
    this.flagForLoadAssignments = this.getBoolean();
    if (!this.flagForLoadAssignments) {
      this.token = (this.route.snapshot.queryParamMap.get('token') || 0);
      localStorage.setItem('token', this.token);
      this.gameId = this.route.snapshot.queryParamMap.get('id')
      localStorage.setItem('gameid', this.gameId);
      this.loadGameData();
      this.route.queryParamMap.subscribe(paramMap => {
        this.commonService.filter(paramMap, 'flag');
        this.commonService.filter(paramMap, 'id');
        this.commonService.filter(paramMap, 'token');
        this.commonService.removeParamFromUrl(paramMap, ['flag', 'id', 'token'])
      });
    }
    else {
      this.gameId = localStorage.getItem("gameid");
      this.loadGameData();
      this.route.queryParamMap.subscribe(paramMap => {
        this.commonService.filter(paramMap, 'flag');
        this.commonService.removeParamFromUrl(paramMap, ['flag']);
      });
    }

  }

  loadGameData() {
    this.gameSummeryService.loadLoadGameLife(this.gameId).subscribe(res => {
      this.allPerson = res;
      this.homeService.allPerson = res;
      this.translationService.setSelectedlanguage(this.allPerson.selected_language);
      this.mainPerson = this.allPerson['SELF'];
      this.gameSummeryService.country = this.mainPerson.country;
      this.registeredCountry = this.mainPerson.register_country;
      this.latitude = this.mainPerson.city.latitude
      this.longitude = this.mainPerson.city.longitude
      this.cityName = this.mainPerson.city.cityName;
      this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid, true, this.allPerson.selected_language).subscribe(sdgInfo => {
        this.sdgDetails = sdgInfo['goals'];
        this.bornSdgSubGoalDeatails = sdgInfo['targets'];
        this.gameSummeryService.getSDGInfo(this.registeredCountry.countryid, false, this.allPerson.selected_language).subscribe(sdgInfo => {
          this.regSdgSubGoalDeatails = sdgInfo['targets'];
          this.gameSummeryService.mapDisplay = false;
          this.gameSummeryService.country = this.mainPerson.country;
          this.gameSummeryService.city = this.mainPerson.city;
          this.gameSummeryService.registerCountry = this.mainPerson.register_country;
          this.sdgScoreBorn = this.mainPerson.country.SdgiScore;
          this.sdgScoreRegister = this.registeredCountry.SdgiScore;
          this.gameSummeryService.getBornSummary(this.mainPerson.game_id).subscribe(
            res => {
              this.bornSummary = res;
              this.gameSummeryService.getWelathMetaData(this.mainPerson.country.countryid).subscribe(
                res => {
                  if (res !== 'false') {
                    this.bornCountryWelathMetaData = res;
                    this.gameSummeryService.bornCountryWelathMetadata = res;
                  }
                  else {
                    this.bornCountryWelathMetaData = false;
                    this.gameSummeryService.bornCountryWelathMetadata = false;
                  }
                  this.gameSummeryService.getWelathMetaData(this.registeredCountry.countryid).subscribe(
                    res => {
                      this.registerCountryWelathMetaData = res;
                      if (res !== 'false') {
                        this.registerCountryWelathMetaData = res;
                      }
                      else {
                        this.registerCountryWelathMetaData = false;
                      }
                      this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
                        res => {
                          this.statusMetadata = res[0];
                          this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
                            res => {
                              this.registerStatusData = res[0]
                              this.gameSummeryService.ecoStatusData = this.statusMetadata;
                              this.setSocialEconomicalStatus();
                              this.displayFamilyIncomeGraph();
                              this.generateArrayForWelathGraph();
                              this.buttonText = this.translate.instant('gameLabel.View_Born_Info');
                              this.bornScreenArray = ['born-summary', 'born-screen', 'country-map', 'wealth-grpah', 'wealth-grpah-comparision',
                                'country-disparities', 'country-SDG', 'eco-status', 'family-tree', 'family-income-grpah', 'country-week','family-amenities'];
                              this.bornScreenButtonArray.push({
                                'born-summary': '',
                                'born-screen': this.translate.instant('gameLabel.View_Born_Info'),
                                'country-map': this.translate.instant('gameLabel.Your_Country_on_the_Map'),
                                'wealth-grpah': this.translate.instant('gameLabel.View_Your_Country_Wealth_Graph'),
                                'wealth-grpah-comparision': this.translate.instant('gameLabel.View_Disparity_Between_Countries'),
                                'country-disparities': this.translate.instant('gameLabel.Comparision_of_Country_Attributes'),
                                'country-SDG': this.translate.instant('gameLabel.View_SDG'),
                                'eco-status': this.translate.instant('gameLabel.View_Economical_Status'),
                                'family-tree': this.translate.instant('gameLabel.View_Your_Family'),
                                'family-income-grpah': this.translate.instant('gameLabel.View_Income_Graph'),
                                'country-week': this.translate.instant('gameLabel.Country_challenges'),
                                'family-amenities': this.translate.instant('gameLabel.View_Family_Amenities'),
                              })
                              this.updateBornSummaryArray();
                              if (this.mainPerson.sex === 'F') {
                                this.boyOrGirl = this.translate.instant('gameLabel.girl');
                                this.gender = this.translate.instant('gameLabel.Female');
                              }
                              else if (this.mainPerson.sex === 'M') {
                                this.boyOrGirl = this.translate.instant('gameLabel.boy');
                                this.gender = this.translate.instant('gameLabel.Male');
                              }
                              if (this.allPerson.sdgId !== 0) {
                                this.gameSummeryService.sdgLogoOrNotFlag = true;
                                this.gameSummeryService.sdgLogoClass = "active"
                                this.gameSummeryService.sdgLogo = "assets/images/sdgicon/sdg" + this.allPerson.sdgId + "_" + this.translationService.selectedLang.code + ".png";
                                this.commonService.getSdgStringSplite(this.homeService.allPerson.sdgDetails);

                              }
                              else {
                                this.gameSummeryService.sdgLogoOrNotFlag = false;
                              }
                              if (this.mainPerson.country.countryid === 74) {
                                this.gameSummeryService.getAllReligion(this.mainPerson.city.Countryregionid).subscribe(data => {
                                  this.religions = data;

                                });
                              } else {
                                this.gameSummeryService.getReligionForWorld(this.mainPerson.country.countryid, this.allPerson.selected_language).subscribe(data => {
                                  this.religions = data;
                                });
                              }
                              let s = this.translate.instant('gameLabel.View_Born_Info')
                              this.translate.get('gameLabel.Continue_playing_as_a', { boygirl: this.boyOrGirl }).subscribe(
                                (str) => {
                                  this.loadGameString = str;
                                })
                              this.showHomeData = true;
                              this.state = "born-summary";
                              this.translate.get('gameLabel.Born_Summary',{firstName : this.allPerson[this.constantService.FAMILY_SELF].first_name}).subscribe(
                                str=>{
                                  this.bornSummaryTitle=str;
                                })
                            })
                        })
                    })
                })
            })
        })
      })
    })
  }


  private getBoolean(): boolean {
    return JSON.parse(this.flagForLoadAssignments);
  }

  close() {
    this.showGraphWindow = false;
  }

  playLifeClick() {
    this.commonService.audio.pause();
    this.modalWindowService.showGotoHome = true;
    this.router.navigate(
      ['/play-life'],
      { queryParams: { flag: 'true' } }
    );
  }

  goBack() {
    this.router.navigate(['/summary']);
  }


  setSocialEconomicalStatus() {
    //regi
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    this.firstValueReg = (f3R);
    this.secondValueReg = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    this.thirdValueReg = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    this.fourthValueReg = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    //end
    this.commonService.calculateWealthStatus(this.bornCountryWelathMetaData, this.homeService.allPerson);
    this.commonService.calculateIncameStatus(this.statusMetadata, this.homeService.allPerson);
    // let wealth=(this.commonService.wealthStatus);
    // let income=(this.commonService.incomeStatus);
    // if(wealth>income){
    //   this.status=this.commonService.wealthStatus;
    //   this.statusValue=this.commonService.wealthValue;
    // }
    // else if(wealth<income){
    //   this.status=this.commonService.incomeStatus;
    //   this.statusValue=this.commonService.incomeValue
    // }
    // else if(wealth===income){
    //   this.status=this.commonService.incomeStatus;
    //   if(this.commonService.wealthValue>this.commonService.incomeValue){
    //     this.statusValue=this.commonService.wealthValue
    //   }
    //   else if(this.commonService.wealthValue<this.commonService.incomeValue){
    //     this.statusValue=this.commonService.incomeValue
    //   }
    //   else{
    //     this.statusValue=this.commonService.incomeValue
    //   }

    // }

    //  let statusObj={
    //    'status':this.status,
    //    'newPoints':this.statusValue,
    //    'oldPoints':this.statusValue
    //  }
    // this.homeService.saveFamilyStatus(this.mainPerson.game_id,statusObj,0,null).subscribe(
    //   res=>{
    //     if(res===true){
    //       this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status=this.status;
    //       this.homeService.allPerson[this.constantService.FAMILY_SELF].old_economic_points=this.statusValue;
    //     this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points=this.statusValue;
    //     }
    //   })
  }


  displayAmenities() {
    var amenities = this.mainPerson.amenities
    if (amenities.computers > 0) {
      this.computerAvailability = "clr_code bg-success d-inline-block mr-2"
    }
    if (amenities.refrigerators > 0)
      this.refrigeratorAvailability = "clr_code bg-success d-inline-block mr-2"

    if (amenities.safeWater)
      this.safeWaterAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.basicSanitation)
      this.basicSanitationAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.healthService)
      this.healthServiceAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.televisions > 0)
      this.televisionAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.radios > 0)
      this.radioAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.telephones > 0)
      this.telephoneAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.mobiles > 0)
      this.mobileAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.vehicles > 0)
      this.vehicleAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.internet)
      this.internetAvailability = "clr_code bg-success d-inline-block mr-2"


  }

  clickNext(state: string) {
    let buttonState;
    let index = this.bornScreenArray.indexOf(state);
    index = index + 1;
    state = this.bornScreenArray[index];
    this.state = state
    buttonState = this.bornScreenArray[index + 1];
    this.buttonText = this.bornScreenButtonArray[0][buttonState];

    if (state === 'country-register-country-map') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.gotoStateCountryRegisterMap();
    }
    if (state === 'family-tree') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting";
      this.createFamilyTree(this.homeService.allPerson);
    }
    else if (state === 'country-map') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.gotoStateCountryMap();
    }
    else if (state === 'wealth-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting";
      this.getDataForWelthGraph();
    }
    else if (state == 'wealth-grpah-comparision') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.view4 = [600, 200];
      this.getDataForWelthGraph();
      this.getDataForWelthGraphCompariosion()
    }
    else if (state == 'family-amenities') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no  livealifestarting";
      this.displayAmenities()
    }
    else if (state === "country-disparities") {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.view = [320, 200];
      this.tabClickDemo();
    }
    else if (state == 'family-income-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.displayFamilyIncomeGraph()

    }
    else if (state === 'eco-status') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting";
      this.calculateTableValues();

    }
    else if (state === 'country-week') {
      this.countryChallengesFormating();
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
    }

  }



  createFamilyTree(allPerson) {
    this.allPerson = allPerson;
    this.mainPerson = this.allPerson[this.constantService.FAMILY_SELF];
    this.mainPersonName = this.mainPerson.first_name;
    this.registerCountryExchangeRate = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.householdIncome = this.mainPerson.expense.householdIncome.toFixed(2);
    this.householdExpenses = this.mainPerson.expense.householdExpenses.toFixed(2);
    this.householdNetWorth = this.mainPerson.expense.householdNetWorth.toFixed(2);
    this.currencyName = this.mainPerson.country.currencyName;
    this.mainPersonAge = this.mainPerson.age;
    this.selfIncome = parseInt("" + (this.mainPerson.expense.householdIncome * 12) / this.mainPerson.country.ExchangeRate);
    this.countryIncome = (this.mainPerson.country.ProdperCapita * this.mainPerson.country.AverageFamilyCount);
    this.level1Arr = [];
    this.level2Arr = [];
    this.currencyCode = this.mainPerson.country.currencyCode;
    this.registerCountryCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural,


      this.father = this.allPerson[this.constantService.FAMILY_FATHER];
    this.classForHealthStatus = "";
    this.classForhealth = "";
    if (this.father.traits.helath < 35) {
      this.classForhealth = "border-bottom colourRed1"
    }
    else {
      this.classForhealth = "border-bottom"
    }
    if (!this.father.dead) {
      this.deadClass = "";
    }
    else if (this.father.dead) {
      this.deadClass = "card_disable";

    }

    let healthProblemFather = "";
    if (this.father.health_disease != null && !this.father.dead) {
      let keys = Object.keys(this.father.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.father.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemFather == "") {
            healthProblemFather = healthProblemFather + this.father.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemFather = healthProblemFather + ", " + this.father.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemFather === "") {
        healthProblemFather = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      if (healthProblemFather === "") {
        healthProblemFather = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    if (this.father.living_at_home) {
      this.moveOut = this.translate.instant('gameLabel.moved_out')
    }
    else {
      this.moveOut = "";
    }
    let educationFather = this.commonService.setEducationForAll(this.father);
    this.jobName = this.setJobOrBusinessnameForOther(this.father);
    this.level1Arr.push({
      "name": this.father.full_name,
      "education": educationFather,
      "age": this.father.age,
      "identity": this.translate.instant('gameLabel.Father'),
      "sex": this.translate.instant('gameLabel.Male'),
      "image": "assets/images/game_images/fam.png",
      "class": "fmly_member men_bg",
      "jobName": this.father.job.Job_displayName,
      "income": this.father.income.toFixed(2),
      "registerIncome": ((this.father.income / this.homeService.allPerson[this.constantService.FAMILY_FATHER].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.father.traits.health,
      "deadClass": this.deadClass,
      "healthProblem": healthProblemFather,
      "currencyCode": this.father.country.CurrencyPlural,
      "currencyName": this.father.country.currency_code,
      "moveOut": this.moveOut,
      "classForHealth": this.classForhealth,
      "classForHelathStatus": this.classForHealthStatus
    })



    //code for mother
    this.mother = this.allPerson[this.constantService.FAMILY_MOTHER];
    this.classForHealthStatus = "";
    this.classForhealth = "";
    if (this.mother.traits.helath < 35) {
      this.classForhealth = "border-bottom colourRed1"
    }
    else {
      this.classForhealth = "border-bottom"
    }

    if (!this.mother.dead) {
      this.deadClass = "";
    }
    else if (this.mother.dead) {
      this.deadClass = "card_disable";

    }

    let healthProblemMother = "";
    if (this.mother.health_disease != null && !this.mother.dead) {
      let keys = Object.keys(this.mother.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.mother.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemMother == "") {
            healthProblemMother = healthProblemMother + this.mother.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemMother = healthProblemMother + ", " + this.mother.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemMother === "") {
        healthProblemMother = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      if (healthProblemMother === "") {
        healthProblemMother = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    if (this.mother.living_at_home) {
      this.moveOut = this.translate.instant('gameLabel.moved_out')
    }
    else {
      this.moveOut = "";
    }
    let educationMother = this.commonService.setEducationForAll(this.mother);
    this.jobName = this.setJobOrBusinessnameForOther(this.mother);
    this.level1Arr.push({
      "name": this.mother.full_name,
      "age": this.mother.age,
      "education": educationMother,
      "identity": this.translate.instant('gameLabel.Mother'),
      "sex": this.translate.instant('gameLabel.Female'),
      "image": "assets/images/game_images/girl.png",
      "class": "fmly_member female_bg",
      "jobName": this.jobName,
      "income": this.mother.income.toFixed(2),
      "registerIncome": ((this.mother.income / this.homeService.allPerson[this.constantService.FAMILY_MOTHER].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.mother.traits.health,
      "deadClass": this.deadClass,
      "healthProblem": healthProblemMother,
      "currencyCode": this.mother.country.CurrencyPlural,
      "currencyName": this.mother.country.currency_code,
      "moveOut": this.moveOut,
      "classForHealth": this.classForhealth,
      "classForHelathStatus": this.classForHealthStatus
    })




    //code for self

    this.self = this.allPerson[this.constantService.FAMILY_SELF];
    this.classForHealthStatus = "";
    this.classForhealth = "";
    if (this.self.traits.helath < 35) {
      this.classForhealth = "border-bottom colourRed1"
    }
    else {
      this.classForhealth = "border-bottom"
    }
    if (!this.self.dead) {
      this.deadClass = "";
    }
    else if (this.self.dead) {
      this.deadClass = "card_disable";

    }

    if (this.self.sex == "F") {
      this.gender = this.translate.instant('gameLabel.Female');
      this.class = "fmly_member female_bg";
      this.image = "assets/images/game_images/girl.png"
    }
    else {
      this.gender = this.translate.instant('gameLabel.Male');
      this.class = "fmly_member men_bg";
      this.image = "assets/images/game_images/fam.png"
    }
    let jobName;
    if (this.mainPerson.job.JobName == this.constantService.JOB_NO_JOB) {
      jobName = "";

    } else {
      if (this.mainPerson.job.JobName == this.constantService.JOB_UNEMPLOYED
        || this.mainPerson.job.JobName == this.constantService.JOB_DOMESTIC_CHORES) {
        if (this.mainPerson.business.Business != this.constantService.BUSINESS_NO_BUSINESS) {
          jobName = this.mainPerson.business.Business;
        }
        else {
          jobName = this.mainPerson.job.Job_displayName;
        }
      }
      else {
        jobName = this.mainPerson.job.Job_displayName;
      }
    }

    let healthProblemSelf = "";
    if (this.self.health_disease != null && !this.self.dead) {
      let keys = Object.keys(this.self.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.self.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemSelf == "") {
            healthProblemSelf = healthProblemSelf + this.self.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemSelf = healthProblemSelf + ", " + this.self.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemSelf === "") {
        healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      if (healthProblemSelf === "") {
        healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    if (this.self.head_of_household) {
      this.moveOut = this.translate.instant('gameLabel.Family_head')
    }
    else {
      this.moveOut = this.translate.instant('gameLabel.moved_out')
    }
    let educationSelf = this.commonService.setEducationForAll(this.self);
    this.level1Arr.push({
      "name": this.self.full_name,
      "age": this.self.age,
      "education": educationSelf,
      "identity": "You",
      "sex": this.gender,
      "image": this.image,
      "class": this.class,
      "jobName": jobName,
      "income": this.self.income.toFixed(2),
      "registerIncome": ((this.self.income / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.self.traits.health,
      "deadClass": this.deadClass,
      "healthProblem": healthProblemSelf,
      "currencyCode": this.self.country.CurrencyPlural,
      "currencyName": this.self.country.currency_code,
      "moveOut": this.moveOut,
      "classForHealth": this.classForhealth,
      "classForHelathStatus": this.classForHealthStatus
    })

    //code for wife
    if (this.allPerson[this.constantService.FAMILY_WIFE] != null) {
      this.wife = this.allPerson[this.constantService.FAMILY_WIFE];
      this.classForHealthStatus = "";
      this.classForhealth = "";
      if (this.wife.traits.helath < 35) {
        this.classForhealth = "border-bottom colourRed1"
      }
      else {
        this.classForhealth = "border-bottom"
      }
      if (this.wife.dead) {
        this.deadClass = "card_disable";
      }
      else {
        this.deadClass = "";
      }
      let healthProblemWife = "";
      if (this.wife.healthDisease != null && !this.wife.dead) {
        let keys = Object.keys(this.wife.health_disease);
        let le = keys.length;
        for (let h = 0; h < keys.length; h++) {
          if (this.wife.health_disease[keys[h]].status === "GOTSICK") {
            if (healthProblemWife == "") {
              healthProblemWife = healthProblemWife + this.wife.health_disease[keys[h]].diseaseName;
            }
            else {
              healthProblemWife = healthProblemWife + ", " + this.wife.health_disease[keys[h]].diseaseName;
            }
          }
        }
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }
      else {
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }
      if (this.wife.living_at_home) {
        this.moveOut = this.translate.instant('gameLabel.moved_out')
      }
      else {
        this.moveOut = "";
      }
      let educationWife = this.commonService.setEducationForAll(this.wife);
      this.jobName = this.setJobOrBusinessnameForOther(this.wife);
      if (this.wife.sex == "F") {
        this.gender = this.translate.instant('gameLabel.Female');
        this.class = "fmly_member female_bg";
        this.image = "assets/images/game_images/girl.png"
      }
      else {
        this.gender = this.translate.instant('gameLabel.Male');
        this.class = "fmly_member men_bg";
        this.image = "assets/images/game_images/fam.png"
      }
      this.level2Arr.push({
        "name": this.wife.full_name,
        "age": this.wife.age,
        "education": educationWife,
        "identity": "Wife",
        "sex": this.gender,
        "image": this.image,
        "class": this.class,
        "jobName": this.jobName,
        "income": this.wife.income.toFixed(2),
        "registerIncome": ((this.wife.income / this.homeService.allPerson[this.constantService.FAMILY_WIFE].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
        "health": this.wife.traits.health,
        "deadClass": this.deadClass,
        "healthProblem": healthProblemWife,
        "currencyCode": this.wife.country.CurrencyPlural,
        "currencyName": this.wife.country.currency_code,
        "moveOut": this.moveOut,
        "classForHealth": this.classForhealth,
        "classForHelathStatus": this.classForHealthStatus
      })


    }


    //code for husband
    if (this.allPerson[this.constantService.FAMILY_HUSBAND] != null) {
      this.husband = this.allPerson[this.constantService.FAMILY_HUSBAND];
      this.classForHealthStatus = "";
      this.classForhealth = "";
      if (this.husband.traits.helath < 35) {
        this.classForhealth = "border-bottom colourRed1"
      }
      else {
        this.classForhealth = "border-bottom"
      }
      if (this.husband.dead) {
        this.deadClass = "card_disable";
      }
      else {
        this.deadClass = "";
      }
      let healthProblemWife = "";
      if (this.husband.healthDisease != null && !this.husband.dead) {
        let keys = Object.keys(this.husband.health_disease);
        let le = keys.length;
        for (let h = 0; h < keys.length; h++) {
          if (this.husband.health_disease[keys[h]].status === "GOTSICK") {
            if (healthProblemWife == "") {
              healthProblemWife = healthProblemWife + this.husband.health_disease[keys[h]].diseaseName;
            }
            else {
              healthProblemWife = healthProblemWife + ", " + this.husband.health_disease[keys[h]].diseaseName;
            }
          }
        }
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }
      else {
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }



      if (this.husband.living_at_home) {
        this.moveOut = this.translate.instant('gameLabel.moved_out')
      }
      else {
        this.moveOut = "";
      }
      let educationHus = this.commonService.setEducationForAll(this.husband);
      this.jobName = this.setJobOrBusinessnameForOther(this.husband);
      if (this.husband.sex == "F") {
        this.gender = this.translate.instant('gameLabel.Female');
        this.class = "fmly_member female_bg";
        this.image = "assets/images/game_images/girl.png"
      }
      else {
        this.gender = this.translate.instant('gameLabel.Male');
        this.class = "fmly_member men_bg";
        this.image = "assets/images/game_images/fam.png"
      }
      this.level2Arr.push({
        "name": this.husband.full_name,
        "age": this.husband.age,
        "education": educationHus,
        "identity": this.translate.instant('gameLabel.Husband'),
        "sex": this.gender,
        "image": this.image,
        "class": this.class,
        "jobName": this.jobName,
        "income": this.husband.income.toFixed(2),
        "registerIncome": ((this.husband.income / this.homeService.allPerson[this.constantService.FAMILY_HUSBAND].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
        "health": this.husband.traits.health,
        "deadClass": this.deadClass,
        "healthProblem": healthProblemWife,
        "currencyCode": this.husband.country.CurrencyPlural,
        "currencyName": this.husband.country.currency_code,
        "moveOut": this.moveOut,
        "classForHealth": this.classForhealth,
        "classForHelathStatus": this.classForHealthStatus
      })
    }

    //code for sibling
    let gendersib
    if (this.mainPerson[this.constantService.FAMILY_SIBLING] != null) {
      this.siblings = this.mainPerson[this.constantService.FAMILY_SIBLING];

      for (var i = 0; i < this.siblings.length; i++) {
        this.sibString = this.mainPerson.siblings[i];
        this.sib = this.allPerson[this.sibString];
        this.classForHealthStatus = "";
        this.classForhealth = "";
        if (this.sib.traits.helath < 35) {
          this.classForhealth = "border-bottom colourRed1"
        }
        else {
          this.classForhealth = "border-bottom"
        }
        if (this.sib.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
        }
        if (this.sib.sex == "F") {
          gendersib = this.translate.instant('gameLabel.Female');
          this.identity = this.translate.instant('gameLabel.Sister');
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          gendersib = this.translate.instant('gameLabel.Male');
          this.identity = this.translate.instant('gameLabel.Brother');
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.sib.health_disease != null && !this.sib.dead) {
          let keys = Object.keys(this.sib.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.sib.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.sib.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.sib.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        if (this.sib.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.sib.job.Job_displayName;

        }
        if (this.sib.living_at_home) {
          this.moveOut = this.translate.instant('gameLabel.moved_out')
        }
        else {
          this.moveOut = "";
        }
        let eduSib = this.commonService.setEducationForAll(this.sib);
        this.jobName = this.setJobOrBusinessnameForOther(this.sib);
        this.level2Arr.push({
          "name": this.sib.full_name,
          "age": this.sib.age,
          "education": eduSib,
          "identity": this.identity,
          "sex": gendersib,
          "class": this.class,
          "image": this.image,
          "jobName": this.jobName,
          "income": this.sib.income.toFixed(2),
          "registerIncome": ((this.sib.income / this.homeService.allPerson[this.sibString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.sib.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.sib.country.CurrencyPlural,
          "currencyName": this.sib.country.currency_code,
          "moveOut": this.moveOut,
          "classForHealth": this.classForhealth,
          "classForHelathStatus": this.classForHealthStatus
        })

      }
    }


    //code for children
    let genderChild;
    if (this.mainPerson[this.constantService.FAMILY_CHILDREN] != null) {
      this.children = this.mainPerson[this.constantService.FAMILY_CHILDREN];

      for (var i = 0; i < this.children.length; i++) {
        this.childString = this.mainPerson.children[i];
        this.child = this.allPerson[this.childString];
        this.classForHealthStatus = "";
        this.classForhealth = "";
        if (this.child.traits.helath < 35) {
          this.classForhealth = "border-bottom colourRed1"
        }
        else {
          this.classForhealth = "border-bottom"
        }
        if (this.child.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
        }
        if (this.child.sex == "F") {
          genderChild = this.translate.instant('gameLabel.Female');
          this.identity = this.translate.instant('gameLabel.Daughter');
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          genderChild = this.translate.instant('gameLabel.Male');
          this.identity = this.translate.instant('gameLabel.Son');
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.child.health_disease != null && !this.child.dead) {
          let keys = Object.keys(this.child.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.child.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.child.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.child.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        if (this.child.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.child.job.Job_displayName;

        }
        if (this.child.living_at_home) {
          this.moveOut = this.translate.instant('gameLabel.moved_out')
        }
        else {
          this.moveOut = "";
        }
        let eduChild = this.commonService.setEducationForAll(this.child);
        this.jobName = this.setJobOrBusinessnameForOther(this.child);
        this.level3Arr.push({
          "name": this.child.full_name,
          "age": this.child.age,
          "education": eduChild,
          "identity": this.identity,
          "sex": genderChild,
          "class": this.class,
          "image": this.image,
          "jobName": this.jobName,
          "income": this.child.income.toFixed(2),
          "registerIncome": ((this.child.income / this.homeService.allPerson[this.childString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.child.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.child.country.CurrencyPlural,
          "currencyName": this.child.country.currency_code,
          "moveOut": this.moveOut,
          "classForHealth": this.classForhealth,
          "classForHelathStatus": this.classForHealthStatus
        })
      }
    }


    //code for grandChildren
    let genderGrandChild;
    if (this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN] != null) {
      this.grandChildren = this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN];

      for (var i = 0; i < this.grandChildren.length; i++) {
        this.grandChildString = this.mainPerson.grand_children[i];
        this.grandChild = this.allPerson[this.grandChildString];
        this.classForHealthStatus = "";
        this.classForhealth = "";
        if (this.grandChild.traits.helath < 35) {
          this.classForhealth = "border-bottom colourRed1"
        }
        else {
          this.classForhealth = "border-bottom"
        }
        if (this.grandChild.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
        }
        if (this.grandChild.sex == "F") {
          genderGrandChild = this.translate.instant('gameLabel.Female');
          this.identity = this.translate.instant('gameLabel.Grand_Daughter');
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          genderGrandChild = this.translate.instant('gameLabel.Male');
          this.identity = this.translate.instant('gameLabel.Grand_Son');
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.grandChild.health_disease != null && !this.grandChild.dead) {
          let keys = Object.keys(this.grandChild.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.grandChild.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.grandChild.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.grandChild.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        if (this.grandChild.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.grandChild.job.Job_displayName;

        }
        if (this.grandChild.living_at_home) {
          this.moveOut = this.translate.instant('gameLabel.moved_out')
        }
        else {
          this.moveOut = "";
        }
        let edugrand = this.commonService.setEducationForAll(this.grandChild);
        this.jobName = this.setJobOrBusinessnameForOther(this.grandChild);
        this.level4Arr.push({
          "name": this.grandChild.full_name,
          "age": this.grandChild.age,
          "education": edugrand,
          "identity": this.identity,
          "sex": genderGrandChild,
          "class": this.class,
          "image": this.image,
          "jobName": this.jobName,
          "income": this.grandChild.income.toFixed(2),
          "registerIncome": ((this.grandChild.income / this.homeService.allPerson[this.grandChildString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.grandChild.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.grandChild.country.CurrencyPlural,
          "currencyName": this.grandChild.country.currency_code,
          "moveOut": this.moveOut,
          "classForHealth": this.classForhealth,
          "classForHelathStatus": this.classForHealthStatus
        })
      }
    }






  }
  clickBack(state) {
    let buttonState;
    let index = this.bornScreenArray.indexOf(state);
    index = index - 1;
    this.state = this.bornScreenArray[index];
    buttonState = this.bornScreenArray[index + 1];
    this.buttonText = this.bornScreenButtonArray[0][buttonState];

    if (state === 'country-register-country-map') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.gotoStateCountryRegisterMap();
    }
    if (state === 'family-tree') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting";
      this.createFamilyTree(this.homeService.allPerson);
    }
    else if (state === 'country-map') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.gotoStateCountryMap();
    }
    else if (state === 'wealth-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting";
      this.getDataForWelthGraph();
    }
    else if (state == 'wealth-grpah-comparision') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.view4 = [600, 200];
      this.getDataForWelthGraph();
      this.getDataForWelthGraphCompariosion()
    }
    else if (state == 'family-amenities') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting";
      this.displayAmenities()
    }
    else if (state === "country-disparities") {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.view = [320, 200];
      this.tabClickDemo();
    }
    else if (state == 'family-income-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
      this.displayFamilyIncomeGraph()

    }
    else if (state === 'eco-status') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting";
      this.calculateTableValues();
    }
    else if (state === 'country-week') {
      this.countryChallengesFormating();
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
    }


  }

  displayFamilyIncomeGraph() {
    let s = "";
    let selfIncome = parseInt("" + (this.mainPerson.expense.householdIncome * 12) / this.mainPerson.country.ExchangeRate);
    let registerIncome = (this.mainPerson.register_country.ProdperCapita * this.mainPerson.register_country.AverageFamilyCount);
    let countryIncome = (this.mainPerson.country.ProdperCapita) * this.mainPerson.country.AverageFamilyCount;
    let value2 = Math.round(this.gameSummeryService.ecoStatusData.chanceOfPoor * 100);
    let value = Math.round((selfIncome / this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members));
    let value3 = (value / 365);
    // this.perdayEarning = value3;
    if (value2 > 0) {
      if (value3 > 2) {
        this.incomeGraphStringFour = this.translate.instant('gameLabel.You_are_not_one_of_them.');
      }
      else {
        this.incomeGraphStringFour = this.translate.instant('gameLabel.You_are_one_of_them.');
      }
    }

    let subString;
    subString = this.translate.instant('gameLabel.a');
    let color = this.commonService.getColurForStatus(this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status);
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 0) {
      this.classForStatusOne = color;
      this.classForStatusBorderOne = "table-bordered-red"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 1) {
      this.classForStatusTwo = color;
      this.classForStatusBorderTwo = "table-bordered-yellow"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 2) {
      this.classForStatusThree = color;
      this.classForStatusBorderThree = "table-bordered-orange"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 3) {
      this.classForStatusFour = color;
      this.classForStatusBorderFour = "table-bordered-blue"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 4) {
      this.classForStatusFive = color;
      this.classForStatusBorderFive = "table-bordered-green"

    }
    this.translate.get('gameLabel.incomeGraphSen1', { subString: subString, ecostatus: this.commonService.getSatusString(this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status) }).subscribe(
      (str: String) => {
        this.incomeGraphStringOne = str;
        this.translate.get('gameLabel.incomeGraphSen3', { value3: value3.toFixed(2) }).subscribe(
          (str: String) => {
            this.incomeGraphStringTwo = str;
            this.translate.get('gameLabel.incomeGraphSen2', { country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, value2: value2.toFixed(0) }).subscribe(
              (str1: String) => {
                this.incomeGraphStringThree = str1;

              })
          })
      })

    //    this.incomeGraphStringOne="You have been born into " +subString+  this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status +" family, " +"earning $"+value3.toFixed(2)+"  per day."
    //     this.incomeGraphStringThree=" The odds of being born as absolutely poor (earning less than $2 per day) in "+this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country + " are " + value2.toFixed(0)+"% ."+s;


    this.multi1 = [
      {
        name: this.translate.instant('gameLabel.Your_family'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(selfIncome))
          }
        ]
      },

      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(countryIncome))
          }
        ]
      },
      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(registerIncome))
          }
        ]
      },
      {
        name: this.translate.instant('gameLabel.World'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(12000))
          }
        ]
      },
    ];
  }
  gotoStateCountryRegisterMap() {
    //for map
    //  this.lat=this.registeredCountry.country.lat;
    //  this.lng=this.mainPerson.country.lng;
    this.latlng = [
      [this.registeredCountry.lat, this.registeredCountry.lng, "assets/images/girl.png"],
      [this.mainPerson.country.lat, this.mainPerson.country.lng, "assets/images/fam.png"]
    ];
    this.start_end_mark.push(this.latlng[0]);
    this.start_end_mark.push(this.latlng[this.latlng.length - 1])
      ;       //   
  }

  gotoStateCountryMap() {
    this.latitude = this.mainPerson.city.latitude
    this.longitude = this.mainPerson.city.longitude
    this.cityName = this.mainPerson.city.cityName;
  }

  checkPopulationFormat(Population) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = (Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }

  availabilityString(amenity) {
    if (amenity == this.active)
      return this.translate.instant('gameLabel.Available');
    else
      return this.translate.instant('gameLabel.Not_Available');

  }



  goToGameSummery() {
    this.modalWindowService.showGameSeetingsFlag = true;
  }

  checkCss(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }


  checkCssForSpecific(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }

  checkCss1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }


  checkCssForSpecific1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }



  getRegisterCountrySdgScore(i) {
    let v
    v = this.regSdgInfo[i].score;
    return v;
  }
  getRegisterCountrySdgColur(i) {
    let v
    v = this.regSdgInfo[i].color;
    return v;
  }
  getNewSdgColor(colour) {
    let classvalue = "";
    if (colour === "green") {
      classvalue = "clr_code bg-success d-inline-block mr-2"
    }
    else if (colour === "red") {
      classvalue = "clr_code bg-red d-inline-block mr-2"
    }
    else if (colour === "orange") {
      classvalue = "clr_code bg-oragne d-inline-block mr-2"
    }
    else if (colour === "yellow") {
      classvalue = "clr_code bg-yellow d-inline-block mr-2"
    }
    else if (colour === "grey") {
      classvalue = "clr_code bg-secondary d-inline-block mr-2"
    }
    return classvalue;
  }
  sdgSelect(sdgId: number) {
    this.bornSdgInfo = [];
    this.regSdgInfo = [];
    this.sdgInfo = this.bornSdgSubGoalDeatails;
    this.values = Object.values(this.sdgInfo)
    for (let i = 0; i < this.values.length; i++) {
      if (this.values[i].SDG_Id === sdgId) {
        this.bornSdgInfo.push(this.values[i]);
      }
    }
    this.valuesR = Object.values(this.regSdgSubGoalDeatails)
    for (let i = 0; i < this.valuesR.length; i++) {
      if (this.valuesR[i].SDG_Id === sdgId) {
        this.regSdgInfo.push(this.valuesR[i]);
      }
    }
    this.sdgImage = "assets/images/sdgicon/sdg" + sdgId + "_" + this.translationService.selectedLang.code + ".png";
    this.sdgDescription = this.sdgDetails[sdgId - 1].SDG_discription;
    this.sdgName = this.sdgDetails[sdgId - 1].SDG_title;
    this.sdgOrigninalName = this.sdgDetails[sdgId - 1].SDG_name;

  }
  markerClick(infowindow, i) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
    if (i == 0) {
      this.labelName = this.registeredCountry.country;
      this.lat = this.registeredCountry.country.lat
      this.lng = this.registeredCountry.country.lng
      this.zoom = 4;
    }
    else if (i == 1) {
      this.labelName = this.mainPerson.country.country;
      this.lat = this.mainPerson.country.lat
      this.lng = this.mainPerson.country.lng
      this.zoom = 5;
    }
  }

  checkColourPatternForGraph(x, y, classIndex) {
    if (classIndex === 0) {
      let val = this.checkCssForSpecific(x, y)
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
    else if (classIndex === 1) {
      let val = this.checkCss(x, y);
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
  }


  generateStringValue(Bi, Ri) {
    let B = parseFloat(Bi);
    let R = parseFloat(Ri);
    if (B < R) {
      this.stringReturnValue = ((100 * ((R - B) / R))).toFixed(2);
      this.valurForHappiAndCorr = (R - B).toFixed(2);
      this.valueForHdi = (100 * (R - B) / R).toFixed(2)
      this.poupulationValue = (((B) / R) * 100).toFixed(2)
    }
    else if (B > R) {
      this.stringReturnValue = (B / R).toFixed(2)
      this.valurForHappiAndCorr = (B - R).toFixed(2);
      this.valueForHdi = ((B / R - 1) * 100).toFixed(2)
      this.poupulationValue = (B / R).toFixed(2);
    }
    else {
      this.stringReturnValue = 0
      this.valurForHappiAndCorr = 0
      this.valueForHdi = 0;
      this.poupulationValue = 0;
    }
    return this.stringReturnValue
  }

  generateStringSubString(B, R) {
    B = parseFloat(B);
    R = parseFloat(R);
    if (B < R) {
      this.stringRetutnText = this.translate.instant('gameLabel.%_less_likely');
      this.bithString = this.translate.instant('gameLabel.%_fewer');
      this.deathString = this.translate.instant('gameLabel.%_less_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.%_less');
      this.happinessString = this.translate.instant('gameLabel.ranks_above');
      this.giniIndexString = this.translate.instant('gameLabel.lower');
      this.sexRatio = this.translate.instant('gameLabel.less');
      this.accesEleString = this.translate.instant('gameLabel.%_less');
      this.populationString = "%"
    }
    else if (B > R) {
      this.stringRetutnText = this.translate.instant('gameLabel.times_more_likely');
      this.bithString = this.translate.instant('gameLabel.times_more');
      this.deathString = this.translate.instant('gameLabel.times_more_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.times_more');
      this.happinessString = this.translate.instant('gameLabel.ranks_below');
      this.giniIndexString = this.translate.instant('gameLabel.higher');
      this.sexRatio = this.translate.instant('gameLabel.more');
      this.accesEleString = this.translate.instant('gameLabel.times_more');
      this.populationString = this.translate.instant('gameLabel.times');

    }
    else {
      this.populationString = this.translate.instant('gameLabel.almost_equal');
      this.stringRetutnText = this.translate.instant('gameLabel.as_good_as');
    }
    return this.stringRetutnText;

  }

  getSexRationValue(B, R) {
    B = parseFloat(B);
    R = parseFloat(R);
    this.sexRatioValue = Math.abs((R - B) * 1000).toFixed(2);
  }

  checkSentenceGenaerateOrNot(B, R) {
    if (B === 0 || R === 0 || B === "NA" || R === "NA") {
      this.senntenceGeneratedFlag = true;
    }
    else {
      this.senntenceGeneratedFlag = false;
    }
  }



  onButtonClickGraphChange(x, y, fieldName, classIndex) {
    if (fieldName === 'Population') {
      this.xAxisLabel = this.translate.instant('gameLabel.Population');
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (this.poupulationValue == 0) {
        this.populationString = this.translate.instant('gameLabel.almost_equal');

        this.translate.get('gameLabel.populationCD1', { country: this.mainPerson.country.country, populationString: this.populationString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // Population of {{country}} is {{populationString}} that of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.mainPerson.country.country+" is "+this.populationString+" that of "+this.mainPerson.register_country.country+"'s population.";
      }
      else {
        this.translate.get('gameLabel.populationCD2', { country: this.mainPerson.country.country, poupulationValue: this.poupulationValue, populationString: this.populationString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        //     Population of {{country}} is {{poupulationValue}} {{populationString}}  of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.mainPerson.country.country+" is "+this.poupulationValue+this.populationString+" of "+this.mainPerson.register_country.country+"'s population.";
      }
      this.colorScheme = {
        domain: ['#00FFFF', '#00FFFF']
      };
    }
    else if (fieldName === "Sex Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Sex_Ratio');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.getSexRationValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.sexRatioValue) === 0) {
          this.sexRatio = this.translate.instant('gameLabel.as_good_as')
          this.translate.get('gameLabel.sexRationDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If  "+this.mainPerson.country.country+" is your home country, at birth, there will be equal, boys per 1000 girls, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.sexRationDC2', { country: this.mainPerson.country.country, sexRatioValue: Math.round(this.sexRatioValue), sexRatio: this.sexRatio, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, at birth, there will be , {{sexRatioValue}} {{sexRatio}} boys per 1000 girls than {{rcountry}}.
          // this.sentenceTextWindow1="If  "+this.mainPerson.country.country+" is your home country, at birth, there will be , "+ Math.round(this.sexRatioValue) +" "+this.sexRatio+" boys per 1000 girls than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Birth Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Birth_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.BirthRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have equal babies, compared to " +this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.BirthRateDC2', { country: this.mainPerson.country.country, value: value, bithString: this.bithString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have "+ value + this.bithString+ " babies than " +this.mainPerson.register_country.country +".";
      }
      this.checkColourPatternForGraph(x, y, classIndex);

    }
    else if (fieldName === "Death Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Death_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.DeathRatioDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have equal risk of dying, compared to {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have equal risk of dying, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.DeathRatioDC2', { country: this.mainPerson.country.country, value: value, deathString: this.deathString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{deathString}} of dying than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have "+ value + this.deathString+ " of dying than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Infant Mortality Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Infant_Mortality_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.InfantMortalityRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are equally likely to die in infancy, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.InfantMortalityRateDC2', { country: this.mainPerson.country.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you are {{value}} {{stringRetutnText}} to die in infancy than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are "+ value + this.stringRetutnText+ " to die in infancy than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Primary School") {
      this.xAxisLabel = this.translate.instant('gameLabel.Primary_School_Enrollment');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.primarySchoolDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have equal access to Primary School, compared to " +this.mainPerson.register_country.country +".";
        } else {
          this.translate.get('gameLabel.primarySchoolDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{primarySchoolString}} access to Primary School than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have "+ value +this.primarySchoolString+ " access to Primary School than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "UnEmployment Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Unemployment_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.UnEmploymentRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are equally likely to be unemployed, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.UnEmploymentRateDC2', { country: this.mainPerson.country.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // "If {{country}} is your home country, you will spend {{value}} {{primarySchoolString}} money on health care than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are "+ value +this.stringRetutnText+ " to be unemployed than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Health Exp. per capita") {
      this.xAxisLabel = this.translate.instant('gameLabel.Health_Exp_per_capita');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.healthExpDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will spend equal money on health care, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.healthExpDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will spend "+ value +this.primarySchoolString+ " money on health care than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "PPP") {
      this.xAxisLabel = this.translate.instant('gameLabel.PPP');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {

          this.translate.get('gameLabel.pppDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will earn equal money, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.pppDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will earn "+ value +this.primarySchoolString+ " money than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Happiness Score") {
      this.xAxisLabel = this.translate.instant('gameLabel.Happiness_Rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.HappinessScoreDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" is almost equal in happiness index, compared to "+this.mainPerson.register_country.country ;
        } else {
          this.translate.get('gameLabel.HappinessScoreDC2', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in happiness index.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Corruption") {
      this.xAxisLabel = this.translate.instant('gameLabel.Corruption');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.CorruptionDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" is equal in the list of the most corrupt countries, compared to "+this.mainPerson.register_country.country;
        }
        else {
          this.translate.get('gameLabel.CorruptionDC2', { rcountry: this.mainPerson.register_country.country, country: this.mainPerson.country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the list of the most corrupt countries.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Sdgi Rank") {
      this.xAxisLabel = this.translate.instant('gameLabel.SDG_rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {

          this.translate.get('gameLabel.SdgiScoreDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1=this.mainPerson.country.country+" is equal in the SDG rank, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.SdgiScoreDC2', { country: this.mainPerson.country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the SDG rank than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Hdi") {
      this.xAxisLabel = this.translate.instant('gameLabel.HDI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valueForHdi) === 0) {
          this.translate.get('gameLabel.hdiDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has  equal HDI, compared to "+ this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.hdiDC2', { country: this.mainPerson.country.country, valueForHdi: this.valueForHdi, giniIndexString: this.giniIndexString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has  "+ this.valueForHdi +"% "+this.giniIndexString+ "  HDI than "+ this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Gini") {
      this.xAxisLabel = this.translate.instant('gameLabel.GINI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if ((x - y) === 0 || (y - x) === 0) {
          this.translate.get('gameLabel.giniDc1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has equal income inequality among rich and poor.";
        }
        else {
          this.translate.get('gameLabel.giniDc2', { country: this.mainPerson.country.country, giniIndexString: this.giniIndexString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has  "+ this.giniIndexString + " income inequality among rich and poor.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Access to Electricity") {
      this.xAxisLabel = this.translate.instant('gameLabel.Electricity_Access');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.electricityDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.mainPerson.country.country+" you will access equal electricity, compared to "+ this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.electricityDC2', { country: this.mainPerson.country.country, value: value, accesEleString: this.accesEleString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.mainPerson.country.country+" you will have  "+ value +this.accesEleString+ " access to electricity than " +this.mainPerson.register_country.country+".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else {
      this.sentenceTextWindow1 = "N/A";
      this.checkColourPatternForGraph(x, y, classIndex);
    }

    this.single = [
      {
        name: this.mainPerson.country.country,
        value: x,
      },
      {
        name: this.registeredCountry.country,
        value: y,
      },
    ];
    Object.assign(this.single);
  }

  tabClickDemo() {
    this.activeClassForDemo = "nav-item nav-link active"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = true;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.mainPerson.country.Population, this.registeredCountry.Population, 'Population', 3)
    this.single = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.Population,
      },
      {
        name: this.registeredCountry.country,
        value: this.registeredCountry.Population,
      },
    ];
    Object.assign(this.single);

  }
  tabClickperCapita() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link active";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = true;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.mainPerson.country.PrimarySchool, this.registeredCountry.PrimarySchool, 'Primary School', 1);
    this.single = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.PrimarySchool,
      },
      {
        name: this.registeredCountry.country,
        value: this.registeredCountry.PrimarySchool,
      },
    ];
    Object.assign(this.single);
  }
  tabClickLFO() {
    let num1 = this.mainPerson.country.Agriculture;
    let num2 = this.mainPerson.country.Services;
    let num3 = this.mainPerson.country.industry;
    let num4 = this.mainPerson.register_country.Agriculture;
    let num5 = this.mainPerson.register_country.Services;
    let num6 = this.mainPerson.register_country.industry;
    let largest;
    let occupation;
    let rlargest;
    let roccupation;
    if (num1 >= num2 && num1 >= num3) {
      largest = num1.toFixed(2);
      occupation = "Agriculture"
    }
    else if (num2 >= num1 && num2 >= num3) {
      largest = num2.toFixed(2);
      occupation = "Service"
    }
    else {
      largest = num3.toFixed(2);
      occupation = "Industry"
    }

    if (num4 >= num5 && num4 >= num6) {
      largest = num1.toFixed(2);
      rlargest = num4.toFixed(2);
      roccupation = "Agriculture"
    }
    else if (num5 >= num4 && num5 >= num6) {
      rlargest = num5.toFixed(2);
      largest = num2.toFixed(2);
      roccupation = "Service"
    }
    else {
      rlargest = num6.toFixed(2);
      largest = num3.toFixed(2);
      roccupation = "Industry"
    }
    this.labourForceString1 = "In " + this.mainPerson.country.country + ", most likely your occupation will be in the " + occupation + " Sector. "
    this.labourForceString2 = "If you are working in " + roccupation + " Sector in " + this.mainPerson.register_country.country + ", you will have " + largest + "% jobs available in " + this.mainPerson.country.country + "."
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link active";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = true;
    this.onClickIndexRating = false;
    this.xAxisLabel = 'Occupation';
    this.colorScheme = {
      domain: ['#5AA454', '#A10A28']
    };
    this.multi = [
      {
        name: this.mainPerson.country.country,
        series: [
          {
            name: 'Agriculture',
            value: this.mainPerson.country.Agriculture,
          },
          {
            name: 'Service',
            value: this.mainPerson.country.Services,
          },
          {
            name: 'Industry',
            value: this.mainPerson.country.industry,
          },
        ],
      },

      {
        name: this.registeredCountry.country,
        series: [
          {
            name: 'Agriculture',
            value: this.registeredCountry.Agriculture,
          },
          {
            name: 'Service',
            value: this.registeredCountry.Services,
          },
          {
            name: 'Industry',
            value: this.registeredCountry.industry,
          },
        ],
      },
    ];
    Object.assign(this.multi);

  }
  tabClickIndex() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link active";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickSdgData = false;
    this.onClickIndexRating = true;
    this.onButtonClickGraphChange(this.mainPerson.country.ppp, this.registeredCountry.ppp, 'PPP', 1);
    this.single = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.ppp,
      },
      {
        name: this.registeredCountry.country,
        value: this.registeredCountry.ppp,
      },
    ];
    Object.assign(this.single);


  }
  tabClickSdg() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link active "
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onClickSdgData = true;
    this.gameSummeryService.getCountryGroupListForBornAndRegCountry(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid, this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid, this.translationService.selectedLang.code).
      subscribe(
        res => {
          let bornCountry = res['bornCountry']
          let registerCountry = res['registerCountry']
          this.bornCountryGroupList = Object.values(bornCountry)
          this.registerCountryGroupList = Object.values(registerCountry)
          let result = this.bornCountryGroupList.filter(o => this.registerCountryGroupList.some(({ gpName }) => o.gpName === gpName));
          let total = this.bornCountryGroupList.length + this.registerCountryGroupList.length;
          let unique = total - result.length;
          this.groupIndex = (1 - unique / total) * 2;
          this.groupIndex = (this.groupIndex * 100).toFixed(2);
          if (result.length > 0) {
            for (let j = 0; j < result.length; j++) {
              for (let i = 0; i < this.bornCountryGroupList.length; i++) {
                if (this.bornCountryGroupList[i].gpName === result[j].gpName) {
                  this.bornCountryGroupList[i].class = "countryGroupCommon";
                }
              }
              for (let k = 0; k < this.registerCountryGroupList.length; k++) {
                if (this.registerCountryGroupList[k].gpName === result[j].gpName) {
                  this.registerCountryGroupList[k].class = "countryGroupCommon";
                }
              }
            }
          }
        })
  }
  openFullscreen() {
    this.commonService.fullScreen();
  }

  closeFullscreen() {
    this.commonService.removeFullScreen();

  }
  onSelect(data): void {
    // console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    // console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    // console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }


  getDataForWelthGraph() {
    this.view = [700, 500];
    this.multi2 = [];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabel = "Population(%)";
    this.showYAxisLabel = true;
    this.yAxisLabel = "Wealth";
    this.colorScheme = {
      domain: ['#0000FF', '#A10A28','#FFA500']
    };
    this.multi2.push({
      name: "Equality Line",
      series: []
    });
    this.multi2.push({
      name: this.mainPerson.country.country,
      series: []
    });
    this.multi2.push({
      name: this.mainPerson.register_country.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi2[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.bornCountryArray.length; i++) {
      if (this.bornCountryArray[i].name !== "99%") {
        this.multi2[1].series.push(this.bornCountryArray[i]);
      }
    }
    for (let i = 0; i < this.registerCountryArray.length; i++) {
      if (this.registerCountryArray[i].name !== "99%") {
        this.multi2[2].series.push(this.registerCountryArray[i]);
      }
    }
    Object.assign(this.multi2);

    this.multi4 = [
      {
        "name": this.translate.instant('gameLabel.Extremely_poor'),
        "value": this.bornCountryWelathMetaData.per20 * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.poor'),
        "value": (this.bornCountryWelathMetaData.per50 - this.bornCountryWelathMetaData.per20) * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Middle_income'),
        "value": (this.bornCountryWelathMetaData.per90 - this.bornCountryWelathMetaData.per50) * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Rich'),
        "value": (this.bornCountryWelathMetaData.per99 - this.bornCountryWelathMetaData.per90) * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Super_rich'),
        "value": (this.bornCountryWelathMetaData.per100 - this.bornCountryWelathMetaData.per99) * this.bornCountryWelathMetaData.wealth
      }
    ];
    Object.assign(this.multi4);
    let v2, v3, v4, g3: any, g4, g5: any;
    g3 = (this.bornCountryWelathMetaData.per50 * 100).toFixed(1);
    g4 = ((this.bornCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
    g5 = ((this.bornCountryWelathMetaData.per100 * 100) - (this.bornCountryWelathMetaData.per99 * 100)).toFixed(1);
    // v2=(((this.mainPerson.country.Population*(50/100)/1000000))).toFixed(2);
    // v3=((this.mainPerson.country.Population*(1/100))/1000000).toFixed(2)
    v2 = (((this.mainPerson.country.Population * (50 / 100)))).toFixed(2);
    v3 = ((this.mainPerson.country.Population * (1 / 100))).toFixed(2)
    v4 = (g5 / g3).toFixed(2);
    v2 = this.commonService.checkNumberFormat(v2)
    v3 = this.commonService.checkNumberFormat(v3)
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn3 = s;
      })
    this.finalStatement = this.wealthGraphSentenceBorn1 + '<br/>' + this.wealthGraphSentenceBorn2 + '<br/>' + this.wealthGraphSentenceBorn3

    // this.wealthGraphSentenceBorn1="Botom 50% population (approximately " +v2+" MN people) owns only "+g3+"% of national wealth";
    // this.wealthGraphSentenceBorn2="Top 1% of population  (approximately "+v3+" MN people) owns "+g5+ "% of national wealth";
    // this.wealthGraphSentenceBorn3="In others word top 1% population share in national wealth is "+v4+" times that of share of national wealth of bottom 50% population";
  }

  getDataForWelthGraphCompariosion() {
    this.multi3 = [];
    this.view = [700, 400];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabel = this.translate.instant('gameLabel.Population(%)');
    this.showYAxisLabel = true;
    this.yAxisLabel = this.translate.instant('gameLabel.Wealth');
    this.colorScheme = {
      domain: ['#0000FF', '#A10A28']
    };

    this.multi3.push({
      name: this.translate.instant('gameLabel.World'),
      series: []
    });
    this.multi3.push({
      name: this.registeredCountry.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi3[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.registerCountryArray.length; i++) {
      if (this.registerCountryArray[i].name !== "99%") {
        this.multi3[1].series.push(this.registerCountryArray[i]);
      }
    }
    Object.assign(this.multi3);

    this.multi5 = [
      {
        "name": this.translate.instant('gameLabel.Extremely_poor'),
        "value": this.registerCountryWelathMetaData.per20 * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.poor'),
        "value": (this.registerCountryWelathMetaData.per50 - this.registerCountryWelathMetaData.per20) * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Middle_income'),
        "value": (this.registerCountryWelathMetaData.per90 - this.registerCountryWelathMetaData.per50) * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Rich'),
        "value": (this.registerCountryWelathMetaData.per99 - this.registerCountryWelathMetaData.per90) * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Super_rich'),
        "value": (this.registerCountryWelathMetaData.per100 - this.registerCountryWelathMetaData.per99) * this.registerCountryWelathMetaData.wealth
      }
    ];

    Object.assign(this.multi5);
    let v2, v3, v4, g3: any, g4, g5: any;
    g3 = (this.registerCountryWelathMetaData.per50 * 100).toFixed(1);
    g4 = ((this.registerCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
    g5 = ((this.registerCountryWelathMetaData.per100 * 100) - (this.registerCountryWelathMetaData.per99 * 100)).toFixed(1);
    // v2=(((this.registeredCountry.Population*(50/100)/1000000))).toFixed(2);
    // v3=((this.registeredCountry.Population*(1/100))/1000000).toFixed(2)
    v2 = (((this.registeredCountry.Population * (50 / 100)))).toFixed(2);
    v3 = ((this.registeredCountry.Population * (1 / 100))).toFixed(2)
    v4 = (g5 / g3).toFixed(2);
    v2 = this.commonService.checkNumberFormat(v2)
    v3 = this.commonService.checkNumberFormat(v3)
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg3 = s;
      })
    this.finalStatementReg = this.wealthGraphSentenceReg1 + '<br/>' + this.wealthGraphSentenceReg2 + '<br/>' + this.wealthGraphSentenceReg3

    // this.wealthGraphSentenceReg1="Botom 50% population (approximately " +v2+" MN people) owns only "+g3+"% of national wealth";
    // this.wealthGraphSentenceReg2="Top 1% of population  (approximately "+v3+" MN people) owns "+g5+ "% of national wealth";
    // this.wealthGraphSentenceReg3="In other word top 1% population share in national wealth is "+v4+" times that of share of national wealth of bottom 50% population";
  }

  yAxisTickFormatting(value) {
    return this.percentTickFormatting(value);
  }

  percentTickFormatting(val: any): string {
    return val.toLocaleString() + '%';
  }

  generateArrayForWelathGraph() {
    if (this.bornCountryWelathMetaData !== false && this.registerCountryWelathMetaData !== false) {
      this.bornCountryArray = [
        { 'name': '10%', 'value': this.bornCountryWelathMetaData.per10 },
        { 'name': '20%', 'value': this.bornCountryWelathMetaData.per20 },
        { 'name': '30%', 'value': this.bornCountryWelathMetaData.per30 },
        { 'name': '40%', 'value': this.bornCountryWelathMetaData.per40 },
        { 'name': '50%', 'value': this.bornCountryWelathMetaData.per40 },
        { 'name': '60%', 'value': this.bornCountryWelathMetaData.per60 },
        { 'name': '70%', 'value': this.bornCountryWelathMetaData.per70 },
        { 'name': '80%', 'value': this.bornCountryWelathMetaData.per80 },
        { 'name': '90%', 'value': this.bornCountryWelathMetaData.per90 },
        { 'name': '99%', 'value': this.bornCountryWelathMetaData.per99 },
        { 'name': '100%', 'value': this.bornCountryWelathMetaData.per100 },
      ]
      this.registerCountryArray = [
        { 'name': '10%', 'value': this.registerCountryWelathMetaData.per10 },
        { 'name': '20%', 'value': this.registerCountryWelathMetaData.per20 },
        { 'name': '30%', 'value': this.registerCountryWelathMetaData.per30 },
        { 'name': '40%', 'value': this.registerCountryWelathMetaData.per40 },
        { 'name': '50%', 'value': this.registerCountryWelathMetaData.per40 },
        { 'name': '60%', 'value': this.registerCountryWelathMetaData.per60 },
        { 'name': '70%', 'value': this.registerCountryWelathMetaData.per70 },
        { 'name': '80%', 'value': this.registerCountryWelathMetaData.per80 },
        { 'name': '90%', 'value': this.registerCountryWelathMetaData.per90 },
        { 'name': '99%', 'value': this.registerCountryWelathMetaData.per99 },
        { 'name': '100%', 'value': this.registerCountryWelathMetaData.per100 },
      ]

    }
    else {
      this.welthGraphDataNotGenrateFlag = true;
    }

  }
  goBackButton() {
    this.router.navigate(['/summary']);
  }
  closeTab() {
    this.activeClassForDemo = "nav-item nav-link "
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
  }

  sendMail() {
    let obituaryData = {}
    obituaryData['obituary'] = this.bornSummary;
    this.postGameService.senfObituaryMail(obituaryData, 'summary').subscribe(
      res => {
        if (res) {
          this.messageText = this.translate.instant('gameLabel.Send_mail_succesfully');
          this.successMsgFlag = true;
        } else {
          this.messageText = this.translate.instant('gameLabel.email_not_found');
          this.successMsgFlag = true;
        }
      }
    )
  }
  successMsgWindowClose() {
    this.successMsgFlag = false;
  }

  sendmailToFriend() {
    this.friendMailShowFlag = true;
    this.emailId = '';
  }

  send() {
    this.friendMailShowFlag = false;
    let obituaryData = {}
    obituaryData['emailId'] = this.emailId;
    obituaryData['obituary'] = this.bornSummary;
    this.postGameService.senfObituaryMail(obituaryData, 'summary').subscribe(
      res => {
        if (res) {
          this.messageText = this.translate.instant('gameLabel.Send_mail_succesfully');
          this.successMsgFlag = true;
        } else {
          this.messageText = this.translate.instant('gameLabel.email_not_found');
          this.successMsgFlag = true;
        }
      })
  }

  closeMail() {
    this.friendMailShowFlag = false;
    this.emailId = '';
  }


  updateBornSummaryArray() {
    if (!this.mainPerson.game_settings.Screen5) {
      let index = this.bornScreenArray.indexOf("country-map");
      this.bornScreenArray.splice(index, 1)
    }
    if (!this.mainPerson.game_settings.Screen6) {
      let index = this.bornScreenArray.indexOf("wealth-grpah");
      this.bornScreenArray.splice(index, 1)
    }

    if (!this.mainPerson.game_settings.Screen7) {
      let index = this.bornScreenArray.indexOf("wealth-grpah-comparision");
      this.bornScreenArray.splice(index, 1)
    }

    if (!this.mainPerson.game_settings.Screen8) {
      let index = this.bornScreenArray.indexOf("country-disparities");
      this.bornScreenArray.splice(index, 1)
    }

    if (!this.mainPerson.game_settings.Screen9) {
      let index = this.bornScreenArray.indexOf("country-SDG");
      this.bornScreenArray.splice(index, 1)
    }

  }


  openIframeWindow(link, title) {
    let title1 = "'" + title + "'";
    this.translate.get('gameLabel.country_group_title', { gName: title1 }).subscribe(
      (str) => {
        this.countryGroupTitle = str;
      })
    this.countryGroupIframeFlag = true;
    this.webLink = link;
  }

  closeIframWindow() {
    this.countryGroupIframeFlag = false;

  }
  knowMore() {
    this.modalWindowService.showKonwMoreFlag = true;
  }

  moreInfo() {
    this.moreInfoFlag = true;
    let s1 = this.translate.instant('gameLabel.status_stm_tlp1');
    let s2 = this.translate.instant('gameLabel.status_stm_tlp2');
    let s3 = this.translate.instant('gameLabel.status_stm_tlp3');
    let s4 = this.translate.instant('gameLabel.status_stm_tlp4');
    let s5 = this.translate.instant('gameLabel.status_stm_tlp5');
    this.moreInfoTitle = s1 + "<br><br>"
    this.moreInfoStm = s2 + "<br><br>" + s3 + "<br><br>" + s4 + "<br><br>" + s5;
  }

  calculateTableValues() {
    this.classForStatusBorderOne = "newTableFontSize";
    this.classForStatusBorderTwo = "newTableFontSize";
    this.classForStatusBorderThree = "newTableFontSize";
    this.classForStatusBorderFour = "newTableFontSize";
    this.classForStatusBorderFive = "newTableFontSize";

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country.length > 8) {
      this.bornCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Code;
    }
    else {
      this.bornCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country.length > 8) {
      this.registerCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.Code;
    }
    else {
      this.registerCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    }
    this.chanceOfPoorR = this.registerStatusData.chanceOfPoor * 100;
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    let firstR = (f3R);
    this.firstR = firstR;
    let secondR = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    let thirdR = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    let fourthR = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    this.rOne = (firstR + secondR) / (firstR);
    this.rTwo = (secondR + thirdR) / (firstR);
    this.rThree = (thirdR + fourthR) / (firstR);
    this.rFour = (fourthR) / (firstR);

    //value for born
    let first = this.commonService.firstValueI;
    let second = this.commonService.secondValueI;
    let third = this.commonService.thirdValueI;
    let fourth = this.commonService.fourthValueI;
    this.bOne = (first + second) / (first);
    this.bTwo = (second + third) / (first);
    this.bThree = (third + fourth) / (first);
    this.bFour = (fourth) / (first);

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 0) {
      this.classForStatusBorderOne = "table-bordered-red newTableFontSize"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 1) {
      this.classForStatusBorderTwo = "table-bordered-yellow newTableFontSize"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 2) {
      this.classForStatusBorderThree = "table-bordered-orange newTableFontSize"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 3) {
      this.classForStatusBorderFour = "table-bordered-blue newTableFontSize"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 4) {
      this.classForStatusBorderFive = "table-bordered-green newTableFontSize"
    }

  }
  successWindowClose() {
    this.moreInfoFlag = false;
  }

  setJobOrBusinessnameForOther(person) {
    this.jobName = '';
    if (person.business.hasOwnProperty('businessesId')) {
      this.jobName = person.business['Business'] + " business";
    }
    else {
      if (person.job.JobName == this.constantService.JOB_NO_JOB) {
        this.jobName = "";

      } else {
        this.jobName = person.job.Job_displayName;

      }
    }
    return this.jobName

  }

  countryChallengesFormating() {
    if (this.countryChallengesArray.length === 0) {
      this.spliteString(this.mainPerson.country.moreInfo1)
      this.spliteString(this.mainPerson.country.moreInfo2)
      this.spliteString(this.mainPerson.country.moreInfo3)
      this.spliteString(this.mainPerson.country.moreInfo4)
      if(this.mainPerson.country.hasOwnProperty('moreInfo5')){
        this.spliteString(this.mainPerson.country.moreInfo5)
        }    }
  }
  spliteString(str: String) {
    this.challengeHeading = '';
    this.challengeInfo = '';
    const dotPosition = str.indexOf(":");
    this.challengeHeading = str.substring(0, dotPosition);
    this.challengeInfo = str.substring(dotPosition + 1);
    this.countryChallengesArray.push({
      'title': this.challengeHeading,
      "text": this.challengeInfo
    })
  }

}
