import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-create-life-with-character-design',
  templateUrl: './create-life-with-character-design.component.html',
  styleUrls: ['./create-life-with-character-design.component.css']
})
export class CreateLifeWithCharacterDesignComponent implements OnInit {
  
  lastNameDummy=[];
  showCountryListFlag;
  continentList=[];
  searchTextCity;
  showCityListFlag;
  showReligionListFlag;
  showNameListFlag;
  countryList;
  countryRadioButtonValue;
  cityRadioButtonValue;
  selectedCountryObject;
  cityList;
  state = 'DesignALife'
  selectedContinent
  selectedCityObject;
  saveCountryObject;
  saveCityObject;
  religionList;
  designALifeObject = {};
  religionButtonValue;
  selectedReligionObject;
  saveReligionobject;
  selectedGender = "M";
  area = "U";
  country;
  attribute;
  nameSelectedValue;
  databaseNameSelectionFlag = "tab-pane active"
  userDefinedNameSelectionFlag = "tab-pane"
  firstName = "";
  lastName = "";
  firstNameList;
  lastNameList;
  firstNameUser = "";
  lastNameUser = ""; femaleFirstNameList
  serDefinedNameSelectionFlag;
  nameGroupId;
  disableFlag;
  gameId;
  disableButton = true;
  openSge;
  openChar;
  religion_url;
  moreInfoTitle;
  selectedLanguage: String;
  public messageText;
  public messageClass;
  public MessageShow;
  public messageType;
  public msgText;
  showGenderTabClass = "";
  showAreaTabClass = "";
  showNameTabClass = "";
  showAttributeTabClass = "";
  noLastNameFlag: boolean = false;
  selectCityString;
  warrningFlag: boolean = false;
  showSuccessMsgFlagLoadLife: boolean = false;
  currentAttributeValue = 0;
  continentUrl="https://en.wikipedia.org/wiki/Continent"
  //sdg

  sdgInfo
  sdgDetails
  sdgImage
  sdgName
  sdgTitle
  sdgDescription
  sdgTargetCode
  modelWindowShowHideService
  sdgGreenCountryList
  sdgYellowCountryList
  sdgGrayCountryList
  sdgRedCountryList
  grey;
  CountryDesignALife;
  disabledButton: boolean = false;
  passFromSgdCountryObject;
  lat: number;
  lng: number;
  markers = [];
  mapZoom = 1.5;
  refresh: boolean;
  disableCityButton: boolean = false;
  showSuccessMsgFlag: boolean = false;
  searchText;
  moreInforStm;
  constructor(public modalWindowService: modalWindowShowHide,
    public gameSummeryService: GameSummeryService,
    private commonService: CommonService,
    private router: Router,
    private translationService: TranslationService,
    private translate: TranslateService,
    public homeService: HomeService,
    public constantService: ConstantService) { }
  ngOnInit(): void {
    this.modalWindowService.showcharacterDesignFlag = true;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.attribute = [
      {
        "attributeName": this.translate.instant('gameLabel.Artistic'),
        value: Math.round(this.commonService.getRandomValue(0, 100))
      },
      {
        "attributeName": this.translate.instant('gameLabel.Happiness'),
        value: Math.round(this.commonService.getRandomValue(0, 100))
      },

      {
        "attributeName": this.translate.instant('gameLabel.Intelligence'),
        value: Math.round(this.commonService.getRandomValue(0, 100))
      },
      {
        "attributeName": this.translate.instant('gameLabel.Musical'),
        value: Math.round(this.commonService.getRandomValue(0, 100))
      },
      {
        "attributeName": this.translate.instant('gameLabel.Athletic'),
        value: Math.round(this.commonService.getRandomValue(0, 100))
      },
      {
        "attributeName": this.translate.instant('gameLabel.Strength'),
        value: Math.round(this.commonService.getRandomValue(0, 100))
      },
      {
        "attributeName": this.translate.instant('gameLabel.Physical_Endurance'),
        value: Math.round(this.commonService.getRandomValue(0, 100))
      }

    ];

    if (!this.modalWindowService.showSdg && !this.modalWindowService.showGroupCountry) {
      this.selectClick(true, 'continent');
    }
    else if (this.modalWindowService.showSdg) {
      this.selectClick(false, 'city');
    }
    else if (this.modalWindowService.showGroupCountry) {
      this.selectClick(false, 'city');
      // this.modalWindowService.showGroupCountry=false;
    }

  }


  selectClick(flag, stateValue) {
    this.modalWindowService.showcharacterDesignFlag = true;
    this.refresh = true;
    this.state = stateValue;
    this.markers = [];
    if(this.state==='continent'){
     this.gameSummeryService.getCotinentList().subscribe(
      res=>{
     this.continentList=res
      }
     )
    }
    else if (this.state === 'country') {
      this.disableButton = true;
      this.gameSummeryService.getCountryListByContinenet(this.selectedContinent).subscribe(country => {
        this.country = country;
        this.countryList = JSON.parse(JSON.stringify(this.country));
        this.fillMarkerCountry();
      })
    }
    if (this.state === 'city') {
      this.disableButton = true;
      if (flag) {
        this.saveCountryObject = this.selectedCountryObject;
        this.designALifeObject["onlyCountry"] = false
        this.designALifeObject["country"] = this.saveCountryObject;
        this.gameSummeryService.getCity(this.selectedCountryObject.countryid).subscribe(city => {
          this.cityList = city;
          this.fillMarkerCity();
          this.translate.get('gameLabel.Select_a_city_in', { country: this.selectedCountryObject.country }).subscribe(
            (str) => {
              this.selectCityString = str;
            })


        })

      }
      else {
        this.disableButton = true;
        this.modalWindowService.showchearacterDesignSdgFlag = false;
        if (this.modalWindowService.showSdg) {
          this.selectedCountryObject = this.modalWindowService.sdgCountrySelectedObject;
        }
        else if (this.modalWindowService.showGroupCountry) {
          this.selectedCountryObject = this.modalWindowService.groupIdSelectedCountryObject;
        }
        this.saveCountryObject = this.selectedCountryObject;
        this.designALifeObject["onlyCountry"] = false
        this.designALifeObject["country"] = this.saveCountryObject;
        this.gameSummeryService.getCity(this.selectedCountryObject.countryid).subscribe(city => {
          this.cityList = city;
          this.fillMarkerCity();
        })

      }
    }
    else if (this.state == 'country-SDG') {
    }
    else if (this.state === 'religion') {
      this.disableButton = true;
      this.designALifeObject['city'] = this.selectedCityObject;
      if (this.saveCountryObject.countryid == 74) {
        //call indian
        this.gameSummeryService.getAllReligion(this.selectedCityObject.Countryregionid).subscribe(religion => {
          this.religionList = religion;
          this.religion_url = this.religionList[0].religion_url;
        })
      }
      else {
        this.gameSummeryService.getReligionForWorld(this.saveCountryObject.countryid, this.translationService.selectedLang.code).subscribe(religion => {
          this.religionList = religion;
          this.religion_url = this.religionList[0].religion_url;

        })

      }


    }
    else if (this.state === 'name') {
      this.showGenderTabClass = "Active"
      this.selectedReligionObject = this.religionList[this.religionButtonValue];
      this.designALifeObject["religion"] = this.selectedReligionObject;
      if (this.selectedCountryObject.countryid == 74) {
        this.gameSummeryService.getIndianNameGroupId(this.selectedReligionObject.religionid).subscribe(
          res => {
            this.nameGroupId = res;
            this.gameSummeryService.getFirstNameList(this.saveCountryObject.countryid, "M", "F", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
              res => {
                this.firstNameList = res;
                this.gameSummeryService.getLastNameList(this.saveCountryObject.countryid, "N", "L", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
                  lastname => {
                    this.lastNameDummy=lastname;
                    this.lastNameList = lastname;
                    if (lastname.length === 0) {
                      this.noLastNameFlag = true;
                    }
                  })
              })
          })
      }
      else {
        this.gameSummeryService.getNameGroupId(this.selectedReligionObject.religionid).subscribe(
          res => {
            this.nameGroupId = res;
            this.gameSummeryService.getFirstNameList(this.saveCountryObject.countryid, "M", "F", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
              res => {
                this.firstNameList = res;
                this.gameSummeryService.getLastNameList(this.saveCountryObject.countryid, "N", "L", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
                  lastname => {
                    this.lastNameDummy=lastname;
                    this.lastNameList = lastname;
                    if (lastname.length === 0) {
                      this.noLastNameFlag = true;
                    }
                  })
              })
          })
      }


    }
  }

  allRadioButtonValue(event: any) {
    if(event.target.name == "continenet"){
      this.disableButton=false
     this.selectedContinent=event.target.value;
     this.continentUrl="https://en.wikipedia.org/wiki/"+this.selectedContinent;
    }
    else if (event.target.name == "country_option") {
      this.countryRadioButtonValue = event.target.value;
      this.countryRadioButtonValue = parseInt(this.countryRadioButtonValue)
      this.markers = [];
      this.mapZoom = 1.5;
      for (let i = 0; i < this.countryList.length; i++) {
        if (this.countryRadioButtonValue === this.countryList[i].countryid) {
          this.markers.push({
            lat: this.countryList[i].lat,
            lng: this.countryList[i].lng,
            label: this.countryList[i].country,
            code: this.countryList[i].Code
          })
          this.selectedCountryObject = this.countryList[i];
          this.countryMarkerClick(this.markers[0]);
          this.disableButton = false;
          break;
        }
      }


    }
    else if (event.target.name == "city_option") {
      this.cityRadioButtonValue = event.target.value;
      this.cityRadioButtonValue = parseInt(this.cityRadioButtonValue)
      this.markers = [];
      this.mapZoom = 1.5;
      for (let i = 0; i < this.cityList.length; i++) {
        if (this.cityRadioButtonValue === this.cityList[i].cityid) {
          this.markers.push({
            lat: this.cityList[i].latitude,
            lng: this.cityList[i].longitude,
            label: this.cityList[i].cityName
          })
          this.selectedCityObject = this.cityList[i];
          this.cityyMarkerClick(this.markers[0]);
          this.disableButton = false;
          break;
        }
      }




    }
    else if (event.target.name == 'religion_option') {
      this.disableButton = false;
      this.religionButtonValue = event.target.value;
      this.religion_url = (this.religionList[this.religionButtonValue].religion_url);

    }
    else if (event.target.name == "sdg_selection") {
      this.sdgTargetCode = this.sdgInfo[event.target.value].Sdg_target_code;
      this.disabledButton = true;

    }
    else if (event.target.name === 'gender') {
      this.firstNameUser = "";
      this.lastNameUser = "";
      this.firstName = "";
      this.lastName = "";
      this.selectedGender = event.target.value;
      if (this.selectedGender == 'F') {
        this.gameSummeryService.getFirstNameList(this.saveCountryObject.countryid, "F", "F", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
          res => {
            this.femaleFirstNameList = res;
            this.firstNameList = res;
            this.gameSummeryService.getLastNameList(this.saveCountryObject.countryid, "N", "L", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
              lastname => {
                this.lastNameList = this.commonService.randomArrayShuffle(lastname);
                if (lastname.length === 0) {
                  this.noLastNameFlag = true;
                }
              })

          })

      }
      if (this.selectedGender == 'M') {
        this.gameSummeryService.getFirstNameList(this.saveCountryObject.countryid, "M", "F", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
          res => {
            this.femaleFirstNameList = res;
            this.firstNameList = res;
            this.gameSummeryService.getLastNameList(this.saveCountryObject.countryid, "N", "L", this.selectedReligionObject.religionid, this.nameGroupId).subscribe(
              lastname => {
                this.lastNameList =  this.commonService.randomArrayShuffle(lastname);
                if (lastname.length === 0) {
                  this.noLastNameFlag = true;
                }
              })

          })
      }
    }
    else if (event.target.name === 'locale') {
      this.area = event.target.value;
    }
    else if (event.target.name == "nameDatabase") {
      if (event.target.value == "userDefined") {
        this.databaseNameSelectionFlag = "tab-pane"
        this.userDefinedNameSelectionFlag = "tab-pane active"
      }
      if (event.target.value == "database") {
        this.databaseNameSelectionFlag = "tab-pane active"
        this.userDefinedNameSelectionFlag = "tab-pane"
      }
    }
    else if (event.target.name == "firstName") {
      this.firstName = this.firstNameList[event.target.value].name
    }
    else if (event.target.name == "lastName") {
      this.lastName = this.lastNameList[event.target.value].name
    }

  }

  selectCountry(country) {
    this.selectedCountryObject = country
  }

  selectUrbanRural() {
    this.showGenderTabClass = "";
    this.showAreaTabClass = "Active";
  }
  selectName() {
    this.showAreaTabClass = "";
    this.showNameTabClass = "Active"
  }
  selectAttribute() {
    this.showNameTabClass = ""
    this.showAttributeTabClass = "Active";

  }



  checkValues(index) {
    if (this.attribute[index].value > 100) {
      this.disableFlag = true
    }
    else if (this.attribute[index].value < 0) {
      this.disableFlag = true;
    }
    else if (this.attribute[index].value > 0 && this.attribute[index].value < 100) {
      this.disableFlag = false
    }
  }


  close() {
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.showGroupCountry = false;
    this.modalWindowService.showCharacterDesighWithGropuFlag = false;
  }


  fillMarkerCountry() {
    for (let i = 0; i < this.countryList.length; i++) {
      this.markers.push(
        {
          lat: this.countryList[i].lat,
          lng: this.countryList[i].lng,
          label: this.countryList[i].country,
          code: this.countryList[i].Code

        }
      );
    }
  }

  fillMarkerCity() {
    this.mapZoom = 2;
    for (let i = 0; i < this.cityList.length; i++) {
      this.markers.push(
        {
          lat: this.cityList[i].latitude,
          lng: this.cityList[i].longitude,
          label: this.cityList[i].cityName
        }
      );

    }
  }

  generateLifeButtonCLick() {
    if ((this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.moreInforStm = this.translate.instant('gameLabel.demo_warr_for_other');
      this.warrningFlag = true;
    }
    else if ((this.gameSummeryService.totalPayedLifeCount >= this.gameSummeryService.maxLifeCount)) {
      this.moreInforStm = this.translate.instant('gameLabel.licences_exide_warr');
      this.showSuccessMsgFlagLoadLife = true;
    }
    else if ((this.firstName === "" || this.lastName === "") && (this.firstNameUser === "" || this.lastNameUser === "") && (!this.noLastNameFlag)) {
      this.modalWindowService.showWaitScreen = false;
      this.msgText = this.translate.instant('gameLabel.Please_choose_or_enter_first_and_last_name_to_proceed');
      this.showSuccessMsgFlag = true;
    }
    else if ((this.noLastNameFlag) && (this.firstName === "") && (this.firstNameUser === "")) {
      this.modalWindowService.showWaitScreen = false;
      this.msgText = this.translate.instant('gameLabel.Please_choose_or_enter_first_to_proceed');
      this.showSuccessMsgFlag = true;
    }

    else if (this.attribute[0].value >= 0 || this.attribute[0].value <= 100 &&
      this.attribute[1].value >= 0 || this.attribute[1].value <= 100 &&
      this.attribute[2].value >= 0 || this.attribute[2].value <= 100 &&
      this.attribute[3].value >= 0 || this.attribute[3].value <= 100 &&
      this.attribute[4].value >= 0 || this.attribute[4].value <= 100 &&
      this.attribute[5].value >= 0 || this.attribute[5].value <= 100 &&
      this.attribute[6].value >= 0 || this.attribute[6].value <= 100) {
      this.modalWindowService.showWaitScreen = true;
      let traits = {
        "artistic": this.attribute[0].value,
        "happiness": this.attribute[1].value,
        "intelligence": this.attribute[2].value,
        "musical": this.attribute[3].value,
        "athletic": this.attribute[4].value,
        "strength": this.attribute[5].value,
        "physicalEndurance": this.attribute[6].value
      }
      this.designALifeObject["traits"] = traits;

      if (this.firstNameUser !== "" || this.lastNameUser !== "") {
        this.designALifeObject['first_name'] = this.firstNameUser
        this.designALifeObject['last_name'] = this.lastNameUser
      }
      else if (this.firstName !== "" || this.lastName !== "") {
        this.designALifeObject['first_name'] = this.firstName
        this.designALifeObject['last_name'] = this.lastName
      }
      else if (this.noLastNameFlag) {
        this.designALifeObject['first_name'] = this.firstName;
        this.designALifeObject['last_name'] = null;
      }
      this.designALifeObject['nameGroupId'] = this.nameGroupId;

      this.designALifeObject['sex'] = this.selectedGender
      this.designALifeObject['urban_rural'] = this.area;

      this.selectedLanguage = this.translationService.selectedLang.code;
      if (!this.modalWindowService.showSdg && !this.modalWindowService.showGroupCountry) {
        this.gameSummeryService.sdgId = 0;
        this.gameSummeryService.generateCharacterDesignLife(this.designALifeObject, true, false, this.gameSummeryService.sdgId, this.selectedLanguage, false, 0).subscribe(
          id => {
            this.gameId = id;
            localStorage.removeItem("gameid");
            localStorage.setItem("gameid", this.gameId);
            this.modalWindowService.showWaitScreen = false;
            this.router.navigate(['/createlife']);
          })
      }
      else if (this.modalWindowService.showSdg || this.modalWindowService.showSdgWithAssignMent) {
        let sdgObject;
        sdgObject = {
          "subGoal": this.gameSummeryService.sdgSubGoalId,
          "challengeId": this.gameSummeryService.sdgChallengeId,
          "goalString": this.gameSummeryService.sdgSubGoalString
        }
        this.designALifeObject['sdg'] = sdgObject
        this.gameSummeryService.generateCharacterDesignLife(this.designALifeObject, false, true, this.gameSummeryService.sdgId, this.selectedLanguage, false, this.gameSummeryService.studentAssId).subscribe(
          id => {
            this.gameId = id;
            localStorage.removeItem("gameid");
            localStorage.setItem("gameid", this.gameId);
            this.modalWindowService.showWaitScreen = false;
            this.router.navigate(['/createlife']);

          })
      }
      else if (this.modalWindowService.showGroupCountry) {
        this.gameSummeryService.sdgId = 0;
        this.gameSummeryService.generateCharacterDesignLife(this.designALifeObject, false, false, this.gameSummeryService.sdgId, this.selectedLanguage, true, this.gameSummeryService.studentAssId).subscribe(
          id => {
            this.gameId = id;
            localStorage.removeItem("gameid");
            localStorage.setItem("gameid", this.gameId);
            this.modalWindowService.showWaitScreen = false;
            // this.modalWindowService.showCharacterDesighWithGropuFlag = false;
            // this.modalWindowService.showcharacterDesignFlag = false;
            this.modalWindowService.showSdg = false;
            this.modalWindowService.showGroupCountry = false;
            this.router.navigate(['/createlife']);

          })
      }
    }
  }

  clearClickCountry() {
    this.disableButton = true;
    this.mapZoom = 1.5;
    this.markers = [];
    this.fillMarkerCountry();
    this.selectClick(true, 'country');
  }


  countryMarkerClick(countryObject) {
    this.disableButton = false;
    this.mapZoom = 4;
    this.lat = countryObject.lat;
    this.lng = countryObject.lng;
  }


  cityyMarkerClick(cityObject) {
    this.disableButton = false;
    this.mapZoom = 4;
    this.lat = cityObject.lat;
    this.lng = cityObject.lng;
  }


  successWindowOk() {
    this.showSuccessMsgFlag = false;
  }


  shuffleCountry() {
    this.commonService.randomArrayShuffle(this.countryList);
  }

  Atoz() {
    this.countryList = JSON.parse(JSON.stringify(this.country));
  }
  windowOk() {
    this.warrningFlag = false;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.showGroupCountry = false;
    this.modalWindowService.showCharacterDesighWithGropuFlag = false;
  }
  loadIncompleteLife() {
    this.router.navigate(['/loadincompletegame']);

  }

  successWindowOkLoadLife() {
    this.showSuccessMsgFlagLoadLife = false;

  }

  backClick(){
    if(this.state==='country'){
      this.state="continent"
      this.continentUrl="https://en.wikipedia.org/wiki/Continent"
      this.disableButton=true;
    }
  else if(this.state==='city'){
    this.state="country"
    this.disableButton=true;
    for (let i = 0; i < this.countryList.length; i++) {
      this.markers.push(
        {
          lat: this.countryList[i].lat,
          lng: this.countryList[i].lng,
          label: this.countryList[i].country,
          code: this.countryList[i].Code

        }
      );
    }
  }
  else if(this.state==='religion'){
    this.state="city"
    this.disableButton=true;
    this.mapZoom = 2;
    for (let i = 0; i < this.cityList.length; i++) {
      this.markers.push(
        {
          lat: this.cityList[i].latitude,
          lng: this.cityList[i].longitude,
          label: this.cityList[i].cityName
        }
      );

    }
  }
  else if(this.state==='name'){
    this.state="religion"
    this.disableButton=true;


  }
  }
}
