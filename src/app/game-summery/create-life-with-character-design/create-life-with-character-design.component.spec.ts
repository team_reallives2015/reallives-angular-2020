import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLifeWithCharacterDesignComponent } from './create-life-with-character-design.component';

describe('CreateLifeWithCharacterDesignComponent', () => {
  let component: CreateLifeWithCharacterDesignComponent;
  let fixture: ComponentFixture<CreateLifeWithCharacterDesignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateLifeWithCharacterDesignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLifeWithCharacterDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
