import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadIncompleteLifeComponent } from './load-incomplete-life.component';

describe('LoadIncompleteLifeComponent', () => {
  let component: LoadIncompleteLifeComponent;
  let fixture: ComponentFixture<LoadIncompleteLifeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadIncompleteLifeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadIncompleteLifeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
