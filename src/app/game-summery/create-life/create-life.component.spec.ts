import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLifeComponent } from './create-life.component';

describe('CreateLifeComponent', () => {
  let component: CreateLifeComponent;
  let fixture: ComponentFixture<CreateLifeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateLifeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLifeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
