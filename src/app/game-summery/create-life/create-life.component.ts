
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AgaAYearService } from 'src/app/home/age-a-year/aga-a-year.service';
import { CommonActionService } from 'src/app/home/common-action.service';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { NumberFormatePipe } from 'src/app/shared/number-formate.pipe';
import { TranslationService } from 'src/app/translation/translation.service';
import { GameSummeryService } from '../game-summery.service';
import { fromEvent, interval, Subject } from 'rxjs';
import { FinanceService } from 'src/app/home/action-finance/finance.service';
import { PostGameService } from 'src/app/home/post-game.service';
import { takeUntil } from 'rxjs/operators';
import { AgeAYearComponent } from 'src/app/home/age-a-year/age-a-year.component';


@Component({
  selector: 'app-create-life',
  templateUrl: './create-life.component.html',
  styleUrls: ['./create-life.component.css']
})
export class CreateLifeComponent implements OnInit {
  bornSummaryTitle;
  countryChallengesArray = [];
  challengeHeading;
  challengeInfo;
  sibDeadCount = 0;
  sibDeadIndex = -1;
  selfEducation;
  selfJob;
  sName = null;
  gameId;
  classForStatementFour;
  allPerson;
  mainPerson;
  showBornVideo = false;
  gender;
  boyOrGirl;
  religions;
  state;
  sibDeadFalg:boolean=false;
  showAnimationFlag: boolean = false;
  showCountryInfoFlag: boolean = false;
  showBornInfoFlag: boolean = false;
  showHomeData: boolean = false;
  checkFormat: string;
  bornScreenArray = [];
  bornScreenButtonArray = [];
  buttonText = this.translate.instant('gameLabel.Fly_to_be_born');
  //dummyCountry
  dummyCountry;
  registeredCountry;
  registerCountrysdgInfo
  //end

  //sdgInfo
  sdgDetails
  bornSdgSubGoalDeatails;
  bornSdgInfo: any
  regSdgInfo: any
  registerSdgInfo: any;
  regSdgSubGoalDeatails
  registerCountrySdgName
  sdgDescription
  sdgInfo
  sdgName
  sdgOrigninalName;
  registeredCountrySdgDeatails;
  sdgNewData;
  sdgReagisterData;
  bornGoalColur;
  regiGoalColue

  //countrySpecificInfo
  countryInfo
  population
  countryFlag
  classvalue;
  //person info variables
  familyMembers
  active = "clr_code bg-success d-inline-block mr-2"
  nonActive
  selectedLanguage: String;
  senntenceGeneratedFlag: boolean = false;
  countryGroupTitle;
  //map
  longitude;
  latitude;
  mapZoom = 4;
  cityName;
  sex;
  sibCount;
  map
  previous;
  //amenities
  computerAvailability = "clr_code bg-red d-inline-block mr-2"
  refrigeratorAvailability = "clr_code bg-red d-inline-block mr-2"
  safeWaterAvailability = "clr_code bg-red d-inline-block mr-2"
  basicSanitationAvailability = "clr_code bg-red d-inline-block mr-2"
  healthServiceAvailability = "clr_code bg-red d-inline-block mr-2"
  televisionAvailability = "clr_code bg-red d-inline-block mr-2"
  radioAvailability = "clr_code bg-red d-inline-block mr-2"
  telephoneAvailability = "clr_code bg-red d-inline-block mr-2"
  mobileAvailability = "clr_code bg-red d-inline-block mr-2"
  vehicleAvailability = "clr_code bg-red d-inline-block mr-2"
  internetAvailability = "clr_code bg-red d-inline-block mr-2"
  lat;
  lng;
  zoom = 2;
  labelName;
  start_end_mark = [];
  latlng = [];
  sdgImage = ""
  values: any;
  valuesR: any;
  //bornSave
  ecoBornData = [];
  welathBornData = [];
  //tree
  religion;
  currencyName;
  diet;
  shelter;
  options;
  selfIncome: number;
  countryIncome;
  currencyCode;
  education;
  self;
  mainPersonName;
  mainPersonAge;
  mother;
  motherString;
  father;
  fatherString;
  wife;
  wifeString;
  husband;
  husbandString;
  siblings;
  siblingString;
  sibString;
  sib;
  children;
  childrenString;
  childString;
  child;
  grandChildren;
  grandChildrenString;
  grandChildString;
  grandChild;
  livingAtHomeArr;
  livingAwayArr = [];
  level1Arr = [];
  level2Arr = [];
  level3Arr = [];
  level4Arr = [];
  identity;
  image;
  width;
  class;
  householdIncome;
  householdExpenses;
  householdNetWorth;
  language;
  groupIndex;
  //
  bornCountry;
  registerCountry;
  chanceOfPoorR;
  bOne: number = 0;
  bTwo: number = 0;
  bThree: number = 0;
  bFour: number = 0;
  rOne: number = 0;
  rTwo: number = 0;
  rThree: number = 0;
  rFour: number = 0;
  firstR
  moreInfoFlag: boolean = false;
  //
  //graph
  showGraphWindow: boolean = false;
  maxLenght: number = 40;
  yTrimValue: boolean = true;
  single: any[];
  view: any[] = [320, 200];
  view1: any[] = [320, 200];
  view2: any[] = [700, 400];
  view3 = [1000, 200];
  view4 = [600, 200];

  multi: any[];
  multi1: any[];
  multi2: any[];
  multi3: any[];
  multi4: any[];
  multi5: any[];

  showDataLabel = true;
  colorScheme2 = {
    domain: ['#00FFFF', '#567caa', '#567caa']
  };
  schemeType: string = 'linear';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  maxXAxisTickLength: number = 10
  gradient: boolean = false;
  showLegend: boolean = false;
  showLegend1: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = this.translate.instant('gameLabel.Country');
  showYAxisLabel: boolean = true;
  xAxisLabel: string = this.translate.instant('gameLabel.Population');
  wealthGraphSentenceBorn1;
  wealthGraphSentenceBorn2;
  wealthGraphSentenceBorn3;
  wealthGraphSentenceReg1;
  wealthGraphSentenceReg2;
  wealthGraphSentenceReg3;
  registerCountryArray;
  poupulationValue;
  classForStatusOne;
  classForStatusTwo;
  classForStatusThree
  classForStatusFour;
  classForStatusFive;
  classForStatusBorderOne;
  classForStatusBorderTwo;
  classForStatusBorderThree
  classForStatusBorderFour;
  classForStatusBorderFive;
  flagStmt;
  bornCountryArray
  colorScheme = {
    domain: ['#00FFFF', '#00FFFF']
  };

  colorScheme1 = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA'],
  };
  colorScheme3 = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#0000FF']
  };

  stringReturnValue;
  stringRetutnText;
  activeClassForDemo = "nav-item nav-link active"
  activeClassForPerCapita = "nav-item nav-link";
  activeClassForLFO = "nav-item nav-link";
  activeClassForIndex = "nav-item nav-link";
  activeClassForSdg = "nav-item nav-link";
  onClickDemoGraphic: boolean = true;
  onClickPerCapita: boolean = false;
  onCLickLabourForce: boolean = false;
  onCLickSdgGroup: boolean = false;
  onClickIndexRating: boolean = false;
  onClickSdgData: boolean = false;
  sentenceTextWindow1: string = "";
  bithString: string = "";
  deathString: string = "";
  primarySchoolString: string = "";
  happinessString: string = "";
  valurForHappiAndCorr;
  giniIndexString: string = "";
  labourForceString1: string = "";
  labourForceString2: string = "";
  sexRatio: string = "";
  accesEleString = "";
  valueForHdi;
  sexRatioValue;
  sdgScoreBorn;
  sdgScoreRegister;
  populationString;
  bornCountryWelathMetaData: any;
  registerCountryWelathMetaData: any;
  bornCountryWelathMetaData1: any;
  registerCountryWelathMetaData1: any;
  welthGraphDataNotGenrateFlag: boolean = false;
  status;
  statusValue;
  firstValue;
  secondValue;
  thirdValue;
  fourthValue;
  firstValueReg;
  secondValueReg;
  thirdValueReg;
  fourthValueReg;
  statusMetadata
  oldStatusValue;
  registerStatusData;
  incomeGraphStringOne;
  incomeGraphStringTwo;
  incomeGraphStringThree;
  incomeGraphStringFour;
  registerCountryExchangeRate;
  registerCountryCode; nextEventObject;
  countryCoveredCount = 0;
  countryCovredString = ""
  //lifesummary
  substring
  substring1 = "";
  substring2 = "";
  substring3 = "";
  substring4 = "";
  substring5 = "";
  substring6 = "";
  substring7 = "";
  substring8 = "";
  substring9 = "";
  substring10 = "";
  substring11 = "";
  substring12 = "";
  substring13 = "";
  substring14 = "";
  substring15 = "";
  substring16 = "";
  substring17 = "";
  substring18 = "";
  deadString = "";
  motherEducation;
  fatherEducation;
  motherJob; fatherJob;
  perdayEarning;
  healthProblemFather;
  healthProblemMother;
  currentSavingRate;
  bornCountryGroupList;
  registerCountryGroupList;
  bornSummaryObj = {};
  bornSummary;
  bSummary;
  friendMailShowFlag: boolean = false;
  emailId;
  messageText;
  successMsgFlag;
  bornString: string;
  dietSubString;
  shelterString;
  finalStatement;
  finalStatementReg;
  classForHealthStatus;
  classForHelath;
  moreInfoTitle;
  completedCount = 0;
  moreInfoStm;
  constantGraphValueArray = [
    { 'name': '10%', 'value': 10 / 100 },
    { 'name': '20%', 'value': 20 / 100 },
    { 'name': '30%', 'value': 30 / 100 },
    { 'name': '40%', 'value': 40 / 100 },
    { 'name': '50%', 'value': 50 / 100 },
    { 'name': '60%', 'value': 60 / 100 },
    { 'name': '70%', 'value': 70 / 100 },
    { 'name': '80%', 'value': 80 / 100 },
    { 'name': '90%', 'value': 90 / 100 },
    { 'name': '100%', 'value': 100 / 100 },
  ]
  webLink = "https://dashboards.sdgindex.org/profiles/";
  countryGroupIframeFlag: boolean = false;
  bornData = [];
  classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting";
  private unsubscriber: Subject<void> = new Subject<void>();
  constructor(public commonService: CommonService,
    public gameSummeryService: GameSummeryService,
    public modalWindowService: modalWindowShowHide,
    private router: Router,
    public agaAYearService: AgaAYearService,
    public translationService: TranslationService,
    public translate: TranslateService,
    public homeService: HomeService,
    public constantService: ConstantService,
    public commonActionService: CommonActionService,
    public numberPipe: NumberFormatePipe,
    public financeService: FinanceService,
    public postGameService: PostGameService,
    public translation: TranslateService) {
    interval(10000).subscribe(x => {
      this.commonService.audio.pause();
    });
  }


  ngOnInit(): void {
    history.pushState(null, '');
    fromEvent(window, 'popstate')
      .pipe(takeUntil(this.unsubscriber))
      .subscribe((_) => {
        history.pushState(null, '');
      });
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.homeService.clicktoGameSummary = false;
    this.createLife();

  }


  createLife() {
    if (!this.modalWindowService.showcharacterDesignFlag && !this.modalWindowService.showCharacterDesighWithGropuFlag && !this.modalWindowService.showAssignment && !this.gameSummeryService.generateLifeWithDemo) {
      this.selectedLanguage = this.translationService.selectedLang.code;
      let age = 0;
      if (this.modalWindowService.ShowOldLifeFlag) {
        age = 17;
      }
      this.gameSummeryService.getCreateLifeData(false, false, 0, this.selectedLanguage, age).subscribe(res => {
        this.gameId = res;
        localStorage.removeItem("gameid");
        localStorage.setItem("gameid", this.gameId);
        this.gameSummeryService.getPersonFromId(this.gameId).subscribe(res => {
          this.homeService.allPerson = res;
          this.allPerson = res;
          this.translate.get('gameLabel.flagStmt', { bCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, rCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country }).subscribe(
            (str) => {
              this.flagStmt = str;
            })
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "M") {
            this.sex = this.translate.instant('gameLabel.boy');
            this.boyOrGirl = this.translate.instant('gameLabel.boy');
            this.gender = this.translate.instant('gameLabel.Male');
          }
          else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "F") {
            this.sex = this.translate.instant('gameLabel.girl');
            this.boyOrGirl = this.translate.instant('gameLabel.girl');
            this.gender = this.translate.instant('gameLabel.Female');
          }
          if (this.allPerson[this.constantService.FAMILY_SELF].siblings.length == 0) {
            this.sibCount = 0;
          }
          else {
            this.sibCount = this.allPerson[this.constantService.FAMILY_SELF].siblings.length;
          }
          if (!this.modalWindowService.ShowOldLifeFlag) {
            if (this.allPerson[this.constantService.FAMILY_SELF].dead) {
              let secondLangCode = "COMMON_BORN_STRING";
              let parameters = {
                Sex: this.sex, status: " dead", Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
                mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
              }
              this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
            }
            else if (this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].dead) {
              let secondLangCode = "COMMON_BORN_STRING";
              let parameters = {
                Sex: this.sex, status: '', Country: this.allPerson[this.constantService.FAMILY_SELF].country.country + ". "+this.translate.instant('gameLabel.mother_dead') + "", self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
                mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
              }
              this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
            } else {
              let secondLangCode = "COMMON_BORN_STRING";
              let parameters = {
                Sex: this.sex, status: '', Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
                mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
              }
              this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
            }
          }
          else if (this.modalWindowService.ShowOldLifeFlag) {
             if (this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].dead && this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].dead) {
              let secondLangCode = "COMMON_BORN_DYNAMIC_AGE";
              let parameters = {
                "Year": this.allPerson[this.constantService.FAMILY_SELF].age, Sex: this.sex, Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, DeathString: " "+ this.translate.instant('gameLabel.dead1'), self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
                mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
              }
              this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
              this.gameSummeryService.BornAtAnyAgeNextEvent(this.allPerson);
            }
           else  if (this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].dead) {
              let secondLangCode = "COMMON_BORN_DYNAMIC_AGE";
              let parameters = {
                "Year": this.allPerson[this.constantService.FAMILY_SELF].age, Sex: this.sex, Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, DeathString: " "+this.translate.instant('gameLabel.dead2'), self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
                mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
              }
              this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
              let sibCount=this.gameSummeryService.checkSiblingDeadAtAge17( this.allPerson);
              if(sibCount>0){
               this.gameSummeryService.BornAtAnyAgeNextEvent(this.allPerson);
              }            }
            else if (this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].dead) {
              let secondLangCode = "COMMON_BORN_DYNAMIC_AGE";
              let parameters = {
                "Year": this.allPerson[this.constantService.FAMILY_SELF].age, Sex: this.sex, Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, DeathString: " "+this.translate.instant('gameLabel.dead3') , self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
                mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
              }
              this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
              let sibCount=this.gameSummeryService.checkSiblingDeadAtAge17( this.allPerson);
              if(sibCount>0){
               this.gameSummeryService.BornAtAnyAgeNextEvent(this.allPerson);
              }            }
            else {
              let secondLangCode = "COMMON_BORN_DYNAMIC_AGE";
              let parameters = {
                "Year": this.allPerson[this.constantService.FAMILY_SELF].age, Sex: this.sex, Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, DeathString: "", self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
                mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
              }
              this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
             let sibCount=this.gameSummeryService.checkSiblingDeadAtAge17( this.allPerson);
             if(sibCount>0){
              this.gameSummeryService.BornAtAnyAgeNextEvent(this.allPerson);
             }
            }
          }
          this.gameSummeryService.getActionNextEvent(this.nextEventObject).subscribe(
            res1 => {
              let result1 = res1;
              this.commonActionService.getCreateLifeEvent(result1);
              if(this.gameSummeryService.createLifenextEventFlag){
                this.gameSummeryService.setNextEventForCreateLife( this.allPerson[this.constantService.FAMILY_SELF].game_id,this.gameSummeryService.createlifeNextEventId).subscribe(
                  res=>{
                    if(res){
                      this.gameSummeryService.createlifeNextEventId=null;
                    }
                  })
                }
              this.translationService.setSelectedlanguage(this.allPerson.selected_language);
              this.mainPerson = this.allPerson['SELF'];
              this.updateBornSummaryArray();
              this.gameSummeryService.mapDisplay = false;
              this.gameSummeryService.country = this.mainPerson.country;
              this.gameSummeryService.city = this.mainPerson.city;
              this.gameSummeryService.registerCountry = this.mainPerson.register_country;
              this.commonService.audio.src = "assets/sound/new-bornbaby-crying.ogg";
              this.commonService.audio.play();
              this.commonService.audio.loop = true;
              this.registeredCountry = this.mainPerson.register_country;
              this.sdgScoreBorn = this.mainPerson.country.SdgiScore;
              this.sdgScoreRegister = this.registeredCountry.SdgiScore;
              this.gameSummeryService.getCountryCoveredCount().subscribe(
                res => {
                  this.countryCoveredCount = res.countriesCovered;
                  this.completedCount = res.completedCountries
                  this.translate.get('gameLabel.country_covred', { countryCoveredCount: this.countryCoveredCount, completedCount: this.completedCount }).subscribe(
                    (str) => {
                      this.countryCovredString = str;
                    })
                  this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid, true, this.allPerson.selected_language).subscribe(sdgInfo => {
                    this.sdgDetails = sdgInfo['goals'];
                    this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                    this.gameSummeryService.getSDGInfo(this.registeredCountry.countryid, false, this.allPerson.selected_language).subscribe(sdgInfo => {
                      this.regSdgSubGoalDeatails = sdgInfo['targets'];
                      if (this.allPerson.sdgId !== 0) {
                        this.gameSummeryService.sdgLogoOrNotFlag = true;
                        this.gameSummeryService.sdgLogoClass = "active"
                        this.gameSummeryService.sdgLogo = "assets/images/sdgicon/sdg" + this.allPerson.sdgId + "_" + this.translationService.selectedLang.code + ".png";
                        this.commonService.getSdgStringSplite(this.homeService.allPerson.sdgDetails);
                      }
                      else {
                        this.gameSummeryService.sdgLogoOrNotFlag = false;
                      }
                      this.gameSummeryService.getWelathMetaData(this.mainPerson.country.countryid).subscribe(
                        res => {
                          if (res !== 'false') {
                            this.bornCountryWelathMetaData = res;
                            this.gameSummeryService.bornCountryWelathMetadata = res;
                          }
                          else {
                            this.bornCountryWelathMetaData = false;
                            this.gameSummeryService.bornCountryWelathMetadata = false;
                          }
                          this.gameSummeryService.getWelathMetaData(this.registeredCountry.countryid).subscribe(
                            res => {
                              this.registerCountryWelathMetaData = res;
                              if (res !== 'false') {
                                this.registerCountryWelathMetaData = res;
                              }
                              else {
                                this.registerCountryWelathMetaData = false;
                              }
                              this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
                                res => {
                                  this.statusMetadata = res[0];
                                  this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
                                    res => {
                                      this.registerStatusData = res[0]
                                      this.gameSummeryService.ecoStatusData = this.statusMetadata;
                                      this.generateArrayForWelathGraph();
                                      this.setSocialEconomicalStatus();
                                      
                                      if (this.mainPerson.country.countryid === 74) {
                                        this.gameSummeryService.getAllReligion(this.mainPerson.city.Countryregionid).subscribe(data => {
                                          this.religions = data;
                                          this.state = "visa-page";
                                          this.showHomeData = true;
                                          this.getDataForWelthGraph();
                                          this.displayFamilyIncomeGraph();
                                          this.createFamilyTree(this.homeService.allPerson);
                                          this.getBornSummaryData(this.homeService.allPerson);
                                          this.createBornSaveData();

                                          this.gameSummeryService.saveBornSummary(this.bornSummaryObj, this.ecoBornData, this.welathBornData, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                            res => {
                                              this.bornSummary = res;
                                              this.translate.get('gameLabel.You_are_born_as_a', { boygirl: this.boyOrGirl, country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe(
                                                (str) => {
                                                  this.bornString = str;
                                                })
                                            })
                                        });
                                      }
                                       else {
                                        this.gameSummeryService.getReligionForWorld(this.mainPerson.country.countryid, this.selectedLanguage).subscribe(data => {
                                          this.religions = data;
                                          this.state = "visa-page";
                                          this.showHomeData = true;
                                          this.getDataForWelthGraph();
                                          this.displayFamilyIncomeGraph();
                                          this.createFamilyTree(this.homeService.allPerson);
                                          this.getBornSummaryData(this.homeService.allPerson);
                                          this.createBornSaveData();
                                          this.gameSummeryService.saveBornSummary(this.bornSummaryObj, this.ecoBornData, this.welathBornData, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                            res => {
                                              this.bornSummary = res;
                                              this.translate.get('gameLabel.You_are_born_as_a', { boygirl: this.boyOrGirl, country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe(
                                                (str) => {
                                                  this.bornString = str;
                                                 
                                                })

                                            })
                                        });
                                      }
                                     
                                    })
                            })
                        })
                    })
                  })
                })
            })
          })
        })
      })
    }
    else if (this.modalWindowService.showcharacterDesignFlag && !this.modalWindowService.showCharacterDesighWithGropuFlag && !this.modalWindowService.showAssignment && !this.gameSummeryService.generateLifeWithDemo) {
      this.gameId = localStorage.getItem('gameid');
      this.gameSummeryService.getPersonFromId(this.gameId).subscribe(res => {
        this.homeService.allPerson = res;
        this.allPerson = res;
        this.selectedLanguage = this.translationService.selectedLang.code;
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "M") {
          this.sex = this.translate.instant('gameLabel.boy');
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "F") {
          this.sex = this.translate.instant('gameLabel.girl');
        }
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].siblings.length == 0) {
          this.sibCount = 0;
        }
        else {
          this.sibCount = this.homeService.allPerson[this.constantService.FAMILY_SELF].siblings.length;
        }
        this.translate.get('gameLabel.flagStmt', { bCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, rCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country }).subscribe(
          (str) => {
            this.flagStmt = str;
          })
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
          let secondLangCode = "COMMON_BORN_STRING";
          let parameters = {
            Sex: this.sex, status: " dead", Country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.homeService.allPerson[this.constantService.FAMILY_SELF].first_name,
            mother_name: this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
          }
          this.nextEventObject = { langCode: secondLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
        }
        else if (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].dead) {
          let secondLangCode = "COMMON_BORN_STRING";
          let parameters = {
            Sex: this.sex, status: '', Country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country + ". Unfortunately, your mother died while giving birth to you" + "", self_name: this.homeService.allPerson[this.constantService.FAMILY_SELF].first_name,
            mother_name: this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
          }
          this.nextEventObject = { langCode: secondLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
        } else {
          let secondLangCode = "COMMON_BORN_STRING";
          let parameters = {
            Sex: this.sex, status: '', Country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.homeService.allPerson[this.constantService.FAMILY_SELF].first_name,
            mother_name: this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
          }
          this.nextEventObject = { langCode: secondLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
        }
        this.gameSummeryService.getActionNextEvent(this.nextEventObject).subscribe(
          res1 => {
            let result1 = res1;
            this.commonActionService.getCreateLifeEvent(result1);
            this.translationService.setSelectedlanguage(this.homeService.allPerson.selected_language);
            this.commonService.audio.src = "assets/sound/new-bornbaby-crying.ogg";
            this.commonService.audio.play();
            this.commonService.audio.loop = true;
            this.mainPerson = this.homeService.allPerson['SELF'];
            this.mainPerson.game_settings.Screen9 = true;
            this.updateBornSummaryArray();
            this.gameSummeryService.country = this.mainPerson.country;
            this.gameSummeryService.city = this.mainPerson.city;
            this.gameSummeryService.registerCountry = this.mainPerson.register_country;
            this.gameSummeryService.mapDisplay = false;
            this.registeredCountry = this.mainPerson.register_country;
            this.sdgScoreBorn = this.mainPerson.country.SdgiScore;
            this.sdgScoreRegister = this.registeredCountry.SdgiScore;
            this.gameSummeryService.getCountryCoveredCount().subscribe(
              res => {
                this.countryCoveredCount = res.countriesCovered;
                this.completedCount = res.completedCountries
                this.translate.get('gameLabel.country_covred', { countryCoveredCount: this.countryCoveredCount, completedCount: this.completedCount }).subscribe(
                  (str) => {
                    this.countryCovredString = str;
                  })
                this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid, true, this.allPerson.selected_language).subscribe(sdgInfo => {
                  this.sdgDetails = sdgInfo['goals'];
                  this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                  this.gameSummeryService.getSDGInfo(this.registeredCountry.countryid, false, this.allPerson.selected_language).subscribe(sdgInfo => {
                    this.regSdgSubGoalDeatails = sdgInfo['targets'];
                    if (this.mainPerson.sex === "F") {
                      this.boyOrGirl = this.translate.instant('gameLabel.girl');
                      this.gender = this.translate.instant('gameLabel.Female');
                    }
                    else if (this.mainPerson.sex === "M") {
                      this.boyOrGirl = this.translate.instant('gameLabel.boy');
                      this.gender = this.translate.instant('gameLabel.Male');

                    }
                    if (this.homeService.allPerson.sdgId !== 0) {
                      this.gameSummeryService.sdgLogoOrNotFlag = true;
                      this.gameSummeryService.sdgLogoClass = "active"
                      this.gameSummeryService.sdgLogo = "assets/images/sdgicon/sdg" + this.homeService.allPerson.sdgId + "_" + this.translationService.selectedLang.code + ".png";
                      this.commonService.getSdgStringSplite(this.homeService.allPerson.sdgDetails);
                    }
                    else {
                      this.gameSummeryService.sdgLogoOrNotFlag = false;
                    }
                    this.gameSummeryService.getWelathMetaData(this.mainPerson.country.countryid).subscribe(
                      res => {
                        if (res !== 'false') {
                          this.bornCountryWelathMetaData = res;
                          this.gameSummeryService.bornCountryWelathMetadata = res;
                        }
                        else {
                          this.bornCountryWelathMetaData = false;
                          this.gameSummeryService.bornCountryWelathMetadata = false;
                        }
                        this.gameSummeryService.getWelathMetaData(this.registeredCountry.countryid).subscribe(
                          res => {
                            this.registerCountryWelathMetaData = res;
                            if (res !== 'false') {
                              this.registerCountryWelathMetaData = res;
                            }
                            else {
                              this.registerCountryWelathMetaData = false;
                            }
                            this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
                              res => {
                                this.statusMetadata = res[0];
                                this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
                                  res => {
                                    this.registerStatusData = res[0]
                                    this.gameSummeryService.ecoStatusData = this.statusMetadata;
                                    this.setSocialEconomicalStatus();
                                    this.generateArrayForWelathGraph();
                                    this.getDataForWelthGraph();
                                    this.createFamilyTree(this.homeService.allPerson);
                                    this.displayFamilyIncomeGraph();
                                    this.getBornSummaryData(this.homeService.allPerson);
                                    this.createBornSaveData();
                                    this.gameSummeryService.saveBornSummary(this.bornSummaryObj, this.ecoBornData, this.welathBornData, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                      res => {
                                        this.bornSummary = res;
                                        this.translate.get('gameLabel.You_are_born_as_a', { boygirl: this.boyOrGirl, country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe(
                                          (str) => {
                                            this.bornString = str;
                                          })
                                        if (this.mainPerson.country.countryid === 74) {
                                          this.gameSummeryService.getAllReligion(this.mainPerson.city.Countryregionid).subscribe(data => {
                                            this.religions = data;
                                            this.showHomeData = true;
                                            this.state = "visa-page";

                                          });
                                        } else {
                                          this.gameSummeryService.getReligionForWorld(this.mainPerson.country.countryid, this.selectedLanguage).subscribe(data => {
                                            this.religions = data;
                                            this.showHomeData = true;
                                            this.state = "visa-page";

                                          });
                                        }
                                      })
                                  })
                              })
                          })
                      })
                  })
                  // this.showHomeData = true;
                  // this.state="born-screen";
                })
              })
          })
      })
    }
    else if (this.modalWindowService.showCharacterDesighWithGropuFlag || this.modalWindowService.showAssignment) {
      this.modalWindowService.showCharacterDesighWithGropuFlag = false;
      this.modalWindowService.showAssignment = false;
      this.gameId = localStorage.getItem('gameid');
      this.gameSummeryService.getPersonFromId(this.gameId).subscribe(res => {
        this.homeService.allPerson = res;
        this.allPerson = res;
        this.selectedLanguage = this.translationService.selectedLang.code;
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "M") {
          this.sex = this.translate.instant('gameLabel.boy');
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "F") {
          this.sex = this.translate.instant('gameLabel.girl');
        }
        if (this.allPerson[this.constantService.FAMILY_SELF].siblings.length == 0) {
          this.sibCount = 0;
        }
        else {
          this.sibCount = this.allPerson[this.constantService.FAMILY_SELF].siblings.length;
        }
        this.translate.get('gameLabel.flagStmt', { bCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, rCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country }).subscribe(
          (str) => {
            this.flagStmt = str;
          })
        if (this.allPerson[this.constantService.FAMILY_SELF].dead) {
          let secondLangCode = "COMMON_BORN_STRING";
          let parameters = {
            Sex: this.sex, status: " dead", Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
            mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
          }
          this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
        }
        else if (this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].dead) {
          let secondLangCode = "COMMON_BORN_STRING";
          let parameters = {
            Sex: this.sex, status: '', Country: this.allPerson[this.constantService.FAMILY_SELF].country.country + ". Unfortunately, your mother died while giving birth to you" + "", self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
            mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
          }
          this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
        } else {
          let secondLangCode = "COMMON_BORN_STRING";
          let parameters = {
            Sex: this.sex, status: '', Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
            mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
          }
          this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
        }
        this.gameSummeryService.getActionNextEvent(this.nextEventObject).subscribe(
          res1 => {
            let result1 = res1;
            this.commonActionService.getCreateLifeEvent(result1);
            this.translationService.setSelectedlanguage(this.allPerson.selected_language);
            this.mainPerson = this.allPerson['SELF'];
            this.updateBornSummaryArray();
            this.gameSummeryService.mapDisplay = false;
            this.gameSummeryService.country = this.mainPerson.country;
            this.gameSummeryService.city = this.mainPerson.city;
            this.gameSummeryService.registerCountry = this.mainPerson.register_country;
            // this.showHomeData = true;
            // this.state = "visa-page";

            // this.modalWindowService.showWaitScreen = true;
            this.commonService.audio.src = "assets/sound/new-bornbaby-crying.ogg";
            this.commonService.audio.play();
            this.commonService.audio.loop = true;
            this.registeredCountry = this.mainPerson.register_country;
            this.sdgScoreBorn = this.mainPerson.country.SdgiScore;
            this.sdgScoreRegister = this.registeredCountry.SdgiScore;
            this.gameSummeryService.getCountryCoveredCount().subscribe(
              res => {
                this.countryCoveredCount = res.countriesCovered;
                this.completedCount = res.completedCountries
                this.translate.get('gameLabel.country_covred', { countryCoveredCount: this.countryCoveredCount, completedCount: this.completedCount }).subscribe(
                  (str) => {
                    this.countryCovredString = str;
                  })
                this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid, true, this.allPerson.selected_language).subscribe(sdgInfo => {
                  this.sdgDetails = sdgInfo['goals'];
                  this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                  this.gameSummeryService.getSDGInfo(this.registeredCountry.countryid, false, this.allPerson.selected_language).subscribe(sdgInfo => {
                    this.regSdgSubGoalDeatails = sdgInfo['targets'];
                    if (this.mainPerson.sex === "F") {
                      this.boyOrGirl = this.translate.instant('gameLabel.girl');
                      this.gender = this.translate.instant('gameLabel.Female');
                    }
                    else if (this.mainPerson.sex === "M") {
                      this.boyOrGirl = this.translate.instant('gameLabel.boy');
                      this.gender = this.translate.instant('gameLabel.Male');
                    }
                    if (this.allPerson.sdgId !== 0) {
                      this.gameSummeryService.sdgLogoOrNotFlag = true;
                      this.gameSummeryService.sdgLogoClass = "active"
                      this.gameSummeryService.sdgLogo = "assets/images/sdgicon/sdg" + this.allPerson.sdgId + "_" + this.translationService.selectedLang.code + ".png";
                      this.commonService.getSdgStringSplite(this.homeService.allPerson.sdgDetails);
                    }
                    else {
                      this.gameSummeryService.sdgLogoOrNotFlag = false;
                    }
                    this.gameSummeryService.getWelathMetaData(this.mainPerson.country.countryid).subscribe(
                      res => {
                        if (res !== 'false') {
                          this.bornCountryWelathMetaData = res;
                          this.gameSummeryService.bornCountryWelathMetadata = res;
                        }
                        else {
                          this.bornCountryWelathMetaData = false;
                          this.gameSummeryService.bornCountryWelathMetadata = false;
                        }
                        this.gameSummeryService.getWelathMetaData(this.registeredCountry.countryid).subscribe(
                          res => {
                            this.registerCountryWelathMetaData = res;
                            if (res !== 'false') {
                              this.registerCountryWelathMetaData = res;
                            }
                            else {
                              this.registerCountryWelathMetaData = false;
                            }
                            this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
                              res => {
                                this.statusMetadata = res[0];
                                this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
                                  res => {
                                    this.registerStatusData = res[0]
                                    this.gameSummeryService.ecoStatusData = this.statusMetadata;
                                    this.generateArrayForWelathGraph();
                                    this.setSocialEconomicalStatus();
                                    if (this.mainPerson.country.countryid === 74) {
                                      this.gameSummeryService.getAllReligion(this.mainPerson.city.Countryregionid).subscribe(data => {
                                        this.religions = data;
                                        this.getDataForWelthGraph();
                                        this.displayFamilyIncomeGraph();
                                        this.createFamilyTree(this.homeService.allPerson);
                                        this.getBornSummaryData(this.homeService.allPerson);
                                        this.createBornSaveData();

                                        this.gameSummeryService.saveBornSummary(this.bornSummaryObj, this.ecoBornData, this.welathBornData, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                          res => {
                                            this.bornSummary = res;
                                            this.translate.get('gameLabel.You_are_born_as_a', { boygirl: this.boyOrGirl, country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe(
                                              (str) => {
                                                this.bornString = str;
                                                this.showHomeData = true;
                                                this.state = "visa-page";
                                              })
                                          })
                                      });
                                    } else {
                                      this.gameSummeryService.getReligionForWorld(this.mainPerson.country.countryid, this.selectedLanguage).subscribe(data => {
                                        this.religions = data;
                                        this.getDataForWelthGraph();
                                        this.displayFamilyIncomeGraph();
                                        this.createFamilyTree(this.homeService.allPerson);
                                        this.getBornSummaryData(this.homeService.allPerson);
                                        this.createBornSaveData();
                                        this.gameSummeryService.saveBornSummary(this.bornSummaryObj, this.ecoBornData, this.welathBornData, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                          res => {
                                            this.bornSummary = res;
                                            this.translate.get('gameLabel.You_are_born_as_a', { boygirl: this.boyOrGirl, country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe(
                                              (str) => {
                                                this.bornString = str;
                                                this.showHomeData = true;
                                                this.state = "visa-page";
                                              })
                                          })
                                      })
                                    }
                                  })
                              })
                          })
                      })
                  })
                })
              })
          })
      })
    }
    else if (this.gameSummeryService.generateLifeWithDemo) {
      this.selectedLanguage = this.translationService.selectedLang.code;
      this.gameSummeryService.createorLoadDemoLife(this.selectedLanguage).subscribe(res => {
        this.gameId = res;
        localStorage.removeItem("gameid");
        localStorage.setItem("gameid", this.gameId);
        this.gameSummeryService.getPersonFromId(this.gameId).subscribe(res => {
          this.homeService.allPerson = res;
          this.allPerson = res;
          this.translate.get('gameLabel.flagStmt', { bCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, rCountry: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country }).subscribe(
            (str) => {
              this.flagStmt = str;
            })
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "M") {
            this.sex = this.translate.instant('gameLabel.boy');
            this.boyOrGirl = this.translate.instant('gameLabel.boy');
            this.gender = this.translate.instant('gameLabel.Male');
          }
          else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "F") {
            this.sex = this.translate.instant('gameLabel.girl');
            this.boyOrGirl = this.translate.instant('gameLabel.girl');
            this.gender = this.translate.instant('gameLabel.Female');
          }
          if (this.allPerson[this.constantService.FAMILY_SELF].siblings.length == 0) {
            this.sibCount = 0;
          }
          else {
            this.sibCount = this.allPerson[this.constantService.FAMILY_SELF].siblings.length;
          }
          if (this.allPerson[this.constantService.FAMILY_SELF].dead) {
            let secondLangCode = "COMMON_BORN_STRING";
            let parameters = {
              Sex: this.sex, status: " dead", Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
              mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
            }
            this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
          }
          else if (this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].dead) {
            let secondLangCode = "COMMON_BORN_STRING";
            let parameters = {
              Sex: this.sex, status: '', Country: this.allPerson[this.constantService.FAMILY_SELF].country.country + ". Unfortunately, your mother died while giving birth to you" + "", self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
              mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
            }
            this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
          } else {
            let secondLangCode = "COMMON_BORN_STRING";
            let parameters = {
              Sex: this.sex, status: '', Country: this.allPerson[this.constantService.FAMILY_SELF].country.country, self_name: this.allPerson[this.constantService.FAMILY_SELF].first_name,
              mother_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].mother].first_name, father_name: this.allPerson[this.allPerson[this.constantService.FAMILY_SELF].father].first_name, sib_count_str: this.sibCount
            }
            this.nextEventObject = { langCode: secondLangCode, gameId: this.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
          }
          this.gameSummeryService.getActionNextEvent(this.nextEventObject).subscribe(
            res1 => {
              let result1 = res1;
              this.commonActionService.getCreateLifeEvent(result1);
              this.translationService.setSelectedlanguage(this.allPerson.selected_language);
              this.mainPerson = this.allPerson['SELF'];
              this.updateBornSummaryArray();
              this.gameSummeryService.mapDisplay = false;
              this.gameSummeryService.country = this.mainPerson.country;
              this.gameSummeryService.city = this.mainPerson.city;
              this.gameSummeryService.registerCountry = this.mainPerson.register_country;
              // this.showHomeData = true;
              // this.state = "visa-page";

              // this.modalWindowService.showWaitScreen = true;
              this.commonService.audio.src = "assets/sound/new-bornbaby-crying.ogg";
              this.commonService.audio.play();
              this.commonService.audio.loop = true;
              this.registeredCountry = this.mainPerson.register_country;
              this.sdgScoreBorn = this.mainPerson.country.SdgiScore;
              this.sdgScoreRegister = this.registeredCountry.SdgiScore;
              this.gameSummeryService.getCountryCoveredCount().subscribe(
                res => {
                  this.countryCoveredCount = res.countriesCovered;
                  this.completedCount = res.completedCountries
                  this.translate.get('gameLabel.country_covred', { countryCoveredCount: this.countryCoveredCount, completedCount: this.completedCount }).subscribe(
                    (str) => {
                      this.countryCovredString = str;
                    })
                  this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid, true, this.allPerson.selected_language).subscribe(sdgInfo => {
                    this.sdgDetails = sdgInfo['goals'];
                    this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                    this.gameSummeryService.getSDGInfo(this.registeredCountry.countryid, false, this.allPerson.selected_language).subscribe(sdgInfo => {
                      this.regSdgSubGoalDeatails = sdgInfo['targets'];
                      if (this.allPerson.sdgId !== 0) {
                        this.gameSummeryService.sdgLogoOrNotFlag = true;
                        this.gameSummeryService.sdgLogoClass = "active"
                        this.gameSummeryService.sdgLogo = "assets/images/sdgicon/sdg" + this.allPerson.sdgId + "_" + this.translationService.selectedLang.code + ".png";
                        this.commonService.getSdgStringSplite(this.homeService.allPerson.sdgDetails);
                      }
                      else {
                        this.gameSummeryService.sdgLogoOrNotFlag = false;
                      }
                      this.gameSummeryService.getWelathMetaData(this.mainPerson.country.countryid).subscribe(
                        res => {
                          if (res !== 'false') {
                            this.bornCountryWelathMetaData = res;
                            this.gameSummeryService.bornCountryWelathMetadata = res;
                          }
                          else {
                            this.bornCountryWelathMetaData = false;
                            this.gameSummeryService.bornCountryWelathMetadata = false;
                          }
                          this.gameSummeryService.getWelathMetaData(this.registeredCountry.countryid).subscribe(
                            res => {
                              this.registerCountryWelathMetaData = res;
                              if (res !== 'false') {
                                this.registerCountryWelathMetaData = res;
                              }
                              else {
                                this.registerCountryWelathMetaData = false;
                              }
                              this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
                                res => {
                                  this.statusMetadata = res[0];
                                  this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
                                    res => {
                                      this.registerStatusData = res[0]
                                      this.gameSummeryService.ecoStatusData = this.statusMetadata;
                                      this.generateArrayForWelathGraph();
                                      this.setSocialEconomicalStatus();
                                      if (this.mainPerson.country.countryid === 74) {
                                        this.gameSummeryService.getAllReligion(this.mainPerson.city.Countryregionid).subscribe(data => {
                                          this.religions = data;
                                          this.getDataForWelthGraph();
                                          this.displayFamilyIncomeGraph();
                                          this.createFamilyTree(this.homeService.allPerson);
                                          this.getBornSummaryData(this.homeService.allPerson);
                                          this.createBornSaveData();
                                          this.gameSummeryService.saveBornSummary(this.bornSummaryObj, this.ecoBornData, this.welathBornData, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                            res => {
                                              this.bornSummary = res;
                                              this.translate.get('gameLabel.You_are_born_as_a', { boygirl: this.boyOrGirl, country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe(
                                                (str) => {
                                                  this.bornString = str;
                                                  this.countryCovredString = str;
                                                  this.showHomeData = true;
                                                  this.state = "visa-page";
                                                })
                                            })
                                        })
                                      } else {
                                        this.gameSummeryService.getReligionForWorld(this.mainPerson.country.countryid, this.selectedLanguage).subscribe(data => {
                                          this.religions = data;
                                          this.getDataForWelthGraph();
                                          this.displayFamilyIncomeGraph();
                                          this.createFamilyTree(this.homeService.allPerson);
                                          this.getBornSummaryData(this.homeService.allPerson);
                                          this.createBornSaveData();
                                          this.gameSummeryService.saveBornSummary(this.bornSummaryObj, this.ecoBornData, this.welathBornData, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                            res => {
                                              this.bornSummary = res;
                                              this.translate.get('gameLabel.You_are_born_as_a', { boygirl: this.boyOrGirl, country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe(
                                                (str) => {
                                                  this.bornString = str;
                                                  this.countryCovredString = str;
                                                  this.showHomeData = true;
                                                  this.state = "visa-page";
                                                })
                                            })
                                        });
                                      }

                                    })
                                })
                            })
                        })
                    })
                  })
                })
            })
        })
      })
    }
  }

  sendMail() {
    let obituaryData = {}
    obituaryData['obituary'] = this.bornSummary;
    this.postGameService.senfObituaryMail(obituaryData, 'summary').subscribe(
      res => {
        if (res) {
          this.messageText = this.translation.instant('gameLabel.Send_mail_succesfully');
          this.successMsgFlag = true;
        } else {
          this.messageText = this.translation.instant('gameLabel.email_not_found');
          this.successMsgFlag = true;
        }
      }
    )
  }
  successMsgWindowClose() {
    this.successMsgFlag = false;
  }

  sendmailToFriend() {
    this.friendMailShowFlag = true;
    this.emailId = '';
  }

  send() {
    this.friendMailShowFlag = false;
    let obituaryData = {}
    obituaryData['emailId'] = this.emailId;
    obituaryData['obituary'] = this.bornSummary;
    this.postGameService.senfObituaryMail(obituaryData, 'summary').subscribe(
      res => {
        if (res) {
          this.messageText = this.translation.instant('gameLabel.Send_mail_succesfully');
          this.successMsgFlag = true;
        } else {
          this.messageText = this.translation.instant('gameLabel.email_not_found');
          this.successMsgFlag = true;
        }
      })
  }

  close() {
    this.friendMailShowFlag = false;
    this.emailId = '';
  }



  playLifeClick() {
    this.commonService.audio.pause();
    this.modalWindowService.showGotoHome = true;
    this.router.navigate(
      ['/play-life'],
      { queryParams: { flag: 'true' } }
    );
  }

  goBack() {
    this.router.navigate(['/summary']);
  }


  setSocialEconomicalStatus() {
    //regi
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    this.firstValueReg = (f3R);
    this.secondValueReg = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    this.thirdValueReg = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    this.fourthValueReg = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    //end
    this.commonService.calculateWealthStatus(this.bornCountryWelathMetaData, this.homeService.allPerson);
    this.commonService.calculateIncameStatus(this.statusMetadata, this.homeService.allPerson);
    let wealth = (this.commonService.wealthStatus);
    let income = (this.commonService.incomeStatus);
    if (wealth > income) {
      this.status = this.commonService.wealthStatus;
      this.statusValue = this.commonService.wealthValue;
    }
    else if (wealth < income) {
      this.status = this.commonService.incomeStatus;
      this.statusValue = this.commonService.incomeValue
    }
    else if (wealth === income) {
      this.status = this.commonService.incomeStatus;
      if (this.commonService.wealthValue > this.commonService.incomeValue) {
        this.statusValue = this.commonService.wealthValue
      }
      else if (this.commonService.wealthValue < this.commonService.incomeValue) {
        this.statusValue = this.commonService.incomeValue
      }
      else {
        this.statusValue = this.commonService.incomeValue
      }

    }

    let statusObj = {
      'status': this.status,
      'newPoints': this.statusValue,
      'oldPoints': this.statusValue
    }
    this.homeService.saveFamilyStatus(this.mainPerson.game_id, statusObj, 0, null).subscribe(
      res => {
        if (res === true) {
          this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status = this.status;
          this.homeService.allPerson[this.constantService.FAMILY_SELF].old_economic_points = this.statusValue;
          this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points = this.statusValue;
        }
      })
  }


  displayAmenities() {
    var amenities = this.mainPerson.amenities
    if (amenities.computers > 0) {
      this.computerAvailability = "clr_code bg-success d-inline-block mr-2"
    }
    if (amenities.refrigerators > 0)
      this.refrigeratorAvailability = "clr_code bg-success d-inline-block mr-2"

    if (amenities.safeWater)
      this.safeWaterAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.basicSanitation)
      this.basicSanitationAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.healthService)
      this.healthServiceAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.televisions > 0)
      this.televisionAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.radios > 0)
      this.radioAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.telephones > 0)
      this.telephoneAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.mobiles > 0)
      this.mobileAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.vehicles > 0)
      this.vehicleAvailability = "clr_code bg-success d-inline-block mr-2"
    if (amenities.internet)
      this.internetAvailability = "clr_code bg-success d-inline-block mr-2"


  }

  clickNext(state: any) {
    let buttonState;
    let index = this.bornScreenArray.indexOf(state);
    index = index + 1;
    state = this.bornScreenArray[index];
    this.state = state
    buttonState = this.bornScreenArray[index + 1];
    this.buttonText = this.bornScreenButtonArray[0][buttonState];

    if (state === 'country-register-country-map') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.gotoStateCountryRegisterMap();
    }
    if (state === 'family-tree') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
      this.createFamilyTree(this.homeService.allPerson);
    }
    else if (state === 'country-map') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.gotoStateCountryMap();
    }
    else if (state === 'wealth-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
      this.getDataForWelthGraph();
    }
    else if (state === 'wealth-grpah-comparision') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.view4 = [600, 200];
      this.getDataForWelthGraph();
      this.getDataForWelthGraphCompariosion()
    }
    else if (state === 'family-amenities') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
      this.displayAmenities()
    }
    else if (state === "country-disparities") {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.view = [320, 200];
      this.tabClickDemo();
    }
    else if (state === "eco-status") {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
      this.displayFamilyIncomeGraph();
    }
    else if (state === 'family-income-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
    }
    else if (state === 'born-summary') {
      this.translate.get('gameLabel.Born_Summary', { firstName: this.mainPerson.first_name }).subscribe(
        (str1: String) => {
          this.bornSummaryTitle = str1;
          this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
        })
    }
    else if (state === 'country-week') {
      this.countryChallengesFormating();
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
    }
  }

  createFamilyTree(allPerson) {
    this.allPerson = allPerson;
    this.mainPerson = this.homeService.allPerson[this.constantService.FAMILY_SELF];
    this.mainPersonName = this.mainPerson.first_name;
    this.registerCountryExchangeRate = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.householdIncome = this.mainPerson.expense.householdIncome.toFixed(2);
    this.householdExpenses = this.mainPerson.expense.householdExpenses.toFixed(2);
    this.householdNetWorth = this.mainPerson.expense.householdNetWorth.toFixed(2);
    this.currencyName = this.mainPerson.country.currencyName;
    this.mainPersonAge = this.mainPerson.age;
    this.selfIncome = parseInt("" + (this.mainPerson.expense.householdIncome * 12) / this.mainPerson.country.ExchangeRate);
    this.countryIncome = (this.mainPerson.country.ProdperCapita * this.mainPerson.country.AverageFamilyCount);
    this.level1Arr = [];
    this.level2Arr = [];
    this.currencyCode = this.mainPerson.country.currencyCode;
    this.registerCountryCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural,

      //code for father
      this.father = this.homeService.allPerson[this.constantService.FAMILY_FATHER];
    let healthProblemFather = "";
    this.classForHelath = "";
    this.classForHealthStatus = "";
    if (this.father.traits.health < 35) {
      this.classForHelath = " border-bottom colourRed1"
    }
    else {
      this.classForHelath = "border-bottom";
    }
    if (this.father.health_disease != null && !this.father.dead) {
      let keys = Object.keys(this.father.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.father.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemFather == "") {
            healthProblemFather = healthProblemFather + this.father.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemFather = healthProblemFather + ", " + this.father.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemFather === "") {
        healthProblemFather = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      if (healthProblemFather === "") {
        healthProblemFather = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    if (this.father.job.JobId === 999) { }
    this.fatherEducation = this.commonService.setEducationForAll(this.homeService.allPerson[this.constantService.FAMILY_FATHER]);
    this.healthProblemFather = healthProblemFather;
    this.level1Arr.push({
      "name": this.father.full_name,
      "education": this.fatherEducation,
      "age": this.father.age,
      "identity": this.translate.instant('gameLabel.Father'),
      "sex": this.translate.instant('gameLabel.Male'),
      "image": "assets/images/game_images/fam.png",
      "class": "fmly_member men_bg",
      "jobName": this.father.job.Job_displayName,
      "income": this.father.income.toFixed(2),
      "registerIncome": ((this.father.income / this.homeService.allPerson[this.constantService.FAMILY_FATHER].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.father.traits.health,
      "healthProblem": healthProblemFather,
      "currencyCode": this.father.country.CurrencyPlural,
      "currencyName": this.father.country.currency_code,
      "healthClass": this.classForHelath,
      "healthClassForStatus": this.classForHealthStatus
    })



    //code for mother
    this.mother = this.homeService.allPerson[this.constantService.FAMILY_MOTHER];
    let healthProblemMother = "";
    this.classForHelath = "";
    this.classForHealthStatus = "";
    if (this.mother.traits.health < 35) {
      this.classForHelath = "border-bottom colourRed1"
    }
    else {
      this.classForHelath = "border-bottom";
    }
    if (this.mother.health_disease != null && !this.mother.dead) {
      let keys = Object.keys(this.mother.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.mother.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemMother == "") {
            healthProblemMother = healthProblemMother + this.mother.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemMother = healthProblemMother + ", " + this.mother.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemMother === "") {
        healthProblemMother = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom";
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      if (healthProblemMother === "") {
        healthProblemMother = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    this.motherEducation = this.commonService.setEducationForAll(this.homeService.allPerson[this.constantService.FAMILY_MOTHER]);
    this.healthProblemMother = healthProblemMother;
    this.level1Arr.push({
      "name": this.mother.full_name,
      "age": this.mother.age,
      "education": this.motherEducation,
      "identity": this.translate.instant('gameLabel.Mother'),
      "sex": this.translate.instant('gameLabel.Female'),
      "image": "assets/images/game_images/girl.png",
      "class": "fmly_member female_bg",
      "jobName": this.mother.job.Job_displayName,
      "income": this.mother.income.toFixed(2),
      "registerIncome": ((this.mother.income / this.homeService.allPerson[this.constantService.FAMILY_MOTHER].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.mother.traits.health,
      "healthProblem": healthProblemMother,
      "currencyCode": this.mother.country.CurrencyPlural,
      "currencyName": this.mother.country.currency_code,
      "healthClass": this.classForHelath,
      "healthClassForStatus": this.classForHealthStatus
    })




    //code for self

    this.self = this.homeService.allPerson[this.constantService.FAMILY_SELF];
    this.classForHelath = "";
    this.classForHealthStatus = "";
    if (this.self.traits.health < 35) {
      this.classForHelath = "border-bottom colourRed1"
    }
    else {
      this.classForHelath = "border-bottom";
    }
    if (this.self.sex == "F") {
      this.gender = this.translate.instant('gameLabel.Female');
      this.class = "fmly_member female_bg";
      this.image = "assets/images/game_images/girl.png"
    }
    else {
      this.gender = this.translate.instant('gameLabel.Male');
      this.class = "fmly_member men_bg";
      this.image = "assets/images/game_images/fam.png"
    }
    let jobName;
    if (this.mainPerson.job.JobId == 999) {
      jobName = "";

    } else {
      if (this.mainPerson.job.JobId == 132
        || this.mainPerson.job.JobId == 133) {
        if (this.mainPerson.business.Business != this.constantService.BUSINESS_NO_BUSINESS) {
          jobName = this.mainPerson.business.Business;
        }
        else {
          jobName = this.mainPerson.job.Job_displayName;
        }
      }
      else {
        jobName = this.mainPerson.job.Job_displayName;
      }

    }
    let healthProblemSelf = "";
    if (this.self.health_disease != null && !this.self.dead) {
      let keys = Object.keys(this.self.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.self.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemSelf == "") {
            healthProblemSelf = healthProblemSelf + this.self.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemSelf = healthProblemSelf + ", " + this.self.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemSelf === "") {
        healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      if (healthProblemSelf === "") {
        healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    let education = this.commonService.setEducationForAll(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
    this.level1Arr.push({
      "name": this.self.full_name,
      "age": this.self.age,
      "education": education,
      "identity": this.translate.instant('gameLabel.You'),
      "sex": this.gender,
      "image": this.image,
      "class": this.class,
      "jobName": jobName,
      "income": this.self.income.toFixed(2),
      "registerIncome": ((this.self.income / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.self.traits.health,
      "healthProblem": healthProblemSelf,
      "currencyCode": this.self.country.CurrencyPlural,
      "currencyName": this.self.country.currency_code,
      "healthClass": this.classForHelath,
      "healthClassForStatus": this.classForHealthStatus
    })
    //code for sibling
    let genderSib;
    if (this.mainPerson[this.constantService.FAMILY_SIBLING] != null) {
      this.siblings = this.mainPerson[this.constantService.FAMILY_SIBLING];
      let jobName
      for (var i = 0; i < this.siblings.length; i++) {
        this.sibString = this.mainPerson.siblings[i];
        this.sib = this.homeService.allPerson[this.sibString];
        this.classForHelath = "";
        this.classForHealthStatus = "";
        if (this.sib.traits.health < 35) {
          this.classForHelath = "border-bottom colourRed1"
        }
        else {
          this.classForHelath = "border-bottom";
        }
        if (this.sib.sex == "F") {
          genderSib = this.translate.instant('gameLabel.Female');
          this.identity = this.translate.instant('gameLabel.Sister')
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          genderSib = this.translate.instant('gameLabel.Male');
          this.identity = this.translate.instant('gameLabel.Brother')
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.sib.health_disease != null && !this.sib.dead) {
          let keys = Object.keys(this.sib.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.sib.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.sib.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.sib.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom";
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        if (this.mainPerson.job.jobId == 999) {
          jobName = "";

        } else {
          jobName = this.sib.job.Job_displayName;
        }
        let education = this.commonService.setEducationForAll(this.sib);
        this.level2Arr.push({
          "name": this.sib.full_name,
          "age": this.sib.age,
          "education": education,
          "identity": this.identity,
          "sex": genderSib,
          "class": this.class,
          "image": this.image,
          "jobName": jobName,
          "income": this.sib.income.toFixed(2),
          "registerIncome": ((this.sib.income / this.homeService.allPerson[this.sibString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.sib.traits.health,
          "healthProblem": healthProblem,
          "currencyCode": this.sib.country.CurrencyPlural,
          "currencyName": this.sib.country.currency_code,
          "healthClass": this.classForHelath,
          "healthClassForStatus": this.classForHealthStatus
        })

      }
    }







  }
  clickBack(state) {
    let buttonState;
    let index = this.bornScreenArray.indexOf(state);
    index = index - 1;
    this.state = this.bornScreenArray[index];
    buttonState = this.bornScreenArray[index + 1];
    this.buttonText = this.bornScreenButtonArray[0][buttonState];
    if (this.state === 'country-register-country-map') {
      this.gotoStateCountryRegisterMap();
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
    }
    if (this.state === 'family-tree') {
      this.createFamilyTree(this.homeService.allPerson);
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
    }
    else if (this.state === 'country-map') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.gotoStateCountryMap();
    }
    else if (this.state === 'wealth-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
      this.getDataForWelthGraph();
    }
    else if (this.state == 'wealth-grpah-comparision') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.view4 = [600, 200];
      this.getDataForWelthGraph();
      this.getDataForWelthGraphCompariosion()
    }
    else if (this.state == 'family-amenities') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
      this.displayAmenities()
    }
    else if (this.state === "country-disparities") {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.view = [320, 200];
      this.tabClickDemo();
    }
    else if (this.state == 'family-income-grpah') {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no glob_bg livealifestarting"
      this.displayFamilyIncomeGraph();
    }
    else if (state === "eco-status") {
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
      this.displayFamilyIncomeGraph();

    }
    else if (this.state == 'born-summary') {
      this.translate.get('gameLabel.Born_Summary', { firstName: this.mainPerson.first_name }).subscribe(
        (str1: String) => {
          this.bornSummaryTitle = str1;
          this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
        })
    }
    else if (state === 'country-week') {
      this.countryChallengesFormating();
      this.classForRemoveGbBag = "main_wraper d-flex flex-column justify-content-between-no livealifestarting"
    }

  }
  displayFamilyIncomeGraph() {
    this.classForStatusBorderOne = "";
    this.classForStatusBorderTwo = "";
    this.classForStatusBorderThree = "";
    this.classForStatusBorderFour = "";
    this.classForStatusBorderFive = "";
    this.incomeGraphStringOne = '';
    let s = "";
    let selfIncome = parseInt("" + (this.mainPerson.expense.householdIncome * 12) / this.mainPerson.country.ExchangeRate);
    let registerIncome = (this.mainPerson.register_country.ProdperCapita * this.mainPerson.register_country.AverageFamilyCount);
    let countryIncome = (this.mainPerson.country.ProdperCapita) * this.mainPerson.country.AverageFamilyCount;
    let value2 = Math.round(this.gameSummeryService.ecoStatusData.chanceOfPoor * 100);
    let value = Math.round((selfIncome));
    // let value = Math.round((selfIncome / this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members));
    let value3 = (value / 365);
    this.perdayEarning =  value3/this.mainPerson.count_of_household_members;
    if (value2 >= 0) {
      if (value3 > 2) {
        this.incomeGraphStringFour = this.translate.instant('gameLabel.You_are_not_one_of_them.');
      }
      else {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 0) {
          this.classForStatementFour = "colourRed1"
        }
        this.incomeGraphStringFour = this.translate.instant('gameLabel.You_are_one_of_them.');
      }
    }

    let subString;
    subString = this.translate.instant('gameLabel.a');
    let color = this.commonService.getColurForStatus(this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status);
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 0) {
      this.classForStatusOne = color;
      this.classForStatusBorderOne = "table-bordered-red"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 1) {
      this.classForStatusTwo = color;
      this.classForStatusBorderTwo = "table-bordered-yellow"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 2) {
      this.classForStatusThree = color;
      this.classForStatusBorderThree = "table-bordered-orange"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 3) {
      this.classForStatusFour = color;
      this.classForStatusBorderFour = "table-bordered-blue"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 4) {
      this.classForStatusFive = color;
      this.classForStatusBorderFive = "table-bordered-green"

    }
    this.sName = null;
    this.sName = this.commonService.getSatusString(this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status)
    if (this.modalWindowService.ShowOldLifeFlag) {
     let statusStr= this.commonService.getAge17StatusString(this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status);
          this.incomeGraphStringOne = statusStr;
          this.translate.get('gameLabel.incomeGraphSen3', { value3: value3.toFixed(2) }).subscribe(
            (str: String) => {
              this.incomeGraphStringTwo = str;
              this.translate.get('gameLabel.incomeGraphSen2', { country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, value2: value2.toFixed(0) }).subscribe(
                (str1: String) => {
                  this.incomeGraphStringThree = str1;
                })
            })
    }
    else {
      this.translate.get('gameLabel.incomeGraphSen1', { subString: subString, ecostatus: this.sName }).subscribe(
        (str1: String) => {
          this.incomeGraphStringOne = str1;
          this.translate.get('gameLabel.incomeGraphSen3', { value3: value3.toFixed(2) }).subscribe(
            (str: String) => {
              this.incomeGraphStringTwo = str;
              this.translate.get('gameLabel.incomeGraphSen2', { country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, value2: value2.toFixed(0) }).subscribe(
                (str1: String) => {
                  this.incomeGraphStringThree = str1;

                })
            })
        })

    }

    //    this.incomeGraphStringOne="You have been born into " +subString+  this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status +" family, " +"earning $"+value3.toFixed(2)+"  per day."
    //     this.incomeGraphStringThree=" The odds of being born as absolutely poor (earning less than $2 per day) in "+this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country + " are " + value2.toFixed(0)+"% ."+s;


    this.multi1 = [
      {
        name: this.translate.instant('gameLabel.Your_family'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(selfIncome))
          }
        ]
      },

      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(countryIncome))
          }
        ]
      },
      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(registerIncome))
          }
        ]
      },
      {
        name: this.translate.instant('gameLabel.World'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(12000))
          }
        ]
      },
    ];
    this.calculateTableValues();
  }
  gotoStateCountryRegisterMap() {
    this.latlng = [
      [this.registeredCountry.lat, this.registeredCountry.lng, "assets/images/girl.png"],
      [this.mainPerson.country.lat, this.mainPerson.country.lng, "assets/images/fam.png"]
    ];
    this.start_end_mark.push(this.latlng[0]);
    this.start_end_mark.push(this.latlng[this.latlng.length - 1]);
  }

  gotoStateCountryMap() {
    this.latitude = this.mainPerson.city.latitude
    this.longitude = this.mainPerson.city.longitude
    this.cityName = this.mainPerson.city.cityName;
  }

  checkPopulationFormat(Population) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = (Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }

  availabilityString(amenity) {
    if (amenity == this.active)
      return this.translate.instant('gameLabel.Available')
    else
      return this.translate.instant('gameLabel.Not_Available')

  }

  goToGameSummery() {
    this.modalWindowService.showGameSeetingsFlag = true;
  }

  checkCss(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }


  checkCssForSpecific(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }

  checkCss1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }


  checkCssForSpecific1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }



  getRegisterCountrySdgScore(i) {
    let v
    v = this.regSdgInfo[i].score;
    return v;
  }
  getRegisterCountrySdgColur(i) {
    let v
    v = this.regSdgInfo[i].color;
    return v;
  }
  getNewSdgColor(colour) {
    let classvalue = "";
    if (colour === "green") {
      classvalue = "clr_code bg-success d-inline-block mr-2"
    }
    else if (colour === "red") {
      classvalue = "clr_code bg-red d-inline-block mr-2"
    }
    else if (colour === "orange") {
      classvalue = "clr_code bg-oragne d-inline-block mr-2"
    }
    else if (colour === "yellow") {
      classvalue = "clr_code bg-yellow d-inline-block mr-2"
    }
    else if (colour === "grey") {
      classvalue = "clr_code bg-secondary d-inline-block mr-2"
    }
    return classvalue;
  }
  sdgSelect(sdgId: number) {
    this.bornSdgInfo = [];
    this.regSdgInfo = [];
    this.sdgInfo = this.bornSdgSubGoalDeatails;
    this.values = Object.values(this.sdgInfo)
    for (let i = 0; i < this.values.length; i++) {
      if (this.values[i].SDG_Id === sdgId) {
        if (this.values[i].score !== 'NA') {
          this.values[i].score = parseFloat(this.values[i].score);
          this.values[i].score = this.values[i].score.toFixed(2);
        }
        else {
          this.values[i].score = this.values[i].score;
        }
        this.bornSdgInfo.push(this.values[i]);
      }
    }
    this.valuesR = Object.values(this.regSdgSubGoalDeatails)
    for (let i = 0; i < this.valuesR.length; i++) {
      if (this.valuesR[i].SDG_Id === sdgId) {
        this.regSdgInfo.push(this.valuesR[i]);
      }
    }
    this.sdgImage = "assets/images/sdgicon/sdg" + sdgId + "_" + this.translationService.selectedLang.code + ".png";
    this.sdgDescription = this.sdgDetails[sdgId - 1].SDG_discription;
    this.sdgName = this.sdgDetails[sdgId - 1].SDG_title;
    this.sdgOrigninalName = this.sdgDetails[sdgId].SDG_name;

  }

  markerClick(infowindow, i) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
    if (i == 0) {
      this.labelName = this.registeredCountry.country;
      this.lat = this.registeredCountry.country.lat
      this.lng = this.registeredCountry.country.lng
      this.zoom = 4;
    }
    else if (i == 1) {
      this.labelName = this.mainPerson.country.country;
      this.lat = this.mainPerson.country.lat
      this.lng = this.mainPerson.country.lng
      this.zoom = 5;
    }
  }

  checkColourPatternForGraph(x, y, classIndex) {
    if (classIndex === 0) {
      let val = this.checkCssForSpecific(x, y)
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
    else if (classIndex === 1) {
      let val = this.checkCss(x, y);
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
  }
  generateStringValue(Bi, Ri) {
    let B = parseFloat(Bi);
    let R = parseFloat(Ri);
    if (B < R) {
      this.stringReturnValue = ((100 * ((R - B) / R))).toFixed(2);
      this.valurForHappiAndCorr = (R - B).toFixed(2);
      this.valueForHdi = (100 * (R - B) / R).toFixed(2)
      this.poupulationValue = (((B) / R) * 100).toFixed(2)
    }
    else if (B > R) {
      this.stringReturnValue = (B / R).toFixed(2)
      this.valurForHappiAndCorr = (B - R).toFixed(2);
      this.valueForHdi = ((B / R - 1) * 100).toFixed(2)
      this.poupulationValue = (B / R).toFixed(2);
    }
    else {
      this.stringReturnValue = 0
      this.valurForHappiAndCorr = 0
      this.valueForHdi = 0;
      this.poupulationValue = 0;
    }
    return this.stringReturnValue
  }

  generateStringSubString(B, R) {
    B = parseFloat(B);
    R = parseFloat(R);
    if (B < R) {
      this.stringRetutnText = this.translate.instant('gameLabel.%_less_likely');
      this.bithString = this.translate.instant('gameLabel.%_fewer');
      this.deathString = this.translate.instant('gameLabel.%_less_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.%_less');
      this.happinessString = this.translate.instant('gameLabel.ranks_above');
      this.giniIndexString = this.translate.instant('gameLabel.lower');
      this.sexRatio = this.translate.instant('gameLabel.less');
      this.accesEleString = this.translate.instant('gameLabel.%_less');
      this.populationString = "%"
    }
    else if (B > R) {
      this.stringRetutnText = this.translate.instant('gameLabel.times_more_likely');
      this.bithString = this.translate.instant('gameLabel.times_more');
      this.deathString = this.translate.instant('gameLabel.times_more_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.times_more');
      this.happinessString = this.translate.instant('gameLabel.ranks_below');
      this.giniIndexString = this.translate.instant('gameLabel.higher');
      this.sexRatio = this.translate.instant('gameLabel.more');
      this.accesEleString = this.translate.instant('gameLabel.times_more');
      this.populationString = this.translate.instant('gameLabel.times');

    }
    else {
      this.populationString = this.translate.instant('gameLabel.almost_equal');
      this.stringRetutnText = this.translate.instant('gameLabel.as_good_as');
    }
    return this.stringRetutnText;

  }

  getSexRationValue(B, R) {
    B = parseFloat(B);
    R = parseFloat(R);
    this.sexRatioValue = Math.abs((R - B) * 1000).toFixed(2);
  }

  checkSentenceGenaerateOrNot(B, R) {
    if (B === 0 || R === 0 || B === "NA" || R === "NA") {
      this.senntenceGeneratedFlag = true;
    }
    else {
      this.senntenceGeneratedFlag = false;
    }
  }



  onButtonClickGraphChange(x, y, fieldName, classIndex) {
    if (fieldName === 'Population') {
      this.xAxisLabel = this.translate.instant('gameLabel.Population');
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (this.poupulationValue == 0) {
        this.populationString = this.translate.instant('gameLabel.almost_equal');

        this.translate.get('gameLabel.populationCD1', { country: this.mainPerson.country.country, populationString: this.populationString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // Population of {{country}} is {{populationString}} that of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.mainPerson.country.country+" is "+this.populationString+" that of "+this.mainPerson.register_country.country+"'s population.";
      }
      else {
        this.translate.get('gameLabel.populationCD2', { country: this.mainPerson.country.country, poupulationValue: this.poupulationValue, populationString: this.populationString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        //     Population of {{country}} is {{poupulationValue}} {{populationString}}  of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.mainPerson.country.country+" is "+this.poupulationValue+this.populationString+" of "+this.mainPerson.register_country.country+"'s population.";
      }
      this.colorScheme = {
        domain: ['#00FFFF', '#00FFFF']
      };
    }
    else if (fieldName === "Sex Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Sex_Ratio');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.getSexRationValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.sexRatioValue) === 0) {
          this.sexRatio = this.translate.instant('gameLabel.as_good_as')
          this.translate.get('gameLabel.sexRationDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If  "+this.mainPerson.country.country+" is your home country, at birth, there will be equal, boys per 1000 girls, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.sexRationDC2', { country: this.mainPerson.country.country, sexRatioValue: Math.round(this.sexRatioValue), sexRatio: this.sexRatio, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, at birth, there will be , {{sexRatioValue}} {{sexRatio}} boys per 1000 girls than {{rcountry}}.
          // this.sentenceTextWindow1="If  "+this.mainPerson.country.country+" is your home country, at birth, there will be , "+ Math.round(this.sexRatioValue) +" "+this.sexRatio+" boys per 1000 girls than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Birth Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Birth_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.BirthRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have equal babies, compared to " +this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.BirthRateDC2', { country: this.mainPerson.country.country, value: value, bithString: this.bithString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have "+ value + this.bithString+ " babies than " +this.mainPerson.register_country.country +".";
      }
      this.checkColourPatternForGraph(x, y, classIndex);

    }
    else if (fieldName === "Death Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Death_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.DeathRatioDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have equal risk of dying, compared to {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have equal risk of dying, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.DeathRatioDC2', { country: this.mainPerson.country.country, value: value, deathString: this.deathString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{deathString}} of dying than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have "+ value + this.deathString+ " of dying than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Infant Mortality Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Infant_Mortality_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.InfantMortalityRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are equally likely to die in infancy, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.InfantMortalityRateDC2', { country: this.mainPerson.country.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you are {{value}} {{stringRetutnText}} to die in infancy than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are "+ value + this.stringRetutnText+ " to die in infancy than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Primary School") {
      this.xAxisLabel = this.translate.instant('gameLabel.Primary_School_Enrollment');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.primarySchoolDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have equal access to Primary School, compared to " +this.mainPerson.register_country.country +".";
        } else {
          this.translate.get('gameLabel.primarySchoolDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{primarySchoolString}} access to Primary School than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have "+ value +this.primarySchoolString+ " access to Primary School than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "UnEmployment Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Unemployment_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.UnEmploymentRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are equally likely to be unemployed, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.UnEmploymentRateDC2', { country: this.mainPerson.country.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // "If {{country}} is your home country, you will spend {{value}} {{primarySchoolString}} money on health care than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are "+ value +this.stringRetutnText+ " to be unemployed than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Health Exp. per capita") {
      this.xAxisLabel = this.translate.instant('gameLabel.Health_Exp_per_capita');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.healthExpDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will spend equal money on health care, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.healthExpDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will spend "+ value +this.primarySchoolString+ " money on health care than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "PPP") {
      this.xAxisLabel = this.translate.instant('gameLabel.PPP');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {

          this.translate.get('gameLabel.pppDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will earn equal money, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.pppDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will earn "+ value +this.primarySchoolString+ " money than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Happiness Score") {
      this.xAxisLabel = this.translate.instant('gameLabel.Happiness_Rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.HappinessScoreDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" is almost equal in happiness index, compared to "+this.mainPerson.register_country.country ;
        } else {
          this.translate.get('gameLabel.HappinessScoreDC2', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in happiness index.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Corruption") {
      this.xAxisLabel = this.translate.instant('gameLabel.Corruption');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.CorruptionDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" is equal in the list of the most corrupt countries, compared to "+this.mainPerson.register_country.country;
        }
        else {
          this.translate.get('gameLabel.CorruptionDC2', { rcountry: this.mainPerson.register_country.country, country: this.mainPerson.country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the list of the most corrupt countries.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Sdgi Rank") {
      this.xAxisLabel = this.translate.instant('gameLabel.SDG_rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {

          this.translate.get('gameLabel.SdgiScoreDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1=this.mainPerson.country.country+" is equal in the SDG rank, compared to " +this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.SdgiScoreDC2', { country: this.mainPerson.country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the SDG rank than " +this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Hdi") {
      this.xAxisLabel = this.translate.instant('gameLabel.HDI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valueForHdi) === 0) {
          this.translate.get('gameLabel.hdiDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has  equal HDI, compared to "+ this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.hdiDC2', { country: this.mainPerson.country.country, valueForHdi: this.valueForHdi, giniIndexString: this.giniIndexString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has  "+ this.valueForHdi +"% "+this.giniIndexString+ "  HDI than "+ this.mainPerson.register_country.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Gini") {
      this.xAxisLabel = this.translate.instant('gameLabel.GINI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if ((x - y) === 0 || (y - x) === 0) {
          this.translate.get('gameLabel.giniDc1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has equal income inequality among rich and poor.";
        }
        else {
          this.translate.get('gameLabel.giniDc2', { country: this.mainPerson.country.country, giniIndexString: this.giniIndexString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.mainPerson.country.country+" has  "+ this.giniIndexString + " income inequality among rich and poor.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Access to Electricity") {
      this.xAxisLabel = this.translate.instant('gameLabel.Electricity_Access');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.electricityDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.mainPerson.country.country+" you will access equal electricity, compared to "+ this.mainPerson.register_country.country +".";
        }
        else {
          this.translate.get('gameLabel.electricityDC2', { country: this.mainPerson.country.country, value: value, accesEleString: this.accesEleString, rcountry: this.mainPerson.register_country.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.mainPerson.country.country+" you will have  "+ value +this.accesEleString+ " access to electricity than " +this.mainPerson.register_country.country+".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else {
      this.sentenceTextWindow1 = "N/A";
      this.checkColourPatternForGraph(x, y, classIndex);
    }

    this.single = [
      {
        name: this.mainPerson.country.country,
        value: x,
      },
      {
        name: this.registeredCountry.country,
        value: y,
      },
    ];
    Object.assign(this.single);
  }

  tabClickDemo() {
    this.activeClassForDemo = "nav-item nav-link active"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = true;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.mainPerson.country.Population, this.registeredCountry.Population, 'Population', 3)
    this.single = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.Population,
      },
      {
        name: this.registeredCountry.country,
        value: this.registeredCountry.Population,
      },
    ];
    Object.assign(this.single);

  }
  tabClickperCapita() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link active";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = true;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.mainPerson.country.PrimarySchool, this.registeredCountry.PrimarySchool, 'Primary School', 1);
    this.single = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.PrimarySchool,
      },
      {
        name: this.registeredCountry.country,
        value: this.registeredCountry.PrimarySchool,
      },
    ];
    Object.assign(this.single);
  }
  tabClickLFO() {
    let num1 = this.mainPerson.country.Agriculture;
    let num2 = this.mainPerson.country.Services;
    let num3 = this.mainPerson.country.industry;
    let num4 = this.mainPerson.register_country.Agriculture;
    let num5 = this.mainPerson.register_country.Services;
    let num6 = this.mainPerson.register_country.industry;
    let largest;
    let occupation;
    let rlargest;
    let roccupation;
    if (num1 >= num2 && num1 >= num3) {
      largest = num1.toFixed(2);
      occupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num2 >= num1 && num2 >= num3) {
      largest = num2.toFixed(2);
      occupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      largest = num3.toFixed(2);
      occupation = this.translate.instant('gameLabel.Industry1');
    }

    if (num4 >= num5 && num4 >= num6) {
      largest = num1.toFixed(2);
      rlargest = num4.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num5 >= num4 && num5 >= num6) {
      rlargest = num5.toFixed(2);
      largest = num2.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      rlargest = num6.toFixed(2);
      largest = num3.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Industry1');
    }
    this.translate.get('gameLabel.LEbourForce1', { country: this.mainPerson.country.country, occupation: occupation }).subscribe(
      (str) => {
        this.labourForceString1 = str;
      })
    this.translate.get('gameLabel.LEbourForce2', { roccupation: roccupation, rcountry: this.mainPerson.register_country.country, largest: largest, country: this.mainPerson.country.country }).subscribe(
      (str) => {
        this.labourForceString2 = str;
      })
    // this.labourForceString1="In "+this.mainPerson.country.country+", most likely your occupation will be in the "+occupation+ " Sector. "
    // this.labourForceString2="If you are working in " + roccupation +" Sector in "+this.mainPerson.register_country.country +", you will have "+largest+"% jobs available in " +this.mainPerson.country.country+"."
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link active";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = true;
    this.onClickIndexRating = false;
    this.xAxisLabel = this.translate.instant('gameLabel.Occupation')
    this.colorScheme = {
      domain: ['#5AA454', '#A10A28']
    };
    this.multi = [
      {
        name: this.mainPerson.country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.mainPerson.country.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.mainPerson.country.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.mainPerson.country.industry,
          },
        ],
      },

      {
        name: this.registeredCountry.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.registeredCountry.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.registeredCountry.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.registeredCountry.industry,
          },
        ],
      },
    ];
    Object.assign(this.multi);

  }
  tabClickIndex() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link active";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickSdgData = false;
    this.onClickIndexRating = true;
    this.onButtonClickGraphChange(this.mainPerson.country.ppp, this.registeredCountry.ppp, 'PPP', 1);
    this.single = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.ppp,
      },
      {
        name: this.registeredCountry.country,
        value: this.registeredCountry.ppp,
      },
    ];
    Object.assign(this.single);


  }
  tabClickSdg() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link active "
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onClickSdgData = true;
    this.gameSummeryService.getCountryGroupListForBornAndRegCountry(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid, this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid, this.translationService.selectedLang.code).
      subscribe(
        res => {
          let bornCountry = res['bornCountry']
          let registerCountry = res['registerCountry']
          this.bornCountryGroupList = Object.values(bornCountry)
          this.registerCountryGroupList = Object.values(registerCountry)
          let result = this.bornCountryGroupList.filter(o => this.registerCountryGroupList.some(({ gpName }) => o.gpName === gpName));
          let total = this.bornCountryGroupList.length + this.registerCountryGroupList.length;
          let unique = total - result.length;
          this.groupIndex = (1 - unique / total) * 2;
          this.groupIndex = (this.groupIndex * 100).toFixed(2)
          if (result.length > 0) {
            for (let j = 0; j < result.length; j++) {
              for (let i = 0; i < this.bornCountryGroupList.length; i++) {
                if (this.bornCountryGroupList[i].gpName === result[j].gpName) {
                  this.bornCountryGroupList[i].class = "countryGroupCommon";
                }
              }
              for (let k = 0; k < this.registerCountryGroupList.length; k++) {
                if (this.registerCountryGroupList[k].gpName === result[j].gpName) {
                  this.registerCountryGroupList[k].class = "countryGroupCommon";
                }
              }
            }
          }

        })
  }


  public getArraysIntersection(a1, a2) {
    return a1.filter(function (n) { return a2.indexOf(n) !== -1; });
  }
  openFullscreen() {
    this.commonService.fullScreen();
  }

  closeFullscreen() {
    this.commonService.removeFullScreen();

  }
  onSelect(data): void {
  }

  onActivate(data): void {
  }

  onDeactivate(data): void {
  }


  getDataForWelthGraph() {
    this.view = [700, 500];
    this.multi2 = [];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabel = this.translate.instant('gameLabel.Population(%)');
    this.showYAxisLabel = true;
    this.yAxisLabel = this.translate.instant('gameLabel.Wealth');
    this.colorScheme = {
      domain: ['#0000FF', '#A10A28','#FFA500']
    };
    this.multi2.push({
      name: this.translate.instant('gameLabel.Equality_Line'),
      series: []
    });
    this.multi2.push({
      name: this.mainPerson.country.country,
      series: []
    });
     this.multi2.push({
      name: this.mainPerson.register_country.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi2[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.bornCountryArray.length; i++) {
      if (this.bornCountryArray[i].name !== "99%") {
        this.multi2[1].series.push(this.bornCountryArray[i]);
      }
    }
    for (let i = 0; i < this.registerCountryArray.length; i++) {
      if (this.registerCountryArray[i].name !== "99%") {
        this.multi2[2].series.push(this.registerCountryArray[i]);
      }
    }
    Object.assign(this.multi2);

    this.multi4 = [
      {
        "name": this.translate.instant('gameLabel.Extremely_poor'),
        "value": this.bornCountryWelathMetaData.per20 * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.poor'),
        "value": (this.bornCountryWelathMetaData.per50 - this.bornCountryWelathMetaData.per20) * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Middle_income'),
        "value": (this.bornCountryWelathMetaData.per90 - this.bornCountryWelathMetaData.per50) * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Rich'),
        "value": (this.bornCountryWelathMetaData.per99 - this.bornCountryWelathMetaData.per90) * this.bornCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Super_rich'),
        "value": (this.bornCountryWelathMetaData.per100 - this.bornCountryWelathMetaData.per99) * this.bornCountryWelathMetaData.wealth
      }
    ];
    Object.assign(this.multi4);
    let v2, v3, v4, g3: any, g4, g5: any;
    if (this.mainPerson.country.Population > 100000) {
      g3 = (this.bornCountryWelathMetaData.per50 * 100).toFixed(1);
      g4 = ((this.bornCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
      g5 = ((this.bornCountryWelathMetaData.per100 * 100) - (this.bornCountryWelathMetaData.per99 * 100)).toFixed(1);
      // v2 = (((this.mainPerson.country.Population * (50 / 100) / 1000000))).toFixed(2);
      // v3 = ((this.mainPerson.country.Population * (1 / 100)) / 1000000).toFixed(2)
      v2 = (((this.mainPerson.country.Population * (50 / 100)))).toFixed(2);
      v3 = ((this.mainPerson.country.Population * (1 / 100))).toFixed(2)
      v4 = (g5 / g3).toFixed(2);
    }
    else {
      g3 = (this.bornCountryWelathMetaData.per50 * 100).toFixed(2);
      g4 = ((this.bornCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
      g5 = ((this.bornCountryWelathMetaData.per100 * 100) - (this.bornCountryWelathMetaData.per99 * 100)).toFixed(2);
      // v2 = (((this.mainPerson.country.Population * (50 / 100) / 1000000))).toFixed(4);
      // v3 = ((this.mainPerson.country.Population * (1 / 100)) / 1000000).toFixed(4)
      v2 = (((this.mainPerson.country.Population * (50 / 100)))).toFixed(2);
      v3 = ((this.mainPerson.country.Population * (1 / 100))).toFixed(2)
      v4 = (g5 / g3).toFixed(2);
    }
    v2 = this.commonService.checkNumberFormat(v2);
    v3 = this.commonService.checkNumberFormat(v3);
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn3 = s;
      })

    this.finalStatement = this.wealthGraphSentenceBorn1 + '<br/>' + this.wealthGraphSentenceBorn2 + '<br/>' + this.wealthGraphSentenceBorn3
    //           this.wealthGraphSentenceBorn1="Botom 50% population (approximately " +v2+" MN people) owns only "+g3+"% of national wealth";
    // this.wealthGraphSentenceBorn2="Top 1% of population  (approximately "+v3+" MN people) owns "+g5+ "% of national wealth";
    // this.wealthGraphSentenceBorn3="In other words top 1% population share in national wealth is "+v4+" times that of share of national wealth of bottom 50% population";
  }

  getDataForWelthGraphCompariosion() {
    this.multi3 = [];
    this.view = [700, 400];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabel = this.translate.instant('gameLabel.Population(%)');
    this.showYAxisLabel = true;
    this.yAxisLabel = this.translate.instant('gameLabel.Wealth');
    this.colorScheme = {
      domain: ['#0000FF', '#A10A28']
    };

    this.multi3.push({
      name: this.translate.instant('gameLabel.World'),
      series: []
    });
    this.multi3.push({
      name: this.registeredCountry.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi3[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.registerCountryArray.length; i++) {
      if (this.registerCountryArray[i].name !== "99%") {
        this.multi3[1].series.push(this.registerCountryArray[i]);
      }
    }
    Object.assign(this.multi3);

    this.multi5 = [
      {
        "name": this.translate.instant('gameLabel.Extremely_poor'),
        "value": this.registerCountryWelathMetaData.per20 * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.poor'),
        "value": (this.registerCountryWelathMetaData.per50 - this.registerCountryWelathMetaData.per20) * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Middle_income'),
        "value": (this.registerCountryWelathMetaData.per90 - this.registerCountryWelathMetaData.per50) * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Rich'),
        "value": (this.registerCountryWelathMetaData.per99 - this.registerCountryWelathMetaData.per90) * this.registerCountryWelathMetaData.wealth
      },
      {
        "name": this.translate.instant('gameLabel.Super_rich'),
        "value": (this.registerCountryWelathMetaData.per100 - this.registerCountryWelathMetaData.per99) * this.registerCountryWelathMetaData.wealth
      }
    ];

    Object.assign(this.multi5);
    let v2, v3, v4, g3: any, g4, g5: any;
    g3 = (this.registerCountryWelathMetaData.per50 * 100).toFixed(1);
    g4 = ((this.registerCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
    g5 = ((this.registerCountryWelathMetaData.per100 * 100) - (this.registerCountryWelathMetaData.per99 * 100)).toFixed(1);
    // v2 = (((this.registeredCountry.Population * (50 / 100) / 1000000))).toFixed(2);
    // v3 = ((this.registeredCountry.Population * (1 / 100)) / 1000000).toFixed(2)
    v2 = (((this.registeredCountry.Population * (50 / 100)))).toFixed(2);
    v3 = ((this.registeredCountry.Population * (1 / 100))).toFixed(2)
    v4 = (g5 / g3).toFixed(2);
    v2 = this.commonService.checkNumberFormat(v2)
    v3 = this.commonService.checkNumberFormat(v3)
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg3 = s;
      })
    this.finalStatementReg = this.wealthGraphSentenceReg1 + '<br/>' + this.wealthGraphSentenceReg2 + '<br/>' + this.wealthGraphSentenceReg3
    // this.wealthGraphSentenceReg1="Botom 50% population (approximately " +v2+" MN people) owns only "+g3+"% of national wealth";
    // this.wealthGraphSentenceReg2="Top 1% of population  (approximately "+v3+" MN people) owns "+g5+ "% of national wealth";
    // this.wealthGraphSentenceReg3="In other word top 1% population share in national wealth is "+v4+" times that of share of national wealth of bottom 50% population";
  }

  yAxisTickFormatting(value) {
    return this.percentTickFormatting(value);
  }

  percentTickFormatting(val: any): string {
    return val.toLocaleString() + '%';
  }

  generateArrayForWelathGraph() {
    if (this.bornCountryWelathMetaData !== false && this.registerCountryWelathMetaData !== false) {
      this.bornCountryArray = [
        { 'name': '10%', 'value': this.bornCountryWelathMetaData.per10 },
        { 'name': '20%', 'value': this.bornCountryWelathMetaData.per20 },
        { 'name': '30%', 'value': this.bornCountryWelathMetaData.per30 },
        { 'name': '40%', 'value': this.bornCountryWelathMetaData.per40 },
        { 'name': '50%', 'value': this.bornCountryWelathMetaData.per40 },
        { 'name': '60%', 'value': this.bornCountryWelathMetaData.per60 },
        { 'name': '70%', 'value': this.bornCountryWelathMetaData.per70 },
        { 'name': '80%', 'value': this.bornCountryWelathMetaData.per80 },
        { 'name': '90%', 'value': this.bornCountryWelathMetaData.per90 },
        { 'name': '99%', 'value': this.bornCountryWelathMetaData.per99 },
        { 'name': '100%', 'value': this.bornCountryWelathMetaData.per100 },
      ]
      this.registerCountryArray = [
        { 'name': '10%', 'value': this.registerCountryWelathMetaData.per10 },
        { 'name': '20%', 'value': this.registerCountryWelathMetaData.per20 },
        { 'name': '30%', 'value': this.registerCountryWelathMetaData.per30 },
        { 'name': '40%', 'value': this.registerCountryWelathMetaData.per40 },
        { 'name': '50%', 'value': this.registerCountryWelathMetaData.per40 },
        { 'name': '60%', 'value': this.registerCountryWelathMetaData.per60 },
        { 'name': '70%', 'value': this.registerCountryWelathMetaData.per70 },
        { 'name': '80%', 'value': this.registerCountryWelathMetaData.per80 },
        { 'name': '90%', 'value': this.registerCountryWelathMetaData.per90 },
        { 'name': '99%', 'value': this.registerCountryWelathMetaData.per99 },
        { 'name': '100%', 'value': this.registerCountryWelathMetaData.per100 },
      ]

    }
    else {
      this.welthGraphDataNotGenrateFlag = true;
    }

  }

  closeTab() {
    this.activeClassForDemo = "nav-item nav-link "
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
  }


  setEducationForSummary(person) {
    let sex;
    if (person.sex === "F") {
      sex = this.translate.instant('gameLabel.her');
    }
    else {
      sex = this.translate.instant('gameLabel.his');
    }
    let education = person.education;
    if (person.age < 6) {
      this.education = this.translate.instant('gameLabel.edu1');
    }
    else {
      if (education.school.inSchool) {
        this.education = this.translate.instant('gameLabel.edu2');
      }
      else if (education.school.highSchoolGraduate && !education.school.schoolDropOut) {
        this.translate.get('gameLabel.edu3', { sex: sex }).subscribe(
          (str) => {
            this.education = str;
          })
        // this.education="has completed " +sex+" school";
      }
      else if (!education.school.highSchoolGraduate && education.school.schoolDropOut && !education.school.inSchool) {
        this.translate.get('gameLabel.edu4', { schoolCompletedYears: education.school.schoolCompletedYears }).subscribe(
          (str) => {
            this.education = str;
          })
        // this.education="is a school drop out (" +education.school.schoolCompletedYears +" years completed)"
      }

      if (education.college.inCollege) {
        this.education = this.translate.instant('gameLabel.edu5');
      }
      else if (education.college.collegeGraduate) {
        this.translate.get('gameLabel.edu6', { sex: sex }).subscribe(
          (str) => {
            this.education = str;
          })
        // this.education="has completed " +sex+" college";
      }
      else if (education.college.collegeDropOut && !education.college.collegeGraduate) {
        this.translate.get('gameLabel.edu7', { collegeCompletedYears: education.college.collegeCompletedYears }).subscribe(
          (str) => {
            this.education = str;
          })
        // this.education="is college drop out (" +education.college.collegeCompletedYears +" years completed)"
      }


      if (education.graduate.inGraduate) {
        this.education = this.translate.instant('gameLabel.edu8');
      }
      else if (education.graduate.graduateDegree && !education.graduate.graduateDropOut) {

        this.translate.get('gameLabel.edu9', { sex: sex }).subscribe(
          (str) => {
            this.education = str;
          }) // this.education="has completed "+sex+ " graduation";
      }
      else if (!education.graduate.graduateDegree && education.graduate.graduateDropOut) {
        this.translate.get('gameLabel.edu10', { graduateCompletedYears: education.graduate.graduateCompletedYears }).subscribe(
          (str) => {
            this.education = str;
          })
        // this.education="is drop out from graduate college (" +education.graduate.graduateCompletedYears +" years completed)"
      }
      return this.education;
    }
  }
  getBornSummaryData(allPerson) {

    if (allPerson[this.constantService.FAMILY_SELF].dead) {

      this.deadString = this.translate.instant('gameLabel.Unfortunately_you_died_during_birth');
      this.bornSummaryObj = {
        'sub1': this.deadString,
        'sub2':this.translate.instant('gameLabel.bornSummary53'),
        'sub3':this.translate.instant('gameLabel.bornSummary73'),
        'sub4':this.translate.instant('gameLabel.bornSummary74'),
        'sub5':this.translate.instant('gameLabel.bornSummary75'),
        'sub6':this.translate.instant('gameLabel.bornSummary76'),
        'sub7':this.translate.instant('gameLabel.bornSummary77'),
        'sub8':this.translate.instant('gameLabel.bornSummary78'),
      }
    }
    else {
      this.substring1, this.substring2, this.substring3, this.substring4, this.substring5, this.substring6 = "";
      let ssex, healthProblemSelf, sibCount, loan = '', school, unemp, ele, health, motherDead = '', status;
      let person = allPerson[this.constantService.FAMILY_SELF];
      if (person.sex === "M") {
        ssex = this.translate.instant('gameLabel.boy');
      }
      else {
        ssex = this.translate.instant('gameLabel.girl');
      }
      //diet
      let localan;
      if (this.mainPerson.expense.diet.dietIndex < 6 && this.mainPerson.expense.diet.dietIndex > 3) {
        localan = this.translate.instant('gameLabel.an');
      }
      else {
        localan = this.translate.instant('gameLabel.a');
      }


      // Your health as a newborn is not good.
      if (person.traits.health >= 80) {
        if (!this.modalWindowService.ShowOldLifeFlag) {
          this.substring2 = this.translate.instant('gameLabel.Your_health_as_a_newborn_is_good');
        }
        else if (this.modalWindowService.ShowOldLifeFlag) {
          this.substring2 = this.translate.instant('gameLabel.bornSummary59');
        }
      }
      else if (person.traits.health > 40 && person.traits.health < 80) {
        if (!this.modalWindowService.ShowOldLifeFlag) {
          this.substring2 = this.translate.instant('gameLabel.Your_health_as_a_newborn_is_not_good');
        }
        else if (this.modalWindowService.ShowOldLifeFlag) {
          this.substring2 = this.translate.instant('gameLabel.bornSummary57');
        }
      }
      else if (person.traits.health < 40) {
        if (!this.modalWindowService.ShowOldLifeFlag) {
          this.substring2 = this.translate.instant('gameLabel.Your_health_as_a_newborn_is_bad');
        }
        else if (this.modalWindowService.ShowOldLifeFlag) {
          this.substring2 = this.translate.instant('gameLabel.bornSummary58');
        }
      }
      if (person.traits.intelligence > 50) {
        this.substring2 = this.translate.instant('gameLabel.You_are_an_intelligent_child');
      }

      if (person.health_disease != null && !person.dead) {
        let keys = Object.keys(person.health_disease);
        let le = keys.length;
        if (keys.length !== 0) {
          for (let h = 0; h < keys.length; h++) {
            if (this.self.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblemSelf == "") {
                healthProblemSelf = healthProblemSelf + this.self.health_disease[keys[h]].diseaseName;
              }
              else {
                if (i === keys.length) {
                  healthProblemSelf = healthProblemSelf + " and  " + this.self.health_disease[keys[h]].diseaseName;
                }
                else {
                  healthProblemSelf = healthProblemSelf + ", " + this.self.health_disease[keys[h]].diseaseName;
                }
              }
            }
            if (this.substring1 !== '') {
              healthProblemSelf = healthProblemSelf.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');

              this.translate.get('gameLabel.You are suffering from', { healthProblemSelf: healthProblemSelf }).subscribe(
                (str) => {
                  this.substring4 = str;
                })
            }
            // this.substring1=" You are suffering from "+healthProblemSelf+". ";
          }
        }
      }

      this.substring18 = this.substring2 + this.substring1 + this.substring3;
      let deadSib=this.gameSummeryService.checkSiblingDeadAtAge17(this.allPerson);
      let allSib=person[this.constantService.FAMILY_SIBLING].length;
      let sib;
      if(person[this.constantService.FAMILY_SIBLING].length>0){
      if(deadSib===allSib){
        sib=0
      }
      else if(deadSib<allSib){
        sib=allSib-deadSib;
      }
    }
    else{
      sib=0
    }
      if (sib == 0) {
        sibCount = this.translate.instant('gameLabel.no_siblings');
      }
      else if (sib === 1) {
        sibCount = sib+ " " + this.translate.instant('gameLabel.sibling') + " ";
      }
      else {
        sibCount = sib + " " + this.translate.instant('gameLabel.siblings') + " ";

      }
      if (sib > 0) {
        if (sib == 1) {
          let age = '', sex;
          for (var i = 0; i < person[this.constantService.FAMILY_SIBLING].length; i++) {
            this.sibString = this.mainPerson.siblings[i];
            this.sib = this.homeService.allPerson[this.sibString];
            if(!this.sib.dead){
            if (this.sib.sex === "M") {
              sex = this.translate.instant('gameLabel.brother');
            }
            else {
              sex = this.translate.instant('gameLabel.sister');
            }
            age = this.sib.age;
          this.translate.get('gameLabel.who_is', { age: age }).subscribe(
            (str) => {
              this.substring4 = str;
            })
          }
          }
          // this.substring4=""+sex+" who is "+age+" years old. "
        }
        // Your mother's name is Elena, your father's name is Gilberto, and you have two siblings who are 3 and 1 years old.
        else if (sib == 2) {
          let age = '';
          for (var i = 0; i < person[this.constantService.FAMILY_SIBLING].length; i++) {
            this.sibString = this.mainPerson.siblings[i];
            this.sib = this.homeService.allPerson[this.sibString];
            if(!this.sib.dead){
            if (age != '') {
              age = age + " " + this.translate.instant('gameLabel.and') + " " + this.sib.age;
            } else {
              age = this.sib.age;
            }
          this.translate.get('gameLabel.who_are', { age: age }).subscribe(
            (str) => {
              this.substring4 = str;
            })
          }
          // this.substring4="who are "+age+" year old. "
          }
        }
        else {
          let age = '';
          for (var i = 0; i < person[this.constantService.FAMILY_SIBLING].length; i++) {
            this.sibString = this.mainPerson.siblings[i];
            this.sib = this.homeService.allPerson[this.sibString];
            if(!this.sib.dead){
            if (i == person[this.constantService.FAMILY_SIBLING].length) {
              if (age != '') {
                age = age + " " + this.translate.instant('gameLabel.and') + " " + this.sib.age;
              } else {
                age = this.sib.age;
              }
            }
            else {
              if (age != '') {
                age = age + ", " + this.sib.age;
              } else {
                age = this.sib.age;
              }
            }
          }
          this.translate.get('gameLabel.who_are', { age: age }).subscribe(
            (str) => {
              this.substring4 = str;
            })
          }
          // this.substring4="who are "+age+" year old. "
        }
        if(this.substring4===''){
          this.substring4="."
        }
      }
      if (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome >= 0 && allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses <= 0
        && allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
        this.currentSavingRate = 0;
      }
      else if (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
        this.currentSavingRate = 0;
      }
      else {
        this.currentSavingRate = (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome - allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses) / (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome) * 100
      }

      this.substring8 = this.translate.instant('gameLabel.Public_amenities_like')
      if (!person.amenities.safeWater) {
        this.substring9 = this.translate.instant('gameLabel.safe_drinking_water')
        // this.substring9="safe_drinking_water"
      }
      if (!person.amenities.basicSanitation) {
        if (this.substring9 !== '') {
          this.substring9 = this.substring9 + ", " + this.translate.instant('gameLabel.sanitation')
        }
        else {
          this.substring9 = this.translate.instant('gameLabel.sanitation')
        }
      }
      if (!person.amenities.healthService) {
        if (this.substring9 !== '') {
          this.substring9 = this.substring9 + ", " + this.translate.instant('gameLabel.healthcare')

        }
        else {
          this.substring9 = this.translate.instant('gameLabel.healthcare')
          // this.substring9="healthcare "
        }
      }
      if (this.substring9 !== '') {
        this.substring9 = this.substring9.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
        this.translate.get('gameLabel.are_not_available_to_your_family', { substring8: this.substring8, substring9: this.substring9 }).subscribe(
          (str) => {
            this.substring10 = str;
          })
        // this.substring10=this.substring8+this.substring9+"are not available to your family. ";
      }
      else {
        this.substring10 = ""
      }
      if (person.expense.householdLiabilities > 0) {
        let loanvalue = (person.expense.householdLiabilities / person.country.ExchangeRate).toFixed(0);
        this.translate.get('gameLabel.loanSte1', { householdLiabilities: this.commonService.checkNumberFormat(person.expense.householdLiabilities), currencyCode: person.country.CurrencyPlural + " ($" + loanvalue + ")" }).subscribe(
          (str) => {
            loan = str;
          })
        // There is a debt of {{householdLiabilities}} {{currencyCode}} arising from a loan taken by your family.
        //  loan="There is a debt of "+person.expense.householdLiabilities.toFixed(0)+" "+person.country.currency_code+" arising from a loan taken by your family. "
      }
      else {
        loan = this.translate.instant('gameLabel.loanSte2')
        // loan="Your parents have never taken out loans so far. Therefore your family isn't in debt. ";
      }



      status = this.translate.instant('gameLabel.a')
      this.getDietString(person.expense.diet.dietIndex);
      this.getShelterString(person.expense.shelter.shelterIndex);
      // Your family is living in the {{shelter}} accommodation and has decided to have {{localan}} {{dietName}} food plan. At the time of your birth your family is saving {{currentSavingRate}} percent of your family's income. "
      // this.substring13="Your family is living in the "+ this.financeService.shelter[person.expense.shelter.shelterIndex].name+" accommodation and has decided to have "+localan +" "+this.financeService.diet[person.expense.diet.dietIndex].name+ " food plan. At the time of your birth your family is saving "+ this.currentSavingRate.toFixed(2) + " percent of your family's income. ";

      if (this.currentSavingRate >= 0) {
        if (!this.modalWindowService.ShowOldLifeFlag) {
          this.translate.get('gameLabel.bornSummary1', { shelterString: this.shelterString, dietString: this.dietSubString, currentSavingRate: this.currentSavingRate.toFixed(2) }).subscribe(
            (str) => {
              this.substring13 = str;
            })
        }
        else if (this.modalWindowService.ShowOldLifeFlag) {
          this.translate.get('gameLabel.bornSummary60', { shelterString: this.shelterString, dietString: this.dietSubString, currentSavingRate: this.currentSavingRate.toFixed(2) }).subscribe(
            (str) => {
              this.substring13 = str;
            })
        }
      }
      else {
        this.translate.get('gameLabel.bornSummary37', { shelterString: this.shelterString, dietString: this.dietSubString, currentSavingRate: this.currentSavingRate.toFixed(2) }).subscribe(
          (str) => {
            this.substring13 = str;
          })
      }
      if (allPerson[this.constantService.FAMILY_MOTHER].dead || allPerson[this.constantService.FAMILY_FATHER].dead) {
        if (this.modalWindowService.ShowOldLifeFlag) {
          if (this.allPerson[this.constantService.FAMILY_MOTHER].dead && this.allPerson[this.constantService.FAMILY_FATHER].dead) {
            this.translate.get('gameLabel.bornSummary63', { mName: allPerson[this.constantService.FAMILY_MOTHER].first_name, fName: allPerson[this.constantService.FAMILY_FATHER].first_name, fAge: allPerson[this.constantService.FAMILY_FATHER].age, sfAge:   allPerson[this.constantService.FAMILY_FATHER].age-allPerson[this.constantService.FAMILY_FATHER].original_age, smAge:   allPerson[this.constantService.FAMILY_MOTHER].age-allPerson[this.constantService.FAMILY_MOTHER].original_age, mAge: allPerson[this.constantService.FAMILY_MOTHER].age, sAge: 17 }).subscribe(
              (str) => {
                motherDead = str;
              })
          }
          else if (this.allPerson[this.constantService.FAMILY_MOTHER].dead) {
            this.translate.get('gameLabel.bornSummary61', { mName: allPerson[this.constantService.FAMILY_MOTHER].first_name, mAge: allPerson[this.constantService.FAMILY_MOTHER].age, sAge: allPerson[this.constantService.FAMILY_MOTHER].age - allPerson[this.constantService.FAMILY_MOTHER].original_age }).subscribe(
              (str) => {
                motherDead = str;
              })
            this.translate.get('gameLabel.bornSummary72', { mName: this.allPerson[this.constantService.FAMILY_MOTHER].first_name, fNAME: allPerson[this.constantService.FAMILY_FATHER].first_name, sibCount: sibCount }).subscribe(
              (str) => {
                motherDead = motherDead + " " + str + this.substring4;
              })
          }
          else if (allPerson[this.constantService.FAMILY_FATHER].dead) {
            this.translate.get('gameLabel.bornSummary62', { fName: allPerson[this.constantService.FAMILY_FATHER].first_name, fAge: allPerson[this.constantService.FAMILY_FATHER].age, sAge: allPerson[this.constantService.FAMILY_FATHER].age-allPerson[this.constantService.FAMILY_FATHER].original_age  }).subscribe(
              (str) => {
                motherDead = str;
              })
            this.translate.get('gameLabel.bornSummary71', { mName: this.allPerson[this.constantService.FAMILY_MOTHER].first_name, fNAME: allPerson[this.constantService.FAMILY_FATHER].first_name, sibCount: sibCount }).subscribe(
              (str) => {
                motherDead = motherDead + " " + str + this.substring4;
              })
          }
        }
        else {
          this.translate.get('gameLabel.motherDead', { mName: allPerson[this.constantService.FAMILY_MOTHER].first_name }).subscribe(
            (str) => {
              motherDead = str;
            })
        }
        this.substring7 = " " + motherDead;
        // motherDead="Sadly, your mother "+allPerson[this.constantService.FAMILY_MOTHER].first_name+" passed away while giving birth to you. "
      }
      else {

        // this.substring5="Your mother's name is "+ allPerson[this.constantService.FAMILY_MOTHER].first_name+ ", your father's name is " +allPerson[this.constantService.FAMILY_FATHER].first_name+" and you have " + sibCount +this.substring4
        this.translate.get('gameLabel.bornSummary2', { mName: this.allPerson[this.constantService.FAMILY_MOTHER].first_name, fNAME: allPerson[this.constantService.FAMILY_FATHER].first_name, sibCount: sibCount }).subscribe(
          (str) => {
            this.substring7 = str;
            if(this.substring4!==''){
            this.substring7 = this.substring7 + this.substring4;
            }
            else{
              this.substring7 = this.substring7 +".";
            }
          })
      }

      //sib dead block
      // "bornSummary66": "Your sibling died when {{he/she}} was {{sibAge}} years old.",
      //   "bornSummary67": "Your {{sibDeadCount}} siblings died in the last {{sAge}} years",

      if (this.allPerson[this.constantService.FAMILY_SELF].siblings.length !== 0) {
        let sib = this.allPerson[this.constantService.FAMILY_SELF].siblings
        for (let i = 0; i < sib.length; i++) {
          if (this.allPerson[sib[i]].dead) {
            this.sibDeadCount = this.sibDeadCount + 1;
            this.sibDeadIndex = i
          }
        }
        if (this.sibDeadCount > 1) {
          this.translate.get('gameLabel.bornSummary67', { totalSibCount:this.allPerson[this.constantService.FAMILY_SELF].siblings.length,sibDeadCount: this.sibDeadCount, sAge: this.allPerson[this.constantService.FAMILY_SELF].age }).subscribe(
            (str) => {
              this.substring7 = this.substring7 + " " + str;
            })
        }
        else if (this.sibDeadCount === 1) {
          let heShe;
          if (this.homeService.allPerson[this.allPerson[this.constantService.FAMILY_SELF].siblings[this.sibDeadIndex]].sex === "M") {
            heShe = "he"
          }
          else {
            heShe = "she"
          }
          let sibAge = this.homeService.allPerson[this.allPerson[this.constantService.FAMILY_SELF].siblings[this.sibDeadIndex]].age
          this.translate.get('gameLabel.bornSummary66', { heshe: heShe, sibAge: sibAge }).subscribe(
            (str) => {
              this.substring7 = this.substring7 + " " + str;
            })
        }
      }

      //taken care block
      // "bornSummary64": "You were taken care by your {{takenCareString}} after your parent's death.",
      //   "bornSummary65": "Your siblings were also taken care of the  {{takenCareString}}.",

      if (this.allPerson[this.constantService.FAMILY_MOTHER].dead && this.allPerson[this.constantService.FAMILY_FATHER].dead) {
        if (this.allPerson[this.constantService.FAMILY_SELF].orphanIndex > 0) {
          let orphaneString = this.gameSummeryService.orpaheStringArray(this.allPerson[this.constantService.FAMILY_SELF].orphanIndex)


          if (this.sibDeadCount > 0) {
            this.translate.get('gameLabel.bornSummary65', { takenCareString: orphaneString }).subscribe(
              (str) => {
                this.substring7 = this.substring7 + " " + str;
              })

          }
          else {
            this.translate.get('gameLabel.bornSummary64', { takenCareString: orphaneString }).subscribe(
              (str) => {
                this.substring7 = this.substring7 + " " + str;
              })
          }
        }

      }




      if (this.perdayEarning <= 0) {
        if (!this.modalWindowService.ShowOldLifeFlag) {
          this.translate.get('gameLabel.bornSummary33', { status: status, status1: this.commonService.getSatusString(this.status), perdayEarning: (this.perdayEarning).toFixed(2) }).subscribe(
            (str) => {
              this.substring7 = this.substring7 + " " + str;
            })
        }
        else if (this.modalWindowService.ShowOldLifeFlag) {
          this.translate.get('gameLabel.bornSummary55', { status: status, status1: this.commonService.getSatusString(this.status), perdayEarning: (this.perdayEarning).toFixed(2) }).subscribe(
            (str) => {
              this.substring7 = this.substring7 + " " + str;
            })
        }
      }
      else {
        if (!this.modalWindowService.ShowOldLifeFlag) {
          // this.substring7="You are born into "+status+" "+ this.commonService.getSatusString(this.status)+" family with a daily income of $"+(this.perdayEarning).toFixed(2)+ " per day per person. " 
          this.translate.get('gameLabel.bornSummary3', { status: status, status1: this.commonService.getSatusString(this.status), perdayEarning: (this.perdayEarning).toFixed(2) }).subscribe(
            (str) => {
              this.substring7 = this.substring7 + " " + str;
            })
        }
        else if (!this.modalWindowService.ShowOldLifeFlag) {
          this.translate.get('gameLabel.bornSummary56', { status: status, status1: this.commonService.getSatusString(this.status), perdayEarning: (this.perdayEarning).toFixed(2) }).subscribe(
            (str) => {
              this.substring7 = this.substring7 + " " + str;
            })

        }
      }
      // You are born into {{status}} {{status}} family with a daily income of ${{perdayEarning}} per day per person. 
      let expenceFoodPer;
      let foodPer = "";

      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense > 0 && this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome > 0) {
        expenceFoodPer = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense * 100 / this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome
        this.translate.get('gameLabel.bornSummary4', { expenceFoodPer: expenceFoodPer.toFixed(0) }).subscribe(
          (str) => {
            foodPer = str;
          })
        // foodPer="Your family is currently spending "+expenceFoodPer.toFixed(0)+ " percent of its total income on diet and food."
      }
      else if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense === 0 || this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome === 0) && this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth < 0) {
        expenceFoodPer = 0;
        foodPer = this.translate.instant('gameLabel.bornSummary5')
        // foodPer="Your family is a very poor family, and cannot afford to buy food right now. Somehow, your family must be getting food from free sources or finding food in some other way."
      }

      if (!this.modalWindowService.ShowOldLifeFlag) {
        // this.substring="Your name is "+person.first_name+", and you are born in a country called " +person.country.country +" as a "+ssex +". "+this.substring7+this.substring18+this.substring5+this.substring10+this.substring13+loan+motherDead+foodPer;
        this.translate.get('gameLabel.bornSummary6', { first_name: person.first_name, country: person.country.country, ssex: ssex, substring7: this.substring7, substring18: this.substring18, substring5: this.substring5, substring10: this.substring10, substring13: this.substring13, loan: loan, foodPer: foodPer }).subscribe(
          (str) => {
            this.substring = str;
          })
      }
      else if (this.modalWindowService.ShowOldLifeFlag) {
        // this.substring ="Your name is {{name}}, and you were born today as a {{sex}} {{age}} years ago in a country called {{country}}. "
        this.translate.get('gameLabel.bornSummary54', { name: person.first_name, sex: ssex, age: person.age, country: person.country.country, substring7: this.substring7, substring18: this.substring18, substring5: this.substring5, substring10: this.substring10, substring13: this.substring13, loan: loan, foodPer: foodPer }).subscribe(
          (str) => {
            this.substring = str;
          })
      }

      // this.substring6="Your father "+ allPerson[this.constantService.FAMILY_FATHER].first_name+", " + this.fatherEducation+" and he is currently "+ job+". Your mother "+allPerson[this.constantService.FAMILY_MOTHER].first_name +", "+ this.motherEducation+" and she is currently "+mjob+". " 
      let job, mjob;
      this.fatherEducation = this.setEducationForSummary(allPerson[this.constantService.FAMILY_FATHER]);
      this.motherEducation = this.setEducationForSummary(allPerson[this.constantService.FAMILY_MOTHER]);

      if (allPerson[this.constantService.FAMILY_FATHER].job.JobId === 999 || allPerson[this.constantService.FAMILY_FATHER].job.JobId === 133 || allPerson[this.constantService.FAMILY_FATHER].job.JobId === 132) {
        job = this.translate.instant('gameLabel.bornSummary32')

      }
      else {
        job = allPerson[this.constantService.FAMILY_FATHER].job.Job_displayName;
      }
      if (allPerson[this.constantService.FAMILY_MOTHER].job.JobId === 999 || allPerson[this.constantService.FAMILY_MOTHER].job.JobId === 133 || allPerson[this.constantService.FAMILY_MOTHER].job.JobId === 132) {
        mjob = this.translate.instant('gameLabel.bornSummary31')

      }
      else {
        mjob = allPerson[this.constantService.FAMILY_MOTHER].job.Job_displayName
      }

      if (!this.allPerson[this.constantService.FAMILY_FATHER].dead) {
        this.translate.get('gameLabel.bornSummary12', { fname: allPerson[this.constantService.FAMILY_FATHER].first_name, fatherEducation: this.fatherEducation }).subscribe(
          (str) => {
            this.substring6 = str;
          })
        if (allPerson[this.constantService.FAMILY_FATHER].job.JobId === 999 || allPerson[this.constantService.FAMILY_FATHER].job.JobId === 133 || allPerson[this.constantService.FAMILY_FATHER].job.JobId === 132) {
          this.substring6 = this.substring6 + job

        }
        else {
          this.translate.get('gameLabel.bornSummary27', { job: job }).subscribe(
            (str) => {
              this.substring6 = this.substring6 + str;
            })
        }
      }

      if (!this.allPerson[this.constantService.FAMILY_MOTHER].dead) {
        this.translate.get('gameLabel.bornSummary28', { mName: allPerson[this.constantService.FAMILY_MOTHER].first_name, motherEducation: this.motherEducation }).subscribe(
          (str) => {
            this.substring6 = this.substring6 + str;
          })

        if (allPerson[this.constantService.FAMILY_MOTHER].job.JobId === 999 || allPerson[this.constantService.FAMILY_MOTHER].job.JobId === 133 || allPerson[this.constantService.FAMILY_MOTHER].job.JobId === 132) {
          this.substring6 = this.substring6 + mjob

        } else {
          this.translate.get('gameLabel.bornSummary29', { mjob: mjob }).subscribe(
            (str) => {
              this.substring6 = this.substring6 + str;
            })

        }
      }

      // "bornSummary68": "Your current education is {{selfEducation}}.",
      //   "bornSummary69": "Currently you are working as a {{selfJob}}.",
      if (this.modalWindowService.ShowOldLifeFlag) {
        this.selfEducation = this.setEducationForSummarySelf(allPerson[this.constantService.FAMILY_SELF]);

        if (allPerson[this.constantService.FAMILY_SELF].job.JobId === 999 || allPerson[this.constantService.FAMILY_SELF].job.JobId === 133 || allPerson[this.constantService.FAMILY_SELF].job.JobId === 132) {
          this.selfJob = this.translate.instant('gameLabel.bornSummary70')

        }
        else {
          this.selfJob = allPerson[this.constantService.FAMILY_SELF].job.Job_displayName;
        }
        if (!this.allPerson[this.constantService.FAMILY_SELF].dead) {
          this.substring6 = this.substring6 + " " + this.selfEducation;

          if (allPerson[this.constantService.FAMILY_SELF].job.JobId === 999 || allPerson[this.constantService.FAMILY_SELF].job.JobId === 133 || allPerson[this.constantService.FAMILY_SELF].job.JobId === 132) {
            this.substring6 = this.substring6 + " " + this.selfJob

          } else {
            this.translate.get('gameLabel.bornSummary69', { selfJob: this.selfJob }).subscribe(
              (str) => {
                this.substring6 = this.substring6 + " " + str;
              })

          }
        }
      }


      if (this.healthProblemMother === 'None' && this.healthProblemFather === 'None') {
        if (!this.allPerson[this.constantService.FAMILY_FATHER].dead && !this.allPerson[this.constantService.FAMILY_MOTHER].dead) {
          this.substring11 = this.translate.instant('gameLabel.bornSummary8')
          // this.substring11="Your mother is in good health. Your father is also in good health. "
        }
      }
      else if (this.healthProblemMother !== 'None') {
        if (!this.allPerson[this.constantService.FAMILY_MOTHER].dead) {
          this.translate.get('gameLabel.bornSummary10', { healthProblemMother: this.healthProblemMother }).subscribe(
            (str) => {
              this.substring11 = str;
            })
        }
        // this.substring11="Your mother is suffering from " +this.healthProblemMother+". ";

      }
      else {
        if (!this.allPerson[this.constantService.FAMILY_FATHER].dead) {
          this.substring11 = this.translate.instant('gameLabel.bornSummary9')
          // this.substring11="Your mother is in good health. "
        }
      }

      if (!this.allPerson[this.constantService.FAMILY_FATHER].dead) {
        if (this.healthProblemFather !== 'None') {
          this.translate.get('gameLabel.bornSummary11', { healthProblemFather: this.healthProblemFather }).subscribe(
            (str) => {
              this.substring12 = str;
            })
          // this.substring12="Your father is suffering from "+this.healthProblemFather+". ";
        }
      }

      let exp;
      if (person.sex === "M") {
        exp = person.country.MaleLifeExpectancy
      }
      else {
        exp = person.country.FemaleLifeExpectancy
      }
      // this.substring14="In " +person.country.country+" the life expectancy for men is "+ person.country.MaleLifeExpectancy +" years and the life expectancy for women is "+person.country.FemaleLifeExpectancy+" years. "
      // In {{country}} the life expectancy for men is {{MaleLifeExpectancy}} years and the life expectancy for women is {{FemaleLifeExpectancy}} years. 
      this.translate.get('gameLabel.bornSummary14', { country: person.country.country, MaleLifeExpectancy: person.country.MaleLifeExpectancy, FemaleLifeExpectancy: person.country.FemaleLifeExpectancy }).subscribe(
        (str) => {
          this.substring14 = str;
        })
      let d = (((this.mainPerson.country.ProdperCapita) * this.mainPerson.country.AverageFamilyCount) * this.mainPerson.country.ExchangeRate).toFixed(2);
      let dummy = this.commonService.checkNumberFormat(d)
      this.translate.get('gameLabel.bornSummary15', {
        country: person.country.country, AverageFamilyCount: ((this.mainPerson.country.ProdperCapita) * this.mainPerson.country.AverageFamilyCount).toFixed(0), AverageFamilyCount1: dummy,
        currencyCode: person.country.CurrencyPlural, status: status, perdayEarning: this.perdayEarning.toFixed(2), count_of_household_members: ((this.perdayEarning * 365) * this.mainPerson.count_of_household_members).toFixed(0)
      }).subscribe(
        
        (str) => {
          this.substring15 = str;
        })
      // {{country}}'s average household income is $ {{AverageFamilyCount}} ({{AverageFamilyCount}}{{currency_code}}) per year.  The family you are born into is {{status}} family that earns ${{perdayEarning}} a day per person which is  {{count_of_household_members}} dollars/year.",
      this.substring15 = this.substring15 + this.incomeGraphStringThree + " " + this.wealthGraphSentenceBorn2 + " " + this.wealthGraphSentenceBorn3


      let value1 = this.generateStringValue(person.country.PrimarySchool, this.registeredCountry.PrimarySchool);
      this.generateStringSubString(person.country.PrimarySchool, this.registeredCountry.PrimarySchool);
      if (Math.round(value1) === 0) {
        school = this.translate.instant('gameLabel.bornSummary16')
        // school="you will have equal access to attend Primary School, ";
      } else {
        this.translate.get('gameLabel.bornSummary17', { value1: value1, primarySchoolString: this.primarySchoolString }).subscribe(
          (str) => {
            school = str;
          })
        // school="you will have "+ value1+this.primarySchoolString+ " access to attend Primary School, ";
      }



      let value = this.generateStringValue(person.country.UnEmploymentRate, this.registeredCountry.UnEmploymentRate);
      value = parseFloat(value);
      this.generateStringSubString(person.country.UnEmploymentRate, this.registeredCountry.UnEmploymentRate);
      if (Math.round(value) === 0) {
        unemp = this.translate.instant('gameLabel.bornSummary18')
        // unemp="you have equal chance to be unemployed, ";
      }
      else {
        this.translate.get('gameLabel.bornSummary19', { value: value, stringRetutnText: this.stringRetutnText }).subscribe(
          (str) => {
            unemp = str;
          })
        // unemp=value +this.stringRetutnText+ " to be unemployed, ";
      }

      let value2 = this.generateStringValue(person.country.PerCapitaElectricityConsumption, this.registeredCountry.PerCapitaElectricityConsumption);
      value2 = parseFloat(value2);
      this.generateStringSubString(person.country.PerCapitaElectricityConsumption, this.registeredCountry.PerCapitaElectricityConsumption);
      if (Math.round(value2) === 0) {
        ele = this.translate.instant('gameLabel.boenSummary20')
        // ele="will have equal access to electricity, ";
      }
      else {
        this.translate.get('gameLabel.bornSummary21', { value2: value2, accessEleString: this.accesEleString }).subscribe(
          (str) => {
            ele = str;
          })
        // ele= value2 +this.accesEleString+ " access to electricity, ";
      }


      let value3 = this.generateStringValue(person.country.PerCapitaHealthConsumption, this.registeredCountry.PerCapitaHealthConsumption,);
      value3 = parseFloat(value3);
      this.generateStringSubString(person.country.PerCapitaHealthConsumption, this.registeredCountry.PerCapitaHealthConsumption,);
      if (Math.round(value3) === 0) {
        health = this.translate.instant('gameLabel.bornSummary22')
        // health="spend equal money on health care, ";
      }
      else {
        this.translate.get('gameLabel.bornSummary23', { value3: value3, primarySchoolString: this.primarySchoolString }).subscribe(
          (str) => {
            health = str;
          })
        // health="spend "+ value3 +this.primarySchoolString+ " money on health care. ";
      }
      if(person.country.countryid!==person.register_country.countryid){
      this.translate.get('gameLabel.bornSummary24', { rcountry: person.register_country.country, country: this.mainPerson.country.country, school: school, unemp: unemp, ele: ele, health: health }).subscribe(
        (str) => {
          this.substring16 = str;
        })
      }
      // this.substring16="Compared to your home country,  "+person.register_country.country+", "+school+unemp+ele+health;
      this.translate.get('gameLabel.bornSummary25', { country: person.country.country, corruption: person.country.corruption }).subscribe(
        (str) => {
          this.substring17 = str;
        })
      // this.substring17=person.country.country+", the country you are born in, " + "ranks (1 is worst) " +person.country.corruption+" in corruption out of 193 countries in the world. "
      this.bornSummaryObj = {
        'sub1': this.substring,
        'sub2': this.substring6,
        'sub3': this.substring11,
        'sub4': this.substring12,
        'sub5': this.substring15,
        'sub6': this.substring16,
        'sub7': this.substring17 + this.substring14,
      }
    }


  }

  getDietString(dietIndex) {
    if (dietIndex == 0) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary34')
    }
    else if (dietIndex == 1) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary36')
    }
    else if (dietIndex == 2) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary38')
    }
    else if (dietIndex == 3) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary40')
    }
    else if (dietIndex == 4) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary41')
    }
    else if (dietIndex == 5) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary42')
    }
    else if (dietIndex == 6) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary43')
    }
    else if (dietIndex == 7) {
      this.dietSubString = this.translate.instant('gameLabel.bornSummary44')
    }

  }


  getShelterString(shelterIndex) {
    if (shelterIndex == 0) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary45')
    }
    else if (shelterIndex == 1) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary46')
    }
    else if (shelterIndex == 2) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary47')
    }
    else if (shelterIndex == 3) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary48')
    }
    else if (shelterIndex == 4) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary49')
    }
    else if (shelterIndex == 5) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary50')
    }
    else if (shelterIndex == 6) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary51')
    }
    else if (shelterIndex == 7) {
      this.shelterString = this.translate.instant('gameLabel.bornSummary52')
    }
  }


  createBornScreenArray() {
    this.bornScreenArray = ['visa-page', 'country-register-country-map', 'born-screen', 'country-map', 'wealth-grpah', 'wealth-grpah-comparision',
      'country-disparities', 'country-SDG', 'eco-status', 'family-tree', 'family-income-grpah', 'country-week', 'family-amenities', 'born-summary']
    this.bornScreenButtonArray.push({
      'visa-page': '',
      'country-register-country-map': this.translate.instant('gameLabel.Fly_to_be_born'),
      'born-screen': this.translate.instant('gameLabel.Know_who_you_are'),
      'country-map': this.translate.instant('gameLabel.Your_Country_on_the_Map'),
      'wealth-grpah': this.translate.instant('gameLabel.View_Your_Country_Wealth_Graph'),
      'wealth-grpah-comparision': this.translate.instant('gameLabel.View_Disparity_Between_Countries'),
      'country-disparities': this.translate.instant('gameLabel.Comparision_of_Country_Attributes'),
      'country-SDG': this.translate.instant('gameLabel.View_SDG'),
      'eco-status': this.translate.instant('gameLabel.View_Economical_Status'),
      'family-tree': this.translate.instant('gameLabel.View_Your_Family'),
      'family-income-grpah': this.translate.instant('gameLabel.View_Income_Graph'),
      'country-week': this.translate.instant('gameLabel.Country_challenges'),
      'family-amenities': this.translate.instant('gameLabel.View_Family_Amenities'),
      'born-summary': this.translate.instant('gameLabel.View_born_summary')
    })


  }
  updateBornSummaryArray() {
    this.createBornScreenArray();
    if (!this.mainPerson.game_settings.Screen5) {
      let index = this.bornScreenArray.indexOf("country-map");
      this.bornScreenArray.splice(index, 1)
    }
    if (!this.mainPerson.game_settings.Screen6) {
      let index = this.bornScreenArray.indexOf("wealth-grpah");
      this.bornScreenArray.splice(index, 1)
    }

    if (!this.mainPerson.game_settings.Screen7) {
      let index = this.bornScreenArray.indexOf("wealth-grpah-comparision");
      this.bornScreenArray.splice(index, 1)
    }

    if (!this.mainPerson.game_settings.Screen8) {
      let index = this.bornScreenArray.indexOf("country-disparities");
      this.bornScreenArray.splice(index, 1)
    }

    if (!this.mainPerson.game_settings.Screen9) {
      let index = this.bornScreenArray.indexOf("country-SDG");
      this.bornScreenArray.splice(index, 1)
    }

  }
  openIframeWindow(link, title) {
    let title1 = "'" + title + "'";
    this.translate.get('gameLabel.country_group_title', { gName: title1 }).subscribe(
      (str) => {
        this.countryGroupTitle = str;
      })
    this.countryGroupIframeFlag = true;
    this.webLink = link;
  }

  closeIframWindow() {
    this.countryGroupIframeFlag = false;

  }

  knowMore() {
    this.modalWindowService.showKonwMoreFlag = true;
  }

  ngOnDestroy() {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  formatDataLabel(value) {
    return "$" + value;
  }

  moreInfo() {
    this.moreInfoFlag = true;
    let s1 = this.translate.instant('gameLabel.status_stm_tlp1');
    let s2 = this.translate.instant('gameLabel.status_stm_tlp2');
    let s3 = this.translate.instant('gameLabel.status_stm_tlp3');
    let s4 = this.translate.instant('gameLabel.status_stm_tlp4');
    let s5 = this.translate.instant('gameLabel.status_stm_tlp5');
    this.moreInfoTitle = s1 + "<br><br>"
    this.moreInfoStm = s2 + "<br><br>" + s3 + "<br><br>" + s4 + "<br><br>" + s5;
  }

  calculateTableValues() {
    this.classForStatusBorderOne = "newTableFontSize";
    this.classForStatusBorderTwo = "newTableFontSize";
    this.classForStatusBorderThree = "newTableFontSize";
    this.classForStatusBorderFour = "newTableFontSize";
    this.classForStatusBorderFive = "newTableFontSize";
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country.length > 8) {
      this.bornCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Code;
    }
    else {
      this.bornCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country.length > 8) {
      this.registerCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.Code;
    }
    else {
      this.registerCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    }
    this.chanceOfPoorR = this.registerStatusData.chanceOfPoor * 100;
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    let firstR = (f3R);
    this.firstR = firstR;
    let secondR = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    let thirdR = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    let fourthR = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    this.rOne = (firstR + secondR) / (firstR);
    this.rTwo = (secondR + thirdR) / (firstR);
    this.rThree = (thirdR + fourthR) / (firstR);
    this.rFour = (fourthR) / (firstR);

    //value for born
    let first = this.commonService.firstValueI;
    let second = this.commonService.secondValueI;
    let third = this.commonService.thirdValueI;
    let fourth = this.commonService.fourthValueI;
    this.bOne = (first + second) / (first);
    this.bTwo = (second + third) / (first);
    this.bThree = (third + fourth) / (first);
    this.bFour = (fourth) / (first);

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 0) {
      this.classForStatusBorderOne = "table-bordered-red newTableFontSize"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 1) {
      this.classForStatusBorderTwo = "table-bordered-yellow newTableFontSize"
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 2) {
      this.classForStatusBorderThree = "table-bordered-orange newTableFontSize"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 3) {
      this.classForStatusBorderFour = "table-bordered-blue newTableFontSize"

    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 4) {
      this.classForStatusBorderFive = "table-bordered-green newTableFontSize"
    }

  }
  successWindowClose() {
    this.moreInfoFlag = false;
  }

  createBornSaveData() {
    this.ecoBornData.push({
      "incomeGraphStringOne": this.incomeGraphStringOne,
      "incomeGraphStringTwo": this.incomeGraphStringTwo,
      "incomeGraphStringThree": this.incomeGraphStringThree,
      "incomeGraphStringFour": this.incomeGraphStringFour,
      "bOne": this.bOne,
      "bTwo": this.bTwo,
      "bThree": this.bThree,
      "bFour": this.bFour,
      "chanceOfPoorB": this.gameSummeryService.ecoStatusData.chanceOfPoor,
      "firstValueI": this.commonService.firstValueI,
      "secondValueI": this.commonService.secondValueI,
      "thirdValueI": this.commonService.thirdValueI,
      "fourthValueI": this.commonService.fourthValueI,
      "chanceOfPoorR": this.chanceOfPoorR

    })
    this.welathBornData.push({
      "wealthGraphSentenceBorn1": this.wealthGraphSentenceBorn1,
      "wealthGraphSentenceBorn2": this.wealthGraphSentenceBorn2,
      "wealthGraphSentenceBorn3": this.wealthGraphSentenceBorn3,
      "graph": this.multi2

    })
  }


  countryChallengesFormating() {
    if (this.countryChallengesArray.length === 0) {
      this.spliteString(this.mainPerson.country.moreInfo1)
      this.spliteString(this.mainPerson.country.moreInfo2)
      this.spliteString(this.mainPerson.country.moreInfo3)
      this.spliteString(this.mainPerson.country.moreInfo4)
      if(this.mainPerson.country.hasOwnProperty('moreInfo5')){
      this.spliteString(this.mainPerson.country.moreInfo5)
      }
    }
  }



  spliteString(str: String) {
    this.challengeHeading = '';
    this.challengeInfo = '';
    const dotPosition = str.indexOf(":");
    this.challengeHeading = str.substring(0, dotPosition);
    this.challengeInfo = str.substring(dotPosition + 1);
    this.countryChallengesArray.push({
      'title': this.challengeHeading,
      "text": this.challengeInfo
    })
  }

  setEducationForSummarySelf(person) {
    this.education = '';
    let education = person.education;
    if (education.school.inSchool) {
      this.education = this.translate.instant('gameLabel.edu11');
    }
    else if (!education.school.highSchoolGraduate && education.school.schoolDropOut && !education.school.inSchool) {
      this.translate.get('gameLabel.edu12', { dropOutAge: 6 + education.school.schoolCompletedYears }).subscribe(
        (str) => {
          this.education = str;
        })
    }
    return this.education
  }
}