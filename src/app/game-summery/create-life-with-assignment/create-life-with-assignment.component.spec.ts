import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLifeWithAssignmentComponent } from './create-life-with-assignment.component';

describe('CreateLifeWithAssignmentComponent', () => {
  let component: CreateLifeWithAssignmentComponent;
  let fixture: ComponentFixture<CreateLifeWithAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateLifeWithAssignmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLifeWithAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
