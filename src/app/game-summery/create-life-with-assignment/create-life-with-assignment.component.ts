import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { CreateLifeWithGroupIdComponent } from '../create-life-with-group-id/create-life-with-group-id.component';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-create-life-with-assignment',
  templateUrl: './create-life-with-assignment.component.html',
  styleUrls: ['./create-life-with-assignment.component.css']
})
export class CreateLifeWithAssignmentComponent implements OnInit ,AfterViewInit{

studentAssignMentId;
gameId;
token;
myCompOneObj ;
  constructor(public route: ActivatedRoute,
              public router :Router,
              public gameSummeryService :GameSummeryService,
              public modalWindowService : modalWindowShowHide,
              public homeService:HomeService,
              public http:HttpClient,
    private sanitizer: DomSanitizer,
    private commonService: CommonService,
    public translationService :TranslationService,) { 
                
              }

  ngOnInit(): void {
    this.gameSummeryService.flowFromStudent=true;
    this.token=(this.route.snapshot.queryParamMap.get('token')||0);
    localStorage.setItem('token',this.token);
    this.gameSummeryService.getValidToken().subscribe(
      res=>{
    this.token=res.data.result.token;
    localStorage.removeItem('token');
    localStorage.setItem('token',this.token);
   this.studentAssignMentId = this.route.snapshot.queryParamMap.get('id')
   this.gameSummeryService.flagForStudentAssignMent=true;
   this.gameSummeryService.studentAssId=this.studentAssignMentId;
   this.translationService.setDefaultLanguage();
    this.modalWindowService.showLanguageWindow=true;
    this.route.queryParamMap.subscribe(paramMap => {
      this.commonService.filter(paramMap, 'id');
      this.commonService.filter(paramMap, 'token');
      this.commonService.removeParamFromUrl(paramMap, ['id','token']);
    });
  })

}
ngAfterViewInit() {
} 


}
