import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-register-country',
  templateUrl: './register-country.component.html',
  styleUrls: ['./register-country.component.css']
})
export class RegisterCountryComponent implements OnInit {

  constructor(public gameSummeryService: GameSummeryService,
    public modalWindowService: modalWindowShowHide,
    public router: Router,
    public translationService: TranslationService,
    public translate: TranslateService) { }
  countryList;
  selectedCountryObject;
  countryId;
  searchText;
  checkBoxDisplayFlag: boolean = false;
  checkBoxCheckFlag: boolean = false;
  successMsgFlag: boolean = false;
  messageText;
  text;
  textCheckBox;
  ngOnInit(): void {
    this.modalWindowService.showWaitScreen=true;
    this.getCountryList();
  }


  getCountryList() {
    this.gameSummeryService.getRegisterCountry().subscribe(
      res => {
        this.gameSummeryService.registerCountryName = res[0].country;
        this.gameSummeryService.chagedRegisterCountryName = res[0].changed_country;
        if (this.gameSummeryService.registerCountryName === this.gameSummeryService.chagedRegisterCountryName) {
          this.checkBoxDisplayFlag = false;
        }
        else {
          this.checkBoxDisplayFlag = true;

        }
        if (this.gameSummeryService.chagedRegisterCountryName === null) {
          this.gameSummeryService.chagedRegisterCountryName = this.gameSummeryService.registerCountryName
        }
        this.gameSummeryService.getCountryList().subscribe(
          country => {
            this.countryList = country;
            for (let i = 0; i < this.countryList.length; i++) {
              if (this.gameSummeryService.registerCountryName === this.gameSummeryService.chagedRegisterCountryName) {
                if (this.gameSummeryService.registerCountryName === this.countryList[i].country) {
                  this.countryId = i;
                  this.modalWindowService.showWaitScreen=false;
                  break;
                }
              }
              else {
                if (this.gameSummeryService.chagedRegisterCountryName === this.countryList[i].country) {
                  this.countryId = i;
                  this.modalWindowService.showWaitScreen=false;
                  break;
                }
              }
            }
            this.translate.get('gameLabel.registerCountryWarr2', { country: this.gameSummeryService.chagedRegisterCountryName }).subscribe(
              (str) => {
                this.text = str;
              })
            this.translate.get('gameLabel.registerCountryWarr1', { country: this.gameSummeryService.registerCountryName }).subscribe(
              (str) => {
                this.textCheckBox = str;
              })
          }
        )
      })

  }

  allRadioButtonValue(event: any) {
    if (event.target.name == "country_option") {
      let countryRadioButtonValue = event.target.value;
      countryRadioButtonValue = parseInt(countryRadioButtonValue)
      for (let i = 0; i < this.countryList.length; i++) {
        if (countryRadioButtonValue === this.countryList[i].countryid) {
          this.selectedCountryObject = this.countryList[i];
          break;
        }
      }
    }
  }

  saveCountry() {
    this.gameSummeryService.setRegisterCountry(this.selectedCountryObject.country).subscribe(
      res => {
        this.gameSummeryService.chagedRegisterCountryName = this.selectedCountryObject.country;
        this.successMsgFlag = true;
        if (this.gameSummeryService.registerCountryName === this.gameSummeryService.chagedRegisterCountryName) {
          this.checkBoxDisplayFlag = false;
        }
        else {
          this.checkBoxDisplayFlag = true;
        }
        for (let i = 0; i < this.countryList.length; i++) {

          if (this.gameSummeryService.registerCountryName === this.gameSummeryService.chagedRegisterCountryName) {
            if (this.gameSummeryService.registerCountryName === this.countryList[i].country) {
              this.countryId = i;
              break;
            }
          }
          else {
            if (this.gameSummeryService.chagedRegisterCountryName === this.countryList[i].country) {
              this.countryId = i;
              break;
            }
          }
        }
        this.messageText = this.translate.instant('gameLabel.regWarr3')
        this.translate.get('gameLabel.registerCountryWarr2', { country: this.gameSummeryService.chagedRegisterCountryName }).subscribe(
          (str) => {
            this.text = str;
          })
      }
    )
  }

  checkBocChecked() {
    if (!this.checkBoxCheckFlag) {
      this.checkBoxDisplayFlag = false;
      this.gameSummeryService.setRegisterCountry(this.gameSummeryService.registerCountryName).subscribe(
        res => {
          for (let i = 0; i < this.countryList.length; i++) {
            this.gameSummeryService.chagedRegisterCountryName = this.gameSummeryService.registerCountryName;
            this.translate.get('gameLabel.registerCountryWarr2', { country: this.gameSummeryService.chagedRegisterCountryName }).subscribe(
              (str) => {
                this.text = str;
              })
            if (this.gameSummeryService.registerCountryName === this.countryList[i].country) {
              this.countryId = i;
              this.successMsgFlag = true;
              this.messageText = this.translate.instant('gameLabel.regWarr3')
              break;
            }
          }
        }
      )
    }
  }

  close() {
    this.modalWindowService.showRegisterCOuntrySelectFlag = false;
    this.router.navigate(['/summary']);
  }


  successMsgWindowClose() {
    this.successMsgFlag = false;
    this.modalWindowService.showRegisterCOuntrySelectFlag = false;
    this.router.navigate(['/summary']);
  }
}
