import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLifeWithGroupIdComponent } from './create-life-with-group-id.component';

describe('CreateLifeWithGroupIdComponent', () => {
  let component: CreateLifeWithGroupIdComponent;
  let fixture: ComponentFixture<CreateLifeWithGroupIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateLifeWithGroupIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLifeWithGroupIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
