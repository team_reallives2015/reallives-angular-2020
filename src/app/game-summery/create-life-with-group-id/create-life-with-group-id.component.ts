import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { GameSummeryService } from '../game-summery.service';
import * as _ from 'lodash';
import { HomeService } from 'src/app/home/home.service';
import { TranslateService } from '@ngx-translate/core';
import { ConstantService } from 'src/app/shared/constant.service';

@Component({
  selector: 'app-create-life-with-group-id',
  templateUrl: './create-life-with-group-id.component.html',
  styleUrls: ['./create-life-with-group-id.component.css']
})
export class CreateLifeWithGroupIdComponent implements OnInit, AfterViewInit {
  groupName;
  groupList;
  selectedGroupId;
  webLink: any;
  dummyLink: string;
  countryList;
  selectedCountry;
  state;
  lat: number;
  lng: number;
  markers = [];
  mapZoom = 0;
  localUrl;
  designALifeObject = {};
  selectedLanguage;
  gameId;
  one;
  disableButton: boolean = true;
  single = [];
  display = [];
  bubbleData = [];
  compaireCountryFlag: boolean = false;
  mySelect1 = '1';
  mySelect2 = '1';
  countryInfo;
  dropDownArray = [];
  group;
  selectedValueFromFirst = "ppp";
  selectedValueFromSecond = "hdi";
  showSuccessMsgFlag: boolean = false;
  moreInforStm;
  moreInfoTitle;
  showSuccessMsgFlagLoadLife: boolean = false;
  selectedCategoryName;
  constructor(public gameSummeryService: GameSummeryService,
    public modalWindowService: modalWindowShowHide,
    private sanitizer: DomSanitizer,
    private commonService: CommonService,
    public translationService: TranslationService,
    public router: Router,
    public homeService: HomeService,
    public translate: TranslateService,
    public constantService: ConstantService
  ) {
  }
  //bar char
  view: any[] = [650, 400];
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = false;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Country';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = 'Population';
  colorScheme = {
    domain: []
  };
  flagForResolution: boolean = false;
  displayLegentAll: boolean = false;
  dispalyOnlyOneLegent: boolean = false;
  countryGroupCatergory = [];
  selectedCategoryId;
  ngAfterViewInit() {
  }

  ngOnInit(): void {
    this.modalWindowService.showCharacterDesighWithGropuFlag = true;
    // this.state="group";
    this.state = "catergory";
    this.getScreenRes();
    this.selectedLanguage = this.translationService.selectedLang.code;
    this.gameSummeryService.getCountryGroupCatergory(this.selectedLanguage).subscribe(
      res => {
        this.countryGroupCatergory = res;
      }
    )
    // this.getGroupMetaData();
  }


  selectCat(cId, cName) {
    this.selectedCategoryId = cId;
    this.selectedCategoryName = cName
    this.getGroupMetaData();
    this.state = "group"

  }

  getScreenRes() {
    let scrHeight = window.innerHeight;
    let scrWidth = window.innerWidth;
    if (scrWidth < 1225) {
      this.view = [500, 400];
    }
  }
  getGroupMetaData() {
    if (!this.gameSummeryService.displayCountryGroupData) {
      this.gameSummeryService.getCountryGropuListByCategories(this.translationService.selectedLang.code, this.selectedCategoryId).subscribe(
        res => {
          this.group = res;
          this.groupList = JSON.parse(JSON.stringify(this.group));
          this.groupName = this.groupList[0].Group_display_name
          this.webLink = this.groupList[0].Wiki_Link;
          this.selectedGroupId = this.groupList[0].Group_ID;
        });
    }
    else {
      this.groupList = this.gameSummeryService.studentAssignmentCountryGroupData;
      this.groupName = this.groupList[0].Group_display_name
      this.webLink = this.groupList[0].Wiki_Link;
      this.disableButton = false;
      this.selectedGroupId = this.groupList[0].Group_ID;
    }

  }

  getSafeUrlValue(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getDropDownText(id, object) {
    const selObj = _.filter(object, function (o) {
      return (_.includes(id, o.id));
    });
    return selObj;
  }

  allRadioButtonValue(event: any) {
    if (event.target.name === "country") {
      this.disableButton = false;
      this.selectedCountry = this.countryList[event.target.value];
      this.markers = [];
      this.mapZoom = 0;
      this.markers.push({
        lat: this.selectedCountry.lat,
        lng: this.selectedCountry.lng,
        label: this.selectedCountry.country,
        code: this.selectedCountry.Code
      })
      this.countryMarkerClick(this.markers[0]);
    }
    else if (event.target.name === "group") {
      this.disableButton = false;
      this.selectedGroupId = this.groupList[event.target.value].Group_ID;
      this.webLink = this.groupList[event.target.value].Wiki_Link;
      this.groupName = this.groupList[event.target.value].Group_display_name
    }
  }


  countryMarkerClick(countryObject) {
    this.disableButton = false;
    this.mapZoom = 4;
    this.lat = countryObject.lat;
    this.lng = countryObject.lng;
  }

  generateLife() {
    if ((this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.moreInforStm = this.translate.instant('gameLabel.demo_warr_for_other');
      this.showSuccessMsgFlag = true;
    }
    else if ((this.gameSummeryService.totalPayedLifeCount >= this.gameSummeryService.maxLifeCount)) {
      this.moreInforStm = this.translate.instant('gameLabel.licences_exide_warr');
      this.showSuccessMsgFlag = true;
    }
    else {
      this.modalWindowService.showWaitScreen = true;
      let getCountryListLength = this.countryList.length;
      let randomNo = this.commonService.getRandomValue(0, getCountryListLength);
      this.selectedCountry = this.countryList[Math.round(randomNo)];
      this.designALifeObject["country"] = this.selectedCountry;
      this.designALifeObject["onlyCountry"] = false;
      this.designALifeObject['city'] = null;
      this.designALifeObject["religion"] = null;
      this.designALifeObject["traits"] = null;
      this.designALifeObject['first_name'] = null;
      this.designALifeObject['last_name'] = null;
      this.designALifeObject['nameGroupId'] = null;
      this.designALifeObject['sex'] = null
      this.designALifeObject['urban_rural'] = null;
      this.gameSummeryService.generateCharacterDesignLife(this.designALifeObject, false, false, 0, this.selectedLanguage, true, 0).subscribe(
        res => {
          this.gameId = res;
          localStorage.removeItem("gameid");
          localStorage.setItem("gameid", this.gameId);
          this.modalWindowService.showWaitScreen = false;
          this.router.navigate(['/createlife']);
          // this.modalWindowService.showCharacterDesighWithGropuFlag=false;
        })
    }
  }


  selectCountry() {
    this.single = [];
    this.disableButton = true;
    this.dispalyOnlyOneLegent = true;
    this.gameSummeryService.getCountryListFromGroupId(this.selectedGroupId).subscribe(
      res => {
        this.countryList = res;
        for (var i = 0; i < this.countryList.length; i++) {
          this.single.push({
            "name": this.countryList[i].country,
            "value": this.countryList[i].population
          })
          this.colorScheme.domain.push('#567caa')
        }
        if (this.single.length % 2 !== 0) {
          this.single.sort((a, b) => a.value - b.value);
          let index: any;
          index = (this.single.length / 2);
          index = parseInt(index);
          index = index - 1;
          index = index + 1;
          this.colorScheme.domain[index] = ('#D1B000')
        }
        else {
          this.single = this.single.sort((a, b) => a.value - b.value);
          let index: any;
          index = this.single.length / 2;
          index = parseInt(index);
          index = index - 1;
          let v1 = this.single[index].value
          index = index + 1;
          let v2 = this.single[index].value
          let v3 = (v1 + v2) / 2
          this.single.push({
            "name": this.translate.instant('gameLabel.Median'),
            "value": v3
          })
          this.colorScheme.domain.push('#567caa');
          this.single = this.single.sort((a, b) => a.value - b.value);
          for (let j = 0; j < this.single.length; j++) {
            if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
              this.colorScheme.domain[j] = ('#D1B000');
              break;
            }
          }
        }
        Object.assign(this.single);
        this.fillMarkerCountry();
        this.translate.get('gameLabel.Choose_a_country_in_group_where_you_would_like_to_be_born', { gName: (this.groupName.toUpperCase()) }).subscribe(
          (s: String) => {
            this.countryInfo = s;
          })
        this.state = "country";
      });
  }

  clickGini() {
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].gini
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.GINI');

  }

  clickHdi() {
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].HDI
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.HDI');

  }

  clickHappiness() {
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": parseFloat(this.countryList[i].HappinessScore)
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.Happiness');

  }

  clickPopulation() {
    this.colorScheme = {
      domain: []
    };
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = true;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].population
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000')
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      let v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
      for (let j = 0; j < this.single.length; j++) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
          break;
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.Population');

  }
  clickppp() {
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      this.single.push({
        "name": this.countryList[i].country,
        "value": this.countryList[i].ppp
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.PPP');
  }

  clickSdg() {
    this.colorScheme = {
      domain: []
    };
    let v3;
    this.compaireCountryFlag = false;
    this.dispalyOnlyOneLegent = false;
    this.single = [];
    for (var i = 0; i < this.countryList.length; i++) {
      if (this.countryList[i].SdgiScore === "NA") {
        this.countryList[i].SdgiScore = 0;
      }
      this.single.push({
        "name": this.countryList[i].country,
        "value": parseFloat(this.countryList[i].SdgiScore)
      })
      this.colorScheme.domain.push('#567caa')
    }
    if (this.single.length % 2 !== 0) {
      this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = (this.single.length / 2);
      index = parseInt(index);
      index = index - 1;
      index = index + 1;
      this.colorScheme.domain[index] = ('#D1B000');
      v3 = this.single[index].value
    }
    else {
      this.single = this.single.sort((a, b) => a.value - b.value);
      let index: any;
      index = this.single.length / 2;
      index = parseInt(index);
      index = index - 1;
      let v1 = this.single[index].value
      index = index + 1;
      let v2 = this.single[index].value
      v3 = (v1 + v2) / 2
      this.single.push({
        "name": this.translate.instant('gameLabel.Median'),
        "value": v3
      })
      this.colorScheme.domain.push('#567caa');
      this.single = this.single.sort((a, b) => a.value - b.value);
    }


    for (let j = 0; j < this.single.length; j++) {
      if (this.single[j].value > v3) {
        this.colorScheme.domain[j] = ('#18A049');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value < v3) {
        this.colorScheme.domain[j] = ('#FF0000');
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
      else if (this.single[j].value === v3) {
        if (this.single[j].name === this.translate.instant('gameLabel.Median')) {
          this.colorScheme.domain[j] = ('#D1B000');
        }
      }
    }
    Object.assign(this.single);
    this.xAxisLabel = this.translate.instant('gameLabel.SDG_Score');

  }


  fillMarkerCountry() {
    for (let i = 0; i < this.countryList.length; i++) {
      this.markers.push(
        {
          lat: this.countryList[i].lat,
          lng: this.countryList[i].lng,
          label: this.countryList[i].country,
          code: this.countryList[i].Code

        }
      );
    }
  }
  stateChange(state) {
    this.state = state;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.groupIdSelectedCountryObject = this.selectedCountry;
    this.gameSummeryService.sdgId = 0;
    this.modalWindowService.showGroupCountry = true;
    this.modalWindowService.showcharacterDesignFlag = true;
  }

  compareCountryClick() {
    this.compaireCountryFlag = true;
  }

  close() {
    this.modalWindowService.showCharacterDesighWithGropuFlag = false;
  }

  onSelect(data): void {
    // console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    // console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    // console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
  shuffleCountry() {
    this.commonService.randomArrayShuffle(this.groupList);
    this.groupName = this.groupList[0].Group_display_name
    this.webLink = this.groupList[0].Wiki_Link;
    this.selectedGroupId = this.groupList[0].Group_ID;
  }

  aToz() {
    this.groupList = JSON.parse(JSON.stringify(this.group));
  }

  successWindowOk() {
    this.showSuccessMsgFlag = false;
  }
  loadIncompleteLife() {
    this.router.navigate(['/loadincompletegame']);

  }

  successWindowOkLoadLife() {
    this.showSuccessMsgFlagLoadLife = false;

  }

  clickBack(state) {
    if (state === 'group') {
      this.state = "catergory"
    }
    else if (state === 'country') {
      this.state = "group"
      this.groupName = this.groupList[0].Group_display_name
      this.webLink = this.groupList[0].Wiki_Link;
      this.selectedGroupId = this.groupList[0].Group_ID;
    }
  }
}
