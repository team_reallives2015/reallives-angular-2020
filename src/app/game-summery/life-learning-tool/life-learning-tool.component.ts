import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-life-learning-tool',
  templateUrl: './life-learning-tool.component.html',
  styleUrls: ['./life-learning-tool.component.css']
})
export class LifeLearningToolComponent implements OnInit {
   //pagination
   config: any;
   pageRangeLimit:number=30;
   currentPage=1
   totalLength;
     //
  showConfirmWindowFlag: boolean = false;
  loadGameData;
  gameId;
  parameterValue;
  elem: any; isFullScreen: boolean;
  searchText: any;
  constructor(private router: Router,
    public phpToolService: PhpToolService,
    public gameSummeryService: GameSummeryService,
    public commonService: CommonService,
    public modalWindowService: modalWindowShowHide,
    @Inject(DOCUMENT) private document: any) { }

  ngOnInit() {
    this.config = {
      currentPage: this.currentPage,
      itemsPerPage: this.pageRangeLimit,
      totalItems: this.totalLength
    };
    this.modalWindowService.showLoadGameFlag = true;
    this.modalWindowService.showWaitScreen = true;
    this.gameSummeryService.getCompleteListForPagination(true,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) // return a Observable with a error message to display
    ).subscribe(res => {
      this.totalLength=res['length']
       this.loadGameData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }

  goBack() {
    this.router.navigate(['/summary'])
  }

  goToHome(gameId) {
    localStorage.removeItem('gameid');
    localStorage.setItem('gameid', gameId);
    this.router.navigate(
      ['/loadgameanimation'],
      { queryParams: { flag: 'true' } }
    );
  }
  loadGameSetting() {
    this.modalWindowService.showGameSeetingsFlag = true;
  }

  @HostListener('document:fullscreenchange', ['$event'])
  @HostListener('document:webkitfullscreenchange', ['$event'])
  @HostListener('document:mozfullscreenchange', ['$event'])
  @HostListener('document:MSFullscreenChange', ['$event'])
  fullscreenmodes(event) {
    this.chkScreenMode();
  }
  chkScreenMode() {
    if (document.fullscreenElement) {
      //fullscreen
      this.isFullScreen = true;
    } else {
      //not in full screen
      this.isFullScreen = false;
    }
  }
  openFullscreen() {
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
  }
  /* Close fullscreen */
  closeFullscreen() {
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }

  clickDocumentDownlode(gameId) {
    this.modalWindowService.showDocumentFlag = true;
    this.gameSummeryService.gameId = gameId;
    this.router.navigate(['/downlodeDoc'])
  }

  clickLearningToolClick(gameId) {
    this.phpToolService.flowFromCountry = true;
    this.modalWindowService.showCountryLearningTool = true;
    this.gameSummeryService.gameId = gameId;
    this.router.navigate(['/learningTool'])
  }


  clickDeleteLife(gameId) {
    this.gameId = gameId
    this.showConfirmWindowFlag = true
  }


  clickOk() {
    this.gameSummeryService.updateSoftDelateGameFlag(this.gameId, 0).subscribe(
      res => {
        this.loadGameData = res;
        this.showConfirmWindowFlag = false
      }
    )
  }


  clickCancel() {
    this.showConfirmWindowFlag = false;
  }
  pageChange(newPage: number) {
    this.modalWindowService.showWaitScreen = true;
    this.loadGameData=[];
    this.currentPage=newPage
    this.gameSummeryService.getCompleteListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.loadGameData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }

  onChange(e){
    this.modalWindowService.showWaitScreen = true;
    this.loadGameData=[];
    this.pageRangeLimit= parseInt(e.target.value)
    this.gameSummeryService.getCompleteListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.loadGameData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }
}

