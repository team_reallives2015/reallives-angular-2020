import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeLearningToolComponent } from './life-learning-tool.component';

describe('LifeLearningToolComponent', () => {
  let component: LifeLearningToolComponent;
  let fixture: ComponentFixture<LifeLearningToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeLearningToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeLearningToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
