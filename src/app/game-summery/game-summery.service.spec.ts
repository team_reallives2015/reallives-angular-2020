import { TestBed } from '@angular/core/testing';

import { GameSummeryService } from './game-summery.service';

describe('GameSummeryService', () => {
  let service: GameSummeryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameSummeryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
