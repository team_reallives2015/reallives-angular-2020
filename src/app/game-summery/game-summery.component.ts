import { ViewChild } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  TranslateService } from '@ngx-translate/core';
import { modalWindowShowHide } from '../modal-window-show-hide.service';
import { CommonService } from '../shared/common.service';
import { ConstantService } from '../shared/constant.service';
import { TranslationComponent } from '../translation/translation.component';
import { TranslationService } from '../translation/translation.service';
import { GameSummeryService } from './game-summery.service';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Component({
  selector: 'app-game-summery',
  templateUrl: './game-summery.component.html',
  styleUrls: ['./game-summery.component.css'],
})
export class GameSummeryComponent implements OnInit, AfterViewInit {
  demoOverFlag:boolean=false;
  licencesExpirePopupShowFlag:boolean=false;
  token: any;
  showCharacterDesignFlag;
  tabWasClosed: boolean = false;
  string;
  exitButtonFlag: boolean = false;
  completeLifeCount;
  incompletelifeCount;
  classForRandomButton;
  showSuccessMsgFlag: boolean = false;
  msgText;
  moreInfoTitle;
  moreInforStm;
  fistTimeClickFlag: boolean = false;
  showSuccessMsgFlagLoadLife: boolean = false;
  flagForScreenSeetingsPopup: boolean = false;
  lifeCountMsg;
  
  constructor(private router: Router,
    public route: ActivatedRoute,
    public commonService: CommonService,
    public modalWindowService: modalWindowShowHide,
    public translationService: TranslationService,
    public translate: TranslateService,
    public gameSummeryService: GameSummeryService,
    public constantService: ConstantService) {
  }
  ngAfterViewInit(): void {
  

}
  @ViewChild(TranslationComponent) translationComponent: TranslationComponent;

  ngOnInit(): void {
    this.commonService.fullScreen();
    this.gameSummeryService.createLifenextEventFlag=false
    this.showCharacterDesignFlag = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showGameSeetingsFlag = false;
    this.gameSummeryService.countryGroupTool = false;
    this.gameSummeryService.countryDisaparityTool = false
    this.gameSummeryService.countrySdgTool = false
    this.gameSummeryService.countryLearningTool = false
    this.gameSummeryService.showLifeBookListFlag = false;
    this.gameSummeryService.showLifeDataListFlag = false;
    this.gameSummeryService.showLifeDiaryListFlag = false;
    this.gameSummeryService.getPlayerRole().subscribe(
      res => {
        this.gameSummeryService.UserRole = res.drole;
        this.gameSummeryService.licenceId = res.prodId;
        this.gameSummeryService.maxLifeCount = res.maxLives;
        this.gameSummeryService.totalPayedLifeCount = res.livesCount
        this.gameSummeryService.licencesExpiryFlag=res.expiryFlag
        this.route.queryParamMap.subscribe(paramMap => {
          this.commonService.filter(paramMap, 'token');
          this.commonService.removeParamFromUrl(paramMap, ['token']);
          if(this.gameSummeryService.licenceId===null ||  this.gameSummeryService.maxLifeCount===1){
            this.gameSummeryService.generateLifeWithDemo = true;
          }
          else{
            this.gameSummeryService.generateLifeWithDemo = false;
          }
          if (this.gameSummeryService.licenceId !== null && (this.gameSummeryService.totalPayedLifeCount >= 1 && this.gameSummeryService.totalPayedLifeCount < 2)) {
            this.flagForScreenSeetingsPopup = true;
          }
          if (this.gameSummeryService.licenceId !== null) {
            this.translationService.gameBuyFlag = true;
            this.gameSummeryService.getLivesCount().subscribe(
              res => {
                let totalLives = res.totalLives;
                let totalCompletedLives = res.completeLives
                if (totalLives > 0) {
                  this.translate.get('gameLabel.life_count_msg', { totalLives: totalLives, totalCompletedLives: totalCompletedLives }).subscribe(
                    (str) => {
                      this.lifeCountMsg = str;
                    })
                } else {
                  this.lifeCountMsg = this.translate.instant('gameLabel.life_count_msg_1');
                }
              }
            )
          }
        });
      });
  }






  playAudio() {
    this.commonService.audio.src = "../sound/moon-woman-spirit2.mp3";
    this.commonService.audio.load();
    this.commonService.audio.play();
  }

  goBack() {
    this.router.navigate(['/summary']);
  }
  goTocreateLife() {
    if ((this.gameSummeryService.licenceId !== null && this.gameSummeryService.maxLifeCount > 1) && (this.gameSummeryService.totalPayedLifeCount <= this.gameSummeryService.maxLifeCount)) {
      this.modalWindowService.showcharacterDesignFlag = false;
      this.modalWindowService.showGameSeetingsFlag = false;
      this.modalWindowService.showchearacterDesignSdgFlag = false;
      this.modalWindowService.showLoadGameFlag = false;
      this.modalWindowService.showAssignment = false;
      this.modalWindowService.showCharacterDesighWithGropuFlag = false;
      this.modalWindowService.ShowOldLifeFlag = false;;
      this.router.navigate(['/createlife']);
    }
    else if ((this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.moreInforStm = this.translate.instant('gameLabel.demo_warr_random_life');
      this.showSuccessMsgFlag = true;
    }
    else if ((this.gameSummeryService.totalPayedLifeCount >= this.gameSummeryService.maxLifeCount)) {
      this.moreInforStm = this.translate.instant('gameLabel.licences_exide_warr');
      this.showSuccessMsgFlagLoadLife = true;
    }

  }

  goToCreateOldLife() {
    if ((this.gameSummeryService.licenceId !== null && this.gameSummeryService.maxLifeCount > 1) && (this.gameSummeryService.totalPayedLifeCount <= this.gameSummeryService.maxLifeCount)) {
      this.modalWindowService.showcharacterDesignFlag = false;
      this.modalWindowService.showGameSeetingsFlag = false;
      this.modalWindowService.showchearacterDesignSdgFlag = false;
      this.modalWindowService.showLoadGameFlag = false;
      this.modalWindowService.showAssignment = false;
      this.modalWindowService.showCharacterDesighWithGropuFlag = false;
      this.modalWindowService.ShowOldLifeFlag = true;
      this.router.navigate(['/createlife']);
    }
    else if ((this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.moreInforStm = this.translate.instant('gameLabel.demo_warr_random_life');
      this.showSuccessMsgFlag = true;
    }
    else if ((this.gameSummeryService.totalPayedLifeCount >= this.gameSummeryService.maxLifeCount)) {
      this.moreInforStm = this.translate.instant('gameLabel.licences_exide_warr');
      this.showSuccessMsgFlagLoadLife = true;

    }

  }
  successWindowOkLoadLife() {
    this.showSuccessMsgFlagLoadLife = false;

  }

  goToDesignLife() {
    this.modalWindowService.showcharacterDesignFlag = true;
    this.modalWindowService.showLoadGameFlag = false;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showAssignment = false;
    this.modalWindowService.showCharacterDesighWithGropuFlag = false;

  }

  createLifeWithSdg() {
    this.gameSummeryService.flowFromStudent = false;
    this.modalWindowService.showSdgWithAssignMent = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.showLoadGameFlag = false;
    this.modalWindowService.showAssignment = false;
    this.modalWindowService.showCharacterDesighWithGropuFlag = false;
    this.modalWindowService.showchearacterDesignSdgFlag = true;
  }

  createLifeWithGroup() {
    this.gameSummeryService.flowFromStudent = false
    this.gameSummeryService.displayCountryGroupData = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.showLoadGameFlag = false;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showAssignment = false;
    this.modalWindowService.showCharacterDesighWithGropuFlag = true;
  }

  createLifeWithDemo() {
    this.gameSummeryService.flowFromStudent = false
    this.gameSummeryService.displayCountryGroupData = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showSdg = false;
    this.modalWindowService.showLoadGameFlag = false;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showAssignment = false;
    this.modalWindowService.showCharacterDesighWithGropuFlag = false;
    this.gameSummeryService.generateLifeWithDemo = true;
    this.gameSummeryService.getLoadGameData().pipe(
      catchError(err => of('load game data error')) // return a Observable with a error message to display
    ).subscribe(res => {
      let incomplete=res;
      this.incompletelifeCount = res.length;
      this.gameSummeryService.getCompletedLifeData().pipe(
        catchError(err => of('load game data error')) // return a Observable with a error message to display
      ).subscribe(res1 => {
        this.completeLifeCount = res1.length;
        if (this.gameSummeryService.totalPayedLifeCount===0) {
          this.fistTimeClickFlag = true;
          let s1 = this.translate.instant('gameLabel.demo1') + "<br><br>";
          let s2 = this.translate.instant('gameLabel.demo2') + "<br><br>";
          let s3 = this.translate.instant('gameLabel.demo3') + "<br>";
        
          this.moreInfoTitle = s1;
          this.moreInforStm = s2 + s3;
        }
        else {
          this.demoOverFlag=true
        }
      })
    })

  }

  okClick() {
    this.fistTimeClickFlag = false;
    this.router.navigate(['/createlife']);
    this.commonService.fullScreen();
  }

  clickRegisterCountrySelect() {
    this.modalWindowService.showRegisterCOuntrySelectFlag = true;
    this.router.navigate(['/registercountry']);
  }

  showAssignment() {
    this.router.navigate(['/showAssignments']);
  }

  loadIncompleteLife() {
    this.router.navigate(['/loadincompletegame']);

  }
  loadCompletedLife() {
    this.router.navigate(['/loadCompletedLife']);
  }

  loadDeletedLife() {
    this.router.navigate(['/loadDeletedLife']);
  }

  clickGameSeeting() {
    this.modalWindowService.showGameSeetingsFlag = true;

  }
  clickBornSeeting() {
    this.modalWindowService.bornScreenSeetingsFlag = true;
    this.flagForScreenSeetingsPopup = false;


  }


  clickLanguageSetting() {
    this.modalWindowService.showLanguageWindow = true;
    this.modalWindowService.showLangFromIcon = true;
   this.gameSummeryService.newFetureOnceFlag=true;
    this.router.navigate(['/language']);
  }

  clickToExitButton() {
    this.exitButtonFlag = true;
  }

  goToDashbordYes() {
    this.exitButtonFlag = false;
    this.commonService.close();
  }


  goToDashbordNo() {
    this.exitButtonFlag = false;
  }


  successWindowOk() {
    this.showSuccessMsgFlag = false;
  }

  okClickBornScreenPopup() {
    this.flagForScreenSeetingsPopup = false;
  }


  clickCountryLearningTool() {
    this.gameSummeryService.countryLearningTool = true;
    this.router.navigate(['/phpToolSummary'])
  }

  clickCountryDisparityTool() {
    this.gameSummeryService.countryDisaparityTool = true;
    this.router.navigate(['/countryDisparityLearningToolSummary'])

  }

  clickCountrySdgTool() {
    this.gameSummeryService.countrySdgTool = true;
    this.router.navigate(['/sdgSummary'])

  }
  clickCountryGroupTool() {
    this.gameSummeryService.countryGroupTool = true;
    this.router.navigate(['/countryGroupLearningToolSummary'])

  }


  loadlifeBookData() {
    this.gameSummeryService.showLifeBookListFlag = true;
    this.router.navigate(['/life-book']);
  }

  loadlifeLerningToolData() {
    this.gameSummeryService.showLifeDataListFlag = true
    this.router.navigate(['/life-tool']);
  }

  loadLifeDiaryData() {
    this.gameSummeryService.showLifeDiaryListFlag = true;
    this.router.navigate(['/life-diary-list'])
  }

  loadPassport() {
    this.router.navigate(['/passport'])

  }

  dataToolClick() {
    this.router.navigate(['/action-page'])
  }

  licencesExpireOk(){
   this.commonService.licenExpireClick();
    }
    newFetureWindowClose(){
      this.gameSummeryService.newFetureOnceFlag=true;
      this.gameSummeryService.newFeatureFlag=false;
    }
    demoCancel(){
      this.demoOverFlag=false;
    }
  
    cancel(){
      this.demoOverFlag=false;
    }

}
