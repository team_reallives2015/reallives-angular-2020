import { AfterViewInit, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { GameSummeryService } from '../game-summery.service';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { TranslateService } from '@ngx-translate/core';
import { Color } from 'ng2-charts';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-create-life-with-sdg',
  templateUrl: './create-life-with-sdg.component.html',
  styleUrls: ['./create-life-with-sdg.component.css']
})
export class CreateLifeWithSdgComponent implements OnInit, AfterViewInit {
  mapTitle;
  sdgMapTitle;
  viewGraph: boolean = false;
  chartReadySecond: boolean = false
  graphTitle;
  sdgIdMap;
  countryListSdg = [];
  colourCode;
  countries = []
  selectedCountryObject;
  sdgInfo: any;
  sdgTargets: any;
  sdgDetails
  sdgImage
  sdgName
  sdgTitle
  sdgDescription
  sdgTargetCode
  modelWindowShowHideService
  sdgGreenCountryList = [];
  sdgYellowCountryList = [];
  sdgOrangeCountryList = [];
  sdgGrayCountryList = [];
  sdgRedCountryList = [];
  sdgGreenCountryScoreList = [];
  sdgYellowCountryScoreList = [];
  sdgOrangeCountryScoreList = [];
  sdgGrayCountryScoreList = [];
  sdgRedCountryScoreList = [];
  sdgCountryListAll = [];
  grey;
  countryList;
  CountryDesignALife;
  state;
  disabledButton: boolean = false;
  countryModelShowHide = false;
  sdgId;
  sdgSubGoalValue;
  sdgTargetDesc;
  values: any;
  loadGoalFromAssignment: boolean = false;
  modelClass = "new_cmn_modal sdg_modal md-overlay";
  sdgCountryMapList: any[] = [];
  green;
  red;
  organge;
  gray;
  yellow
  score: any;
  redMin; redMax; redMediun; redTotal
  greenMin; greenMax; greenMediun; greenTotal
  orangeMin; orangeMax; orangeMediun; orangeTotal
  yellowMin; yellowMax; yellowMediun; yellowTotal
  grayMin; grayMax; grayMediun; grayTotal
  disabledButtonMap: boolean = false;
  showAlertFlag: boolean = false;
  moreInfoFlag: boolean = false;
  moreInforStm;
  msgText;
  changeGraphFlag: boolean = false;
  flagForSort: boolean = false;
  allMaxValue = [];
  allMinValue = [];
  allMediunValue = [];
  redMinCountry;
  redMaxCountry;
  greenMinCountry;
  greenMaxCountry;
  yellowMinCountry;
  yellowMaxCountry;
  orangeMaxCountry;
  orangeMinCountry;
  //window
  firstWindowFlag: boolean = false;
  secondWindowFlag: boolean = false;
  thirdWindowFlag: boolean = false;
  fourWindowFlag: boolean = false;
  fifthWindowFlag: boolean = false;
  sixWindowFlag: boolean = false;
  finalCountry: any[] = [];
  selectedCountryName = "No country selected"

  //graph
  chartReady: boolean = false;
  public chartColors = [];
  firstCopy = false;
  // data
  public lineChartData: Array<any>;
  // labels
  public lineChartLabels: Array<any>;
  public lineChartOptions: any
  public lineChartType = 'line';
  public labelMFL: Array<any>
  //multiple line chart
  public lineChartColors: Color[];
  public lineChartLegend = true;
  public lineChartPlugins = [];
  constructor(public modalWindowService: modalWindowShowHide,
    public gameSummeryService: GameSummeryService,
    public commonService: CommonService,
    public homService: HomeService,
    public translationService: TranslationService,
    public translation: TranslateService,
  ) { }

  ngOnInit(): void {
    this.modalWindowService.showchearacterDesignSdgFlag = true;
    if (this.modalWindowService.showSdgWithAssignMent) {
      this.secondWindowFlag = true;
      this.commonService.fullScreen();
      this.sdgMapSelect(this.homService.sdgId);
    }
  }


  ngAfterViewInit() {

  }
  sdgMapSelect(sdgId) {
    this.firstWindowFlag = true;
    this.sdgId = sdgId;
    this.translation.get('gameLabel.sdg_map_title', { sdgName: "SDG's goal " + sdgId }).subscribe(
      (str) => {
        this.mapTitle = str;
      })
    this.sdgIdMap = this.commonService.getSdgNameFromId(sdgId);
    this.gameSummeryService.getSdgCountryColour(this.sdgIdMap).subscribe(
      res => {
        this.countryListSdg = res;
        this.getSdgMap()
      })
  }

  sdgSelect(sdgId: number) {
    this.secondWindowFlag = true;
    am4core.disposeAllCharts();
    this.sdgInfo = [];
    this.sdgTargets = [];
    var index = sdgId;
    this.sdgId = index;
    let keys, values;
    this.gameSummeryService.getSDGDetailsForDesignALife(this.translationService.selectedLang.code).subscribe(res => {
      this.sdgDetails = res;
      this.values = Object.values(this.sdgDetails)
      for (let i = 0; i < this.values.length; i++) {
        if (this.values[i].SDG_Id === this.sdgId) {
          this.sdgInfo.push(this.values[i]);
          keys = Object.keys(this.values[i].targets);
          values = Object.values(this.values[i].targets)
        }
      }
      for (let j = 0; j < keys.length; j++) {
        let cnt = j + 1
        this.sdgTargets.push({
          key: keys[j],
          value: values[j],
          id: sdgId + "." + cnt
        });
      }
      this.sdgImage = "assets/images/sdgicon/sdg" + sdgId + "_" + this.translationService.selectedLang.code + ".png"
      this.sdgDescription = this.sdgInfo[0].SDG_discription
      this.sdgTitle = this.sdgInfo[0].SDG_title
      this.sdgName = this.sdgInfo[0].SDG_name
    })


  }
  sdgCountryList() {
    this.fifthWindowFlag = false;
    this.fourWindowFlag = true;
    this.firstWindowFlag = false;
    this.thirdWindowFlag = false
    this.secondWindowFlag = false;
    this.countryList = []
    this.sdgGreenCountryList = [];
    this.sdgRedCountryList = [];
    this.sdgYellowCountryList = [];
    this.sdgGrayCountryList = [];
    this.sdgOrangeCountryList = [];
    this.gameSummeryService.getSDGDetailsByCountryForDesignALife(this.sdgTargetCode).subscribe(res => {
      let sdgCountryList = res;
      this.sdgGreenCountryList = sdgCountryList['green'];
      this.sdgRedCountryList = sdgCountryList['red'];
      this.sdgOrangeCountryList = sdgCountryList['orange'];
      this.sdgYellowCountryList = sdgCountryList['yellow'];
      this.sdgGrayCountryList = sdgCountryList['grey'];
      this.disabledButton = false;
    })
  }

  sdgGraphAndTableData() {
    this.firstWindowFlag = false;
    this.secondWindowFlag = false;
    this.thirdWindowFlag = true;
    this.translation.get('gameLabel.sdg_graph_title', { subgoalName: this.sdgSubGoalValue + " " + this.sdgTargetDesc }).subscribe(
      (str) => {
        this.graphTitle = str;
      })
    this.sdgRedCountryList = [];
    this.sdgGreenCountryList = [];
    this.sdgYellowCountryList = [];
    this.sdgOrangeCountryList = [];
    this.sdgGreenCountryScoreList = [];
    this.sdgOrangeCountryScoreList = [];
    this.sdgYellowCountryScoreList = [];
    this.sdgRedCountryScoreList = [];
    this.green = 0, this.greenMax = 0; this.greenMin = 0; this.greenMediun = 0;
    this.red = 0, this.redMax = 0; this.redMin = 0; this.redMediun = 0
    this.organge = 0, this.orangeMax = 0; this.orangeMin = 0; this.orangeMediun = 0
    this.yellow = 0, this.yellowMax = 0; this.yellowMin = 0; this.yellowMediun = 0
    this.gameSummeryService.getSdgSubGoalCountryColour(this.sdgTargetCode).subscribe(res => {
      this.sdgCountryMapList = res;
      for (let i = 0; i < this.sdgCountryMapList.length; i++) {
        if (this.sdgCountryMapList[i].color === "red") {
          this.sdgRedCountryList.push(this.sdgCountryMapList[i])
        }

        else if (this.sdgCountryMapList[i].color === "orange") {
          this.sdgOrangeCountryList.push(this.sdgCountryMapList[i])

        }
        else if (this.sdgCountryMapList[i].color === "yellow") {
          this.sdgYellowCountryList.push(this.sdgCountryMapList[i])
        }
        else if (this.sdgCountryMapList[i].color === "green") {
          this.sdgGreenCountryList.push(this.sdgCountryMapList[i])
        }
      }
      for (let j = 0; j < this.sdgGreenCountryList.length; j++) {
        this.sdgGreenCountryScoreList.push(parseFloat(this.sdgGreenCountryList[j].score))
      }
      for (let j = 0; j < this.sdgOrangeCountryList.length; j++) {
        this.sdgOrangeCountryScoreList.push(parseFloat(this.sdgOrangeCountryList[j].score))
      }
      for (let j = 0; j < this.sdgYellowCountryList.length; j++) {
        this.sdgYellowCountryScoreList.push(parseFloat(this.sdgYellowCountryList[j].score))
      }
      for (let j = 0; j < this.sdgRedCountryList.length; j++) {
        this.sdgRedCountryScoreList.push(parseFloat(this.sdgRedCountryList[j].score))
      }
      //for green

      this.green = Object.values(this.sdgGreenCountryScoreList)
      if (this.green.length > 0) {
        this.greenMin = (Math.min(...this.green));
        this.greenMax = (Math.max(...this.green));
        this.greenTotal = this.sdgGreenCountryList.length;
      }
      else {
        this.greenMin = 0;
        this.greenMax = 0
        this.greenMediun = 0;
        this.greenTotal = 0;
      }
      //for orange
      this.organge = Object.values(this.sdgOrangeCountryScoreList)
      if (this.organge.length > 0) {
        this.orangeMin = (Math.min(...this.organge));
        this.orangeMax = (Math.max(...this.organge))
        this.orangeTotal = this.sdgOrangeCountryList.length;
      }
      else {
        this.orangeMin = 0;
        this.orangeMax = 0
        this.orangeTotal = 0;
      }
      //for yellow
      this.yellow = Object.values(this.sdgYellowCountryScoreList)
      if (this.yellow.length > 0) {
        this.yellowMin = (Math.min(...this.yellow));
        this.yellowMax = (Math.max(...this.yellow))
        this.yellowTotal = this.sdgYellowCountryList.length;
      }
      else {
        this.yellowMin = 0;
        this.yellowMax = 0
        this.yellowTotal = 0
      }



      //for red
      this.red = Object.values(this.sdgRedCountryScoreList)
      if (this.red.length > 0) {
        this.redMin = (Math.min(...this.red))
        this.redMax = (Math.max(...this.red))
        this.redTotal = this.sdgRedCountryList.length;
      }
      else {
        this.redMin = 0
        this.redMax = 0
        this.redTotal = 0
      }

      //
      this.allMaxValue = [];
      this.allMinValue = [];
      this.allMediunValue = [];
      this.flagForSort = false;

      this.allMaxValue.push(parseFloat(this.redMax));
      this.allMaxValue.push(parseFloat(this.orangeMax));
      this.allMaxValue.push(parseFloat(this.yellowMax));
      this.allMaxValue.push(parseFloat(this.greenMax));

      let max = Math.max(...this.allMaxValue)
      if (parseFloat(this.redMax) === max) {
        this.flagForSort = true
      }
      else {
        this.flagForSort = false;
      }

      for (let i = 0; i < this.sdgCountryMapList.length; i++) {
        if (this.sdgCountryMapList[i].score) {
          if (parseFloat(this.sdgCountryMapList[i].score) === (this.redMax)) {
            this.redMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.redMin)) {
            this.redMinCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.greenMax)) {
            this.greenMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.greenMin)) {
            this.greenMinCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.orangeMax)) {
            this.orangeMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.orangeMin)) {
            this.orangeMinCountry = this.sdgCountryMapList[i].country
          } else if (parseFloat(this.sdgCountryMapList[i].score) === (this.yellowMax)) {
            this.yellowMaxCountry = this.sdgCountryMapList[i].country
          }
          else if (parseFloat(this.sdgCountryMapList[i].score) === (this.yellowMin)) {
            this.yellowMinCountry = this.sdgCountryMapList[i].country
          }
        }
      }
      this.dotPlotGraph();
    })
  }

  getSdgSubGoalMapData() {
    this.fourWindowFlag = false;
    this.fifthWindowFlag = true
    this.firstWindowFlag = false;
    this.thirdWindowFlag = false
    this.secondWindowFlag = false;
    this.getSdgCountryListMap();
    this.disabledButtonMap = false;
  }

  stateChange() {
    this.fourWindowFlag = false;
    this.firstWindowFlag = false;
    this.thirdWindowFlag = false
    this.secondWindowFlag = false;
    this.fifthWindowFlag = false
    am4core.disposeAllCharts();
    this.state = 'city';
    this.modalWindowService.showSdgWithAssignMent = false;
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showSdg = true;
    this.modalWindowService.sdgCountrySelectedObject = this.selectedCountryObject;
    this.gameSummeryService.sdgId = this.sdgId;
    this.modalWindowService.showcharacterDesignFlag = true;
  }

  radioButtonValue(event, targetId, i) {
    if (event.target.name = 'sdg_selection') {
      if (targetId !== 0) {
        this.sdgTargetCode = targetId;
        this.sdgSubGoalValue = this.sdgTargets[i].id;
        this.sdgTargetDesc = this.sdgTargets[i].value;
        //

        this.gameSummeryService.sdgSubGoalId = this.sdgSubGoalValue;
        this.gameSummeryService.sdgSubGoalString = this.sdgTargetDesc;
        //
      }

      this.disabledButton = true;
    }
  }
  selectCountry(country, id) {
    this.gameSummeryService.sdgChallengeId = id;
    this.selectedCountryObject = country;
    this.CountryDesignALife = country
    this.disabledButton = true;
  }

  countryModalClose() {
    this.countryModelShowHide = false;
  }

  close() {
    this.modalWindowService.showchearacterDesignSdgFlag = false;
    this.modalWindowService.showcharacterDesignFlag = false;
    this.modalWindowService.showSdg = false;

  }

  getSdgMap() {
    this.translation.get('gameLabel.View_countries_reflecting_SDG_Goal_Score_for_the_Goal', { goalName: this.sdgId }).subscribe(
      (str) => {
        this.sdgMapTitle = str;
      })
    // 
    let map = am4core.create("sdgchartdiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    // polygonTemplate.events.on("hit", function (ev) {
    //   // zoom to an object
    //   ev.target.series.chart.zoomToMapObject(ev.target);
    // });
    let county1 = [];
    for (let i = 0; i < this.countryListSdg.length; i++) {
      if (this.countryListSdg[i].color === "red") {
        this.colourCode = am4core.color("#d3270d")
      }
      else if (this.countryListSdg[i].color === "green") {
        this.colourCode = am4core.color("#36AD1F")

      }
      else if (this.countryListSdg[i].color === "orange") {
        this.colourCode = am4core.color("#FFA019")

      }
      else if (this.countryListSdg[i].color === "yellow") {
        this.colourCode = am4core.color("#ffff00")

      }
      else if (this.countryListSdg[i].color === "grey" || this.countryListSdg[i].color === "NA") {
        this.colourCode = am4core.color("#808080")
      }
      county1.push({
        "id": this.countryListSdg[i].code,
        "name": this.countryListSdg[i].countryname,
        "value": 100,
        "fill": this.colourCode
      })
    }
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }

  getSdgCountryListMap() {
    let countryId, country;
    let map = am4core.create("mapDiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    map.projection = new am4maps.projections.Mercator();
    am4core.options.autoDispose = true;
    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";

    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];

    polygonTemplate.events.on("hit", function (ev) {
      let cnt = 0;
      // zoom to an object
      // ev.target.series.chart.zoomToMapObject(ev.target);
      let object = ev.target.dataItem.dataContext;
      let c = [];
      for (let i = 0; i < this.sdgCountryMapList.length; i++) {
        if (this.sdgCountryMapList[i].code === object["id"]) {
          cnt = -1
          c.push({
            "id": this.finalCountry[i].id,
            "name": this.finalCountry[i].name,
            "value": 100,
            "fill": am4core.color("#0000FF")
          })
          if (this.sdgCountryMapList[i].score !== null) {
            countryId = this.sdgCountryMapList[i].countryid;
            country = this.sdgCountryMapList[i].country;
            this.gameSummeryService.sdgChallengeId = countryId;
            this.CountryDesignALife = country
            this.selectedCountryObject = this.sdgCountryMapList[i]
            this.gameSummeryService.sdgChallengeId = object["value"];
            this.disabledButtonMap = true;
            this.showAlertFlag = true;
            this.selectedCountryName = country;
            this.translation.get('gameLabel.Selected country', { countryName: country }).subscribe(
              (str) => {
                this.msgText = str;
              })
          }
        }
        else {
          c.push({
            "id": this.finalCountry[i].id,
            "name": this.finalCountry[i].name,
            "value": 100,
            "fill": this.finalCountry[i].fill
          })
        }

      }
      if (cnt == 0) {
        this.showAlertFlag = true;
        this.msgText = this.translation.instant('gameLabel.sdg_warr2');
      }
      polygonSeries.data = c
      polygonTemplate.propertyFields.fill = "fill";
    }, this);
    let county1: any[] = [];
    let value;
    for (let i = 0; i < this.sdgCountryMapList.length; i++) {
      if (this.sdgCountryMapList[i].color === "red") {
        this.colourCode = am4core.color("#d3270d")
        value = 0;
      }
      else if (this.sdgCountryMapList[i].color === "green") {
        this.colourCode = am4core.color("#36AD1F")
        value = 3;

      }
      else if (this.sdgCountryMapList[i].color === "orange") {
        this.colourCode = am4core.color("#FFA019")
        value = 1;

      }
      else if (this.sdgCountryMapList[i].color === "yellow") {
        this.colourCode = am4core.color("#ffff00")
        value = 2;

      }
      else if (this.sdgCountryMapList[i].color === '') {
        this.colourCode = am4core.color("#808080")
        value = 4;
      }
      county1.push({
        "id": this.sdgCountryMapList[i].code,
        "name": this.sdgCountryMapList[i].country,
        "value": value,
        "fill": this.colourCode
      })

    }
    this.finalCountry = (county1);
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";


  }


  median = arr => {
    const mid = Math.floor(arr.length / 2),
      nums = [...arr].sort((a, b) => a - b);
    return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
  };

  dotPlotGraph() {
    this.chartReady = true;
    this.chartColors = [];
    this.lineChartData = [];
    this.lineChartLabels = [];
    this.chartColors[0] = {}
    let graphData = [];
    let i = 0
    this.chartColors[0]['pointBackgroundColor'] = []
    graphData = JSON.parse(JSON.stringify(this.sdgCountryMapList))
    let graphFinalData = [];
    for (let j = 0; j < graphData.length; j++) {
      if (graphData[j].hasOwnProperty("score") && graphData[j].hasOwnProperty("color")) {
        if (graphData[j].score || graphData[j].color) {
          graphData[j].score = (parseFloat(graphData[j].score))
          graphFinalData.push(graphData[j]);
        }
      }
    }
    let dummy
    if (this.flagForSort) {
      dummy = graphFinalData.sort((first, second) => 0 - (first.score < second.score ? -1 : 1));
    }
    else {
      dummy = graphFinalData.sort((first, second) => 0 - (first.score > second.score ? -1 : 1));
    }
    for (let sdg of dummy) {
      if (sdg.color == 'red') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = "#d3270d"
        this.lineChartData[i] = (sdg.score)
      }
      else if (sdg.color == 'orange') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = "#FFA019"
        this.lineChartData[i] = (sdg.score)
      }
      else if (sdg.color == 'yellow') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = " #ffff00"
        this.lineChartData[i] = (sdg.score)
      }
      else if (sdg.color == 'green') {
        this.lineChartLabels[i] = sdg.country
        this.chartColors[0]['pointBackgroundColor'][i] = "#36AD1F"
        this.lineChartData[i] = (sdg.score)
      }
      i++

    }
    this.labelMFL = [
      {
        data: this.lineChartData,
        label: this.translation.instant('gameLabel.SDG')
      }
    ];
    this.lineChartOptions = {
      responsive: true,
      scales: {
        yAxes: [{
          ticks: {

          },
          scaleLabel: {
            display: true,
            labelString: this.translation.instant('gameLabel.Subgoal_score')
          }
        }],
        xAxes: [{
          display: false,
          ticks: {

          }
        }],
      },
    };

  }
  closeAllMap() {
    this.firstWindowFlag = false;
    this.secondWindowFlag = false;
    this.thirdWindowFlag = false;
    this.fourWindowFlag = false;
    this.fifthWindowFlag = false;
    am4core.disposeAllCharts();
  }
  clickOk() {
    this.showAlertFlag = false;
  }

  clickMoreInfo() {
    this.moreInfoFlag = true
    let s1 = this.translation.instant('gameLabel.Minimum_info');
    let s2 = this.translation.instant('gameLabel.Maximum_info');
    let s3 = this.translation.instant('gameLabel.Median_info');
    this.moreInforStm = s1 + "<br><br>" + s2 + "<br><br>" + s3;
  }
  successMsgWindowClose() {
    this.moreInfoFlag = false;
  }
  public chartClicked(e: any): void {
  }
  public chartHovered(e: any): void {
  }

  generateMultipleLineChart() {
    this.chartReady = false;
    this.lineChartLegend = true;
    this.lineChartType = 'line';
    this.lineChartPlugins = [];
    this.lineChartLabels = [
      'red',
      'orange',
      "yellow",
      'green',
    ];
    this.lineChartOptions = {
      bezierCurve: false,
      elements: {
        line: {
          tension: 0
        }
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            ticks: {
            },
            gridLines: {},
          },
        ],
        xAxes: [{
          display: false,
          ticks: {

          }
        }],
      },
    };
    let min, max;
    if (this.flagForSort) {
      max = 'red'
      min = 'green'
    }
    else {
      max = 'green'
      min = 'red'
    }
    this.lineChartColors = [
      {
        borderColor: min,
        backgroundColor: min,
      },
      {
        borderColor: 'blue',
        backgroundColor: 'blue',
      },
      {
        borderColor: max,
        backgroundColor: max,
      },
    ];
    this.lineChartData = [
      {
        data: this.allMinValue,
        label: this.translation.instant('gameLabel.Min'),
        fill: false,

      },
      {
        data: this.allMediunValue,
        label: this.translation.instant('gameLabel.Median'),
        fill: false,

      }, {
        data: this.allMaxValue,
        label: this.translation.instant('gameLabel.Max'),
        fill: false,

      },];
  }

}


