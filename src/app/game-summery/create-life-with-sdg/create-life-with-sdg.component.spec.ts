import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLifeWithSdgComponent } from './create-life-with-sdg.component';

describe('CreateLifeWithSdgComponent', () => {
  let component: CreateLifeWithSdgComponent;
  let fixture: ComponentFixture<CreateLifeWithSdgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateLifeWithSdgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLifeWithSdgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
