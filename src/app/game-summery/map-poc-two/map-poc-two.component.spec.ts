import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapPocTwoComponent } from './map-poc-two.component';

describe('MapPocTwoComponent', () => {
  let component: MapPocTwoComponent;
  let fixture: ComponentFixture<MapPocTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapPocTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapPocTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
