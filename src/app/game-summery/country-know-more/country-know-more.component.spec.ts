import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryKnowMoreComponent } from './country-know-more.component';

describe('CountryKnowMoreComponent', () => {
  let component: CountryKnowMoreComponent;
  let fixture: ComponentFixture<CountryKnowMoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryKnowMoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryKnowMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
