import { Component, OnInit } from '@angular/core';
import { HomeComponent } from 'src/app/home/home.component';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';

@Component({
  selector: 'app-country-know-more',
  templateUrl: './country-know-more.component.html',
  styleUrls: ['./country-know-more.component.css']
})
export class CountryKnowMoreComponent implements OnInit {
  activeClassHealth='show active';
  sdgCountry
  govermentSocialServices;
  activeClassSocial='';
  activeClassPolitical='';
  activeClassEconomical='';
  activeClassHealthForTab='active';
  activeClassSocialForTab='';
  activeClassPoliticalForTab='';
  activeClassEcomnomicalForTab='';
  showPoliticFlag:boolean=true;
  showSocialFlag:boolean=true; 
  showHelathFlag:boolean=true;
  showEconomicalFlag:boolean=true;
  dispaly;
  checkFormat
  cityLink="https://en.wikipedia.org/wiki/";
  myUrl;
  displayCapital: boolean;
  displayCity: boolean;
  displayMap = true;
   displayWiki = false;
   displayLonly = false;
   checkFlag = true;
   dispalyEncyclopedia=false;
   dispalyNationalGeographic=false;
   displayUnesco=false;
   dispalyClimate:boolean=false;
   disaplayHealth:boolean=false;
   dispalyHdr:boolean=false;
   climateClassForTab;
   healthClassForTab;
   hdrClassForTab
   mapClassForTab = "active";
   lonlyClassForTab;
   wikiClassForTab;
   cityclassForTab;
   capitalclassForTab;
   encyclopediaClassForTab;
   nationalClassForTab
   unescoClassForTab
   classvalue;
   countryShort;
   wikiUrl = "https://en.wikipedia.org/wiki/";
   sdgUrl="https://dashboards.sdgindex.org/profiles/"
 ciaUrl="https://www.cia.gov/the-world-factbook/countries/";
 encyclopediaUrl="https://www.britannica.com/place/";
 nationalGeographicUrl="https://kids.nationalgeographic.com/geography/countries/article/";
 clicmateUrl="https://en.wikipedia.org/wiki/Category:Ethnic_groups_in_";
 helathUrl="https://data.unicef.org/country/";
 hdrUrl="https://hdr.undp.org/data-center/specific-country-data#/countries/"
 biodiversityUrl="https://www.ibat-alliance.org/country_profiles/"
 readMoreFlag:boolean=false;
 longitude;
 latitude;
 mapZoom=3;
 cityName
 currentCountry
  constructor(public honeService:HomeService,
    public modalWindowService: modalWindowShowHide,
    ) { }

  ngOnInit(): void {
    this.readMoreFlag=true;
    this.currentCountry=this.honeService.allPerson['SELF'].country;
    this.countryShort=(this.currentCountry.country.slice(0, 3));
    this.countryShort=this.countryShort.toLowerCase();
    this.changeMarkerData(this.currentCountry);
     this.displayMap=true;
     this.displayData('map');
  }
  checkTabView(tabName){
    if(tabName==='Political'){
      this.activeClassHealth="";
      this.activeClassPolitical="show active";
      this.activeClassSocial="";
      this.activeClassEconomical="";
      this.activeClassEcomnomicalForTab="";
      this.activeClassHealthForTab="";
      this.activeClassPoliticalForTab="active";
      this.activeClassSocialForTab="";


    }
    else if(tabName==='Social'){
      this.activeClassHealth="";
      this.activeClassPolitical="";
      this.activeClassSocial="show active";
      this.activeClassEconomical="";
      this.activeClassEcomnomicalForTab="";
      this.activeClassHealthForTab="";
      this.activeClassPoliticalForTab="";
      this.activeClassSocialForTab="active";

    }
    else if(tabName==='Economical'){
      this.activeClassHealth="";
      this.activeClassPolitical="";
      this.activeClassSocial="";
      this.activeClassEconomical="show active";
      this.activeClassEcomnomicalForTab="active";
      this.activeClassHealthForTab="";
      this.activeClassPoliticalForTab="";
      this.activeClassSocialForTab="";

    }
    else if(tabName==='Health'){
      this.activeClassHealth="show active";
      this.activeClassPolitical="";
      this.activeClassSocial="";
      this.activeClassEconomical="";
      this.activeClassEcomnomicalForTab="";
      this.activeClassHealthForTab="active";
      this.activeClassPoliticalForTab="";
      this.activeClassSocialForTab="";

    }
  }
  changeMarkerData(changedCounty){
    this.latitude=changedCounty.latitude
    this.longitude=changedCounty.longitude 
  this.cityName=changedCounty.cityName;
  }
  displayData(check) {

    switch (check) {
      case "map":
        this.changeMarkerData(this.honeService.allPerson['SELF'].city);
      //   this.latitude=this.currentCountry.cityLatitude;
      //   this.longitude=this.currentCountry.cityLongitude;
      // this.cityName=this.currentCountry.cityName;  
           this.displayMap = true;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
       this.dispalyEncyclopedia=false;
       this.dispalyNationalGeographic=false;
       this.displayUnesco=false;
       this.disaplayHealth=false;
this.dispalyClimate=false;
this.dispalyHdr=false;
this.hdrClassForTab="";
       this.unescoClassForTab=""
        this.cityclassForTab = "";
        this.mapClassForTab = "active";
        this.healthClassForTab="";

this.climateClassForTab="";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab="",
        this.encyclopediaClassForTab="";
        break;
      case "wiki":
        this.displayMap = false;
        this.displayWiki = true;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia=false;
        this.dispalyNationalGeographic=false;
        this.displayUnesco=false;
        this.disaplayHealth=false;
this.dispalyClimate=false;
this.dispalyHdr=false;
this.hdrClassForTab="";
        this.unescoClassForTab=""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "active";
        this.healthClassForTab="";
this.climateClassForTab="";
        this.nationalClassForTab="",
        this.encyclopediaClassForTab="";
        break;
      case "Lonly":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayCapital = false;
        this.displayLonly = true;
        this.displayCity = false;
        this.dispalyEncyclopedia=false;
       this.dispalyNationalGeographic=false;
       this.disaplayHealth=false;
this.dispalyClimate=false;
       this.displayUnesco=false;
       this.dispalyHdr=false;
this.hdrClassForTab="";
       this.unescoClassForTab=""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "active";
        this.healthClassForTab="";
this.climateClassForTab="";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab="",
        this.encyclopediaClassForTab="";
        break;
      case "capital":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = true;
        this.displayCity = false;
        this.dispalyEncyclopedia=false;
        this.dispalyNationalGeographic=false;
        this.disaplayHealth=false;
this.dispalyClimate=false;
        this.displayUnesco=false;
        this.dispalyHdr=false;
this.hdrClassForTab="";
        this.unescoClassForTab=""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "active";
        this.healthClassForTab="";
this.climateClassForTab="";
        this.wikiClassForTab = "";
        this.nationalClassForTab="",
        this.encyclopediaClassForTab="";
        break;
      case "city":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = true;
        this.dispalyEncyclopedia=false;
       this.dispalyNationalGeographic=false;
       this.displayUnesco=false;
       this.disaplayHealth=false;
this.dispalyClimate=false;
this.dispalyHdr=false;
this.hdrClassForTab="";
          this.unescoClassForTab=""
        this.cityclassForTab = "active";
        this.healthClassForTab="";
this.climateClassForTab="";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab="",
        this.encyclopediaClassForTab="";
        break;
case "biodiversity":
  this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = true;
        this.dispalyEncyclopedia=true;
       this.dispalyNationalGeographic=false;
       this.displayUnesco=false;
       this.disaplayHealth=false;
this.dispalyClimate=false;
this.dispalyHdr=false;
this.hdrClassForTab="";
          this.unescoClassForTab=""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab=""
        this.encyclopediaClassForTab="active"
        this.healthClassForTab="";
this.climateClassForTab="";
        break;
        case "nationalGeographic":
         this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia=false;
        this.dispalyNationalGeographic=true;
        this.displayUnesco=false;
        this.disaplayHealth=false;
this.dispalyClimate=false;
this.dispalyHdr=false;
this.hdrClassForTab="";
          this.unescoClassForTab=""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";

        this.nationalClassForTab="active"
        this.healthClassForTab="";
this.climateClassForTab="";
        this.encyclopediaClassForTab=""
        break;
        case 'sdg':
          if(this.honeService.allPerson['SELF'].country.countryid===122){
           this.sdgCountry="korea-dem-rep"
          }
          else if(this.honeService.allPerson['SELF'].country.countryid===150){
            this.sdgCountry="korea-rep"
          }
          else{
            this.sdgCountry= this.currentCountry.country.split(" ").join('-').toLowerCase();
          }
          this.displayMap = false;
          this.displayWiki = false;
          this.displayLonly = false;
          this.displayCapital = false;
          this.displayCity = false;
          this.dispalyEncyclopedia=false;
          this.dispalyNationalGeographic=false;
          this.disaplayHealth=false;
this.dispalyClimate=false;
          this.displayUnesco=true;
          this.dispalyHdr=false;
this.hdrClassForTab="";
          this.unescoClassForTab="active"
          this.healthClassForTab="";
this.climateClassForTab="";
          this.cityclassForTab = "";
          this.mapClassForTab = "";
          this.lonlyClassForTab = "";
          this.capitalclassForTab = "";
          this.wikiClassForTab = "";
          this.nationalClassForTab=""
          this.encyclopediaClassForTab=""
          break;
case 'climate':
this.dispalyClimate=true;
this.displayMap = false;
this.displayWiki = false;
this.displayLonly = false;
this.displayCapital = false;
this.displayCity = false;
this.dispalyEncyclopedia=false;
this.dispalyNationalGeographic=false;
this.disaplayHealth=false;
this.displayUnesco=false;
this.dispalyHdr=false;
this.hdrClassForTab="";
this.climateClassForTab="active";
this.healthClassForTab="";
this.unescoClassForTab=""
this.cityclassForTab = "";
this.mapClassForTab = "";
this.lonlyClassForTab = "";
this.capitalclassForTab = "";
this.wikiClassForTab = "";
this.nationalClassForTab=""
this.encyclopediaClassForTab=""
break;
case 'health':
  this.countryShort=(this.currentCountry.country.slice(0, 3));
  this.countryShort=this.countryShort.toLowerCase();
  this.disaplayHealth=true;
this.dispalyClimate=false;
this.displayMap = false;
this.displayWiki = false;
this.displayLonly = false;
this.displayCapital = false;
this.displayCity = false;
this.dispalyEncyclopedia=false;
this.dispalyNationalGeographic=false;
this.displayUnesco=false;
this.dispalyHdr=false;
this.hdrClassForTab="";
this.healthClassForTab="active";
this.climateClassForTab="";
this.unescoClassForTab=""
this.cityclassForTab = "";
this.mapClassForTab = "";
this.lonlyClassForTab = "";
this.capitalclassForTab = "";
this.wikiClassForTab = "";
this.nationalClassForTab=""
this.encyclopediaClassForTab=""
break;
case 'human':
  this.countryShort=(this.currentCountry.country.slice(0, 3));
  this.countryShort=this.countryShort.toLowerCase();
  this.disaplayHealth=false;
this.dispalyClimate=false;
this.displayMap = false;
this.displayWiki = false;
this.displayLonly = false;
this.displayCapital = false;
this.displayCity = false;
this.dispalyEncyclopedia=false;
this.dispalyNationalGeographic=false;
this.displayUnesco=false;
this.dispalyHdr=true;
this.hdrClassForTab="active";
this.healthClassForTab="";
this.climateClassForTab="";
this.unescoClassForTab=""
this.cityclassForTab = "";
this.mapClassForTab = "";
this.lonlyClassForTab = "";
this.capitalclassForTab = "";
this.wikiClassForTab = "";
this.nationalClassForTab=""
this.encyclopediaClassForTab=""
break;   
}
  }
  closeReadMore(){
    this.modalWindowService.showKonwMoreFlag=false;
  }

  checkPopulationFormat(Population) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = ( Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }
}
