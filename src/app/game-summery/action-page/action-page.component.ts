import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-action-page',
  templateUrl: './action-page.component.html',
  styleUrls: ['./action-page.component.css']
})
export class ActionPageComponent implements OnInit {
  lifeCountMsg = '';
  totalLives = 0;
  totalCompletedLives = 0
  constructor(private router: Router,
    public translate: TranslateService,
    public modalWindowService: modalWindowShowHide,
    public gameSummary: GameSummeryService,
    public route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.modalWindowService.showDocumentFlag = false

  }

  loadIncompleteLife() {
    this.router.navigate(['/loadincompletegame']);
  }
  loadCompletedLife() {
    this.router.navigate(['/loadCompletedLife']);
  }

  loadDeletedLife() {
    this.router.navigate(['/loadDeletedLife']);
  }

  loadlifeBookData() {
    this.gameSummary.showLifeBookListFlag = true;
    this.router.navigate(['/life-book']);
  }

  loadlifeLerningToolData() {
    this.gameSummary.showLifeDataListFlag = true
    this.router.navigate(['/life-tool']);
  }

  loadLifeDiaryData() {
    this.gameSummary.showLifeDiaryListFlag = true;
    this.router.navigate(['/life-diary-list'])
  }

  loadPassport() {
    this.router.navigate(['/passport'])

  }

  clickCountryLearningTool() {
    this.gameSummary.countryLearningTool = true;
    this.router.navigate(['/phpToolSummary'])
  }

  clickCountryDisparityTool() {
    this.gameSummary.countryDisaparityTool = true;
    this.router.navigate(['/countryDisparityLearningToolSummary'])

  }

  clickCountrySdgTool() {
    this.gameSummary.countrySdgTool = true;
    this.router.navigate(['/sdgSummary'])

  }
  clickCountryGroupTool() {
    this.gameSummary.countryGroupTool = true;
    this.router.navigate(['/countryGroupLearningToolSummary'])

  }


  clickBack() {
    this.router.navigate(['/summary']);
  }
}
