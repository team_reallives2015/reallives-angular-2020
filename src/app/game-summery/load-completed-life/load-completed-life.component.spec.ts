import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadCompletedLifeComponent } from './load-completed-life.component';

describe('LoadCompletedLifeComponent', () => {
  let component: LoadCompletedLifeComponent;
  let fixture: ComponentFixture<LoadCompletedLifeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadCompletedLifeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadCompletedLifeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
