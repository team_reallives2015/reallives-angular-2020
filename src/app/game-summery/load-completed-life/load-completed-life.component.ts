import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-load-completed-life',
  templateUrl: './load-completed-life.component.html',
  styleUrls: ['./load-completed-life.component.css']
})
export class LoadCompletedLifeComponent implements OnInit {
  //pagination
  config: any;
pageRangeLimit:number=30;
currentPage=1
totalLength;

  //
  showLoadAnimation = false;
  showConfirmWindowFlag: boolean = false;
  gameId;
  completeGameData;
  searchText: any;
  docInfoFlag: boolean = false
  constructor(private router: Router,
    public gameSummeryService: GameSummeryService,
    public commonService: CommonService,
    public modalWindowService: modalWindowShowHide,
    public homeService: HomeService,
    public translate: TranslateService,
    public constantService: ConstantService) { }

  ngOnInit(): void {
    this.config = {
      currentPage: this.currentPage,
      itemsPerPage: this.pageRangeLimit,
      totalItems: this.totalLength
    };
    this.modalWindowService.showLoadCompleteLifeFlag = true;
    this.modalWindowService.showWaitScreen = true;
    this.gameSummeryService.getCompleteListForPagination(true,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) // return a Observable with a error message to display
    ).subscribe(res => {
      this.totalLength=res['length']
      this.completeGameData = res['loadGameData'];
     this.config = {
       currentPage: this.currentPage,
       itemsPerPage: this.pageRangeLimit,
       totalItems: this.totalLength
     };
      this.modalWindowService.showWaitScreen = false;
    })
  }

  goBack() {
    this.router.navigate(['/summary'])
  }


  getSocialEcoData(countryId) {
    this.homeService.geteconomicalStatusMetadata(countryId).subscribe(
      res => {
        let statusMetadata = res[0];
        this.gameSummeryService.ecoStatusData = statusMetadata;
        this.gameSummeryService.getWelathMetaData(countryId).subscribe(
          res => {
            if (res !== 'false') {
              this.gameSummeryService.bornCountryWelathMetadata = res;
            }
            else {
              this.gameSummeryService.bornCountryWelathMetadata = false;
            }
          })
      })
  }


  goToHome(gameId, countryId) {
    this.showLoadAnimation = true;
    setTimeout(function () {
      this.gameId = gameId;
      this.getSocialEcoData(countryId);
      localStorage.setItem('gameid', gameId);
      this.router.navigate(
        ['/play-life'],
        { queryParams: { flag: 'true' } }
      );
      this.homeService.activeLinkClassfinanceStatus = "";
      this.homeService.activeLinkClassCountryCapsule = "";
      this.homeService.activeLinkClassLifeSummery = "active";
      this.homeService.activeLinkClassSdgIndicator = "";
      this.homeService.activeLinkClasslifeExepectancy = ""
      this.modalWindowService.showFinanceStatus = false;
      this.modalWindowService.showCountryCapsule = false;
      this.modalWindowService.showSdgIndicator = false;
      this.modalWindowService.showLifeSummery = true;
      this.modalWindowService.showLifeExpectancyGraph = false;
      this.homeService.clicktoGameSummary = true;
      this.homeService.goToObituaryButton = true;
      this.modalWindowService.showGameSummary = false;
      this.modalWindowService.showObituary = false;
      this.modalWindowService.showFeddback = false;
      this.modalWindowService.showBugReport = false;
      this.modalWindowService.showEvent = true;
      this.modalWindowService.showLetter = false;
    }.bind(this), 3000);

  }
  obituaryClick(gameId, countryId) {
    this.showLoadAnimation = true;
    setTimeout(function () {
      this.gameId = gameId;
      this.getSocialEcoData(countryId);
      localStorage.setItem('gameid', gameId);
      this.router.navigate(
        ['/play-life'],
        { queryParams: { flag: 'true' } }
      );
      this.homeService.activeLinkClassfinanceStatus = "";
      this.homeService.activeLinkClassCountryCapsule = "";
      this.homeService.activeLinkClassLifeSummery = "";
      this.homeService.activeLinkClassSdgIndicator = "";
      this.homeService.activeLinkClasslifeExepectancy = "active"
      this.modalWindowService.showFinanceStatus = false;
      this.modalWindowService.showCountryCapsule = false;
      this.modalWindowService.showSdgIndicator = false;
      this.modalWindowService.showLifeSummery = false;
      this.modalWindowService.showLifeExpectancyGraph = true;
      this.homeService.clicktoGameSummary = true;
      this.homeService.goToObituaryButton = false;
      this.modalWindowService.showObituary = true;
      this.modalWindowService.showGameSummary = false;
      this.modalWindowService.showEvent = false;
      this.modalWindowService.showFeddback = false;
      this.modalWindowService.showBugReport = false;
      this.modalWindowService.showLetter = false;
    }.bind(this), 3000);
  }

  gameSummaryClick(gameId, countryId) {
    this.showLoadAnimation = true;
    setTimeout(function () {
      this.gameId = gameId;
      localStorage.setItem('gameid', gameId);
      this.getSocialEcoData(countryId);
      this.router.navigate(
        ['/play-life'],
        { queryParams: { flag: 'true' } }
      ); this.homeService.activeLinkClassfinanceStatus = "";
      this.homeService.activeLinkClassCountryCapsule = "";
      this.homeService.activeLinkClassLifeSummery = "";
      this.homeService.activeLinkClassSdgIndicator = "";
      this.homeService.activeLinkClasslifeExepectancy = "active"
      this.modalWindowService.showFinanceStatus = false;
      this.modalWindowService.showCountryCapsule = false;
      this.modalWindowService.showSdgIndicator = false;
      this.modalWindowService.showLifeSummery = false;
      this.modalWindowService.showLifeExpectancyGraph = true;
      this.homeService.clicktoGameSummary = true;
      this.homeService.goToObituaryButton = false;
      this.modalWindowService.showGameSummary = true;
      this.modalWindowService.showObituary = false;
      this.modalWindowService.showEvent = false;
      this.modalWindowService.showBugReport = false;
      this.modalWindowService.showFeddback = false;
      this.modalWindowService.showLetter = false;
    }.bind(this), 3000);
  }

  rightALetterClick(gameId, countryId) {
    this.showLoadAnimation = true;
    setTimeout(function () {
      this.gameId = gameId;
      localStorage.setItem('gameid', gameId);
      this.getSocialEcoData(countryId);
      this.router.navigate(
        ['/play-life'],
        { queryParams: { flag: 'true' } }
      ); this.homeService.activeLinkClassfinanceStatus = "";
      this.homeService.activeLinkClassCountryCapsule = "";
      this.homeService.activeLinkClassLifeSummery = "";
      this.homeService.activeLinkClassSdgIndicator = "";
      this.homeService.activeLinkClasslifeExepectancy = "active"
      this.modalWindowService.showFinanceStatus = false;
      this.modalWindowService.showCountryCapsule = false;
      this.modalWindowService.showSdgIndicator = false;
      this.modalWindowService.showLifeSummery = false;
      this.modalWindowService.showLifeExpectancyGraph = true;
      this.homeService.clicktoGameSummary = true;
      this.homeService.goToObituaryButton = false;
      this.modalWindowService.showLetter = true;
      this.modalWindowService.showGameSummary = false;
      this.modalWindowService.showObituary = false;
      this.modalWindowService.showEvent = false;
      this.modalWindowService.showBugReport = false;
      this.modalWindowService.showFeddback = false;
    }.bind(this), 3000);
  }

  gotoFeedbackClick(gameId, countryId) {
    this.showLoadAnimation = true;
    setTimeout(function () {
      this.gameId = gameId;
      localStorage.setItem('gameid', gameId);
      this.getSocialEcoData(countryId);
      this.router.navigate(
        ['/play-life'],
        { queryParams: { flag: 'true' } }
      ); this.homeService.activeLinkClassfinanceStatus = "";
      this.homeService.activeLinkClassCountryCapsule = "";
      this.homeService.activeLinkClassLifeSummery = "";
      this.homeService.activeLinkClassSdgIndicator = "";
      this.homeService.activeLinkClasslifeExepectancy = "active"
      this.modalWindowService.showFinanceStatus = false;
      this.modalWindowService.showCountryCapsule = false;
      this.modalWindowService.showSdgIndicator = false;
      this.modalWindowService.showLifeSummery = false;
      this.modalWindowService.showLifeExpectancyGraph = true;
      this.homeService.clicktoGameSummary = true;
      this.homeService.goToObituaryButton = false;
      this.modalWindowService.showGameSummary = false;
      this.modalWindowService.showObituary = false;
      this.modalWindowService.showEvent = false;
      this.modalWindowService.showBugReport = false;
      this.modalWindowService.showFeddback = true;
      this.modalWindowService.showLetter = false;
    }.bind(this), 3000);
  }

  gotoBugReportClick(gameId, countryId) {
    this.showLoadAnimation = true;
    setTimeout(function () {
      this.gameId = gameId;
      localStorage.setItem('gameid', gameId);
      this.getSocialEcoData(countryId);
      this.router.navigate(
        ['/play-life'],
        { queryParams: { flag: 'true' } }
      );
      this.homeService.activeLinkClassfinanceStatus = "";
      this.homeService.activeLinkClassCountryCapsule = "";
      this.homeService.activeLinkClassLifeSummery = "";
      this.homeService.activeLinkClassSdgIndicator = "";
      this.homeService.activeLinkClasslifeExepectancy = "active"
      this.modalWindowService.showFinanceStatus = false;
      this.modalWindowService.showCountryCapsule = false;
      this.modalWindowService.showSdgIndicator = false;
      this.modalWindowService.showLifeSummery = false;
      this.modalWindowService.showLifeExpectancyGraph = true;
      this.homeService.clicktoGameSummary = true;
      this.homeService.goToObituaryButton = false;
      this.modalWindowService.showGameSummary = false;
      this.modalWindowService.showObituary = false;
      this.modalWindowService.showEvent = false;
      this.modalWindowService.showBugReport = true;
      this.modalWindowService.showFeddback = false;
      this.modalWindowService.showLetter = false;
    }.bind(this), 3000);
  }

  clickDeleteLife(gameId) {
    this.gameId = gameId
    this.showConfirmWindowFlag = true
  }


  clickOk() {
    this.gameSummeryService.updateSoftDelateGameFlag(this.gameId, 1).subscribe(
      res => {
        this.completeGameData = res;
        this.showConfirmWindowFlag = false
      }
    )
  }


  clickCancel() {
    this.showConfirmWindowFlag = false;
  }


  clickDocumentDownlode(gameId, flag) {
    if (flag) {
      this.modalWindowService.showDocumentFlag = true;
      this.gameSummeryService.gameId = gameId;
      this.router.navigate(['/downlodeDoc'])
    }
    else {
      this.docInfoFlag = true;
    }
  }
  clickDairy(gameId) {
    this.modalWindowService.showDocumentFlag = true;
    this.gameSummeryService.gameId = gameId;
    this.router.navigate(['/lifeDiary'])
  }
  closeDoc() {
    this.docInfoFlag = false;
  }

  pageChange(newPage: number) {
    this.modalWindowService.showWaitScreen = true;
    this.completeGameData=[];
    this.currentPage=newPage
    this.gameSummeryService.getCompleteListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.completeGameData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }

  onChange(e){
    this.modalWindowService.showWaitScreen = true;
    this.completeGameData=[];
    this.pageRangeLimit= parseInt(e.target.value)
    this.gameSummeryService.getCompleteListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.completeGameData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }
}
