import { Component, OnInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-sdg-world-view',
  templateUrl: './sdg-world-view.component.html',
  styleUrls: ['./sdg-world-view.component.css']
})
export class SdgWorldViewComponent implements OnInit {
  sdgId;
  countryList = [];
  colourCode;
  countries = []
  constructor(public gameSummary: GameSummeryService) { }

  ngOnInit(): void {
    let map = am4core.create("chartdiv", am4maps.MapChart);
    am4core.options.autoDispose = true;
    map.geodata = am4geodata_worldLow;
    map.projection = new am4maps.projections.Mercator();
    let polygonSeries = new am4maps.MapPolygonSeries();
    map.series.push(polygonSeries);
    polygonSeries.exclude = ["AQ"];
    polygonSeries.useGeodata = true;
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    // map.height = 420;
    // map.width = 750;
    // map.homeZoomLevel = 1;
    // map.seriesContainer.draggable = true;
    // map.seriesContainer.resizable = false;
    this.sdgId = 'one';
    this.gameSummary.getSdgCountryColour(this.sdgId).subscribe(
      res => {
        polygonSeries.data = null
        this.countryList = res;
        for (let i = 0; i < this.countryList.length; i++) {
          if (this.countryList[i].color === "red") {
            this.colourCode = am4core.color("#d3270d")
          }
          else if (this.countryList[i].color === "green") {
            this.colourCode = am4core.color("#36AD1F")

          }
          else if (this.countryList[i].color === "orange") {
            this.colourCode = am4core.color("#FFA019")

          }
          else if (this.countryList[i].color === "yellow") {
            this.colourCode = am4core.color("#ffff00")

          }
          else if (this.countryList[i].color === "grey" || this.countryList[i].color === "NA") {
            this.colourCode = am4core.color("#808080")
          }
          polygonSeries.data.push({
            "id": this.countryList[i].code,
            "name": this.countryList[i].country,
            "value": 100,
            "fill": this.colourCode

          })
          polygonTemplate.propertyFields.fill = "fill";
        }
      });
  }
}

