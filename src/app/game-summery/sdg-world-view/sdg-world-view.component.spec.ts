import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgWorldViewComponent } from './sdg-world-view.component';

describe('SdgWorldViewComponent', () => {
  let component: SdgWorldViewComponent;
  let fixture: ComponentFixture<SdgWorldViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgWorldViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgWorldViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
