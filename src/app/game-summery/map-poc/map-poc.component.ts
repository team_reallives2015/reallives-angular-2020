import { Component, OnInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { GameSummeryService } from '../game-summery.service';
@Component({
  selector: 'app-map-poc',
  templateUrl: './map-poc.component.html',
  styleUrls: ['./map-poc.component.css']
})
export class MapPocComponent implements OnInit {

  code: String;
  constructor(public gameSummeryService: GameSummeryService) { }


  ngOnInit(): void {
    let map = am4core.create("chartdiv", am4maps.MapChart);
    am4core.options.autoDispose = true;
    let country = this.gameSummeryService.country;
    let code = country.Code;
    let countryName = country.country;
    map.geodata = am4geodata_worldLow;
    map.projection = new am4maps.projections.Mercator();
    let polygonSeries = map.series.push(new am4maps.MapPolygonSeries());
    polygonSeries.exclude = ["AQ"];
    polygonSeries.useGeodata = true;
    map.height = 420;
    map.width = 750;
    map.homeZoomLevel = 1;
    map.seriesContainer.draggable = true;
    map.seriesContainer.resizable = false;
    map.events.on("ready", function () {
      let polygonTemplate = polygonSeries.getPolygonById(code);
      polygonTemplate.tooltipText = countryName;
      polygonTemplate.fill = am4core.color("#FF0000");
      var hs = polygonTemplate.states.create("hover");
      hs.properties.fill = am4core.color("#FF0000");

    });
  }


}
