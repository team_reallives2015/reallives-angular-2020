import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapPocComponent } from './map-poc.component';

describe('MapPocComponent', () => {
  let component: MapPocComponent;
  let fixture: ComponentFixture<MapPocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapPocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapPocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
