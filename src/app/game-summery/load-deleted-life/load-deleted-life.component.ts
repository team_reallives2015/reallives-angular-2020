import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-load-deleted-life',
  templateUrl: './load-deleted-life.component.html',
  styleUrls: ['./load-deleted-life.component.css']
})
export class LoadDeletedLifeComponent implements OnInit {
   //pagination
   config: any;
   pageRangeLimit:number=30;
   currentPage=1
   totalLength;
     //
  deletedLifeData;
  searchText: any;
  gameId;
  checkLifeFlag: boolean = false;
  showConfirmWindowFlag: boolean = false;
  constructor(private router: Router,
    public gameSummeryService: GameSummeryService,
    public commonService: CommonService,
    public modalWindowService: modalWindowShowHide,
    public homeService: HomeService,
    public constantService: ConstantService) { }

  ngOnInit(): void {
    this.config = {
      currentPage: this.currentPage,
      itemsPerPage: this.pageRangeLimit,
      totalItems: this.totalLength
    };
    this.modalWindowService.showDeletedLifeList = true;
    this.modalWindowService.showWaitScreen = true;
    this.gameSummeryService.getDeletedListForPagination(true,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('deleted game data error')) // return a Observable with a error message to display
    ).subscribe(res => {
      this.totalLength=res['length']
      this.deletedLifeData = res['loadGameData'];
     this.config = {
       currentPage: this.currentPage,
       itemsPerPage: this.pageRangeLimit,
       totalItems: this.totalLength
     };      this.modalWindowService.showWaitScreen = false;

    })
  }

  goBack() {
    this.router.navigate(['/summary'])
  }

  recoverLife(gameId) {
    this.gameId = gameId;
    this.showConfirmWindowFlag = true;
  }

  clickOk() {
    this.gameSummeryService.updateDeletedLife(this.gameId).subscribe(
      res => {
        this.checkLifeFlag = true;
      })
  }

  clickCancel() {
    this.showConfirmWindowFlag = false;
  }

  clickSuccessOk() {
    this.showConfirmWindowFlag = false;
    this.checkLifeFlag = false;
    this.router.navigate(['/summary'])

  }

  pageChange(newPage: number) {
    this.modalWindowService.showWaitScreen = true;
    this.deletedLifeData=[];
    this.currentPage=newPage
    this.gameSummeryService.getDeletedListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.deletedLifeData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }

  onChange(e){
    this.modalWindowService.showWaitScreen = true;
    this.deletedLifeData=[];
    this.pageRangeLimit= parseInt(e.target.value)
    this.gameSummeryService.getDeletedListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.deletedLifeData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }

}
