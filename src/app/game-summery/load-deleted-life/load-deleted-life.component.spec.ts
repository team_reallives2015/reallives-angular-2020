import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadDeletedLifeComponent } from './load-deleted-life.component';

describe('LoadDeletedLifeComponent', () => {
  let component: LoadDeletedLifeComponent;
  let fixture: ComponentFixture<LoadDeletedLifeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadDeletedLifeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadDeletedLifeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
