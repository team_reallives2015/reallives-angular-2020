import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeDairyComponent } from './life-dairy.component';

describe('LifeDairyComponent', () => {
  let component: LifeDairyComponent;
  let fixture: ComponentFixture<LifeDairyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeDairyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeDairyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
