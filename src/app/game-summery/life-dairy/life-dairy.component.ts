import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-life-dairy',
  templateUrl: './life-dairy.component.html',
  styleUrls: ['./life-dairy.component.css']
})
export class LifeDairyComponent implements OnInit {
  eventList;
  constructor(public router: Router,
    public modalWindowService: modalWindowShowHide,
    public gameSummary: GameSummeryService) { }

  ngOnInit(): void {
    this.modalWindowService.showDairy = true;
    this.gameSummary.getAllEventList(this.gameSummary.gameId).subscribe(
      res => {
        this.eventList = res;
      }
    )
  }

  clickToExitButton() {
    if (this.gameSummary.showLifeDiaryListFlag) {
      this.modalWindowService.showDairy = false;
      this.router.navigate(['/summary'])
    }
    else {
      this.modalWindowService.showDairy = false;
      this.router.navigate(['/loadCompletedLife'])
    }

  }

}
