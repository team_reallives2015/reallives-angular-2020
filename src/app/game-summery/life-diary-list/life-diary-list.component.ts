import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HomeService } from 'src/app/home/home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { GameSummeryService } from '../game-summery.service';

@Component({
  selector: 'app-life-diary-list',
  templateUrl: './life-diary-list.component.html',
  styleUrls: ['./life-diary-list.component.css']
})
export class LifeDiaryListComponent implements OnInit {
    //pagination
    config: any;
    pageRangeLimit:number=30;
    currentPage=1
    totalLength;
      //
  completeGameData;
  searchText: any;

  constructor(private router: Router,
    public gameSummeryService: GameSummeryService,
    public commonService: CommonService,
    public modalWindowService: modalWindowShowHide,
    public homeService: HomeService,
    public constantService: ConstantService) { }

  ngOnInit(): void {
    this.config = {
      currentPage: this.currentPage,
      itemsPerPage: this.pageRangeLimit,
      totalItems: this.totalLength
    };
    this.modalWindowService.showWaitScreen = true;
    this.gameSummeryService.getCompleteListForPagination(true,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) // return a Observable with a error message to display
    ).subscribe(res => {
      this.totalLength=res['length']
      this.completeGameData = res['loadGameData'];
     this.config = {
       currentPage: this.currentPage,
       itemsPerPage: this.pageRangeLimit,
       totalItems: this.totalLength
     };
      this.modalWindowService.showWaitScreen = false;
    })
  }

  clickDiary(gameId) {
    this.gameSummeryService.gameId = gameId;
    this.router.navigate(['/lifeDiary'])
  }

  goBack() {
    this.router.navigate(['/summary'])
  }

  pageChange(newPage: number) {
    this.modalWindowService.showWaitScreen = true;
    this.completeGameData=[];
    this.currentPage=newPage
    this.gameSummeryService.getCompleteListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.completeGameData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }

  onChange(e){
    this.modalWindowService.showWaitScreen = true;
    this.completeGameData=[];
    this.pageRangeLimit= parseInt(e.target.value)
    this.gameSummeryService.getCompleteListForPagination(false,this.pageRangeLimit,this.currentPage).pipe(
      catchError(err => of('load game data error')) 
    ).subscribe(res => {
       this.completeGameData = res['loadGameData'];
      this.config = {
        currentPage: this.currentPage,
        itemsPerPage: this.pageRangeLimit,
        totalItems: this.totalLength
      };
      this.modalWindowService.showWaitScreen = false;
    })
  }
}

