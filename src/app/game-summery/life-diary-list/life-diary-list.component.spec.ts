import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeDiaryListComponent } from './life-diary-list.component';

describe('LifeDiaryListComponent', () => {
  let component: LifeDiaryListComponent;
  let fixture: ComponentFixture<LifeDiaryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeDiaryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeDiaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
