import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryDisparityToolSummaryComponent } from './country-disparity-tool-summary.component';

describe('CountryDisparityToolSummaryComponent', () => {
  let component: CountryDisparityToolSummaryComponent;
  let fixture: ComponentFixture<CountryDisparityToolSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryDisparityToolSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDisparityToolSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
