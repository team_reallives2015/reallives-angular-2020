import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-country-disparity-tool-summary',
  templateUrl: './country-disparity-tool-summary.component.html',
  styleUrls: ['./country-disparity-tool-summary.component.css']
})
export class CountryDisparityToolSummaryComponent implements OnInit {

  constructor(public router: Router,
    public gameSummary: GameSummeryService,
    public commonService: CommonService, public phpToolService: PhpToolService) { }

  ngOnInit(): void {
    this.phpToolService.flowFromCountryDisaparity = true;
  }

  clickFromMap() {
    this.router.navigate(['/countryMap'])
  }

  clickFromList() {
    this.router.navigate(['/countryList'])
  }



  clickToExitButton() {
    if (this.gameSummary.countryDisaparityTool) {
      this.gameSummary.countryDisaparityTool = false;
      this.phpToolService.flowFromCountryDisaparity = false;
      this.router.navigate(['/action-page']);
    }
    else {
      this.phpToolService.flowFromCountryDisaparity = false;
      this.commonService.close();
    }
  }
}
