import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PhpToolService } from '../data-learning-tool-php/php-tool.service';
import { CommonService } from '../shared/common.service';

@Component({
  selector: 'app-country-disparity-tool',
  templateUrl: './country-disparity-tool.component.html',
  styleUrls: ['./country-disparity-tool.component.css']
})
export class CountryDisparityToolComponent implements OnInit {
  token;
  constructor(public router: Router, public translate: TranslateService,
    public phpToolService: PhpToolService,
    public commonService: CommonService,
    public activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.phpToolService.flowFromCountryDisaparity = true;
    this.token = (this.activatedRouter.snapshot.queryParamMap.get('token') || 0);
    // this.token = "63ad1fb893312510221887f1"
    localStorage.setItem('token', this.token);
    this.router.navigate(['/language']);
  }

}
