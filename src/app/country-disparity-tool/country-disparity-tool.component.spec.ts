import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryDisparityToolComponent } from './country-disparity-tool.component';

describe('CountryDisparityToolComponent', () => {
  let component: CountryDisparityToolComponent;
  let fixture: ComponentFixture<CountryDisparityToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryDisparityToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDisparityToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
