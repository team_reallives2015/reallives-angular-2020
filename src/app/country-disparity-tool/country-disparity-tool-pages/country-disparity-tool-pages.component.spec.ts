import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryDisparityToolPagesComponent } from './country-disparity-tool-pages.component';

describe('CountryDisparityToolPagesComponent', () => {
  let component: CountryDisparityToolPagesComponent;
  let fixture: ComponentFixture<CountryDisparityToolPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryDisparityToolPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDisparityToolPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
