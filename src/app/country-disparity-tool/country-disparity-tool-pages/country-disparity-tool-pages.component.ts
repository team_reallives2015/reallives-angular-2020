import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';

@Component({
  selector: 'app-country-disparity-tool-pages',
  templateUrl: './country-disparity-tool-pages.component.html',
  styleUrls: ['./country-disparity-tool-pages.component.css']
})
export class CountryDisparityToolPagesComponent implements OnInit {
  playLifeWarrFlag: boolean = false;
  selectedCountryName = 'Null';
  state = "pageOne";
  gameId;
  selectedcountry = [];
  designALifeObject = {};
  classForCountryOne = ""
  classForCountryTwo = '';
  msgText;
  //graph
  multi: any[] = [];
  multi2: any[] = [];
  multi3: any[] = [];
  showGraphWindow: boolean = false;
  maxLenght: number = 40;
  yTrimValue: boolean = true;
  schemeType: string = 'linear';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  maxXAxisTickLength: number = 10
  gradient: boolean = false;
  showLegend: boolean = false;
  showLegend1: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = this.translate.instant('gameLabel.Country');
  yAxisLabelReg: any;
  showYAxisLabel: boolean = true;
  xAxisLabel: string = this.translate.instant('gameLabel.Population');
  xAxisLabelReg: any;
  colorSchemeBorn: any;
  activeClassForDemo: any;
  activeClassForPerCapita = "nav-item nav-link";
  activeClassForLFO = "nav-item nav-link";
  activeClassForIndex = "nav-item nav-link";
  activeClassForSdg = "nav-item nav-link";
  onClickDemoGraphic: boolean = true;
  onClickPerCapita: boolean = false;
  onCLickLabourForce: boolean = false;
  onCLickSdgGroup: boolean = false;
  onClickIndexRating: boolean = false;
  onClickSdgData: boolean = false;
  sentenceTextWindow1: string = "";
  bithString: string = "";
  deathString: string = "";
  primarySchoolString: string = "";
  happinessString: string = "";
  valurForHappiAndCorr: any;
  giniIndexString: string = "";
  labourForceString1: string = "";
  labourForceString2: string = "";
  sexRatio: string = "";
  accesEleString = "";
  valueForHdi: any;
  sexRatioValue: any;
  sdgScoreBorn: any;
  sdgScoreRegister: any;
  populationString: any;
  welthGraphDataNotGenrateFlag: boolean = false;
  status: any;
  statusValue: any;
  firstValue: any;
  secondValue: any;
  thirdValue: any;
  fourthValue: any;
  firstValueReg: any;
  secondValueReg: any;
  thirdValueReg: any;
  fourthValueReg: any;
  statusMetadata: any
  oldStatusValue: any;
  registerStatusData: any;
  incomeGraphStringOne: any;
  incomeGraphStringTwo: any;
  incomeGraphStringThree: any;
  registerCountryExchangeRate: any;
  stringReturnValue: any;
  classvalue: any;
  poupulationValue: any;
  showAlertFlag: boolean = false;
  stringRetutnText: any;
  senntenceGeneratedFlag: any;
  single: any;
  checkFormat: any;
  bornCountryGroupList: any;
  registerCountryGroupList: any;
  groupIndex: any
  quetionayMetadata: any
  colorScheme = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorScheme1 = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA'],
  };
  colorScheme3 = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#0000FF']
  };
  view: any;
  view1: any;
  view2: any;
  view3: any;
  view4: any;
  multi1: any;
  showDataLabel = true;
  sdgData: any;
  sdgDispalyArrayBorn: any;
  sdgDispalyArrayRegister: any;
  webLink = "https://dashboards.sdgindex.org/profiles/";
  countryGroupTitle: any;
  countryGroupIframeFlag: boolean = false;
  colorScheme2 = {
    domain: ['#00FFFF', '#567caa', '#567caa']
  };

  formatDataLabel(value: any) {
    return "$" + value;
  }
  constructor(public commonService: CommonService,
    public modalWindowService: modalWindowShowHide,
    public router: Router,
    public gameSummary: GameSummeryService,
    public constantService: ConstantService,
    public translate: TranslateService,
    public gameSummeryService: GameSummeryService,
    public translationService: TranslationService,
    public phpToolService: PhpToolService) { }

  ngOnInit(): void {
    this.view = [320, 200];
    this.view1 = [320, 200];
    this.view2 = [700, 400];
    this.view3 = [1000, 200];
    this.view4 = [600, 200];
    this.tabClickDemo();
  }

  clickBack(state) {
    if (state === 'pageOne' || state === 'pageTwo' || state === 'pageThree' || state === 'pageFour') {
      this.router.navigate(['/countryDisparityLearningToolSummary'])
    }
    else if (state === 'pageFive') {
      this.state = "pageOne"
    }
  }

  clickPageOne() {
    this.state = "pageOne"
    this.tabClickDemo();
  }

  clickPageTwo() {
    this.state = "pageTwo"
    this.tabClickperCapita();

  }

  clickPageThree() {
    this.state = "pageThree"
    this.tabClickLFO();

  }

  clickPageFour() {
    this.state = "pageFour"

  }

  clickToExitButton() {
    if (this.gameSummary.countryDisaparityTool) {
      this.gameSummary.countryDisaparityTool = false;
      this.phpToolService.flowFromCountryDisaparity = false;
      this.router.navigate(['/action-page']);
    }
    else {
      this.phpToolService.flowFromCountryDisaparity = false;
      this.commonService.close();
    }
  }


  yAxisTickFormatting(value: any) {
    return this.percentTickFormatting(value);
  }

  percentTickFormatting(val: any): string {
    return val.toLocaleString() + '%';
  }
  checkColourPatternForGraph(x: any, y: any, classIndex: any) {
    if (classIndex === 0) {
      let val = this.checkCssForSpecific(x, y)
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
    else if (classIndex === 1) {
      let val = this.checkCss(x, y);
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
  }
  generateStringValue(Bi: any, Ri: any) {
    let B = parseFloat(Bi);
    let R = parseFloat(Ri);
    if (B < R) {
      this.stringReturnValue = ((100 * ((R - B) / R))).toFixed(2);
      this.valurForHappiAndCorr = (R - B).toFixed(2);
      this.valueForHdi = (100 * (R - B) / R).toFixed(2)
      this.poupulationValue = (((B) / R) * 100).toFixed(2)
    }
    else if (B > R) {
      this.stringReturnValue = (B / R).toFixed(2)
      this.valurForHappiAndCorr = (B - R).toFixed(2);
      this.valueForHdi = ((B / R - 1) * 100).toFixed(2)
      this.poupulationValue = (B / R).toFixed(2);
    }
    else {
      this.stringReturnValue = 0
      this.valurForHappiAndCorr = 0
      this.valueForHdi = 0;
      this.poupulationValue = 0;
    }
    return this.stringReturnValue
  }

  generateStringSubString(B: any, R: any) {
    B = parseFloat(B);
    R = parseFloat(R);
    if (B < R) {
      this.stringRetutnText = this.translate.instant('gameLabel.%_less_likely');
      this.bithString = this.translate.instant('gameLabel.%_fewer');
      this.deathString = this.translate.instant('gameLabel.%_less_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.%_less');
      this.happinessString = this.translate.instant('gameLabel.ranks_above');
      this.giniIndexString = this.translate.instant('gameLabel.lower');
      this.sexRatio = this.translate.instant('gameLabel.less');
      this.accesEleString = this.translate.instant('gameLabel.%_less');
      this.populationString = "%"
    }
    else if (B > R) {
      this.stringRetutnText = this.translate.instant('gameLabel.times_more_likely');
      this.bithString = this.translate.instant('gameLabel.times_more');
      this.deathString = this.translate.instant('gameLabel.times_more_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.times_more');
      this.happinessString = this.translate.instant('gameLabel.ranks_below');
      this.giniIndexString = this.translate.instant('gameLabel.higher');
      this.sexRatio = this.translate.instant('gameLabel.more');
      this.accesEleString = this.translate.instant('gameLabel.times_more');
      this.populationString = this.translate.instant('gameLabel.times');

    }
    else {
      this.populationString = this.translate.instant('gameLabel.almost_equal');
      this.stringRetutnText = this.translate.instant('gameLabel.as_good_as');
    }
    return this.stringRetutnText;

  }

  getSexRationValue(B: any, R: any) {
    B = parseFloat(B);
    R = parseFloat(R);
    this.sexRatioValue = Math.abs((R - B) * 1000).toFixed(2);
  }

  checkSentenceGenaerateOrNot(B: any, R: any) {
    if (B === 0 || R === 0 || B === "NA" || R === "NA") {
      this.senntenceGeneratedFlag = true;
    }
    else {
      this.senntenceGeneratedFlag = false;
    }
  }

  onButtonClickGraphChange(x: any, y: any, fieldName: any, classIndex: any) {
    if (fieldName === 'Population') {
      this.xAxisLabel = this.translate.instant('gameLabel.Population');
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (this.poupulationValue == 0) {
        this.populationString = this.translate.instant('gameLabel.almost_equal');

        this.translate.get('gameLabel.populationCD1', { country: this.phpToolService.selectedBornCountryObject.country, populationString: this.populationString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // Population of {{country}} is {{populationString}} that of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.toolService.selectedBornCountryObject.country+" is "+this.populationString+" that of "+this.toolService.selectedRegisterCountryObject.country+"'s population.";
      }
      else {
        this.translate.get('gameLabel.populationCD2', { country: this.phpToolService.selectedBornCountryObject.country, poupulationValue: this.poupulationValue, populationString: this.populationString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        //     Population of {{country}} is {{poupulationValue}} {{populationString}}  of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.toolService.selectedBornCountryObject.country+" is "+this.poupulationValue+this.populationString+" of "+this.toolService.selectedRegisterCountryObject.country+"'s population.";
      }
      this.colorScheme = {
        domain: ['#00FFFF', '#00FFFF']
      };
    }
    else if (fieldName === "Sex Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Sex_Ratio');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.getSexRationValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.sexRatioValue) === 0) {
          this.sexRatio = this.translate.instant('gameLabel.as_good_as')
          this.translate.get('gameLabel.sexRationDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If  "+this.toolService.selectedBornCountryObject.country+" is your home country, at birth, there will be equal, boys per 1000 girls, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.sexRationDC2', { country: this.phpToolService.selectedBornCountryObject.country, sexRatioValue: Math.round(this.sexRatioValue), sexRatio: this.sexRatio, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, at birth, there will be , {{sexRatioValue}} {{sexRatio}} boys per 1000 girls than {{rcountry}}.
          // this.sentenceTextWindow1="If  "+this.toolService.selectedBornCountryObject.country+" is your home country, at birth, there will be , "+ Math.round(this.sexRatioValue) +" "+this.sexRatio+" boys per 1000 girls than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Birth Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Birth_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.BirthRateDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have equal babies, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
      }
      else {
        this.translate.get('gameLabel.BirthRateDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, bithString: this.bithString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have "+ value + this.bithString+ " babies than " +this.toolService.selectedRegisterCountryObject.country +".";
      }
      this.checkColourPatternForGraph(x, y, classIndex);

    }
    else if (fieldName === "Death Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Death_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.DeathRatioDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have equal risk of dying, compared to {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have equal risk of dying, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.DeathRatioDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, deathString: this.deathString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{deathString}} of dying than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have "+ value + this.deathString+ " of dying than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Infant Mortality Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Infant_Mortality_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.InfantMortalityRateDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are equally likely to die in infancy, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.InfantMortalityRateDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you are {{value}} {{stringRetutnText}} to die in infancy than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are "+ value + this.stringRetutnText+ " to die in infancy than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Primary School") {
      this.xAxisLabel = this.translate.instant('gameLabel.Primary_School_Enrollment');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.primarySchoolDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have equal access to Primary School, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        } else {
          this.translate.get('gameLabel.primarySchoolDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{primarySchoolString}} access to Primary School than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have "+ value +this.primarySchoolString+ " access to Primary School than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "UnEmployment Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Unemployment_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.UnEmploymentRateDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are equally likely to be unemployed, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.UnEmploymentRateDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // "If {{country}} is your home country, you will spend {{value}} {{primarySchoolString}} money on health care than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are "+ value +this.stringRetutnText+ " to be unemployed than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Health Exp. per capita") {
      this.xAxisLabel = this.translate.instant('gameLabel.Health_Exp_per_capita');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.healthExpDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will spend equal money on health care, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.healthExpDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will spend "+ value +this.primarySchoolString+ " money on health care than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "PPP") {
      this.xAxisLabel = this.translate.instant('gameLabel.PPP');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {

          this.translate.get('gameLabel.pppDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will earn equal money, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.pppDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will earn "+ value +this.primarySchoolString+ " money than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Happiness Score") {
      this.xAxisLabel = this.translate.instant('gameLabel.Happiness_Rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.HappinessScoreDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" is almost equal in happiness index, compared to "+this.toolService.selectedRegisterCountryObject.country ;
        } else {
          this.translate.get('gameLabel.HappinessScoreDC2', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.toolService.selectedRegisterCountryObject.country +","+this.toolService.selectedBornCountryObject.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in happiness index.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Corruption") {
      this.xAxisLabel = this.translate.instant('gameLabel.Corruption');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.CorruptionDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" is equal in the list of the most corrupt countries, compared to "+this.toolService.selectedRegisterCountryObject.country;
        }
        else {
          this.translate.get('gameLabel.CorruptionDC2', { rcountry: this.phpToolService.selectedRegisterCountryObject.country, country: this.phpToolService.selectedBornCountryObject.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.toolService.selectedRegisterCountryObject.country +","+this.toolService.selectedBornCountryObject.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the list of the most corrupt countries.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Sdgi Rank") {
      this.xAxisLabel = this.translate.instant('gameLabel.SDG_rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {

          this.translate.get('gameLabel.SdgiScoreDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" is equal in the SDG rank, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.SdgiScoreDC2', { country: this.phpToolService.selectedBornCountryObject.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.toolService.selectedRegisterCountryObject.country +","+this.toolService.selectedBornCountryObject.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the SDG rank than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Hdi") {
      this.xAxisLabel = this.translate.instant('gameLabel.HDI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valueForHdi) === 0) {
          this.translate.get('gameLabel.hdiDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has  equal HDI, compared to "+ this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.hdiDC2', { country: this.phpToolService.selectedBornCountryObject.country, valueForHdi: this.valueForHdi, giniIndexString: this.giniIndexString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has  "+ this.valueForHdi +"% "+this.giniIndexString+ "  HDI than "+ this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Gini") {
      this.xAxisLabel = this.translate.instant('gameLabel.GINI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if ((x - y) === 0 || (y - x) === 0) {
          this.translate.get('gameLabel.giniDc1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has equal income inequality among rich and poor.";
        }
        else {
          this.translate.get('gameLabel.giniDc2', { country: this.phpToolService.selectedBornCountryObject.country, giniIndexString: this.giniIndexString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has  "+ this.giniIndexString + " income inequality among rich and poor.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Access to Electricity") {
      this.xAxisLabel = this.translate.instant('gameLabel.Electricity_Access');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.electricityDC1', { country: this.phpToolService.selectedBornCountryObject.country, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.toolService.selectedBornCountryObject.country+" you will access equal electricity, compared to "+ this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.electricityDC2', { country: this.phpToolService.selectedBornCountryObject.country, value: value, accesEleString: this.accesEleString, rcountry: this.phpToolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.toolService.selectedBornCountryObject.country+" you will have  "+ value +this.accesEleString+ " access to electricity than " +this.toolService.selectedRegisterCountryObject.country+".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else {
      this.sentenceTextWindow1 = "N/A";
      this.checkColourPatternForGraph(x, y, classIndex);
    }

    this.single = [
      {
        name: this.phpToolService.selectedBornCountryObject.country,
        value: x,
      },
      {
        name: this.phpToolService.selectedRegisterCountryObject.country,
        value: y,
      },
    ];
    Object.assign(this.single);
  }

  tabClickDemo() {
    this.activeClassForDemo = "nav-item nav-link active"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = true;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.phpToolService.selectedBornCountryObject.Population, this.phpToolService.selectedRegisterCountryObject.Population, 'Population', 3)
    this.single = [
      {
        name: this.phpToolService.selectedBornCountryObject.country,
        value: this.phpToolService.selectedBornCountryObject.Population,
      },
      {
        name: this.phpToolService.selectedRegisterCountryObject.country,
        value: this.phpToolService.selectedRegisterCountryObject.Population,
      },
    ];
    Object.assign(this.single);

  }
  tabClickperCapita() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link active";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = true;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.phpToolService.selectedBornCountryObject.PrimarySchool, this.phpToolService.selectedRegisterCountryObject.PrimarySchool, 'Primary School', 1);
    this.single = [
      {
        name: this.phpToolService.selectedBornCountryObject.country,
        value: this.phpToolService.selectedBornCountryObject.PrimarySchool,
      },
      {
        name: this.phpToolService.selectedRegisterCountryObject.country,
        value: this.phpToolService.selectedRegisterCountryObject.PrimarySchool,
      },
    ];
    Object.assign(this.single);
  }
  tabClickLFO() {
    this.view1 = [320, 200]
    let num1 = this.phpToolService.selectedBornCountryObject.Agriculture;
    let num2 = this.phpToolService.selectedBornCountryObject.Services;
    let num3 = this.phpToolService.selectedBornCountryObject.industry;
    let num4 = this.phpToolService.selectedRegisterCountryObject.Agriculture;
    let num5 = this.phpToolService.selectedRegisterCountryObject.Services;
    let num6 = this.phpToolService.selectedRegisterCountryObject.industry;
    let largest;
    let occupation;
    let roccupation;
    if (num1 >= num2 && num1 >= num3) {
      largest = num1.toFixed(2);
      occupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num2 >= num1 && num2 >= num3) {
      largest = num2.toFixed(2);
      occupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      largest = num3.toFixed(2);
      occupation = this.translate.instant('gameLabel.Industry1');
    }

    if (num4 >= num5 && num4 >= num6) {
      largest = num1.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num5 >= num4 && num5 >= num6) {
      largest = num2.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      largest = num3.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Industry1');
    }
    this.translate.get('gameLabel.LEbourForce1', { country: this.phpToolService.selectedBornCountryObject.country, occupation: occupation }).subscribe(
      (str) => {
        this.labourForceString1 = str;
      })
    this.translate.get('gameLabel.LEbourForce2', { roccupation: roccupation, rcountry: this.phpToolService.selectedRegisterCountryObject.country, largest: largest, country: this.phpToolService.selectedBornCountryObject.country }).subscribe(
      (str) => {
        this.labourForceString2 = str;
      })
    // this.labourForceString1="In "+this.toolService.selectedBornCountryObject.country+", most likely your occupation will be in the "+occupation+ " Sector. "
    // this.labourForceString2="If you are working in " + roccupation +" Sector in "+this.toolService.selectedRegisterCountryObject.country +", you will have "+largest+"% jobs available in " +this.toolService.selectedBornCountryObject.country+"."
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link active";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = true;
    this.onClickIndexRating = false;
    this.xAxisLabel = this.translate.instant('gameLabel.Occupation')
    this.colorScheme = {
      domain: ['#5AA454', '#A10A28']
    };
    this.multi = [
      {
        name: this.phpToolService.selectedBornCountryObject.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.phpToolService.selectedBornCountryObject.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.phpToolService.selectedBornCountryObject.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.phpToolService.selectedBornCountryObject.industry,
          },
        ],
      },

      {
        name: this.phpToolService.selectedRegisterCountryObject.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.phpToolService.selectedRegisterCountryObject.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.phpToolService.selectedRegisterCountryObject.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.phpToolService.selectedRegisterCountryObject.industry,
          },
        ],
      },
    ];
    Object.assign(this.multi);

  }
  tabClickIndex() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link active";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickSdgData = false;
    this.onClickIndexRating = true;
    this.onButtonClickGraphChange(this.phpToolService.selectedBornCountryObject.ppp, this.phpToolService.selectedRegisterCountryObject.ppp, 'PPP', 1);
    this.single = [
      {
        name: this.phpToolService.selectedBornCountryObject.country,
        value: this.phpToolService.selectedBornCountryObject.ppp,
      },
      {
        name: this.phpToolService.selectedRegisterCountryObject.country,
        value: this.phpToolService.selectedRegisterCountryObject.ppp,
      },
    ];
    Object.assign(this.single);


  }

  checkPopulationFormat(Population: any) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = (Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }
  checkCss(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }
  checkCssForSpecific(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }
  checkCss1(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }
  checkCssForSpecific1(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }
  onActivate(data: any): void {
    // console.log('Activate', JSON.parse(JSON.stringify(data)));
  }
  onDeactivate(data: any): void {
    // console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
  countrySelectionToPlayLifeFirst() {
    this.classForCountryOne = "country_map_log mx-auto countrySelectionClass"
    this.classForCountryTwo = "country_map_log mx-auto"
    this.selectedcountry = this.phpToolService.selectedBornCountryObject
    this.selectedCountryName = this.phpToolService.selectedBornCountryObject.country
  }


  countrySelectionToPlayLifeSecond() {
    this.classForCountryOne = "country_map_log mx-auto"
    this.classForCountryTwo = " country_map_log mx-auto countrySelectionClass"
    this.selectedcountry = this.phpToolService.selectedRegisterCountryObject
    this.selectedCountryName = this.phpToolService.selectedRegisterCountryObject.country

  }

  playReallives() {
    if (this.selectedcountry.length === 0) {
      this.msgText = this.translate.instant('gameLabel.live_life_warr')
      this.playLifeWarrFlag = true
    }
    else {
      this.phpToolService.flowFromCountryDisaparity = false;
      this.modalWindowService.showCharacterDesighWithGropuFlag = true;
      this.modalWindowService.showWaitScreen = true
      this.designALifeObject["country"] = this.selectedcountry;
      this.designALifeObject["onlyCountry"] = false;
      this.designALifeObject['city'] = null;
      this.designALifeObject["religion"] = null;
      this.designALifeObject["traits"] = null;
      this.designALifeObject['first_name'] = null;
      this.designALifeObject['last_name'] = null;
      this.designALifeObject['nameGroupId'] = null;
      this.designALifeObject['sex'] = null
      this.designALifeObject['urban_rural'] = null;
      this.gameSummeryService.generateCharacterDesignLife(this.designALifeObject, false, false, 0, this.translationService.selectedLang.code, true, 0).subscribe(
        res => {
          this.gameId = res;
          localStorage.removeItem("gameid");
          localStorage.setItem("gameid", this.gameId);
          this.modalWindowService.showWaitScreen = false;
          this.router.navigate(['/createlife']);
        })
    }
  }



  clickPageFive() {
    this.state = "pageFive"
    if (this.translationService.checkGameBuyOrNot) {
      this.msgText = this.translate.instant('gameLabel.live_life')
    }
    else {
      this.msgText = this.translate.instant('gameLabel.buy')

    }
  }


  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }

  warrWindowClose() {
    this.playLifeWarrFlag = false;
  }
}
