import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../shared/common.service';
import { modalWindowShowHide } from '../modal-window-show-hide.service';
import { ViewChild } from '@angular/core';
import { BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';
import { TranslationService } from './translation.service';
import { GameSummeryService } from '../game-summery/game-summery.service';
import { HomeService } from '../home/home.service';
import { GameSettingService } from '../game-setting/game-setting.service';
import { PhpToolService } from '../data-learning-tool-php/php-tool.service';
import { SdgToolServiceService } from '../sdg-data-learning-tool/sdg-tool-service.service';


@Component({
  selector: 'app-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.css']
})
export class TranslationComponent implements OnInit {
  modalRef: BsModalRef;
  title = '';
  setDefaultLang: boolean = false;
  gameSettings;
  flagForDefaultLang: boolean = false;
  successMsgFlag: boolean = false;
  messageText;
  constructor(public translate: TranslateService,
    public router: Router,
    public commonService: CommonService,
    public homeService: HomeService,
    public sdgToolService: SdgToolServiceService,
    config: NgbModalConfig,
    public modalWindowShowHide: modalWindowShowHide,
    public translationService: TranslationService,
    public gameSummeryService: GameSummeryService,
    public gameSettingService: GameSettingService,
    public phpToolService: PhpToolService) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
  }

  @ViewChild('autoShownModal', { static: false }) autoShownModal: ModalDirective;
  isModalShown = false;
  gameId;
  exitButtonFlag: boolean = false;


  ngDoCheck(): void {
    if (!this.setDefaultLang) {
      this.flagForDefaultLang = false;
    }
    else {
      this.flagForDefaultLang = true;
    }

  }
  ngOnInit(): void {
    this.gameSettingService.getAllGameSetting().subscribe(
      res => {
        this.gameSettings = res;
        if (this.gameSettings.DefaultLang === null) {
          this.translationService.setDefaultLanguage();
          this.setDefaultLang = false;
        }
        else {
          this.translationService.setSelectedlanguage(this.gameSettings.DefaultLang);
          this.setDefaultLang = true;
        }
        if (!this.modalWindowShowHide.showLangFromIcon) {
          this.modalWindowShowHide.showLanguageWindow = true;
          if (this.modalWindowShowHide.showLanguageWindow) {
            this.showModal();
          }
        }
        else if (this.modalWindowShowHide.showLangFromIcon) {
          this.modalWindowShowHide.showLanguageWindow = true;
          this.translationService.showLangWindowInnerFlag = false;
          this.showModal();
        }
      })



  }

  showModal(): void {
    this.isModalShown = true;
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onHidden(): void {
    this.isModalShown = false;

  }

  radioButtonValue() {
    this.translate.use(this.translationService.selectedLang.code);
    if (this.translationService.selectedLang.code === this.gameSettings.DefaultLang) {
      this.setDefaultLang = true;
    }
    else {
      this.setDefaultLang = false;
    }

  }

  ClickOk() {
    this.translate.use(this.translationService.selectedLang.code);
    this.translationService.changeLang.emit(this.translationService.selectedLang.code);
    if (this.flagForDefaultLang) {
      this.gameSettings.DefaultLang = this.translationService.selectedLang.code
    }
    else {
      this.gameSettings.DefaultLang = null;
    }
    this.gameSettingService.updateAllGameSetting(this.gameSettings).subscribe(
      res => {
        if (this.gameSettings.DefaultLang === 'ko' || this.translationService.selectedLang.code === 'ko') {
          this.successMsgFlag = true;
          this.hideModal();
          this.showModal();
          this.commonService.fullScreen();
        }
        else {
          this.hideModal();
          if (this.gameSummeryService.flagForStudentAssignMent) {
            this.modalWindowShowHide.showAssignment = true;
            this.modalWindowShowHide.showWaitScreen = true;
            this.gameSummeryService.createLifeWithAssignment(this.gameSummeryService.studentAssId, false, this.translationService.selectedLang.code).subscribe(
              res => {
                if (res.hasOwnProperty('gameId')) {
                  this.gameId = res['gameId'];
                  localStorage.removeItem("gameid");
                  localStorage.setItem("gameid", this.gameId);
                  this.modalWindowShowHide.showWaitScreen = false;
                  this.router.navigate(['/createlife']);
                }
                else if (res.hasOwnProperty('countryGroup')) {
                  this.gameSummeryService.studentAssignmentCountryGroupData = res['countryGroup'];
                  this.gameSummeryService.displayCountryGroupData = true;
                  this.modalWindowShowHide.showCharacterDesighWithGropuFlag = true;
                  this.modalWindowShowHide.showWaitScreen = false;

                }
                else if (res.hasOwnProperty('sdgId')) {
                  this.homeService.sdgId = res['sdgId'];
                  this.modalWindowShowHide.showSdgWithAssignMent = true;
                  this.modalWindowShowHide.showWaitScreen = false;
                  this.modalWindowShowHide.showchearacterDesignSdgFlag = true;
                }
              })
          }
          else if (this.phpToolService.flagForPhpToolClick) {
            this.translationService.checkGameBuyOrNot().subscribe(
              res => {
                this.translationService.gameBuyFlag = res;
                this.gameSummeryService.getPlayerRole().subscribe(
                  res => {
                    this.gameSummeryService.UserRole = res.drole;
                    this.gameSummeryService.licenceId = res.prodId;
                    this.gameSummeryService.maxLifeCount = res.maxLives;
                    this.gameSummeryService.totalPayedLifeCount = res.livesCount
                    this.commonService.fullScreen();
                    this.router.navigate(['/phpToolSummary']);
                  });
              });


          }
          else if (this.phpToolService.flowFromSdg) {
            this.translationService.checkGameBuyOrNot().subscribe(
              res => {
                this.translationService.gameBuyFlag = res;
                this.gameSummeryService.getPlayerRole().subscribe(
                  res => {
                    this.gameSummeryService.UserRole = res.drole;
                    this.gameSummeryService.licenceId = res.prodId;
                    this.gameSummeryService.maxLifeCount = res.maxLives;
                    this.gameSummeryService.totalPayedLifeCount = res.livesCount
                    this.commonService.fullScreen();
                    this.router.navigate(['/sdgSummary']);
                  });

              })
          }
          else if (this.phpToolService.folwFromCountryGroup) {
            this.translationService.checkGameBuyOrNot().subscribe(
              res => {
                this.translationService.gameBuyFlag = res;
                this.gameSummeryService.getPlayerRole().subscribe(
                  res => {
                    this.gameSummeryService.UserRole = res.drole;
                    this.gameSummeryService.licenceId = res.prodId;
                    this.gameSummeryService.maxLifeCount = res.maxLives;
                    this.gameSummeryService.totalPayedLifeCount = res.livesCount
                    this.commonService.fullScreen();
                    this.router.navigate(['/countryGroupLearningToolSummary']);
                  })
              })
          }
          else if (this.phpToolService.flowFromCountryDisaparity) {
            this.translationService.checkGameBuyOrNot().subscribe(
              res => {
                this.translationService.gameBuyFlag = res;
                this.gameSummeryService.getPlayerRole().subscribe(
                  res => {
                    this.gameSummeryService.UserRole = res.drole;
                    this.gameSummeryService.licenceId = res.prodId;
                    this.gameSummeryService.maxLifeCount = res.maxLives;
                    this.gameSummeryService.totalPayedLifeCount = res.livesCount
                    this.commonService.fullScreen();
                    this.router.navigate(['/countryDisparityLearningToolSummary']);
                  })
              })
          }

          else {
            this.router.navigate(['/summary']);
          }
        }
        if(!this.gameSummeryService.newFetureOnceFlag){
       this.gameSummeryService.newFeatureFlag=true;
        }
      })
  }


  clickToShowKoreanInfo() {
    if (this.gameSummeryService.flagForStudentAssignMent) {
      this.modalWindowShowHide.showAssignment = true;
      this.modalWindowShowHide.showWaitScreen = true;
      this.gameSummeryService.createLifeWithAssignment(this.gameSummeryService.studentAssId, false, this.translationService.selectedLang.code).subscribe(
        res => {
          if (res.hasOwnProperty('gameId')) {
            this.gameId = res['gameId'];
            localStorage.removeItem("gameid");
            localStorage.setItem("gameid", this.gameId);
            this.modalWindowShowHide.showWaitScreen = false;
            this.router.navigate(['/createlife']);
          }
          else if (res.hasOwnProperty('countryGroup')) {
            this.gameSummeryService.studentAssignmentCountryGroupData = res['countryGroup'];
            this.gameSummeryService.displayCountryGroupData = true;
            this.modalWindowShowHide.showCharacterDesighWithGropuFlag = true;
            this.modalWindowShowHide.showWaitScreen = false;

          }
          else if (res.hasOwnProperty('sdgId')) {
            this.homeService.sdgId = res['sdgId'];
            this.modalWindowShowHide.showSdgWithAssignMent = true;
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showchearacterDesignSdgFlag = true;
          }
        })
    }
    else if (this.phpToolService.flagForPhpToolClick) {
      this.translationService.checkGameBuyOrNot().subscribe(
        res => {
          this.translationService.gameBuyFlag = res;
          this.commonService.fullScreen();
          this.gameSummeryService.getPlayerRole().subscribe(
            res => {
              this.gameSummeryService.UserRole = res.drole;
              this.gameSummeryService.licenceId = res.prodId;
              this.gameSummeryService.maxLifeCount = res.maxLives;
              this.gameSummeryService.totalPayedLifeCount = res.livesCount
              this.router.navigate(['/phpToolSummary']);
            })
        })
    }
    else if (this.phpToolService.flowFromSdg) {
      this.translationService.checkGameBuyOrNot().subscribe(
        res => {
          this.translationService.gameBuyFlag = res;
          this.gameSummeryService.getPlayerRole().subscribe(
            res => {
              this.gameSummeryService.UserRole = res.drole;
              this.gameSummeryService.licenceId = res.prodId;
              this.gameSummeryService.maxLifeCount = res.maxLives;
              this.gameSummeryService.totalPayedLifeCount = res.livesCount
              this.commonService.fullScreen();
              this.router.navigate(['/sdgSummary']);
            })
        })

    }
    else if (this.phpToolService.folwFromCountryGroup) {
      this.translationService.checkGameBuyOrNot().subscribe(
        res => {
          this.translationService.gameBuyFlag = res;
          this.gameSummeryService.getPlayerRole().subscribe(
            res => {
              this.gameSummeryService.UserRole = res.drole;
              this.gameSummeryService.licenceId = res.prodId;
              this.gameSummeryService.maxLifeCount = res.maxLives;
              this.gameSummeryService.totalPayedLifeCount = res.livesCount
              this.commonService.fullScreen();
              this.router.navigate(['/countryGroupLearningToolSummary']);
            })
        })

    }
    else if (this.phpToolService.flowFromCountryDisaparity) {
      this.translationService.checkGameBuyOrNot().subscribe(
        res => {
          this.translationService.gameBuyFlag = res;
          this.gameSummeryService.getPlayerRole().subscribe(
            res => {
              this.gameSummeryService.UserRole = res.drole;
              this.gameSummeryService.licenceId = res.prodId;
              this.gameSummeryService.maxLifeCount = res.maxLives;
              this.gameSummeryService.totalPayedLifeCount = res.livesCount
              this.commonService.fullScreen();
              this.router.navigate(['/countryDisparityLearningToolSummary']);
            })
        })
    }
    else {
      this.router.navigate(['/summary']);
    }
  }

  successMsgWindowClose() {
    this.successMsgFlag = false;
  }

  clickInfoOk() {
    this.router.navigate(['/summary']);

  }

  clickToExitButton() {
    this.exitButtonFlag = true;
  }

  goToDashbordYes() {
    this.exitButtonFlag = false;
    this.commonService.close();
  }


  goToDashbordNo() {
    this.exitButtonFlag = false;
  }
}

