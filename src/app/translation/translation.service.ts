import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../shared/common.service';


@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  languageArray = [{ name: "English", code: 'en', flagCode: 'in', image: 'assets/images/translation/english.png' }, { name: "Korean", code: 'ko', flagCode: 'kr', image: 'assets/images/translation/korean.png' }]
  selectedLang: any;
  gameBuyFlag: boolean = false;
  showLangWindowInnerFlag: boolean = false;
  showGameSummaryWindow: boolean = false;
  @Output() changeLang = new EventEmitter();

  constructor(public translate: TranslateService,
    public commonService: CommonService,
    private http: HttpClient) { }

  setDefaultLanguage() {
    this.translate.addLangs(['en', 'ko']);
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en |ko/) ? browserLang : 'en');
    this.selectedLang = this.languageArray[0];
    this.translate.use(this.selectedLang.code);
  }

  setSelectedlanguage(code) {
    for (let i = 0; i < this.languageArray.length; i++) {
      if (code === this.languageArray[i].code) {
        this.selectedLang = this.languageArray[i];
        this.translate.use(this.selectedLang.code);
        break;
      }
    }
  }
  checkGameBuyOrNot() {
    return this.http.get<boolean>(`${this.commonService.url}game/checkLicense`);

  }

}
