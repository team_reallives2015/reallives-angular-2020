import { object } from '@amcharts/amcharts4/core';
import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from '../game-summery/game-summery.service';
import { modalWindowShowHide } from '../modal-window-show-hide.service';
import { TranslationService } from '../translation/translation.service';

@Component({
  selector: 'app-passport',
  templateUrl: './passport.component.html',
  styleUrls: ['./passport.component.css']
})
export class PassportComponent implements OnInit {
  openListFlag: boolean = false;
  passportData;
  passport = [];
  selectedList;
  text;
  displayFlag: boolean = false;
  constructor(public router: Router,
    public translate: TranslateService,
    public modalWindowService: modalWindowShowHide,
    public translationService: TranslationService,
    public gameSummary: GameSummeryService) { }

  ngOnInit(): void {
    let scrHeight = window.innerHeight;
    let scrWidth = window.innerWidth;
    if (scrWidth > 1225) {
      this.displayFlag = true;
    }
    this.modalWindowService.showWaitScreen = true;
    this.gameSummary.getAllPassportData().subscribe(
      res => {
        this.passportData = res;
        let keys = object.keys(this.passportData);
        for (let i = 0; i < keys.length; i++) {
          let list = this.passportData[keys[i]]
          let code = list[0].code;
          this.passport.push({
            country: keys[i],
            code: code,
            list: this.passportData[keys[i]],
            count: this.passportData[keys[i]].length
          })

        }
        this.modalWindowService.showWaitScreen = false;
      }
    )
  }


  viewList(i) {
    this.translate.get('gameLabel.Lived_life_list_from_country', { name: this.passport[i].country }).subscribe(
      (str) => {
        this.text = str;
      })
    this.selectedList = this.passport[i].list;
    this.openListFlag = true
  }
  close() {
    this.openListFlag = false
  }

  clickBack() {
    this.router.navigate(['/summary'])
  }

  clickToLoad(gameId) {
    localStorage.removeItem('gameid');
    localStorage.setItem('gameid', gameId);
    this.router.navigate(
      ['/loadgameanimation'],
      { queryParams: { flag: 'true' } }
    );
  }
}
