import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BornScreenSeetingsComponent } from './born-screen-seetings.component';

describe('BornScreenSeetingsComponent', () => {
  let component: BornScreenSeetingsComponent;
  let fixture: ComponentFixture<BornScreenSeetingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BornScreenSeetingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BornScreenSeetingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
