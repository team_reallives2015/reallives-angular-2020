import { Component, DoCheck, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GameSettingService } from '../game-setting/game-setting.service';
import { GameSummeryService } from '../game-summery/game-summery.service';
import { HomeService } from '../home/home.service';
import { modalWindowShowHide } from '../modal-window-show-hide.service';
import { CommonService } from '../shared/common.service';
import { TranslationService } from '../translation/translation.service';

@Component({
  selector: 'app-born-screen-seetings',
  templateUrl: './born-screen-seetings.component.html',
  styleUrls: ['./born-screen-seetings.component.css']
})
export class BornScreenSeetingsComponent implements OnInit,DoCheck {
  screen5: boolean = false;
  screen6: boolean = false;
  screen7: boolean = false;
  screen8: boolean = false;
  screen9: boolean = false;
  gameSettings=[];
  successMsgFlag: boolean = false;
  messageText;
  flagForDemo: boolean = false;
  constructor(public gameSummeryService: GameSummeryService,
    public modalWindowService: modalWindowShowHide,
    public translationService: TranslationService,
    public router: Router,
    public homeService: HomeService,
    public translate: TranslateService,
    public gameSettingService: GameSettingService) { }
  
  ngOnInit(): void {
    this.modalWindowService.bornScreenSeetingsFlag = true;
    this.gameSettingService.getAllGameSetting().subscribe(
      res => {
        this.gameSettings[0] = res;
        this.screen5 = this.gameSettings[0].Screen5;
        this.screen6 = this.gameSettings[0].Screen6;
        this.screen7 = this.gameSettings[0].Screen7;
        this.screen8 = this.gameSettings[0].Screen8;
        this.screen9 = this.gameSettings[0].Screen9;
      })
  }

  updateSetting() {
    this.gameSettingService.updateAllGameSetting(this.gameSettings[0]).subscribe(
      res => {
        this.successMsgFlag = true;
        this.messageText = (this.translate.instant('gameLabel.Save_Settings_Succesfully'));
      }

    )
  }
  successMsgWindowClose() {
    this.successMsgFlag = false;
    this.modalWindowService.bornScreenSeetingsFlag = false;
  }

  close() {
    this.modalWindowService.bornScreenSeetingsFlag = false;
  }

  ngDoCheck(): void {
    if( this.gameSettings.length!==0){
    if (!this.screen5) {
      this.gameSettings[0].Screen5 = false
    }
    else {
      this.gameSettings[0].Screen5 = true
    }
    if (!this.screen6) {
      this.gameSettings[0].Screen6 = false
    }
    else {
      this.gameSettings[0].Screen6 = true
    }
    if (!this.screen7) {
      this.gameSettings[0].Screen7 = false
    }
    else {
      this.gameSettings[0].Screen7 = true
    }
    if (!this.screen8) {
      this.gameSettings[0].Screen8 = false
    }
    else {
      this.gameSettings[0].Screen8 = true
    } if (!this.screen9) {
      this.gameSettings[0].Screen9 = false
    }
    else {
      this.gameSettings[0].Screen9 = true
    }
  }
}

}
