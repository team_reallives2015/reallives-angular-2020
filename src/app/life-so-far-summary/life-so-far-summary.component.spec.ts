import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeSoFarSummaryComponent } from './life-so-far-summary.component';

describe('LifeSoFarSummaryComponent', () => {
  let component: LifeSoFarSummaryComponent;
  let fixture: ComponentFixture<LifeSoFarSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeSoFarSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeSoFarSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
