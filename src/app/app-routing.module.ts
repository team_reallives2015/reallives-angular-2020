import { from } from 'rxjs';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GameSummeryComponent } from './game-summery/game-summery.component';
import { CreateLifeComponent } from './game-summery/create-life/create-life.component';
import { LoadIncompleteLifeComponent } from './game-summery/load-incomplete-life/load-incomplete-life.component';
import { CreateLifeWithSdgComponent } from './game-summery/create-life-with-sdg/create-life-with-sdg.component';
import { LoadCompletedLifeComponent } from './game-summery/load-completed-life/load-completed-life.component';
import { LoadGameAnimationComponent } from './game-summery/load-game-animation/load-game-animation.component';
import { ObituaryPdfComponent } from './home/obituary-pdf/obituary-pdf.component';
import { RootComponent } from './root/root.component';
import { PdfGeneraterComponent } from './pdf-generater/pdf-generater.component';
import { AssignmentComponent } from './game-summery/assignment/assignment.component';
import { CreateLifeWithAssignmentComponent } from './game-summery/create-life-with-assignment/create-life-with-assignment.component';
import { TranslationComponent } from './translation/translation.component';
import { RegisterCountryComponent } from './game-summery/register-country/register-country.component';
import { CountryLearningDocComponent } from './country-learning-doc/country-learning-doc.component';
import { LearningToolPagesComponent } from './data-learning-tool/learning-tool-pages/learning-tool-pages.component';
import { DataLearningToolPhpComponent } from './data-learning-tool-php/data-learning-tool-php.component';
import { CountryListMapComponent } from './data-learning-tool-php/country-list-map/country-list-map.component';
import { CountryListComponent } from './data-learning-tool-php/country-list/country-list.component';
import { ToolSummaryComponent } from './data-learning-tool-php/tool-summary/tool-summary.component';
import { LoadDeletedLifeComponent } from './game-summery/load-deleted-life/load-deleted-life.component';
import { SdgDataLearningToolComponent } from './sdg-data-learning-tool/sdg-data-learning-tool.component';
import { SdgSummaryPageComponent } from './sdg-data-learning-tool/sdg-summary-page/sdg-summary-page.component';
import { SdgCountryPagesComponent } from './sdg-data-learning-tool/sdg-country-pages/sdg-country-pages.component';
import { SdgPagesComponent } from './sdg-data-learning-tool/sdg-pages/sdg-pages.component';
import { SdgStatementsComponent } from './sdg-data-learning-tool/sdg-statements/sdg-statements.component';
import { CountryGroupsLearningToolComponent } from './country-groups-learning-tool/country-groups-learning-tool.component';
import { CountryGroupSummaryComponent } from './country-groups-learning-tool/country-group-summary/country-group-summary.component';
import { CountryGroupPagesComponent } from './country-groups-learning-tool/country-group-pages/country-group-pages.component';
import { CountryDisparityToolPagesComponent } from './country-disparity-tool/country-disparity-tool-pages/country-disparity-tool-pages.component';
import { CountryDisparityToolSummaryComponent } from './country-disparity-tool/country-disparity-tool-summary/country-disparity-tool-summary.component';
import { CountryDisparityToolComponent } from './country-disparity-tool/country-disparity-tool.component';
import { PassportComponent } from './passport/passport.component';
import { CountrySelectionComponent } from './sdg-data-learning-tool/country-selection/country-selection.component';
import { LifeDairyComponent } from './game-summery/life-dairy/life-dairy.component';
import { ActionPageComponent } from './game-summery/action-page/action-page.component';
import { LifeBookComponent } from './game-summery/life-book/life-book.component';
import { LifeLearningToolComponent } from './game-summery/life-learning-tool/life-learning-tool.component';
import { LifeDiaryListComponent } from './game-summery/life-diary-list/life-diary-list.component';

const appRoutes: Routes = [
    { path: '', pathMatch: 'full', component: RootComponent },
    { path: 'play-life', component: HomeComponent },
    { path: 'summary', component: GameSummeryComponent },
    { path: 'language', component: TranslationComponent },
    { path: 'registercountry', component: RegisterCountryComponent },
    { path: 'createlife', component: CreateLifeComponent },
    { path: 'loadincompletegame', component: LoadIncompleteLifeComponent },
    { path: 'createLifeWith-sdg', component: CreateLifeWithSdgComponent },
    { path: 'loadCompletedLife', component: LoadCompletedLifeComponent },
    { path: 'loadgameanimation', component: LoadGameAnimationComponent },
    { path: 'downlode', component: ObituaryPdfComponent },
    { path: 'pdf', component: PdfGeneraterComponent },
    { path: 'showAssignments', component: AssignmentComponent },
    { path: 'createlifeWithAssignment', component: CreateLifeWithAssignmentComponent },
    { path: 'downlodeDoc', component: CountryLearningDocComponent },
    { path: 'learningTool', component: LearningToolPagesComponent },
    { path: 'learningToolPhp', component: DataLearningToolPhpComponent },
    { path: 'sdgLearningToolPhp', component: SdgDataLearningToolComponent },
    { path: 'countryMap', component: CountryListMapComponent },
    { path: 'countryList', component: CountryListComponent },
    { path: 'phpToolSummary', component: ToolSummaryComponent },
    { path: 'loadDeletedLife', component: LoadDeletedLifeComponent },
    { path: 'sdgSummary', component: SdgSummaryPageComponent },
    { path: 'sdgPages', component: SdgPagesComponent },
    { path: 'sdgCountryPages', component: SdgCountryPagesComponent },
    { path: 'sdgCountrySelection', component: CountrySelectionComponent },
    { path: 'sdgStmt', component: SdgStatementsComponent },
    { path: 'countryGroupLearningToolPhp', component: CountryGroupsLearningToolComponent },
    { path: 'countryGroupLearningToolSummary', component: CountryGroupSummaryComponent },
    { path: 'countryGroupPages', component: CountryGroupPagesComponent },
    { path: 'countryDisaparityLearningToolPhp', component: CountryDisparityToolComponent },
    { path: 'countryDisparityLearningToolSummary', component: CountryDisparityToolSummaryComponent },
    { path: 'countryDisparityPages', component: CountryDisparityToolPagesComponent },
    { path: 'passport', component: PassportComponent },
    { path: 'lifeDiary', component: LifeDairyComponent },
    { path: 'passport', component: PassportComponent },
    { path: 'action-page', component: ActionPageComponent },
    { path: 'life-book', component: LifeBookComponent },
    { path: 'life-tool', component: LifeLearningToolComponent },
    { path: 'life-diary-list', component: LifeDiaryListComponent }







]
@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})

export class AppRoutingModule {

}