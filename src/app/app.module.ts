import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { modalWindowShowHide } from './modal-window-show-hide.service';
import { HomeComponent } from './home/home.component';
import { SelfCapsuleComponent } from './home/self-capsule/self-capsule.component';
import { GameSummeryComponent } from './game-summery/game-summery.component';
import { CreateLifeComponent } from './game-summery/create-life/create-life.component';
import { CreateLifeWithCharacterDesignComponent } from './game-summery/create-life-with-character-design/create-life-with-character-design.component';
import { LoadIncompleteLifeComponent } from './game-summery/load-incomplete-life/load-incomplete-life.component';
import { CreateLifeWithSdgComponent } from './game-summery/create-life-with-sdg/create-life-with-sdg.component';
import { LoadCompletedLifeComponent } from './game-summery/load-completed-life/load-completed-life.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AgmCoreModule } from '@agm/core';
import { GameSettingComponent } from './game-setting/game-setting.component';
import { TruncatePipe } from 'src/app/shared/truncate';
import { HomeService } from './home/home.service';
import { UtilityBarComponent } from './home/utility-bar/utility-bar.component';
import { FamilyCapsuleComponent } from './home/family-capsule/family-capsule.component';
import { FamilyCapsuleWindowComponent } from './home/family-capsule-window/family-capsule-window.component';
import { LoadGameAnimationComponent } from './game-summery/load-game-animation/load-game-animation.component';
import { CountryCapsuleComponent } from './home/country-capsule/country-capsule.component';
import { LifeSummeryComponent } from './home/life-summery/life-summery.component';
import { SdgIndicatorsComponent } from './home/sdg-indicators/sdg-indicators.component';
import { ActionEducationComponent } from './home/action-education/action-education.component';
import { ActionResidenceComponent } from './home/action-residence/action-residence.component';
import { ActionCarrerComponent } from './home/action-carrer/action-carrer.component';
import { ActionFinanceComponent } from './home/action-finance/action-finance.component';
import { ActionLeisureComponent } from './home/action-leisure/action-leisure.component';
import { ActionRelationshipComponent } from './home/action-relationship/action-relationship.component';
import { JobpipePipe } from './home/action-carrer/jobpipe.pipe';
import { BusinessPipe } from './home/action-carrer/business.pipe';
import { ObituaryComponent } from './home/obituary/obituary.component';
import { WriteLetterComponent } from './home/write-letter/write-letter.component';
import { FeedbackComponent } from './home/feedback/feedback.component';
import { BugReportComponent } from './home/bug-report/bug-report.component';
import { AgeAYearComponent } from './home/age-a-year/age-a-year.component';
import { SummaryComponent } from './home/summary/summary.component';
import { EventComponent } from './home/event/event.component';
import { WellBeingIndicatorComponent } from './home/well-being-indicator/well-being-indicator.component';
import { WellBeingIndicatorWindowComponent } from './home/well-being-indicator-window/well-being-indicator-window.component';
import { AgaAYearService } from './home/age-a-year/aga-a-year.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonService } from './shared/common.service';
import { TranslateModule } from '@ngx-translate/core';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslationComponent } from './translation/translation.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslationService } from './translation/translation.service';
import { ObituaryPdfComponent } from './home/obituary-pdf/obituary-pdf.component';
import { HeaderIntercepterService } from './shared/header-intercepter.service';
import { ErrorHandlingInterceptorService } from './shared/retry-interceptor.service';
import { DisplayReversePipe } from './shared/reversePipe';
import { ChartsModule } from 'ng2-charts';
import { OrganBloodDonationComponent } from './home/organ-blood-donation/organ-blood-donation.component';
import { OrganBloodDonationService } from './home/organ-blood-donation/organ-blood-donation.service';
import { DecisionWindowComponent } from './home/decision-window/decision-window.component';
import { AlertComponent } from './shared/alert/alert.component';
import { SanitizeHtmlPipe } from './home/write-letter/sanitize-html.pipe';
import { CKEditorModule } from 'ckeditor4-angular';
import { ConstantService } from './shared/constant.service';
import { GameSettingService } from './game-setting/game-setting.service';
import { GameSummeryService } from './game-summery/game-summery.service';
import { CarrerService } from './home/action-carrer/carrer.service';
import { EducationService } from './home/action-education/education.service';
import { LeisureService } from './home/action-leisure/leisure.service';
import { RelationService } from './home/action-relationship/relation.service';
import { ResidenceService } from './home/action-residence/residence.service';
import { CommonActionService } from './home/common-action.service';
import { CountryCapsuleService } from './home/country-capsule/country-capsule.service';
import { DecisionService } from './home/decision-window/decision.service';
import { EventService } from './home/event/event.service';
import { IdentityDifferenceFindService } from './home/identity-difference-find.service';
import { PostGameService } from './home/post-game.service';
import { SdgIndicatorsService } from './home/sdg-indicators/sdg-indicators.service';
import { UtilityService } from './home/utility-bar/utility.service';
import { WellBeingIndicatorService } from './home/well-being-indicator/well-being-indicator.service';
import { RootComponent } from './root/root.component';
import { MapPocComponent } from './game-summery/map-poc/map-poc.component';
import { MapPocTwoComponent } from './game-summery/map-poc-two/map-poc-two.component';
import { PdfGeneraterComponent } from './pdf-generater/pdf-generater.component';
import { CreateLifeWithGroupIdComponent } from './game-summery/create-life-with-group-id/create-life-with-group-id.component';
import { AssignmentComponent } from './game-summery/assignment/assignment.component';
import { ArraySortPipe } from './game-summery/create-life-with-group-id/soertPipe';
import { ByPassSecurityPipe } from './game-summery/sanitize-pipe';
import { LifeExpectancyGraphComponent } from './home/life-expectancy-graph/life-expectancy-graph.component';
import { OrganDonationCertificateComponent } from './home/organ-donation-certificate/organ-donation-certificate.component';
import { CreateLifeWithAssignmentComponent } from './game-summery/create-life-with-assignment/create-life-with-assignment.component';
import { FinaceStatusComponent } from './home/finace-status/finace-status.component';
import { NumberFormatePipe } from './shared/number-formate.pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularD3CloudModule } from 'angular-d3-cloud';
import { BornSummaryComponent } from './home/born-summary/born-summary.component';
import { ReadMoreModule } from 'ng-readmore';
import { SiblingFamilyTreeComponent } from './home/sibling-family-tree/sibling-family-tree.component';
import { ChildrenFamilyTreeComponent } from './home/children-family-tree/children-family-tree.component';
import { GrandChildrenFamilyTreeComponent } from './home/grand-children-family-tree/grand-children-family-tree.component';
import { BornScreenSeetingsComponent } from './born-screen-seetings/born-screen-seetings.component';
import { CountryKnowMoreComponent } from './game-summery/country-know-more/country-know-more.component';
import { RegisterCountryComponent } from './game-summery/register-country/register-country.component';
import { SdgWorldViewComponent } from './game-summery/sdg-world-view/sdg-world-view.component';
import { CountryLearningDocComponent } from './country-learning-doc/country-learning-doc.component';
import { DataLearningToolComponent } from './data-learning-tool/data-learning-tool.component';
import { ToolService } from './data-learning-tool/tool.service';
import { LearningToolPagesComponent } from './data-learning-tool/learning-tool-pages/learning-tool-pages.component';
import { QuestionnaireComponent } from './data-learning-tool/questionnaire/questionnaire.component';
import { SanitizePipe } from './data-learning-tool/sanitize.pipe';
import { MapComponent } from './data-learning-tool/map/map.component';
import { DataLearningToolPhpComponent } from './data-learning-tool-php/data-learning-tool-php.component';
import { ToolSummaryComponent } from './data-learning-tool-php/tool-summary/tool-summary.component';
import { CountryListMapComponent } from './data-learning-tool-php/country-list-map/country-list-map.component';
import { CountryListComponent } from './data-learning-tool-php/country-list/country-list.component';
import { CountryKnowMoreToolComponent } from './data-learning-tool/country-know-more-tool/country-know-more-tool.component';
import { LoadDeletedLifeComponent } from './game-summery/load-deleted-life/load-deleted-life.component';
import { SdgDataLearningToolComponent } from './sdg-data-learning-tool/sdg-data-learning-tool.component';
import { SdgSummaryPageComponent } from './sdg-data-learning-tool/sdg-summary-page/sdg-summary-page.component';
import { SdgPagesComponent } from './sdg-data-learning-tool/sdg-pages/sdg-pages.component';
import { SdgCountryPagesComponent } from './sdg-data-learning-tool/sdg-country-pages/sdg-country-pages.component';
import { SdgGoalMapComponent } from './sdg-data-learning-tool/sdg-goal-map/sdg-goal-map.component';
import { SdgSubgoalMapComponent } from './sdg-data-learning-tool/sdg-subgoal-map/sdg-subgoal-map.component';
import { SdgQuizeMapComponent } from './sdg-data-learning-tool/sdg-quize-map/sdg-quize-map.component';
import { SdgStatementsComponent } from './sdg-data-learning-tool/sdg-statements/sdg-statements.component';
import { WaitScreenComponent } from './wait-screen/wait-screen.component';
import { LearningDocService } from './country-learning-doc/learning-doc.service';
import { LifeSoFarSummaryComponent } from './life-so-far-summary/life-so-far-summary.component';
import { CountryGroupsLearningToolComponent } from './country-groups-learning-tool/country-groups-learning-tool.component';
import { CountryGroupSummaryComponent } from './country-groups-learning-tool/country-group-summary/country-group-summary.component';
import { CountryGroupPagesComponent } from './country-groups-learning-tool/country-group-pages/country-group-pages.component';
import { CountryDisparityToolComponent } from './country-disparity-tool/country-disparity-tool.component';
import { CountryDisparityToolSummaryComponent } from './country-disparity-tool/country-disparity-tool-summary/country-disparity-tool-summary.component';
import { CountryDisparityToolPagesComponent } from './country-disparity-tool/country-disparity-tool-pages/country-disparity-tool-pages.component';
import { PassportComponent } from './passport/passport.component';
import { CountrySelectionComponent } from './sdg-data-learning-tool/country-selection/country-selection.component';
import { LifeDairyComponent } from './game-summery/life-dairy/life-dairy.component';
import { ActionPageComponent } from './game-summery/action-page/action-page.component';
import { LifeBookComponent } from './game-summery/life-book/life-book.component';
import { LifeLearningToolComponent } from './game-summery/life-learning-tool/life-learning-tool.component';
import { LifeDiaryListComponent } from './game-summery/life-diary-list/life-diary-list.component';
import { BusinessTabComponent } from './home/business-tab/business-tab.component';
import { BloodDonationComponent } from './home/blood-donation/blood-donation.component';
import { PetComponent } from './home/pet/pet.component';
import { CharityComponent } from './home/charity/charity.component';
import { NgxPaginationModule } from 'ngx-pagination';
// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SelfCapsuleComponent,
    GameSummeryComponent,
    CreateLifeComponent,
    CreateLifeWithCharacterDesignComponent,
    LoadIncompleteLifeComponent,
    CreateLifeWithSdgComponent,
    LoadCompletedLifeComponent,
    GameSettingComponent,
    TruncatePipe,
    UtilityBarComponent,
    FamilyCapsuleComponent,
    FamilyCapsuleWindowComponent,
    LoadGameAnimationComponent,
    CountryCapsuleComponent,
    LifeSummeryComponent,
    SdgIndicatorsComponent,
    ActionEducationComponent,
    ActionResidenceComponent,
    ActionCarrerComponent,
    ActionFinanceComponent,
    ActionLeisureComponent,
    ActionRelationshipComponent,
    JobpipePipe,
    BusinessPipe,
    ObituaryComponent,
    WriteLetterComponent,
    FeedbackComponent,
    BugReportComponent,
    AgeAYearComponent,
    SummaryComponent,
    EventComponent,
    WellBeingIndicatorComponent,
    WellBeingIndicatorWindowComponent,
    TranslationComponent,
    ObituaryPdfComponent,
    DisplayReversePipe,
    OrganBloodDonationComponent,
    DecisionWindowComponent,
    AlertComponent,
    SanitizeHtmlPipe,
    RootComponent,
    MapPocComponent,
    MapPocTwoComponent,
    PdfGeneraterComponent,
    CreateLifeWithGroupIdComponent,
    AssignmentComponent,
    ArraySortPipe,
    ByPassSecurityPipe,
    LifeExpectancyGraphComponent,
    OrganDonationCertificateComponent,
    CreateLifeWithAssignmentComponent,
    FinaceStatusComponent,
    NumberFormatePipe,
    BornSummaryComponent,
    SiblingFamilyTreeComponent,
    ChildrenFamilyTreeComponent,
    GrandChildrenFamilyTreeComponent,
    BornScreenSeetingsComponent,
    CountryKnowMoreComponent,
    RegisterCountryComponent,
    SdgWorldViewComponent,
    CountryLearningDocComponent,
    DataLearningToolComponent,
    LearningToolPagesComponent,
    QuestionnaireComponent,
    SanitizePipe,
    MapComponent,
    DataLearningToolPhpComponent,
    ToolSummaryComponent,
    CountryListMapComponent,
    CountryListComponent,
    CountryKnowMoreToolComponent,
    LoadDeletedLifeComponent,
    SdgDataLearningToolComponent,
    SdgSummaryPageComponent,
    SdgPagesComponent,
    SdgCountryPagesComponent,
    SdgGoalMapComponent,
    SdgSubgoalMapComponent,
    SdgQuizeMapComponent,
    SdgStatementsComponent,
    WaitScreenComponent,
    LifeSoFarSummaryComponent,
    CountryGroupsLearningToolComponent,
    CountryGroupSummaryComponent,
    CountryGroupPagesComponent,
    CountryDisparityToolComponent,
    CountryDisparityToolSummaryComponent,
    CountryDisparityToolPagesComponent,
    PassportComponent,
    CountrySelectionComponent,
    LifeDairyComponent,
    ActionPageComponent,
    LifeBookComponent,
    LifeLearningToolComponent,
    LifeDiaryListComponent,
    BusinessTabComponent,
    BloodDonationComponent,
    PetComponent,
    CharityComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxSpinnerModule,
    NgxChartsModule,
    NgbModule,
    CKEditorModule,
    ChartsModule,
    ReadMoreModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    AngularD3CloudModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBWgKrffZyYKsciFmx0xf9DwKDZMacosxI'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [modalWindowShowHide, HomeService, AgaAYearService, ConstantService,
    CommonService, TranslationService, OrganBloodDonationService, ErrorHandlingInterceptorService, HeaderIntercepterService, GameSettingService,
    GameSummeryService, PostGameService, IdentityDifferenceFindService, CommonActionService, WellBeingIndicatorService, UtilityService,
    SdgIndicatorsService, CarrerService, EducationService, LeisureService, RelationService, ResidenceService, CountryCapsuleService, DecisionService,
    EventService, NumberFormatePipe, ToolService, SanitizePipe, LearningDocService,

    { provide: 'apiKey', useValue: '81c4seedwzwce0' },
    { provide: 'scope', useValue: 'r_basicprofile r_emailaddress' },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlingInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HeaderIntercepterService, multi: true }
  ],

  bootstrap: [AppComponent],

})
export class AppModule { }
