import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryLearningDocComponent } from './country-learning-doc.component';

describe('CountryLearningDocComponent', () => {
  let component: CountryLearningDocComponent;
  let fixture: ComponentFixture<CountryLearningDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryLearningDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryLearningDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
