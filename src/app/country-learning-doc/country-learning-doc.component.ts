import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { FinanceService } from 'src/app/home/action-finance/finance.service';
import { HomeService } from 'src/app/home/home.service';
import { PostGameService } from 'src/app/home/post-game.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { Router } from '@angular/router';
import { LearningDocService } from './learning-doc.service';
import { FinanceStatusService } from '../home/finace-status/finance-status.service';
import { Event } from '../home/event/Event'

@Component({
  selector: 'app-country-learning-doc',
  templateUrl: './country-learning-doc.component.html',
  styleUrls: ['./country-learning-doc.component.css']
})
export class CountryLearningDocComponent implements OnInit {
  //
  allEvents = [];
  eventList;
  singleEvent;
  carrerStmtFlag: boolean = false;
  state = 'info'
  showCommentSave: boolean = false;
  msgText;
  finanaceInfo;
  docTitle;
  classForCurrentStatusOne = "";
  classForCurrentStatusTwo = "";
  classForCurrentStatusThree = "";
  classForCurrentStatusFour = "";
  classForCurrentStatusFive = "";
  bclassForCurrentStatusOne = "";
  bclassForCurrentStatusTwo = "";
  bclassForCurrentStatusThree = "";
  bclassForCurrentStatusFour = "";
  bclassForCurrentStatusFive = "";
  bincomeGraphStringOne;
  bincomeGraphStringThree;
  userName;
  schoolName = '';
  falgForFamilyTreeTable: boolean = false;
  //
  sex;
  familyTree = [];
  expenceFoodPer;
  expenceShelterPer;
  allPerson;
  mainPerson;
  country; registerCountry;
  religions = [];
  data: boolean = false;
  languages;
  bornSummary;
  statusMetadata;
  registerStatusData;
  firstValueReg;
  secondValueReg; thirdValueReg; fourthValueReg;
  bornCountryWelathMetaData: any;
  registerCountryWelathMetaData: any;
  bornCountryWelathMetaData1: any;
  registerCountryWelathMetaData1: any;
  incomeGraphStringOne;
  incomeGraphStringDeadOne
  incomeGraphStringThree;
  bornCountryArray = [];
  registerCountryArray = [];
  perdayEarning;
  classvalue;
  reg_countryInfo_array
  sentenceTextWindow1;
  stringReturnValue;
  stringRetutnText;
  poupulationValue;
  activeClassForDemo = "nav-item nav-link active"
  activeClassForPerCapita = "nav-item nav-link";
  activeClassForLFO = "nav-item nav-link";
  activeClassForIndex = "nav-item nav-link";
  activeClassForSdg = "nav-item nav-link";
  onClickDemoGraphic: boolean = true;
  onClickPerCapita: boolean = false;
  onCLickLabourForce: boolean = false;
  onCLickSdgGroup: boolean = false;
  onClickIndexRating: boolean = false;
  onClickSdgData: boolean = false;
  bithString: string = "";
  deathString: string = "";
  primarySchoolString: string = "";
  happinessString: string = "";
  valurForHappiAndCorr;
  giniIndexString: string = "";
  labourForceString1: string = "";
  labourForceString2: string = "";
  sexRatio: string = "";
  accesEleString = "";
  valueForHdi;
  sexRatioValue;
  sdgScoreBorn;
  sdgScoreRegister;
  populationString;
  welthGraphDataNotGenrateFlag: boolean = false;
  status;
  statusValue;
  firstValue;
  secondValue;
  thirdValue;
  fourthValue;
  registerCountryExchangeRate;
  registerCountryCode; nextEventObject;
  senntenceGeneratedFlag: boolean = false;
  colorScheme;
  colorSchemeBorn = {
    domain: ['#0000FF', '#A10A28']
  }
  colorSchemeReg;

  colorScheme1 = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA'],
  };
  colorScheme3 = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#0000FF']
  };
  formatDataLabel(value) {
    return "$" + value;
  }
  //graph
  showGraphWindow: boolean = false;
  maxLenght: number = 40;
  yTrimValue: boolean = true;
  single: any[];
  viewPop: any[];
  view: any[] = [320, 200];
  view1: any[] = [320, 200];
  view2: any[] = [700, 400];
  view3 = [1000, 200];
  view4 = [600, 200];
  bOne: number = 0;
  bTwo: number = 0;
  bThree: number = 0;
  bFour: number = 0;
  rOne: number = 0;
  rTwo: number = 0;
  rThree: number = 0;
  rFour: number = 0;
  multi: any[];
  multi1: any[];
  multi2: any[];
  multi3: any[];
  bornWealthGraph: any[];
  multi4: any[];
  multi5: any[];
  firstR;
  checkFormat
  sdgDispalyArray = [];
  showDataLabel = true;
  singleSexR: any[];
  singleBirth: any[];
  singleDeath: any[];
  singleInfant: any[];
  singlePrimarySchool: any[];
  singleUnemp: any = [];
  singleEle: any = [];
  singleHealth: any = [];
  singlePpp: any = [];
  singleGini: any = [];
  singleSdg: any = [];
  singleHappiness: any = [];
  singleHdi: any = [];
  singleCorruption: any = [];
  religionMax = 0;
  colorSchemePop = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeSexR = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeBirth = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeDeath = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeInfant = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemePrimarySchool = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeUnemo = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeEle = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeHealth = {
    domain: ['#00FFFF', '#00FFFF']
  }
  colorSchemePpp = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeGini = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeSdg = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeHdi = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeHappiness = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeCorruption = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorSchemeService = {
    domain: ['#00FFFF', '#00FFFF']
  };
  viewSexR = [];
  viewBirth = [];
  viewDeath = [];
  viewInfant = [];
  yAxisLabelPop: string;
  xAxisLabelPop: string;
  yAxisLabelSex: string;
  xAxisLabelSex: string;
  yAxisLabelBirth: string;
  xAxisLabelBirth: string;
  yAxisLabelDeath: string;
  xAxisLabelDeath: string;
  yAxisLabelInfant: string;
  xAxisLabelInfant: string;
  yAxisLabelPrimarySchool: string;
  xAxisLabelPrimarySchool: string;
  yAxisLabelUnemo: string;
  xAxisLabelUnemo: string;
  yAxisLabelEle: string;
  xAxisLabelEle: string;
  yAxisLabelHealth: string;
  xAxisLabelHealth: string;
  yAxisLabelPpp: string;
  xAxisLabelPpp: string;
  yAxisLabelHdi: string;
  xAxisLabelHdi: string;
  yAxisLabelSdg: string;
  xAxisLabelSdg: string;
  yAxisLabelCorruption: string;
  xAxisLabelcorruption: string;
  yAxisLabelHappiness: string;
  xAxisLabelHapiness: string;
  yAxisLabelGini: string;
  xAxisLabelGini: string;
  xAxisLabelService: string;
  obituary = {};
  populationSen;
  sexSen;
  birthSen; deathSen;
  infantSen;
  primarySchoolSen;
  unempSen;
  helathSen;
  eleSen;
  pppSen;
  hdiSen;
  sdgSen;
  hapineesSen;
  corruptionSen;
  giniSen;
  colorScheme2 = {
    domain: ['#00FFFF', '#567caa', '#567caa']
  };
  schemeType: string = 'linear';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  maxXAxisTickLength: number = 10
  gradient: boolean = false;
  showLegend: boolean = false;
  showLegend1: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = this.translate.instant('gameLabel.Country');
  yAxisLabelReg: string;
  showYAxisLabel: boolean = true;
  xAxisLabel: string = this.translate.instant('gameLabel.Population');
  xAxisLabelReg: string;
  wealthGraphSentenceBorn1;
  wealthGraphSentenceBorn2;
  wealthGraphSentenceBorn3;
  chanceOfPoorR;
  bornCountry;
  countryInfo_array;
  registerCountryLangages;
  summaryPart1 = "";
  summaryPart2 = "";
  summaryPart3 = "";
  summaryPart4 = "";
  summaryPart5 = "";
  summaryPart6 = "";
  summaryPart7 = "";
  wealthGraphSentenceReg1;
  wealthGraphSentenceReg2;
  wealthGraphSentenceReg3;
  registerReligion = [];
  sdgData;
  statusStmt;
  constantGraphValueArray = [
    { 'name': '10%', 'value': 10 / 100 },
    { 'name': '20%', 'value': 20 / 100 },
    { 'name': '30%', 'value': 30 / 100 },
    { 'name': '40%', 'value': 40 / 100 },
    { 'name': '50%', 'value': 50 / 100 },
    { 'name': '60%', 'value': 60 / 100 },
    { 'name': '70%', 'value': 70 / 100 },
    { 'name': '80%', 'value': 80 / 100 },
    { 'name': '90%', 'value': 90 / 100 },
    { 'name': '100%', 'value': 100 / 100 },
  ]
  //sdg
  selectedSdgId: any;
  showSdgInfo: boolean = false;
  sdgDetails: any;
  bornSdgSubGoalDeatails: any;
  regSdgSubGoalDeatails: any
  bornSdgInfo: any
  regSdgInfo: any
  registerSdgInfo: any;
  bcode: any = "";
  rcode: any = "";
  registerCountrySdgName: any
  sdgDescription: any
  sdgInfo: any
  sdgName: any
  sdgOrigninalName: any;
  registeredCountrySdgDeatails: any;
  sdgNewData: any;
  sdgReagisterData: any;
  bornGoalColur: any;
  regiGoalColue: any;
  values: any;
  valuesR: any;
  sdgImage: any;
  loanValue;
  saving; publicAmanities;
  bornEcoStatusMetaData;
  bornWealthMetaData = [];
  docCommentData = [];
  fullName = ''
  constructor(public gameSummeryService: GameSummeryService,
    public homeService: HomeService,
    public constantService: ConstantService,
    public translationService: TranslationService,
    public commonService: CommonService,
    public financeService: FinanceService,
    public postGameService: PostGameService,
    public translate: TranslateService,
    public modalWindowService: modalWindowShowHide,
    public router: Router,
    public financeStatusService: FinanceStatusService,
    public learningDocService: LearningDocService
  ) { }
  @ViewChild('content', { static: false }) content: ElementRef;
  @ViewChild('pdfTable') pdfTable: ElementRef;
  @ViewChild('pdf') pdf: ElementRef;


  ngOnInit(): void {
    this.docCommentData['finanaceStatusCmt'] = [];
    this.docCommentData['economicalSttusCmt'] = [];
    this.docCommentData['countryHealthCmt'] = [];
    this.docCommentData['countrySocialCmt'] = [];
    this.docCommentData['countryecoCmt'] = [];
    this.docCommentData['demographicsCmt'] = []
    this.docCommentData['perCapitaCmt'] = [];
    this.docCommentData['indexCmt'] = [];
    this.docCommentData['occupationCmt'] = [];
    this.homeService.getPersonFromId(this.gameSummeryService.gameId).subscribe(
      res => {
        this.allPerson = res;
        this.schoolName = this.allPerson.school_name;
        this.userName = this.allPerson.user_fullname
        this.translationService.setSelectedlanguage(this.allPerson.selected_language);
        this.translationService.changeLang.emit(this.allPerson.selected_language);
        this.mainPerson = this.allPerson[this.constantService.FAMILY_SELF]
        this.fullName = this.mainPerson.full_name;
        this.country = this.mainPerson.country;
        this.registerCountry = this.mainPerson.register_country
      })
  }

  getCountryInfo() {
    this.data = true
    this.modalWindowService.showWaitScreen = true;
    this.translate.get('gameLabel.doc_stm', { status: this.commonService.getSatusString(this.mainPerson.born_economical_status) }).subscribe(
      (s: String) => {
        this.statusStmt = s;
      })
    if (this.mainPerson.sex === "M") {
      this.sex = this.translate.instant('gameLabel.boy')
    }
    else {
      this.sex = this.translate.instant('gameLabel.girl')
    }
    this.translate.get('gameLabel.doc_title', { bcountry: this.mainPerson.register_country.country, rcountry: this.mainPerson.country.country, sex: this.sex }).subscribe(
      (s: String) => {
        this.docTitle = s;
      })
    if (this.mainPerson.country.countryid === 74) {
      this.gameSummeryService.getAllReligion(this.mainPerson.city.Countryregionid).subscribe(data => {
        this.religions = data;
      });
    }
    else {
      this.gameSummeryService.getReligionForWorld(this.mainPerson.country.countryid, this.translationService.selectedLang).subscribe(data => {
        this.religions = data;
      });
    }
    this.gameSummeryService.getReligionForWorld(this.mainPerson.register_country.countryid, this.translationService.selectedLang.code).subscribe(data => {
      this.registerReligion = data;
      let country_info = this.mainPerson.country.Country_Intro
      this.countryInfo_array = (country_info.split('.'))
      let reg_country_info = this.mainPerson.register_country.Country_Intro
      this.reg_countryInfo_array = (reg_country_info.split('.'))
      //get language
      this.gameSummeryService.getCountryAllLanguages(this.country.countryid).subscribe(
        res => {
          this.languages = res;
          this.postGameService.getObituary(this.mainPerson.game_id).subscribe(
            res => {
              if (res === false) {
                this.homeService.getAllEventList(this.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                  res => {
                    this.eventList = res;
                    for (let i = 0; i < this.eventList.length; i++) {
                      this.singleEvent = new Event();
                      // this.singleEvent = new Event();
                      this.singleEvent.gameEventId = this.eventList[i].game_event_id;
                      this.singleEvent.eventId = this.eventList[i].lang_code;
                      this.singleEvent.text = this.eventList[i].text;
                      this.singleEvent.age = this.eventList[i].game_age;
                      this.singleEvent.oldTraits = this.eventList[i].oldtraits;
                      this.singleEvent.newTraits = this.eventList[i].newtraits;
                      this.singleEvent.factroid = this.eventList[i].factroid;
                      this.singleEvent.link = this.eventList[i].factroidLink;
                      this.singleEvent.addline = this.eventList[i].addline;
                      this.singleEvent.expression = this.eventList[i].expression
                      this.singleEvent.type = this.eventList[i].type;
                      this.allEvents.push(this.singleEvent);
                      this.homeService.allEvent = this.allEvents;
                    }
                    let saveObj = this.homeService.cretaeObituary(this.allPerson)
                    this.postGameService.updateObituaryRetutnByMe(this.allPerson[this.constantService.FAMILY_SELF].game_id, saveObj, false).subscribe(
                      res => {
                        this.obituary = res;
                        //check carre statement or not
                        let count = this.obituary['sub7'].search(".")
                        if (count === 1) {
                          this.carrerStmtFlag = true
                        }
                        this.learningDocService.getCommentData(this.gameSummeryService.gameId).subscribe(
                          res => {
                            this.docCommentData = res;
                            this.gameSummeryService.getCountryAllLanguages(this.mainPerson.register_country.countryid).subscribe(
                              res => {
                                this.registerCountryLangages = res;
                                this.gameSummeryService.getBornSummary(this.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                                  res => {
                                    this.bornSummary = res;
                                    this.summaryPart1 = this.bornSummary['sub1']
                                    this.summaryPart2 = this.bornSummary['sub2']
                                    this.summaryPart3 = this.bornSummary['sub3']
                                    this.summaryPart4 = this.bornSummary['sub4']
                                    this.summaryPart5 = this.bornSummary['sub5']
                                    this.summaryPart6 = this.bornSummary['sub6']
                                    this.summaryPart7 = this.bornSummary['sub7']
                                    this.gameSummeryService.getSdgGoalCountryColor(this.mainPerson.country.countryid, this.mainPerson.register_country.countryid).subscribe(
                                      res => {
                                        this.sdgData = res;
                                        this.gameSummeryService.getWelathMetaData(this.mainPerson.country.countryid).subscribe(
                                          res => {
                                            this.bornCountryWelathMetaData = res;
                                            this.gameSummeryService.getWelathMetaData(this.registerCountry.countryid).subscribe(
                                              res => {
                                                this.registerCountryWelathMetaData = res;
                                                this.homeService.geteconomicalStatusMetadata(this.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
                                                  res => {
                                                    this.statusMetadata = res[0];
                                                    this.homeService.geteconomicalStatusMetadata(this.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
                                                      res => {
                                                        this.registerStatusData = res[0]
                                                        this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid, true, this.translationService.selectedLang.code).subscribe((sdgInfo: any) => {
                                                          this.sdgDetails = sdgInfo['goals'];
                                                          this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                                                          this.gameSummeryService.getSDGInfo(this.mainPerson.register_country.countryid, false, this.translationService.selectedLang.code).subscribe((res: any) => {
                                                            this.regSdgSubGoalDeatails = sdgInfo['targets'];
                                                            this.gameSummeryService.ecoStatusData = this.statusMetadata;
                                                            this.getSdgMap();
                                                            this.getSdgMaptwo();
                                                            this.setSdgArray(this.docCommentData['Sdg']);
                                                            this.generateArrayForWelathGraph();
                                                            this.setSocialEconomicalStatus();
                                                            this.getDataForWelthGraph();
                                                            this.calculateTableValues();
                                                            this.displayFamilyIncomeGraph();
                                                            this.getDataForWelthGraphCompariosion();
                                                            this.populationValues(this.mainPerson.country.Population, this.mainPerson.register_country.Population, 3);
                                                            this.sexRationValues(this.mainPerson.country.SexRatio, this.mainPerson.register_country.SexRatio, 0);
                                                            this.BirthRationValues(this.mainPerson.country.BirthRate, this.mainPerson.register_country.BirthRate, 1);
                                                            this.DeathRateValues(this.mainPerson.country.DeathRate, this.mainPerson.register_country.DeathRate, 0);
                                                            this.infantMoratalityRate(this.mainPerson.country.InfantMortalityRate, this.mainPerson.register_country.InfantMortalityRate, 0)
                                                            this.primarySchoolValues(this.mainPerson.country.PrimarySchool, this.mainPerson.register_country.PrimarySchool, 1)
                                                            this.umEmpValues(this.mainPerson.country.UnEmploymentRate, this.mainPerson.register_country.UnEmploymentRate, 0)
                                                            this.eleValues(this.mainPerson.country.PerCapitaElectricityConsumption, this.mainPerson.register_country.PerCapitaElectricityConsumption, 1)
                                                            this.healthValues(this.mainPerson.country.PerCapitaHealthConsumption, this.mainPerson.register_country.PerCapitaHealthConsumption, 1)
                                                            this.pppValues(this.mainPerson.country.ppp, this.mainPerson.register_country.ppp, 1);
                                                            this.giniValues(this.mainPerson.country.gini, this.mainPerson.register_country.gini, 0);
                                                            this.sdgValues(this.mainPerson.country.SdgiRank, this.mainPerson.register_country.SdgiRank, 0);
                                                            this.happinessValues(this.mainPerson.country.HappinessRank, this.mainPerson.register_country.HappinessRank, 0);
                                                            this.giniValues(this.mainPerson.country.HDI, this.mainPerson.register_country.HDI, 1);
                                                            this.corruptionValues(this.mainPerson.country.corruption, this.mainPerson.register_country.corruption, 1);
                                                            this.tabClickLFO();
                                                            this.setAmenitiesData();
                                                            this.setLeisure();
                                                            this.createFamilyTreeTable();
                                                            this.learningDocService.finanaceStatus(this.allPerson);
                                                            this.learningDocService.finanaceStatusBorn(this.allPerson);
                                                            this.modalWindowService.showWaitScreen = false;
                                                          })
                                                        })
                                                      })
                                                  })
                                              });
                                          })
                                      })
                                  });
                              })
                          })
                      })
                  })
              }
              else {
                let obObj = res
                this.obituary = obObj['systemGeneratedObituary'].obituary;
                //check carre statement or not
                let count = this.obituary['sub7'].search(".")
                if (count === 1) {
                  this.carrerStmtFlag = true
                }
                this.learningDocService.getCommentData(this.gameSummeryService.gameId).subscribe(
                  res => {
                    this.docCommentData = res;
                    this.gameSummeryService.getCountryAllLanguages(this.mainPerson.register_country.countryid).subscribe(
                      res => {
                        this.registerCountryLangages = res;
                        this.gameSummeryService.getBornSummary(this.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                          res => {
                            this.bornSummary = res;
                            this.summaryPart1 = this.bornSummary['sub1']
                            this.summaryPart2 = this.bornSummary['sub2']
                            this.summaryPart3 = this.bornSummary['sub3']
                            this.summaryPart4 = this.bornSummary['sub4']
                            this.summaryPart5 = this.bornSummary['sub5']
                            this.summaryPart6 = this.bornSummary['sub6']
                            this.summaryPart7 = this.bornSummary['sub7']

                            this.gameSummeryService.getSdgGoalCountryColor(this.mainPerson.country.countryid, this.mainPerson.register_country.countryid).subscribe(
                              res => {
                                this.sdgData = res;
                                this.gameSummeryService.getWelathMetaData(this.mainPerson.country.countryid).subscribe(
                                  res => {
                                    this.bornCountryWelathMetaData = res;
                                    this.gameSummeryService.getWelathMetaData(this.registerCountry.countryid).subscribe(
                                      res => {
                                        this.registerCountryWelathMetaData = res;
                                        this.homeService.geteconomicalStatusMetadata(this.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
                                          res => {
                                            this.statusMetadata = res[0];
                                            this.homeService.geteconomicalStatusMetadata(this.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
                                              res => {
                                                this.registerStatusData = res[0]
                                                this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid, true, this.translationService.selectedLang.code).subscribe((sdgInfo: any) => {
                                                  this.sdgDetails = sdgInfo['goals'];
                                                  this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                                                  this.gameSummeryService.getSDGInfo(this.mainPerson.register_country.countryid, false, this.translationService.selectedLang.code).subscribe((res: any) => {
                                                    this.regSdgSubGoalDeatails = sdgInfo['targets'];
                                                    this.gameSummeryService.ecoStatusData = this.statusMetadata;
                                                    this.getSdgMap();
                                                    this.getSdgMaptwo();
                                                    this.setSdgArray(this.docCommentData['Sdg']);
                                                    this.generateArrayForWelathGraph();
                                                    this.setSocialEconomicalStatus();
                                                    this.getDataForWelthGraph();
                                                    this.calculateTableValues();
                                                    this.displayFamilyIncomeGraph();
                                                    this.getDataForWelthGraphCompariosion();
                                                    this.populationValues(this.mainPerson.country.Population, this.mainPerson.register_country.Population, 3);
                                                    this.sexRationValues(this.mainPerson.country.SexRatio, this.mainPerson.register_country.SexRatio, 0);
                                                    this.BirthRationValues(this.mainPerson.country.BirthRate, this.mainPerson.register_country.BirthRate, 1);
                                                    this.DeathRateValues(this.mainPerson.country.DeathRate, this.mainPerson.register_country.DeathRate, 0);
                                                    this.infantMoratalityRate(this.mainPerson.country.InfantMortalityRate, this.mainPerson.register_country.InfantMortalityRate, 0)
                                                    this.primarySchoolValues(this.mainPerson.country.PrimarySchool, this.mainPerson.register_country.PrimarySchool, 1)
                                                    this.umEmpValues(this.mainPerson.country.UnEmploymentRate, this.mainPerson.register_country.UnEmploymentRate, 0)
                                                    this.eleValues(this.mainPerson.country.PerCapitaElectricityConsumption, this.mainPerson.register_country.PerCapitaElectricityConsumption, 1)
                                                    this.healthValues(this.mainPerson.country.PerCapitaHealthConsumption, this.mainPerson.register_country.PerCapitaHealthConsumption, 1)
                                                    this.pppValues(this.mainPerson.country.ppp, this.mainPerson.register_country.ppp, 1);
                                                    this.giniValues(this.mainPerson.country.gini, this.mainPerson.register_country.gini, 0);
                                                    this.sdgValues(this.mainPerson.country.SdgiRank, this.mainPerson.register_country.SdgiRank, 0);
                                                    this.happinessValues(this.mainPerson.country.HappinessRank, this.mainPerson.register_country.HappinessRank, 0);
                                                    this.giniValues(this.mainPerson.country.HDI, this.mainPerson.register_country.HDI, 1);
                                                    this.corruptionValues(this.mainPerson.country.corruption, this.mainPerson.register_country.corruption, 1);
                                                    this.tabClickLFO();
                                                    this.setAmenitiesData();
                                                    this.setLeisure();
                                                    this.createFamilyTreeTable();
                                                    this.learningDocService.finanaceStatus(this.allPerson);
                                                    this.learningDocService.finanaceStatusBorn(this.allPerson);
                                                    this.modalWindowService.showWaitScreen = false;
                                                  })
                                                })
                                              })
                                          })
                                      });
                                  })
                              })
                          });
                      })
                  })
              }
              //  check born country change or not
              //get bornSummary

            })
        })
    })
    // })
  }

  setSocialEconomicalStatus() {
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    this.firstValueReg = (f3R);
    this.secondValueReg = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    this.thirdValueReg = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    this.fourthValueReg = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    //end
    this.commonService.calculateWealthStatus(this.bornCountryWelathMetaData, this.allPerson);
    this.commonService.calculateIncameStatus(this.statusMetadata, this.allPerson);
  }
  generateArrayForWelathGraph() {
    this.bornCountryArray = [
      { 'name': '10%', 'value': this.bornCountryWelathMetaData.per10 },
      { 'name': '20%', 'value': this.bornCountryWelathMetaData.per20 },
      { 'name': '30%', 'value': this.bornCountryWelathMetaData.per30 },
      { 'name': '40%', 'value': this.bornCountryWelathMetaData.per40 },
      { 'name': '50%', 'value': this.bornCountryWelathMetaData.per40 },
      { 'name': '60%', 'value': this.bornCountryWelathMetaData.per60 },
      { 'name': '70%', 'value': this.bornCountryWelathMetaData.per70 },
      { 'name': '80%', 'value': this.bornCountryWelathMetaData.per80 },
      { 'name': '90%', 'value': this.bornCountryWelathMetaData.per90 },
      { 'name': '99%', 'value': this.bornCountryWelathMetaData.per99 },
      { 'name': '100%', 'value': this.bornCountryWelathMetaData.per100 },
    ]
    this.registerCountryArray = [
      { 'name': '10%', 'value': this.registerCountryWelathMetaData.per10 },
      { 'name': '20%', 'value': this.registerCountryWelathMetaData.per20 },
      { 'name': '30%', 'value': this.registerCountryWelathMetaData.per30 },
      { 'name': '40%', 'value': this.registerCountryWelathMetaData.per40 },
      { 'name': '50%', 'value': this.registerCountryWelathMetaData.per40 },
      { 'name': '60%', 'value': this.registerCountryWelathMetaData.per60 },
      { 'name': '70%', 'value': this.registerCountryWelathMetaData.per70 },
      { 'name': '80%', 'value': this.registerCountryWelathMetaData.per80 },
      { 'name': '90%', 'value': this.registerCountryWelathMetaData.per90 },
      { 'name': '99%', 'value': this.registerCountryWelathMetaData.per99 },
      { 'name': '100%', 'value': this.registerCountryWelathMetaData.per100 },
    ]

  }
  getDataForWelthGraph() {
    this.view = [600, 300];
    this.multi2 = [];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabel = this.translate.instant('gameLabel.Population(%)');
    this.showYAxisLabel = true;
    this.yAxisLabel = this.translate.instant('gameLabel.Wealth');
    this.multi2.push({
      name: this.translate.instant('gameLabel.Equality_Line'),
      series: []
    });
    this.multi2.push({
      name: this.mainPerson.country.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi2[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.bornCountryArray.length; i++) {
      if (this.bornCountryArray[i].name !== "99%") {
        this.multi2[1].series.push(this.bornCountryArray[i]);
      }
    }
    Object.assign(this.multi2);
    let v2, v3, v4, g3: any, g4, g5: any;
    if (this.mainPerson.country.Population > 100000) {
      g3 = (this.bornCountryWelathMetaData.per50 * 100).toFixed(1);
      g4 = ((this.bornCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
      g5 = ((this.bornCountryWelathMetaData.per100 * 100) - (this.bornCountryWelathMetaData.per99 * 100)).toFixed(1);
      v2 = (((this.mainPerson.country.Population * (50 / 100)))).toFixed(2);
      v3 = ((this.mainPerson.country.Population * (1 / 100))).toFixed(2)
      v4 = (g5 / g3).toFixed(2);
    }
    else {
      g3 = (this.bornCountryWelathMetaData.per50 * 100).toFixed(2);
      g4 = ((this.bornCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
      g5 = ((this.bornCountryWelathMetaData.per100 * 100) - (this.bornCountryWelathMetaData.per99 * 100)).toFixed(2);
      v2 = (((this.mainPerson.country.Population * (50 / 100)))).toFixed(2);
      v3 = ((this.mainPerson.country.Population * (1 / 100))).toFixed(2)
      v4 = (g5 / g3).toFixed(2);
    }
    v2 = this.commonService.checkNumberFormat(v2);
    v3 = this.commonService.checkNumberFormat(v3);
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn3 = s;
      })
  }
  getDataForWelthGraphCompariosion() {
    this.multi3 = [];
    this.view1 = [600, 300];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabelReg = this.translate.instant('gameLabel.Population(%)');
    this.showYAxisLabel = true;
    this.yAxisLabelReg = this.translate.instant('gameLabel.Wealth');
    this.multi3.push({
      name: this.translate.instant('gameLabel.Equality_Line'),
      series: []
    });
    this.multi3.push({
      name: this.mainPerson.register_country.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi3[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.registerCountryArray.length; i++) {
      if (this.registerCountryArray[i].name !== "99%") {
        this.multi3[1].series.push(this.registerCountryArray[i]);
      }
    }
    Object.assign(this.multi3);
    let v2, v3, v4, g3: any, g4, g5: any;
    g3 = (this.registerCountryWelathMetaData.per50 * 100).toFixed(1);
    g4 = ((this.registerCountryWelathMetaData.per99 * 100) - g3).toFixed(2);
    g5 = ((this.registerCountryWelathMetaData.per100 * 100) - (this.registerCountryWelathMetaData.per99 * 100)).toFixed(1);
    v2 = (((this.mainPerson.register_country.Population * (50 / 100)))).toFixed(2);
    v3 = ((this.mainPerson.register_country.Population * (1 / 100))).toFixed(2)
    v4 = (g5 / g3).toFixed(2);
    v2 = this.commonService.checkNumberFormat(v2)
    v3 = this.commonService.checkNumberFormat(v3)
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg3 = s;
      })
  }
  displayFamilyIncomeGraph() {
    let s = "";
    let selfIncome = parseInt("" + (this.mainPerson.expense.householdIncome * 12) / this.mainPerson.country.ExchangeRate);
    let registerIncome = (this.mainPerson.register_country.ProdperCapita * this.mainPerson.register_country.AverageFamilyCount);
    let countryIncome = (this.mainPerson.country.ProdperCapita) * this.mainPerson.country.AverageFamilyCount;
    let value2 = Math.round(this.gameSummeryService.ecoStatusData.chanceOfPoor * 100);
    let value = Math.round((selfIncome / this.allPerson[this.constantService.FAMILY_SELF].count_of_household_members));
    let value3 = (value / 365);
    this.perdayEarning = value3.toFixed(0);
    if (value2 > 0) {
      if (value3 > 2) {
        s = this.translate.instant('gameLabel.You_are_not_one_of_them.');
      }
      else {
        s = this.translate.instant('gameLabel.You_are_one_of_them.');
      }
    }
    if (this.mainPerson.current_economical_status === 0) {
      this.classForCurrentStatusOne = "table-bordered-red text-color"
    }
    else if (this.mainPerson.current_economical_status === 1) {
      this.classForCurrentStatusTwo = "table-bordered-yellow text-color"

    }
    else if (this.mainPerson.current_economical_status === 2) {
      this.classForCurrentStatusThree = "table-bordered-orange text-color"


    } else if (this.mainPerson.current_economical_status === 3) {
      this.classForCurrentStatusFour = "table-bordered-blue text-color"


    } else if (this.mainPerson.current_economical_status === 4) {
      this.classForCurrentStatusFive = "table-bordered-green text-color"
    }

    if (this.mainPerson.born_economical_status === 0) {
      this.bclassForCurrentStatusOne = "table-bordered-red text-color"
    }
    else if (this.mainPerson.born_economical_status === 1) {
      this.bclassForCurrentStatusTwo = "table-bordered-yellow text-color"

    }
    else if (this.mainPerson.born_economical_status === 2) {
      this.bclassForCurrentStatusThree = "table-bordered-orange text-color"


    } else if (this.mainPerson.born_economical_status === 3) {
      this.bclassForCurrentStatusFour = "table-bordered-blue text-color"


    } else if (this.mainPerson.born_economical_status === 4) {
      this.bclassForCurrentStatusFive = "table-bordered-green text-color"


    }
    let subString;
    subString = this.translate.instant('gameLabel.a');
    this.translate.get('gameLabel.incomeGraphSen1', { subString: subString, ecostatus: this.commonService.getSatusString(this.allPerson[this.constantService.FAMILY_SELF].current_economical_status), value3: value3.toFixed(2) }).subscribe(
      (str: String) => {
        this.incomeGraphStringOne = str;
        this.translate.get('gameLabel.incomeGraphSen2', { country: this.allPerson[this.constantService.FAMILY_SELF].country.country, value2: value2.toFixed(0), s: s }).subscribe(
          (str1: String) => {
            this.incomeGraphStringThree = str1;
            this.translate.get('gameLabel.incomegraphDead', { subString: subString, status: this.commonService.getSatusString(this.allPerson[this.constantService.FAMILY_SELF].current_economical_status) }).subscribe(
              (str2: String) => {
                this.incomeGraphStringDeadOne = str2;
              })
          })
      })

    //    this.incomeGraphStringOne="You have been born into " +subString+  this.allPerson[this.constantService.FAMILY_SELF].current_economical_status +" family, " +"earning $"+value3.toFixed(2)+"  per day."
    //     this.incomeGraphStringThree=" The odds of being born as absolutely poor (earning less than $2 per day) in "+this.allPerson[this.constantService.FAMILY_SELF].country.country + " are " + value2.toFixed(0)+"% ."+s;


    this.multi1 = [
      {
        name: this.translate.instant('gameLabel.Your_family'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(selfIncome))
          }
        ]
      },

      {
        name: this.allPerson[this.constantService.FAMILY_SELF].country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(countryIncome))
          }
        ]
      },
      {
        name: this.allPerson[this.constantService.FAMILY_SELF].register_country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(registerIncome))
          }
        ]
      },
      {
        name: this.translate.instant('gameLabel.World'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(12000))
          }
        ]
      },
    ];
  }
  yAxisTickFormatting(value) {
    return this.percentTickFormatting(value);
  }

  percentTickFormatting(val: any): string {
    return val.toLocaleString() + '%';
  }
  calculateTableValues() {

    if (this.allPerson[this.constantService.FAMILY_SELF].country.country.length > 8) {
      this.bornCountry = this.allPerson[this.constantService.FAMILY_SELF].country.Code;
    }
    else {
      this.bornCountry = this.allPerson[this.constantService.FAMILY_SELF].country.country;
    }
    if (this.allPerson[this.constantService.FAMILY_SELF].register_country.country.length > 8) {
      this.registerCountry = this.allPerson[this.constantService.FAMILY_SELF].register_country.Code;
    }
    else {
      this.registerCountry = this.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    }
    //value for register
    this.chanceOfPoorR = this.registerStatusData.chanceOfPoor * 100;
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    let firstR = (f3R);
    this.firstR = firstR;
    let secondR = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    let thirdR = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    let fourthR = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    this.rOne = (firstR + secondR) / (firstR);
    this.rTwo = (secondR + thirdR) / (firstR);
    this.rThree = (thirdR + fourthR) / (firstR);
    this.rFour = (fourthR) / (firstR);

    //value for born
    let first = this.commonService.firstValueI;
    let second = this.commonService.secondValueI;
    let third = this.commonService.thirdValueI;
    let fourth = this.commonService.fourthValueI;
    this.bOne = (first + second) / (first);
    this.bTwo = (second + third) / (first);
    this.bThree = (third + fourth) / (first);
    this.bFour = (fourth) / (first);

  }

  setSdgArray(comment) {
    let born=null,register=null;
    if( this.sdgData[0]!=null){
      born = this.sdgData[0]
    }
    if( this.sdgData[1]!=null){
     register = this.sdgData[1]
    }
     if(born!==null && register!==null){
    this.sdgDispalyArray = [{
      sdgId: 1,
      sdgName: this.translate.instant('gameLabel.sdg1'),
      bcolour: this.getSdgColor(born.one),
      rcolour: this.getSdgColor(register.one),
      comment: comment.Sdg1,
      image: "assets/images/sdgicon/sdg1_" + this.translationService.selectedLang.code + ".png"
    }, {
      sdgId: 2,
      sdgName: this.translate.instant('gameLabel.sdg2'),
      bcolour: this.getSdgColor(born.two),
      rcolour: this.getSdgColor(register.two),
      comment: comment.Sdg2,
      image: "assets/images/sdgicon/sdg2_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 3,
      sdgName: this.translate.instant('gameLabel.sdg3'),
      bcolour: this.getSdgColor(born.three),
      rcolour: this.getSdgColor(register.three),
      comment: comment.Sdg3,
      image: "assets/images/sdgicon/sdg3_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 4,
      sdgName: this.translate.instant('gameLabel.sdg4'),
      bcolour: this.getSdgColor(born.four),
      rcolour: this.getSdgColor(register.four),
      comment: comment.Sdg4,
      image: "assets/images/sdgicon/sdg4_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 5,
      sdgName: this.translate.instant('gameLabel.sdg5'),
      bcolour: this.getSdgColor(born.five),
      rcolour: this.getSdgColor(register.five),
      comment: comment.Sdg5,
      image: "assets/images/sdgicon/sdg5_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 6,
      sdgName: this.translate.instant('gameLabel.sdg6'),
      bcolour: this.getSdgColor(born.six),
      rcolour: this.getSdgColor(register.six),
      comment: comment.Sdg6,
      image: "assets/images/sdgicon/sdg6_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 7,
      sdgName: this.translate.instant('gameLabel.sdg7'),
      bcolour: this.getSdgColor(born.seven),
      rcolour: this.getSdgColor(register.seven),
      comment: comment.Sdg7,
      image: "assets/images/sdgicon/sdg7_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 8,
      sdgName: this.translate.instant('gameLabel.sdg8'),
      bcolour: this.getSdgColor(born.eight),
      rcolour: this.getSdgColor(register.eight),
      comment: comment.Sdg8,
      image: "assets/images/sdgicon/sdg8_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 9,
      sdgName: this.translate.instant('gameLabel.sdg9'),
      bcolour: this.getSdgColor(born.nine),
      rcolour: this.getSdgColor(register.nine),
      comment: comment.Sdg9,
      image: "assets/images/sdgicon/sdg9_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 10,
      sdgName: this.translate.instant('gameLabel.sdg10'),
      bcolour: this.getSdgColor(born.ten),
      rcolour: this.getSdgColor(register.ten),
      comment: comment.Sdg10,
      image: "assets/images/sdgicon/sdg10_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 11,
      sdgName: this.translate.instant('gameLabel.sdg11'),
      bcolour: this.getSdgColor(born.eleven),
      rcolour: this.getSdgColor(register.eleven),
      comment: comment.Sdg11,
      image: "assets/images/sdgicon/sdg11_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 12,
      sdgName: this.translate.instant('gameLabel.sdg12'),
      bcolour: this.getSdgColor(born.twelve),
      rcolour: this.getSdgColor(register.twelve),
      comment: comment.Sdg12,
      image: "assets/images/sdgicon/sdg12_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 13,
      sdgName: this.translate.instant('gameLabel.sdg13'),
      bcolour: this.getSdgColor(born.thirteen),
      rcolour: this.getSdgColor(register.thirteen),
      comment: comment.Sdg13,
      image: "assets/images/sdgicon/sdg13_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 14,
      sdgName: this.translate.instant('gameLabel.sdg14'),
      bcolour: this.getSdgColor(born.fourteen),
      rcolour: this.getSdgColor(register.fourteen),
      comment: comment.Sdg14,
      image: " assets/images/sdgicon/sdg14_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 15,
      sdgName: this.translate.instant('gameLabel.sdg15'),
      bcolour: this.getSdgColor(born.fifteen),
      rcolour: this.getSdgColor(register.fifteen),
      comment: comment.Sdg15,
      image: " assets/images/sdgicon/sdg15_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 16,
      sdgName: this.translate.instant('gameLabel.sdg16'),
      bcolour: this.getSdgColor(born.sixteen),
      rcolour: this.getSdgColor(register.sixteen),
      comment: comment.Sdg16,
      image: " assets/images/sdgicon/sdg16_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 17,
      sdgName: this.translate.instant('gameLabel.sdg17'),
      bcolour: this.getSdgColor(born.seventeen),
      rcolour: this.getSdgColor(register.seventeen),
      comment: comment.Sdg17,
      image: "assets/images/sdgicon/sdg17_" + this.translationService.selectedLang.code + ".png"
    }
    ]
  }
  else if(born===null && register!==null){
    this.sdgDispalyArray = [{
      sdgId: 1,
      sdgName: this.translate.instant('gameLabel.sdg1'),
      bcolour: "",
      rcolour: this.getSdgColor(register.one),
      comment: comment.Sdg1,
      image: "assets/images/sdgicon/sdg1_" + this.translationService.selectedLang.code + ".png"
    }, {
      sdgId: 2,
      sdgName: this.translate.instant('gameLabel.sdg2'),
      bcolour:"",
      rcolour: this.getSdgColor(register.two),
      comment: comment.Sdg2,
      image: "assets/images/sdgicon/sdg2_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 3,
      sdgName: this.translate.instant('gameLabel.sdg3'),
      bcolour:"",
      rcolour: this.getSdgColor(register.three),
      comment: comment.Sdg3,
      image: "assets/images/sdgicon/sdg3_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 4,
      sdgName: this.translate.instant('gameLabel.sdg4'),
      bcolour:"",
      rcolour: this.getSdgColor(register.four),
      comment: comment.Sdg4,
      image: "assets/images/sdgicon/sdg4_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 5,
      sdgName: this.translate.instant('gameLabel.sdg5'),
      bcolour: "",
      rcolour: this.getSdgColor(register.five),
      comment: comment.Sdg5,
      image: "assets/images/sdgicon/sdg5_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 6,
      sdgName: this.translate.instant('gameLabel.sdg6'),
      bcolour: "",
      rcolour: this.getSdgColor(register.six),
      comment: comment.Sdg6,
      image: "assets/images/sdgicon/sdg6_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 7,
      sdgName: this.translate.instant('gameLabel.sdg7'),
      bcolour: "",
      rcolour: this.getSdgColor(register.seven),
      comment: comment.Sdg7,
      image: "assets/images/sdgicon/sdg7_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 8,
      sdgName: this.translate.instant('gameLabel.sdg8'),
      bcolour: "",
      rcolour: this.getSdgColor(register.eight),
      comment: comment.Sdg8,
      image: "assets/images/sdgicon/sdg8_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 9,
      sdgName: this.translate.instant('gameLabel.sdg9'),
      bcolour:"",
      rcolour: this.getSdgColor(register.nine),
      comment: comment.Sdg9,
      image: "assets/images/sdgicon/sdg9_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 10,
      sdgName: this.translate.instant('gameLabel.sdg10'),
      bcolour:"",
      rcolour: this.getSdgColor(register.ten),
      comment: comment.Sdg10,
      image: "assets/images/sdgicon/sdg10_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 11,
      sdgName: this.translate.instant('gameLabel.sdg11'),
      bcolour:"",
      rcolour: this.getSdgColor(register.eleven),
      comment: comment.Sdg11,
      image: "assets/images/sdgicon/sdg11_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 12,
      sdgName: this.translate.instant('gameLabel.sdg12'),
      bcolour: "",
      rcolour: this.getSdgColor(register.twelve),
      comment: comment.Sdg12,
      image: "assets/images/sdgicon/sdg12_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 13,
      sdgName: this.translate.instant('gameLabel.sdg13'),
      bcolour: "",
      rcolour: this.getSdgColor(register.thirteen),
      comment: comment.Sdg13,
      image: "assets/images/sdgicon/sdg13_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 14,
      sdgName: this.translate.instant('gameLabel.sdg14'),
      bcolour:"",
      rcolour: this.getSdgColor(register.fourteen),
      comment: comment.Sdg14,
      image: " assets/images/sdgicon/sdg14_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 15,
      sdgName: this.translate.instant('gameLabel.sdg15'),
      bcolour: "",
      rcolour: this.getSdgColor(register.fifteen),
      comment: comment.Sdg15,
      image: " assets/images/sdgicon/sdg15_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 16,
      sdgName: this.translate.instant('gameLabel.sdg16'),
      bcolour: "",
      rcolour: this.getSdgColor(register.sixteen),
      comment: comment.Sdg16,
      image: " assets/images/sdgicon/sdg16_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 17,
      sdgName: this.translate.instant('gameLabel.sdg17'),
      bcolour:"",
      rcolour: this.getSdgColor(register.seventeen),
      comment: comment.Sdg17,
      image: "assets/images/sdgicon/sdg17_" + this.translationService.selectedLang.code + ".png"
    }
    ]
  }

  else if(born!==null && register===null){
    this.sdgDispalyArray = [{
      sdgId: 1,
      sdgName: this.translate.instant('gameLabel.sdg1'),
      bcolour: this.getSdgColor(born.one),
      rcolour: "",
      comment: comment.Sdg1,
      image: "assets/images/sdgicon/sdg1_" + this.translationService.selectedLang.code + ".png"
    }, {
      sdgId: 2,
      sdgName: this.translate.instant('gameLabel.sdg2'),
      bcolour: this.getSdgColor(born.two),
      rcolour: "",
      comment: comment.Sdg2,
      image: "assets/images/sdgicon/sdg2_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 3,
      sdgName: this.translate.instant('gameLabel.sdg3'),
      bcolour: this.getSdgColor(born.three),
      rcolour:  "",
      comment: comment.Sdg3,
      image: "assets/images/sdgicon/sdg3_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 4,
      sdgName: this.translate.instant('gameLabel.sdg4'),
      bcolour: this.getSdgColor(born.four),
      rcolour:  "",
      comment: comment.Sdg4,
      image: "assets/images/sdgicon/sdg4_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 5,
      sdgName: this.translate.instant('gameLabel.sdg5'),
      bcolour: this.getSdgColor(born.five),
      rcolour:  "",
      comment: comment.Sdg5,
      image: "assets/images/sdgicon/sdg5_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 6,
      sdgName: this.translate.instant('gameLabel.sdg6'),
      bcolour: this.getSdgColor(born.six),
      rcolour:  "",
      comment: comment.Sdg6,
      image: "assets/images/sdgicon/sdg6_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 7,
      sdgName: this.translate.instant('gameLabel.sdg7'),
      bcolour: this.getSdgColor(born.seven),
      rcolour:  "",
      comment: comment.Sdg7,
      image: "assets/images/sdgicon/sdg7_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 8,
      sdgName: this.translate.instant('gameLabel.sdg8'),
      bcolour: this.getSdgColor(born.eight),
      rcolour: "",
      comment: comment.Sdg8,
      image: "assets/images/sdgicon/sdg8_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 9,
      sdgName: this.translate.instant('gameLabel.sdg9'),
      bcolour: this.getSdgColor(born.nine),
      rcolour:  "",
      comment: comment.Sdg9,
      image: "assets/images/sdgicon/sdg9_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 10,
      sdgName: this.translate.instant('gameLabel.sdg10'),
      bcolour: this.getSdgColor(born.ten),
      rcolour: "",
      comment: comment.Sdg10,
      image: "assets/images/sdgicon/sdg10_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 11,
      sdgName: this.translate.instant('gameLabel.sdg11'),
      bcolour: this.getSdgColor(born.eleven),
      rcolour:  "",
      comment: comment.Sdg11,
      image: "assets/images/sdgicon/sdg11_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 12,
      sdgName: this.translate.instant('gameLabel.sdg12'),
      bcolour: this.getSdgColor(born.twelve),
      rcolour:  "",
      comment: comment.Sdg12,
      image: "assets/images/sdgicon/sdg12_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 13,
      sdgName: this.translate.instant('gameLabel.sdg13'),
      bcolour: this.getSdgColor(born.thirteen),
      rcolour:  "",
      comment: comment.Sdg13,
      image: "assets/images/sdgicon/sdg13_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 14,
      sdgName: this.translate.instant('gameLabel.sdg14'),
      bcolour: this.getSdgColor(born.fourteen),
      rcolour: "",
      comment: comment.Sdg14,
      image: " assets/images/sdgicon/sdg14_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 15,
      sdgName: this.translate.instant('gameLabel.sdg15'),
      bcolour: this.getSdgColor(born.fifteen),
      rcolour: "",
      comment: comment.Sdg15,
      image: " assets/images/sdgicon/sdg15_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 16,
      sdgName: this.translate.instant('gameLabel.sdg16'),
      bcolour: this.getSdgColor(born.sixteen),
      rcolour:"",
      comment: comment.Sdg16,
      image: " assets/images/sdgicon/sdg16_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 17,
      sdgName: this.translate.instant('gameLabel.sdg17'),
      bcolour: this.getSdgColor(born.seventeen),
      rcolour: "",
      comment: comment.Sdg17,
      image: "assets/images/sdgicon/sdg17_" + this.translationService.selectedLang.code + ".png"
    }
    ]
  }
  else{
    this.sdgDispalyArray = [{
      sdgId: 1,
      sdgName: this.translate.instant('gameLabel.sdg1'),
      bcolour: "",
      rcolour: "",
      comment: comment.Sdg1,
      image: "assets/images/sdgicon/sdg1_" + this.translationService.selectedLang.code + ".png"
    }, {
      sdgId: 2,
      sdgName: this.translate.instant('gameLabel.sdg2'),
      bcolour:"",
      rcolour: "",
      comment: comment.Sdg2,
      image: "assets/images/sdgicon/sdg2_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 3,
      sdgName: this.translate.instant('gameLabel.sdg3'),
      bcolour:"",
      rcolour: "",
      comment: comment.Sdg3,
      image: "assets/images/sdgicon/sdg3_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 4,
      sdgName: this.translate.instant('gameLabel.sdg4'),
      bcolour:"",
      rcolour:"",
      comment: comment.Sdg4,
      image: "assets/images/sdgicon/sdg4_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 5,
      sdgName: this.translate.instant('gameLabel.sdg5'),
      bcolour: "",
      rcolour: "",
      comment: comment.Sdg5,
      image: "assets/images/sdgicon/sdg5_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 6,
      sdgName: this.translate.instant('gameLabel.sdg6'),
      bcolour: "",
      rcolour: "",
      comment: comment.Sdg6,
      image: "assets/images/sdgicon/sdg6_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 7,
      sdgName: this.translate.instant('gameLabel.sdg7'),
      bcolour: "",
      rcolour: "",
      comment: comment.Sdg7,
      image: "assets/images/sdgicon/sdg7_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 8,
      sdgName: this.translate.instant('gameLabel.sdg8'),
      bcolour: "",
      rcolour:"",
      comment: comment.Sdg8,
      image: "assets/images/sdgicon/sdg8_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 9,
      sdgName: this.translate.instant('gameLabel.sdg9'),
      bcolour:"",
      rcolour: "",
      comment: comment.Sdg9,
      image: "assets/images/sdgicon/sdg9_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 10,
      sdgName: this.translate.instant('gameLabel.sdg10'),
      bcolour:"",
      rcolour: "",
      comment: comment.Sdg10,
      image: "assets/images/sdgicon/sdg10_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 11,
      sdgName: this.translate.instant('gameLabel.sdg11'),
      bcolour:"",
      rcolour: "",
      comment: comment.Sdg11,
      image: "assets/images/sdgicon/sdg11_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 12,
      sdgName: this.translate.instant('gameLabel.sdg12'),
      bcolour: "",
      rcolour:"",
      comment: comment.Sdg12,
      image: "assets/images/sdgicon/sdg12_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 13,
      sdgName: this.translate.instant('gameLabel.sdg13'),
      bcolour: "",
      rcolour: "",
      comment: comment.Sdg13,
      image: "assets/images/sdgicon/sdg13_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 14,
      sdgName: this.translate.instant('gameLabel.sdg14'),
      bcolour:"",
      rcolour: "",
      comment: comment.Sdg14,
      image: " assets/images/sdgicon/sdg14_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 15,
      sdgName: this.translate.instant('gameLabel.sdg15'),
      bcolour: "",
      rcolour: "",
      comment: comment.Sdg15,
      image: " assets/images/sdgicon/sdg15_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 16,
      sdgName: this.translate.instant('gameLabel.sdg16'),
      bcolour: "",
      rcolour: "",
      comment: comment.Sdg16,
      image: " assets/images/sdgicon/sdg16_" + this.translationService.selectedLang.code + ".png"


    }, {
      sdgId: 17,
      sdgName: this.translate.instant('gameLabel.sdg17'),
      bcolour:"",
      rcolour: "",
      comment: comment.Sdg17,
      image: "assets/images/sdgicon/sdg17_" + this.translationService.selectedLang.code + ".png"
    }
    ]
  }
}

  getSdgMap() {
    let map = am4core.create("sdgchartdiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.events.on("hit", function (ev) {
      // zoom to an object
      ev.target.series.chart.zoomToMapObject(ev.target);
    });
    let county1 = [];
    county1 = [{
      "id": this.mainPerson.country.Code,
      "name": this.mainPerson.country.country,
      "value": 100,
      "fill": am4core.color("#36AD1F")
    }]

    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }
  getSdgMaptwo() {
    let map = am4core.create("chartdiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1.2;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.events.on("hit", function (ev) {
      // zoom to an object
      ev.target.series.chart.zoomToMapObject(ev.target);
    });
    let county1 = [];
    county1 = [
      {
        "id": this.mainPerson.register_country.Code,
        "name": this.mainPerson.register_country.country,
        "value": 100,
        "fill": am4core.color("#36AD1F")
      }]

    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }
  checkPopulationFormat(Population) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = (Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }
  checkCss(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }
  checkCssForSpecific(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }

  checkCss1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind text-color";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind text-color";
    }
    else if (value1 === value2) {
      this.classvalue = "text-color";
    }
    return this.classvalue;
  }


  checkCssForSpecific1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind text-color";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind text-color";
    }
    else if (value1 === value2) {
      this.classvalue = "text-color";
    }
    return this.classvalue;
  }

  onSelect(data): void {
  }

  onActivate(data): void {
    // console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    // console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  populationValues(x, y, classValue) {
    this.viewPop = [400, 200];
    this.xAxisLabelPop = this.translate.instant('gameLabel.Population');
    this.generateStringValue(x, y);
    this.generateStringSubString(x, y);
    if (this.poupulationValue == 0) {
      this.populationString = this.translate.instant('gameLabel.almost_equal');

      this.translate.get('gameLabel.populationCD1', { country: this.mainPerson.country.country, populationString: this.populationString, rcountry: this.mainPerson.register_country.country }).subscribe(
        (str) => {
          this.populationSen = str;
        })
    }
    else {
      this.translate.get('gameLabel.populationCD2', { country: this.mainPerson.country.country, poupulationValue: this.poupulationValue, populationString: this.populationString, rcountry: this.mainPerson.register_country.country }).subscribe(
        (str) => {
          this.populationSen = str;
        })
    }

    this.checkColourPatternForGraph(x, y, classValue);
    this.single = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.Population
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.Population
      },
    ];
    Object.assign(this.single);
  }

  sexRationValues(x, y, classValue) {
    this.viewSexR = [400, 200];
    this.xAxisLabelSex = this.translate.instant('gameLabel.Sex_Ratio');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      this.getSexRationValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(this.sexRatioValue) === 0) {
        this.sexRatio = this.translate.instant('gameLabel.as_good_as')
        this.translate.get('gameLabel.sexRationDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sexSen = str;
          })
      }
      else {
        this.translate.get('gameLabel.sexRationDC2', { country: this.mainPerson.country.country, sexRatioValue: Math.round(this.sexRatioValue), sexRatio: this.sexRatio, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sexSen = str;
          })
      }

    }
    else {
      this.sentenceTextWindow1 = 'N/A'
    }
    this.colorSchemeSexR = this.checkColourPatternForGraph(x, y, classValue);
    this.singleSexR = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.SexRatio,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.SexRatio,
      },
    ];
    Object.assign(this.singleSexR);
  }

  BirthRationValues(x, y, classValue) {
    this.xAxisLabelBirth = this.translate.instant('gameLabel.Birth_Rate');
    this.checkSentenceGenaerateOrNot(x, y);
    let value = this.generateStringValue(x, y);
    this.generateStringSubString(x, y);
    if (Math.round(value) === 0) {
      this.translate.get('gameLabel.BirthRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
        (str) => {
          this.birthSen = str;
        })
    }
    else {
      this.translate.get('gameLabel.BirthRateDC2', { country: this.mainPerson.country.country, value: value, bithString: this.bithString, rcountry: this.mainPerson.register_country.country }).subscribe(
        (str) => {
          this.birthSen = str;
        })
    }
    this.colorSchemeBirth = this.checkColourPatternForGraph(x, y, classValue);
    this.singleBirth = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.BirthRate,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.BirthRate,
      },
    ];
    Object.assign(this.singleBirth);

  }

  DeathRateValues(x, y, classValue) {
    this.xAxisLabelDeath = this.translate.instant('gameLabel.Death_Rate');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.DeathRatioDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.deathSen = str;
          })
      }
      else {
        this.translate.get('gameLabel.DeathRatioDC2', { country: this.mainPerson.country.country, value: value, deathString: this.deathString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.deathSen = str;
          })
      }
    }
    else {
      this.sentenceTextWindow1 = 'N/A'
    }
    this.colorSchemeDeath = this.checkColourPatternForGraph(x, y, classValue);
    this.singleDeath = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.DeathRate,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.DeathRate,
      },
    ];
    Object.assign(this.singleDeath);

  }

  infantMoratalityRate(x, y, classValue) {
    this.xAxisLabelInfant = this.translate.instant('gameLabel.Infant_Mortality_Rate');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.InfantMortalityRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.infantSen = str;
          })
      }
      else {
        this.translate.get('gameLabel.InfantMortalityRateDC2', { country: this.mainPerson.country.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.infantSen = str;
          })
      }
    }
    else {
      this.infantSen = 'N/A'
    }
    this.colorSchemeInfant = this.checkColourPatternForGraph(x, y, classValue);
    this.singleInfant = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.InfantMortalityRate,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.InfantMortalityRate,
      },
    ];
    Object.assign(this.singleInfant);
  }

  primarySchoolValues(x, y, classValue) {
    this.xAxisLabelPrimarySchool = this.translate.instant('gameLabel.Primary_School_Enrollment');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.primarySchoolDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.primarySchoolSen = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have equal access to Primary School, compared to " +this.mainPerson.register_country.country +".";
      } else {
        this.translate.get('gameLabel.primarySchoolDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.primarySchoolSen = str;
          })
        // If {{country}} is your home country, you will have {{value}} {{primarySchoolString}} access to Primary School than {{rcountry}}.
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will have "+ value +this.primarySchoolString+ " access to Primary School than " +this.mainPerson.register_country.country +".";
      }
    }
    else {
      this.primarySchoolSen = 'N/A'
    }
    this.colorSchemePrimarySchool = this.checkColourPatternForGraph(x, y, classValue);
    this.singlePrimarySchool = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.PrimarySchool,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.PrimarySchool,
      },
    ];
    Object.assign(this.singlePrimarySchool);
  }

  umEmpValues(x, y, classValue) {
    this.xAxisLabelUnemo = this.translate.instant('gameLabel.Unemployment_Rate');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.UnEmploymentRateDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.unempSen = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are equally likely to be unemployed, compared to " +this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.UnEmploymentRateDC2', { country: this.mainPerson.country.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.unempSen = str;
          })
        // "If {{country}} is your home country, you will spend {{value}} {{primarySchoolString}} money on health care than {{rcountry}}.
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you are "+ value +this.stringRetutnText+ " to be unemployed than " +this.mainPerson.register_country.country +".";
      }
    }
    else {
      this.unempSen = 'N/A'
    }
    this.colorSchemeUnemo = this.checkColourPatternForGraph(x, y, classValue);
    this.singleUnemp = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.UnEmploymentRate,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.UnEmploymentRate,
      },
    ];
    Object.assign(this.singleUnemp);
  }

  eleValues(x, y, classValue) {
    this.xAxisLabelEle = this.translate.instant('gameLabel.Electricity_Access');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.electricityDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.eleSen = str;
          })
        // this.sentenceTextWindow1="In "+this.mainPerson.country.country+" you will access equal electricity, compared to "+ this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.electricityDC2', { country: this.mainPerson.country.country, value: value, accesEleString: this.accesEleString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.eleSen = str;
          })
        // this.sentenceTextWindow1="In "+this.mainPerson.country.country+" you will have  "+ value +this.accesEleString+ " access to electricity than " +this.mainPerson.register_country.country+".";
      }
    }
    else {
      this.eleSen = 'N/A'
    }

    this.colorSchemeEle = this.checkColourPatternForGraph(x, y, classValue);
    this.singleEle = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.PerCapitaElectricityConsumption,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.PerCapitaElectricityConsumption,
      },
    ];
    Object.assign(this.singleEle);
  }

  healthValues(x, y, classValue) {
    this.xAxisLabelHealth = this.translate.instant('gameLabel.Health_Exp_per_capita');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.healthExpDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.helathSen = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will spend equal money on health care, compared to " +this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.healthExpDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.helathSen = str;
          })
        // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will spend "+ value +this.primarySchoolString+ " money on health care than " +this.mainPerson.register_country.country +".";
      }
    }
    else {
      this.sentenceTextWindow1 = 'N/A'
    }
    this.colorSchemeHealth = this.checkColourPatternForGraph(x, y, classValue);
    this.singleHealth = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.PerCapitaHealthConsumption,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.PerCapitaHealthConsumption,
      },
    ];
    Object.assign(this.singleHealth);
  }
  pppValues(x, y, classValues) {
    this.xAxisLabelPpp = this.translate.instant('gameLabel.PPP');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {

        this.translate.get('gameLabel.pppDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.pppSen = str;
          }) // this.sentenceTextWindow1="If "+this.mainPerson.country.country+" is your home country, you will earn equal money, compared to " +this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.pppDC2', { country: this.mainPerson.country.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.pppSen = str;
          })
      }
    }
    else {
      this.pppSen = 'N/A'
    }
    this.colorSchemePpp = this.checkColourPatternForGraph(x, y, classValues);
    this.singlePpp = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.ppp,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.ppp,
      },
    ];
    Object.assign(this.singlePpp);
  }

  giniValues(x, y, classValues) {
    this.xAxisLabelGini = this.translate.instant('gameLabel.GINI');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if ((x - y) === 0 || (y - x) === 0) {
        this.translate.get('gameLabel.giniDc1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.giniSen = str;
          })
      }
      else {
        this.translate.get('gameLabel.giniDc2', { country: this.mainPerson.country.country, giniIndexString: this.giniIndexString }).subscribe(
          (str) => {
            this.giniSen = str;
          })
      }
    }
    else {
      this.giniSen = 'N/A'
    }

    this.colorSchemeGini = this.checkColourPatternForGraph(x, y, classValues);
    this.singleGini = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.gini,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.gini,
      },
    ];
    Object.assign(this.singleGini);
  }

  sdgValues(x, y, classValues) {
    this.xAxisLabelSdg = this.translate.instant('gameLabel.SDG_rank');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(this.valurForHappiAndCorr) === 0) {

        this.translate.get('gameLabel.SdgiScoreDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sdgSen = str;
          }) // this.sentenceTextWindow1=this.mainPerson.country.country+" is equal in the SDG rank, compared to " +this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.SdgiScoreDC2', { country: this.mainPerson.country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.sdgSen = str;
          })
        // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the SDG rank than " +this.mainPerson.register_country.country +".";
      }
    }
    else {
      this.sentenceTextWindow1 = 'N/A'
    }
    this.colorSchemeSdg = this.checkColourPatternForGraph(x, y, classValues);
    this.singleSdg = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.SdgiRank,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.SdgiRank,
      },
    ];
    Object.assign(this.singlePpp);
  }

  happinessValues(x, y, classValues) {

    this.xAxisLabelHapiness = this.translate.instant('gameLabel.Happiness_Rank');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(this.valurForHappiAndCorr) === 0) {
        this.translate.get('gameLabel.HappinessScoreDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.hapineesSen = str;
          })
      } else {
        this.translate.get('gameLabel.HappinessScoreDC2', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
          (str) => {
            this.hapineesSen = str;
          })
      }
    }
    else {
      this.hapineesSen = 'N/A'
    }
    this.colorSchemeHappiness = this.checkColourPatternForGraph(x, y, classValues);
    this.singleHappiness = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.HappinessRank,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.HappinessRank,
      },
    ];
    Object.assign(this.singleHappiness);
  }

  corruptionValues(x, y, classValues) {
    this.xAxisLabelcorruption = this.translate.instant('gameLabel.Corruption');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(this.valurForHappiAndCorr) === 0) {
        this.translate.get('gameLabel.CorruptionDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.corruptionSen = str;
          })
        // this.sentenceTextWindow1=this.mainPerson.country.country+" is equal in the list of the most corrupt countries, compared to "+this.mainPerson.register_country.country;
      }
      else {
        this.translate.get('gameLabel.CorruptionDC2', { rcountry: this.mainPerson.register_country.country, country: this.mainPerson.country.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
          (str) => {
            this.corruptionSen = str;
          })
        // this.sentenceTextWindow1="Compared to "+this.mainPerson.register_country.country +","+this.mainPerson.country.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the list of the most corrupt countries.";
      }
    }
    else {
      this.corruptionSen = 'N/A'
    }

    this.colorSchemeCorruption = this.checkColourPatternForGraph(x, y, classValues);
    this.singleCorruption = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.corruption,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.corruption,
      },
    ];
    Object.assign(this.singleCorruption);
  }
  hdiValues(x, y, classValues) {
    this.xAxisLabelHdi = this.translate.instant('gameLabel.HDI');
    this.checkSentenceGenaerateOrNot(x, y);
    if (!this.senntenceGeneratedFlag) {
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(this.valueForHdi) === 0) {
        this.translate.get('gameLabel.hdiDC1', { country: this.mainPerson.country.country, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.hdiSen = str;
          })
        // this.sentenceTextWindow1=this.mainPerson.country.country+" has  equal HDI, compared to "+ this.mainPerson.register_country.country +".";
      }
      else {
        this.translate.get('gameLabel.hdiDC2', { country: this.mainPerson.country.country, valueForHdi: this.valueForHdi, giniIndexString: this.giniIndexString, rcountry: this.mainPerson.register_country.country }).subscribe(
          (str) => {
            this.hdiSen = str;
          })
        // this.sentenceTextWindow1=this.mainPerson.country.country+" has  "+ this.valueForHdi +"% "+this.giniIndexString+ "  HDI than "+ this.mainPerson.register_country.country +".";
      }
    }
    else {
      this.hdiSen = 'N/A'
    }
    this.colorSchemeHdi = this.checkColourPatternForGraph(x, y, classValues);
    this.singleHdi = [
      {
        name: this.mainPerson.country.country,
        value: this.mainPerson.country.HDI,
      },
      {
        name: this.mainPerson.register_country.country,
        value: this.mainPerson.register_country.HDI,
      },
    ];
    Object.assign(this.singleHdi);
  }


  tabClickLFO() {
    let num1 = this.mainPerson.country.Agriculture;
    let num2 = this.mainPerson.country.Services;
    let num3 = this.mainPerson.country.industry;
    let num4 = this.mainPerson.register_country.Agriculture;
    let num5 = this.mainPerson.register_country.Services;
    let num6 = this.mainPerson.register_country.industry;
    let largest;
    let occupation;
    let rlargest;
    let roccupation;
    if (num1 >= num2 && num1 >= num3) {
      largest = num1.toFixed(2);
      occupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num2 >= num1 && num2 >= num3) {
      largest = num2.toFixed(2);
      occupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      largest = num3.toFixed(2);
      occupation = this.translate.instant('gameLabel.Industry1');
    }

    if (num4 >= num5 && num4 >= num6) {
      largest = num1.toFixed(2);
      rlargest = num4.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num5 >= num4 && num5 >= num6) {
      rlargest = num5.toFixed(2);
      largest = num2.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      rlargest = num6.toFixed(2);
      largest = num3.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Industry1');
    }
    this.translate.get('gameLabel.LEbourForce1', { country: this.mainPerson.country.country, occupation: occupation }).subscribe(
      (str) => {
        this.labourForceString1 = str;
      })
    this.translate.get('gameLabel.LEbourForce2', { roccupation: roccupation, rcountry: this.mainPerson.register_country.country, largest: largest, country: this.mainPerson.country.country }).subscribe(
      (str) => {
        this.labourForceString2 = str;
      })
    // this.labourForceString1="In "+this.mainPerson.country.country+", most likely your occupation will be in the "+occupation+ " Sector. "
    // this.labourForceString2="If you are working in " + roccupation +" Sector in "+this.mainPerson.register_country.country +", you will have "+largest+"% jobs available in " +this.mainPerson.country.country+"."
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link active";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = true;
    this.onClickIndexRating = false;
    this.xAxisLabelService = this.translate.instant('gameLabel.Occupation')
    this.colorSchemeService = {
      domain: ['#5AA454', '#A10A28']
    };
    this.multi = [
      {
        name: this.mainPerson.country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.mainPerson.country.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.mainPerson.country.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.mainPerson.country.industry,
          },
        ],
      },

      {
        name: this.mainPerson.register_country.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.mainPerson.register_country.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.mainPerson.register_country.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.mainPerson.register_country.industry,
          },
        ],
      },
    ];
    Object.assign(this.multi);

  }


  checkColourPatternForGraph(x, y, classIndex) {
    if (classIndex === 0) {
      let val = this.checkCssForSpecific(x, y)
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
    else if (classIndex === 1) {
      let val = this.checkCss(x, y);
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
    return this.colorScheme;
  }

  generateStringValue(Bi, Ri) {
    let B = parseFloat(Bi);
    let R = parseFloat(Ri);
    if (B < R) {
      this.stringReturnValue = ((100 * ((R - B) / R))).toFixed(2);
      this.valurForHappiAndCorr = (R - B).toFixed(2);
      this.valueForHdi = (100 * (R - B) / R).toFixed(2)
      this.poupulationValue = (((B) / R) * 100).toFixed(2)
    }
    else if (B > R) {
      this.stringReturnValue = (B / R).toFixed(2)
      this.valurForHappiAndCorr = (B - R).toFixed(2);
      this.valueForHdi = ((B / R - 1) * 100).toFixed(2)
      this.poupulationValue = (B / R).toFixed(2);
    }
    else {
      this.stringReturnValue = 0
      this.valurForHappiAndCorr = 0
      this.valueForHdi = 0;
      this.poupulationValue = 0;
    }
    return this.stringReturnValue
  }

  generateStringSubString(B, R) {
    B = parseFloat(B);
    R = parseFloat(R);
    if (B < R) {
      this.stringRetutnText = this.translate.instant('gameLabel.%_less_likely');
      this.bithString = this.translate.instant('gameLabel.%_fewer');
      this.deathString = this.translate.instant('gameLabel.%_less_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.%_less');
      this.happinessString = this.translate.instant('gameLabel.ranks_above');
      this.giniIndexString = this.translate.instant('gameLabel.lower');
      this.sexRatio = this.translate.instant('gameLabel.less');
      this.accesEleString = this.translate.instant('gameLabel.%_less');
      this.populationString = "%"
    }
    else if (B > R) {
      this.stringRetutnText = this.translate.instant('gameLabel.times_more_likely');
      this.bithString = this.translate.instant('gameLabel.times_more');
      this.deathString = this.translate.instant('gameLabel.times_more_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.times_more');
      this.happinessString = this.translate.instant('gameLabel.ranks_below');
      this.giniIndexString = this.translate.instant('gameLabel.higher');
      this.sexRatio = this.translate.instant('gameLabel.more');
      this.accesEleString = this.translate.instant('gameLabel.times_more');
      this.populationString = this.translate.instant('gameLabel.times');

    }
    else {
      this.populationString = this.translate.instant('gameLabel.almost_equal');
      this.stringRetutnText = this.translate.instant('gameLabel.as_good_as');
    }
    return this.stringRetutnText;

  }

  checkSentenceGenaerateOrNot(B, R) {
    if (B === 0 || R === 0 || B === "NA" || R === "NA") {
      this.senntenceGeneratedFlag = true;
    }
    else {
      this.senntenceGeneratedFlag = false;
    }
  }

  getSexRationValue(B, R) {
    B = parseFloat(B);
    R = parseFloat(R);
    this.sexRatioValue = Math.abs((R - B) * 1000).toFixed(2);
  }

  close() {
    if (this.homeService.flagFromDeadScreens) {
      this.modalWindowService.showDocumentFlag = false;
    }
    else {
      this.data = false;
      this.modalWindowService.showDocumentFlag = false;
      this.router.navigate(['/life-book'])
    }
  }
  getSdgColor(color) {
    let code;
    if (color === 'red') {
      code = "#d3270d "
    }
    else if (color === 'green') {
      code = " #36AD1F"

    }
    else if (color === 'orange') {
      code = "#ed7c1c "

    }
    else if (color === 'grey') {
      code = "#b9b9b9 "

    }
    else if (color === 'yellow') {
      code = "#ffc107 "

    }
    return code;
  }

  setAmenitiesData() {
    this.learningDocService.createBornAmenities(this.allPerson[this.constantService.FAMILY_SELF].born_data.amenities, this.docCommentData['amenitiCmt']);
    this.learningDocService.createCurrentAmenities(this.allPerson[this.constantService.FAMILY_SELF].amenities);
    this.learningDocService.compaireAmenities();
  }

  setLeisure() {
    this.learningDocService.leisureData = [];
    this.learningDocService.createLeisureTableArray(this.allPerson[this.constantService.FAMILY_SELF].leisure_settings, this.allPerson[this.constantService.FAMILY_SELF], this.docCommentData['leisureCmt']);
  }


  createFamilyTreeTable() {
    // familyTree
    let husband = null, wife = null, mother, father, deadAge, sib, textColor, grandChild, deadStatus = "", deadAgeSelf;
    mother = this.allPerson[this.constantService.FAMILY_MOTHER];
    father = this.allPerson[this.constantService.FAMILY_FATHER];
    if (this.mainPerson.sex === "M") {
      if (this.allPerson.hasOwnProperty(this.constantService.FAMILY_WIFE)) {
        wife = this.allPerson[this.constantService.FAMILY_WIFE]
      }
    }
    else if (this.mainPerson.sex === "F") {
      if (this.allPerson.hasOwnProperty(this.constantService.FAMILY_HUSBAND)) {
        husband = this.allPerson[this.constantService.FAMILY_HUSBAND]
      }
    }
    //mother
    if (mother.dead) {
      this.falgForFamilyTreeTable = true;
      deadAge = mother.age;
      deadAgeSelf = mother.age - this.mainPerson.age
      deadStatus = this.translate.instant('gameLabel.Dead')
      textColor = "textRed"
    }
    else {
      deadAge = "";
      textColor = ""
      deadAgeSelf = ""
      deadStatus = this.translate.instant('gameLabel.Alive')

    }
    this.familyTree.push({
      "name": mother.full_name,
      "relation": this.translate.instant('gameLabel.Mother'),
      "age": mother.age,
      "deadAge": deadAge,
      "deadAgeSelf": Math.abs(deadAgeSelf),
      "deadStatus": deadStatus,
      "color": textColor
    })

    //father
    if (father.dead) {
      deadAge = father.age;
      deadAgeSelf = father.age - this.mainPerson.age
      deadStatus = this.translate.instant('gameLabel.Dead')
      this.falgForFamilyTreeTable = true;
      textColor = "textRed"

    }
    else {
      deadAge = "";
      textColor = ""
      deadAgeSelf = ""
      deadStatus = this.translate.instant('gameLabel.Alive')
    }
    this.familyTree.push({
      "name": father.full_name,
      "relation": this.translate.instant('gameLabel.Father'),
      "age": father.age,
      "deadAge": deadAge,
      "deadAgeSelf":Math.abs( deadAgeSelf),
      "deadStatus": deadStatus,
      "color": textColor
    })


    //husaband
    if (husband !== null) {
      if (husband.dead) {
        deadAge = husband.age
        deadStatus = this.translate.instant('gameLabel.Dead')
        this.falgForFamilyTreeTable = true;
        textColor = "textRed"
        deadAgeSelf= husband.age - this.mainPerson.age

      }
      else {
        deadAgeSelf=""
        deadAge = ""
        textColor = ""
        deadStatus = this.translate.instant('gameLabel.Alive')

      }
      this.familyTree.push({
        "name": husband.full_name,
        "relation": this.translate.instant('gameLabel.Husband'),
        "age": husband.age,
        "deadAge": deadAge,
        "deadAgeSelf":Math.abs( deadAgeSelf),
        "deadStatus": deadStatus,
        "color": textColor

      })
    }

    //wife
    if (wife !== null) {
      if (wife.dead) {
        deadAge = wife.age 
        deadAgeSelf = wife.age - this.mainPerson.age
        deadStatus = this.translate.instant('gameLabel.Dead')
        this.falgForFamilyTreeTable = true;
        textColor = "textRed"

      }
      else {
        deadAge = ""
        deadAgeSelf=""
        deadStatus = this.translate.instant('gameLabel.Alive')
        textColor = ""
      }
      this.familyTree.push({
        "name": wife.full_name,
        "relation": this.translate.instant('gameLabel.Wife'),
        "age": wife.age,
        "deadAge": deadAge,
        "deadAgeSelf": Math.abs(deadAgeSelf),
        "deadStatus": deadStatus,
        "color": textColor


      })
    }

    //sib
    let sex;
     textColor=""
    if (this.mainPerson[this.constantService.FAMILY_SIBLING] != null) {
      let siblings = this.mainPerson[this.constantService.FAMILY_SIBLING];
      for (var i = 0; i < siblings.length; i++) {
        let sibString = this.mainPerson.siblings[i];
        sib = this.allPerson[sibString];
        if (sib.dead) {
          deadAge = sib.age
          deadAgeSelf = sib.age - this.mainPerson.age
          deadStatus = this.translate.instant('gameLabel.Dead')
          this.falgForFamilyTreeTable = true;
          textColor = "textRed"

        }
        else {
          deadAge = ""
          deadAgeSelf = ""
          deadStatus = this.translate.instant('gameLabel.Alive')
          textColor = ""
        }
        if (sib.sex === "M") {
          sex = this.translate.instant('gameLabel.Brother')
        }
        else {
          sex = this.translate.instant('gameLabel.Sister')
        }
        this.familyTree.push({
          "name": sib.full_name,
          "relation": sex,
          "age": sib.age,
          "deadAge": deadAge,
          "deadAgeSelf":  Math.abs(deadAgeSelf),
          "deadStatus": deadStatus,
          "color": textColor


        })
      }
    }
    //child
    if (this.mainPerson[this.constantService.FAMILY_CHILDREN] != null) {
      let child = this.mainPerson[this.constantService.FAMILY_CHILDREN];
      for (var i = 0; i < child.length; i++) {
        let childString = this.mainPerson.children[i];
        child = this.allPerson[childString];
        if (child.dead) {
          deadAge = child.age
          deadAgeSelf = child.age - this.mainPerson.age
          deadStatus = this.translate.instant('gameLabel.Dead')
          this.falgForFamilyTreeTable = true;
          textColor = "textRed"
        }
        else {
          deadAge = "";
          deadAgeSelf = ""
          deadStatus = this.translate.instant('gameLabel.Alive')
          textColor = ""
        }
        if (child.sex === "M") {
          sex = this.translate.instant('gameLabel.Son')
        }
        else {
          sex = this.translate.instant('gameLabel.Daughter')
        }
        this.familyTree.push({
          "name": child.full_name,
          "relation": sex,
          "age": child.age,
          "deadAge": deadAge,
          "deadAgeSelf": Math.abs(deadAgeSelf),
          "deadStatus": deadStatus,
          "color": textColor

        })
      }
    }

    //grandchild
    if (this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN] != null) {
      let gchild = this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN];
      for (var i = 0; i < gchild.length; i++) {
        let gchildString = this.mainPerson.grand_children[i];
        gchild = this.allPerson[gchildString];
        if (gchild.dead) {
          deadAge = gchild.age
          deadAgeSelf = gchild.age - this.mainPerson.age
          deadStatus = this.translate.instant('gameLabel.Dead')
          this.falgForFamilyTreeTable = true;
          textColor = "textRed"
        }
        else {
          deadAge = ""
          deadAgeSelf = ""
          deadStatus = this.translate.instant('gameLabel.Alive')
          textColor = ""
        }
        if (gchild.sex === "M") {
          sex = this.translate.instant('gameLabel.Grand_Son')
        }
        else {
          sex = this.translate.instant('gameLabel.Grand_Daughter')
        }
        this.familyTree.push({
          "name": gchild.full_name,
          "relation": sex,
          "age": gchild.age,
          "deadAge": deadAge,
          "deadAgeSelf":Math.abs( deadAgeSelf),
          "deadStatus": deadStatus,
          "color": textColor


        })
      }
    }
  }
  fiananceInfoClick(info) {
    if (info === 'totalFamMemberTol') {
      this.finanaceInfo = this.translate.instant('gameLabel.totalFamMemberTol');
    }
    else if (info === 'Total_earning_memberstlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Total_earning_memberstlp');
    }
    else if (info === 'Current_diet_typetlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Current_diet_typetlp');
    }
    else if (info === 'Expenses_on_foodtlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Expenses_on_foodtlp');
    }
    else if (info === 'Current_shelter_type') {
      this.finanaceInfo = this.translate.instant('gameLabel.Current_shelter_type');
    }
    else if (info === 'Expenses_on_shelter') {
      this.finanaceInfo = this.translate.instant('gameLabel.Expenses_on_shelter');
    }
    else if (info === 'Current_saving_ratetlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Current_saving_ratetlp');
    }
    else if (info === 'Networth_to_income_ratiotlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Networth_to_income_ratiotlp');
    }
    else if (info === 'Family_income_per_monthtlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Family_income_per_monthtlp');
    }
    else if (info === 'Family_expenes_per_monthtlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Family_expenes_per_monthtlp');
    } else if (info === 'Family_assetstlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Family_assetstlp');
    }
    else if (info === 'Cash_in_handtlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Cash_in_handtlp');
    }
    else if (info === 'Family_net_worthtlp') {
      this.finanaceInfo = this.translate.instant('gameLabel.Family_net_worthtlp');
    }
  }
  checkCssForSpecificText(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "textRed";
    }
    else if (value1 < value2) {
      this.classvalue = "textGreen";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }

  checkCssText(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "text-green";
    }
    else if (value1 < value2) {
      this.classvalue = "text-red";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }


  updateCommentData() {
    this.docCommentData['amenitiCmt'] = this.learningDocService.updateCommentDataForAmenities(this.learningDocService.dispalyUtilityBornArr, this.docCommentData['amenitiCmt']);
    this.docCommentData['leisureCmt'] = this.learningDocService.updateLeisureCommentData(this.learningDocService.leisureData, this.docCommentData['leisureCmt']);
    this.docCommentData['Sdg'] = this.learningDocService.updateSdgComment(this.sdgDispalyArray, this.docCommentData['Sdg'])
    this.learningDocService.updateCommentData(this.gameSummeryService.gameId, this.docCommentData).subscribe(
      res => {
        this.docCommentData = res;
        this.msgText = this.translate.instant('gameLabel.saveCommentMsg')
        this.showCommentSave = true
      }

    )
  }


  sdgClickInfo(goalId: any) {
    this.selectedSdgId = parseInt(goalId)
    this.bornSdgInfo = [];
    this.regSdgInfo = [];
    this.sdgInfo = this.bornSdgSubGoalDeatails;
    this.values = Object.values(this.sdgInfo)
    for (let i = 0; i < this.values.length; i++) {
      if (this.values[i].SDG_Id === this.selectedSdgId) {
        if (this.values[i].score !== 'NA') {
          this.values[i].score = parseFloat(this.values[i].score);
          this.values[i].score = this.values[i].score.toFixed(2);
        }
        else {
          this.values[i].score = this.values[i].score;
        }
        this.bornSdgInfo.push(this.values[i]);
      }
    }
    this.valuesR = Object.values(this.regSdgSubGoalDeatails)
    for (let i = 0; i < this.valuesR.length; i++) {
      if (this.valuesR[i].SDG_Id === this.selectedSdgId) {
        this.regSdgInfo.push(this.valuesR[i]);
      }
    }
    this.sdgImage = "assets/images/sdgicon/sdg" + this.selectedSdgId + "_" + 'en' + ".png";
    this.sdgDescription = this.sdgDetails[this.selectedSdgId - 1].SDG_discription;
    this.sdgName = this.sdgDetails[this.selectedSdgId - 1].SDG_title;
    this.sdgOrigninalName = this.sdgDetails[this.selectedSdgId - 1].SDG_name;
    this.showSdgInfo = true;
  }

  closeSdgInfo() {
    this.showSdgInfo = false;

  }
  getNewSdgColor(colour: any) {
    let classvalue = "";
    if (colour === "green") {
      classvalue = "clr_code bg-success d-inline-block mr-2"
    }
    else if (colour === "red") {
      classvalue = "clr_code bg-red d-inline-block mr-2"
    }
    else if (colour === "orange") {
      classvalue = "clr_code bg-oragne d-inline-block mr-2"
    }
    else if (colour === "yellow") {
      classvalue = "clr_code bg-yellow d-inline-block mr-2"
    }
    else if (colour === "grey") {
      classvalue = "clr_code bg-secondary d-inline-block mr-2"
    }
    return classvalue;
  }

  getRegisterCountrySdgScore(i: any) {
    let v
    v = this.regSdgInfo[i].score;
    return v;
  }
  getRegisterCountrySdgColur(i: any) {
    let v
    v = this.regSdgInfo[i].color;
    return v;
  }

  commentSaveAlertOk() {
    this.showCommentSave = false
  }


  clickNext(state) {
    if (state === 'info')
      this.state = 'docInfo'
    this.getCountryInfo();
  }


  backClick(state) {
    if (state === 'docInfo')
      this.state = 'info'
  }


  back() {
    this.modalWindowService.showDocumentFlag = false
    this.router.navigate(['/life-book'])

  }
}


