import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FinanceService } from '../home/action-finance/finance.service';
import { FinanceStatusService } from '../home/finace-status/finance-status.service';
import { CommonService } from '../shared/common.service';
import { ConstantService } from '../shared/constant.service';

@Injectable({
  providedIn: 'root'
})
export class LearningDocService {
  usCountryName
  registerCountryExchangeRate
  registerCountryName
  currencyCode
  countryName
  registerCountryCode
  registerCountryCurrentcyName
  totalLoanValue = 0;
  countOfEarningMember = 0;
  subscribe;
  incomeToWealthRatio: any = 0;
  bincomeToWealthRatio: any = 0
  currentSavingRate: any = 0;
  expenceFoodPer: number = 0;
  expenceFoodPerInRes: number;
  expenceShelterPer: number = 0;
  fMamber = '#FF0000';
  diet;
  shelter;
  statusSatement = "";
  moreInforStm;
  moreInfoFlag: boolean = false;
  moreInfoTitle;
  amenities;;
  dispalyUtilityBornArr = [];
  dispalyUtilityCurrentArr = []
  UtilityArr = [];
  leisureData = [];
  leisure = [];
  btotalLoanValue = 0;
  bcountOfEarningMember = 0;
  bsubscribe;
  bcurrentSavingRate: any = 0;
  bexpenceFoodPer: number = 0;
  bexpenceFoodPerInRes: number;
  bexpenceShelterPer: number = 0;
  bfMamber = '#FF0000';
  bdiet;
  bshelter;
  constructor(public translate: TranslateService,
    private http: HttpClient,
    public constantService: ConstantService,
    public commonService: CommonService,
    public financeStatusService: FinanceStatusService,
    public financeService: FinanceService) { }

  getCommentData(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getDocComments/${gameId}`);

  }

  updateCommentData(gameId, doc_comments) {
    return this.http.post<any>(`${this.commonService.url}game/saveDocComments/${gameId}`, { doc_comments });

  }
  createBornAmenities(utility, comment) {
    this.amenities = utility;
    this.UtilityArr = [
      {
        DisplayName: this.translate.instant('gameLabel.COMPUTERS'),
        utilityValue: this.amenities.computers,
        image: "assets/images/computer_utility.png",
        familyModalImage: "assets/images/computer_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: comment.computerCmt
      },
      {
        DisplayName: this.translate.instant('gameLabel.REFRIGERATORS'),
        utilityValue: this.amenities.refrigerators,
        image: "assets/images/refrigerator_utility.png",
        familyModalImage: "assets/images/refrigerator_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: comment.refrigatertorCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.SAFE_WATER'),
        utilityValue: this.amenities.safeWater,
        image: "assets/images/surro_icon1.png",
        familyModalImage: "assets/images/safewater_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: "",
        comment: comment.waterCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.PUBLIC_SANITATION'),
        utilityValue: this.amenities.basicSanitation,
        image: "assets/images/surro_icon2.png",
        familyModalImage: "assets/images/sanitation_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: "",
        comment: comment.sanitationCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.MEDICAL_CARE'),
        utilityValue: this.amenities.healthService,
        image: "assets/images/surro_icon3.png",
        familyModalImage: "assets/images/medical_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: "",
        comment: comment.medicalCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.TELEVISIONS'),
        utilityValue: this.amenities.televisions,
        image: "assets/images/surro_icon7.png",
        familyModalImage: "assets/images/television_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: comment.televisionCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.RADIOS'),
        utilityValue: this.amenities.radios,
        image: "assets/images/surro_icon4.png",
        familyModalImage: "assets/images/radio_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: comment.redioCmt

      },

      {
        DisplayName: this.translate.instant('gameLabel.TELEPHONES'),
        utilityValue: this.amenities.telephones,
        image: "assets/images/surro_icon5.png",
        familyModalImage: "assets/images/telephone_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: comment.telephoneCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.CELLPHONES'),
        utilityValue: this.amenities.mobiles,
        image: "assets/images/surro_icon6.png",
        familyModalImage: "assets/images/mobile_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: comment.cellphoneCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.CARS'),
        utilityValue: this.amenities.vehicles,
        image: "assets/images/surro_icon9.png",
        familyModalImage: "assets/images/car_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: comment.carsCmt

      },
      {
        DisplayName: this.translate.instant('gameLabel.INTERNET'),
        utilityValue: this.amenities.internet,
        image: "assets/images/surro_icon8.png",
        familyModalImage: "assets/images/internet_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: "",
        comment: comment.internetCmt

      }

    ]
    this.dispalyUtilityBornArr = this.UtilityArr;
    let size = this.dispalyUtilityBornArr.length;
    for (let i = 0; i < size; i++) {
      if (this.dispalyUtilityBornArr[i].boolean == true) {
        if (this.dispalyUtilityBornArr[i].utilityValue) {
          this.dispalyUtilityBornArr[i].checkUncheckClass = "material-icons";
          this.dispalyUtilityBornArr[i].bvalue = "Yes";
        }
        else if (!this.dispalyUtilityBornArr[i].utilityValue) {
          this.dispalyUtilityBornArr[i].checkUncheckClass = "material-icons";
          this.dispalyUtilityBornArr[i].bvalue = "No";
        }
      }
      else if (this.dispalyUtilityBornArr[i].boolean == false) {
        this.dispalyUtilityBornArr[i].checkUncheckClass = "";
        this.dispalyUtilityBornArr[i].bvalue = this.dispalyUtilityBornArr[i].utilityValue
      }


    }
    //  indication not_avalable  indication avalable
  }
  createCurrentAmenities(utility) {
    this.amenities = utility;
    this.UtilityArr = [
      {
        DisplayName: this.translate.instant('gameLabel.COMPUTERS'),
        utilityValue: this.amenities.computers,
        image: "assets/images/computer_utility.png",
        familyModalImage: "assets/images/computer_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",
        comment: ""
      },
      {
        DisplayName: this.translate.instant('gameLabel.REFRIGERATORS'),
        utilityValue: this.amenities.refrigerators,
        image: "assets/images/refrigerator_utility.png",
        familyModalImage: "assets/images/refrigerator_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: ""

      },
      {
        DisplayName: this.translate.instant('gameLabel.SAFE_WATER'),
        utilityValue: this.amenities.safeWater,
        image: "assets/images/surro_icon1.png",
        familyModalImage: "assets/images/safewater_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: ""
      },
      {
        DisplayName: this.translate.instant('gameLabel.PUBLIC_SANITATION'),
        utilityValue: this.amenities.basicSanitation,
        image: "assets/images/surro_icon2.png",
        familyModalImage: "assets/images/sanitation_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: ""

      },
      {
        DisplayName: this.translate.instant('gameLabel.MEDICAL_CARE'),
        utilityValue: this.amenities.healthService,
        image: "assets/images/surro_icon3.png",
        familyModalImage: "assets/images/medical_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: ""

      },
      {
        DisplayName: this.translate.instant('gameLabel.TELEVISIONS'),
        utilityValue: this.amenities.televisions,
        image: "assets/images/surro_icon7.png",
        familyModalImage: "assets/images/television_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: ""

      },
      {
        DisplayName: this.translate.instant('gameLabel.RADIOS'),
        utilityValue: this.amenities.radios,
        image: "assets/images/surro_icon4.png",
        familyModalImage: "assets/images/radio_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: ""

      },

      {
        DisplayName: this.translate.instant('gameLabel.TELEPHONES'),
        utilityValue: this.amenities.telephones,
        image: "assets/images/surro_icon5.png",
        familyModalImage: "assets/images/telephone_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: "",

      },
      {
        DisplayName: this.translate.instant('gameLabel.CELLPHONES'),
        utilityValue: this.amenities.mobiles,
        image: "assets/images/surro_icon6.png",
        familyModalImage: "assets/images/mobile_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: ""

      },
      {
        DisplayName: this.translate.instant('gameLabel.CARS'),
        utilityValue: this.amenities.vehicles,
        image: "assets/images/surro_icon9.png",
        familyModalImage: "assets/images/car_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: false,
        checkUncheckClass: ""
      },
      {
        DisplayName: this.translate.instant('gameLabel.INTERNET'),
        utilityValue: this.amenities.internet,
        image: "assets/images/surro_icon8.png",
        familyModalImage: "assets/images/internet_utility_b.png",
        hoverValue: "",
        redGreeClassValue: "",
        bvalue: "",
        dvalue: "",
        boolean: true,
        checkUncheckClass: "",
      }

    ]
    this.dispalyUtilityCurrentArr = this.UtilityArr;
    let size = this.dispalyUtilityCurrentArr.length;
    for (let i = 0; i < size; i++) {
      if (this.dispalyUtilityCurrentArr[i].boolean == true) {
        if (this.dispalyUtilityCurrentArr[i].utilityValue) {
          this.dispalyUtilityCurrentArr[i].checkUncheckClass = "material-icons";
          this.dispalyUtilityCurrentArr[i].dvalue = "Yes";
          this.dispalyUtilityBornArr[i].dvalue = this.dispalyUtilityCurrentArr[i].dvalue
        }
        else if (!this.dispalyUtilityCurrentArr[i].utilityValue) {
          this.dispalyUtilityCurrentArr[i].checkUncheckClass = "material-icons";
          this.dispalyUtilityCurrentArr[i].dvalue = "No";
          this.dispalyUtilityBornArr[i].dvalue = this.dispalyUtilityCurrentArr[i].dvalue
        }
      }
      else if (this.dispalyUtilityCurrentArr[i].boolean == false) {
        this.dispalyUtilityCurrentArr[i].checkUncheckClass = "";
        this.dispalyUtilityCurrentArr[i].dvalue = this.dispalyUtilityCurrentArr[i].utilityValue
        this.dispalyUtilityBornArr[i].dvalue = this.dispalyUtilityCurrentArr[i].dvalue
      }
    }
    //  indication not_avalable  indication avalable
  }


  compaireAmenities() {
    for (let i = 0; i < this.dispalyUtilityBornArr.length; i++) {
      if (this.dispalyUtilityBornArr[i].boolean == true) {
        if (this.dispalyUtilityBornArr[i].bvalue === this.dispalyUtilityBornArr[i].dvalue) {
          this.dispalyUtilityBornArr[i].redGreeClassValue = ""
        }
        else if (this.dispalyUtilityBornArr[i].bvalue !== this.dispalyUtilityBornArr[i].dvalue) {
          if (this.dispalyUtilityBornArr[i].bvalue === "Yes" && this.dispalyUtilityBornArr[i].dvalue === "No") {
            this.dispalyUtilityBornArr[i].redGreeClassValue = "textRed"

          }
          else if (this.dispalyUtilityBornArr[i].bvalue === "No" && this.dispalyUtilityBornArr[i].dvalue === "Yes") {
            this.dispalyUtilityBornArr[i].redGreeClassValue = "textgreen"

          }
        }
      }
      else if (this.dispalyUtilityBornArr[i].boolean == false) {
        if (this.dispalyUtilityBornArr[i].bvalue === this.dispalyUtilityBornArr[i].dvalue) {
          this.dispalyUtilityBornArr[i].redGreeClassValue = ""
        }
        else if (this.dispalyUtilityBornArr[i].bvalue < this.dispalyUtilityBornArr[i].dvalue) {
          this.dispalyUtilityBornArr[i].redGreeClassValue = "textgreen"
        }
        else if (this.dispalyUtilityBornArr[i].bvalue > this.dispalyUtilityBornArr[i].dvalue) {
          this.dispalyUtilityBornArr[i].redGreeClassValue = "textRed"
        }
      }
    }
  }

  createLeisureTableArray(leisure, mainPerson, comment) {
    this.leisureData = [];
    this.leisure = [];
    for (let i in leisure) {
      this.leisure.push(leisure[i]);
    }
    for (let i = 0; i < this.leisure.length; i++) {
      if (this.leisure[i].leisure_code === 'MUSICAL') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Music'),
          "years": mainPerson.years_of_music,
          "comment": comment.musicalCmt
        })
      }
      else if (this.leisure[i].leisure_code === 'SOCIAL_OR_POLITICAL_ACTIVITIES') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Social_or_Political_Activities'),
          "years": mainPerson.years_of_socializing,
          "comment": comment.socialAndPoliticalActCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'ART') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Art'),
          "years": mainPerson.years_of_arts,
          "comment": comment.artCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'ENDURANCE') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Physical_Training'),
          "years": mainPerson.years_of_endurance,
          "comment": comment.physicalEnduranceTraningCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'VOLUNTEERING') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Volunteering'),
          "years": mainPerson.years_of_volunteering,
          "comment": comment.volunteeringCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'TELEVISION_VIEWING') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Television_Viewing'),
          "years": mainPerson.years_of_television,
          "comment": comment.televisionViewingCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'READING_STUDY') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Reading_Study'),
          "years": mainPerson.years_of_reading,
          "comment": comment.readingCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'OUTDOOR_ACTIVITIES') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Outdoor_Activities'),
          "years": mainPerson.years_of_outdoors,
          "comment": comment.outdoorActCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'FASHION_CLOTHING_APPEARANCE') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Fashion_Clothing_Appearance'),
          "years": mainPerson.years_of_fashion,
          "comment": comment.fashionAndClothingCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'SPORTS') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Sports'),
          "years": mainPerson.years_of_sports,
          "comment": comment.sportsCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'PLAY') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Playing_and_Socializing'),
          "years": mainPerson.years_of_music,
          "comment": comment.playingCmt

        })
      }
      else if (this.leisure[i].leisure_code === 'RELIGIOUS_ACTIVITY') {
        this.leisureData.push({
          "code": this.leisure[i].leisure_name,
          "name": this.translate.instant('gameLabel.Religious_Activity'),
          "years": mainPerson.years_of_social_activism,
          "comment": comment.religiousActCmt

        })
      }


    }
  }

  finanaceStatus(allPerson) {
    this.countOfEarningMember = 0;
    this.registerCountryCode = allPerson[this.constantService.FAMILY_SELF].register_country.currency_code;
    this.registerCountryCurrentcyName = allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural;
    this.usCountryName = this.constantService.US_COUNTRYNAME;
    this.registerCountryExchangeRate = allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.registerCountryName = allPerson[this.constantService.FAMILY_SELF].register_country.country;
    this.currencyCode = allPerson[this.constantService.FAMILY_SELF].country.currency_code;
    this.countryName = allPerson[this.constantService.FAMILY_SELF].country.country;
    this.diet = this.financeService.diet[allPerson[this.constantService.FAMILY_SELF].expense.diet.dietIndex].name
    this.shelter = this.financeService.shelter[allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex].name
    this.incomeToWealthRatio = parseFloat(allPerson[this.constantService.FAMILY_SELF].expense.incomeToWealthRatio)

    //
    if (allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense > 0 && allPerson[this.constantService.FAMILY_SELF].expense.householdIncome > 0) {
      this.expenceFoodPer = allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense * 100 / allPerson[this.constantService.FAMILY_SELF].expense.householdIncome;
      //  this.expenceFoodPerInRes=(usd*allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate);
    }
    else if (allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense <= 0 || allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.expenceFoodPer = 0;
    }
    //

    if (allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense > 0 && allPerson[this.constantService.FAMILY_SELF].expense.householdIncome > 0) {
      this.expenceShelterPer = allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense * 100 / allPerson[this.constantService.FAMILY_SELF].expense.householdIncome
    }
    else if (allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense <= 0 || allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.expenceShelterPer = 0;
    }






    if (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome >= 0 && allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses <= 0
      && allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.currentSavingRate = 0;
    }
    else if (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.currentSavingRate = 0;
    }
    else {
      let hi = (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome)
      let he = (allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses)
      let hi1 = (hi * 100)
      this.currentSavingRate = ((hi - he) / hi1).toFixed(2)
      // this.currentSavingRate = (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome - allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses) / (allPerson[this.constantService.FAMILY_SELF].expense.householdIncome) * 100
    }
    this.financeStatusService.calculateLoan(allPerson);

    let allPersonkeys = Object.keys(allPerson)
    for (let identity of allPersonkeys) {
      if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
        && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
        if (allPerson[identity].living_at_home) {
          if (allPerson[identity].income > 0) {
            this.countOfEarningMember = this.countOfEarningMember + 1;
          }
        }
      }
    }
    this.financeStatusService.checkClassForRedGreen(this.countOfEarningMember, this.currentSavingRate, allPerson, false);
  }

  finanaceStatusBorn(allPerson) {
    this.countOfEarningMember = 0;
    this.registerCountryCode = allPerson[this.constantService.FAMILY_SELF].register_country.currency_code;
    this.registerCountryCurrentcyName = allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural;
    this.usCountryName = this.constantService.US_COUNTRYNAME;
    this.registerCountryExchangeRate = allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.registerCountryName = allPerson[this.constantService.FAMILY_SELF].register_country.country;
    this.currencyCode = allPerson[this.constantService.FAMILY_SELF].country.currency_code;
    this.countryName = allPerson[this.constantService.FAMILY_SELF].country.country;
    this.bdiet = this.financeService.diet[allPerson[this.constantService.FAMILY_SELF].born_data.expense.diet.dietIndex].name
    this.bshelter = this.financeService.shelter[allPerson[this.constantService.FAMILY_SELF].born_data.expense.shelter.shelterIndex].name
    this.bincomeToWealthRatio = parseFloat(allPerson[this.constantService.FAMILY_SELF].born_data.expense.incomeToWealthRatio)
    //
    if (allPerson[this.constantService.FAMILY_SELF].born_data.expense.diet.householdFoodExpense > 0 && allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome > 0) {
      this.bexpenceFoodPer = allPerson[this.constantService.FAMILY_SELF].born_data.expense.diet.householdFoodExpense * 100 / allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome;
      //  this.expenceFoodPerInRes=(usd*allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate);
    }
    else if (allPerson[this.constantService.FAMILY_SELF].born_data.expense.diet.householdFoodExpense <= 0 || allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome <= 0) {
      this.bexpenceFoodPer = 0;
    }
    //

    if (allPerson[this.constantService.FAMILY_SELF].born_data.expense.shelter.householdHousingExpense > 0 && allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome > 0) {
      this.bexpenceShelterPer = allPerson[this.constantService.FAMILY_SELF].born_data.expense.shelter.householdHousingExpense * 100 / allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome
    }
    else if (allPerson[this.constantService.FAMILY_SELF].born_data.expense.shelter.householdHousingExpense <= 0 || allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome <= 0) {
      this.bexpenceShelterPer = 0;
    }

    if (allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome >= 0 && allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdExpenses <= 0
      && allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome <= 0) {
      this.bcurrentSavingRate = 0;
    }
    else if (allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome <= 0) {
      this.bcurrentSavingRate = 0;
    }
    else {
      let hi = (allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdIncome)
      let he = (allPerson[this.constantService.FAMILY_SELF].born_data.expense.householdExpenses)
      let hi1 = (hi * 100)
      this.bcurrentSavingRate = ((hi - he) / hi1).toFixed(2)
    }
    this.financeStatusService.calculateLoan(allPerson);

    let allPersonkeys = Object.keys(allPerson)
    for (let identity of allPersonkeys) {
      if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
        && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
        if (allPerson[identity].living_at_home) {
          if (allPerson[identity].income > 0) {
            this.bcountOfEarningMember = this.bcountOfEarningMember + 1;
          }
        }
      }
    }
    this.financeStatusService.checkClassForRedGreen(this.bcountOfEarningMember, this.bcurrentSavingRate, allPerson, true);
  }


  updateCommentDataForAmenities(amenities, comment) {
    for (let i = 0; i < amenities.length; i++) {
      if (amenities[i].DisplayName === this.translate.instant('gameLabel.COMPUTERS')) {
        comment.computerCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.REFRIGERATORS')) {
        comment.refrigatertorCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.SAFE_WATER')) {
        comment.waterCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.PUBLIC_SANITATION')) {
        comment.sanitationCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.MEDICAL_CARE')) {
        comment.medicalCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.TELEVISIONS')) {
        comment.televisionCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.RADIOS')) {
        comment.redioCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.TELEPHONES')) {
        comment.telephoneCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.CELLPHONES')) {
        comment.cellphoneCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.CARS')) {
        comment.carsCmt = amenities[i].comment
      }
      else if (amenities[i].DisplayName === this.translate.instant('gameLabel.INTERNET')) {
        comment.internetCmt = amenities[i].comment
      }
    }
    return comment;

  }

  updateLeisureCommentData(leisure, comment) {
    for (let i = 0; i < leisure.length; i++) {
      if (leisure[i].code === 'MUSICAL') {
        comment.musicalCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'SOCIAL_OR_POLITICAL_ACTIVITIES') {
        comment.socialAndPoliticalActCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'ART') {
        comment.artCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'ENDURANCE') {
        comment.physicalEnduranceTraningCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'VOLUNTEERING') {
        comment.volunteeringCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'TELEVISION_VIEWING') {
        comment.televisionViewingCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'READING_STUDY') {
        comment.readingCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'OUTDOOR_ACTIVITIES') {
        comment.outdoorActCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'FASHION_CLOTHING_APPEARANCE') {
        comment.fashionAndClothingCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'SPORTS') {
        comment.sportsCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'PLAY') {
        comment.playingCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'CHURCH') {
        comment.churchCmt = leisure[i].comment
      }
      else if (leisure[i].code === 'RELIGIOUS_ACTIVITY') {
        comment.religiousActCmt = leisure[i].comment
      }
    }
    return comment;
  }


  updateSdgComment(sdg, comment) {
    for (let i = 0; i < sdg.length; i++) {
      if (sdg[i].sdgId == 1) {
        comment.Sdg1 = sdg[i].comment
      }
      else if (sdg[i].sdgId == 2) {
        comment.Sdg2 = sdg[i].comment
      }
      else if (sdg[i].sdgId == 3) {
        comment.Sdg3 = sdg[i].comment
      }
      else if (sdg[i].sdgId == 4) {
        comment.Sdg4 = sdg[i].comment
      }
      else if (sdg[i].sdgId == 5) {
        comment.Sdg5 = sdg[i].comment
      } else if (sdg[i].sdgId == 6) {
        comment.Sdg6 = sdg[i].comment
      } else if (sdg[i].sdgId == 7) {
        comment.Sdg7 = sdg[i].comment
      }
      else if (sdg[i].sdgId == 8) {
        comment.Sdg8 = sdg[i].comment
      } else if (sdg[i].sdgId == 9) {
        comment.Sdg9 = sdg[i].comment
      } else if (sdg[i].sdgId == 10) {
        comment.Sdg10 = sdg[i].comment
      } else if (sdg[i].sdgId == 11) {
        comment.Sdg11 = sdg[i].comment
      }
      else if (sdg[i].sdgId == 12) {
        comment.Sdg12 = sdg[i].comment
      } else if (sdg[i].sdgId == 13) {
        comment.Sdg13 = sdg[i].comment = sdg[i].comment
      } else if (sdg[i].sdgId == 14) {
        comment.Sdg14 = sdg[i].comment
      } else if (sdg[i].sdgId == 15) {
        comment.Sdg15 = sdg[i].comment
      } else if (sdg[i].sdgId == 16) {
        comment.Sdg16 = sdg[i].comment = sdg[i].comment
      } else if (sdg[i].sdgId == 17) {
        comment.Sdg17 = sdg[i].comment
      }
    }
    return comment
  }


}
