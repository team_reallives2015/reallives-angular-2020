import { TestBed } from '@angular/core/testing';

import { LearningDocService } from './learning-doc.service';

describe('LearningDocService', () => {
  let service: LearningDocService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LearningDocService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
