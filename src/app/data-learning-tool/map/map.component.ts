import { Component, OnInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { TranslateService } from '@ngx-translate/core';
import { ToolService } from '../tool.service';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  getAnsFlag: boolean = false;
  bornCountry: any = [];
  registerCountry: any = [];
  r: any; g: any; b: any
  showAlertFlag: boolean = false;
  msgText: any;
  resetFlag: boolean = false
  count: any = 0;
  bornCountryCode: any[] = [];
  registerCountryCode: any;
  selectedObject: any[] = [];
  showLegent: boolean = false;
  constructor(public toolService: ToolService,
    public translate: TranslateService,
    public gameSummeryService: GameSummeryService) { }

  ngOnInit(): void {
    this.bornCountry = this.toolService.selectedBornCountryObject;
    this.registerCountry = this.toolService.selectedRegisterCountryObject;
    this.gameSummeryService.getCountryList().subscribe(
      res => {
        this.toolService.countryList = res;
        this.setCountryMap();
      })
  }


  setCountryMap() {
    this.r = 255; this.g = 0; this.b = 0
    let countryB = this.bornCountry;
    let countryR = this.registerCountry
    let map = am4core.create("chartdiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();
    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 0;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.events.on("hit", function (ev) {
      if (this.toolService.flagForMapQue) {
        let selectedObj: any;
        selectedObj = ev.target.dataItem.dataContext;
        this.count = 0;
        for (let i = 0; i < country1.length; i++) {
          if (country1[i].hasOwnProperty('fill')) {
            let r = country1[i].fill._value['r'];
            let g = country1[i].fill._value['g'];
            let b = country1[i].fill._value['b'];
            if (this.r === r && this.g === g && this.b === b) {
              this.count = this.count + 1;
            }
          }
        }
        if (this.count < 2 || this.count === 0) {
          for (let i = 0; i < country1.length; i++) {
            if (country1[i].id === selectedObj["id"]) {
              this.bornCountryCode.push(selectedObj["id"])
              country1[i].fill = am4core.color("#FF0000");
              this.selectedObject.push(selectedObj["id"])
              break;
            }
          }
        }
        else {
          this.showAlertFlag = true;
          this.msgText = this.translate.instant('gameLabel.map_warr');
        }
        polygonSeries.data = country1
        polygonTemplate.propertyFields.fill = "fill";
      }
    }, this);
    let country1: any[] = [];
    if (!this.toolService.flagForMapQue) {
      country1.push({
        "id": countryB.Code,
        "name": countryB.country,
        "value": 100,
        "fill": am4core.color("#00FF00")
      })
      country1.push({
        "id": countryR.Code,
        "name": countryR.country,
        "value": 100,
        "fill": am4core.color("#FFA500")
      })
      polygonSeries.data = country1
      polygonTemplate.propertyFields.fill = "fill";
    }
    else if (this.getAnsFlag && !this.resetFlag) {
      for (let i = 0; i < this.toolService.countryList.length; i++) {
        if (this.selectedObject.includes(this.toolService.countryList[i].Code)) {
          if (this.toolService.countryList[i].Code === this.toolService.selectedBornCountryObject.Code) {
            country1.push({
              "id": this.toolService.countryList[i].Code,
              "name": this.toolService.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#0000FF")
            })
          }
          else if (this.toolService.countryList[i].Code === this.toolService.selectedRegisterCountryObject.Code) {
            country1.push({
              "id": this.toolService.countryList[i].Code,
              "name": this.toolService.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#0000FF")
            })
          }
          else {
            country1.push({
              "id": this.toolService.countryList[i].Code,
              "name": this.toolService.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#FF0000")
            })
          }
        }
        else {
          if (this.toolService.countryList[i].Code === this.toolService.selectedBornCountryObject.Code) {
            country1.push({
              "id": this.toolService.countryList[i].Code,
              "name": this.toolService.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#00FF00")
            })
          }
          else if (this.toolService.countryList[i].Code === this.toolService.selectedRegisterCountryObject.Code) {
            country1.push({
              "id": this.toolService.countryList[i].Code,
              "name": this.toolService.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#00FF00")
            })
          }
          else {
            for (let k = 0; k < this.selectedObject.length; k++) {
              if (this.toolService.countryList[i].Code === this.selectedObject[k]) {
                country1.push({
                  "id": this.toolService.countryList[i].Code,
                  "name": this.toolService.countryList[i].country,
                  "value": 100,
                  "fill": am4core.color("#FF0000")
                })
              }
              else {
                country1.push({
                  "id": this.toolService.countryList[i].Code,
                  "name": '',
                  "value": 100,
                  "fill": am4core.color("#808080")
                })
              }
            }

          }
        }
      }
      polygonSeries.data = country1
      polygonTemplate.propertyFields.fill = "fill";
    }
    else if (this.getAnsFlag && this.resetFlag) {
      for (let i = 0; i < this.toolService.countryList.length; i++) {
        country1.push({
          "id": this.toolService.countryList[i].Code,
          "name": '',
          "value": 100,
          "fill": am4core.color("#808080")
        })
      }
      polygonSeries.data = country1
      polygonTemplate.propertyFields.fill = "fill";
    }
    else {
      for (let i = 0; i < this.toolService.countryList.length; i++) {
        country1.push({
          "id": this.toolService.countryList[i].Code,
          "name": '',
          "value": 100,
          "fill": am4core.color("#808080")
        })
      }
      polygonSeries.data = country1
      polygonTemplate.propertyFields.fill = "fill";
    }
  }

  clickOk() {
    this.showAlertFlag = false
  }

  reset() {
    this.showAlertFlag = false
    this.resetFlag = true;
    am4core.disposeAllCharts();
    this.setCountryMap();
  }

  getAns() {
    this.showAlertFlag = false
    this.getAnsFlag = true;
    this.showLegent = true;
    this.setCountryMap();
    let count = 0;
    for (let i = 0; i < this.bornCountryCode.length; i++) {
      if (this.bornCountryCode[i] === this.toolService.selectedBornCountryObject.Code || this.bornCountryCode[i] === this.toolService.selectedRegisterCountryObject.Code) {
        count = count + 1;
      }
    }
    if (count === 2) {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.map_warr1');
    }
    else if (count === 1) {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.map_warr2');
    }
    else {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.map_warr3');
    }
  }
}
