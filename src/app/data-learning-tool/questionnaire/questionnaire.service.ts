import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionnaireService {
  quetionMeatadata: any;
  constructor(public commonService: CommonService,
    public http: HttpClient) { }


  getQuetionMetadata(bornId: any, registerId: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/generateCountryQuestions/${bornId}/${registerId}`);

  }
}
