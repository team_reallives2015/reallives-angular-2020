import { Component, OnInit } from '@angular/core';
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { ToolService } from '../tool.service';
import { QuestionnaireService } from './questionnaire.service';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.css']
})
export class QuestionnaireComponent implements OnInit {
  state = "que"
  quetionMetadata: any;
  finalMetadata1: any = [];
  finalMetadata2: any = [];
  finalMetadata: any = [];
  selectedActionForRedioButton: any;
  opObj: any
  obj: any = [];
  first: any = []
  firstdispaly: any = []
  correcteAns: any;
  notGivenAns: any;
  wrongAns: any;
  showResult: boolean = false;
  constructor(public questionnaireService: QuestionnaireService,
    public toolService: ToolService,
    public phpToolService: PhpToolService) { }

  ngOnInit(): void {
    this.quetionMetadata = this.questionnaireService.quetionMeatadata
    this.setQuetions();
  }

  setQuetions() {
    let op = 0
    for (let i = 0; i <= 9; i++) {
      this.obj = [];
      for (let j = 0; j < this.quetionMetadata[i].options.length; j++) {
        this.obj.push({
          "id": op++,
          "value": this.quetionMetadata[i].options[j],
          "color": ''
        })
      }
      this.finalMetadata.push({
        "quetion": this.quetionMetadata[i].question,
        "option": this.obj,
        "ans": this.quetionMetadata[i].answer,
        "selectedAns": '',
        "class": '',
      })
      if (i <= 4) {
        this.finalMetadata1.push({
          "quetion": this.quetionMetadata[i].question,
          "option": this.obj,
          "ans": this.quetionMetadata[i].answer,
          "selectedAns": '',
          "class": '',

        })
      }
      else if (i > 4) {
        this.finalMetadata2.push({
          "quetion": this.quetionMetadata[i].question,
          "option": this.obj,
          "ans": this.quetionMetadata[i].answer,
          "selectedAns": '',
          "class": '',
          "color": ''

        })
      }
    }

  }

  redioButtonClick(j: any, ans: any, id: any, flag: any) {
    if (flag) {
      for (let i = 0; i < this.finalMetadata2.length; i++) {
        if (j === i) {
          this.finalMetadata2[i].selectedAns = ans;
          break;
        }
      }
    }
    else {
      for (let i = 0; i < this.finalMetadata1.length; i++) {
        if (j === i) {
          this.finalMetadata1[i].selectedAns = ans;
          break;
        }
      }
    }

  }
  onSubmit() {
    this.correcteAns = 0; this.notGivenAns = 0; this.wrongAns = 0;
    for (let i = 0; i < this.finalMetadata1.length; i++) {
      if (!isNaN(Number(this.finalMetadata1[i].ans))) {
        this.finalMetadata1[i].ans = String(this.finalMetadata1[i].ans)
        for (let j = 0; j < this.finalMetadata1[i].option.length; j++) {
          if (!isNaN(Number(this.finalMetadata1[i].option[j].value))) {
            this.finalMetadata1[i].option[j].value = String(this.finalMetadata1[i].option[j].value)
          }
        }
      }
      if (this.finalMetadata1[i].ans === this.finalMetadata1[i].selectedAns) {
        this.correcteAns = this.correcteAns + 1;
        this.finalMetadata1[i].class = 'check'
      }
      else if (this.finalMetadata1[i].selectedAns === "") {
        this.notGivenAns = this.notGivenAns + 1;
      }
      else {
        for (let j = 0; j < this.finalMetadata1[i].option.length; j++) {
          if (this.finalMetadata1[i].option[j].value === this.finalMetadata1[i].selectedAns) {
            this.finalMetadata1[i].option[j].color = 'red'
          }
        }
        this.wrongAns = this.wrongAns + 1;
        this.finalMetadata1[i].class = 'close'

      }
    }
    for (let i = 0; i < this.finalMetadata2.length; i++) {
      if (!isNaN(Number(this.finalMetadata2[i].ans))) {
        this.finalMetadata2[i].ans = String(this.finalMetadata2[i].ans)
        for (let j = 0; j < this.finalMetadata2[i].option.length; j++) {
          if (!isNaN(Number(this.finalMetadata2[i].option[j].value))) {
            this.finalMetadata2[i].option[j].value = String(this.finalMetadata2[i].option[j].value)
          }
        }
      }

      if (this.finalMetadata2[i].ans === this.finalMetadata2[i].selectedAns) {
        this.correcteAns = this.correcteAns + 1;
        this.finalMetadata2[i].class = 'check'
      }
      else if (this.finalMetadata2[i].selectedAns === "") {
        this.notGivenAns = this.notGivenAns + 1;
      }
      else {
        for (let j = 0; j < this.finalMetadata2[i].option.length; j++) {
          if (this.finalMetadata2[i].option[j].value === this.finalMetadata2[i].selectedAns) {
            this.finalMetadata2[i].option[j].color = 'red'
          }

        }
        this.wrongAns = this.wrongAns + 1;
        this.finalMetadata2[i].class = 'close'
      }
    }
    this.showResult = true;

  }

  reTake() {
    this.quetionMetadata = [];
    this.finalMetadata1 = [];
    this.finalMetadata2 = [];
    this.finalMetadata = [];
    this.questionnaireService.getQuetionMetadata(this.phpToolService.selectedBornCountryObject.countryid, this.phpToolService.selectedRegisterCountryObject.countryid).subscribe(
      res => {
        this.quetionMetadata = res;
        this.setQuetions();
      }
    )
  }

  successMsgWindowClose() {
    this.showResult = false;
  }


  ClickNext() {

  }
}
