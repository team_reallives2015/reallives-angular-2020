import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from '../shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class ToolService {
  countryList = [];
  selectedBornCountryObject: any = null;
  selectedRegisterCountryObject: any = null;
  showKnowMore: boolean = false;
  konwMoreCountry: any;
  showQuetions: boolean = false;
  flowFromCountry: boolean = false;
  flowFromSdg: boolean = false;
  flagForMapQue: boolean = false;
  constructor(public commonService: CommonService,
    private http: HttpClient) { }

  getCountryList() {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/getAllCountries`);
  }

  getRegisterAndBornCountry(bornId: any, registerId: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/getAllCountries/${bornId}/${registerId}`)
  }
  getBornRegisterCountryData(born: any, register: any, lang: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/getCountries/${born}/${register}/${lang}`);
  }
  getWealthData(born: any, register: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/getWealth/${born}/${register}`)
  }
  getEcoStatusData(born: any, register: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/getEconomicStatusData/${born}/${register}`)
  }

  getRegisterAndBornCountryGroupData(bornId: any, registerId: any, lang: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/getCountryGroupInfo/${bornId}/${registerId}/${lang}`)

  }

  getSdgCountryColour(country1: any, country2: any) {
    return this.http.post<any>(`${this.commonService.url}tool/getCountrySDG`, { country1, country2 });

  }


  getSDGInfo(countryId: any, flagForGoalsInfo: any, lang: any) {
    return this.http.get<Array<Object>>(`${this.commonService.url}tool/SDGDetails/${countryId}/${flagForGoalsInfo}/${lang}`);
  }
}