import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryKnowMoreToolComponent } from './country-know-more-tool.component';

describe('CountryKnowMoreToolComponent', () => {
  let component: CountryKnowMoreToolComponent;
  let fixture: ComponentFixture<CountryKnowMoreToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryKnowMoreToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryKnowMoreToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
