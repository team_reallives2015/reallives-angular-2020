import { Component, OnInit } from '@angular/core';
import { SanitizePipe } from '../sanitize.pipe';
import { ToolService } from '../tool.service';

@Component({
  selector: 'app-country-know-more-tool',
  templateUrl: './country-know-more-tool.component.html',
  styleUrls: ['./country-know-more-tool.component.css']
})
export class CountryKnowMoreToolComponent implements OnInit {

  displayLonly: boolean = false;
  displayUnesco: boolean = false;
  disaplayHealth: boolean = false;
  lonlyClassForTab = "active";
  healthClassForTab = "";
  unescoClassForTab = ""
  sdgCountry = "";
  countryShort = ""
  wikiUrl = "https://en.wikipedia.org/wiki/";
  sdgUrl = "https://dashboards.sdgindex.org/profiles/"
  ciaUrl = "https://www.cia.gov/the-world-factbook/countries/";
  encyclopediaUrl = "https://www.britannica.com/place/";
  nationalGeographicUrl = "https://kids.nationalgeographic.com/geography/countries/article/";
  clicmateUrl = "https://en.wikipedia.org/wiki/Category:Ethnic_groups_in_";
  helathUrl = "https://data.unicef.org/country/";
  hdrUrl = "https://hdr.undp.org/data-center/specific-country-data#/countries/"
  biodiversityUrl = "https://www.ibat-alliance.org/country_profiles/"
  constructor(public sanitize: SanitizePipe,
    public toolService: ToolService) { }

  ngOnInit(): void {
    this.countryShort = (this.toolService.konwMoreCountry.country.slice(0, 3));
    this.countryShort = this.countryShort.toLowerCase();
    this.displayData('Lonly');
  }
  displayData(check: any) {

    switch (check) {

      case "Lonly":
        this.lonlyClassForTab = "active";
        this.healthClassForTab = "";
        this.unescoClassForTab = ""
        this.disaplayHealth = false;
        this.displayLonly = true;
        this.displayUnesco = false;
        break;
      case 'sdg':
        this.lonlyClassForTab = "";
        this.healthClassForTab = "";
        this.unescoClassForTab = "active"
        this.sdgCountry = this.toolService.konwMoreCountry.country.split(" ").join('-').toLowerCase();
        this.disaplayHealth = false;
        this.displayLonly = false;
        this.displayUnesco = true;
        break;

      case 'health':
        this.lonlyClassForTab = "";
        this.healthClassForTab = "active";
        this.unescoClassForTab = ""
        this.disaplayHealth = true;
        this.displayLonly = false;
        this.displayUnesco = false;
        break;

    }
  }
  closeReadMore() {
    this.toolService.showKnowMore = false
  }
}
