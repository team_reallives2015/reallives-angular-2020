import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataLearningToolComponent } from './data-learning-tool.component';

describe('DataLearningToolComponent', () => {
  let component: DataLearningToolComponent;
  let fixture: ComponentFixture<DataLearningToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataLearningToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataLearningToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
