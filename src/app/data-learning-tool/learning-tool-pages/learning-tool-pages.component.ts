import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { HomeService } from 'src/app/home/home.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { QuestionnaireService } from '../questionnaire/questionnaire.service';
import { ToolService } from '../tool.service';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { PhpToolService } from 'src/app/data-learning-tool-php/php-tool.service';
import { Router } from '@angular/router';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
@Component({
  selector: 'app-learning-tool-pages',
  templateUrl: './learning-tool-pages.component.html',
  styleUrls: ['./learning-tool-pages.component.css']
})
export class LearningToolPagesComponent implements OnInit {
  selectedCountryName = 'Null'
  //
  brokenLadder1;
  brokenLadder2
  brokenLadder3
  classForStatusBorderOne = "newTableFontSize";
  classForStatusBorderTwo = "newTableFontSize";
  classForStatusBorderThree = "newTableFontSize"
  classForStatusBorderFour = "newTableFontSize";
  classForStatusBorderFive = "newTableFontSize";
  bornCountry;
  registerCountry;
  chanceOfPoorR;
  bOne: number = 0;
  bTwo: number = 0;
  bThree: number = 0;
  bFour: number = 0;
  rOne: number = 0;
  rTwo: number = 0;
  rThree: number = 0;
  rFour: number = 0;
  firstR
  moreInfoFlag: boolean = false;
  //
  gameId;
  selectedcountry = [];
  designALifeObject = {};
  classForCountryOne = ""
  classForCountryTwo = ""
  statusOneClass = "";
  statusTwoClass = "";
  statusThreeClass = "";
  statusFourClass = "";
  statusFiveClass = "";
  data;
  allPerson;
  country;
  mainPerson;
  bornFlagArray: any[] = []
  registerFlagArray: any[] = []
  chartQuemapFlag: boolean = false
  label: any;
  state: any;
  markers: any[] = [];
  mapZoom: any;
  lat: any;
  lng: any;
  wealthGraphSentenceBorn1: any;
  wealthGraphSentenceBorn2: any;
  wealthGraphSentenceBorn3: any;
  wealthGraphSentenceReg1: any;
  wealthGraphSentenceReg2: any;
  wealthGraphSentenceReg3: any;
  bornCountryWelathMetaData: any;
  registerCountryWelathMetaData: any;
  bornCountryWelathMetaData1: any;
  registerCountryWelathMetaData1: any;
  bornCountryArray: any[] = [];
  registerCountryArray: any[] = [];
  constantGraphValueArray = [
    { 'name': '10%', 'value': 10 / 100 },
    { 'name': '20%', 'value': 20 / 100 },
    { 'name': '30%', 'value': 30 / 100 },
    { 'name': '40%', 'value': 40 / 100 },
    { 'name': '50%', 'value': 50 / 100 },
    { 'name': '60%', 'value': 60 / 100 },
    { 'name': '70%', 'value': 70 / 100 },
    { 'name': '80%', 'value': 80 / 100 },
    { 'name': '90%', 'value': 90 / 100 },
    { 'name': '100%', 'value': 100 / 100 },
  ]
  registerCurrency = '';
  bornCurrency = '';
  selectedSdgId: any;
  showSdgInfo: boolean = false;
  sdgDetails: any;
  bornSdgSubGoalDeatails: any;
  regSdgSubGoalDeatails: any
  bornSdgInfo: any
  regSdgInfo: any
  registerSdgInfo: any;
  bcode: any = "";
  rcode: any = "";
  registerCountrySdgName: any
  sdgDescription: any
  sdgInfo: any
  sdgName: any
  sdgOrigninalName: any;
  registeredCountrySdgDeatails: any;
  sdgNewData: any;
  sdgReagisterData: any;
  bornGoalColur: any;
  regiGoalColue: any;
  values: any;
  valuesR: any;
  sdgImage: any;
  //graph
  multi: any[] = [];
  multi2: any[] = [];
  multi3: any[] = [];
  showGraphWindow: boolean = false;
  maxLenght: number = 40;
  yTrimValue: boolean = true;
  schemeType: string = 'linear';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  maxXAxisTickLength: number = 10
  gradient: boolean = false;
  showLegend: boolean = false;
  showLegend1: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = this.translate.instant('gameLabel.Country');
  yAxisLabelReg: any;
  showYAxisLabel: boolean = true;
  xAxisLabel: string = this.translate.instant('gameLabel.Population');
  xAxisLabelReg: any;
  colorSchemeBorn: any;
  activeClassForDemo: any;
  activeClassForPerCapita = "nav-item nav-link";
  activeClassForLFO = "nav-item nav-link";
  activeClassForIndex = "nav-item nav-link";
  activeClassForSdg = "nav-item nav-link";
  onClickDemoGraphic: boolean = true;
  onClickPerCapita: boolean = false;
  onCLickLabourForce: boolean = false;
  onCLickSdgGroup: boolean = false;
  onClickIndexRating: boolean = false;
  onClickSdgData: boolean = false;
  sentenceTextWindow1: string = "";
  bithString: string = "";
  deathString: string = "";
  primarySchoolString: string = "";
  happinessString: string = "";
  valurForHappiAndCorr: any;
  giniIndexString: string = "";
  labourForceString1: string = "";
  labourForceString2: string = "";
  sexRatio: string = "";
  accesEleString = "";
  valueForHdi: any;
  sexRatioValue: any;
  sdgScoreBorn: any;
  sdgScoreRegister: any;
  populationString: any;
  welthGraphDataNotGenrateFlag: boolean = false;
  status: any;
  statusValue: any;
  firstValue: any;
  secondValue: any;
  thirdValue: any;
  fourthValue: any;
  firstValueReg: any;
  secondValueReg: any;
  thirdValueReg: any;
  fourthValueReg: any;
  statusMetadata: any
  oldStatusValue: any;
  registerStatusData: any;
  incomeGraphStringOne: any;
  incomeGraphStringTwo: any;
  incomeGraphStringThree: any;
  registerCountryExchangeRate: any;
  stringReturnValue: any;
  classvalue: any;
  poupulationValue: any;
  showAlertFlag: boolean = false;
  msgText: any;
  stringRetutnText: any;
  senntenceGeneratedFlag: any;
  single: any;
  checkFormat: any;
  bornCountryGroupList: any;
  registerCountryGroupList: any;
  groupIndex: any
  quetionayMetadata: any
  colorScheme = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorScheme1 = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA'],
  };
  colorScheme3 = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#0000FF']
  };
  view: any;
  view1: any;
  view2: any;
  view3: any;
  view4: any;
  multi1: any;
  showDataLabel = true;
  sdgData: any;
  sdgDispalyArrayBorn: any;
  sdgDispalyArrayRegister: any;
  webLink = "https://dashboards.sdgindex.org/profiles/";
  countryGroupTitle: any;
  countryGroupIframeFlag: boolean = false;
  colorScheme2 = {
    domain: ['#00FFFF', '#567caa', '#567caa']
  };

  formatDataLabel(value: any) {
    return "$" + value;
  }
  constructor(public toolService: ToolService,
    public phpToolService: PhpToolService,
    public homeService: HomeService,
    public modalWindowService: modalWindowShowHide,
    public gameSummeryService: GameSummeryService,
    public constantService: ConstantService,
    public translate: TranslateService,
    public commonService: CommonService,
    public translationService: TranslationService,
    public router: Router,
    public questionnaireService: QuestionnaireService) { }


  ngOnInit(): void {
    this.modalWindowService.showCountryLearningTool = true
    this.modalWindowService.showWaitScreen = true;
    if (!this.phpToolService.flagForPhpToolClick) {
      this.homeService.getPersonFromId(this.gameSummeryService.gameId).subscribe(
        res => {
          this.data = true;
          this.allPerson = res;
          this.translationService.setSelectedlanguage(this.allPerson.selected_language);
          this.translationService.changeLang.emit(this.allPerson.selected_language);
          this.mainPerson = this.allPerson[this.constantService.FAMILY_SELF]
          this.country = this.mainPerson.country;
          this.registerCountry = this.mainPerson.register_country
          this.phpToolService.selectedBornCountryObject = this.country
          this.phpToolService.selectedRegisterCountryObject = this.registerCountry
          this.toolService.selectedBornCountryObject = this.country
          this.toolService.selectedRegisterCountryObject = this.registerCountry
          this.bornCountry = this.country;
          this.registerCountry = this.registerCountry;
          if (typeof this.toolService.selectedRegisterCountryObject.Currency_name === 'undefined') {
            this.registerCurrency = this.toolService.selectedRegisterCountryObject['Currency name']
          } else {
            this.registerCurrency = this.toolService.selectedRegisterCountryObject['Currency_name']
          }
          if (typeof this.toolService.selectedBornCountryObject.Currency_name === 'undefined') {
            this.bornCurrency = this.toolService.selectedBornCountryObject['Currency name']
          } else {
            this.bornCurrency = this.toolService.selectedBornCountryObject['Currency_name']
          }
          this.toolService.getWealthData(this.bornCountry.countryid, this.registerCountry.countryid).subscribe(
            res => {
              this.bornCountryWelathMetaData = res[0];
              this.registerCountryWelathMetaData = res[1];
              this.toolService.getEcoStatusData(this.bornCountry.countryid, this.registerCountry.countryid).subscribe(
                res => {
                  this.statusMetadata = res[0];
                  this.gameSummeryService.ecoStatusData = this.statusMetadata;
                  this.registerStatusData = res[1];
                  this.toolService.getRegisterAndBornCountryGroupData(this.bornCountry.countryid, this.registerCountry.countryid, this.translationService.selectedLang.code).subscribe(
                    res => {
                      let data: any;
                      data = res;
                      let bornCountry = data['bornCountry']
                      let registerCountry = data['registerCountry']
                      this.bornCountryGroupList = Object.values(bornCountry);
                      this.registerCountryGroupList = Object.values(registerCountry);
                      this.toolService.getSdgCountryColour(this.bornCountry.countryid, this.registerCountry.countryid).subscribe(
                        (res: any) => {
                          this.sdgData = res;
                          this.toolService.getSDGInfo(this.bornCountry.countryid, true, this.translationService.selectedLang.code).subscribe((sdgInfo: any) => {
                            this.sdgDetails = sdgInfo['goals'];
                            this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                            this.toolService.getSDGInfo(this.registerCountry.countryid, false, this.translationService.selectedLang.code).subscribe((res: any) => {
                              this.regSdgSubGoalDeatails = sdgInfo['targets'];
                              this.questionnaireService.getQuetionMetadata(this.bornCountry.countryid, this.registerCountry.countryid).subscribe(
                                res => {
                                  this.quetionayMetadata = res;
                                  this.questionnaireService.quetionMeatadata = this.quetionayMetadata;
                                  this.generateArrayForWelathGraph();
                                  this.setSocialEconomicalStatus();
                                  this.state = 'country-information';
                                  this.modalWindowService.showWaitScreen = false;
                                })
                            })
                          })
                        })
                    })
                })
            })
        })

    }
    else if (this.phpToolService.flagForPhpToolClick) {
      this.view = [320, 200];
      this.view1 = [320, 200];
      this.view2 = [700, 400];
      this.view3 = [1000, 200];
      this.view4 = [600, 200];
      this.bornCountry = this.country;
      this.registerCountry = this.registerCountry;
      this.bornCountry = this.phpToolService.selectedBornCountryObject;
      this.registerCountry = this.phpToolService.selectedRegisterCountryObject;
      this.toolService.selectedBornCountryObject = this.bornCountry
      this.toolService.selectedRegisterCountryObject = this.registerCountry
      if (typeof this.toolService.selectedRegisterCountryObject.Currency_name === 'undefined') {
        this.registerCurrency = this.toolService.selectedRegisterCountryObject['Currency name']
      } else {
        this.registerCurrency = this.toolService.selectedRegisterCountryObject['Currency_name']
      }
      if (typeof this.toolService.selectedBornCountryObject.Currency_name === 'undefined') {
        this.bornCurrency = this.toolService.selectedBornCountryObject['Currency name']
      } else {
        this.bornCurrency = this.toolService.selectedBornCountryObject['Currency_name']
      }
      this.phpToolService.getBornRegisterCountryData(this.bornCountry.countryid, this.registerCountry.countryid, this.translationService.selectedLang.code).subscribe(
        res => {
          this.data = true;
          this.toolService.selectedBornCountryObject = res[0];
          this.toolService.selectedRegisterCountryObject = res[1];
          this.phpToolService.getWealthData(this.bornCountry.countryid, this.registerCountry.countryid).subscribe(
            res => {
              this.bornCountryWelathMetaData = res[0];
              this.registerCountryWelathMetaData = res[1];
              this.phpToolService.getEcoStatusData(this.bornCountry.countryid, this.registerCountry.countryid).subscribe(
                res => {
                  this.statusMetadata = res[0];
                  this.gameSummeryService.ecoStatusData = this.statusMetadata;
                  this.registerStatusData = res[1];
                  this.phpToolService.getRegisterAndBornCountryGroupData(this.bornCountry.countryid, this.registerCountry.countryid, this.translationService.selectedLang.code).subscribe(
                    res => {
                      let data: any;
                      data = res;
                      let bornCountry = data['bornCountry']
                      let registerCountry = data['registerCountry']
                      this.bornCountryGroupList = Object.values(bornCountry);
                      this.registerCountryGroupList = Object.values(registerCountry);
                      this.phpToolService.getSdgCountryColour(this.bornCountry.countryid, this.registerCountry.countryid).subscribe(
                        (res: any) => {
                          this.sdgData = res;
                          this.phpToolService.getSDGInfo(this.bornCountry.countryid, true, this.translationService.selectedLang.code).subscribe((sdgInfo: any) => {
                            this.sdgDetails = sdgInfo['goals'];
                            this.bornSdgSubGoalDeatails = sdgInfo['targets'];
                            this.phpToolService.getSDGInfo(this.registerCountry.countryid, false, this.translationService.selectedLang.code).subscribe((res: any) => {
                              this.regSdgSubGoalDeatails = sdgInfo['targets'];
                              this.generateArrayForWelathGraph();
                              this.setSocialEconomicalStatus();
                              this.state = 'country-information';
                              this.modalWindowService.showWaitScreen = false;
                            })
                          })
                        })
                    })
                })
            })
        })
    }
  }


  getCountryMarkerData() {
    this.markers = [];
    this.mapZoom = 0;
    this.markers.push(
      {
        lat: this.bornCountry.lat,
        lng: this.bornCountry.lng,
        label: (this.bornCountry.country)
      }
    );

    this.markers.push(
      {
        lat: this.registerCountry.lat,
        lng: this.registerCountry.lng,
        label: this.registerCountry.country
      }
    );
    this.countryMarkerClick(this.markers[0]);
  }

  countryMarkerClick(countryObject: any) {
    this.mapZoom = 4;
    this.lat = countryObject.lat;
    this.lng = countryObject.lng;
  }

  backClick(state: any) {
    // if (this.gameSummeryService.showLifeDataListFlag) {
    //   this.modalWindowService.showCountryLearningTool = false;
    //   this.router.navigate(['/life-tool'])
    // }
    // else {
      if (state === 'wealth-grpah') {
        this.state = "country-political"
      }
      else if (state === 'country-map') {
        this.state = "country-information"
      }
      else if (state === 'broken-ladder') {
        this.state = 'family-income-grpah'
      }
      // else if (state === 'country-disparities') {
      //   this.state = "wealth-grpah"
      //   this.view = [320, 200];
      //   this.getDataForWelthGraph();
      //   this.getDataForWelthGraphCompariosion();
      // }
      else if (state === 'family-income-grpah') {
        this.state = "wealth-grpah"
        this.view = [320, 200];
        this.getDataForWelthGraph();
        this.getDataForWelthGraphCompariosion();
      }
      else if (state === 'family-income-table') {
        this.state = 'broken-ladder'
        this.calculateTableValues();
      }
      else if (state === 'country-health') {
        this.state = "country-map"
      }
      else if (state === 'country-social') {
        this.state = "country-health"
      }
      else if (state === 'country-economical') {
        this.state = "country-social"
      }
      else if (state === 'country-political') {
        this.state = "country-economical"
      }
      // else if (state === 'family-income-grpah') {
      //   this.state = "country-disparities"
      // }
      else if (state === 'family-income-table') {
        this.state = "family-income-grpah"
      }
      else if (state === 'country-group') {
        this.state = 'family-income-table'
      }
      else if (state === 'sdg') {
        this.state = 'country-group'
        this.countryGroup();
      }
      // else if (state === 'mapQue') {
      //   this.state = 'sdg'
      //   this.setSdgArray();
      // }
      // else if (state === 'FlagQue') {
      //   this.toolService.flagForMapQue = true;
      //   this.state = 'mapQue';
      // }
      // else if (state === 'feedback') {
      //   this.toolService.showQuetions = true;
      //   this.state = 'FlagQue';
      // }
      else if (state === 'reallives') {
        this.toolService.showQuetions = true;
        this.state = 'sdg';
      }
    // }

  }
  clickNext(state: any) {
    if (state === "country-information") {
      this.state = "country-map"
      this.getCountryMarkerData();
    }
    else if (state === "country-map") {
      this.state = "country-health"
    }
    else if (state === 'country-political') {
      this.state = "wealth-grpah"
      this.view = [320, 200];
      this.getDataForWelthGraph();
      this.getDataForWelthGraphCompariosion();
    }
    else if (state === 'country-health') {
      this.state = "country-social"
    }
    else if (state === 'country-social') {
      this.state = "country-economical"
    }
    else if (state === 'country-economical') {
      this.state = "country-political"
    }
    else if (state === 'wealth-grpah') {
      // this.state = "country-disparities"
      // this.view = [320, 200];
      // this.tabClickDemo();
      this.state = 'family-income-grpah';
      this.displayFamilyIncomeGraph();
    }
    else if (state === 'broken-ladder') {
      this.state = 'family-income-table'
    }
    // else if (state === 'country-disparities') {
    //   this.state = 'family-income-grpah';
    //   this.displayFamilyIncomeGraph();
    // }
    else if (state === 'family-income-grpah') {
      this.state = 'broken-ladder';
      this.calculateTableValues();
    }
    else if (state === 'family-income-table') {
      this.state = "country-group"
      this.countryGroup();
    }
    else if (state === 'country-group') {
      this.state = 'sdg'
      this.setSdgArray();
    }
    else if (state === 'sdg') {
      this.state = 'reallives'
      if (this.translationService.checkGameBuyOrNot) {
        this.msgText = this.translate.instant('gameLabel.For_more_experience_choose_a_country_to_play_in_RealLives')
      }
      else {
        this.msgText = this.translate.instant('gameLabel.buy')

      }
    }
    // else if (state === 'mapQue') {
    //   this.state = 'FlagQue'
    //   this.getFlagQuetion();
    // }
    // else if (state === 'FlagQue') {
    //   this.toolService.showQuetions = true;
    //   this.state = 'feedback'
    // }
    // else if (state === 'feedback') {

    //   this.state = 'reallives'
    // }

  }
  generateArrayForWelathGraph() {
    this.bornCountryArray = [
      { 'name': '10%', 'value': this.bornCountryWelathMetaData.per10 },
      { 'name': '20%', 'value': this.bornCountryWelathMetaData.per20 },
      { 'name': '30%', 'value': this.bornCountryWelathMetaData.per30 },
      { 'name': '40%', 'value': this.bornCountryWelathMetaData.per40 },
      { 'name': '50%', 'value': this.bornCountryWelathMetaData.per40 },
      { 'name': '60%', 'value': this.bornCountryWelathMetaData.per60 },
      { 'name': '70%', 'value': this.bornCountryWelathMetaData.per70 },
      { 'name': '80%', 'value': this.bornCountryWelathMetaData.per80 },
      { 'name': '90%', 'value': this.bornCountryWelathMetaData.per90 },
      { 'name': '99%', 'value': this.bornCountryWelathMetaData.per99 },
      { 'name': '100%', 'value': this.bornCountryWelathMetaData.per100 },
    ]
    this.registerCountryArray = [
      { 'name': '10%', 'value': this.registerCountryWelathMetaData.per10 },
      { 'name': '20%', 'value': this.registerCountryWelathMetaData.per20 },
      { 'name': '30%', 'value': this.registerCountryWelathMetaData.per30 },
      { 'name': '40%', 'value': this.registerCountryWelathMetaData.per40 },
      { 'name': '50%', 'value': this.registerCountryWelathMetaData.per40 },
      { 'name': '60%', 'value': this.registerCountryWelathMetaData.per60 },
      { 'name': '70%', 'value': this.registerCountryWelathMetaData.per70 },
      { 'name': '80%', 'value': this.registerCountryWelathMetaData.per80 },
      { 'name': '90%', 'value': this.registerCountryWelathMetaData.per90 },
      { 'name': '99%', 'value': this.registerCountryWelathMetaData.per99 },
      { 'name': '100%', 'value': this.registerCountryWelathMetaData.per100 },
    ]

  }
  getDataForWelthGraph() {
    this.view = [600, 300];
    this.multi2 = [];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabel = this.translate.instant('gameLabel.Population(%)');
    this.showYAxisLabel = true;
    this.yAxisLabel = this.translate.instant('gameLabel.Wealth');
    this.colorSchemeBorn = {
      domain: ['#0000FF', '#A10A28']
    };
    this.multi2.push({
      name: this.translate.instant('gameLabel.Equality_Line'),
      series: []
    });
    this.multi2.push({
      name: this.toolService.selectedBornCountryObject.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi2[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.bornCountryArray.length; i++) {
      if (this.bornCountryArray[i].name !== "99%") {
        this.multi2[1].series.push(this.bornCountryArray[i]);
      }
    }
    Object.assign(this.multi2);
    let v2, v3, v4, g3: any, g5: any;
    if (this.toolService.selectedBornCountryObject.Population > 100000) {
      g3 = (this.bornCountryWelathMetaData.per50 * 100).toFixed(1);
      g5 = ((this.bornCountryWelathMetaData.per100 * 100) - (this.bornCountryWelathMetaData.per99 * 100)).toFixed(1);
      v2 = (((this.toolService.selectedBornCountryObject.Population * (50 / 100)))).toFixed(2);
      v3 = ((this.toolService.selectedBornCountryObject.Population * (1 / 100))).toFixed(2)
      v4 = (g5 / g3).toFixed(2);
    }
    else {
      g3 = (this.bornCountryWelathMetaData.per50 * 100).toFixed(2);
      g5 = ((this.bornCountryWelathMetaData.per100 * 100) - (this.bornCountryWelathMetaData.per99 * 100)).toFixed(2);
      v2 = (((this.toolService.selectedBornCountryObject.Population * (50 / 100)))).toFixed(2);
      v3 = ((this.toolService.selectedBornCountryObject.Population * (1 / 100))).toFixed(2)
      v4 = (g5 / g3).toFixed(2);
    }
    v2 = this.commonService.checkNumberFormat(v2);
    v3 = this.commonService.checkNumberFormat(v3);
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceBorn3 = s;
      })
  }
  getDataForWelthGraphCompariosion() {
    this.multi3 = [];
    this.view1 = [700, 400];
    this.showXAxis = true;
    this.showYAxis = true;
    this.gradient = false;
    this.showLegend = true;
    this.showXAxisLabel = true;
    this.xAxisLabelReg = this.translate.instant('gameLabel.Population(%)');
    this.showYAxisLabel = true;
    this.yAxisLabelReg = this.translate.instant('gameLabel.Wealth');
    this.colorSchemeBorn = {
      domain: ['#0000FF', '#A10A28']
    };

    this.multi3.push({
      name: this.translate.instant('gameLabel.Equality_Line'),
      series: []
    });
    this.multi3.push({
      name: this.toolService.selectedRegisterCountryObject.country,
      series: []
    });
    for (let i = 0; i < this.constantGraphValueArray.length; i++) {
      this.multi3[0].series.push(this.constantGraphValueArray[i]);
    }
    for (let i = 0; i < this.registerCountryArray.length; i++) {
      if (this.registerCountryArray[i].name !== "99%") {
        this.multi3[1].series.push(this.registerCountryArray[i]);
      }
    }
    Object.assign(this.multi3);
    let v2, v3, v4, g3: any, g5: any;
    g3 = (this.registerCountryWelathMetaData.per50 * 100).toFixed(1);
    g5 = ((this.registerCountryWelathMetaData.per100 * 100) - (this.registerCountryWelathMetaData.per99 * 100)).toFixed(1);
    v2 = (((this.toolService.selectedRegisterCountryObject.Population * (50 / 100)))).toFixed(2);
    v3 = ((this.toolService.selectedRegisterCountryObject.Population * (1 / 100))).toFixed(2)
    v4 = (g5 / g3).toFixed(2);
    v2 = this.commonService.checkNumberFormat(v2)
    v3 = this.commonService.checkNumberFormat(v3)
    this.translate.get('gameLabel.wealthSen1', { v2: v2, g3: g3 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg1 = s;
      })
    this.translate.get('gameLabel.wealthSen2', { v3: v3, g5: g5 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg2 = s;
      })
    this.translate.get('gameLabel.wealthSen3', { v4: v4 }).subscribe(
      (s: String) => {
        this.wealthGraphSentenceReg3 = s;
      })
  }
  yAxisTickFormatting(value: any) {
    return this.percentTickFormatting(value);
  }

  percentTickFormatting(val: any): string {
    return val.toLocaleString() + '%';
  }
  checkColourPatternForGraph(x: any, y: any, classIndex: any) {
    if (classIndex === 0) {
      let val = this.checkCssForSpecific(x, y)
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
    else if (classIndex === 1) {
      let val = this.checkCss(x, y);
      if (val === "bad_ind") {
        this.colorScheme = {
          domain: ['#A10A28', '#5AA454']
        };
      }
      else if (val === "good_ind") {
        this.colorScheme = {
          domain: ['#5AA454', '#A10A28']
        };
      }
      else {
        this.colorScheme = {
          domain: ['#00FFFF', '#00FFFF']
        };
      }
    }
  }
  generateStringValue(Bi: any, Ri: any) {
    let B = parseFloat(Bi);
    let R = parseFloat(Ri);
    if (B < R) {
      this.stringReturnValue = ((100 * ((R - B) / R))).toFixed(2);
      this.valurForHappiAndCorr = (R - B).toFixed(2);
      this.valueForHdi = (100 * (R - B) / R).toFixed(2)
      this.poupulationValue = (((B) / R) * 100).toFixed(2)
    }
    else if (B > R) {
      this.stringReturnValue = (B / R).toFixed(2)
      this.valurForHappiAndCorr = (B - R).toFixed(2);
      this.valueForHdi = ((B / R - 1) * 100).toFixed(2)
      this.poupulationValue = (B / R).toFixed(2);
    }
    else {
      this.stringReturnValue = 0
      this.valurForHappiAndCorr = 0
      this.valueForHdi = 0;
      this.poupulationValue = 0;
    }
    return this.stringReturnValue
  }

  generateStringSubString(B: any, R: any) {
    B = parseFloat(B);
    R = parseFloat(R);
    if (B < R) {
      this.stringRetutnText = this.translate.instant('gameLabel.%_less_likely');
      this.bithString = this.translate.instant('gameLabel.%_fewer');
      this.deathString = this.translate.instant('gameLabel.%_less_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.%_less');
      this.happinessString = this.translate.instant('gameLabel.ranks_above');
      this.giniIndexString = this.translate.instant('gameLabel.lower');
      this.sexRatio = this.translate.instant('gameLabel.less');
      this.accesEleString = this.translate.instant('gameLabel.%_less');
      this.populationString = "%"
    }
    else if (B > R) {
      this.stringRetutnText = this.translate.instant('gameLabel.times_more_likely');
      this.bithString = this.translate.instant('gameLabel.times_more');
      this.deathString = this.translate.instant('gameLabel.times_more_risk');
      this.primarySchoolString = this.translate.instant('gameLabel.times_more');
      this.happinessString = this.translate.instant('gameLabel.ranks_below');
      this.giniIndexString = this.translate.instant('gameLabel.higher');
      this.sexRatio = this.translate.instant('gameLabel.more');
      this.accesEleString = this.translate.instant('gameLabel.times_more');
      this.populationString = this.translate.instant('gameLabel.times');

    }
    else {
      this.populationString = this.translate.instant('gameLabel.almost_equal');
      this.stringRetutnText = this.translate.instant('gameLabel.as_good_as');
    }
    return this.stringRetutnText;

  }

  getSexRationValue(B: any, R: any) {
    B = parseFloat(B);
    R = parseFloat(R);
    this.sexRatioValue = Math.abs((R - B) * 1000).toFixed(2);
  }

  checkSentenceGenaerateOrNot(B: any, R: any) {
    if (B === 0 || R === 0 || B === "NA" || R === "NA") {
      this.senntenceGeneratedFlag = true;
    }
    else {
      this.senntenceGeneratedFlag = false;
    }
  }

  onButtonClickGraphChange(x: any, y: any, fieldName: any, classIndex: any) {
    if (fieldName === 'Population') {
      this.xAxisLabel = this.translate.instant('gameLabel.Population');
      this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (this.poupulationValue == 0) {
        this.populationString = this.translate.instant('gameLabel.almost_equal');

        this.translate.get('gameLabel.populationCD1', { country: this.toolService.selectedBornCountryObject.country, populationString: this.populationString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // Population of {{country}} is {{populationString}} that of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.toolService.selectedBornCountryObject.country+" is "+this.populationString+" that of "+this.toolService.selectedRegisterCountryObject.country+"'s population.";
      }
      else {
        this.translate.get('gameLabel.populationCD2', { country: this.toolService.selectedBornCountryObject.country, poupulationValue: this.poupulationValue, populationString: this.populationString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        //     Population of {{country}} is {{poupulationValue}} {{populationString}}  of {{rcountry}}'s population.
        // this.sentenceTextWindow1="Population of " +this.toolService.selectedBornCountryObject.country+" is "+this.poupulationValue+this.populationString+" of "+this.toolService.selectedRegisterCountryObject.country+"'s population.";
      }
      this.colorScheme = {
        domain: ['#00FFFF', '#00FFFF']
      };
    }
    else if (fieldName === "Sex Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Sex_Ratio');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.getSexRationValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.sexRatioValue) === 0) {
          this.sexRatio = this.translate.instant('gameLabel.as_good_as')
          this.translate.get('gameLabel.sexRationDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If  "+this.toolService.selectedBornCountryObject.country+" is your home country, at birth, there will be equal, boys per 1000 girls, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.sexRationDC2', { country: this.toolService.selectedBornCountryObject.country, sexRatioValue: Math.round(this.sexRatioValue), sexRatio: this.sexRatio, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, at birth, there will be , {{sexRatioValue}} {{sexRatio}} boys per 1000 girls than {{rcountry}}.
          // this.sentenceTextWindow1="If  "+this.toolService.selectedBornCountryObject.country+" is your home country, at birth, there will be , "+ Math.round(this.sexRatioValue) +" "+this.sexRatio+" boys per 1000 girls than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Birth Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Birth_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      let value = this.generateStringValue(x, y);
      this.generateStringSubString(x, y);
      if (Math.round(value) === 0) {
        this.translate.get('gameLabel.BirthRateDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have equal babies, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
      }
      else {
        this.translate.get('gameLabel.BirthRateDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, bithString: this.bithString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
          (str) => {
            this.sentenceTextWindow1 = str;
          })
        // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have "+ value + this.bithString+ " babies than " +this.toolService.selectedRegisterCountryObject.country +".";
      }
      this.checkColourPatternForGraph(x, y, classIndex);

    }
    else if (fieldName === "Death Ratio") {
      this.xAxisLabel = this.translate.instant('gameLabel.Death_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.DeathRatioDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have equal risk of dying, compared to {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have equal risk of dying, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.DeathRatioDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, deathString: this.deathString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{deathString}} of dying than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have "+ value + this.deathString+ " of dying than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Infant Mortality Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Infant_Mortality_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.InfantMortalityRateDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are equally likely to die in infancy, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.InfantMortalityRateDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you are {{value}} {{stringRetutnText}} to die in infancy than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are "+ value + this.stringRetutnText+ " to die in infancy than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Primary School") {
      this.xAxisLabel = this.translate.instant('gameLabel.Primary_School_Enrollment');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.primarySchoolDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have equal access to Primary School, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        } else {
          this.translate.get('gameLabel.primarySchoolDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // If {{country}} is your home country, you will have {{value}} {{primarySchoolString}} access to Primary School than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will have "+ value +this.primarySchoolString+ " access to Primary School than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "UnEmployment Rate") {
      this.xAxisLabel = this.translate.instant('gameLabel.Unemployment_Rate');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.UnEmploymentRateDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are equally likely to be unemployed, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.UnEmploymentRateDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, stringRetutnText: this.stringRetutnText, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // "If {{country}} is your home country, you will spend {{value}} {{primarySchoolString}} money on health care than {{rcountry}}.
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you are "+ value +this.stringRetutnText+ " to be unemployed than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Health Exp. per capita") {
      this.xAxisLabel = this.translate.instant('gameLabel.Health_Exp_per_capita');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.healthExpDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will spend equal money on health care, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.healthExpDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will spend "+ value +this.primarySchoolString+ " money on health care than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "PPP") {
      this.xAxisLabel = this.translate.instant('gameLabel.PPP');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {

          this.translate.get('gameLabel.pppDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will earn equal money, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.pppDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, primarySchoolString: this.primarySchoolString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="If "+this.toolService.selectedBornCountryObject.country+" is your home country, you will earn "+ value +this.primarySchoolString+ " money than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Happiness Score") {
      this.xAxisLabel = this.translate.instant('gameLabel.Happiness_Rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.HappinessScoreDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" is almost equal in happiness index, compared to "+this.toolService.selectedRegisterCountryObject.country ;
        } else {
          this.translate.get('gameLabel.HappinessScoreDC2', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.toolService.selectedRegisterCountryObject.country +","+this.toolService.selectedBornCountryObject.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in happiness index.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Corruption") {
      this.xAxisLabel = this.translate.instant('gameLabel.Corruption');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {
          this.translate.get('gameLabel.CorruptionDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" is equal in the list of the most corrupt countries, compared to "+this.toolService.selectedRegisterCountryObject.country;
        }
        else {
          this.translate.get('gameLabel.CorruptionDC2', { rcountry: this.toolService.selectedRegisterCountryObject.country, country: this.toolService.selectedBornCountryObject.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.toolService.selectedRegisterCountryObject.country +","+this.toolService.selectedBornCountryObject.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the list of the most corrupt countries.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Sdgi Rank") {
      this.xAxisLabel = this.translate.instant('gameLabel.SDG_rank');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valurForHappiAndCorr) === 0) {

          this.translate.get('gameLabel.SdgiScoreDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            }) // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" is equal in the SDG rank, compared to " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.SdgiScoreDC2', { country: this.toolService.selectedBornCountryObject.country, valurForHappiAndCorr: Math.round(this.valurForHappiAndCorr), happinessString: this.happinessString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="Compared to "+this.toolService.selectedRegisterCountryObject.country +","+this.toolService.selectedBornCountryObject.country+" is "+ Math.round(this.valurForHappiAndCorr) +" "+this.happinessString+ " in the SDG rank than " +this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Hdi") {
      this.xAxisLabel = this.translate.instant('gameLabel.HDI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(this.valueForHdi) === 0) {
          this.translate.get('gameLabel.hdiDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has  equal HDI, compared to "+ this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.hdiDC2', { country: this.toolService.selectedBornCountryObject.country, valueForHdi: this.valueForHdi, giniIndexString: this.giniIndexString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has  "+ this.valueForHdi +"% "+this.giniIndexString+ "  HDI than "+ this.toolService.selectedRegisterCountryObject.country +".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Gini") {
      this.xAxisLabel = this.translate.instant('gameLabel.GINI');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if ((x - y) === 0 || (y - x) === 0) {
          this.translate.get('gameLabel.giniDc1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has equal income inequality among rich and poor.";
        }
        else {
          this.translate.get('gameLabel.giniDc2', { country: this.toolService.selectedBornCountryObject.country, giniIndexString: this.giniIndexString }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1=this.toolService.selectedBornCountryObject.country+" has  "+ this.giniIndexString + " income inequality among rich and poor.";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else if (fieldName === "Access to Electricity") {
      this.xAxisLabel = this.translate.instant('gameLabel.Electricity_Access');
      this.checkSentenceGenaerateOrNot(x, y);
      if (!this.senntenceGeneratedFlag) {
        let value = this.generateStringValue(x, y);
        this.generateStringSubString(x, y);
        if (Math.round(value) === 0) {
          this.translate.get('gameLabel.electricityDC1', { country: this.toolService.selectedBornCountryObject.country, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.toolService.selectedBornCountryObject.country+" you will access equal electricity, compared to "+ this.toolService.selectedRegisterCountryObject.country +".";
        }
        else {
          this.translate.get('gameLabel.electricityDC2', { country: this.toolService.selectedBornCountryObject.country, value: value, accesEleString: this.accesEleString, rcountry: this.toolService.selectedRegisterCountryObject.country }).subscribe(
            (str) => {
              this.sentenceTextWindow1 = str;
            })
          // this.sentenceTextWindow1="In "+this.toolService.selectedBornCountryObject.country+" you will have  "+ value +this.accesEleString+ " access to electricity than " +this.toolService.selectedRegisterCountryObject.country+".";
        }
        this.checkColourPatternForGraph(x, y, classIndex);
      }
      else {
        this.sentenceTextWindow1 = 'N/A'
      }
    }
    else {
      this.sentenceTextWindow1 = "N/A";
      this.checkColourPatternForGraph(x, y, classIndex);
    }

    this.single = [
      {
        name: this.toolService.selectedBornCountryObject.country,
        value: x,
      },
      {
        name: this.toolService.selectedRegisterCountryObject.country,
        value: y,
      },
    ];
    Object.assign(this.single);
  }

  tabClickDemo() {
    this.activeClassForDemo = "nav-item nav-link active"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = true;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.toolService.selectedBornCountryObject.Population, this.toolService.selectedRegisterCountryObject.Population, 'Population', 3)
    this.single = [
      {
        name: this.toolService.selectedBornCountryObject.country,
        value: this.toolService.selectedBornCountryObject.Population,
      },
      {
        name: this.toolService.selectedRegisterCountryObject.country,
        value: this.toolService.selectedRegisterCountryObject.Population,
      },
    ];
    Object.assign(this.single);

  }
  tabClickperCapita() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link active";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = true;
    this.onCLickLabourForce = false;
    this.onClickIndexRating = false;
    this.onButtonClickGraphChange(this.toolService.selectedBornCountryObject.PrimarySchool, this.toolService.selectedRegisterCountryObject.PrimarySchool, 'Primary School', 1);
    this.single = [
      {
        name: this.toolService.selectedBornCountryObject.country,
        value: this.toolService.selectedBornCountryObject.PrimarySchool,
      },
      {
        name: this.toolService.selectedRegisterCountryObject.country,
        value: this.toolService.selectedRegisterCountryObject.PrimarySchool,
      },
    ];
    Object.assign(this.single);
  }
  tabClickLFO() {
    this.view1 = [320, 200]
    let num1 = this.toolService.selectedBornCountryObject.Agriculture;
    let num2 = this.toolService.selectedBornCountryObject.Services;
    let num3 = this.toolService.selectedBornCountryObject.industry;
    let num4 = this.toolService.selectedRegisterCountryObject.Agriculture;
    let num5 = this.toolService.selectedRegisterCountryObject.Services;
    let num6 = this.toolService.selectedRegisterCountryObject.industry;
    let largest;
    let occupation;
    let roccupation;
    if (num1 >= num2 && num1 >= num3) {
      largest = num1.toFixed(2);
      occupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num2 >= num1 && num2 >= num3) {
      largest = num2.toFixed(2);
      occupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      largest = num3.toFixed(2);
      occupation = this.translate.instant('gameLabel.Industry1');
    }

    if (num4 >= num5 && num4 >= num6) {
      largest = num1.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Agriculture1');
    }
    else if (num5 >= num4 && num5 >= num6) {
      largest = num2.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Service1');
    }
    else {
      largest = num3.toFixed(2);
      roccupation = this.translate.instant('gameLabel.Industry1');
    }
    this.translate.get('gameLabel.LEbourForce1', { country: this.toolService.selectedBornCountryObject.country, occupation: occupation }).subscribe(
      (str) => {
        this.labourForceString1 = str;
      })
    this.translate.get('gameLabel.LEbourForce2', { roccupation: roccupation, rcountry: this.toolService.selectedRegisterCountryObject.country, largest: largest, country: this.toolService.selectedBornCountryObject.country }).subscribe(
      (str) => {
        this.labourForceString2 = str;
      })
    // this.labourForceString1="In "+this.toolService.selectedBornCountryObject.country+", most likely your occupation will be in the "+occupation+ " Sector. "
    // this.labourForceString2="If you are working in " + roccupation +" Sector in "+this.toolService.selectedRegisterCountryObject.country +", you will have "+largest+"% jobs available in " +this.toolService.selectedBornCountryObject.country+"."
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link active";
    this.activeClassForIndex = "nav-item nav-link";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickSdgData = false;
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = true;
    this.onClickIndexRating = false;
    this.xAxisLabel = this.translate.instant('gameLabel.Occupation')
    this.colorScheme = {
      domain: ['#5AA454', '#A10A28']
    };
    this.multi = [
      {
        name: this.toolService.selectedBornCountryObject.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.toolService.selectedBornCountryObject.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.toolService.selectedBornCountryObject.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.toolService.selectedBornCountryObject.industry,
          },
        ],
      },

      {
        name: this.toolService.selectedRegisterCountryObject.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Agriculture1'),
            value: this.toolService.selectedRegisterCountryObject.Agriculture,
          },
          {
            name: this.translate.instant('gameLabel.Service1'),
            value: this.toolService.selectedRegisterCountryObject.Services,
          },
          {
            name: this.translate.instant('gameLabel.Industry1'),
            value: this.toolService.selectedRegisterCountryObject.industry,
          },
        ],
      },
    ];
    Object.assign(this.multi);

  }
  tabClickIndex() {
    this.activeClassForDemo = "nav-item nav-link"
    this.activeClassForPerCapita = "nav-item nav-link";
    this.activeClassForLFO = "nav-item nav-link";
    this.activeClassForIndex = "nav-item nav-link active";
    this.activeClassForSdg = "nav-item nav-link";
    this.onClickDemoGraphic = false;
    this.onClickPerCapita = false;
    this.onCLickLabourForce = false;
    this.onClickSdgData = false;
    this.onClickIndexRating = true;
    this.onButtonClickGraphChange(this.toolService.selectedBornCountryObject.ppp, this.toolService.selectedRegisterCountryObject.ppp, 'PPP', 1);
    this.single = [
      {
        name: this.toolService.selectedBornCountryObject.country,
        value: this.toolService.selectedBornCountryObject.ppp,
      },
      {
        name: this.toolService.selectedRegisterCountryObject.country,
        value: this.toolService.selectedRegisterCountryObject.ppp,
      },
    ];
    Object.assign(this.single);


  }

  checkPopulationFormat(Population: any) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = (Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }
  checkCss(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }
  checkCssForSpecific(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }
  checkCss1(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }
  checkCssForSpecific1(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }
  onActivate(data: any): void {
    // console.log('Activate', JSON.parse(JSON.stringify(data)));
  }
  onDeactivate(data: any): void {
    // console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
  setSocialEconomicalStatus() {
    //regi
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    this.firstValueReg = (f3R);
    this.secondValueReg = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    this.thirdValueReg = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    this.fourthValueReg = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    //end
    this.commonService.calculateWealthStatusForTool(this.bornCountryWelathMetaData, this.toolService.selectedBornCountryObject);
    this.commonService.calculateIncameStatusForTool(this.statusMetadata, this.toolService.selectedRegisterCountryObject);
  }
  displayFamilyIncomeGraph() {
    let subString = this.translate.instant('gameLabel.a');
    let s = "";
    let registerIncome = (this.toolService.selectedRegisterCountryObject.ProdperCapita * this.toolService.selectedRegisterCountryObject.AverageFamilyCount);
    let countryIncome = (this.toolService.selectedBornCountryObject.ProdperCapita) * this.toolService.selectedBornCountryObject.AverageFamilyCount;
    let value2 = Math.round(this.statusMetadata.chanceOfPoor * 100);
    let value3 = Math.round(this.registerStatusData.chanceOfPoor * 100);
    if (!this.phpToolService.flagForPhpToolClick) {
      if (!this.phpToolService.flagForPhpToolClick) {
        if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 0) {
          this.statusOneClass = "table-bordered-red"
        }
        else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 1) {
          this.statusTwoClass = "table-bordered-yellow"
        }
        else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 2) {
          this.statusThreeClass = "table-bordered-orange"
        }
        else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 3) {
          this.statusFourClass = "table-bordered-blue"
        }
        else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 4) {
          this.statusFiveClass = "table-bordered-green"
        }
      }
      this.translate.get('gameLabel.load_eco_status_stmt', { subString: subString, status: this.commonService.getSatusString(this.allPerson[this.constantService.FAMILY_SELF].current_economical_status) }).subscribe(
        (str1: String) => {
          this.incomeGraphStringOne = str1;
        })
      this.translate.get('gameLabel.incomeGraphSen2', { country: this.toolService.selectedRegisterCountryObject.country, value2: value2.toFixed(0), s: s }).subscribe(
        (str1: String) => {
          this.incomeGraphStringTwo = str1;
        })
      this.translate.get('gameLabel.incomeGraphSen3', { value3: value3.toFixed(0) }).subscribe(
        (str1: String) => {
          this.incomeGraphStringThree = str1;
        })

    }
    this.multi1 = [
      {
        name: this.toolService.selectedBornCountryObject.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(countryIncome))
          }
        ]
      },
      {
        name: this.toolService.selectedRegisterCountryObject.country,
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(registerIncome))
          }
        ]
      },
      {
        name: this.translate.instant('gameLabel.World'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: (Math.round(12000))
          }
        ]
      },
    ];
  }
  onSelect(event: any) {
  }
  knowMore(country: any) {
    this.toolService.konwMoreCountry = country;
    this.toolService.showKnowMore = true;
  }
  openIframeWindow(link: any, title: any) {
    let title1 = "'" + title + "'";
    this.translate.get('gameLabel.country_group_title', { gName: title1 }).subscribe(
      (str) => {
        this.countryGroupTitle = str;
      })
    this.countryGroupIframeFlag = true;
    this.webLink = link;
  }
  closeIframWindow() {
    this.countryGroupIframeFlag = false;
  }
  setSdgArray() {
    let born=null,register=null;
    if(this.sdgData[0]!=null){
      born = this.sdgData[0]
    }
    if(this.sdgData[1]!=null){
      register = this.sdgData[1]

    }
    if(born!==null && register!==null){
    this.sdgDispalyArrayBorn = [{
      sdgName: "SDG 1",
      dispalyName: this.translate.instant('gameLabel.sdg1'),
      bcolour: born.one,
      rcolour: register.one
    }, {
      sdgName: "SDG 2",
      dispalyName: this.translate.instant('gameLabel.sdg2'),
      bcolour: born.two,
      rcolour: register.two
    }, {
      sdgName: "SDG 3",
      dispalyName: this.translate.instant('gameLabel.sdg3'),
      bcolour: born.three,
      rcolour: register.three

    }, {
      sdgName: "SDG 4",
      dispalyName: this.translate.instant('gameLabel.sdg4'),
      bcolour: born.four,
      rcolour: register.four

    }, {
      sdgName: "SDG 5",
      dispalyName: this.translate.instant('gameLabel.sdg5'),
      bcolour: born.five,
      rcolour: register.five

    }, {
      sdgName: "SDG 6",
      dispalyName: this.translate.instant('gameLabel.sdg6'),
      bcolour: born.six,
      rcolour: register.six
    }, {
      sdgName: "SDG 7",
      dispalyName: this.translate.instant('gameLabel.sdg7'),
      bcolour: born.seven,
      rcolour: register.seven
    }, {
      sdgName: "SDG 8",
      dispalyName: this.translate.instant('gameLabel.sdg8'),
      bcolour: born.eight,
      rcolour: register.eight
    }, {
      sdgName: "SDG 9",
      dispalyName: this.translate.instant('gameLabel.sdg9'),
      bcolour: born.nine,
      rcolour: register.nine

    }, {
      sdgName: "SDG 10",
      dispalyName: this.translate.instant('gameLabel.sdg10'),
      bcolour: born.ten,
      rcolour: register.ten

    }, {
      sdgName: "SDG 11",
      dispalyName: this.translate.instant('gameLabel.sdg11'),
      bcolour: born.eleven,
      rcolour: register.eleven

    }, {
      sdgName: "SDG 12",
      dispalyName: this.translate.instant('gameLabel.sdg12'),
      bcolour: born.twelve,
      rcolour: register.twelve

    }, {
      sdgName: "SDG 13",
      dispalyName: this.translate.instant('gameLabel.sdg13'),
      bcolour: born.thirteen,
      rcolour: register.thirteen

    }, {
      sdgName: "SDG 14",
      dispalyName: this.translate.instant('gameLabel.sdg14'),
      bcolour: born.fourteen,
      rcolour: register.fourteen

    }, {
      sdgName: "SDG 15",
      dispalyName: this.translate.instant('gameLabel.sdg15'),
      bcolour: born.fifteen,
      rcolour: register.fifteen

    }, {
      sdgName: "SDG 16",
      dispalyName: this.translate.instant('gameLabel.sdg16'),
      bcolour: born.sixteen,
      rcolour: register.sixteen

    }, {
      sdgName: "SDG 17",
      dispalyName: this.translate.instant('gameLabel.sdg17'),
      bcolour: born.seventeen,
      rcolour: register.seventeen

    }
    ]
  }
  else if(born===null &&register!==null){
    this.sdgDispalyArrayBorn = [{
      sdgName: "SDG 1",
      dispalyName: this.translate.instant('gameLabel.sdg1'),
      bcolour:"",
      rcolour: register.one
    }, {
      sdgName: "SDG 2",
      dispalyName: this.translate.instant('gameLabel.sdg2'),
      bcolour:"",
      rcolour: register.two
    }, {
      sdgName: "SDG 3",
      dispalyName: this.translate.instant('gameLabel.sdg3'),
      bcolour: "",
      rcolour: register.three

    }, {
      sdgName: "SDG 4",
      dispalyName: this.translate.instant('gameLabel.sdg4'),
      bcolour:"",
      rcolour: register.four

    }, {
      sdgName: "SDG 5",
      dispalyName: this.translate.instant('gameLabel.sdg5'),
      bcolour: "",
      rcolour: register.five

    }, {
      sdgName: "SDG 6",
      dispalyName: this.translate.instant('gameLabel.sdg6'),
      bcolour: "",
      rcolour: register.six
    }, {
      sdgName: "SDG 7",
      dispalyName: this.translate.instant('gameLabel.sdg7'),
      bcolour:"",
      rcolour: register.seven
    }, {
      sdgName: "SDG 8",
      dispalyName: this.translate.instant('gameLabel.sdg8'),
      bcolour: "",
      rcolour: register.eight
    }, {
      sdgName: "SDG 9",
      dispalyName: this.translate.instant('gameLabel.sdg9'),
      bcolour:"",
      rcolour: register.nine

    }, {
      sdgName: "SDG 10",
      dispalyName: this.translate.instant('gameLabel.sdg10'),
      bcolour: "",
      rcolour: register.ten

    }, {
      sdgName: "SDG 11",
      dispalyName: this.translate.instant('gameLabel.sdg11'),
      bcolour: "",
      rcolour: register.eleven

    }, {
      sdgName: "SDG 12",
      dispalyName: this.translate.instant('gameLabel.sdg12'),
      bcolour: "",
      rcolour: register.twelve

    }, {
      sdgName: "SDG 13",
      dispalyName: this.translate.instant('gameLabel.sdg13'),
      bcolour: "",
      rcolour: register.thirteen

    }, {
      sdgName: "SDG 14",
      dispalyName: this.translate.instant('gameLabel.sdg14'),
      bcolour: "",
      rcolour: register.fourteen

    }, {
      sdgName: "SDG 15",
      dispalyName: this.translate.instant('gameLabel.sdg15'),
      bcolour: "",
      rcolour: register.fifteen

    }, {
      sdgName: "SDG 16",
      dispalyName: this.translate.instant('gameLabel.sdg16'),
      bcolour: "",
      rcolour: register.sixteen

    }, {
      sdgName: "SDG 17",
      dispalyName: this.translate.instant('gameLabel.sdg17'),
      bcolour: "",
      rcolour: register.seventeen

    }
    ]
  }
  else if(born!==null &&register===null){
    this.sdgDispalyArrayBorn = [{
      sdgName: "SDG 1",
      dispalyName: this.translate.instant('gameLabel.sdg1'),
      bcolour: born.one,
      rcolour: ""
    }, {
      sdgName: "SDG 2",
      dispalyName: this.translate.instant('gameLabel.sdg2'),
      bcolour: born.two,
      rcolour:""
    }, {
      sdgName: "SDG 3",
      dispalyName: this.translate.instant('gameLabel.sdg3'),
      bcolour: born.three,
      rcolour: ""

    }, {
      sdgName: "SDG 4",
      dispalyName: this.translate.instant('gameLabel.sdg4'),
      bcolour: born.four,
      rcolour: ""

    }, {
      sdgName: "SDG 5",
      dispalyName: this.translate.instant('gameLabel.sdg5'),
      bcolour: born.five,
      rcolour: ""

    }, {
      sdgName: "SDG 6",
      dispalyName: this.translate.instant('gameLabel.sdg6'),
      bcolour: born.six,
      rcolour: ""
    }, {
      sdgName: "SDG 7",
      dispalyName: this.translate.instant('gameLabel.sdg7'),
      bcolour: born.seven,
      rcolour: ""
    }, {
      sdgName: "SDG 8",
      dispalyName: this.translate.instant('gameLabel.sdg8'),
      bcolour: born.eight,
      rcolour:""
    }, {
      sdgName: "SDG 9",
      dispalyName: this.translate.instant('gameLabel.sdg9'),
      bcolour: born.nine,
      rcolour: register.nine

    }, {
      sdgName: "SDG 10",
      dispalyName: this.translate.instant('gameLabel.sdg10'),
      bcolour: born.ten,
      rcolour: ""

    }, {
      sdgName: "SDG 11",
      dispalyName: this.translate.instant('gameLabel.sdg11'),
      bcolour: born.eleven,
      rcolour: ""

    }, {
      sdgName: "SDG 12",
      dispalyName: this.translate.instant('gameLabel.sdg12'),
      bcolour: born.twelve,
      rcolour: ""

    }, {
      sdgName: "SDG 13",
      dispalyName: this.translate.instant('gameLabel.sdg13'),
      bcolour: born.thirteen,
      rcolour: ""
    }, {
      sdgName: "SDG 14",
      dispalyName: this.translate.instant('gameLabel.sdg14'),
      bcolour: born.fourteen,
      rcolour:""

    }, {
      sdgName: "SDG 15",
      dispalyName: this.translate.instant('gameLabel.sdg15'),
      bcolour: born.fifteen,
      rcolour: ""

    }, {
      sdgName: "SDG 16",
      dispalyName: this.translate.instant('gameLabel.sdg16'),
      bcolour: born.sixteen,
      rcolour: ""

    }, {
      sdgName: "SDG 17",
      dispalyName: this.translate.instant('gameLabel.sdg17'),
      bcolour: born.seventeen,
      rcolour:""

    }
    ]
  }
else{
  this.sdgDispalyArrayBorn = [{
    sdgName: "SDG 1",
    dispalyName: this.translate.instant('gameLabel.sdg1'),
    bcolour:"",
    rcolour: ""
  }, {
    sdgName: "SDG 2",
    dispalyName: this.translate.instant('gameLabel.sdg2'),
    bcolour:"",
    rcolour:""
  }, {
    sdgName: "SDG 3",
    dispalyName: this.translate.instant('gameLabel.sdg3'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 4",
    dispalyName: this.translate.instant('gameLabel.sdg4'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 5",
    dispalyName: this.translate.instant('gameLabel.sdg5'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 6",
    dispalyName: this.translate.instant('gameLabel.sdg6'),
    bcolour: "",
    rcolour: ""
  }, {
    sdgName: "SDG 7",
    dispalyName: this.translate.instant('gameLabel.sdg7'),
    bcolour: "",
    rcolour: ""
  }, {
    sdgName: "SDG 8",
    dispalyName: this.translate.instant('gameLabel.sdg8'),
    bcolour: "",
    rcolour:""
  }, {
    sdgName: "SDG 9",
    dispalyName: this.translate.instant('gameLabel.sdg9'),
    bcolour: "",
    rcolour: register.nine

  }, {
    sdgName: "SDG 10",
    dispalyName: this.translate.instant('gameLabel.sdg10'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 11",
    dispalyName: this.translate.instant('gameLabel.sdg11'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 12",
    dispalyName: this.translate.instant('gameLabel.sdg12'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 13",
    dispalyName: this.translate.instant('gameLabel.sdg13'),
    bcolour: "",
    rcolour: ""
  }, {
    sdgName: "SDG 14",
    dispalyName: this.translate.instant('gameLabel.sdg14'),
    bcolour:"",
    rcolour:""

  }, {
    sdgName: "SDG 15",
    dispalyName: this.translate.instant('gameLabel.sdg15'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 16",
    dispalyName: this.translate.instant('gameLabel.sdg16'),
    bcolour: "",
    rcolour: ""

  }, {
    sdgName: "SDG 17",
    dispalyName: this.translate.instant('gameLabel.sdg17'),
    bcolour: "",
    rcolour:""

  }
  ]
}
  }
  countryGroup() {
    let result = this.bornCountryGroupList.filter((o: { gpName: any }) => this.registerCountryGroupList.some(({ gpName }: { gpName: any }) => o.gpName === gpName));
    let total = this.bornCountryGroupList.length + this.registerCountryGroupList.length;
    let unique = total - result.length;
    this.groupIndex = (1 - unique / total) * 2;
    this.groupIndex = (this.groupIndex * 100).toFixed(2)
    if (result.length > 0) {
      for (let j = 0; j < result.length; j++) {
        for (let i = 0; i < this.bornCountryGroupList.length; i++) {
          if (this.bornCountryGroupList[i].gpName === result[j].gpName) {
            this.bornCountryGroupList[i].class = "countryGroupCommon";
          }
        }
        for (let k = 0; k < this.registerCountryGroupList.length; k++) {
          if (this.registerCountryGroupList[k].gpName === result[j].gpName) {
            this.registerCountryGroupList[k].class = "countryGroupCommon";
          }
        }
      }
    }
  }
  SdgClickInfo(goalId: any) {
    this.selectedSdgId = goalId.substring(goalId.indexOf(' ') + 1);
    this.selectedSdgId = parseInt(this.selectedSdgId)
    this.bornSdgInfo = [];
    this.regSdgInfo = [];
    this.sdgInfo = this.bornSdgSubGoalDeatails;
    this.values = Object.values(this.sdgInfo)
    for (let i = 0; i < this.values.length; i++) {
      if (this.values[i].SDG_Id === this.selectedSdgId) {
        if (this.values[i].score !== 'NA') {
          this.values[i].score = parseFloat(this.values[i].score);
          this.values[i].score = this.values[i].score.toFixed(2);
        }
        else {
          this.values[i].score = this.values[i].score;
        }
        this.bornSdgInfo.push(this.values[i]);
      }
    }
    this.valuesR = Object.values(this.regSdgSubGoalDeatails)
    for (let i = 0; i < this.valuesR.length; i++) {
      if (this.valuesR[i].SDG_Id === this.selectedSdgId) {
        this.regSdgInfo.push(this.valuesR[i]);
      }
    }
    this.sdgImage = "assets/images/sdgicon/sdg" + this.selectedSdgId + "_" + 'en' + ".png";
    this.sdgDescription = this.sdgDetails[this.selectedSdgId - 1].SDG_discription;
    this.sdgName = this.sdgDetails[this.selectedSdgId - 1].SDG_title;
    this.sdgOrigninalName = this.sdgDetails[this.selectedSdgId - 1].SDG_name;
    this.showSdgInfo = true;
  }
  getRegisterCountrySdgScore(i: any) {
    let v
    v = this.regSdgInfo[i].score;
    return v;
  }
  getRegisterCountrySdgColur(i: any) {
    let v
    v = this.regSdgInfo[i].color;
    return v;
  }
  getNewSdgColor(colour: any) {
    let classvalue = "";
    if (colour === "green") {
      classvalue = "clr_code bg-success d-inline-block mr-2"
    }
    else if (colour === "red") {
      classvalue = "clr_code bg-red d-inline-block mr-2"
    }
    else if (colour === "orange") {
      classvalue = "clr_code bg-oragne d-inline-block mr-2"
    }
    else if (colour === "yellow") {
      classvalue = "clr_code bg-yellow d-inline-block mr-2"
    }
    else if (colour === "grey") {
      classvalue = "clr_code bg-secondary d-inline-block mr-2"
    }
    return classvalue;
  }
  closeSdgInfo() {
    this.showSdgInfo = false;

  }

  clickBuy() {

  }

  clickMapQuetion() {
    let map = am4core.create("chartdivque", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();
    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.propertyFields.fill = "fill";
    this.chartQuemapFlag = true;
  }


  getFlagQuetion() {
    this.bornFlagArray = [];
    this.registerFlagArray = [];
    this.bornFlagArray.push(this.toolService.selectedBornCountryObject.Code);
    this.registerFlagArray.push(this.toolService.selectedRegisterCountryObject.Code)
    for (let i = 0; i < this.toolService.countryList.length; i++) {
      if (i < 3) {
        let value = this.commonService.getRandomValueint(0, 50);
        if (this.toolService.countryList[value].Code !== this.toolService.selectedBornCountryObject.Code) {
          this.bornFlagArray.push(this.toolService.countryList[value].Code);
        }
        else {
          let value = this.commonService.getRandomValueint(0, 50);
          this.bornFlagArray.push(this.toolService.countryList[value].Code);
        }
      }
    }

    this.bornFlagArray = this.commonService.randomArrayShuffle(this.bornFlagArray);

    for (let i = 0; i < this.toolService.countryList.length; i++) {
      if (i < 3) {
        let value = this.commonService.getRandomValueint(51, 191);
        if (this.toolService.countryList[value].Code !== this.toolService.selectedRegisterCountryObject.Code) {
          this.registerFlagArray.push(this.toolService.countryList[value].Code);
        }
        else {
          let value = this.commonService.getRandomValueint(51, 191);
          this.registerFlagArray.push(this.toolService.countryList[value].Code);
        }
      }
    }
    this.registerFlagArray = this.commonService.randomArrayShuffle(this.registerFlagArray);
  }

  redioButtonBorn(b: any) {
    this.bcode = b;
  }

  redioButtonRegister(r: any) {
    this.rcode = r;
  }
  bornFlagAns() {
    if (this.bcode === this.toolService.selectedBornCountryObject.Code) {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.ans')
    }
    else if (this.bcode === '') {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.ans2')
    }
    else {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.ans1')
    }
  }


  registerFlagAns() {
    if (this.rcode === this.toolService.selectedRegisterCountryObject.Code) {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.ans')
    }
    else if (this.rcode === '') {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.ans2')
    }
    else {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.ans1')

    }
  }

  clickOk() {
    this.showAlertFlag = false;

  }
  clickToExitButton() {
    if (this.phpToolService.flowFromCountry) {
      this.modalWindowService.showCountryLearningTool = false;
      this.router.navigate(['/life-tool']);
      this.phpToolService.flowFromCountry = false
      this.phpToolService.flagForPhpToolClick = false;
    }
    else if (this.homeService.flagFromDeadScreens) {
      this.phpToolService.flowFromCountry = false
      this.phpToolService.flagForPhpToolClick = false;
      this.modalWindowService.showCountryLearningTool = false
    }
    else if (this.phpToolService.flagForPhpToolClick && !this.gameSummeryService.countryLearningTool) {
      this.data = false;
      this.phpToolService.flowFromCountry = false
      this.phpToolService.flagForPhpToolClick = false;
      this.modalWindowService.showCountryLearningTool = false
      this.commonService.close();
    }
    else if (this.phpToolService.flagForPhpToolClick && this.gameSummeryService.countryLearningTool) {
      this.modalWindowService.showCountryLearningTool = false;
      this.router.navigate(['/action-page']);
      this.phpToolService.flowFromCountry = false
      this.phpToolService.flagForPhpToolClick = false;
      this.gameSummeryService.countryLearningTool = false;
    }

  }
  checkCssForSpecificText(x: any, y: any) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "textRed";
    }
    else if (value1 < value2) {
      this.classvalue = "textGreen";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }

  checkCssText(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "text-green";
    }
    else if (value1 < value2) {
      this.classvalue = "text-red";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }


  countrySelectionToPlayLifeFirst() {
    this.classForCountryOne = "country_map_log mx-auto countrySelectionClass"
    this.classForCountryTwo = "country_map_log mx-auto"
    this.selectedcountry = this.phpToolService.selectedBornCountryObject
    this.selectedCountryName = this.phpToolService.selectedBornCountryObject.country

  }


  countrySelectionToPlayLifeSecond() {
    this.classForCountryOne = "country_map_log mx-auto"
    this.classForCountryTwo = " country_map_log mx-auto countrySelectionClass"
    this.selectedcountry = this.phpToolService.selectedRegisterCountryObject
    this.selectedCountryName = this.phpToolService.selectedRegisterCountryObject.country
  }

  playReallives() {
    if (this.selectedcountry.length === 0) {
      this.showAlertFlag = true;
      this.msgText = this.translate.instant('gameLabel.live_life_warr')
    }
    else {
      this.phpToolService.flowFromCountry = false
      this.phpToolService.flagForPhpToolClick = false;
      this.modalWindowService.showCountryLearningTool = false
      this.modalWindowService.showWaitScreen = true
      this.modalWindowService.showCharacterDesighWithGropuFlag = true;
      this.designALifeObject["country"] = this.selectedcountry;
      this.designALifeObject["onlyCountry"] = false;
      this.designALifeObject['city'] = null;
      this.designALifeObject["religion"] = null;
      this.designALifeObject["traits"] = null;
      this.designALifeObject['first_name'] = null;
      this.designALifeObject['last_name'] = null;
      this.designALifeObject['nameGroupId'] = null;
      this.designALifeObject['sex'] = null
      this.designALifeObject['urban_rural'] = null;
      this.gameSummeryService.generateCharacterDesignLife(this.designALifeObject, false, false, 0, this.translationService.selectedLang.code, true, 0).subscribe(
        res => {
          this.gameId = res;
          localStorage.removeItem("gameid");
          localStorage.setItem("gameid", this.gameId);
          this.modalWindowService.showWaitScreen = false;
          this.router.navigate(['/createlife']);
        })
    }
  }

  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }


  goToHome() {
    this.modalWindowService.showWaitScreen = true
    this.phpToolService.flowFromCountry = false
    this.phpToolService.flagForPhpToolClick = false;
    this.modalWindowService.showCountryLearningTool = false
    localStorage.removeItem("gameid");
    localStorage.setItem("gameid", this.gameSummeryService.gameId);
    this.router.navigate(
      ['/play-life'],
      { queryParams: { flag: 'true' } }
    );
    this.modalWindowService.showWaitScreen = false
  }




  calculateTableValues() {
    if (this.toolService.selectedBornCountryObject.country.length > 8) {
      this.bornCountry = this.toolService.selectedBornCountryObject.Code;
    }
    else {
      this.bornCountry = this.toolService.selectedBornCountryObject.country;
    }
    if (this.toolService.selectedRegisterCountryObject.country.length > 8) {
      this.registerCountry = this.toolService.selectedRegisterCountryObject.Code;
    }
    else {
      this.registerCountry = this.toolService.selectedRegisterCountryObject.country;
    }
    this.chanceOfPoorR = this.registerStatusData.chanceOfPoor * 100;
    let meanIncomeR = this.registerStatusData.meanIncome
    let incomeDeviationR = this.registerStatusData.incomeDeviation
    let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
    let f3R = (Math.exp(f1R))
    let firstR = (f3R);
    this.firstR = firstR;
    let secondR = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
    let thirdR = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
    let fourthR = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
    this.rOne = (firstR + secondR) / (firstR);
    this.rTwo = (secondR + thirdR) / (firstR);
    this.rThree = (thirdR + fourthR) / (firstR);
    this.rFour = (fourthR) / (firstR);

    //value for born
    let meanIncome = this.statusMetadata.meanIncome
    let incomeDeviation = this.statusMetadata.incomeDeviation
    let exchangeRate = this.statusMetadata.exchangeRate;
    let f1 = (meanIncome + (this.constantService.Economically_Challenged) * incomeDeviation)
    let f3 = (Math.exp(f1))
    let first = (f3);
    let second = (Math.exp(meanIncome + (this.constantService.Bearly_Managed * incomeDeviation)));
    let third = (Math.exp(meanIncome + (this.constantService.Well_to_do * incomeDeviation)));
    let fourth = (Math.exp(meanIncome + (this.constantService.Rich * incomeDeviation)));
    this.bOne = (first + second) / (first);
    this.bTwo = (second + third) / (first);
    this.bThree = (third + fourth) / (first);
    this.bFour = (fourth) / (first);
    this.translate.get('gameLabel.borken_ladder_stmt1', { c1: this.phpToolService.selectedBornCountryObject.country, value1: (this.bTwo).toFixed(2) }).subscribe(
      (str) => {
        this.brokenLadder1 = str;
      })
    this.translate.get('gameLabel.borken_ladder_stmt2', { c2: this.phpToolService.selectedRegisterCountryObject.country, value2: (this.rTwo).toFixed(2) }).subscribe(
      (str) => {
        this.brokenLadder2 = str;
      })
    let adjective;
    if (this.bTwo > this.rTwo) {
      adjective = this.phpToolService.selectedBornCountryObject.Adjective
    }
    else if (this.bTwo < this.rTwo) {
      adjective = this.phpToolService.selectedRegisterCountryObject.Adjective
    }
    else {
      adjective = this.phpToolService.selectedBornCountryObject.Adjective
    }
    this.translate.get('gameLabel.borken_ladder_stmt3', { adjective: (adjective) }).subscribe(
      (str) => {
        this.brokenLadder3 = str;
      })
    if (!this.phpToolService.flagForPhpToolClick) {
      if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 0) {
        this.classForStatusBorderOne = "table-bordered-red newTableFontSize"
      }
      else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 1) {
        this.classForStatusBorderTwo = "table-bordered-yellow newTableFontSize"
      }
      else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 2) {
        this.classForStatusBorderThree = "table-bordered-orange newTableFontSize"

      }
      else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 3) {
        this.classForStatusBorderFour = "table-bordered-blue newTableFontSize"

      }
      else if (this.allPerson[this.constantService.FAMILY_SELF].current_economical_status === 4) {
        this.classForStatusBorderFive = "table-bordered-green newTableFontSize"
      }
    }
  }
}
