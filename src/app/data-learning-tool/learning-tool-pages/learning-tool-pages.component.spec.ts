import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningToolPagesComponent } from './learning-tool-pages.component';

describe('LearningToolPagesComponent', () => {
  let component: LearningToolPagesComponent;
  let fixture: ComponentFixture<LearningToolPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LearningToolPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningToolPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
