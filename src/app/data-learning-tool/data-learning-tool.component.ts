import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from '../game-summery/game-summery.service';
import { HomeService } from '../home/home.service';
import { CommonService } from '../shared/common.service';
import { ConstantService } from '../shared/constant.service';
import { ToolService } from './tool.service';
import { TranslationService } from '../translation/translation.service';
import { QuestionnaireService } from './questionnaire/questionnaire.service';
@Component({
  selector: 'app-data-learning-tool',
  templateUrl: './data-learning-tool.component.html',
  styleUrls: ['./data-learning-tool.component.css']
})
export class DataLearningToolComponent implements OnInit {
  data;
  allPerson;
  country;
  mainPerson;
  registerCountry
  bornFlagArray: any[] = []
  registerFlagArray: any[] = []
  chartQuemapFlag: boolean = false
  label: any;
  bornCountry: any;
  state: any;
  markers: any[] = [];
  mapZoom: any;
  lat: any;
  lng: any;
  wealthGraphSentenceBorn1: any;
  wealthGraphSentenceBorn2: any;
  wealthGraphSentenceBorn3: any;
  wealthGraphSentenceReg1: any;
  wealthGraphSentenceReg2: any;
  wealthGraphSentenceReg3: any;
  bornCountryWelathMetaData: any;
  registerCountryWelathMetaData: any;
  bornCountryWelathMetaData1: any;
  registerCountryWelathMetaData1: any;
  bornCountryArray: any[] = [];
  registerCountryArray: any[] = [];
  constantGraphValueArray = [
    { 'name': '10%', 'value': 10 / 100 },
    { 'name': '20%', 'value': 20 / 100 },
    { 'name': '30%', 'value': 30 / 100 },
    { 'name': '40%', 'value': 40 / 100 },
    { 'name': '50%', 'value': 50 / 100 },
    { 'name': '60%', 'value': 60 / 100 },
    { 'name': '70%', 'value': 70 / 100 },
    { 'name': '80%', 'value': 80 / 100 },
    { 'name': '90%', 'value': 90 / 100 },
    { 'name': '100%', 'value': 100 / 100 },
  ]
  selectedSdgId: any;
  showSdgInfo: boolean = false;
  sdgDetails: any;
  bornSdgSubGoalDeatails: any;
  regSdgSubGoalDeatails: any
  bornSdgInfo: any
  regSdgInfo: any
  registerSdgInfo: any;
  bcode: any = "";
  rcode: any = "";
  registerCountrySdgName: any
  sdgDescription: any
  sdgInfo: any
  sdgName: any
  sdgOrigninalName: any;
  registeredCountrySdgDeatails: any;
  sdgNewData: any;
  sdgReagisterData: any;
  bornGoalColur: any;
  regiGoalColue: any;
  values: any;
  valuesR: any;
  sdgImage: any;
  //graph
  multi: any[] = [];
  multi2: any[] = [];
  multi3: any[] = [];
  showGraphWindow: boolean = false;
  maxLenght: number = 40;
  yTrimValue: boolean = true;
  schemeType: string = 'linear';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  maxXAxisTickLength: number = 10
  gradient: boolean = false;
  showLegend: boolean = false;
  showLegend1: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = this.translate.instant('gameLabel.Country');
  yAxisLabelReg: any;
  showYAxisLabel: boolean = true;
  xAxisLabel: string = this.translate.instant('gameLabel.Population');
  xAxisLabelReg: any;
  colorSchemeBorn: any;
  activeClassForDemo: any;
  activeClassForPerCapita = "nav-item nav-link";
  activeClassForLFO = "nav-item nav-link";
  activeClassForIndex = "nav-item nav-link";
  activeClassForSdg = "nav-item nav-link";
  onClickDemoGraphic: boolean = true;
  onClickPerCapita: boolean = false;
  onCLickLabourForce: boolean = false;
  onCLickSdgGroup: boolean = false;
  onClickIndexRating: boolean = false;
  onClickSdgData: boolean = false;
  sentenceTextWindow1: string = "";
  bithString: string = "";
  deathString: string = "";
  primarySchoolString: string = "";
  happinessString: string = "";
  valurForHappiAndCorr: any;
  giniIndexString: string = "";
  labourForceString1: string = "";
  labourForceString2: string = "";
  sexRatio: string = "";
  accesEleString = "";
  valueForHdi: any;
  sexRatioValue: any;
  sdgScoreBorn: any;
  sdgScoreRegister: any;
  populationString: any;
  welthGraphDataNotGenrateFlag: boolean = false;
  status: any;
  statusValue: any;
  firstValue: any;
  secondValue: any;
  thirdValue: any;
  fourthValue: any;
  firstValueReg: any;
  secondValueReg: any;
  thirdValueReg: any;
  fourthValueReg: any;
  statusMetadata: any
  oldStatusValue: any;
  registerStatusData: any;
  incomeGraphStringOne: any;
  incomeGraphStringTwo: any;
  incomeGraphStringThree: any;
  registerCountryExchangeRate: any;
  stringReturnValue: any;
  classvalue: any;
  poupulationValue: any;
  showAlertFlag: boolean = false;
  msgText: any;
  stringRetutnText: any;
  senntenceGeneratedFlag: any;
  single: any;
  checkFormat: any;
  bornCountryGroupList: any;
  registerCountryGroupList: any;
  groupIndex: any
  quetionayMetadata: any
  colorScheme = {
    domain: ['#00FFFF', '#00FFFF']
  };
  colorScheme1 = {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA'],
  };
  colorScheme3 = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#0000FF']
  };
  view: any;
  view1: any;
  view2: any;
  view3: any;
  view4: any;
  multi1: any;
  showDataLabel = true;
  sdgData: any;
  sdgDispalyArrayBorn: any;
  sdgDispalyArrayRegister: any;
  webLink = "https://dashboards.sdgindex.org/profiles/";
  countryGroupTitle: any;
  countryGroupIframeFlag: boolean = false;
  colorScheme2 = {
    domain: ['#00FFFF', '#567caa', '#567caa']
  };

  formatDataLabel(value: any) {
    return "$" + value;
  }
  constructor(public toolService: ToolService,
    public homeService: HomeService,
    public gameSummeryService: GameSummeryService,
    public constantService: ConstantService,
    public translate: TranslateService,
    public commonService: CommonService,
    public translationService: TranslationService,
    public questionnaireService: QuestionnaireService) { }

  ngOnInit(): void {

  }
}
