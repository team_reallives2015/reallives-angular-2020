import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionRelationshipComponent } from './action-relationship.component';

describe('ActionRelationshipComponent', () => {
  let component: ActionRelationshipComponent;
  let fixture: ComponentFixture<ActionRelationshipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionRelationshipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionRelationshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
