import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class RelationService {

  constructor(private http : HttpClient,
              private commonService : CommonService) { }


  getNewRomance(gameId){
    return this.http.get<Array<Object>>(`${this.commonService.url}game/relation/getLover/${gameId}`);
  }

  saveRomance(gameId,flag,lover){
    return this.http.post<Array<Object>>(`${this.commonService.url}game/relation/insertLover/${gameId}/${flag}`,{lover});

  }

  endRomance(gameId,endRelation){
    return this.http.get<Array<Object>>(`${this.commonService.url}game/relation/endRelation/${gameId}/${endRelation}`);
  }


  tryToHaveChild(gameId){
    return this.http.get<Array<Object>>(`${this.commonService.url}game/relation/tryToHaveChild/${gameId}`);
  }


  marriageSave(gameId){
    return this.http.get<boolean>(`${this.commonService.url}game/relation/proposeNormal/${gameId}`);
  }

  leaveMarriage(gameId,cash){
    return this.http.post<boolean>(`${this.commonService.url}game/relation/leaveMarriage/${gameId}`,{cash});
  }

  getAdoptChild(gameId,sex,age){
    return this.http.get<boolean>(`${this.commonService.url}game/relation/adoptChild/${gameId}/${sex}/${age}`);

  }

  getAEvent(gameId,object){
    return this.http.post<any>(`${this.commonService.url}game/addEventNo/${gameId}`,{object});
  }

  getActionNextEvent(nextEventObject){
    return  this.http.post<Array<Object>>(`${this.commonService.url}game/actionEventNext`,{nextEventObject});
  }

  setHomesexualIndexForSameSex(gameId){
    return this.http.get<any>(`${this.commonService.url}game/saveHomoSexualityIndex/${gameId}`);

  }

  kickOut(gameId,action){
    return this.http.get<any>(`${this.commonService.url}game/parentKickOut/${action}/${gameId}`);

  }

  proposeMarriageUpdate(gameId,upForInheritance){
    return this.http.get<any>(`${this.commonService.url}game/proposeMarriageComplication/${gameId}/${upForInheritance}`);

  }

  normalProposemarriage(gameId){
    return this.http.get<any>(`${this.commonService.url}game/relation/proposeNormal/${gameId}`);
  }

  proposeMarriageNo(gameId){
    return this.http.get<any>(`${this.commonService.url}game/proposeMarriageNo/${gameId}`);

  }







}
