import { truncateWithEllipsis } from '@amcharts/amcharts4/.internal/core/utils/Utils';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { interval } from 'rxjs';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { CommonActionService } from '../common-action.service';
import { HomeService } from '../home.service';
import { RelationService } from './relation.service';

@Component({
  selector: 'app-action-relationship',
  templateUrl: './action-relationship.component.html',
  styleUrls: ['./action-relationship.component.css']
})
export class ActionRelationshipComponent implements OnInit {
  allPerson;
  mainPerson;
  allCards = [];
  firstCard = [];
  cardLength;
  default = 0;
  messageText;
  lover;
  confirmBoxFlag: boolean = false;
  romanceCondirmBoxFlag: boolean = false;
  updatedObject;
  successMsgFlag: boolean = false;
  endRomanceConfirmBoxFlag: boolean = false;
  proposeMarriageConfirmFlag: boolean = false;
  leaveMarriageConfirmFlag: boolean = false;
  leaveMarriageAmount;
  adoptChildClick: boolean = false;
  tryToHaveChildSelection: { [key: number]: string } =
    {
      1: this.translation.instant('gameLabel.Adopt_An_Infant'),
      2: this.translation.instant('gameLabel.Older_Child'),
    };

  slectedtryToHaveChildSelection = {
    tryToHaveChildSelection: '1'
  };
  showAlertFlag: boolean = false;
  alertClass;
  alertMsg;
  alertType;
  newBabySex;
  adoptedChildAge;
  seekANewRomanceDecisionText;
  seekANewRomanceDecisionFactroid
  sameSexDecisionText;
  sameSexDecisionFactroid
  flagForEvent: boolean = false;
  parentDecisionText;
  parentDecisionFactroid;
  parentKickOutDecisionText;
  parentKickOutDecisionFactroid;
  result;
  potentialPartner;
  initial;
  msgClass;
  age;
  decisionLangCode;
  parameters: any;
  loverPronoun;
  parentEngagedDecisionText;
  parentEngagedDecisionFactroid;
  parentEngageOutOfWillText;
  parentEngageOutOfWillFactroid;
  sex;
  eventDispalyString;
  endRomanceAge;
  endRomanceFlag: boolean = true;

  constructor(public modalWindowShowHide: modalWindowShowHide,
    public homeService: HomeService,
    public constantService: ConstantService,
    public commonService: CommonService,
    private relationService: RelationService,
    public commonActionService: CommonActionService,
    public translation: TranslateService) {
    interval(30000).subscribe(x => {
      if (this.showAlertFlag) {
        this.showAlertFlag = false;
      }
    });
  }
  @ViewChild('seekANewRomance') seekANewRomance: ModalDirective;
  @ViewChild('parentApproveDecision') parentApproveDecision: ModalDirective;
  @ViewChild('parentKickOut') parentKickOut: ModalDirective;
  @ViewChild('sameSex') sameSex: ModalDirective;
  @ViewChild('parentEngage') parentEngage: ModalDirective;
  @ViewChild('parentEngageOutOfWill') parentEngageOutOfWill: ModalDirective;


  ngOnInit(): void {
    this.allCards = [];
    this.allPerson = this.homeService.allPerson;
    this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
    this.allCards = this.allCards.reverse();
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.relationType === this.constantService.ACTION_RELATION_NEWROMANCE) {
      this.endRomanceAge = (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.fromAge) - (this.homeService.allPerson[this.constantService.FAMILY_SELF].age);
      if (this.endRomanceAge > 0) {
        this.endRomanceFlag = false;
      }
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
      this.alertMsg = this.translation.instant('gameLabel.You cannot take any action because you are dead!');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 13) {
      this.alertMsg = this.translation.instant('gameLabel.You_cannot_start_a_relationship_before_age_13.');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
  }


  getANewRomance() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.relationService.getNewRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.result = res;
        this.lover = this.result.data.result.lover;
        this.homeService.allPerson["LOVER"] = this.lover;
        this.homeService.changeAllPerson.emit(this.homeService.allPerson);
        this.age = this.result.data.result.age;
        this.seekANewRomanceDecisionText = this.result.data.result.lifeevent
        this.seekANewRomanceDecisionFactroid = this.result.data.result.factoid;
        this.seekANewRomance.show();
        this.modalWindowShowHide.showWaitScreen = false;
      })
  }


  getARomanceYes() {
    this.seekANewRomance.hide();
    this.modalWindowShowHide.showWaitScreen = true;
    this.romanceCondirmBoxFlag = false;
    this.confirmBoxFlag = false;
    if (this.lover.sex == this.constantService.MALE) {
      this.initial = this.translation.instant('gameLabel.He');
      this.potentialPartner = this.translation.instant('gameLabel.boy');

    }
    else {
      this.initial = this.translation.instant('gameLabel.She');
      this.potentialPartner = this.translation.instant('gameLabel.girl');

    }

    if ((this.commonService.getRandomValue(0, 100)) < (this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.goodLooks + this.commonService.getRandomValue(0, 20))) {
      if (!this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].dead && !this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].father].dead
        && (this.homeService.allPerson[this.constantService.FAMILY_SELF].living_at_home)
        && (this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.intelligence - this.lover.traits.intelligence > 10) && (this.commonService.getRandomValue(0, 1) > 0.58)) {
        //decision for parent
        this.parameters = {
          'PotentialPartner': this.potentialPartner, 'Lover_FullName': this.lover.full_name, 'Lover_Job': this.lover.job.Job_displayName.toLowerCase(), 'Lover_Age': this.lover.age, 'Lover_Health': this.lover.traits.health, 'Lover_Happiness': this.lover.traits.happiness,
          'Lover_Intelligence': this.lover.traits.intelligence, 'Lover_Musical': this.lover.traits.musical, 'Lover_Athletic': this.lover.traits.athletic, 'Lover_Strength': this.lover.traits.strength, 'Lover_PhysicalEndurance': this.lover.traits.physicalEndurance,
          'Lover_Spiritual': this.lover.traits.spiritual, 'Lover_Wisdom': this.lover.traits.wisdom, 'Lover_Artistic': this.lover.traits.artistic
        };
        this.decisionLangCode = "COMMON_FATE_LOVE_DECISSION_02";
        let nextEventObject = { langCode: this.decisionLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters: this.parameters };
        this.relationService.getActionNextEvent(nextEventObject).subscribe(
          res => {
            this.result = res;
            this.age = this.result.data.result.age;
            this.parentDecisionText = this.result.data.result.lifeevent
            this.parentDecisionFactroid = this.result.data.result.factoid;
            this.parentApproveDecision.show();
            this.modalWindowShowHide.showWaitScreen = false;
          })
      }

      //   succesfully get a lover
      else {
        this.relationService.saveRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, true, this.lover).subscribe(
          res => {
            this.result = res;
            this.modalWindowShowHide.showWaitScreen = false;
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
            this.allCards = this.allCards.reverse();
          })
      }
    }
    // not get lover
    else {
      this.flagForEvent = true;
      let object = {
        "lover": this.lover,
        "flag": this.flagForEvent,
        "eventFlag": true
      }
      this.relationService.getAEvent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, object).subscribe(
        res => {
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
          this.allCards = this.allCards.reverse();
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.relationType === this.constantService.ACTION_RELATION_NEWROMANCE) {
            this.endRomanceAge = (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.fromAge) - (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.toAge);
            if (this.endRomanceAge > 0) {
              this.endRomanceFlag = false;
            }
          }
          this.translation.get('gameLabel.doesnt_feel_the_same_about_you_unfortunately', { sex: this.initial }).subscribe(
            (s: String) => {
              this.messageText = s;
              this.successMsgFlag = true;
              this.msgClass = "successpopup";
              this.modalWindowShowHide.showWaitScreen = false;
            })
        })
    }
  }


  getARomanceNo() {
    this.seekANewRomance.hide();
    this.romanceCondirmBoxFlag = false;
    this.confirmBoxFlag = false;
    this.flagForEvent = true;
    let object = {
      "lover": this.lover,
      "flag": this.flagForEvent,
      "eventFlag": false
    }
    if (this.lover.sex == this.homeService.allPerson[this.constantService.FAMILY_SELF].sex) {
      this.decisionLangCode = "COMMON_ACTION_RELATIONSHIP_SEEKNEWROMANCE_DECISION_01";
      let nextEventObject = { langCode: this.decisionLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id };
      this.relationService.getActionNextEvent(nextEventObject).subscribe(
        res => {
          this.result = res;
          this.age = this.result.data.result.age;
          this.sameSexDecisionText = this.result.data.result.lifeevent
          this.sameSexDecisionFactroid = this.result.data.result.factoid;
          this.sameSex.show();
          this.modalWindowShowHide.showWaitScreen = false;
        })
    }
    else {
      this.relationService.getAEvent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, object).subscribe(
        res => {
          this.result = res;
        })
    }

  }

  sameSexYes() {
    this.sameSex.hide();
    this.relationService.setHomesexualIndexForSameSex(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.result = res;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
        this.allCards = this.allCards.reverse();
      }
    )
  }

  sameSexNo() {
    this.sameSex.hide();
  }


  parentApproveYes() {
    this.parentApproveDecision.hide();
    this.modalWindowShowHide.showWaitScreen = true;
    if (this.commonService.getRandomValue(0, 1) < 0.5) {
      this.parameters = {
        'PotentialPartner': this.potentialPartner, 'Lover_FullName': this.lover.full_name, 'Lover_Job': this.lover.job.Job_displayName.toLowerCase(), 'Lover_Age': this.lover.age, 'Lover_Health': this.lover.traits.health, 'Lover_Happiness': this.lover.traits.happiness,
        'Lover_Intelligence': this.lover.traits.intelligence, 'Lover_Musical': this.lover.traits.musical, 'Lover_Athletic': this.lover.traits.athletic, 'Lover_Strength': this.lover.traits.strength, 'Lover_PhysicalEndurance': this.lover.traits.physicalEndurance,
        'Lover_Spiritual': this.lover.traits.spiritual, 'Lover_Wisdom': this.lover.traits.wisdom, 'Lover_Artistic': this.lover.traits.artistic
      };
      this.decisionLangCode = "COMMON_FATE_LOVE_DECISSION_03";
      let nextEventObject = { langCode: this.decisionLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters: this.parameters };
      this.relationService.getActionNextEvent(nextEventObject).subscribe(
        res => {
          this.result = res;
          this.age = this.result.data.result.age;
          this.parentKickOutDecisionText = this.result.data.result.lifeevent
          this.parentKickOutDecisionFactroid = this.result.data.result.factoid;
          this.parentKickOut.show();
          this.modalWindowShowHide.showWaitScreen = false;
        }
      )
    }
    else {
      this.modalWindowShowHide.showWaitScreen = false;
      this.relationService.saveRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, true, this.lover).subscribe(
        res => {
          this.result = res;
          this.modalWindowShowHide.showWaitScreen = false;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
          this.allCards = this.allCards.reverse();
        })
    }

  }

  parentApproveNo() {
    this.parentApproveDecision.hide();
    this.modalWindowShowHide.showWaitScreen = true;
    //delete lover
    this.relationService.saveRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, false, this.lover).subscribe(
      res => {
        this.result = res;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
        this.allCards = this.allCards.reverse();
        this.modalWindowShowHide.showWaitScreen = false;
      })

  }

  kickOutYes() {
    this.parentKickOut.hide();
    this.modalWindowShowHide.showWaitScreen = true;
    this.relationService.saveRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, true, this.lover).subscribe(
      res => {
        this.result = res;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        let action = this.constantService.ACTION_RELATION_NEW_ROMANCE;
        this.relationService.kickOut(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, action).subscribe(
          res1 => {
            let result1 = res1;
            this.commonActionService.getActionEventAndIdentityDifference(result1, this.homeService.allPerson);
            this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
            this.allCards = this.allCards.reverse();
            this.modalWindowShowHide.showWaitScreen = false;
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.relationType === this.constantService.ACTION_RELATION_NEWROMANCE) {
              this.endRomanceAge = (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.fromAge) - (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_my_relationship.toAge);
              if (this.endRomanceAge > 0) {
                this.endRomanceFlag = false;
              }
            }
          })
      })
  }

  kickOutNo() {
    this.parentKickOut.hide();
    this.modalWindowShowHide.showWaitScreen = true;
    this.relationService.saveRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, false, this.lover).subscribe(
      res => {
        this.result = res;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
        this.allCards = this.allCards.reverse();
        this.modalWindowShowHide.showWaitScreen = false;
      })

  }

  endRomance() {
    this.romanceCondirmBoxFlag = false;
    this.confirmBoxFlag = true;
    this.endRomanceConfirmBoxFlag = true
    this.messageText = this.translation.instant('gameLabel.Are_you_sure you want to End Romance');

  }


  endRomanceYes() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.romanceCondirmBoxFlag = false;
    this.confirmBoxFlag = false;
    this.endRomanceConfirmBoxFlag = false
    this.relationService.endRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.constantService.ACTION_RELATION_END_ROMANCE).subscribe(res => {
      this.result = res;
      this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
      this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
      this.allCards = this.allCards.reverse();
      this.messageText = this.translation.instant('gameLabel.Successfully_ended_romance!');
      this.msgClass = "successpopup"
      this.successMsgFlag = true;
      this.modalWindowShowHide.showWaitScreen = false;
    })

  }


  endRomanceNo() {
    this.romanceCondirmBoxFlag = false;
    this.confirmBoxFlag = false;
    this.endRomanceConfirmBoxFlag = false
  }

  proposeForMarriage() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.lover = this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover];
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE) {
      this.loverPronoun = this.translation.instant('gameLabel.He');
    }
    else {
      this.loverPronoun = this.translation.instant('gameLabel.she');
    }

    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid === 34) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 22)) {
      this.messageText = this.translation.instant('gameLabel.According_to_government_authorities_you_are_too_young_to_marry.');
      this.msgClass = "errorpopup";
      this.successMsgFlag = true;
      this.modalWindowShowHide.showWaitScreen = false;
      return;
    }

    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].has_lover) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null)
      && ((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex)
        || (!this.homeService.allPerson[this.constantService.FAMILY_SELF].country.marriageCountryFlag))) {
      if (((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE)
        && (this.commonService.getRandomValue(0, 1) < 0.80)) || ((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== this.constantService.MALE)
          && (this.commonService.getRandomValue(0, 1) < 0.50))) {
        //parent does not approve
        if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null) && (!this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].dead) &&
          (!this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].father].dead)
          && ((this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.intelligence - this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].traits.intelligence) > 10)
          && (this.commonService.getRandomValue(0, 1) > 0.4)) {
          this.parameters = {
            'YourMy': "your", 'Lover_FullName': this.lover.full_name, 'Lover_Job': this.lover.job.Job_displayName.toLowerCase(), 'Lover_HouseholdNetWorth': this.lover.netWorth, 'CurrencyPlural': this.lover.country.currencyName, 'Lover_Age': this.lover.age, 'Lover_Health': this.lover.traits.health, 'Lover_Happiness': this.lover.traits.happiness,
            'Lover_Intelligence': this.lover.traits.intelligence, 'Lover_Musical': this.lover.traits.musical, 'Lover_Athletic': this.lover.traits.athletic, 'Lover_Strength': this.lover.traits.strength, 'Lover_PhysicalEndurance': this.lover.traits.physicalEndurance,
            'Lover_Spiritual': this.lover.traits.spiritual, 'Lover_Wisdom': this.lover.traits.wisdom, 'Lover_Artistic': this.lover.traits.artistic
          };
          this.decisionLangCode = "COMMON_FATE_MARRIAGE_DECISSION_03"
          let nextEventObject = { langCode: this.decisionLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters: this.parameters };
          this.relationService.getActionNextEvent(nextEventObject).subscribe(
            res => {
              this.result = res;
              this.age = this.result.data.result.age;
              this.parentEngagedDecisionText = this.result.data.result.lifeevent
              this.parentEngagedDecisionFactroid = this.result.data.result.factoid;
              this.parentEngage.show();
              this.modalWindowShowHide.showWaitScreen = false;
            })
        }
        else {
          if (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex === this.constantService.MALE) {
            this.sex = this.translation.instant('gameLabel.He');
          }
          else if (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex === this.constantService.FEMALE) {
            this.sex = this.translation.instant('gameLabel.She');
          }
          this.relationService.normalProposemarriage(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,).subscribe(
            res => {
              this.result = res;
              this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
              this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
              this.allCards = this.allCards.reverse();
              this.translation.get('gameLabel.accepts_Congratulations_you_are_engaged!', { sex: this.sex }).subscribe(
                (s: String) => {
                  this.messageText = s;
                  this.msgClass = "sucesspopup";
                  this.successMsgFlag = true;
                  this.modalWindowShowHide.showWaitScreen = false;
                })
            })
        }
      }
      else {
        if (this.commonService.getRandomValue(0, 1) < 0.2) {
          this.modalWindowShowHide.showWaitScreen = true;
          this.eventDispalyString = "crazy";
          if (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex === this.constantService.MALE) {
            this.sex = this.translation.instant('gameLabel.He');
          }
          else if (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex === this.constantService.FEMALE) {
            this.sex = this.translation.instant('gameLabel.She');
          }
          this.relationService.endRomance(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.constantService.ACTION_RELATION_END_ROMANCE).subscribe(
            res => {
              this.result = res;
              this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
              this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
              this.allCards = this.allCards.reverse();
              this.modalWindowShowHide.showWaitScreen = false;
              this.translation.get('gameLabel.Your_lover_thinks_you_are_crazy_and_tells_you', { sex: this.sex }).subscribe(
                (s: String) => {
                  this.messageText = s;
                  this.msgClass = "errorpopup";
                  this.successMsgFlag = true;
                  this.modalWindowShowHide.showWaitScreen = false;
                })
            });
        }
        else {
          this.flagForEvent = false;
          let object = {
            "lover": this.lover,
            "flag": this.flagForEvent,
            "eventFlag": true
          }
          this.relationService.getAEvent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, object).subscribe(
            res => {
              this.result = res;
              this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
              this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
              this.allCards = this.allCards.reverse();
              this.messageText = this.translation.instant('gameLabel.says_that_it_is_too_early_for_that.');
              this.msgClass = "errorpopup";
              this.successMsgFlag = true;
              this.modalWindowShowHide.showWaitScreen = false;
            });
        }
      }
    }
    else if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null) &&
      (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex)) {
      this.messageText = this.translation.instant('gameLabel.Same_sex_marriage_is_generally_not_allowed');
      this.msgClass = "errorpopup";
      this.successMsgFlag = true;
      this.modalWindowShowHide.showWaitScreen = false;
    }

  }
  parentEngageYes() {
    this.parentEngage.hide();
    this.modalWindowShowHide.showWaitScreen = true;
    if (this.commonService.getRandomValue(0, 1) < 0.5) {
      this.parameters = {
        'PotentialPartner': this.potentialPartner, 'Lover_FullName': this.lover.full_name, 'Lover_Job': this.lover.job.Job_displayName.toLowerCase(), 'Lover_Age': this.lover.age, 'Lover_Health': this.lover.traits.health, 'Lover_Happiness': this.lover.traits.happiness,
        'Lover_Intelligence': this.lover.traits.intelligence, 'Lover_Musical': this.lover.traits.musical, 'Lover_Athletic': this.lover.traits.athletic, 'Lover_Strength': this.lover.traits.strength, 'Lover_PhysicalEndurance': this.lover.traits.physicalEndurance,
        'Lover_Spiritual': this.lover.traits.spiritual, 'Lover_Wisdom': this.lover.traits.wisdom, 'Lover_Artistic': this.lover.traits.artistic
      };
      this.decisionLangCode = "COMMON_FATE_MARRIAGE_DECISSION_04";
      let nextEventObject = { langCode: this.decisionLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters: this.parameters };
      this.relationService.getActionNextEvent(nextEventObject).subscribe(
        res => {
          this.result = res;
          this.age = this.result.data.result.age;
          this.parentEngageOutOfWillText = this.result.data.result.lifeevent
          this.parentEngageOutOfWillFactroid = this.result.data.result.factoid;
          this.parentEngageOutOfWill.show();
          this.modalWindowShowHide.showWaitScreen = false;
        })
    }
    else {
      this.modalWindowShowHide.showWaitScreen = false;

    }
  }

  parentNotApproveAndParentEngageNo() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.parentEngage.hide();
    this.parentEngageOutOfWill.hide();
    this.relationService.proposeMarriageNo(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.result = res;
        this.commonActionService.getActionIdentityDiffrence(this.result, this.homeService.allPerson);
        this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
        this.allCards = this.allCards.reverse();
        this.modalWindowShowHide.showWaitScreen = false;
      }
    )
  }

  engageOutOfWillYes() {
    this.parentEngageOutOfWill.hide();
    this.modalWindowShowHide.showWaitScreen = true;
    if (this.commonService.getRandomValue(0, 1) < 0.5) {
      if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
        this.homeService.allPerson[this.constantService.FAMILY_SELF].up_for_inheritance = false;
        this.relationService.proposeMarriageUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].up_for_inheritance).subscribe(
          res => {
            this.result = res;
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            let action = "PROPOSE";
            if (this.result.data.result.hasOwnProperty('actionNextEvent')) {
              let secondLangCode = this.result.data.result.actionLangCode;
              let parameters = this.result.data.result.nextEventParameters;
              let nextEventObject = { langCode: secondLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
              this.relationService.getActionNextEvent(nextEventObject).subscribe(
                res1 => {
                  let result1 = res1;
                  this.commonActionService.getActionEventAndIdentityDifference(result1, this.homeService.allPerson);
                  this.relationService.kickOut(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, action).subscribe(
                    res2 => {
                      let result2 = res2;
                      this.commonActionService.getActionEventAndIdentityDifference(result2, this.homeService.allPerson);
                      this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
                      this.allCards = this.allCards.reverse();
                      this.modalWindowShowHide.showWaitScreen = false;
                    })
                })
            }
            else {
              this.modalWindowShowHide.showWaitScreen = false;
            }

          })

      }
      else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
        this.relationService.proposeMarriageUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].up_for_inheritance).subscribe(
          res => {
            this.result = res;
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            if (this.result.data.result.hasOwnProperty('actionNextEvent')) {
              let secondLangCode = this.result.data.result.actionLangCode;
              let parameters = this.result.data.result.nextEventParameters;
              let nextEventObject = { langCode: secondLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
              this.relationService.getActionNextEvent(nextEventObject).subscribe(
                res1 => {
                  let result1 = res1;
                  this.commonActionService.getActionEventAndIdentityDifference(result1, this.homeService.allPerson);
                  this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
                  this.allCards = this.allCards.reverse();
                  this.modalWindowShowHide.showWaitScreen = false;
                })
            }
            else {
              this.modalWindowShowHide.showWaitScreen = false;
            }

          })
      }
      else {
        this.modalWindowShowHide.showWaitScreen = false;
      }
    }
    else {
      this.homeService.allPerson[this.constantService.FAMILY_SELF].up_for_inheritance = true;
      this.relationService.proposeMarriageUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].up_for_inheritance).subscribe(
        res => {
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          if (this.result.data.result.hasOwnProperty('actionNextEvent')) {
            let secondLangCode = this.result.data.result.actionLangCode;
            let parameters = this.result.data.result.nextEventParameters;
            let nextEventObject = { langCode: secondLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, parameters };
            this.relationService.getActionNextEvent(nextEventObject).subscribe(
              res1 => {
                let result1 = res1;
                this.commonActionService.getActionEventAndIdentityDifference(result1, this.homeService.allPerson);
                this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
                this.allCards = this.allCards.reverse();
                this.modalWindowShowHide.showWaitScreen = false;
              })
          }
          else {
            this.modalWindowShowHide.showWaitScreen = false;
          }
        })
    }
  }

  proposeMarriageYes() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.successMsgFlag = false;
    this.confirmBoxFlag = false;
    this.proposeMarriageConfirmFlag = false;
    this.relationService.marriageSave(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.homeService.getPersonFromId(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
          res => {
            this.homeService.allPerson = res;
            this.homeService.changeAllPerson.emit(this.homeService.allPerson);
            this.modalWindowShowHide.showWaitScreen = false;
            this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
            this.allCards = this.allCards.reverse();
            this.messageText = this.translation.instant('gameLabel.Married_successfully!');
            this.msgClass = "succespopup";
            this.successMsgFlag = true;
          })
      })
  }

  proposeMarriageNo() {
    this.successMsgFlag = false;
    this.confirmBoxFlag = false;
    this.proposeMarriageConfirmFlag = false;
  }


  leaveMarriage() {
    this.messageText = this.translation.instant('gameLabel.Are_you_sure_you_want_to_Leave_Marriage');
    this.successMsgFlag = true;
    this.confirmBoxFlag = true;
    this.leaveMarriageConfirmFlag = true;
  }

  leaverMarriageYes() {
    this.successMsgFlag = false;
    this.confirmBoxFlag = false;
    this.leaveMarriageConfirmFlag = false;
    this.modalWindowShowHide.showWaitScreen = true;
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex == this.constantService.MALE)
      && (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife != null)
      && (!this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].dead)) {
      this.leaveMarriageAmount = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 30) / 100;
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash > this.leaveMarriageAmount) {
        this.relationService.leaveMarriage(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.leaveMarriageAmount).subscribe(
          res => {
            this.result = res;
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showWaitScreen = false;
            this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
            this.allCards = this.allCards.reverse();
            this.messageText = this.translation.instant('gameLabel.Left_marriage_successfully!');
            this.successMsgFlag = true;
            this.msgClass = "succespopup";
          }
        );
      }
      else {

        this.messageText = this.translation.instant('gameLabel.you_does_not_have_an_enough_cash_please_liquidate_investment_to_get_money');
        this.successMsgFlag = true;
        this.msgClass = "errorpopup";
        this.modalWindowShowHide.showWaitScreen = false;
      }
    }
    else if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.FEMALE)
      && (this.homeService.allPerson[this.constantService.FAMILY_SELF].husband != null)
      && (!this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].husband].dead)) {
      this.leaveMarriageAmount = 0;
      this.relationService.leaveMarriage(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.leaveMarriageAmount).subscribe(
        res => {
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.modalWindowShowHide.showWaitScreen = false;
          this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
          this.allCards = this.allCards.reverse();
          this.messageText = this.translation.instant('gameLabel.Left_marriage_successfully!');
          this.successMsgFlag = true;
          this.msgClass = "succespopup";
        });
    }

  }

  leaveMarriageNo() {
    this.successMsgFlag = false;
    this.confirmBoxFlag = false;
    this.leaveMarriageConfirmFlag = false;
  }

  adoptChild() {
    this.adoptChildClick = true;
  }


  adoptInfantORadoptOlderChild() {
    this.adoptChildClick = false;
    if (this.slectedtryToHaveChildSelection.tryToHaveChildSelection == "1") {
      this.modalWindowShowHide.showWaitScreen = true;
      if (this.commonService.getRandomValue(0, 1) < 0.4) {
        this.modalWindowShowHide.showWaitScreen = false;
        this.messageText = this.translation.instant('gameLabel.No_babies_are_available_for_adoption.');
        this.adoptChildClick = false;
        this.successMsgFlag = true;
        return;
      }
      if (this.commonService.getRandomValue(0, 1) > 0.49) {
        this.newBabySex = this.constantService.MALE;
        this.adoptedChildAge = 0;
      }
      else {
        this.newBabySex = this.constantService.FEMALE;
        this.adoptedChildAge = 0;
      }
      this.relationService.getAdoptChild(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.newBabySex, this.adoptedChildAge).subscribe(
        res => {
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
          this.allCards = this.allCards.reverse();
          this.messageText = this.translation.instant('gameLabel.Adopted_child_successfully!');
          this.successMsgFlag = true;
          this.msgClass = "successpopup";
          this.modalWindowShowHide.showWaitScreen = false;
        });
    }
    else if (this.slectedtryToHaveChildSelection.tryToHaveChildSelection == "2") {
      this.modalWindowShowHide.showWaitScreen = true;

      if ((this.commonService.getRandomValue(0, 1)) > 0.49) {
        this.adoptedChildAge = (this.commonService.getRandomValue(0, 4) + 1);
        this.adoptedChildAge = Math.round(this.adoptedChildAge);
        this.newBabySex = this.constantService.FEMALE;
      }
      else {
        this.adoptedChildAge = (this.commonService.getRandomValue(0, 4) + 1);
        this.adoptedChildAge = Math.round(this.adoptedChildAge);
        this.newBabySex = this.constantService.MALE;
      }
      this.relationService.getAdoptChild(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.newBabySex, this.adoptedChildAge).subscribe(
        res => {
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.modalWindowShowHide.showWaitScreen = false;
          this.allCards = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_relationship))
          this.allCards = this.allCards.reverse();
          this.messageText = this.translation.instant('gameLabel.Adopted_child_successfully!');
          this.successMsgFlag = true;
          this.msgClass = "successpopup";
          this.modalWindowShowHide.showWaitScreen = false;
        })
    }
  }

  adoptChildCancle() {
    this.adoptChildClick = false;
  }

  tryToHaveChild() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].number_of_children > 11) {
      this.messageText = this.translation.instant('gameLabel.You_cannot_have_more_than_12_children.');
      this.successMsgFlag = true;
      this.msgClass = "errorpopup";
      return;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].infertile) {
      this.messageText = this.translation.instant('gameLabel.You_are_infertile_and_will_not_be_able_to_have_your_own_children.');
      this.msgClass = "errorpopup";
      this.successMsgFlag = true;
      return;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null &&
      (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].infertile)) {
      this.messageText = this.translation.instant('gameLabel.Your_partner_is_infertile_and_will_not_be_able_to_have_his_or_her_own_child');
      this.successMsgFlag = true;
      this.msgClass = "errorpopup";
      return;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null && this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].infertile) {
      this.messageText = this.translation.instant('gameLabel.Your_partner_is_infertile_and_will_not_be_able_to_have_his_or_her_own_child');
      this.successMsgFlag = true;
      this.msgClass = "errorpopup";
      return;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.FEMALE) {

      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age >= 45) {
        this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_henceforth');
        this.successMsgFlag = true;
        this.msgClass = "errorpopup";
        return;
      }
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover != null) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex) {
          this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_for_same_sex_marriage.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
          return;
        }
      } else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE && this.homeService.allPerson[this.constantService.FAMILY_SELF].husband !== null) {

        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].husband].sex) {
          this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_for_same_sex_marriage.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
          return;

        }
      }
      else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE && this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null) {

        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].sex) {
          this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_for_same_sex_marriage.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
          return;

        }
      }


    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE && this.homeService.allPerson[this.constantService.FAMILY_SELF].age > 70 && this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null || this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null) {

      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE && this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null && this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].age > 45) {

        this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_henceforth');
        this.successMsgFlag = true;
        this.msgClass = "errorpopup";
        return;
      } else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE && this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null && this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].age > 45) {

        this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_henceforth');
        this.successMsgFlag = true;
        this.msgClass = "errorpopup";

      }

      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE && this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null) {

        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex) {
          this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_for_same_sex_marriage.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
          return;
        }

      } else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE && this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null) {

        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].sex) {
          this.messageText = this.translation.instant('gameLabel.Pregnancy_is_not_possible_for_same_sex_marriage.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
          return;

        }
      }


    }

    if ((((this.homeService.allPerson[this.constantService.FAMILY_SELF].lover != null)
      && (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex)
      && ((this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.health > 10) && ((this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].traits.health > 10)))
      && ((((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== this.constantService.MALE) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 45) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age > 15))
        && (((this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].age < 70) && (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].age > 15))))

        || (((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 70) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age > 13))
          && ((this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].age < 45) && (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].age > 13)))))

      || ((this.homeService.allPerson[this.constantService.FAMILY_SELF].wife != null)
        && (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].sex)
        && ((this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.health > 10) && ((this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].traits.health > 10)))
        && ((((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.constantService.MALE) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 70) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age > 13))
          && ((this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].age < 45) && (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].age > 13)))))

      || ((this.homeService.allPerson[this.constantService.FAMILY_SELF].husband != null)
        && (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].husband].sex)
        && ((this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.health > 10) && ((this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].husband].traits.health > 10)))
        && ((((this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== this.constantService.MALE) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 45) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].age > 13))
          && ((this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].husband].age < 70) && (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].husband].age > 13)))))

    ) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].number_of_children <= 11) && (!this.homeService.allPerson[this.constantService.FAMILY_SELF].pregnant)) {
      this.modalWindowShowHide.showWaitScreen = true;
      let bornBaby: boolean = false, wife, husband;
      //check wife object which
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === (this.constantService.MALE)) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover != null) {
          wife = this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover]
          husband = this.homeService.allPerson[this.constantService.FAMILY_SELF]
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife != null) {
          wife = this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife]
          husband = this.homeService.allPerson[this.constantService.FAMILY_SELF]
        }
      }
      else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== (this.constantService.MALE)) {
        wife = this.homeService.allPerson[this.constantService.FAMILY_SELF]
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover != null) {
          husband = this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover]
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].husband != null) {
          husband = this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].husband]
        }

      }
      //
      if ((!wife.infertile) && (!husband.infertile) && (wife.traits.health > 0)) {
        if (wife.age < 34 && wife.age > 19 && wife.traits.health > 30) {
          if ((this.homeService.allPerson[this.constantService.FAMILY_SELF
          ].country.BirthRate > this.commonService.getRandomValue(0, 60)) && this.homeService.allPerson[this.constantService.FAMILY_SELF].number_of_children < 12) {
            bornBaby = true
          }
          else {
            bornBaby = false
          }
        }
        // make sure that births occur according to birth rate and that big gaps between children are rare

        else if (wife.age > 34 && wife.age < 42 || wife.age < 19 && wife.age > 14 && wife.living_at_home) {
          if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].country.BirthRate > this.commonService.getRandomValue(0, 90,)) && this.homeService.allPerson[this.constantService.FAMILY_SELF].number_of_children < 12) {
            bornBaby = true
          }
          else {
            bornBaby = false
          }
        }
      }
      if (bornBaby)
      //  if(((this.commonService.getRandomValue(0,1)<0.7) &&(this.homeService.allPerson[this.constantService.FAMILY_SELF].age>15) &&(this.homeService.allPerson[this.constantService.FAMILY_SELF].age<28)) ||(this.commonService.getRandomValue(0,1)<0.4))
      {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex !== (this.constantService.MALE)) {
          this.homeService.allPerson[this.constantService.FAMILY_SELF].pregnant = true;
          this.modalWindowShowHide.showWaitScreen = false;
          this.messageText = this.translation.instant('gameLabel.You_are_pregnant!');
          this.msgClass = "successpopup";
          this.successMsgFlag = true;
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].married) {
          this.modalWindowShowHide.showWaitScreen = false;
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife != null) {
            this.homeService.allPerson[this.constantService.FAMILY_SELF].pregnant = true;
            this.messageText = this.translation.instant('gameLabel.Your_wife_is_pregnant!');
            this.successMsgFlag = true;
            this.msgClass = "successpopup";
          }
        }
        else {
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover != null) {
            this.homeService.allPerson[this.constantService.FAMILY_SELF].pregnant = true;
            this.modalWindowShowHide.showWaitScreen = false;
            this.messageText = this.translation.instant('gameLabel.Your_girlfriend_is_pregnant!');
            this.successMsgFlag = true;
            this.msgClass = "successpopup";
          }
        }
        this.relationService.tryToHaveChild(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
          res => {
            this.result = res;
            this.commonActionService.getActionIdentityDiffrence(this.result, this.homeService.allPerson);
            this.modalWindowShowHide.showWaitScreen = false;
          });

      }
      else {
        this.modalWindowShowHide.showWaitScreen = false;
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === (this.constantService.MALE)) {
          if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].married) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null)) {
            this.messageText = this.translation.instant('gameLabel.Your_wife_did_not_get_pregnant.');
            this.successMsgFlag = true;
            this.msgClass = "errorpopup";


          }
          else {
            this.messageText = this.translation.instant('gameLabel.Your_girlfriend_did_not_get_pregnant.');
            this.successMsgFlag = true;
            this.msgClass = "errorpopup";


          }
        }

        else {
          this.messageText = this.translation.instant('gameLabel.You_did_not_get_pregnant.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";


        }
      }
    }
    else {
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === (this.constantService.MALE)) {
        if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].married) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null) && (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].sex !== this.constantService.MALE)) {
          this.messageText = this.translation.instant('gameLabel.Your_wife_did_not_get_pregnant.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null && this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].wife].sex) {
          this.messageText = this.translation.instant('gameLabel.Natural_conception_is_not_possible_in_a_same_sex_relationship.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
        }
        else if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null) && (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex !== this.constantService.MALE)) {
          this.messageText = this.translation.instant('gameLabel.Your_girlfriend_did_not_get_pregnant.');
          this.successMsgFlag = true;
          this.msgClass = "errorpopup";
        }


        //
      }
      else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].wife !== null && this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover].sex) {
        this.messageText = this.translation.instant('gameLabel.Natural_conception_is_not_possible_in_a_same_sex_relationship.');
        this.successMsgFlag = true;
        this.msgClass = "errorpopup";
      }
      else {
        this.messageText = this.translation.instant('gameLabel.You_did_not_get_pregnant.');
        this.successMsgFlag = true;
        this.msgClass = "errorpopup";
      }
    }
  }
  okSuccessWindow() {
    this.successMsgFlag = false;
    this.modalWindowShowHide.showActionRelation = false;
  }
  close() {
    this.modalWindowShowHide.showActionRelation = false;
  }
}
