import { Pipe, PipeTransform } from '@angular/core';
import { Job } from './job.mode';

@Pipe({
  name: 'filterPipe'
})
export class JobpipePipe implements PipeTransform {
  jobListdata:Job [];
  job:Job[];

  transform(value: Array<Job>, args: any, args1: any): Array<Job>
  {
    value.sort((a: any, b: any) => {
      if ( a[args1] > b[args1] ){
        return -1;
      }else if( a[args1] < b[args1] ){
        return 1;
      }else{
        return 0;
      }
    });
    if(args===false){
      this.jobListdata=value;
        }
    else if(args===true){
      this.jobListdata= value.filter(
        job => job.eligible === true);
          }
          return this.jobListdata;

  }

}
