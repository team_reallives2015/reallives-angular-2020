import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionCarrerComponent } from './action-carrer.component';

describe('ActionCarrerComponent', () => {
  let component: ActionCarrerComponent;
  let fixture: ComponentFixture<ActionCarrerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionCarrerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionCarrerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
