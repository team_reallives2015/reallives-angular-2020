import { Pipe, PipeTransform } from '@angular/core';
import { Business } from './business';

@Pipe({
  name: 'businessPipe'
})
export class BusinessPipe implements PipeTransform {

  businessListdata:Business[];
  transform(value: Business[], args: any): any
  {

    this.businessListdata=value;

    if (value==null) {
      return null;
    }
    else{
      if(args===false){
        this.businessListdata=value;
      }
      else if(args===true){
        this.businessListdata=value.filter(
          business => business.eligible===true);

      }
    }
    return this.businessListdata;
  }

}