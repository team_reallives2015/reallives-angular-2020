import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { HomeService } from '../home.service';

@Injectable({
  providedIn: 'root'
})
export class CarrerService {
  businessType = [];
  loanFlag: boolean = false;
  oldBusinessFlag: boolean = false;
  constructor(private http: HttpClient,
    public translation: TranslateService,
    public homeService: HomeService,
    public commonService: CommonService,
    public constantService: ConstantService) { }

  getAllJobList(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/career/getAllAplicableJobs/${gameId}`);

  }

  saveCareer(gameId, playerId, myCareer) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/career/saveCareer/${gameId}/${playerId}`, { myCareer });

  }

  quitJob(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/career/quitJob/${gameId}`);

  }

  workOverTimeStartStop(gameId, workTimeFlag, income) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/career/extraWorkTime/${gameId}/${workTimeFlag}`, { income });

  }

  askForRaise(gameId, raiseReceivedFlag, raiseObject) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/career/setRaise/${gameId}/${raiseReceivedFlag}`, { raiseObject });

  }


  getAllBusinessList(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/career/getAllAplicableBusinessList/${gameId}`);

  }

  saveBusiness(gameId, myCareer) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/career/saveBusiness/${gameId}`, { myCareer });

  }



  stopBusiness(gameId, myCareer) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/career/stopBusiness/${gameId}`, { myCareer });
  }

  manageBusiness(gameId, myCareer) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/career/manageBusiness/${gameId}`, { myCareer });
  }


  createBusinessTypeArray(selectedBusinessType, flag, no) {
    this.businessType = [{
      BusinessType: this.translation.instant('gameLabel.Micro'),
      Value: 1,
      disabled: false,
      valueForScaleupBusiness:""
    },
    {
      BusinessType: this.translation.instant('gameLabel.Small'),
      Value: 2,
      disabled: false,
      valueForScaleupBusiness:""
    },
    {
      BusinessType: this.translation.instant('gameLabel.Medium'),
      Value: 3,
      disabled: false,
      valueForScaleupBusiness:""
    },
    {
      BusinessType: this.translation.instant('gameLabel.Large'),
      Value: 4,
      disabled: false,
      valueForScaleupBusiness:""
    }]
    if (flag) {
      for (let i = 0; i < this.businessType.length; i++) {
        if (no === 1) {
          if (this.businessType[i].Value === selectedBusinessType) {
            this.businessType[i].disabled = false;
          }
          else {
            this.businessType[i].disabled = true;
          }
        }
        else {
          if (this.businessType[i].Value > selectedBusinessType) {
            this.businessType[i].disabled = true;
          }
        }
        this.businessType[i].valueForScaleupBusiness="";
      }
    }
    else {
      for (let i = 0; i < this.businessType.length; i++) {
        let lastSelected = selectedBusinessType - 1
        if (this.businessType[i].Value === lastSelected) {
          this.businessType[i].disabled = true;
          this.businessType[i].valueForScaleupBusiness=this.translation.instant('gameLabel.current');
        }
        else if(this.businessType[i].Value<lastSelected){
          this.businessType[i].valueForScaleupBusiness=this.translation.instant('gameLabel.Scale_down');
        }
        else if(this.businessType[i].Value>lastSelected){
          this.businessType[i].valueForScaleupBusiness=this.translation.instant('gameLabel.Scale_up');

        }
      }
    }
    return this.businessType;

  }



}
