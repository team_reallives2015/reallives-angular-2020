
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { interval } from 'rxjs';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { FinanceService } from '../action-finance/finance.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { CommonActionService } from '../common-action.service';
import { EventService } from '../event/event.service';
import { FamilyCapsuleService } from '../family-capsule/family-capsule.service';
import { HomeService } from '../home.service';
import { UtilityService } from '../utility-bar/utility.service';
import { CarrerService } from './carrer.service';
import { TranslationService } from 'src/app/translation/translation.service';

@Component({
  selector: 'app-action-carrer',
  templateUrl: './action-carrer.component.html',
  styleUrls: ['./action-carrer.component.css']
})


export class ActionCarrerComponent implements OnInit {
  //default emp
  unSkillDef
  skilledDef;
  semiSkilledDef;

  showOldBusiness: boolean = false;
  manageBusinessMAxSelfSal = 0
  takenBankLoan = 0;
  moreInfoFlag: boolean = false;
  selfEducation = '';
  cancleBusinessFlag: boolean = false;
  startBusinessInfoFlag: boolean = false;
  startBUsinessCOuntryInfoArray = [];
  disableCashCur: boolean = false;
  disableloanCur: boolean = false;
  disablesharkCur: boolean = false;
  aveFamilyLoan = 0;
  takenFamilyLoan = 0;
  notStartBusinessFlag: boolean = false;
  oldSelectedBankLoan = 0;
  oldSelectedAmountFromLoanWindow = 0;
  oldSelectedBusinessCash = 0;
  oldSelectedFriendLoan = 0
  oldSelectedHouseholdCash = 0
  oldSelectedSharkLoan = 0;
  oldFriendValue = 0;
  oldFreindIndex = 0
  oldFcet = 0;
  setBusinessNameFlag: boolean = false;
  selectedBusinessName = '';
  calculaterFlag: boolean = false;
  text = ""
  cancelConfirmFlag: boolean = false;
  oldSpaceCost = 0;
  initialSpaceCost = 0;
  total = 0;
  selectedBusinessId;
  mCapitalClass = "pr-3 textSize"
  mexpClass = "pr-3 textSize";
  manageBusinessExp;
  manageBusinessCapital;
  firstScreen: boolean = false;
  secondScreen: boolean = false;
  thirdScreen:boolean=false;
  friendLoanArr = [];
  friendLoanValue = 2;
  stopBusinessOptionArr = [];
  selctedStopOption = ""
  selctedStopValue = 0
  disabplePayButtonBankLoan: boolean = false;
  disabplePayButtonSharkLoan: boolean = false;
  stopBusinessTotalCash = 0;
  stopBusinessTotalLoans = 0;
  stopBusinessPayLoansFlag: boolean = false;
  stopBusinesspayLoanWindow: boolean = false;
  stopBusinessFlag: boolean = false;
  skilledMax = 0;
  unskillledMax = 0;
  semiskilledMax = 0
  skilledMin = 0;
  unskillledMin = 0;
  semiskilledMin = 0
  unskilledMMax = 0;
  semiskilledMMax = 0;
  skilledMMax = 0;
  unskilledFMax = 0;
  semiskilledFMax = 0;
  skilledFMax = 0;
  startupCapitalwithoutLabourExp = 0;
  selectedCashClass = "";
  selfSalary = 0;
  orgFloan = 0;
  fCnt = 0;
  flagForFriend: boolean = false;
  setFriendLoanFirstFlag: boolean = false;
  takenFriendLoan = 0;
  takenLoanAmt = 0;
  takenSharkAmt = 0;
  classForLoanValue = ""
  selectedAmountFromLoanWindow = 0;
  friendValue = 0;
  loanWindoFlag: boolean = false;
  selectedBusinessCash = 0;
  selectedBankLoan = 0;
  selectedFriendLoan = 0;
  selectedSharkLoan = 0;
  businessHouseHoldCash;
  expencesForEmpScreen;
  labourExp;
  cashEmpClass = "";
  expeEmpClass = "";
  originalHouseholdCash = 0;
  loanForCarrer = 0;
  originalStratupCapital = 0;
  bank: boolean = false;
  shark: boolean = false;
  takenSharkLoanAmount = 0;
  buainessLoan;
  bankLoanObj;
  sharkLoanObj;
  takeLoan: boolean = false;
  loanMsg = '';
  regUnregValue = 1;
  regArr = [];
  originalBribeCost = 0;
  originalComplianceCost = 0
  calculatedBusinessType = 0;
  salaryDifferencPer;
  oldfamilyObject = [];
  welthIncome = 0;
  alertFlag: boolean = false;
  wealthDeviation = 0;
  sigmaDistanceUnskilled = -2.00
  sigmaDistanceSemiskilled = -1.50
  sigmaDistanceSkilled = -1
  initialStartup_capital = 0;;
  localMinimumWage;
  totalMonthlyExp: number = 0;
  totalUnSkilAmount = 0;
  totalSemiSkilAmount = 0;
  totalSkilAmount = 0;
  monthlyExpenceAme = 0;
  monthlyExpenceLabour;
  perUnSkileedCost = 0;
  perSkileedCost = 0;
  perSemiSkilledCost = 0;
  amenitiesCost = 0;
  workingCapital = 0;
  businessAmenities = [];
  amenitiesCount = 0;
  complentionCost = 0;
  unSkillEmp = 0;
  semiSkillEmp = 0;
  skiledEmp = 0;
  totalYeaExp = 0
  luckyStart: boolean = false;
  bribeCost = 0
  payBribeFlag: boolean = false
  curuptionWarrFlag: boolean = false
  registerBusinessWarr: boolean = false;
  value: boolean = false;
  famMenberCount = 0;
  businessTypeFlag: boolean = false;
  profitPerValue = 5
  businessType = "";
  showJobListFlag: boolean = false;
  allPerson;
  mainPerson;
  job;
  check = ""
  allJobList = [];
  manageBusinessFlag: boolean = false;
  cardBusinessTyp = "";
  cashClass = "";
  loanClass = "";
  SharkClass = "";
  flagToShowBankLoan: boolean = false;
  flagToShowSharkLoan: boolean = false;
  diffBvaleCash = 0;
  //
  businessTypeWindow: boolean = false;
  selectedBusinessType = 0;
  selectedBusinessTypeName = "";
  familyMemberList = [];
  worlPlaceType = [];
  selectedWorlPlaceValue = 0;
  selectedWorlPlaceName = 0;
  amenityCost;
  amenitiesFlag: boolean = false
  totalSpaceCost;
  funskilled = 0;
  fsemiSkilled = 0; fskilled = 0
  workplaceId = 0;
  profitmargine = 0
  humanCapital = 0
  businessSigmaSpread
  empSelectionFlag: boolean = false;
  maleSkilled = 0;
  maleSemiskilled = 0;
  maleUnskilled = 0;
  femaleSkilled = 0;
  femaleSemiskilled = 0;
  femaleUnskilled = 0;
  perUnskilleFCost = 0;
  perSemiskilleFCost = 0;
  perSkilleFCost = 0;
  perUnskilleMCost = 0;
  perSemiskilleMCost = 0;
  perSkilleMCost = 0;
  cardRegister;
  cardBusinessType;
  cardGrothPer;
  totalEmp = 0
  flagForSameSal: boolean = false;
  flagForMaleSal: boolean = false;
  salaryFlag: boolean = false;
  salaryChoice = [];
  selectedChoice = 1;
  futureBusinessValue = 0;
  scaleupBusinessFlag: boolean = false;
  scalupRrgisterWindowFlag: boolean = false;
  deletedmaleSkilled = 0;
  deletedmaleSemiskilled = 0;
  deletedmaleUnskilled = 0;
  deletedfemaleSkilled = 0;
  deletedfemaleSemiskilled = 0;
  deletedfemaleUnskilled = 0;

  addedmaleSkilled = 0;
  addedmaleSemiskilled = 0;
  addedmaleUnskilled = 0;
  addedfemaleSkilled = 0;
  addedfemaleSemiskilled = 0;
  addedfemaleUnskilled = 0;

  maxSmall = 30
  maxMedium = 90
  maxlarge = 1600

  minSmall = 1
  minMedium = 30
  minlarge = 90
  famMunskilled = 0;
  famFunskilled = 0;

  famMsemiskilled = 0;
  famFsemiskilled = 0;

  famMskilled = 0;
  famFskilled = 0;
  familyHumanCapital = 0;

  oldfemaleUnskilled = 0;
  oldfemaleSemiskilled = 0;
  oldfemaleSkilled = 0;
  sameSalaryFlagForStartBusiness: boolean = false;
  aveLoanAmount;
  sharkLoanAmount = 0
  //
  business;
  allBusinessList = [];
  eligiblejobdataflag: boolean = false;
  selectedJobIndex;
  showAlertFlag: boolean = false;
  alertClass;
  alertMsg;
  alertType;
  messageText;
  messageType;
  messageClass = "alert-info";
  MessageShow;
  successMsgFlag: boolean = false;
  selectedJob;
  selectedJobId;
  showJobConfirmWindow: boolean = false;
  saveCarrerObject;
  allSaveCarrerObject;
  updatedObject;
  allCarrerCards: [] = [];
  firstCards = [];
  cardLength;
  default = 0;
  income;
  registerFlag: boolean = false;
  payRaisePercentage: any;
  quitJobFlag: boolean = false;
  getJobFlag: boolean = false;
  showBusinessList: boolean = false;
  eligiblejBusinessdataflag: boolean = false;
  houseHoldCash;
  businessId;
  selectedBusiness;
  minimumInvestment;
  yearlyInvestMent;
  showBusinessWindow;
  businessSetupWindowShow: boolean = false;
  startup_capital = 0;
  yearly_investment: number;
  dispalyAllCards: [] = [];
  quitBusinessFlag: boolean = false;
  startBusiness: boolean = false;
  result;
  diiferenBetIdentity;
  identity = [];
  identityValues = [];
  dataGet: boolean = false;
  popupDispalyMimInv;
  popupDisYearInv;
  popupDisCash;
  age;
  diablegetJob: boolean = false;
  flagForBusinness: boolean = false;
  weekTraitName;
  businessTypeList;
  smallDisable;
  smallValue;
  smallScaleup;
  microDisable;
  microValue;
  microScaleup;
  mediumValue;
  mediumDisable;
  mediumScaleup;
  largeValue;
  largeDisable;
  largeScaleup;
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public homeService: HomeService,
    public constantService: ConstantService,
    public carrerService: CarrerService,
    public commonService: CommonService,
    public agaAYearService: AgaAYearService,
    public eventService: EventService,
    public translation: TranslateService,
    public familyCapsuleService: FamilyCapsuleService,
    private commonActionService: CommonActionService,
    public gameSummeryService: GameSummeryService,
    public utilityService: UtilityService,
    public translationService:TranslationService,
    public fianance: FinanceService) {
    interval(30000).subscribe(x => {
      if (this.showAlertFlag) {
        this.showAlertFlag = false;
      }
    });
  }

  ngOnInit(): void {
    this.allPerson = this.homeService.allPerson;
    this.homeService.allPerson[this.constantService.FAMILY_SELF] = this.homeService.allPerson[this.constantService.FAMILY_SELF];
    this.age = this.homeService.allPerson[this.constantService.FAMILY_SELF].age;
    this.houseHoldCash = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash;
    this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
    this.cardLength = this.allCarrerCards.length;
    this.carrerService.oldBusinessFlag = this.commonService.checkBusinessTyoeOldOrNew(this.homeService.allPerson[this.constantService.FAMILY_SELF].business)
    if (this.cardLength == 1) {
      this.firstCards.push(this.allCarrerCards[this.default]);
      this.allCarrerCards = [];
    }
    else if (this.allCarrerCards.length > 1) {
      this.firstCards.push(this.allCarrerCards[this.cardLength - 1]);
      this.allCarrerCards.reverse();
    }
    if (this.firstCards[0].careerType === "business" && !this.carrerService.oldBusinessFlag) {
      this.setBusinessCardValues(this.firstCards);
    }

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
      this.alertMsg = this.translation.instant('gameLabel.You_cannot_take_any_action_because_you_are_dead');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 6 && !(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.inSchool))
      && !(this.homeService.allPerson[this.constantService.FAMILY_SELF].dead)) {
      this.alertMsg = this.translation.instant('gameLabel.You_cannot_take_up_a_job_or_start_a_business_before_you_are_eleven_year_old');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 11 && (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.inSchool) && !(this.homeService.allPerson[this.constantService.FAMILY_SELF].dead)) {
      this.alertMsg = this.translation.instant('gameLabel.You_cannot_take_up_a_job_or_start_a_business_before_you_are_eleven_year_old');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }

  }
  getAJobClick() {
    this.eligiblejobdataflag = false;
    this.allJobList = [];
    this.showJobListFlag = true;
    this.showBusinessList = false;
    this.carrerService.getAllJobList(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.job = res;
        for (let i in this.job) {
          this.allJobList.push(this.job[i]);
        }
        this.dataGet = true;
        this.eligiblejobdataflag = true;
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].job.jobName === "Soldier" && this.homeService.allPerson[this.constantService.FAMILY_SELF].yearsOfMilitaryService) {
          this.messageText = this.translation.instant('gameLabel.You_cannot_leave_the_military_until_youve_completed_your_tour_of_duty');
          this.messageClass = "errorpopup";
          this.successMsgFlag = true;

        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.inSchool ||
          this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.inVocational ||
          this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.inCollege ||
          this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.inGraduate) {

          this.messageText = this.translation.instant('gameLabel.Because_you_are_attending_school_you_will_only_be_able_to_work_part_time.');
          this.messageClass = "errorpopup";
          this.successMsgFlag = true;
          this.diablegetJob = true;
        }
        else {
          this.diablegetJob = true;
        }

      }

    )
  }
  getABusinessOld() {
    this.carrerService.oldBusinessFlag = true;
    let count = 0;
    this.eligiblejBusinessdataflag = false;
    this.allBusinessList = [];
    this.showBusinessList = true;
    this.showJobListFlag = false;
    this.carrerService.getAllBusinessList(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.business = res;
        this.diablegetJob = true;
        for (let i in this.business) {
          if (this.business[i].eligible) {
            count = count + 1;
          }
          this.allBusinessList.push(this.business[i]);

        }
        if (count == 0) {
          this.messageText = this.translation.instant('gameLabel.Not_applicable_to_take_any_business');
          this.successMsgFlag = true;
        }
        this.eligiblejBusinessdataflag = true;

      }

    )
  }
  getABusiness() {
    this.carrerService.oldBusinessFlag = false;
    let count = 0;
    this.eligiblejBusinessdataflag = false;
    this.allBusinessList = [];
    this.showBusinessList = true;
    this.showJobListFlag = false;
    this.carrerService.getAllBusinessList(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.business = res;
        let wagein$ = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ProdperCapita / 2.5
        this.localMinimumWage = wagein$ * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
        this.localMinimumWage = this.localMinimumWage.toFixed(0);
        this.diablegetJob = true;
        for (let i in this.business) {
          if (this.business[i].eligible) {
            count = count + 1;
          }
          this.allBusinessList.push(this.business[i]);

        }
        if (count == 0) {
          this.messageText = this.translation.instant('gameLabel.Not_applicable_to_take_any_business');
          this.successMsgFlag = true;
        }
        this.eligiblejBusinessdataflag = true;
        this.countryInfoArray();
      }

    )
  }
  displayJobAsPerIncome(job) {
    let sal;
    let exp = Math.exp(this.gameSummeryService.ecoStatusData.meanIncome + (job.SalarySigmaSpread * this.gameSummeryService.ecoStatusData.incomeDeviation));
    var income = exp;
    income = income / 12;
    sal = income * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 18 || this.homeService.allPerson[this.constantService.FAMILY_SELF].age > 65) {
      sal = sal / 1.5;
    }

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].part_time) {
      sal = sal / 2.0;
    }
    return sal;
  }
  closeBusinessList() {
    this.showBusinessList = false;
    this.diablegetJob = false;
  }
  seletedRadioButtonValueForJob(job) {
    this.showOldBusiness = false;
    this.selectedJobId = job.jobId;
    this.selectedJob = job;
    this.messageText = this.translation.instant('gameLabel.Do_you_really_want_to_apply_for_this_job?');
    this.showJobConfirmWindow = true;
    this.getJobFlag = true;
    this.carrerService.oldBusinessFlag = false;
  }



  selectRedioButtonValueForBusiness(business) {
    this.showOldBusiness = false;
    this.selectedBusinessId = business.businessesId;
    this.setBusinessCheck();
    this.setAllCostToZero();
    this.businessId = business.businessesId;
    this.selectedBusinessType = business.business_type
    this.selectedBusiness = business;
    this.selectedBusiness.BusinessSigmaSpread
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    }
    this.originalHouseholdCash = this.businessHouseHoldCash;
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    if (!(this.selectedBusiness.eligible)) {
      this.messageText = this.translation.instant('gameLabel.Not_applicable_for_these_businesses');
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
    else {
      this.checkAndCalculateInitialCostOfBusiness();
      if (!this.notStartBusinessFlag) {
        this.messageText = this.translation.instant('gameLabel.What size of business would you want to start?')
        this.businessTypeWindow = true;
      }
      else {
        this.businessTypeWindow = false;
      }
    }
  }
  selectRedioButtonValueForOldBusiness(business) {
    this.showOldBusiness = true;
    this.businessId = business.businessesId;
    this.selectedBusinessId = business.businessesId
    this.setBusinessCheck();
    this.selectedBusiness = business;
    if (!(this.selectedBusiness.eligible)) {
      this.messageText = this.translation.instant('gameLabel.Not_applicable_for_these_businesses');
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
    else {
      this.minimumInvestment = Math.round(this.selectedBusiness.MinimumInvestment);
      this.yearlyInvestMent = Math.round((70 / 100) * this.minimumInvestment);
      this.startup_capital = Math.round(this.minimumInvestment);
      this.yearly_investment = Math.round(this.yearlyInvestMent);
      this.popupDispalyMimInv = Math.round(this.minimumInvestment);
      this.popupDisYearInv = Math.round(this.yearlyInvestMent);
      this.popupDisCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash);
      this.businessSetupWindowShow = true;
      this.showBusinessWindow = true;
      this.startBusiness = true;
      this.carrerService.oldBusinessFlag = true
    }

  }




  setBusinessCheck() {
    for (let i = 0; i < this.allBusinessList.length; i++) {
      if (this.allBusinessList[i].businessesId === this.selectedBusinessId) {
        this.allBusinessList[i].check = 'check';
        break;
      }
    }
  }

  cancelBusinessCheck() {
    for (let i = 0; i < this.allBusinessList.length; i++) {
      if (this.allBusinessList[i].businessesId === this.selectedBusinessId) {
        this.allBusinessList[i].check = '';
        break;
      }
    }
    this.selectedBusinessId = 0;
  }

  setBusinessName() {
    this.setBusinessNameFlag = true;
  }

  BusineeTypeOk() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    } this.originalHouseholdCash = this.businessHouseHoldCash;
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.selectedBankLoan = 0;
    this.selectedFriendLoan = 0;
    this.selectedSharkLoan = 0;
    this.carrerService.loanFlag = false
    this.profitPerValue = 5
    if (this.selectedBusinessType === 1) {
      this.setInitialValueForMicroBusiness();
      this.calculateAvalabelLoanAmount();
    }
    else if (this.selectedBusinessType === 2) {
      this.setInitialValuesForSmallBusiness()
      this.calculateAvalabelLoanAmount();
    }
    else if (this.selectedBusinessType === 3) {
      this.setInitialValuesForMediumBusiness()
      this.calculateAvalabelLoanAmount();

    }
    else if (this.selectedBusinessType === 4) {
      this.setInitialValuesForLargeBusiness();
      this.calculateAvalabelLoanAmount();
    }
    this.selectedBusinessTypeName = this.businessTypeList[this.selectedBusinessType - 1].BusinessType
    this.businessTypeWindow = false;
    this.showJobConfirmWindow = true;
    this.businessTypeFlag = true;
    this.calculateHumanCapital(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
    this.calculateFamilyHumanCapital();
    this.checkAllExepence();
    this.messageText = this.translation.instant('gameLabel.Do you want to start your business as registered or unregistered?')

  }
  businessTypeClose() {
    this.businessTypeWindow = false;
    this.selectedBusiness = [];
    this.cancelBusinessCheck();

  }
  calculateCompriationCost(totalCapital) {
    this.complentionCost = 0
    let capitalTen = (totalCapital * 10) / 100
    let inDollerConversionCost = 10 * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
    let max = Math.max(capitalTen, inDollerConversionCost);
    this.complentionCost = max;
    this.originalComplianceCost = this.complentionCost;
    this.checkAllExepence();
  }
  startBusinessYes() {
    if (this.startup_capital > this.selectedAmountFromLoanWindow) {
      this.carrerService.loanFlag = true
      this.translation.get('gameLabel.The_amount_of_initial_cash_you_are_trying_to_invest_is_greater_than_the_cash_currently_available_with_you', { houseHoldCash: Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
        this.messageText = s;
        this.messageClass = "errorpopup";
        this.successMsgFlag = true;
      })
    }
    else {
      this.firstScreen = false;
      this.secondScreen = false;
      this.thirdScreen=false;
      this.luckyStart = false;
      this.showBusinessWindow = false;
      this.showBusinessList = false;
      this.businessSetupWindowShow = false;
      this.showBusinessWindow = false;
      this.startBusiness = false;
      this.diablegetJob = false;
      this.modalWindowShowHide.showWaitScreen = true;
      this.setBusinessObjectToSave();
      this.carrerService.saveBusiness(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.saveCarrerObject).subscribe(
        res => {
          this.modalWindowShowHide.showWaitScreen = false; this.firstCards = [];
          this.allCarrerCards = [];
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
          this.firstCards.push(this.allCarrerCards[this.allCarrerCards.length - 1]);
          if (this.firstCards[0].careerType === "business") {
            this.setBusinessCardValues(this.firstCards)
          }
          this.allCarrerCards.reverse();
          this.translation.get('gameLabel.You_successfully_started_a_business', { businessName: this.selectedBusiness.Business, name: this.selectedBusinessName }).subscribe((s: string) => {
            this.messageText = s;
            this.successMsgFlag = true;
            this.carrerService.loanFlag = false;
          })
        })
    }

  }
  registerBusinessYes() {
    this.businessTypeFlag = false;
    this.payBribeFlag = false;
    this.bribeCost = 0;
    this.complentionCost = 0;
    this.showJobConfirmWindow = false;
    this.businessTypeFlag = false
    this.registerFlag = true;
    this.getNameOfBusinessType();
    this.setRegisterUnregister();
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.corruption < 60) {
      this.curuptionWarrFlag = true;
      this.showJobConfirmWindow = true;
      this.translation.get('gameLabel.busWarr', { country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country }).subscribe((s: string) => {
        this.messageText = s;

      })
      // this.messageText = "Starting a registered business in " + this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country + "  takes a lot of time.  Would you still like to give it a try?"
    }
    else {
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
        this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
      }
      else {
        this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
          this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
          + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
      } this.originalHouseholdCash = this.businessHouseHoldCash;
      if (this.businessHouseHoldCash <= 0) {
        this.businessHouseHoldCash = 0;
      }
      this.registerFlag = true;
      this.setFamilyMemberList();
      this.calculateCompriationCost(this.startup_capital);
      this.checkAllExepence();
      this.TypeOfWorlPlaces(this.selectedBusinessType);
      this.showBusinessWindow = true;
      this.startBusiness = true;
      this.firstScreen = true;
    }
  }
  takeAtimeYes() {
    this.curuptionWarrFlag = false;
    this.showJobConfirmWindow = false;
    let ran = this.commonService.getRandomValue(0, 1)
    if (ran < 0.5) {
      this.registerFlag = true;
      this.successMsgFlag = true;
      this.luckyStart = true;
      this.setRegisterUnregister();
      this.setFamilyMemberList();
      this.calculateCompriationCost(this.startup_capital);
      this.TypeOfWorlPlaces(this.selectedBusinessType);
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
        this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
      }
      else {
        this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
          this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
          + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
      } this.originalHouseholdCash = this.businessHouseHoldCash;
      if (this.businessHouseHoldCash <= 0) {
        this.businessHouseHoldCash = 0;
      }
      this.messageText = this.translation.instant('gameLabel.You are fortunate to have started your own business without having to pay any bribes or face any problems.')
      this.checkAllExepence();
    }
    else {
      this.curuptionWarrFlag = false;
      this.registerBusinessWarr = true;
      this.showJobConfirmWindow = true;
      this.messageText = this.translation.instant('gameLabel.An official is asking for a bribe to start the registered business.What will you do?')

    }
  }
  payBribe() {
    this.messageText = this.translation.instant('gameLabel.An official is asking for a bribe to start the registered business.What will you do?')
    this.bribeCost = 0;
    this.payBribeFlag = true;
    this.registerBusinessWarr = true;
    this.showJobConfirmWindow = true;
    this.registerFlag = true;
    this.calculateCompriationCost(this.startup_capital);
    this.bribeCost = (this.startup_capital * 15 / 100)
    this.originalBribeCost = this.bribeCost;
    this.checkAllExepence();
    this.setRegisterUnregister();
    this.TypeOfWorlPlaces(this.selectedBusinessType);
    this.firstScreen = true;
    this.showBusinessWindow = true;
    this.startBusiness = true;
    this.setFamilyMemberList();
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    } this.originalHouseholdCash = this.businessHouseHoldCash;
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.checkAllExepence();
    this.firstScreen = true;
    this.registerBusinessWarr = false;
    this.showJobConfirmWindow = false;
  }
  doNaotStartBusiness() {
    this.registerBusinessWarr = false;
    this.showJobConfirmWindow = false;
    this.curuptionWarrFlag = false;
    this.showBusinessList = false;
    this.showBusinessWindow = false;
    this.businessSetupWindowShow = false;
    this.startBusiness = false;
    this.diablegetJob = false;
    this.selectedBusinessName = ''
  }
  startBusinessNo() {
    this.showBusinessList = false;
    this.showBusinessWindow = false;
    this.businessSetupWindowShow = false;
    this.startBusiness = false;
    this.diablegetJob = false;
    this.selectedBusinessName = ''
  }
  starUnregisterBusiness() {
    this.businessSetupWindowShow = false;
    this.complentionCost = 0;
    this.bribeCost = 0;
    this.registerBusinessWarr = false;
    this.showJobConfirmWindow = false;
    this.registerFlag = true;
    this.setRegisterUnregister();
    this.setFamilyMemberList();
    this.TypeOfWorlPlaces(this.selectedBusinessType);
    this.firstScreen = true
    // this.showBusinessWindow = true;
    this.startBusiness = true;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    }
    this.originalHouseholdCash = this.businessHouseHoldCash;
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.checkAllExepence();
  }
  takeATimeNo() {
    this.curuptionWarrFlag = false;
    this.showJobConfirmWindow = false;
    this.registerFlag = false;
    this.setInitialCapital();
    this.getNameOfBusinessType();
    this.setFamilyMemberList();
    this.setRegisterUnregister();
    this.setBusiness(this.regUnregValue);
    this.checkAllExepence();
    this.firstScreen = true;
    // this.businessSetupWindowShow = true;
    this.showBusinessWindow = true;
    this.startBusiness = true;
  }
  doNotStatrtBusiness() {
    this.businessTypeFlag = false
    this.luckyStart = false;
    this.curuptionWarrFlag = false;
    this.showJobConfirmWindow = false;
    this.businessSetupWindowShow = false;
    this.showBusinessWindow = false;
    this.startBusiness = false;
    this.diablegetJob = false;
    this.registerBusinessWarr = false;
    this.firstScreen = false;
    this.secondScreen = false;
    this.thirdScreen=false;
    this.selectedBusinessName = '';
    this.cancelBusinessCheck();

  }
  nonRegisterBusinessYes() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    }
    this.originalHouseholdCash = this.businessHouseHoldCash;
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.showJobConfirmWindow = false;
    this.businessTypeFlag = false
    this.registerFlag = false
    this.setInitialCapital();
    this.getNameOfBusinessType();
    this.setFamilyMemberList();
    this.setRegisterUnregister();
    this.setBusiness(this.regUnregValue);
    this.TypeOfWorlPlaces(this.selectedBusinessType);
    this.checkAllExepence();
    this.firstScreen = true;
    this.showBusinessWindow = true;
    this.startBusiness = true;
  }

  startBusinessCancle() {
    this.text = this.translation.instant('gameLabel.cancleBusWarr')
    this.cancleBusinessFlag = true;
  }

  startBusinessCancleleYes() {
    this.cancleBusinessFlag = false;
    this.setBusinessNameFlag = false;
    this.firstScreen = false;
    this.businessTypeFlag = false;
    this.registerBusinessWarr = false;
    this.curuptionWarrFlag = false
    this.secondScreen = false
    this.thirdScreen=false;
    this.businessId = null;
    this.selectedBusinessType = null
    this.businessTypeList;
    this.luckyStart = false;
    this.businessSetupWindowShow = false;
    this.showBusinessWindow = false;
    this.flagForFriend = false
    this.selectedBusinessName = '';
    this.selectedBusinessCash = 0;
    this.selectedBankLoan = 0;
    this.selectedSharkLoan = 0;
    this.selectedFriendLoan = 0;
    this.selectedFriendLoan = 0;
    this.fCnt = 0;
    this.friendLoanValue = 2
    this.oldSelectedBusinessCash = 0;
    this.oldSelectedBankLoan = 0;
    this.oldSelectedHouseholdCash = 0;
    this.oldSelectedSharkLoan = 0;
    this.oldFcet = 0
    this.oldFreindIndex = 2;
    this.oldFriendValue = 0;
    this.businessTypeWindow = false;
    this.cancelBusinessCheck();
  }

  startBusinessCancelNo() {
    this.cancleBusinessFlag = false;
  }

  checkProfitValue(value) {
    this.profitPerValue = value;
  }

  getNameOfBusinessType() {
    if (this.registerFlag) {
      this.businessType = this.translation.instant('gameLabel.Registered')
      this.cardBusinessType = this.translation.instant('gameLabel.Yes')
    }
    else if (!this.registerFlag) {
      this.businessType = this.translation.instant('gameLabel.Non registered')
      this.cardBusinessType = this.translation.instant('gameLabel.No')
    }
  }
  checkBoxValue(flag, type, sex, id) {
    this.monthlyExpenceLabour = 0;
    if (!flag) {
      if (type === 1) {
        if (sex === 'Male') {
          this.maleUnskilled = this.maleUnskilled + 1
          this.famMunskilled = this.famMunskilled + 1;
        }
        else {
          this.femaleUnskilled = this.femaleUnskilled + 1
          this.famFunskilled = this.famFunskilled + 1;
        }
        this.famMenberCount = this.famMenberCount + 1;
        this.calculateWages();
      }
      else if (type === 2) {
        if (sex === 'Male') {
          this.maleSemiskilled = this.maleSemiskilled + 1
          this.famMsemiskilled = this.famMsemiskilled + 1;

        }
        else {
          this.femaleSemiskilled = this.femaleSemiskilled + 1
          this.famFsemiskilled = this.famFsemiskilled + 1;
        }
        this.famMenberCount = this.famMenberCount + 1;
        this.calculateWages();
      }
      else if (type === 3) {
        if (sex === 'Male') {
          this.maleSkilled = this.maleSkilled + 1
          this.famMskilled = this.famMskilled + 1;
        }
        else {
          this.femaleSkilled = this.femaleSkilled + 1
          this.famFskilled = this.famFskilled + 1;
        }
        this.famMenberCount = this.famMenberCount + 1;
        this.calculateWages();
      }
    }
    else if (flag) {
      if (type === 1) {
        if (sex === 'Male') {
          this.maleUnskilled = this.maleUnskilled - 1
          this.famMunskilled = this.famMunskilled - 1;
        }
        else {
          this.femaleUnskilled = this.femaleUnskilled - 1
          this.famFunskilled = this.famFunskilled - 1;
        }
        this.famMenberCount = this.famMenberCount - 1;
        this.calculateWages();
      }
      else if (type === 2) {
        if (sex === 'Male') {
          this.maleSemiskilled = this.maleSemiskilled - 1
          this.famMsemiskilled = this.famMsemiskilled - 1;

        }
        else {
          this.femaleSemiskilled = this.femaleSemiskilled - 1
          this.famFsemiskilled = this.famFsemiskilled - 1;
        }
        this.famMenberCount = this.famMenberCount - 1;
        this.calculateWages();
      }
      else if (type === 3) {
        if (sex === 'Male') {
          this.maleSkilled = this.maleSkilled - 1
          this.famMskilled = this.famMskilled - 1;

        }
        else {
          this.femaleSkilled = this.femaleSkilled - 1
          this.famFskilled = this.famFskilled - 1;
        }
        this.famMenberCount = this.famMenberCount - 1;
        this.calculateWages();
      }


    }
    this.calculateFamemeberSelectionChanges(id, flag);
  }
  stopBusiness() {
    this.businessTypeFlag = false;
    this.messageText = this.translation.instant('gameLabel.stop_business');
    this.stopBusinessFlag = true;
  }


  stopOldBusiness() {
    this.messageText = this.translation.instant('gameLabel.stop_business');
    this.quitBusinessFlag = true;
    this.showJobConfirmWindow = true;

  }

  stopBusinessY() {
    this.stopBusinessFlag = false
    this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
    if (this.selectedBusiness.bankLoan > 0 || this.selectedBusiness.sharkLoan > 0) {
      this.stopBusinessTotalCash = this.selectedBusiness.businessValue;
      this.stopBusinessTotalLoans = this.selectedBusiness.bankLoan + this.selectedBusiness.sharkLoan
      if (this.stopBusinessTotalCash > this.stopBusinessTotalLoans) {
        this.stopBusinessYes();
      }
      else {
        this.selctedStopValue = 1
        this.messageText = this.translation.instant('gameLabel.Your current business cash and household cash are insufficient to pay off your loan amount. Please consider the following options for a possible solution and see how lucky you are.')
        this.stopBusinessPayLoansFlag = true;
        this.stopBusinessOptionArr = [
          {
            "name": this.translation.instant('gameLabel.Ask for help from your friends'),
            "value": 1
          },
          {
            "name": this.translation.instant('gameLabel.Continue paying your loans every year'),
            "value": 3
          }
        ]
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.bankLoan > 0) {
          this.stopBusinessOptionArr.push({
            "name": this.translation.instant('gameLabel.Request for bank loan waiver'),
            "value": 2
          })
        }
        this.selctedStopOption = this.stopBusinessOptionArr[0].name;
      }
    }
    else {
      this.stopBusinessYes();
    }
  }

  stopBusinessN() {
    this.stopBusinessFlag = false
    this.stopBusinessPayLoansFlag = false;

  }

  stopBusinessYes() {
    this.stopBusinessPayLoansFlag = false;
    this.modalWindowShowHide.showWaitScreen = true;
    this.quitBusinessFlag = false;
    this.showJobConfirmWindow = false;
    this.saveCarrerObject = {
      "jobId": 132,
      "currentSalary": 0,
      "careerType": "job",
      "fromAge": this.homeService.allPerson.SELF.age,
      "jobName": "UNEMPLOYED",
      "startUpCapital": this.homeService.allPerson[this.constantService.FAMILY_SELF].business.startUpCapital,
      "yearlyInvestment": this.homeService.allPerson[this.constantService.FAMILY_SELF].business.startUpCapital,
      "businessId": 130,
      "choice": this.selctedStopValue
    };

    this.carrerService.stopBusiness(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.saveCarrerObject).subscribe(
      res => {
        this.modalWindowShowHide.showWaitScreen = false;
        this.firstCards = [];
        this.allCarrerCards = [];
        this.result = res;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
        this.firstCards.push(this.allCarrerCards[this.allCarrerCards.length - 1]);
        if (this.firstCards[0].careerType === "business") {
          this.setBusinessCardValues(this.firstCards)
        }
        this.allCarrerCards.reverse();
        this.messageText = this.translation.instant('gameLabel.You_successfully_stopped_your_business!');
        this.successMsgFlag = true;
        this.carrerService.loanFlag = false;
        this.modalWindowShowHide.showBusinessTabFlag = false;
        this.homeService.activeLinkClassfinanceStatus = "active"
        this.modalWindowShowHide.showFinanceStatus = true;


      }
    )
  }

  stopOldBusinessNo() {
    this.quitBusinessFlag = false;
    this.showJobConfirmWindow = false;
  }

  manageBusiness() {
    this.setAllCostToZero();
    this.carrerService.loanFlag = false
    this.scaleupBusinessFlag = false;
    this.manageBusinessFlag = true;
    this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses <= 0) {
      let sal = Math.round(((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12))
      if (sal > this.selectedBusiness.selfSalary) {
        this.selfSalary = sal
        this.manageBusinessMAxSelfSal = sal;
      }
      else {
        this.selfSalary = this.selectedBusiness.selfSalary;
        this.manageBusinessMAxSelfSal = this.selfSalary;
      }
    }
    else {
      this.selfSalary = this.selectedBusiness.selfSalary;
    }
    this.calculateInitialCapital();
    this.registerFlag = this.selectedBusiness.isRegistered
    this.getNameOfBusinessType();
    this.selectedBusinessType = this.selectedBusiness.businessType;
    this.setInitialCapital();
    this.checkAmenitiesForBusiness();
    this.businessTypeList = this.carrerService.createBusinessTypeArray(this.selectedBusiness.businessType, true, 0);
    this.filValueForSelection();
    this.selectedBusinessTypeName = this.businessTypeList[this.selectedBusiness.businessType - 1].BusinessType
    this.setFamilyMemberList();
    this.changeFamilyMemberSelectionChanges()
    if (this.selectedBusinessType >= 2) {
      this.TypeOfWorlPlaces(this.selectedBusinessType);
    }
    this.setPerPersonSalaryOfEmp();
    this.flagForSameSal = this.selectedBusiness.empSalarySame;
    if (!this.flagForSameSal) {
      this.flagForMaleSal = true
    }
    this.setInitialEmp();
    this.setValuesForEmp();
    this.calculateHumanCapital(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
    this.calculateFamilyHumanCapital();
    this.calculateWages();
    this.calculateAvalabelLoanAmount();
    this.profitPerValue = this.selectedBusiness.profitMarginPer;
    this.maleUnskilled = this.selectedBusiness.unSkilledEmp['male']
    this.femaleUnskilled = this.selectedBusiness.unSkilledEmp['female']
    this.unSkillEmp = this.selectedBusiness.unSkilledEmp['totalUnskilled']
    this.maleSemiskilled = this.selectedBusiness.semiSkilledEmp['male']
    this.femaleSemiskilled = this.selectedBusiness.semiSkilledEmp['female']
    this.semiSkillEmp = this.selectedBusiness.semiSkilledEmp['totalSemiskilled']
    this.maleSkilled = this.selectedBusiness.skilledEmp['male']
    this.femaleSkilled = this.selectedBusiness.skilledEmp['female']
    this.skiledEmp = this.selectedBusiness.skilledEmp['totalUnskilled']
    this.unSkillEmp = this.maleUnskilled + this.femaleUnskilled;
    this.semiSkillEmp = this.maleSemiskilled + this.femaleSemiskilled;
    this.skiledEmp = this.maleSkilled + this.femaleSkilled;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    } this.originalHouseholdCash = this.businessHouseHoldCash;
    this.selectedAmountFromLoanWindow = this.selectedBankLoan + this.selectedBusinessCash + this.selectedFriendLoan + this.selectedSharkLoan
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.checkAllExepence();
    this.startBusiness = false;
    this.thirdScreen = true;
    this.manageBusinessCapital = this.startup_capital;
    this.manageBusinessExp = this.totalYeaExp;
    this.selectedBusinessName = this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessName;
  }

  manageOldBusiness() {
    this.businessSetupWindowShow = true;
    this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
    this.minimumInvestment = this.selectedBusiness.MinimumInvestment;
    this.yearlyInvestMent = this.selectedBusiness.yearlyInvestment;
    this.startup_capital = this.selectedBusiness.startUpCapital;
    this.yearly_investment = this.yearlyInvestMent;
    this.popupDispalyMimInv = (this.startup_capital);
    this.popupDisYearInv = (this.yearlyInvestMent);
    this.popupDisCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash);
    this.startBusiness = false;
    this.manageBusinessCapital = this.startup_capital;
    this.manageBusinessExp = this.totalYeaExp;
    this.selectedBusinessName = this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessName;
  }


  manageBusinessYes() {
    this.calculateWealth();
    if (this.selectedAmountFromLoanWindow + (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessValue - this.startup_capital) <= 0) {
      if (this.startup_capital > this.manageBusinessCapital) {
        this.carrerService.loanFlag = true
        this.translation.get('gameLabel.The_amount_of_initial_cash_you_are_trying_to_invest_is_greater_than_the_cash_currently_available_with_you', { houseHoldCash: Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
          this.messageText = s;
          this.messageClass = "errorpopup";
          this.successMsgFlag = true;
        })
      }
      else {
        this.manageBusinessSuccess()
      }
    }
    else {
      this.manageBusinessSuccess()
    }
  }


  manageBusinessNo() {
    this.luckyStart = false;
    this.firstScreen = false;
    this.secondScreen = false;
    this.thirdScreen=false;
    this.showBusinessWindow = false;
    this.startBusiness = false;
    this.diablegetJob = false;
    this.selectedBusiness = [];
    this.flagForFriend = false
    this.setAllCostToZero();
    this.setBusinessCardValues(this.firstCards);
    this.selectedBusinessName = '';
  }

  scaleUpBusiness() {
    this.carrerService.loanFlag = false
    this.setAllCostToZero();
    this.businessTypeFlag = false;
    this.registerBusinessWarr = false;
    this.curuptionWarrFlag = false;
    this.manageBusinessFlag = false;
    this.scaleupBusinessFlag = true
    this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
    let nextSelectedType = this.selectedBusiness.businessType + 1;
    this.selectedBusinessType = nextSelectedType
    this.businessTypeList = this.carrerService.createBusinessTypeArray(nextSelectedType, false, 0);
    this.filValueForSelection();
    this.messageText = this.translation.instant('gameLabel.Select size of business you want to scale up / scale down?')
    this.businessTypeWindow = true;
    this.selectedBusinessName = this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessName;
  }

  scaleUpBusinessOk() {
    this.selectedBusinessTypeName = this.businessTypeList[this.selectedBusinessType - 1].BusinessType
    this.businessTypeWindow = false
    this.registerFlag = this.selectedBusiness.isRegistered
    this.calculateInitialCapital();
    if (this.selectedBusinessType > this.selectedBusiness.businessType) {
      if (!this.registerFlag) {
        this.messageText = this.translation.instant('gameLabel.Your business is unregistered. For scaling up you need to register your business first. Do you want to register your business?')
        this.scalupRrgisterWindowFlag = true;
      }
      else {
        this.scaleUpBusinessSet();
      }
    }
    else {
      this.scaleUpBusinessSet();

    }
  }

  scaleUpBusinessCancle() {
    this.businessTypeWindow = false;
    this.luckyStart = false;
    this.businessSetupWindowShow = false;
    this.firstScreen = false;
    this.secondScreen = false;
    this.thirdScreen=false;
    this.showBusinessWindow = false;
    this.scaleupBusinessFlag = false
    this.flagForFriend = false;
    this.selectedBusiness = [];
    this.setAllCostToZero();
    this.setBusinessCardValues(this.firstCards);

  }
  scaleupRegisterYes() {
    this.businessTypeWindow = false
    this.scalupRrgisterWindowFlag = false;
    this.amenitiesCost = 0
    this.complentionCost = 0;
    this.bribeCost = 0;
    this.fskilled = 0
    this.fsemiSkilled = 0; this.fskilled = 0
    this.skiledEmp = 0
    this.unSkillEmp = 0
    this.semiSkillEmp = 0
    this.selectedWorlPlaceValue = 0
    this.monthlyExpenceLabour = 0;
    this.businessTypeFlag = true;
    this.totalSpaceCost = 0
    this.setInitialEmp();
    this.calculateWages();
    this.calculateHumanCapital(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
    if (this.selectedBusinessType >= 2) {
      this.TypeOfWorlPlaces(this.selectedBusinessType);
      this.calculateWages();
    }
    this.registerBusinessWarr = false;
    this.showJobConfirmWindow = false;
    this.registerFlag = true;
    this.setInitialCapital();
    this.getNameOfBusinessType();
    this.checkAmenitiesForBusiness();
    this.setFamilyMemberList();
    this.changeFamilyMemberSelectionChanges()
    this.profitPerValue = this.selectedBusiness.profitMarginPer;
    this.calculateCompriationCost(this.startup_capital);
    this.bribeCost = (this.startup_capital * 15 / 100)
    this.originalBribeCost = this.bribeCost;
    this.calculateFamilyHumanCapital();
    this.calculateAvalabelLoanAmount();
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    } this.originalHouseholdCash = this.businessHouseHoldCash;
    this.selectedAmountFromLoanWindow = this.selectedBankLoan + this.selectedBusinessCash + this.selectedFriendLoan + this.selectedSharkLoan
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.checkAllExepence();
    this.showBusinessWindow = true;
    this.startBusiness = true;
    this.firstScreen = true;
  }

  scaleupRegisterNo() {
    this.businessTypeFlag = false;
    this.businessTypeWindow = false;
    this.scalupRrgisterWindowFlag = false
    this.selectedBusinessType = this.selectedBusiness.selectedBusinessType;
  }

  scaleUpBusinessSucces() {
    if (this.selectedAmountFromLoanWindow + (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessValue - this.startup_capital) < 0) {
      this.carrerService.loanFlag = true
      this.translation.get('gameLabel.The_amount_of_initial_cash_you_are_trying_to_invest_is_greater_than_the_cash_currently_available_with_you', { houseHoldCash: Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
        this.messageText = s;
        this.messageClass = "errorpopup";
        this.successMsgFlag = true;
      })
    }
    else {
      this.manageBusinessSuccess()
    }
  }

  scaleUpBusinessSuccessNo() {
    this.showBusinessWindow = false;
    this.luckyStart = false;
    this.businessSetupWindowShow = false;
    this.firstScreen = false;
    this.secondScreen = false;

  }



  jobSetYes() {
    this.modalWindowShowHide.showWaitScreen = true;
    // this.showJobListFlag=false;
    this.showJobConfirmWindow = false;
    if (!this.selectedJob.eligible && !this.selectedJob.qualified) {
      this.messageText = this.translation.instant('gameLabel.You_are_not_eligible_for_this_job_due_to_your_age_or_qualification.');
      this.successMsgFlag = true;
      this.diablegetJob = true;
      this.messageClass = "errorpopup";
      this.modalWindowShowHide.showWaitScreen = false;
      this.getJobFlag = true;
    }
    else if (this.selectedJob.eligible && this.selectedJob.qualified) {
      let random = this.commonService.getRandomValue(0, 100);
      //use unemploymentrate
      random = random + this.homeService.allPerson[this.constantService.FAMILY_SELF].country.UnEmploymentRate;
      if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.intelligence < this.selectedJob.Intelligence ||
        this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.artistic < this.selectedJob.Artistic ||
        this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.musical < this.selectedJob.Musical ||
        this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.athletic < this.selectedJob.Athletic ||
        this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.spiritual < this.selectedJob.Spiritual ||
        this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.strength < this.selectedJob.Strength ||
        this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.physicalEndurance < this.selectedJob.Endurance ||
        this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.goodLooks < this.selectedJob.GoodLooks)) {
        // this.getWeekAttributeList(this.homeService.allPerson[this.constantService.FAMILY_SELF].traits, this.selectedJob);
        this.translation.get('gameLabel.job_war2', { jobName: this.selectedJob.Job_displayName }).subscribe(
          (str) => {
            this.messageText = str;
            this.successMsgFlag = true;
            this.diablegetJob = true;
            this.getJobFlag = true;
            this.messageClass = "errorpopup";
            this.modalWindowShowHide.showWaitScreen = false;
          })

      }
      else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === "F" && this.commonService.getRandomValue(0, 1) > Math.min((this.homeService.allPerson[this.constantService.FAMILY_SELF].discrimination_factor - (this.selectedJob.SalarySigmaSpread * 10), 100))) {
        this.messageText = this.translation.instant('gameLabel.job_war3');
        this.successMsgFlag = true;
        this.diablegetJob = true;
        this.getJobFlag = true;
        this.messageClass = "errorpopup";
        this.modalWindowShowHide.showWaitScreen = false;
      }
      else if (random >= Math.min((this.selectedJob.Possibility * 2), 100)) {
        this.messageText = this.translation.instant('gameLabel.job_war');
        this.successMsgFlag = true;
        this.diablegetJob = true;
        this.getJobFlag = true;
        this.messageClass = "errorpopup";
        this.modalWindowShowHide.showWaitScreen = false;
      }
      else {
        this.getJobFlag = false;
        this.showJobListFlag = false;
        this.saveCarrerObject = {
          "jobId": this.selectedJob.JobId,
          "currentSalary": this.selectedJob.Salary,
          "careerType": "job",
          "fromAge": this.homeService.allPerson.SELF.age,
          "jobName": this.selectedJob.Job_displayName,
          "startUpCapital": 0,
          "yearlyInvestment": 0,
          "businessId": 130,
        };

        this.allSaveCarrerObject = {
          "myCareer": this.saveCarrerObject,
          "job": this.selectedJob
        }
        this.carrerService.saveCareer(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id, this.allSaveCarrerObject).subscribe(
          res => {
            this.modalWindowShowHide.showWaitScreen = false;
            this.firstCards = [];
            this.allCarrerCards = [];
            this.result = res;
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
            this.firstCards.push(this.allCarrerCards[this.allCarrerCards.length - 1]);
            this.allCarrerCards.reverse();
            this.translation.get('gameLabel.You_started_working_successfully_as_a', { jobname: this.selectedJob.Job_displayName })
              .subscribe((s: string) => {
                this.messageText = s;
                this.successMsgFlag = true;
                this.messageClass = "successpopup";
              })
          })
      }
    }
    else if (this.selectedJob.qualified && !this.selectedJob.eligible) {
      this.messageText = this.translation.instant('gameLabel.job_war1');
      this.successMsgFlag = true;
      this.diablegetJob = false;
      this.getJobFlag = true;
      this.messageClass = "errorpopup";
      this.modalWindowShowHide.showWaitScreen = false;
    }


  }

  getWeekAttributeList(pTraits, jobTraits) {
    if (pTraits.intelligence < jobTraits.Intelligence) {
      this.weekTraitName = this.translation.instant('gameLabel.Intelligence');
    }
    else if (pTraits.artistic < jobTraits.Artistic) {
      this.weekTraitName = this.translation.instant('gameLabel.Artistic');
    }
    else if (pTraits.musical < jobTraits.Musical) {
      this.weekTraitName = this.translation.instant('gameLabel.Musical');
    }
    else if (pTraits.athletic < jobTraits.Athletic) {
      this.weekTraitName = this.translation.instant('gameLabel.Athletic');
    }
    else if (pTraits.spiritual < jobTraits.Spiritual) {
      this.weekTraitName = this.translation.instant('gameLabel.Spiritual');

    } else if (pTraits.strength < jobTraits.Strength) {
      this.weekTraitName = this.translation.instant('gameLabel.Strength');

    } else if (pTraits.physicalEndurance < jobTraits.Endurance) {
      this.weekTraitName = this.translation.instant('gameLabel.Physical_Endurance');

    } else if (pTraits.goodLooks < jobTraits.GoodLooks) {
      this.weekTraitName = this.translation.instant('gameLabel.GoodLooks');
    }
    return this.weekTraitName;
  }


  workOvertime() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.income = this.homeService.allPerson[this.constantService.FAMILY_SELF].income * 1.1;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].extra_work_time = true;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].income = this.income;
    this.carrerService.workOverTimeStartStop(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].extra_work_time, this.income).subscribe(
      res => {
        this.result = res;
        this.commonActionService.getActionIdentityDiffrence(this.result, this.homeService.allPerson);
        this.modalWindowShowHide.showActionCareer = false;
        this.modalWindowShowHide.showWaitScreen = false;
      })
  }

  stopWorkOvertime() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.income = this.homeService.allPerson[this.constantService.FAMILY_SELF].income / 1.1;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].extra_work_time = false;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].income = this.income;
    this.carrerService.workOverTimeStartStop(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].extra_work_time, this.income).subscribe(
      res => {
        this.result = res;
        this.commonActionService.getActionIdentityDiffrence(this.result, this.homeService.allPerson);
        this.modalWindowShowHide.showWaitScreen = false;
        this.modalWindowShowHide.showActionCareer = false;
      })
  }


  askForRaise() {
    this.modalWindowShowHide.showWaitScreen = true;
    let income: any;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].raise_received) {
      this.modalWindowShowHide.showWaitScreen = false;
      this.messageText = this.translation.instant('gameLabel.You_have_already_received_a_salary_raise_this_year');
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;


    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].job.SelfEmployed) {
      this.modalWindowShowHide.showWaitScreen = false;
      this.messageText = this.translation.instant('gameLabel.Because_you_are_self_employed_you_cannot_ask_for_a_raise');
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.intelligence > this.commonService.getRandomValue(0, 100)
      && !(this.homeService.allPerson[this.constantService.FAMILY_SELF].raise_received)) {

      let ran = this.commonService.getRandomValue(0, 1);

      if (ran < 0.4) {
        this.payRaisePercentage = 1;
      }
      else if (ran < 0.5) {
        this.payRaisePercentage = 2;
      }
      else if (ran < 0.6) {
        this.payRaisePercentage = 2.5;
      }
      else if (ran < 0.7) {
        this.payRaisePercentage = 3;
      }
      else if (ran < 0.8) {
        this.payRaisePercentage = 3.5;
      }
      else if (ran < 0.9) {
        this.payRaisePercentage = 4.5;
      }

      this.payRaisePercentage = this.payRaisePercentage * this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.intelligence / 50;

      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].alcoholic || this.homeService.allPerson[this.constantService.FAMILY_SELF].drug_addict || this.homeService.allPerson[this.constantService.FAMILY_SELF].convicted_criminal && (this.commonService.getRandomValue(0, 1) < 0.5)) {
        this.payRaisePercentage = 0.0;
      }
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex != this.constantService.MALE && ran > this.homeService.allPerson[this.constantService.FAMILY_SELF].discrimination_factor) {
        this.payRaisePercentage = this.payRaisePercentage * 0.5;
      }
      if (this.payRaisePercentage > 3.0) {
        this.payRaisePercentage = this.payRaisePercentage.toFixed(2);
        this.homeService.allPerson[this.constantService.FAMILY_SELF].raise_received = true;
        this.income = this.homeService.allPerson[this.constantService.FAMILY_SELF].income + (this.homeService.allPerson[this.constantService.FAMILY_SELF].income / 100.0 * this.payRaisePercentage);
        this.homeService.allPerson[this.constantService.FAMILY_SELF].income = this.income;
        let raiseObject = {
          "income": this.income,
          "payRaise": this.payRaisePercentage
        }
        this.carrerService.askForRaise(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].raise_received, raiseObject).subscribe(
          res => {
            this.modalWindowShowHide.showWaitScreen = false;
            this.result = res;
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            this.translation.get('gameLabel.You_have_received_a_pay_raise', { percent: this.payRaisePercentage }).subscribe((s: string) => {
              this.messageText = s;
              this.messageClass = "successpopup";
              this.successMsgFlag = true;
            }
            )

          })
      }
      else {
        this.modalWindowShowHide.showWaitScreen = false;
        this.messageText = this.translation.instant('gameLabel.Your_request_for_a_raise_has_been_refused');
        this.messageClass = "errorpopup";
        this.successMsgFlag = true;
      }
    }
    else {
      this.modalWindowShowHide.showWaitScreen = false;
      this.messageText = this.translation.instant('gameLabel.Your_request_for_a_raise_has_been_refused');
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
  }


  quitJob() {
    this.quitJobFlag = true;
    this.messageText = this.translation.instant('gameLabel.Do_you_really_want_to_quit_your_current_job?');
    this.showJobConfirmWindow = true;
  }

  setBusinessObjectToSave() {
    this.saveCarrerObject = [];
    let familyEmp = []
    for (let i = 0; i < this.familyMemberList.length; i++) {
      if (this.familyMemberList[i].check) {
        if (this.familyMemberList[i].empType === 1) {
          if (this.flagForSameSal) {
            this.familyMemberList[i].humancapital = this.perUnskilleMCost
          }
          else {
            this.familyMemberList[i].humancapital = this.perUnskilleFCost
          }
        }
        if (this.familyMemberList[i].empType === 2) {
          if (this.flagForSameSal) {
            this.familyMemberList[i].humancapital = this.perSemiskilleMCost
          }
          else {
            this.familyMemberList[i].humancapital = this.perSemiskilleFCost
          }
        }
        if (this.familyMemberList[i].empType === 3) {
          if (this.flagForSameSal) {
            this.familyMemberList[i].humancapital = this.perSkilleMCost
          }
          else {
            this.familyMemberList[i].humancapital = this.perSkilleFCost
          }
        }
        familyEmp.push(this.familyMemberList[i])
      }
    }
    let unskilled = {
      "male": this.maleUnskilled,
      "female": this.femaleUnskilled,
      "totalUnskilled": this.unSkillEmp
    }

    let semiSkilled = {
      "male": this.maleUnskilled,
      "female": this.femaleSemiskilled,
      "totalSemiskilled": this.semiSkillEmp
    }

    let skilled = {
      "male": this.maleSkilled,
      "female": this.femaleSkilled,
      "totalSkilled": this.skiledEmp
    }
    let loan = {
      "bankLoan": Math.round(this.selectedBankLoan),
      "sharkLoan": Math.round(this.selectedSharkLoan),
      "friendLoan": Math.round(this.selectedFriendLoan)
    }
    this.saveCarrerObject = {
      "businessName": this.selectedBusinessName,
      "jobId": 132,
      "currentSalary": 0,
      "careerType": "business",
      "fromAge": this.homeService.allPerson.SELF.age,
      "jobName": this.selectedBusiness.Business,
      "startUpCapital": Math.round(this.startup_capital),
      "businessId": this.selectedBusiness.businessesId,
      "profitMarginPer": this.profitPerValue,
      "selfSalary": Math.round(this.selfSalary),
      "isRegistered": this.registerFlag,
      "familyMember": familyEmp,
      "skilledEmp": skilled,
      "unSkilledEmp": unskilled,
      "semiSkilledEmp": semiSkilled,
      "spaceType": Math.round(this.selectedWorlPlaceValue),
      "businessType": this.selectedBusinessType,
      "HumanCapital": Math.round(this.humanCapital),
      "familyHumanCapital": Math.round(this.familyHumanCapital),
      "yearlyExp": Math.round(this.totalYeaExp),
      "empSalarySame": this.flagForSameSal,
      "loan": loan,
      "bribeCost": this.bribeCost,
      "complianceCost": this.complentionCost,
      "usedHouseholdCash": this.selectedBusinessCash,
      "spaceCost": Math.round(this.totalSpaceCost * 12),
      "amenityCost": Math.round(this.amenityCost * 12),
      "empCost": Math.round(this.monthlyExpenceLabour * 12),
      "initialCapital": this.initialStartup_capital * 12
    };
  }


  jobSetNo() {
    this.showJobListFlag = false;
    this.getJobFlag = false;
    this.showJobConfirmWindow = false;
  }

  quitJobYes() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.quitJobFlag = false;
    this.showJobConfirmWindow = false;
    this.carrerService.quitJob(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.modalWindowShowHide.showWaitScreen = false;
        this.firstCards = [];
        this.allCarrerCards = [];
        this.result = res;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
        this.firstCards.push(this.allCarrerCards[this.allCarrerCards.length - 1]);
        this.allCarrerCards.reverse();
        this.modalWindowShowHide.showActionCareer = false;
      })
  }

  quitJobNo() {
    this.quitJobFlag = false;
    this.showJobConfirmWindow = false;
  }

  close() {
    this.modalWindowShowHide.showActionCareer = false;
  }

  successMsgWindowClose() {
    if (this.luckyStart) {
      this.setFamilyMemberList();
      if (this.selectedBusinessType >= 2) {
        this.checkAmenitiesForBusiness();
        this.TypeOfWorlPlaces(this.selectedBusinessType);
      }
      this.curuptionWarrFlag = false;
      this.showJobConfirmWindow = false;
      this.getNameOfBusinessType();
      this.firstScreen = true;
      // this.businessSetupWindowShow = true;
      this.showBusinessWindow = true;
      this.startBusiness = true;
    }

    if (!this.showJobListFlag && !this.showOldBusiness) {
      this.setFamilyMemberList();
      if (this.selectedBusinessType >= 2) {
        this.checkAmenitiesForBusiness();
        this.TypeOfWorlPlaces(this.selectedBusinessType);
      }
      this.showJobListFlag = false;
      this.successMsgFlag = false;
      if (this.showBusinessWindow) {
        this.firstScreen = true;
        // this.businessSetupWindowShow = true;
      }
    }
    else if (this.successMsgFlag) {
      this.successMsgFlag = false;
    }
    else {
      this.setFamilyMemberList();
      if (this.selectedBusinessType >= 2) {
        this.checkAmenitiesForBusiness();
        this.TypeOfWorlPlaces(this.selectedBusinessType);
      }
      this.calculateCompriationCost(this.startup_capital);
      this.showJobListFlag = true;
      this.successMsgFlag = false;
      if (this.showBusinessWindow) {
        this.firstScreen = true;
      }
    }
  }

  clocsJobListClick() {
    this.showJobListFlag = false;

  }
  emigrate() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 11) {
      this.alertMsg = this.translation.instant('gameLabel.You_cannot_move_out_before_you_are_11_year_old.');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    else {
      this.modalWindowShowHide.showActionResidence = true;
      this.modalWindowShowHide.showActionCareer = false;
    }
  }

  getColor(status) {
    switch (status) {
      case 0:
        return '#e30000';
      case 1:
        return '#FFF600';
      case 2:
        return '#e76b00';
      case 3:
        return '#0000FF';
      case 4:
        return '#34db00';
    }
  }

  setPerPersonmaleSalary(sigmaDisatance) {
    let mexp = Math.exp(this.gameSummeryService.ecoStatusData.meanIncome + (sigmaDisatance * this.gameSummeryService.ecoStatusData.incomeDeviation));
    let mincome = ((mexp / 12) * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate);
    return mincome
  }

  setPerPersonFemaleSalary(sigmaDisatance) {
    let fexpence = Math.exp(this.gameSummeryService.ecoStatusData.meanIncome + sigmaDisatance * this.gameSummeryService.ecoStatusData.incomeDeviation)
    let fincome = fexpence / 12
    fincome = fincome * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
    fincome = fincome * (1 - (1 - this.homeService.allPerson[this.constantService.FAMILY_SELF].discrimination_factor) * (1 - this.homeService.allPerson[this.constantService.FAMILY_SELF].discrimination_factor))
    return fincome
  }
  calculateWages() {
    //calculation
    this.unSkillEmp = this.maleUnskilled + this.femaleUnskilled;
    this.semiSkillEmp = this.maleSemiskilled + this.femaleSemiskilled;
    this.skiledEmp = this.maleSkilled + this.femaleSkilled;
    if (!this.manageBusinessFlag) {
      this.totalUnSkilAmount = (((this.maleUnskilled - this.famMunskilled) * this.perUnskilleMCost) + ((this.femaleUnskilled - this.famFunskilled) * this.perUnskilleFCost));
      this.totalSemiSkilAmount = (((this.maleSemiskilled - this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.femaleSemiskilled - this.famFsemiskilled) * this.perSemiskilleFCost))
      this.totalSkilAmount = (((this.maleSkilled - this.famMskilled) * this.perSkilleMCost) + ((this.femaleSkilled - this.famFskilled) * this.perSkilleFCost))
      this.monthlyExpenceLabour = this.totalSemiSkilAmount + this.totalUnSkilAmount + this.totalSkilAmount
    }
    else {
      if (this.flagForSameSal) {
        this.totalUnSkilAmount = (((this.maleUnskilled - this.famMunskilled) * this.perUnskilleMCost) + ((this.femaleUnskilled - this.famFunskilled) * this.perUnskilleMCost));
        this.totalSemiSkilAmount = (((this.maleSemiskilled - this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.femaleSemiskilled - this.famFsemiskilled) * this.perSemiskilleMCost))
        this.totalSkilAmount = (((this.maleSkilled - this.famMskilled) * this.perSkilleMCost) + ((this.femaleSkilled - this.famFskilled) * this.perSkilleMCost))
        this.monthlyExpenceLabour = this.totalSemiSkilAmount + this.totalUnSkilAmount + this.totalSkilAmount
      }
      else {
        this.totalUnSkilAmount = (((this.maleUnskilled - this.famMunskilled) * this.perUnskilleMCost) + ((this.femaleUnskilled - this.famFunskilled) * this.perUnskilleFCost));
        this.totalSemiSkilAmount = (((this.maleSemiskilled - this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.femaleSemiskilled - this.famFsemiskilled) * this.perSemiskilleFCost))
        this.totalSkilAmount = (((this.maleSkilled - this.famMskilled) * this.perSkilleMCost) + ((this.femaleSkilled - this.famFskilled) * this.perSkilleFCost))
        this.monthlyExpenceLabour = this.totalSemiSkilAmount + this.totalUnSkilAmount + this.totalSkilAmount
      }
    }
    this.checkAllExepence();
  }

  setEmp() {
    this.oldSpaceCost = this.totalSpaceCost;
    this.oldfamilyObject = JSON.parse(JSON.stringify(this.familyMemberList));
    this.expencesForEmpScreen = this.totalYeaExp - (this.monthlyExpenceLabour * 12)
    this.empSelectionFlag = true;
    if (!this.manageBusinessFlag && !this.scaleupBusinessFlag) {
      this.sameSalaryFlagForStartBusiness = false;
      this.oldfemaleUnskilled = this.femaleUnskilled;
      this.oldfemaleSemiskilled = this.femaleSemiskilled;
      this.oldfemaleSkilled = this.femaleSkilled;
    }
    this.calculateWages();
    this.setCurrentEmp();


  }



  saveEmpData() {
    this.salaryFlag = false
    this.chaeckInitialCheckForEmp();
    this.calculateFamilyHumanCapital();
    this.setCurrentEmp();
    this.checkFamMemberInvolve();
  }

  salaryDesignYes() {
    this.salaryFlag = false;
    if (this.selectedChoice === 1) {
      this.flagForMaleSal = true
    }
    else {
      this.flagForMaleSal = false;
    }

    if (this.selectedChoice === 2) {
      this.flagForSameSal = true
    }
    else {
      this.flagForSameSal = false
    }
    if (this.flagForSameSal) {
      this.unSkillEmp = this.maleUnskilled + this.femaleUnskilled;
      this.semiSkillEmp = this.maleSemiskilled + this.femaleSemiskilled;
      this.skiledEmp = this.maleSkilled + this.femaleSkilled;

      this.totalUnSkilAmount = (((this.maleUnskilled - this.famMunskilled) * this.perUnskilleMCost) + ((this.femaleUnskilled - this.famFunskilled) * this.perUnskilleMCost));
      this.totalSemiSkilAmount = (((this.maleSemiskilled - this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.femaleSemiskilled - this.famFsemiskilled) * this.perSemiskilleMCost))
      this.totalSkilAmount = (((this.maleSkilled - this.famMskilled) * this.perSkilleMCost) + ((this.femaleSkilled - this.famFskilled) * this.perSkilleMCost))
      this.monthlyExpenceLabour = this.totalSemiSkilAmount + this.totalUnSkilAmount + this.totalSkilAmount
      this.checkAllExepence();
    }
    else if (this.flagForMaleSal) {
      this.unSkillEmp = this.maleUnskilled + this.femaleUnskilled;
      this.semiSkillEmp = this.maleSemiskilled + this.femaleSemiskilled;
      this.skiledEmp = this.maleSkilled + this.femaleSkilled;

      this.totalUnSkilAmount = (((this.maleUnskilled - this.famMunskilled) * this.perUnskilleMCost) + ((this.femaleUnskilled - this.famFunskilled) * this.perUnskilleFCost));
      this.totalSemiSkilAmount = (((this.maleSemiskilled - this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.femaleSemiskilled - this.famFsemiskilled) * this.perSemiskilleFCost))
      this.totalSkilAmount = (((this.maleSkilled - this.famMskilled) * this.perSkilleMCost) + ((this.femaleSkilled - this.famFskilled) * this.perSkilleFCost))
      this.monthlyExpenceLabour = this.totalSemiSkilAmount + this.totalUnSkilAmount + this.totalSkilAmount
      this.checkAllExepence();
    }
    else {
      this.selectedBusiness.unSkilledEmp['male'] = this.maleUnskilled
      this.selectedBusiness.unSkilledEmp['female'] = this.femaleUnskilled
      this.selectedBusiness.unSkilledEmp['totalUnskilled'] = this.unSkillEmp

      this.selectedBusiness.semiSkilledEmp['male'] = this.maleSemiskilled
      this.selectedBusiness.semiSkilledEmp['female'] = this.femaleSemiskilled
      this.selectedBusiness.semiSkilledEmp['totalSemiskilled'] = this.semiSkillEmp


      this.selectedBusiness.skilledEmp['male'] = this.maleSkilled
      this.selectedBusiness.skilledEmp['female'] = this.femaleSkilled
      this.selectedBusiness.skilledEmp['totalUnskilled'] = this.skiledEmp
    }
    this.calculateFamilyHumanCapital();
  }
  salaryDesignNo() {
    this.salaryFlag = false;
  }


  cancelEmpConfirm() {
    this.cancelConfirmFlag = true;
    this.text = this.translation.instant('gameLabel.Are you sure you want to cancel your employee selection from different categories?')
  }


  cancelEmpConfirmYes() {
    this.cancelConfirmFlag = false;
    this.empSelectionFlag = false;
    this.cancelEmpData();
  }

  cancelEmpConfirmNo() {
    this.cancelConfirmFlag = false;
    this.calculateFamilyHumanCapital();
    this.checkFamMemberInvolve();
  }



  cancelEmpData() {
    this.empSelectionFlag = false;
    this.totalSpaceCost = this.oldSpaceCost;
    if (this.manageBusinessFlag) {
      this.setvalueOnCancel();
      this.calculateWages();
      this.familyMemberList = this.oldfamilyObject
      this.setOldValues();
    }
    else {
      this.setvalueOnCancel();
      this.familyMemberList = this.oldfamilyObject
      this.setOldValues();
    }

  }
  setFamilyMemberList() {
    this.familyMemberList = [];
    for (let i = 0; i < this.familyCapsuleService.livingAtHomeArray.length; i++) {

      if (this.familyCapsuleService.livingAtHomeArray[i].age > 18 && this.familyCapsuleService.livingAtHomeArray[i].age < 70) {
        if (this.familyCapsuleService.livingAtHomeArray[i].job === 999 || this.familyCapsuleService.livingAtHomeArray[i].job === 133 || this.familyCapsuleService.livingAtHomeArray[i].job === 132) {
          if (this.familyCapsuleService.livingAtHomeArray[i].education.school.highSchoolGraduate &&
            this.familyCapsuleService.livingAtHomeArray[i].education.college.collegeGraduate) {
            this.familyMemberList.push({
              "id": this.familyCapsuleService.livingAtHomeArray[i].id,
              "name": this.familyCapsuleService.livingAtHomeArray[i].name,
              "identity": this.familyCapsuleService.livingAtHomeArray[i].identity,
              "age": this.familyCapsuleService.livingAtHomeArray[i].age,
              "empType": 3,
              "empTypeName": this.translation.instant('gameLabel.Skilled'),
              "check": false,
              "sex": this.familyCapsuleService.livingAtHomeArray[i].sex,
              "humancapital": 0
            })
          }
          else if (this.familyCapsuleService.livingAtHomeArray[i].education.school.highSchoolGraduate &&
            !this.familyCapsuleService.livingAtHomeArray[i].education.college.collegeGraduate) {
            this.familyMemberList.push({
              "id": this.familyCapsuleService.livingAtHomeArray[i].id,
              "name": this.familyCapsuleService.livingAtHomeArray[i].name,
              "identity": this.familyCapsuleService.livingAtHomeArray[i].identity,
              "age": this.familyCapsuleService.livingAtHomeArray[i].age,
              "empType": 2,
              "empTypeName": this.translation.instant('gameLabel.Semi-skilled'),
              "check": false,
              "sex": this.familyCapsuleService.livingAtHomeArray[i].sex,
              "humancapital": 0
            })
          }
          else if (!this.familyCapsuleService.livingAtHomeArray[i].education.school.highSchoolGraduate) {
            this.familyMemberList.push({
              "id": this.familyCapsuleService.livingAtHomeArray[i].id,
              "name": this.familyCapsuleService.livingAtHomeArray[i].name,
              "identity": this.familyCapsuleService.livingAtHomeArray[i].identity,
              "age": this.familyCapsuleService.livingAtHomeArray[i].age,
              "empType": 1,
              "empTypeName": this.translation.instant('gameLabel.Unskilled'),
              "check": false,
              "sex": this.familyCapsuleService.livingAtHomeArray[i].sex,
              "humancapital": 0
            })
          }
        }
      }
    }
    this.oldfamilyObject = JSON.parse(JSON.stringify(this.familyMemberList));
    // console.log(this.familyMemberList);
  }

  TypeOfWorlPlaces(businssType) {
    if (businssType === 2) {
      this.totalSpaceCost = parseInt((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.moderateShelterCost * 1.25).toFixed(0))
      this.initialSpaceCost = this.totalSpaceCost
    }
    else if (businssType === 3) {
      this.totalSpaceCost = parseInt((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.ampleShelterCost * 1.50).toFixed(0))
      this.initialSpaceCost = this.totalSpaceCost
    }
    else if (businssType === 4) {
      this.totalSpaceCost = parseInt((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.luxuriousShelterCost * 1.50).toFixed(0))
      this.initialSpaceCost = this.totalSpaceCost
    }
    this.checkAllExepence();
  }
  checkAmenitiesForBusiness() {
    this.amenityCost = 0;
    let shelterCost = {
      1: "simplestSharedShelterCost",
      2: "simplestSharedShelterCost",
      3: "simplestShelterCost",
      4: "modestShelterCost",
      5: "moderateShelterCost",
      6: "ampleShelterCost",
      7: "luxuriousShelterCost"
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities.basicSanitation && this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities.healthService && this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities.safeWater) {
      this.amenitiesFlag = false
    }
    else {
      this.amenitiesFlag = true
      let shelterIndex = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex
      let currentCost = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense
      let nextShelterIndex = shelterIndex + 1;
      let costName = shelterCost[nextShelterIndex]
      let nextCost = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter[costName]
      this.amenityCost = (nextCost - currentCost)
      this.checkAllExepence();
    }
  }

  checkAllExepence() {
    this.totalMonthlyExp = 0;
    this.totalSpaceCost = Math.round(this.totalSpaceCost);
    this.monthlyExpenceLabour = Math.round(this.monthlyExpenceLabour)
    if (this.selectedBusinessType > 2) {
      this.startup_capital = 0
      this.totalMonthlyExp = this.amenityCost + this.totalSpaceCost + this.monthlyExpenceLabour + this.initialStartup_capital
    }
    else if (this.selectedBusinessType === 1) {
      this.amenitiesCost = 0;
      this.totalSpaceCost = 0;
      this.monthlyExpenceLabour = 0;
      this.totalMonthlyExp = this.amenityCost + this.totalSpaceCost + this.monthlyExpenceLabour + this.initialStartup_capital
    }
    else if (this.selectedBusinessType === 2) {
      this.total = this.unSkillEmp + this.semiSkillEmp + this.skiledEmp
      this.totalMonthlyExp = this.amenityCost + (this.totalSpaceCost) + this.monthlyExpenceLabour + (this.initialStartup_capital)
    }

    this.totalYeaExp = Math.round((this.totalMonthlyExp * 12) + this.bribeCost + this.complentionCost + this.selfSalary);
    this.startup_capital = Math.round((this.totalYeaExp) + (this.totalYeaExp * (10 / 100)))
    this.diffBvaleCash = Math.round(this.selectedBusiness.businessValue - this.startup_capital)
    if (this.diffBvaleCash < 0) {
      this.diffBvaleCash *= -1;
    }
    if (this.manageBusinessFlag || this.scaleupBusinessFlag) {
      this.checkManageBusinessCapAndExpClass();
      this.checkRedGreenClassForCashValuesManage();
    }
    else {
      this.checkRedGreenClassForCashValues();
    }
  }


  checkManageBusinessCapAndExpClass() {
    if (this.manageBusinessCapital > this.startup_capital) {
      this.mCapitalClass = "pr-3 textSize red"
    }
    else if (this.manageBusinessCapital < this.startup_capital) {
      this.mCapitalClass = "pr-3 textSize green"
    }
    else {
      this.mCapitalClass = "pr-3 textSize"
    }

    if (this.manageBusinessExp > this.totalYeaExp) {
      this.mexpClass = "pr-3 textSize red"
    }
    else if (this.manageBusinessExp < this.totalYeaExp) {
      this.mexpClass = "pr-3 textSize green"
    }
    else {
      this.mexpClass = "pr-3 textSize"
    }
  }
  setAllCostToZero() {
    this.orgFloan = 0
    this.fCnt = 0
    this.friendLoanValue = 2;
    this.businessHouseHoldCash = 0;
    this.selectedAmountFromLoanWindow = 0
    this.famMunskilled = 0;
    this.famFunskilled = 0;
    this.famMsemiskilled = 0;
    this.famFsemiskilled = 0;
    this.famMskilled = 0;
    this.famFskilled = 0;
    this.familyHumanCapital = 0;
    this.manageBusinessFlag = false;
    this.scaleupBusinessFlag = false;
    this.bribeCost = 0;
    this.complentionCost = 0;
    this.amenityCost = 0;
    this.totalSpaceCost = 0;
    this.monthlyExpenceLabour = 0;
    this.startup_capital = 0;
    this.humanCapital = 0;
    this.wealthDeviation = 0;
    this.welthIncome = 0;
    this.totalUnSkilAmount = 0;
    this.totalSemiSkilAmount = 0;
    this.totalSkilAmount = 0;
    this.maleSemiskilled = 0;
    this.femaleSemiskilled = 0;
    this.maleUnskilled = 0;
    this.femaleUnskilled = 0;
    this.maleSkilled = 0;
    this.femaleSkilled = 0;
    this.unSkillEmp = 0;
    this.semiSkillEmp = 0;
    this.skiledEmp = 0
    this.originalComplianceCost = 0;
    this.bribeCost = 0;
    this.aveLoanAmount = 0;
    this.takenSharkLoanAmount = 0;
    this.sharkLoanAmount = 0
    this.takenLoanAmt = 0;
    this.takenFriendLoan = 0;
    this.takenSharkAmt = 0
    this.selectedFriendLoan = 0;
    this.initialStartup_capital = 0;
    this.originalStratupCapital = 0
    this.totalMonthlyExp = 0;
    this.totalYeaExp = 0;
    this.selfSalary = 0;
    this.selectedBankLoan = 0;
    this.selectedSharkLoan = 0;

  }

  calculateHumanCapital(person) {
    this.humanCapital = 0
    if (person.job.JobId === 999 || person.job.JobId === 132 || person.job.JobId === 133) {
      if (person.education.school.highSchoolGraduate && person.education.college.collegeGraduate) {
        let exp = Math.exp(this.gameSummeryService.ecoStatusData.meanIncome + (this.sigmaDistanceSkilled * this.gameSummeryService.ecoStatusData.incomeDeviation));
        let income = ((exp / 12) * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate);
        this.humanCapital = 1 * income
      }
      else if (person.education.school.highSchoolGraduate &&
        !person.education.college.collegeGraduate) {
        let exp = Math.exp(this.gameSummeryService.ecoStatusData.meanIncome + (this.sigmaDistanceSemiskilled * this.gameSummeryService.ecoStatusData.incomeDeviation));
        let income = ((exp / 12) * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate);
        this.humanCapital = 1 * income
      }
      else if (!person.education.school.highSchoolGraduate) {
        let exp = Math.exp(this.gameSummeryService.ecoStatusData.meanIncome + (this.sigmaDistanceUnskilled * this.gameSummeryService.ecoStatusData.incomeDeviation));
        let income = ((exp / 12) * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate);
        this.humanCapital = 1 * income
      }
    }
  }

  calculateWealth() {
    this.wealthDeviation = 0;
    this.welthIncome = 0;
    let welathMeataData = this.gameSummeryService.bornCountryWelathMetadata
    welathMeataData.wealth = (welathMeataData.wealth);
    let value1 = welathMeataData.per20 * welathMeataData.wealth
    let value2 = (welathMeataData.per50 - welathMeataData.per20) * welathMeataData.wealth
    let value3 = (welathMeataData.per90 - welathMeataData.per50) * welathMeataData.wealth
    let value4 = (welathMeataData.per99 - welathMeataData.per90) * welathMeataData.wealth
    let value5 = (welathMeataData.per100 - welathMeataData.per99) * welathMeataData.wealth
    let totalWealth1 = value1 + value2 + value3 + value4 + value5;
    let totalWealth2 = totalWealth1 / welathMeataData.pop2021;
    let avgWealth = (totalWealth2 * (10 ** 9));
    let wealthDeviation = welathMeataData.stdDeviation
    let meanIncomeSeq = wealthDeviation * wealthDeviation / 2
    let logWealth = Math.log(avgWealth * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.AverageFamilyCount);
    this.welthIncome = (logWealth - meanIncomeSeq);
    this.wealthDeviation = wealthDeviation
  }

  setInitialCapital() {
    this.initialStartup_capital = this.originalStratupCapital;
    if (this.selectedBusinessType === 2) {
      this.initialStartup_capital = this.initialStartup_capital * 2
    }
    else {
      this.initialStartup_capital = this.initialStartup_capital
    }
  }

  setBusinessCardValues(first) {
    this.registerFlag = first[0].isRegistered
    this.getNameOfBusinessType();
    this.cardRegister = this.cardBusinessType;
    this.businessTypeList = this.carrerService.createBusinessTypeArray(first[0].businessType, true, 0);
    this.filValueForSelection();
    this.selectedBusinessTypeName = this.businessTypeList[first[0].businessType - 1].BusinessType;
    let val = (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessValue - first[0].startUpCapital) / this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessValue;
    this.cardGrothPer = val * 100
    this.totalEmp = first[0].unSkilledEmp['totalUnskilled'] + first[0].semiSkilledEmp['totalSemiskilled'] + first[0].skilledEmp['totalSkilled']
  }

  setInitialEmp() {
    if (this.selectedBusinessType === 2) {
      this.maleUnskilled = 0
      this.femaleUnskilled = 0
      this.maleSemiskilled = 0;
      this.femaleSemiskilled = 0;
      this.maleSkilled = 1;
      this.femaleSkilled = 0;

      this.unSkillDef= this.maleUnskilled +this.femaleUnskilled;
      this.semiSkilledDef=this.maleSemiskilled+this.femaleSemiskilled
      this.skilledDef=this.maleSkilled+this.femaleSkilled

      this.unskillledMax = 5;
      this.semiskilledMax = 5
      this.skilledMax = 20;

    
      this.skilledMin = 1;
      this.unskillledMin = 0;
      this.semiskilledMin = 0


    }
    else if (this.selectedBusinessType === 3) {
      this.maleUnskilled = 0
      this.femaleUnskilled = 0
      this.maleSemiskilled = 5;
      this.femaleSemiskilled = 5;
      this.maleSkilled = 10;
      this.femaleSkilled = 10;

      this.unSkillDef= this.maleUnskilled +this.femaleUnskilled;
      this.semiSkilledDef=this.maleSemiskilled+this.femaleSemiskilled
      this.skilledDef=this.maleSkilled+this.femaleSkilled

      this.unskillledMin = 0;
      this.semiskilledMin = 10
      this.skilledMin = 20;

      this.unskillledMax = 10;
      this.semiskilledMax = 20
      this.skilledMax = 60;





    }
    else if (this.selectedBusinessType === 4) {
      this.maleUnskilled = 0
      this.femaleUnskilled = 0
      this.maleSemiskilled = 15;
      this.femaleSemiskilled = 15;
      this.maleSkilled = 30;
      this.femaleSkilled = 30;

      this.unSkillDef= this.maleUnskilled +this.femaleUnskilled;
      this.semiSkilledDef=this.maleSemiskilled+this.femaleSemiskilled
      this.skilledDef=this.maleSkilled+this.femaleSkilled


      this.unskillledMin = 0;
      this.semiskilledMin = 30
      this.skilledMin = 60;

      this.unskillledMax = 100;
      this.semiskilledMax = 500
      this.skilledMax = 1000;



    }
  }

  calculateBusinessTypeChange() {
    let value
    if (this.selectedBusiness.businessType === 2) {
      value = this.setInitialValuesForSmallBusiness();
    }
    else if (this.selectedBusiness.businessType === 3) {
      value = this.setInitialValuesForMediumBusiness();
    }
    else if (this.selectedBusiness.businessType === 4) {
      value = this.setInitialValuesForLargeBusiness();
    }
    if (this.selectedBusiness.businessValue > value) {
      value = this.selectedBusiness.businessValue
    }
    else {
      value = this.startup_capital
    }
    let v1 = value / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
    let v2 = Math.log(v1)
    let v3 = ((v2 - this.welthIncome) / this.wealthDeviation)
    if (v3 > this.constantService.Economically_Challenged_Wealth && v3 < this.constantService.Bearly_Managed_Wealth) {
      this.futureBusinessValue = 1
    }
    else if (v3 > this.constantService.Bearly_Managed_Wealth && v3 < this.constantService.Well_to_do__Wealth) {
      this.futureBusinessValue = 2;
    }
    else if (v3 > this.constantService.Well_to_do__Wealth && v3 < this.constantService.Rich_Wealth) {
      this.futureBusinessValue = 3;
    }
    else if (v3 > this.constantService.Rich_Wealth) {
      this.futureBusinessValue = 4;
    }
  }

  chaeckInitialCheckForEmp() {
    this.selectedChoice = 1;
    this.unSkillEmp = this.maleUnskilled + this.femaleUnskilled;
    this.semiSkillEmp = this.maleSemiskilled + this.femaleSemiskilled;
    this.skiledEmp = this.maleSkilled + this.femaleSkilled;
    if (this.selectedBusinessType === 1) {
      if ((this.femaleUnskilled > 0 || this.femaleSemiskilled > 0 || this.femaleSkilled > 0)) {
        this.empSelectionFlag = false;
        this.salaryDesign();
      }
      else {
        this.empSelectionFlag = false;
        this.setValuesForEmp();
      }
    }
    if (this.selectedBusinessType === 2) {
      if (this.unSkillEmp < this.unskillledMin || this.semiSkillEmp < this.semiskilledMin || this.skiledEmp < this.skilledMin) {
        this.alertFlag = true;
        this.messageText = this.translation.instant('gameLabel.To start a small business, you will need at least one skilled employee.')
        this.setvalueOnCancel();
        this.familyMemberList = this.oldfamilyObject
        this.setOldValues();
      }
      else if (this.unSkillEmp > this.unskillledMax || this.semiSkillEmp > this.semiskilledMax || this.skiledEmp > this.skilledMax) {
        this.alertFlag = true;
        this.messageText = this.translation.instant('gameLabel.Selected number of employees is greater than maximum number of employees')
        if (this.manageBusinessFlag) {
          this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
          this.maleUnskilled = this.selectedBusiness.unSkilledEmp['male']
          this.femaleUnskilled = this.selectedBusiness.unSkilledEmp['female']
          this.unSkillEmp = this.selectedBusiness.unSkilledEmp['totalUnskilled']

          this.maleSemiskilled = this.selectedBusiness.semiSkilledEmp['male']
          this.femaleSemiskilled = this.selectedBusiness.semiSkilledEmp['female']
          this.semiSkillEmp = this.selectedBusiness.semiSkilledEmp['totalSemiskilled']


          this.maleSkilled = this.selectedBusiness.skilledEmp['male']
          this.femaleSkilled = this.selectedBusiness.skilledEmp['female']
          this.skiledEmp = this.selectedBusiness.skilledEmp['totalUnskilled']
          this.calculateWages();
          this.familyMemberList = this.oldfamilyObject
          this.setOldValues();
        }
        else {
          this.setvalueOnCancel();
          this.familyMemberList = this.oldfamilyObject
          this.setOldValues();
        }
      }
      else if (this.femaleUnskilled > 0 || this.femaleSemiskilled > 0 || this.femaleSkilled > 0) {
        if (this.manageBusinessFlag || this.scaleupBusinessFlag) {
          if (this.femaleUnskilled > this.selectedBusiness.unSkilledEmp['female'] || this.femaleSemiskilled > this.selectedBusiness.semiSkilledEmp['female'] || this.femaleSkilled > this.selectedBusiness.skilledEmp['female']) {
            this.empSelectionFlag = false;
            this.salaryDesign();
            this.setEmpCost();
          }
          else {
            this.empSelectionFlag = false;
            this.setValuesForEmp();
            this.setEmpCost();
          }
        }
        else if (this.femaleUnskilled > this.oldfemaleUnskilled || this.femaleSemiskilled > this.oldfemaleSemiskilled || this.femaleSkilled > this.oldfemaleSkilled) {
          this.empSelectionFlag = false;
          this.salaryDesign();
          this.setEmpCost();
        }
        else {
          this.empSelectionFlag = false;
          this.setValuesForEmp();
          this.setEmpCost();
        }

      }
      else {
        this.empSelectionFlag = false;
        this.setValuesForEmp();
        this.setEmpCost();

      }
    }
    if (this.selectedBusinessType === 3) {
      if (this.unSkillEmp < this.unskillledMin || this.semiSkillEmp < this.semiskilledMin || this.skiledEmp < this.skilledMin) {
        this.alertFlag = true;
        this.messageText = this.translation.instant('gameLabel.To start a medium-sized business, you will need at least 10 semi-skilled and 20 skilled employees')
        this.setvalueOnCancel();
        this.familyMemberList = this.oldfamilyObject
        this.setOldValues();
      }
      else if (this.unSkillEmp > this.unskillledMax || this.semiSkillEmp > this.semiskilledMax || this.skiledEmp > this.skilledMax) {

        this.alertFlag = true;
        this.messageText = this.translation.instant('gameLabel.The number of employees selected is greater than the maximum number of employees you can choose.')
        if (this.manageBusinessFlag) {
          this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
          this.maleUnskilled = this.selectedBusiness.unSkilledEmp['male']
          this.femaleUnskilled = this.selectedBusiness.unSkilledEmp['female']
          this.unSkillEmp = this.selectedBusiness.unSkilledEmp['totalUnskilled']

          this.maleSemiskilled = this.selectedBusiness.semiSkilledEmp['male']
          this.femaleSemiskilled = this.selectedBusiness.semiSkilledEmp['female']
          this.semiSkillEmp = this.selectedBusiness.semiSkilledEmp['totalSemiskilled']


          this.maleSkilled = this.selectedBusiness.skilledEmp['male']
          this.femaleSkilled = this.selectedBusiness.skilledEmp['female']
          this.skiledEmp = this.selectedBusiness.skilledEmp['totalUnskilled']
          this.calculateWages();
          this.familyMemberList = this.oldfamilyObject
          this.setOldValues();
        }
        else {
          this.setvalueOnCancel();
          this.familyMemberList = this.oldfamilyObject
          this.setOldValues();
        }
      }
      else if (this.femaleUnskilled > 0 || this.femaleSemiskilled > 0 || this.femaleSkilled > 0) {
        if (this.manageBusinessFlag || this.scaleupBusinessFlag) {
          if (this.femaleUnskilled > this.selectedBusiness.unSkilledEmp['female'] || this.femaleSemiskilled > this.selectedBusiness.semiSkilledEmp['female'] || this.femaleSkilled > this.selectedBusiness.skilledEmp['female']) {
            this.empSelectionFlag = false;
            this.salaryDesign();
            this.setEmpCost();

          }
          else if (this.femaleUnskilled > this.oldfemaleUnskilled || this.femaleSemiskilled > this.oldfemaleSemiskilled || this.femaleSkilled > this.oldfemaleSkilled) {
            this.empSelectionFlag = false;
            this.salaryDesign();
            this.setEmpCost();

          }
          else {
            this.empSelectionFlag = false;
            this.setValuesForEmp();
            this.setEmpCost();

          }
        }
        else {
          this.empSelectionFlag = false;
          this.salaryDesign();
          this.setEmpCost();

        }
      }
      else {
        this.empSelectionFlag = false;
        this.setValuesForEmp();
        this.setEmpCost();


      }
    }
    if (this.selectedBusinessType === 4) {
      if (this.unSkillEmp < this.unskillledMin || this.semiSkillEmp < this.semiskilledMin || this.skiledEmp < this.skilledMin) {
        this.alertFlag = true;
        this.messageText = this.translation.instant('gameLabel.To start a large business, you will need at least 30 semi-skilled and 60 skilled employees.')
        this.setvalueOnCancel();
        this.familyMemberList = this.oldfamilyObject
        this.setOldValues();
      }
      else if (this.unSkillEmp > this.unskillledMax || this.semiSkillEmp > this.semiskilledMax || this.skiledEmp > this.skilledMax) {
        this.alertFlag = true;
        this.messageText = this.translation.instant('gameLabel.The number of employees selected is greater than the maximum number of employees you can choose.')
        if (this.manageBusinessFlag) {
          this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
          this.maleUnskilled = this.selectedBusiness.unSkilledEmp['male']
          this.femaleUnskilled = this.selectedBusiness.unSkilledEmp['female']
          this.unSkillEmp = this.selectedBusiness.unSkilledEmp['totalUnskilled']

          this.maleSemiskilled = this.selectedBusiness.semiSkilledEmp['male']
          this.femaleSemiskilled = this.selectedBusiness.semiSkilledEmp['female']
          this.semiSkillEmp = this.selectedBusiness.semiSkilledEmp['totalSemiskilled']


          this.maleSkilled = this.selectedBusiness.skilledEmp['male']
          this.femaleSkilled = this.selectedBusiness.skilledEmp['female']
          this.skiledEmp = this.selectedBusiness.skilledEmp['totalUnskilled']
          this.calculateWages();
          this.familyMemberList = this.oldfamilyObject
          this.setOldValues();
        }
        else {
          this.setvalueOnCancel();
          this.familyMemberList = this.oldfamilyObject
          this.setOldValues();
        }
      }
      else if (this.femaleUnskilled > 0 || this.femaleSemiskilled > 0 || this.femaleSkilled > 0) {
        if (this.manageBusinessFlag || this.scaleupBusinessFlag) {
          if (this.femaleUnskilled > this.selectedBusiness.unSkilledEmp['female'] || this.femaleSemiskilled > this.selectedBusiness.semiSkilledEmp['female'] || this.femaleSkilled > this.selectedBusiness.skilledEmp['female']) {
            this.empSelectionFlag = false;
            this.salaryDesign();
            this.setEmpCost();

          }
          else {
            this.empSelectionFlag = false;
            this.setValuesForEmp();
            this.setEmpCost();

          }
        }
        else if (this.femaleUnskilled > this.oldfemaleUnskilled || this.femaleSemiskilled > this.oldfemaleSemiskilled || this.femaleSkilled > this.oldfemaleSkilled) {
          this.empSelectionFlag = false;
          this.salaryDesign();
          this.setEmpCost();

        }
        else {
          this.empSelectionFlag = false;
          this.setValuesForEmp();
          this.setEmpCost();

        }
      }
      else {
        this.empSelectionFlag = false;
        this.setValuesForEmp();
        this.setEmpCost();


      }
    }
  }


  manageBusinessSuccess() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.showBusinessList = false;
    this.luckyStart = false;
    this.businessSetupWindowShow = false;
    this.showBusinessWindow = false;
    this.startBusiness = false;
    this.secondScreen = false;
    this.firstScreen = false;
    this.thirdScreen=false;
    this.setBusinessObjectToSave();
    this.carrerService.manageBusiness(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.saveCarrerObject).subscribe(
      res => {
        this.result = res;
        this.modalWindowShowHide.showWaitScreen = false; this.firstCards = [];
        this.allCarrerCards = [];
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
        this.firstCards.push(this.allCarrerCards[this.allCarrerCards.length - 1]);
        if (this.firstCards[0].careerType === "business") {
          this.setBusinessCardValues(this.firstCards)
        }
        this.allCarrerCards.reverse();
        if (this.manageBusinessFlag) {
          this.messageText = this.translation.instant('gameLabel.Congratulations! You have successfully managed your business and achieved your goals.');
        }
        else if (this.scaleupBusinessFlag) {
          if (this.selectedBusiness.businessType > this.selectedBusinessType) {
            this.messageText = this.translation.instant('gameLabel.Congratulations on successfully restructuring your business to scale down and meet the changing market conditions');
          }
          else {
            this.messageText = this.translation.instant('gameLabel.Congratulations on successfully restructuring your business to scale up and meet the changing market conditions')
          }
        }
        this.messageClass = "successpopup";
        this.successMsgFlag = true;
        this.carrerService.loanFlag = false;
      }
    )
  }


  getALoan() {
    this.modalWindowShowHide.showActionFinanace = true;
  }

  alertClose() {
    this.alertFlag = false;
    if (this.notStartBusinessFlag)
      this.cancelBusinessCheck();
    this.notStartBusinessFlag = false;
  }

  scaleUpBusinessSucess() {
    if (this.selectedAmountFromLoanWindow + (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessValue - this.startup_capital) < 0) {
      this.carrerService.loanFlag = true
      this.translation.get('gameLabel.The_amount_of_initial_cash_you_are_trying_to_invest_is_greater_than_the_cash_currently_available_with_you', { houseHoldCash: Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
        this.messageText = s;
        this.messageClass = "errorpopup";
        this.successMsgFlag = true;
      })
    }
    else {
      this.manageBusinessSuccess()
    }

  }

  salaryDesign() {
    this.salaryDifferencPer = (100 - (this.perSemiskilleFCost / this.perSemiskilleMCost) * 100)
    this.salaryChoice = [{
      "name": this.translation.instant('gameLabel.op1'),
      "value": 1
    },
    {
      "name": this.translation.instant('gameLabel.op2'),
      "value": 2
    }]
    this.translation.get('gameLabel.emp_sal_warr', { country: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country, salaryDifferencPer: parseInt(this.salaryDifferencPer) }).subscribe((s: string) => {
      this.messageText = s;
      this.salaryFlag = true;

    })
    // this.messageText = "In " + this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country + " women earn " + parseInt(this.salaryDifferencPer) + "% than their male counterparts. Do you want to pay them paid less or an equal salary to men? "
  }

  setPerPersonSalaryOfEmp() {
    //unskille
    //male
    this.perUnskilleMCost = this.setPerPersonmaleSalary(this.sigmaDistanceUnskilled)
    //female
    this.perUnskilleFCost = this.setPerPersonFemaleSalary(this.sigmaDistanceUnskilled)

    //semiSkille 
    //male
    this.perSemiskilleMCost = this.setPerPersonmaleSalary(this.sigmaDistanceSemiskilled)
    //female
    this.perSemiskilleFCost = this.setPerPersonFemaleSalary(this.sigmaDistanceSemiskilled)


    //skilee
    //male
    this.perSkilleMCost = this.setPerPersonmaleSalary(this.sigmaDistanceSkilled)
    //female
    this.perSkilleFCost = this.setPerPersonFemaleSalary(this.sigmaDistanceSkilled)


  }

  calculateFamilyHumanCapital() {
    this.familyHumanCapital = 0

    if (this.flagForMaleSal) {
      let fUnskilledCost = (((this.famMunskilled) * this.perUnskilleMCost) + ((this.famFunskilled) * this.perUnskilleMCost));
      let fSemiCost = (((this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.famFsemiskilled) * this.perSemiskilleMCost))
      let fSkilledCost = (((this.famMskilled) * this.perSkilleMCost) + ((this.famFskilled) * this.perSkilleMCost))
      this.familyHumanCapital = fUnskilledCost + fSemiCost + fSkilledCost
    }
    else if (this.flagForSameSal) {
      let fUnskilledCost = (((this.famMunskilled) * this.perUnskilleMCost) + ((this.famFunskilled) * this.perUnskilleFCost));
      let fSemiCost = (((this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.famFsemiskilled) * this.perSemiskilleFCost))
      let fSkilledCost = (((this.famMskilled) * this.perSkilleMCost) + ((this.famFskilled) * this.perSkilleFCost))
      this.familyHumanCapital = fUnskilledCost + fSemiCost + fSkilledCost
    }

  }

  setValuesForEmp() {
    this.totalUnSkilAmount = (((this.maleUnskilled - this.famMunskilled) * this.perUnskilleMCost) + ((this.femaleUnskilled - this.famFunskilled) * this.perUnskilleFCost));
    this.totalSemiSkilAmount = (((this.maleSemiskilled - this.famMsemiskilled) * this.perSemiskilleMCost) + ((this.femaleSemiskilled - this.famFsemiskilled) * this.perSemiskilleFCost))
    this.totalSkilAmount = (((this.maleSkilled - this.famMskilled) * this.perSkilleMCost) + ((this.femaleSkilled - this.famFskilled) * this.perSkilleFCost))
    this.monthlyExpenceLabour = this.totalSemiSkilAmount + this.totalUnSkilAmount + this.totalSkilAmount
    this.checkAllExepence();
    if (this.manageBusinessFlag || this.scaleupBusinessFlag) {
      this.selectedBusiness.unSkilledEmp['male'] = this.maleUnskilled
      this.selectedBusiness.unSkilledEmp['female'] = this.femaleUnskilled
      this.selectedBusiness.unSkilledEmp['totalUnskilled'] = this.unSkillEmp

      this.selectedBusiness.semiSkilledEmp['male'] = this.maleSemiskilled
      this.selectedBusiness.semiSkilledEmp['female'] = this.femaleSemiskilled
      this.selectedBusiness.semiSkilledEmp['totalSemiskilled'] = this.semiSkillEmp


      this.selectedBusiness.skilledEmp['male'] = this.maleSkilled
      this.selectedBusiness.skilledEmp['female'] = this.femaleSkilled
      this.selectedBusiness.skilledEmp['totalUnskilled'] = this.skiledEmp
    }
  }

  changeFamilyMemberSelectionChanges() {
    for (let i = 0; i < this.familyMemberList.length; i++) {
      for (let j = 0; j < this.selectedBusiness.famMember.length; j++) {
        if (this.selectedBusiness.famMember[j].id === this.familyMemberList[i].id) {
          this.familyMemberList[i].check = true;
          if (this.familyMemberList[i].empType === 1 && this.familyMemberList[i].sex === 'Male') {
            this.famMunskilled = this.famMunskilled + 1;
          }
          else if (this.familyMemberList[i].empType === 1 && this.familyMemberList[i].sex === 'Female') {
            this.famFunskilled = this.famFunskilled + 1;
          }


          if (this.familyMemberList[i].empType === 2 && this.familyMemberList[i].sex === 'Male') {
            this.famMsemiskilled = this.famMsemiskilled + 1;
          }
          else if (this.familyMemberList[i].empType === 2 && this.familyMemberList[i].sex === 'Female') {
            this.famFsemiskilled = this.famFsemiskilled + 1;
          }


          if (this.familyMemberList[i].empType === 3 && this.familyMemberList[i].sex === 'Male') {
            this.famMskilled = this.famMskilled + 1;
          }
          else if (this.familyMemberList[i].empType === 3 && this.familyMemberList[i].sex === 'Female') {
            this.famFskilled = this.famFskilled + 1;
          }
        }
      }
    }
  }

  calculateFamemeberSelectionChanges(id, flag) {
    for (let i = 0; i < this.familyMemberList.length; i++) {
      if (this.familyMemberList[i].id === id) {
        if (flag) {
          this.familyMemberList[i].check = false;
        }
        else {
          this.familyMemberList[i].check = true;
        }
      }
    }
  }


  setOldValues() {
    this.famMunskilled = 0;
    this.famFunskilled = 0;
    this.famMsemiskilled = 0;
    this.famFsemiskilled = 0;
    this.famMskilled = 0;
    this.famFskilled = 0;
    for (let i = 0; i < this.familyMemberList.length; i++) {
      if (this.familyMemberList[i].check) {
        if (this.familyMemberList[i].empType === 1 && this.familyMemberList[i].sex === 'Male') {
          this.famMunskilled = this.famMunskilled + 1;
        }
        else if (this.familyMemberList[i].empType === 1 && this.familyMemberList[i].sex === 'Female') {
          this.famFunskilled = this.famFunskilled + 1;
        }


        if (this.familyMemberList[i].empType === 2 && this.familyMemberList[i].sex === 'Male') {
          this.famMsemiskilled = this.famMsemiskilled + 1;
        }
        else if (this.familyMemberList[i].empType === 2 && this.familyMemberList[i].sex === 'Female') {
          this.famFsemiskilled = this.famFsemiskilled + 1;
        }


        if (this.familyMemberList[i].empType === 3 && this.familyMemberList[i].sex === 'Male') {
          this.famMskilled = this.famMskilled + 1;
        }
        else if (this.familyMemberList[i].empType === 3 && this.familyMemberList[i].sex === 'Female') {
          this.famFskilled = this.famFskilled + 1;
        }
      }
    }
  }

  checkAndCalculateInitialCostOfBusiness() {
    let smallStartup = 0, mediumStartup = 0, largeStartup = 0; this.calculatedBusinessType = 0
    if (this.selectedBusiness.business_type > 1) {
      if (this.selectedBusiness.business_type === 2) {
        this.selectedBusinessType = 2
        smallStartup = this.setInitialValuesForSmallBusiness();
        this.selectedBusinessType = 3
        mediumStartup = this.setInitialValuesForMediumBusiness();
        this.selectedBusinessType = 4
        largeStartup = this.setInitialValuesForLargeBusiness();
      }
      else if (this.selectedBusiness.business_type === 3) {
        this.selectedBusinessType = 3
        mediumStartup = this.setInitialValuesForMediumBusiness();
        this.selectedBusinessType = 4
        largeStartup = this.setInitialValuesForLargeBusiness();
      }
      else if (this.selectedBusiness.business_type === 4) {
        this.selectedBusinessType = 4
        largeStartup = this.setInitialValuesForLargeBusiness();
      }
    }
    else {
      this.selectedBusinessType = 2
      if (this.selectedBusinessType === 2) {
        this.selectedBusinessType = 2
        smallStartup = this.setInitialValuesForSmallBusiness();
        this.selectedBusinessType = 3
        mediumStartup = this.setInitialValuesForMediumBusiness();
        this.selectedBusinessType = 4
        largeStartup = this.setInitialValuesForLargeBusiness();
      }
      this.selectedBusinessType = 3
      if (this.selectedBusinessType === 3) {
        this.selectedBusinessType = 3
        mediumStartup = this.setInitialValuesForMediumBusiness();
        this.selectedBusinessType = 4
        largeStartup = this.setInitialValuesForLargeBusiness();
      }
      this.selectedBusinessType = 4
      if (this.selectedBusinessType === 4) {
        this.selectedBusinessType = 4
        largeStartup = this.setInitialValuesForLargeBusiness();
      }
    }

    this.calculateAvalabelLoanAmount();
    let totalLoanValue = this.aveLoanAmount + this.sharkLoanAmount + this.businessHouseHoldCash
    if (smallStartup < totalLoanValue) {
      this.calculatedBusinessType = 2
    }
    if (mediumStartup < totalLoanValue) {
      this.calculatedBusinessType = 3;

    }
    if (largeStartup < totalLoanValue) {
      this.calculatedBusinessType = 4;
    }
    if (this.calculatedBusinessType === 0) {
      this.calculatedBusinessType = 1;
    }
    if (this.selectedBusiness.business_type < this.calculatedBusinessType) {
      this.selectedBusinessType = this.calculatedBusinessType
      this.businessTypeList = this.carrerService.createBusinessTypeArray(this.selectedBusinessType, true, 0);
      this.filValueForSelection();
    }
    else if (this.selectedBusiness.business_type === this.calculatedBusinessType) {
      this.selectedBusinessType = this.calculatedBusinessType
      this.businessTypeList = this.carrerService.createBusinessTypeArray(this.selectedBusinessType, true, 1);
      this.filValueForSelection();
    }
    else if (this.selectedBusiness.business_type > this.calculatedBusinessType) {
      this.alertFlag = true;
      this.messageText = this.translation.instant('gameLabel.Your current household net worth is not sufficient to start your business.')
      this.notStartBusinessFlag = true;
      this.setAllCostToZero();
    }
  }

  setInitialValueForMicroBusiness() {
    this.setAllCostToZero();
    this.calculateInitialCapital();
    this.setInitialCapital();
  }


  setInitialValuesForSmallBusiness() {
    this.setAllCostToZero();
    this.calculateInitialCapital();
    this.setInitialCapital();
    this.checkAmenitiesForBusiness();
    this.setInitialEmp();
    this.setPerPersonSalaryOfEmp();
    this.calculateWages();
    this.TypeOfWorlPlaces(2);
    return this.startup_capital;
  }

  setInitialValuesForMediumBusiness() {
    this.setAllCostToZero();
    this.calculateInitialCapital();
    this.setInitialCapital();
    this.checkAmenitiesForBusiness();
    this.setInitialEmp();
    this.setPerPersonSalaryOfEmp();
    this.calculateWages();
    this.TypeOfWorlPlaces(3);
    return this.startup_capital;

  }

  setInitialValuesForLargeBusiness() {
    this.setAllCostToZero();
    this.setInitialCapital();
    this.checkAmenitiesForBusiness();
    this.setInitialEmp();
    this.setPerPersonSalaryOfEmp();
    this.calculateWages();
    this.TypeOfWorlPlaces(4);
    return this.startup_capital;

  }

  calculateAvalabelLoanAmount() {
    this.takenSharkLoanAmount = 0,
      this.takenLoanAmt = 0;
    this.takenSharkAmt = 0;
    this.takenBankLoan = 0;
    this.takenFriendLoan = 0;
    this.takenFamilyLoan = 0;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities <= 0) {
      let householdAssetsPer = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6);
      this.aveLoanAmount = householdAssetsPer;
      this.sharkLoanAmount = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash * 1.2);
      this.takenLoanAmt = 0;
      this.takenSharkAmt = 0;
    }
    else {
      for (let i = 0; i < this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.length; i++) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 7) {
          this.takenSharkAmt = Math.round(this.takenSharkAmt + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance);
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 6) {
          this.takenBankLoan = Math.round(this.takenBankLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance)
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 8) {
          this.takenFriendLoan = Math.round(this.takenFriendLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance)
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 9) {
          this.takenFamilyLoan = Math.round(this.takenFamilyLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance)
        }
      }
      this.takenLoanAmt = this.takenBankLoan
      this.sharkLoanAmount = this.takenSharkAmt
      let avaLoan = Math.round(Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6) - this.takenLoanAmt);
      let aveShark = Math.round(Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash * 1.2) - this.sharkLoanAmount);
      if (avaLoan > this.takenLoanAmt) {
        this.aveLoanAmount = Math.round(avaLoan - this.takenLoanAmt)
      }
      if (aveShark > this.takenSharkAmt) {
        this.sharkLoanAmount = Math.round(aveShark - this.takenSharkAmt);
      }
    }
  }


  setRegisterUnregister() {
    this.regArr = [{
      "name": this.translation.instant('gameLabel.Registered1'),
      "value": 1
    },
    {
      "name": this.translation.instant('gameLabel.Unregistered'),
      "value": 2
    }
    ]
    if (this.registerFlag) {
      this.regUnregValue = 1
      this.regArr[0].name = this.translation.instant('gameLabel.Regitered (Selected)')
      this.regArr[1].name = this.translation.instant('gameLabel.Unregistered')
    }
    else {
      this.regUnregValue = 2
      this.regArr[1].name = this.translation.instant('gameLabel.Unregistered (Selected)')
      this.regArr[0].name = this.translation.instant('gameLabel.Registered1')
    }

  }

  setBusiness(regUnregValue) {
    if (regUnregValue == 1) {
      this.regArr[0].name = this.translation.instant('gameLabel.Regitered (Selected)')
      this.regArr[1].name = this.translation.instant('gameLabel.Unregistered')
      this.registerFlag = true;
      if (this.originalComplianceCost === 0) {
        this.showJobConfirmWindow = true;
        this.curuptionWarrFlag = true;
        this.registerBusinessWarr = false;
        this.registerBusinessYes()
      }
      else {
        this.bribeCost = this.originalBribeCost;
        this.complentionCost = this.originalComplianceCost
        this.checkAllExepence();
      }

    }
    else if (regUnregValue == 2) {
      this.regArr[1].name = this.translation.instant('gameLabel.Unregistered (Selected)')
      this.regArr[0].name = this.translation.instant('gameLabel.Registered1')
      this.registerFlag = false;
      this.bribeCost = 0;
      this.complentionCost = 0
      this.checkAllExepence();
    }
  }

  checkRedGreenClassForCashValues() {
    if (this.startup_capital > this.selectedAmountFromLoanWindow) {
      this.selectedCashClass = "pr-3 textSize red"
    }
    else if (this.selectedAmountFromLoanWindow === 0) {
      this.selectedCashClass = "pr-3 textSize"
    }
    else {
      this.selectedCashClass = "pr-3 textSize green"
    }

  }

  checkRedGreenClassForCashValuesManage() {
    this.businessHouseHoldCash = parseInt(this.businessHouseHoldCash)
    if (this.selectedBusiness.businessValue - this.startup_capital < 0) {
      this.flagToShowSharkLoan = true;
    }
    else {
      this.flagToShowSharkLoan = false;
    }

    if (this.startup_capital < this.selectedBusiness.businessValue) {
      this.cashClass = "pr-3 textSize green"
    }
    else if (this.businessHouseHoldCash === 0) {
      this.cashClass = "pr-3 textSize "
    }
    else if (this.diffBvaleCash > this.businessHouseHoldCash) {
      this.cashClass = "pr-3 textSize red"
    }
    else {
      this.cashClass = "pr-3 textSize green"
      this.selectedAmountFromLoanWindow = this.selectedBankLoan + this.selectedBusinessCash + this.selectedFriendLoan + this.selectedSharkLoan
    }


    if (this.selectedAmountFromLoanWindow === 0) {
      this.selectedCashClass = "pr-3 textSize red"
    }
    else if (this.diffBvaleCash < this.selectedAmountFromLoanWindow) {
      this.selectedCashClass = "pr-3 textSize green"
    }
    else {
      this.selectedCashClass = "pr-3 textSize red"
    }
  }





  takeLoanClick() {
    this.calculateAvalabelLoanAmount();
    this.aveFamilyLoan = 0;
    this.loanWindoFlag = true;
    this.businessHouseHoldCash = 0
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    }
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.businessHouseHoldCash = parseInt(this.businessHouseHoldCash)
    if (this.businessHouseHoldCash >= this.startup_capital) {
      this.businessHouseHoldCash = this.startup_capital;
    }
    if (this.aveLoanAmount >= this.startup_capital) {
      this.aveLoanAmount = this.startup_capital;
    }
    if (this.sharkLoanAmount >= this.startup_capital) {
      this.sharkLoanAmount = this.startup_capital;
    }
    if (this.manageBusinessFlag || this.scaleupBusinessFlag) {
      this.checkRedGreenClassForCashValuesManage();
    }
    else {
      this.checkRedGreenClassForCashValues();
    }
    if (this.selectedBusinessCash === this.businessHouseHoldCash && this.selectedBankLoan === this.aveLoanAmount) {
      if (this.selectedAmountFromLoanWindow < this.startup_capital) {
        this.flagForFriend = true
      }
    }
    else {
      this.flagForFriend = false;
      this.selectedFriendLoan = 0;

    }
    this.friendLoanArr = [
      {
        "name": this.translation.instant('gameLabel.Yes'),
        "value": 1
      },
      {
        "name": this.translation.instant('gameLabel.No'),
        "value": 2
      },
    ]
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.aveFamilyLoan = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12))
    }
    else {
      this.aveFamilyLoan = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    }
  }

  setUpLoanOk() {
    let addCapital = this.startup_capital + ((this.startup_capital * 5) / 100)
    if (this.selectedAmountFromLoanWindow > addCapital) {
      this.messageText = this.translation.instant('gameLabel.Select either cash or a loan amount that is no more than 5% higher than the required capital for your business, or choose the exact amount.');
      this.alertFlag = true;
    }
    else {
      this.loanWindoFlag = false;
      if (this.scaleupBusinessFlag || this.manageBusinessFlag) {
        this.checkRedGreenClassForCashValuesManage();
      }
      else {
        this.checkRedGreenClassForCashValues();
      }
      this.setOldLoanValues();
    }
  }

  setUpLoanCancel() {
    this.loanWindoFlag = false;
    this.selectedFriendLoan = this.oldSelectedFriendLoan;
    this.selectedBankLoan = this.oldSelectedBankLoan
    this.selectedBusinessCash = this.oldSelectedBusinessCash;
    this.selectedSharkLoan = this.oldSelectedSharkLoan
    this.selectedAmountFromLoanWindow = this.oldSelectedAmountFromLoanWindow
    this.friendLoanValue = this.oldFreindIndex;
    this.originalHouseholdCash = this.businessHouseHoldCash;
    this.fCnt = this.oldFcet;
    if (this.scaleupBusinessFlag || this.manageBusinessFlag) {
      this.checkRedGreenClassForCashValuesManage();
    }
    else {
      this.checkRedGreenClassForCashValues();
    }
    // this.setLoanValues()
  }

  setOldLoanValues() {
    this.oldSelectedBankLoan = this.selectedBankLoan
    this.oldSelectedBusinessCash = this.selectedBusinessCash;
    this.oldSelectedFriendLoan = this.selectedFriendLoan
    this.oldSelectedSharkLoan = this.selectedSharkLoan
    this.oldFriendValue = this.orgFloan;
    this.oldFcet = this.oldFcet;
    this.oldFreindIndex = this.friendLoanValue
    this.oldSelectedAmountFromLoanWindow = this.selectedAmountFromLoanWindow

  }

  setLoanValues() {
    this.selectedAmountFromLoanWindow = 0;
    if (this.selectedBankLoan !== this.aveLoanAmount) {
      this.selectedFriendLoan = 0;
    }
    this.selectedAmountFromLoanWindow = Math.round(this.selectedBankLoan) + Math.round(this.selectedBusinessCash) + Math.round(this.selectedFriendLoan) + Math.round(this.selectedSharkLoan)
    this.aveLoanAmount = Math.round(this.aveLoanAmount);
    if (this.selectedBusinessCash === this.businessHouseHoldCash && this.selectedBankLoan === this.aveLoanAmount) {
      if (this.selectedAmountFromLoanWindow < this.startup_capital) {
        this.flagForFriend = true
      }
    }
    else {
      this.flagForFriend = false;
      this.selectedFriendLoan = 0;

    }
    if (this.manageBusinessFlag || this.scaleupBusinessFlag) {
      this.checkRedGreenClassForCashValuesManage();
    }
    else {
      this.checkRedGreenClassForCashValues();
    }
  }


  setFriendValues(value) {
    if (this.selectedAmountFromLoanWindow >= this.startup_capital) {
      this.fCnt = 0;
      this.alertFlag = true;
      this.messageText = this.translation.instant('gameLabel.Your loan installment is already taken care of, so you may not need to ask your friends for help.')
      this.selectedFriendLoan = 0
      this.orgFloan = 0
    }
    if (this.fCnt < 1) {
      if (value === 1) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].traits.spiritual > 35) {
          if (this.takenFriendLoan > 0) {
            this.alertFlag = true;
            this.messageText = this.translation.instant('gameLabel.Unfortunately, your friend is unable to assist you with paying off your loans at this time.')
            this.selectedFriendLoan = 0
            this.orgFloan = this.selectedFriendLoan;
          }
          else {
            this.fCnt = this.fCnt + 1;
            let friendValue = Math.round(this.startup_capital - this.selectedAmountFromLoanWindow)
            let random = this.commonService.getRandomValue(10, 50);
            this.selectedFriendLoan = (friendValue * (random / 100))
            this.orgFloan = this.selectedFriendLoan;
          }
        }
        else {
          this.alertFlag = true;
          this.messageText = this.translation.instant('gameLabel.Unfortunately, your friend has rejected your request for help this time.')
          this.selectedFriendLoan = 0
          this.orgFloan = this.selectedFriendLoan;
        }
      }
      else if (value === 2) {
        this.selectedFriendLoan = 0
      }
      this.setLoanValues();
    }
    else {
      if (value === 1) {
        this.selectedFriendLoan = this.orgFloan
      }
      else if (value === 2) {
        this.selectedFriendLoan = 0
      }
      this.setLoanValues();
    }
  }

  setSelfSalary() {
    this.checkAllExepence();
  }

  setCurrentEmp() {
    this.addedmaleSkilled = 0;
    this.addedmaleSemiskilled = 0;
    this.addedmaleUnskilled = 0;
    this.addedfemaleSkilled = 0;
    this.addedfemaleSemiskilled = 0;
    this.addedfemaleUnskilled = 0;


    this.addedmaleSkilled = this.maleSkilled;
    this.addedfemaleSkilled = this.femaleSkilled

    this.addedmaleSemiskilled = this.maleSemiskilled;
    this.addedfemaleSemiskilled = this.femaleSemiskilled

    this.addedmaleUnskilled = this.maleUnskilled;
    this.addedfemaleUnskilled = this.femaleUnskilled
  }

  setvalueOnCancel() {
    this.maleSkilled = this.addedmaleSkilled;
    this.femaleSkilled = this.addedfemaleSkilled

    this.maleSemiskilled = this.addedmaleSemiskilled;
    this.femaleSemiskilled = this.addedfemaleSemiskilled

    this.maleUnskilled = this.addedmaleUnskilled;
    this.femaleUnskilled = this.addedfemaleUnskilled
  }

  calculateInitialCapital() {
    this.calculateWealth();
    let exp = Math.exp(this.welthIncome + (this.selectedBusiness.BusinessSigmaSpread * this.wealthDeviation));
    var income = exp;
    income = income / 12;
    this.originalStratupCapital = income * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
  }

  scaleUpBusinessSet() {
    this.totalSpaceCost = 0
    this.amenitiesCost = 0
    this.complentionCost = 0;
    this.bribeCost = 0;
    this.fskilled = 0
    this.fsemiSkilled = 0; this.fskilled = 0
    this.skiledEmp = 0
    this.unSkillEmp = 0
    this.semiSkillEmp = 0
    this.selectedWorlPlaceValue = 0
    this.monthlyExpenceLabour = 0;
    this.businessTypeFlag = true;
    this.registerBusinessWarr = false;
    this.showJobConfirmWindow = false;
    this.setPerPersonSalaryOfEmp();
    this.setInitialEmp();
    this.calculateWages();
    this.calculateHumanCapital(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
    if (this.selectedBusinessType >= 2) {
      this.TypeOfWorlPlaces(this.selectedBusinessType);
      this.calculateWages();
    }
    this.setInitialCapital();
    this.getNameOfBusinessType();
    this.checkAmenitiesForBusiness();
    this.setValuesForEmp();
    this.setFamilyMemberList();
    this.changeFamilyMemberSelectionChanges()
    this.calculateFamilyHumanCapital();
    this.calculateAvalabelLoanAmount();
    this.setRegisterUnregister();
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses > 0) {
      this.businessHouseHoldCash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash - (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12)).toFixed(0)
    }
    else {
      this.businessHouseHoldCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.minimalDietCost +
        this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.simplestSharedShelterCost
        + this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.miserlyConsumerCost) * 12)
    }
    this.originalHouseholdCash = this.businessHouseHoldCash;
    this.selectedAmountFromLoanWindow = this.selectedBankLoan + this.selectedBusinessCash + this.selectedFriendLoan + this.selectedSharkLoan
    if (this.businessHouseHoldCash <= 0) {
      this.businessHouseHoldCash = 0;
    }
    this.profitPerValue = this.selectedBusiness.profitMarginPer;
    this.selfSalary = this.selectedBusiness.selfSalary
    this.checkAllExepence();
    this.firstScreen = true;
    this.showBusinessWindow = true;
    this.startBusiness = true;
  }


  checkFamMemberInvolve() {
    if (this.famFunskilled > 0 || this.famMunskilled > 0) {
      let fUnskilled = this.famMunskilled + this.famFunskilled
      let unSkilled = this.maleUnskilled + this.femaleUnskilled
      if (unSkilled < fUnskilled) {
        if (this.maleUnskilled < this.famMskilled) {
          this.maleUnskilled = this.maleUnskilled + this.famMunskilled
        }
        if (this.femaleUnskilled < this.famFunskilled) {
          this.femaleUnskilled = this.femaleUnskilled + this.famMunskilled
        }
      }
    }
    if (this.famFsemiskilled > 0 || this.famMsemiskilled > 0) {
      let fsemiSkilled = this.famFsemiskilled + this.famMsemiskilled
      let semiskilled = this.maleSemiskilled + this.femaleSemiskilled
      if (semiskilled < fsemiSkilled) {
        if (this.maleSemiskilled < this.famMsemiskilled) {
          this.maleSemiskilled = this.maleSemiskilled + this.famMsemiskilled
        }
        if (this.femaleSemiskilled < this.famFsemiskilled) {
          this.femaleSemiskilled = this.femaleSemiskilled + this.famFsemiskilled
        }
      }
    }
    if (this.famFskilled > 0 || this.famMskilled > 0) {
      let fskilled = this.famMskilled + this.famFskilled
      let skilled = this.maleSkilled + this.femaleSkilled
      if (skilled < fskilled) {
        if (this.maleSkilled < this.famMskilled) {
          this.maleSkilled = this.maleSkilled + this.famMskilled
        }
        if (this.femaleSkilled < this.famFskilled) {
          this.femaleSkilled = this.femaleSkilled + this.famFskilled
        }
      }
    }
  }

  goSecondScreenYes() {
    this.firstScreen = false;
    this.thirdScreen=false;
    this.secondScreen = true;

  }
  goThirdScreenYes() {
    this.secondScreen = false;
    this.firstScreen = false
    this.thirdScreen=true;
  }

  goSecondScreenBack() {
    this.secondScreen = false;
    this.firstScreen = true
    this.thirdScreen=false;
  }

  goThirdScreenBack(){
    this.secondScreen = true;
    this.firstScreen = false
    this.thirdScreen=false;
  }

  empSelection(){
    this.thirdScreen=true;
    this.firstScreen=false;
    this.secondScreen=false;
  }


  SetSmallBusinessScreen(){
    this.thirdScreen=true;
    this.firstScreen=false;
    this.secondScreen=false;
  }

 

  calculateSpaceCostPerEmpChanges(selectedEmp, maxEmp, minEmp) {
    if (selectedEmp > minEmp) {
      let incPer = ((selectedEmp / maxEmp) * 100) / 100
      let incSpaceCost = parseInt((this.initialSpaceCost * incPer).toFixed(0))
      this.totalSpaceCost = (this.initialSpaceCost + incSpaceCost).toFixed(0)
    }
    else {
      this.totalSpaceCost = this.initialSpaceCost

    }
    this.checkAllExepence();

  }


  setEmpCost() {
    this.total = this.unSkillEmp + this.semiSkillEmp + this.skiledEmp
    if (this.selectedBusinessType === 2) {
      this.calculateSpaceCostPerEmpChanges(this.total, this.maxSmall, this.minSmall)
    }
    else if (this.selectedBusinessType === 3) {
      this.calculateSpaceCostPerEmpChanges(this.total, this.maxMedium, this.minMedium)
    }
    else if (this.selectedBusinessType === 4) {
      this.calculateSpaceCostPerEmpChanges(this.total, this.maxlarge, this.minlarge)
    }
  }


  EmpSelectionChange() {
    this.unSkillEmp = this.maleUnskilled + this.femaleUnskilled;
    this.semiSkillEmp = this.maleSemiskilled + this.femaleSemiskilled;
    this.skiledEmp = this.maleSkilled + this.femaleSkilled;
    this.setEmpCost();

  }

  showCalculater() {
    this.calculaterFlag = true;
  }
  okExpCalculater() {
    this.calculaterFlag = false;

  }

  saveBusinessName() {
    this.BusineeTypeOk();
    this.setBusinessNameFlag = false;
  }

  okBusinessInfo() {
    this.startBusinessInfoFlag = false;
  }

  countryInfoArray() {
    this.startBusinessInfoFlag = true;
    this.startBUsinessCOuntryInfoArray = [];
    this.startBUsinessCOuntryInfoArray.push(
      {
        name: this.translation.instant('gameLabel.Ease_of_doing_business'),
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.EaseOfBusiness,
        info: this.translation.instant('gameLabel.ease_of_business'),
        moreInfo: ""
      },
      {
        name: this.translation.instant('gameLabel.Corruption'),
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.corruption +' / '+193,
        info: this.translation.instant('gameLabel.coor_rate'),
        moreInfo: this.translation.instant('gameLabel.corr_warr')
      },
      {
        name: this.translation.instant('gameLabel.Global Peace Score'),
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Peace_Rank +' / '+193,
        info: this.translation.instant('gameLabel.peace'),
        moreInfo: this.translation.instant('gameLabel.peace_Warr')
      },
      {
        name: this.translation.instant('gameLabel.HDI'),
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.HDI,
        info: this.translation.instant('gameLabel.Hdi'),
        moreInfo: this.translation.instant('gameLabel.Hdi_warr')
      },
      {
        name: this.translation.instant('gameLabel.Inflation_rate'),
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Inflation +"%",
        info: this.translation.instant('gameLabel.inflation'),
        moreInfo: ""
      }
    )
    this.selfEducation = this.commonService.setEducationForAll(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
  }

  moreInfoClick() {
    this.moreInfoFlag = true;
  }

  closeMoreInfo() {
    this.moreInfoFlag = false;

  }


  filValueForSelection() {
    for (let i = 0; i < this.carrerService.businessType.length; i++) {
      if (this.carrerService.businessType[i].Value === 1) {
        this.microDisable = this.carrerService.businessType[i].disabled;
        this.microValue = this.carrerService.businessType[i].Value
        this.microScaleup=this.carrerService.businessType[i].valueForScaleupBusiness

      }
      else if (this.carrerService.businessType[i].Value === 2) {
        this.smallDisable = this.carrerService.businessType[i].disabled;
        this.smallValue = this.carrerService.businessType[i].Value;
        this.smallScaleup=this.carrerService.businessType[i].valueForScaleupBusiness

      }
      else if (this.carrerService.businessType[i].Value === 3) {
        this.mediumDisable = this.carrerService.businessType[i].disabled;
        this.mediumValue = this.carrerService.businessType[i].Value;
        this.mediumScaleup=this.carrerService.businessType[i].valueForScaleupBusiness

      }
      else if (this.carrerService.businessType[i].Value === 4) {
        this.largeDisable = this.carrerService.businessType[i].disabled;
        this.largeValue = this.carrerService.businessType[i].Value;
        this.largeScaleup=this.carrerService.businessType[i].valueForScaleupBusiness

      }
    }
  }

  startOldBusinessYes() {
    let diff;
    let value = Math.round((70 / 100) * this.startup_capital)
    diff = this.startup_capital
    this.minimumInvestment = Math.round(this.selectedBusiness.MinimumInvestment);
    if (diff > this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash) {
      this.translation.get('gameLabel.The_amount_of_initial_cash_you_are_trying_to_invest_is_greater_than_the_cash_currently_available_with_you', { houseHoldCash: Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
        this.messageText = s;
        this.messageClass = "errorpopup";
        this.successMsgFlag = true;
      })
    }
    else if (this.startup_capital < this.minimumInvestment) {
      this.messageText = this.translation.instant('gameLabel.You_will_need_to_invest_enough_start_up_amount_to_at_least_cover_your_first_year_expenses');
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;

    }
    else if (this.startup_capital < this.yearly_investment) {
      this.messageText = this.translation.instant('gameLabel.You_will_need_to_invest_enough_starting_funds_to_cover_your_first_year_costs');
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
    else if (this.yearly_investment < value) {
      this.translation.get('gameLabel.You require minimum of amount as yearly expenses in cash.', { yearlyInvestMent: Math.round(value), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
        this.messageText = s;
        this.messageClass = "errorpopup";
        this.successMsgFlag = true;
      })
    }
    else {
      this.modalWindowShowHide.showWaitScreen = true;
      this.showBusinessList = false;
      this.businessSetupWindowShow = false;
      this.showBusinessWindow = false;
      this.startBusiness = false;
      this.diablegetJob = false;
      this.showOldBusiness = false;
      this.saveCarrerObject = {
        "jobId": 132,
        "currentSalary": 0,
        "careerType": "business",
        "fromAge": this.homeService.allPerson.SELF.age,
        "jobName": this.selectedBusiness.Business,
        "startUpCapital": (this.startup_capital),
        "yearlyInvestment": this.yearly_investment,
        "businessId": this.selectedBusiness.businessesId
      };

      this.carrerService.saveBusiness(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.saveCarrerObject).subscribe(
        res => {
          this.modalWindowShowHide.showWaitScreen = false; this.firstCards = [];
          this.allCarrerCards = [];
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
          this.firstCards.push(this.allCarrerCards[this.allCarrerCards.length - 1]);
          this.allCarrerCards.reverse();
          this.translation.get('gameLabel.You_successfully_started_a_business_old', { businessName: this.selectedBusiness.Business }).subscribe((s: string) => {
            this.messageText = s;
            this.successMsgFlag = true;
          })
        })

    }
    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.houseHoldCash = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.houseHoldCash - this.startup_capital + this.startup_capital;

  }

  startOldBusinessNo() {
    // this.showBusinessList = false;
    this.businessSetupWindowShow = false;
    this.startBusiness = false;
    // this.diablegetJob = false;
    this.cancelBusinessCheck();


  }


  manageOldBusinessYes() {
    let diff;
    let value = ((70 / 100) * this.startup_capital)
    this.selectedBusiness = this.homeService.allPerson[this.constantService.FAMILY_SELF].business;
    if (this.startup_capital > this.selectedBusiness.startUpCapital) {
      diff = this.startup_capital - this.selectedBusiness.startUpCapital
      if (diff > this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash) {
        this.translation.get('gameLabel.The_amount_of_initial_cash_you_are_trying_to_invest_is_greater_than_the_cash_currently_available_with_you', { houseHoldCash: Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
          this.messageText = s;
          this.messageClass = "errorpopup";
          this.successMsgFlag = true;
        })
      }
      else if (this.startup_capital < this.yearly_investment) {
        this.messageText = this.translation.instant('gameLabel.You_will_need_to_invest_enough_starting_funds_to_cover_your_first_year_costs');
        this.messageClass = "errorpopup";
        this.successMsgFlag = true;
      }
      else if (this.yearly_investment < value) {
        this.translation.get('gameLabel.You require minimum of amount as yearly expenses in cash.', { yearlyInvestMent: Math.round(value), CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code }).subscribe((s: string) => {
          this.messageText = s;
          this.messageClass = "errorpopup";
          this.successMsgFlag = true;
        })
      }
      else {
        this.modalWindowShowHide.showWaitScreen = true;
        this.showBusinessList = false;
        this.businessSetupWindowShow = false;
        this.showBusinessWindow = false;
        this.startBusiness = false;
        this.saveCarrerObject = {
          "jobId": 132,
          "currentSalary": 0,
          "careerType": "business",
          "fromAge": this.homeService.allPerson.SELF.age,
          "jobName": this.selectedBusiness.Business,
          "startUpCapital": this.startup_capital,
          "yearlyInvestment": this.yearly_investment,
          "businessId": this.selectedBusiness.businessesId
        };

        this.carrerService.manageBusiness(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.saveCarrerObject).subscribe(
          res => {
            this.result = res;
            this.modalWindowShowHide.showWaitScreen = false; this.firstCards = [];
            this.allCarrerCards = [];
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
            this.firstCards.push(this.allCarrerCards[this.allCarrerCards.length - 1]);
            this.allCarrerCards.reverse();
            this.messageText = this.translation.instant('gameLabel.You_successfully_scaled-up_your_business_old');
            this.messageClass = "successpopup";
            this.successMsgFlag = true;
          }
        )

      }
    }
  }


  manageOldBusinessNo() {
    this.businessSetupWindowShow = false;
    this.startBusiness = false;
    this.diablegetJob = false;
  }

  openFullscreen() {
    this.commonService.fullScreen();
  }
}
