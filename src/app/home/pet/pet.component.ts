import { Component, OnInit } from '@angular/core';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { PetService } from './pet.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { EventService } from '../event/event.service';
import { interval } from 'rxjs';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.css']
})
export class PetComponent implements OnInit {
  showDecision: boolean = false;
  showAlertFlag: boolean = false;
  petData;
  selectedActionForPet;
  displayDecision;
  selectedPetValue;
  textAreaValid: string = "";
  diiferenBetIdentity;
  textAreaOkButton: boolean = false;
  petTextAreaFlag: boolean = false;
  result;
  successMsgFlag: boolean = false;
  messageText = '';
  notShowPet: boolean = false;
  alertMsg;
  messageType;
  alertClass;

  constructor(public modalWindowShowHide: modalWindowShowHide,
    public commonService: CommonService,
    public translate: TranslateService,
    public homeService: HomeService,
    public eventService: EventService,
    public petService: PetService,
    public translationService: TranslationService,
    public agaAYearService: AgaAYearService,
    public constantService: ConstantService) {
    interval(30000).subscribe(x => {
      if (this.showAlertFlag) {
        this.showAlertFlag = false;
      }
    });
  }

  doCheck() {
    if (this.textAreaValid.length > 0) {
      this.textAreaOkButton = false;
    }
    else {
      this.textAreaOkButton = true;
    }
  }
  ngOnInit(): void {
    this.modalWindowShowHide.showWaitScreen = true
    if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].dead && this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 16) {
      this.alertMsg = this.translate.instant('gameLabel.You cannot take any action before age 16');
      this.messageType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
      this.notShowPet = true;
    }
    if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].dead && this.homeService.allPerson[this.constantService.FAMILY_SELF].my_pets.length > 2) {
      this.alertMsg =this.translate.instant('gameLabel.A maximum of three pets can be taken');
      this.messageType = "Warning: ";
      this.alertClass = "alert-warning";
      this.notShowPet = true;
      this.showAlertFlag = true;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
      this.alertMsg = this.translate.instant('gameLabel.You_cannot_take_any_action_because_you_are_dead');
      this.messageType = "Warning: ";
      this.alertClass = "alert-warning";
      this.notShowPet = true;
      this.showAlertFlag = true;
    }
    this.textAreaOkButton = true;
    this.selectedActionForPet = this.translate.instant('gameLabel.Dog')
    this.petService.getPetData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.petData = res;
        this.agaAYearService.setDecision(this.petData.data.result);
        this.displayDecision = this.agaAYearService.decision;
        for (let i = 0; i < this.displayDecision.choiceArray.length; i++) {
          if (i == 0) {
            this.selectedPetValue = this.displayDecision.choiceArray[i].Value;
            break;
          }
        }
        this.showDecision = true;
        this.modalWindowShowHide.showWaitScreen = false
      }
    )
  }

  successMsgWindowClose() {
    this.successMsgFlag = false;
  }

  clickPetDecision() {
    this.petTextAreaFlag = true;

  }

  setSelectedPet(pet) {
    this.selectedPetValue = pet;
  }

  closePetDecision() {
    this.modalWindowShowHide.showPetFlag = false;
  }

  closeTextAreaDecision() {
    this.petTextAreaFlag = false;
    this.textAreaValid = '';
    this.textAreaOkButton = true
  }

  clickTextDecision() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.textAreaOkButton = true;
    let pet = {
      "name": this.textAreaValid,
      "petType": this.selectedPetValue,
      "lang": this.translationService.selectedLang.code
    }
    this.petService.updatePet(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, pet).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.hasOwnProperty('difference')) {
          if (Object.keys(this.result.data.result.difference).length !== 0) {
            this.diiferenBetIdentity = this.result.data.result.difference;
            let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
            if (retuenValue !== 'false') {
              this.homeService.allPerson = retuenValue;
              this.homeService.changeAllPerson.emit(this.homeService.allPerson);
            }
          }
        }
        if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
          this.agaAYearService.nextEventResultIsEvent(this.result);
          this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
          this.modalWindowShowHide.showPetFlag = false;
        }
        this.modalWindowShowHide.showWaitScreen = false
      })
  }

  OnCloseWindowPopup() {
    this.modalWindowShowHide.showPetFlag = false;
  }

}
