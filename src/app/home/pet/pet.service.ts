import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(private http: HttpClient,
    private commonService: CommonService) { }

  getPetData(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/choosePet/${gameId}`);
  }

  updatePet(gameId, pet) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/savePet/${gameId}`, { pet });
  }
}

