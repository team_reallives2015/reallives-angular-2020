import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrandChildrenFamilyTreeComponent } from './grand-children-family-tree.component';

describe('GrandChildrenFamilyTreeComponent', () => {
  let component: GrandChildrenFamilyTreeComponent;
  let fixture: ComponentFixture<GrandChildrenFamilyTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrandChildrenFamilyTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrandChildrenFamilyTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
