import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganDonationCertificateComponent } from './organ-donation-certificate.component';

describe('OrganDonationCertificateComponent', () => {
  let component: OrganDonationCertificateComponent;
  let fixture: ComponentFixture<OrganDonationCertificateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganDonationCertificateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganDonationCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
