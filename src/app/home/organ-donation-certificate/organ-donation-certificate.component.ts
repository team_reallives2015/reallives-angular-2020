import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';

@Component({
  selector: 'app-organ-donation-certificate',
  templateUrl: './organ-donation-certificate.component.html',
  styleUrls: ['./organ-donation-certificate.component.css']
})
export class OrganDonationCertificateComponent implements OnInit {
count=0;
organeDonationObj=[];
  constructor(public modalWindowShowHide :modalWindowShowHide ,
    public commonService :CommonService,
    public homeService :HomeService,
    public constantService:ConstantService) { }

  ngOnInit(): void {
    this.modalWindowShowHide.showOrganDonationCertificate=true;
    this.fillOrganDonationCetificate();
  }

  OnCloseWindowPopup(){
    this.modalWindowShowHide.showOrganDonationCertificate=false;
  }

  fillOrganDonationCetificate(){
    for(let i=0;i<this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation.length;i++){
      //splite string for korean certificate images
      let str=this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation[i].imageLink
      let str1=str.split("-",2);
      let obj=str1[1];
      let obj1=obj.split(".",2)
      let obj2=obj1[0]
      obj2=this.capitalizeFirstLetter(obj2)
     this.organeDonationObj.push(
      {
        "organStatus" : this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation[i].organStatus,
            "imageLink" :this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation[i].imageLink,
            "organName" : this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation[i].organName,
            "certificateLink":"assets/images/organcertificate/"+obj2+".jpg"
      }
     )
    }
  }

  public capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
}
