import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { EventService } from '../event/event.service';
import { HomeService } from '../home.service';
import { PostGameService } from '../post-game.service';
import { SdgIndicatorsService } from '../sdg-indicators/sdg-indicators.service';
import { Data } from '@angular/router';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
sdgCommentList;
addCssForDisableHealth="";
addCssForDisableResistance="";
addCssForDisableHappiness="";
addCssForDisableIntelligence="";
addCssForDisableArtistic="";
addCssForDisableMusical="";
addCssForDisableAtheletic="";
addCssForDisableStrength="";
addCssForDisableEndurance="";
addCssForDisableSpiritual="";
addCssForDisableWisdom="";

addCsForCommonButton="";
public  commonSuggetion;
  public healthSuggetion;
  public resistanceSuggetion;
  public happinessSuggetion;
  public intelligenceSuggetion;
  public artisticSuggetion;
  public musicalSuggetion;
  public atheleticSuggetion;
  public strengthSuggetion;
  public  enduranceSuggetion;
  public spiritualSuggetion;
  public wisdomSuggetion;
  myGameSummarySuggetion;
  checkOrganCerFlag:boolean=false;
 lineChartDataHealth: ChartDataSets[]=[];
 lineChartLabelsHealth: Label[] = [];
 lineChartOptionsHealth;


 lineChartDataResistance: ChartDataSets[]=[];
 lineChartLabelsResistance: Label[] = [];
 lineChartOptionsResistance;


 lineChartDataHappiness: ChartDataSets[]=[];
 lineChartLabelsHappiness: Label[] = [];
 lineChartOptionsHappiness;


 lineChartDataIntelligence: ChartDataSets[]=[];
 lineChartLabelsIntelligence: Label[] = [];
 lineChartOptionsIntelligence;


 lineChartDataArtistic: ChartDataSets[]=[];
 lineChartLabelsArtistic: Label[] = [];
 lineChartOptionsArtistic;



 lineChartDataMusical: ChartDataSets[]=[];
 lineChartLabelsMusical: Label[] = [];
 lineChartOptionsMusical;


 lineChartDataAtheletic: ChartDataSets[]=[];
 lineChartLabelsAtheletic: Label[] = [];
 lineChartOptionsAtheletic;


 lineChartDataStrength: ChartDataSets[]=[];
 lineChartLabelsStrength: Label[] = [];
 lineChartOptionsStrength;



 lineChartDataEndurance: ChartDataSets[]=[];
 lineChartLabelsEndurance: Label[] = [];
 lineChartOptionsEndurance;


 lineChartDataSpiritual: ChartDataSets[]=[];
 lineChartLabelsSpiritual: Label[] = [];
 lineChartOptionsSpiritual;



 lineChartDataWisdom: Data[]=[];
 lineChartLabelsWisdom: Label[] = [];
 lineChartOptionsWisdom;




 lineChartColors: Color[] = [
   { // dark grey
    
   backgroundColor: 'rgba(236,236,236,1)',
     borderColor: 'rgba(86,124,170,1)'
   }
 ];
 lineChartLegend=false;
 lineChartType = 'line';
 lineChartPlugins = [];
 count=0;
 flagForCetificate:boolean=false;
  constructor(public modalWindowShowHide : modalWindowShowHide,
              public homeService :HomeService,
              public sdgIndicatorsService :SdgIndicatorsService,
              public eventService :EventService,
              public postGameService :PostGameService,
              public constantService:ConstantService,
              public translate :TranslateService) { }
              @ViewChild('GameSummerySave') public GameSummerySave: ModalDirective;

  ngOnInit(): void {
    this.modalWindowShowHide.showGameSummary=true;
    this.getAllSdgCommentData();
    this.generateHealthGraph();
    this.generateResistenceGraph();
    this.generateHappinessGraph();
    this.generateIntelligenceGraph();
    this.generateArtisticGraph();
    this.generateMusicalGraph();
    this.generateAthleticGraph();
    this.generateStrengthGraph();
    this.generateEnduranceGraph();
    this.generateSpiritualGraph();
    this.generateWisdomGraph();
    this.postGameService.getGameSummaryData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res=>{
        this.myGameSummarySuggetion=res;
        this.commonSuggetion=this.myGameSummarySuggetion.commonSuggestion;
        this.healthSuggetion=this.myGameSummarySuggetion.healthSuggestion;
        this.resistanceSuggetion=this.myGameSummarySuggetion.resistanceSuggestion;
        this.happinessSuggetion=this.myGameSummarySuggetion.happinessSuggestion;
        this.intelligenceSuggetion=this.myGameSummarySuggetion.intelligenceSuggestion;
        this.artisticSuggetion=this.myGameSummarySuggetion.artisicSuggestion;
        this.musicalSuggetion=this.myGameSummarySuggetion.musicalSuggestion;
        this.atheleticSuggetion=this.myGameSummarySuggetion.athleticSuggestion;
        this.strengthSuggetion=this.myGameSummarySuggetion.strengthSuggestion;
        this.enduranceSuggetion=this.myGameSummarySuggetion.enduranceSuggestion;
        this.spiritualSuggetion=this.myGameSummarySuggetion.spiritualSuggestion;
        this.wisdomSuggetion=this.myGameSummarySuggetion.wisdomSuggestion;
        this.checkDisableCss();
        let cnt=0;;
        let newOrganObject = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation));
         for(let i=0;i<newOrganObject.length;i++){
           if(newOrganObject[i].organStatus){
            cnt++;
            }
         }
         if(cnt===0){
this.checkOrganCerFlag=true;         }
      })
  }

  checkDisableCss(){
    if(this.commonSuggetion==='' || this.commonSuggetion===null){
      this.addCsForCommonButton="disable";
   }
   else{
     this.addCsForCommonButton="";
   }

   if( (this.healthSuggetion === ''  || this.healthSuggetion==null) ){
     this.addCssForDisableHealth="disable";
   }
   else{
     this.addCssForDisableHealth="";

   }

  if (this.resistanceSuggetion==''  || this.resistanceSuggetion==null) {
    this.addCssForDisableResistance="disable";
  }
  else{
   this.addCssForDisableResistance="";

  }
  if(this.happinessSuggetion==''  || this.happinessSuggetion==null){
    this.addCssForDisableHappiness="disable";
  }
  else{
   this.addCssForDisableHappiness="";

  }
  if (this.intelligenceSuggetion==''  || this.intelligenceSuggetion==null) {
    this.addCssForDisableIntelligence="disable";
  }
  else{
   this.addCssForDisableIntelligence="";
  }
 if(this.artisticSuggetion === ''  || this.artisticSuggetion === null) {
     this.addCssForDisableArtistic="disable";
 }else{
   this.addCssForDisableArtistic="";

 }
 if  (this.musicalSuggetion === ''  || this.musicalSuggetion==null) {
   this.addCssForDisableMusical="disable";
 }
 else{
   this.addCssForDisableMusical="";
 }
 if(this.atheleticSuggetion==''  || this.atheleticSuggetion==null)  {
   this.addCssForDisableAtheletic="disable";
 }
 else{
   this.addCssForDisableAtheletic="";

 }
  if (this.strengthSuggetion==''  || this.strengthSuggetion==null) {
     this.addCssForDisableStrength="disable";
  }
  else{
   this.addCssForDisableStrength="";
  }
  if (this.enduranceSuggetion==''  || this.enduranceSuggetion==null){
    this.addCssForDisableEndurance="disable";

  }
  else{
   this.addCssForDisableEndurance="";
  }
   if (this.spiritualSuggetion === ''  || this.spiritualSuggetion === null){
   this.addCssForDisableSpiritual="disable";
 }else{
   this.addCssForDisableSpiritual="";
 }
 if (this.wisdomSuggetion==''  || this.wisdomSuggetion==null){
   this.addCssForDisableWisdom="disable";
 }
 else{
   this.addCssForDisableWisdom="";
 }
  }

  getAllSdgCommentData(){
    this.sdgIndicatorsService.getAllSdgComment(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res=>{
       this.sdgCommentList=res;
      })
  }

generateHealthGraph(){
  let i=0;
  let dummyData=[]
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
       dummyData[i]=(this.homeService.allEvent[i].newTraits['health']);
      this.lineChartLabelsHealth.push(this.homeService.allEvent[i].age)
    }
             this.lineChartDataHealth=[{data:dummyData}];

    this.lineChartOptionsHealth = {
            elements: {
        line: {
            tension: 0 
        }
	},
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+this.translate.instant('gameLabel.health_At_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
}
 generateResistenceGraph(){
  let i=0;
  let dummyData=[]
  for(i=0;i<this.homeService.allEvent.length;i++)
  {
    dummyData[i]=(this.homeService.allEvent[i].newTraits['resistance']);
    this.lineChartLabelsResistance.push(this.homeService.allEvent[i].age)
  }

          this.lineChartDataResistance=[{data:dummyData}];
  this.lineChartOptionsResistance = {
    elements: {
      line: {
          tension: 0 
      }
},
    responsive: true,
    scales: {
      yAxes: [{
              display: true,
              ticks: {
                  max: 100
              }
          }]
  },
    plugins: {
     tooltip: {
      enabled: true,
       callbacks: {
        label: function(context) {
             var label =  '<span>'+ this.translate.instant('gameLabel.resistence_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
          return label;
      }
       }}
    }
  };
 }
  generateHappinessGraph(){
     
    let i=0,dummyData=[]
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
      dummyData[i]=(this.homeService.allEvent[i].newTraits['happiness']);
      this.lineChartLabelsHappiness.push(this.homeService.allEvent[i].age)
    }
    this.lineChartDataHappiness=[{data:dummyData}];
    this.lineChartOptionsHappiness = {
      elements: {
        line: {
            tension: 0 
        }
  },
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+this.translate.instant('gameLabel.happiness_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
  }
  generateIntelligenceGraph(){
    let i=0,dummyData=[];
  for(i=0;i<this.homeService.allEvent.length;i++)
  {
    dummyData[i]=(this.homeService.allEvent[i].newTraits['intelligence']);
    this.lineChartLabelsIntelligence.push(this.homeService.allEvent[i].age)
  }
  this.lineChartDataIntelligence=[{data:dummyData}];
  this.lineChartOptionsIntelligence = {
    elements: {
      line: {
          tension: 0 
      }
},
    responsive: true,
    scales: {
      yAxes: [{
              display: true,
              ticks: {
                  max: 100
              }
          }]
  },
    plugins: {
     tooltip: {
      enabled: true,
       callbacks: {
        label: function(context) {
             var label =  '<span>'+ this.translate.instant('gameLabel.intelligence_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
          return label;
      }
       }}
    }
  };
  }
  generateArtisticGraph(){
    
    let i=0,dummyData=[];
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
      dummyData[i]=(this.homeService.allEvent[i].newTraits['artistic']);
      this.lineChartLabelsArtistic.push(this.homeService.allEvent[i].age)
    }
    this.lineChartDataArtistic=[{data:dummyData}];
    this.lineChartOptionsArtistic = {
      elements: {
        line: {
            tension: 0 
        }
  },
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+ this.translate.instant('gameLabel.artistic_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
  }
  generateMusicalGraph(){
      
    let i=0,dummyData=[];
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
      dummyData[i]=(this.homeService.allEvent[i].newTraits['musical']);
      this.lineChartLabelsMusical.push(this.homeService.allEvent[i].age)
    }
    this.lineChartDataMusical=[{data:dummyData}];
    this.lineChartOptionsMusical = {
      elements: {
        line: {
            tension: 0 
        }
  },
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+ this.translate.instant('gameLabel.musical_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
  }
  generateAthleticGraph(){
    let i=0,dummyData=[];
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
      dummyData[i]=(this.homeService.allEvent[i].newTraits['athletic']);
      this.lineChartLabelsAtheletic.push(this.homeService.allEvent[i].age)
    }
    this.lineChartDataAtheletic=[{data:dummyData}];
    this.lineChartOptionsAtheletic = {
      elements: {
        line: {
            tension: 0 
        }
  },
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+ this.translate.instant('gameLabel.athletic_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
  }
  generateStrengthGraph(){
    let i=0,dummyData=[];
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
      dummyData[i]=(this.homeService.allEvent[i].newTraits['strength']);
      this.lineChartLabelsStrength.push(this.homeService.allEvent[i].age)
    }
    this.lineChartDataStrength=[{data:dummyData}];
    this.lineChartOptionsStrength = {
      elements: {
        line: {
            tension: 0 
        }
  },
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+ this.translate.instant('gameLabel.strength_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
  }
  generateEnduranceGraph(){
    let i=0;
    let dummyData=[]
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
      dummyData[i]=(this.homeService.allEvent[i].newTraits['physicalEndurance']);
      this.lineChartLabelsEndurance.push(this.homeService.allEvent[i].age)
    }
    this.lineChartDataEndurance=[{data:dummyData}];
    this.lineChartOptionsEndurance = {
      elements: {
        line: {
            tension: 0 
        }
  },
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+ this.translate.instant('gameLabel.endurance_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
  }
  
  generateSpiritualGraph(){
    let i=0;
    let dummyData=[]
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
       dummyData[i]=(this.homeService.allEvent[i].newTraits['spiritual']);
      this.lineChartLabelsSpiritual.push(this.homeService.allEvent[i].age)
    }
            this.lineChartDataSpiritual=[{data:dummyData}];

    this.lineChartOptionsSpiritual = {
      elements: {
        line: {
            tension: 0 
        }
  },
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
      plugins: {
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+ this.translate.instant('gameLabel.spiritual_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
      }
    };
  }
  generateWisdomGraph(){
    let i=0;
    let dummyData=[]
    for(i=0;i<this.homeService.allEvent.length;i++)
    {
      dummyData[i]=(this.homeService.allEvent[i].newTraits['wisdom']);
      this.lineChartLabelsWisdom.push(this.homeService.allEvent[i].age)
    }

           this.lineChartDataWisdom=[{data:dummyData}];
    this.lineChartOptionsWisdom = {
      responsive: true,
      scales: {
        yAxes: [{
                display: true,
                ticks: {
                    max: 100
                }
            }]
    },
       tooltip: {
        enabled: true,
         callbacks: {
          label: function(context) {
               var label =  '<span>'+this.translate.instant('gameLabel.wisdom_at_age') +context.parsed.x+ '</span>: <b>' + context.parsed.y + '</b><br/>';
            return label;
        }
         }}
    };
  }









  
change(){
  if(this.commonSuggetion!=null && this.commonSuggetion!='') {
    let str:string = this.commonSuggetion;
    this.commonSuggetion = str.trim();
  }
  if(this.healthSuggetion!=null && this.healthSuggetion!='') {
    let str1:string = this.healthSuggetion;
    this.healthSuggetion = str1.trim();
  }
  if(this.resistanceSuggetion!=null && this.resistanceSuggetion!='') {
    let str2:string = this.resistanceSuggetion;
    this.resistanceSuggetion = str2.trim();
  }
  if(this.happinessSuggetion!=null && this.happinessSuggetion!='') {
    let str3:string = this.happinessSuggetion;
    this.happinessSuggetion = str3.trim();
  }

  if(this.intelligenceSuggetion!=null && this.intelligenceSuggetion!='') {
    let str4:string = this.intelligenceSuggetion;
    this.intelligenceSuggetion = str4.trim();
  }

  if(this.artisticSuggetion!=null && this.artisticSuggetion!='') {
    let str5:string = this.artisticSuggetion;
    this.artisticSuggetion = str5.trim();
  }

  if(this.musicalSuggetion!=null && this.musicalSuggetion!='') {
    let str6:string = this.musicalSuggetion;
    this.musicalSuggetion = str6.trim();
  }

  if(this.atheleticSuggetion!=null && this.atheleticSuggetion!='') {
    let str7:string = this.atheleticSuggetion;
    this.atheleticSuggetion = str7.trim();
  }

  if(this.strengthSuggetion!=null && this.strengthSuggetion!='') {
    let str8:string = this.strengthSuggetion;
    this.strengthSuggetion = str8.trim();
  }

  if(this.enduranceSuggetion!=null && this.enduranceSuggetion!='') {
    let str9:string = this.enduranceSuggetion;
    this.enduranceSuggetion = str9.trim();
  }

  if(this.spiritualSuggetion!=null && this.spiritualSuggetion!='') {
    let str10:string = this.spiritualSuggetion;
    this.spiritualSuggetion = str10.trim();
  }
  if(this.wisdomSuggetion!=null && this.wisdomSuggetion!='') {
    let str11:string = this.wisdomSuggetion;
    this.wisdomSuggetion = str11.trim();
  }
  this.checkDisableCss();


}

  saveSuggetion(){
    let gameSummary = {
      'commonSuggestion': this.commonSuggetion,
      'healthSuggestion': this.healthSuggetion,
      'resistanceSuggestion': this.resistanceSuggetion,
      'happinessSuggestion':this.happinessSuggetion,
      'ntelligenceSuggestion':this.intelligenceSuggetion,
      'artisicSuggestion':this.artisticSuggetion,
      'musicalSuggestion':this.musicalSuggetion,
      'athleticSuggestion':this.atheleticSuggetion,
      'strengthSuggestion':this.strengthSuggetion,
      'enduranceSuggestion':this.enduranceSuggetion,
      'spiritualSuggestion':this.spiritualSuggetion,
      'wisdomSuggestion':this.wisdomSuggetion
    };
    this.postGameService.updateGameSummaryData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,gameSummary).subscribe(
      res=>{
        this.GameSummerySave.show();
      this.checkDisableCss();
      })
  }


  openOrganDonationCertificagte(){
    this.modalWindowShowHide.showOrganDonationCertificate=true;
    }

}
