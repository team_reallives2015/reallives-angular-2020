import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgeAYearComponent } from './age-a-year.component';

describe('AgeAYearComponent', () => {
  let component: AgeAYearComponent;
  let fixture: ComponentFixture<AgeAYearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgeAYearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeAYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
