import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { AgaAYearService } from './aga-a-year.service';
import { WellBeingIndicatorService } from '../well-being-indicator/well-being-indicator.service';
import { IdentityDifferenceFindService } from '../identity-difference-find.service';
import { CommonService } from 'src/app/shared/common.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { EventService } from '../event/event.service';
import { DecisionWindowComponent } from '../decision-window/decision-window.component';
import { TranslationService } from 'src/app/translation/translation.service';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';


@Component({
  selector: 'app-age-a-year',
  templateUrl: './age-a-year.component.html',
  styleUrls: ['./age-a-year.component.css']
})
export class AgeAYearComponent implements OnInit {

  allPerson;
  mainPerson;
  age;
  BlockerLeisureDecisionQuestion;
  BlockerDecisionQuestion;
  helpFriendAndGovermentBlocker;
  leisureBlockerShowFlag: boolean = false;
  financeBlockerFlag: boolean = false;
  decisionModalFriendGovermentHelpShow: boolean = false;
  decisionModalCheckInvesmentForFinanceBlockerFriendFlag: boolean = false;
  decisionModalCheckInvesmentForFinanceBlockerGovermentFlag: boolean = false;
  result;
  singleEvent;
  messageText;
  selectedAction = 1;
  ageAYearClickFlag: boolean = false;
  ageAYearDoneFlag: boolean = false;
  diiferenBetIdentity;
  identity = [];
  identityValues = [];
  successMsgFlag: boolean = false;
  helfFormFriendDecisionFlag: boolean = false;
  helfForGovernmentDecisionFlag: boolean = false;
  Actions;
  resultLength;
  msgTrue: boolean = false;
  constructor(public homeService: HomeService,
    public modalWindowShowHide: modalWindowShowHide,
    public constantService: ConstantService,
    public agaAYearService: AgaAYearService,
    public eventService: EventService,
    public wellBeingIndicatorService: WellBeingIndicatorService,
    public commonService: CommonService,
    public translation: TranslationService,
    public translate: TranslateService,
    public gameSummeryService: GameSummeryService,
    private identityDifferenceFindService: IdentityDifferenceFindService) { }
  @ViewChild(DecisionWindowComponent) decisionComponent: DecisionWindowComponent;
  @ViewChild('decisionLeisureModalBlocker') public decisionLeisureModalBlocker: ModalDirective;
  @ViewChild('decisionModalBlocker') public decisionModalBlocker: ModalDirective;


  @Input() data: boolean = false;
  ngOnInit(): void {
    this.allPerson = this.homeService.allPerson;
    this.mainPerson = this.homeService.allPerson[this.constantService.FAMILY_SELF];
    if(this.gameSummeryService.createLifenextEventFlag){
      this.ageAYearClickFlag=true;
    }
  }

  onAgeAYearClick() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age === 0 && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.successMsgFlag = true;
      let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
      let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
      this.messageText = s1 + s2;
    }
    else {
      this.ageAyearBlocker().then(res => {
        if (res == true) {
          this.ageAYearEventAndDecision();
        }
      })
    }
  }

  ageAYearEventAndDecision() {
    this.ageAYearClickFlag = true;
    this.modalWindowShowHide.showWaitScreen = true;
    this.agaAYearService.nextEvent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.hasOwnProperty('difference')) {
          if (Object.keys(this.result.data.result.difference).length !== 0) {
            this.diiferenBetIdentity = this.result.data.result.difference;
            let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
            if (retuenValue !== 'false') {
              this.homeService.allPerson = retuenValue;
              this.homeService.changeAllPerson.emit(this.homeService.allPerson);
              if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age === 0 && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
                this.successMsgFlag = true;
                let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
                let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
                this.messageText = s1 + s2;
              }
            }
          }
        }

        if (this.result.data.result.hasOwnProperty('lifeevent') && this.result.data.result.age_a_year) {
          this.ageAYearClickFlag = false
        }
        else if (!this.result.data.result.hasOwnProperty('lifeevent') && this.result.data.result.age_a_year) {
          this.ageAYearClickFlag = false
        }
        else if (!this.result.data.result.age_a_year) {
          this.ageAYearClickFlag = true;
        }
        if ((this.result.data.result.type === "event" || this.result.data.result.type === 'result') && this.result.data.result.lifeevent !== null) {
          this.agaAYearService.nextEventResultIsEvent(this.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
            this.homeService.cssForDisable = "disable";
            setTimeout(() => {
              // this.modalWindowShowHide.showObituary = true;
              // this.modalWindowShowHide.showGameSummary = false;
              // this.modalWindowShowHide.showEvent = false;
              this.homeService.cssForDisable = "";
            },
              3000);
          }
        }
        else if (this.result.data.result.type === "Decision" && this.result.data.result.lifeevent !== null) {
          this.agaAYearService.setDecision(this.result.data.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = true;
        }
        else {
          this.modalWindowShowHide.showWaitScreen = false;

        }
      })
  }

  nextEventButtonClick() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.agaAYearService.nextEvent(this.mainPerson.game_id).subscribe(
      res => {
        this.result = res;
        this.gameSummeryService.createLifenextEventFlag=false
        if (this.result.data.result.hasOwnProperty('difference')) {
          if (Object.keys(this.result.data.result.difference).length !== 0) {
            this.diiferenBetIdentity = this.result.data.result.difference;
            let returnValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
            if (returnValue !== 'false') {
              this.homeService.allPerson = returnValue;
              this.homeService.changeAllPerson.emit(this.homeService.allPerson);
              if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age === this.constantService.DEMOLIFEMAXAGE || this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
                this.successMsgFlag = true;
                let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
                let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
                this.messageText = s1 + s2;
              }
            }
          }
        }

        if (this.result.data.result.hasOwnProperty('lifeevent') && this.result.data.result.age_a_year) {
          this.ageAYearClickFlag = false
        }
        else if (!this.result.data.result.hasOwnProperty('lifeevent') && this.result.data.result.age_a_year) {
          this.ageAYearClickFlag = false
        }
        else if (!this.result.data.result.age_a_year) {
          this.ageAYearClickFlag = true;
        }
        if ((this.result.data.result.type === 'event' || this.result.data.result.type === 'result') && this.result.data.result.lifeevent !== null) {
          this.agaAYearService.nextEventResultIsEvent(this.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
          if (this.allPerson[this.constantService.FAMILY_SELF].dead) {
            this.homeService.cssForDisable = "disable";
            setTimeout(() => {
              // this.modalWindowShowHide.showObituary = true;
              // this.modalWindowShowHide.showGameSummary = false;
              // this.modalWindowShowHide.showEvent = false;
              this.homeService.cssForDisable = "";
            },
              3000);
          }
        }
        else if (this.result.data.result.type === 'Decision' && this.result.data.result.lifeevent !== null) {
          this.agaAYearService.setDecision(this.result.data.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = true;
        }
        else {
          this.modalWindowShowHide.showWaitScreen = false;
        }
      })
  }


  ageAyearBlocker(): Promise<any> {
    this.mainPerson = this.homeService.allPerson[this.constantService.FAMILY_SELF];
    this.age = this.homeService.allPerson[this.constantService.FAMILY_SELF].age;
    let income = 12 * parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome);
    let expense = 12 * parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses);
    let cash = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash) + income;
    if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
      this.Actions = [
        { "display": this.translate.instant('gameLabel.Select/Change_your_job/business'), "value": 1 },
        { "display": this.translate.instant('gameLabel.Help_from_friend'), "value": 5 },
        { "display": this.translate.instant('gameLabel.Help_from_Goverment'), "value": 6 },
        { "display": this.translate.instant('gameLabel.move_out'), "value": 7 }

      ];
    }
    else {
      this.Actions = [
        { "display": this.translate.instant('gameLabel.Select/Change_your_job/business'), "value": 1 },
        { "display": this.translate.instant('gameLabel.Change_your_expense'), "value": 2 },
        { "display": this.translate.instant('gameLabel.Liquidate_investment'), "value": 3 },
        { "display": this.translate.instant('gameLabel.Take_loan'), "value": 4 },
        { "display": this.translate.instant('gameLabel.Help_from_friend'), "value": 5 },
        { "display": this.translate.instant('gameLabel.Help_from_Goverment'), "value": 6 }
      ];
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age > 8) {
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].leisure_time_available < this.homeService.allPerson[this.constantService.FAMILY_SELF].current_selected_leisure) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].leisure_time_available <= 0) {
          this.BlockerLeisureDecisionQuestion = this.translate.instant('gameLabel.leisueWar');
        }
        else {
          this.BlockerLeisureDecisionQuestion = this.translate.instant('gameLabel.warr8') + " " + this.homeService.allPerson[this.constantService.FAMILY_SELF].leisure_time_available + " " + this.translate.instant('gameLabel.Leisure_activities');
        }
        this.decisionLeisureModalBlocker.show();
        return Promise.resolve(false);
      }
      else {
        if (15 <= this.homeService.allPerson[this.constantService.FAMILY_SELF].age) {
          let loanreq = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium);
          let expense1 = expense + loanreq;
          if (cash < expense1) {
            this.BlockerDecisionQuestion = this.translate.instant('gameLabel.ageBlocker');
            this.decisionModalBlocker.show();
            return Promise.resolve(false);
          }
          else {
            // this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium = 0;
            // this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + (income - expense1);
            return Promise.resolve(true);
          }
        }
        else if (11 <= this.homeService.allPerson[this.constantService.FAMILY_SELF].age && this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
          let loanreq = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium);
          let expense1 = expense + loanreq;
          if (cash < expense1) {
            this.BlockerDecisionQuestion = this.translate.instant('gameLabel.expWarr');
            this.decisionModalBlocker.show();
            return Promise.resolve(false);
          }
          else {
            // this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium = 0;
            // this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + ( income - expense1);
            return Promise.resolve(true);
          }
        }
        else {
          return Promise.resolve(true);
        }
      }
    }
    else {
      if (15 <= this.homeService.allPerson[this.constantService.FAMILY_SELF].age) {
        let loanreq = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium);
        let expense1 = expense + loanreq;
        if (cash < expense1) {
          this.BlockerDecisionQuestion = this.translate.instant('gameLabel.expWarr');
          this.decisionModalBlocker.show();
          return Promise.resolve(false);
        }
        else {
          // this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium = 0;
          // this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + ( income - expense1);
          return Promise.resolve(true);
        }
      }
      else if (11 <= this.homeService.allPerson[this.constantService.FAMILY_SELF].age && this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
        let loanreq = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium);
        let expense1 = expense + loanreq;
        if (cash < expense1) {
          this.BlockerDecisionQuestion = this.translate.instant('gameLabel.expWarr');
          this.decisionModalBlocker.show();
          return Promise.resolve(false);
        }
        else {
          // this.homeService.allPerson[this.constantService.FAMILY_SELF].required_loan_premium = 0;
          // this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash + ( income - expense1);
          return Promise.resolve(true);
        }
      }
      else {
        return Promise.resolve(true);
      }
    }
  }

  leisureBlockerYes() {
    this.modalWindowShowHide.showActionLeisure = true;
    this.decisionLeisureModalBlocker.hide();
  }

  blockerClickedOk(selectedAction) {
    this.decisionModalBlocker.hide();
    switch (selectedAction) {
      case 1:
        this.modalWindowShowHide.showActionCareer = true;
        break;
      case 2:
        this.modalWindowShowHide.showActionFinanace = true;
        break;
      case 3:
        this.modalWindowShowHide.showActionFinanace = true;
        break;
      case 4:
        this.modalWindowShowHide.showActionFinanace = true;
        break;
      case 7:
        this.modalWindowShowHide.showActionResidence = true;
        break;
      case 5:
        let statusValue = (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status);
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_flag) {
          this.messageText = this.translate.instant('gameLabel.warr1');
          this.msgTrue = true;
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdInvestmentValue > 0) {
          this.messageText = this.translate.instant('gameLabel.warr2');
          this.helfFormFriendDecisionFlag = true;
        }
        else
          if (statusValue >= 2) {
            if (!(this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_flag) && this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_count < 2) {
              this.modalWindowShowHide.showWaitScreen = true;
              this.agaAYearService.updatehelpData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                res => {
                  this.result = res
                  if (this.result.data.result.hasOwnProperty('difference')) {
                    if (Object.keys(this.result.data.result.difference).length !== 0) {
                      this.diiferenBetIdentity = this.result.data.result.difference;
                      let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
                      if (retuenValue !== 'false') {
                        this.homeService.allPerson = retuenValue;
                        this.homeService.changeAllPerson.emit(this.homeService.allPerson);
                      }
                    }
                  }
                  this.agaAYearService.nextEventResultIsEvent(this.result);
                  this.modalWindowShowHide.showWaitScreen = false;
                  this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
                  if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
                    this.homeService.cssForDisable = "disable";
                    setTimeout(() => {
                      this.modalWindowShowHide.showObituary = true;
                      this.modalWindowShowHide.showGameSummary = false;
                      this.modalWindowShowHide.showEvent = false;
                      this.homeService.cssForDisable = "";
                    },
                      3000);
                  }
                  this.modalWindowShowHide.showWaitScreen = false;
                })

            }
            else {
              this.messageText = this.translate.instant('gameLabel.warr3');
              this.msgTrue = true;
            }
          }
          else if (statusValue <= 1) {
            if (!(this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_flag) && this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_count < 4) {
              this.modalWindowShowHide.showWaitScreen = true;
              this.agaAYearService.updatehelpData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                res => {
                  this.result = res
                  if (this.result.data.result.hasOwnProperty('difference')) {
                    if (Object.keys(this.result.data.result.difference).length !== 0) {
                      this.diiferenBetIdentity = this.result.data.result.difference;
                      let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
                      if (retuenValue !== 'false') {
                        this.homeService.allPerson = retuenValue;
                        this.homeService.changeAllPerson.emit(this.homeService.allPerson);
                      }
                    }
                  }
                  this.agaAYearService.nextEventResultIsEvent(this.result);
                  this.modalWindowShowHide.showWaitScreen = false;
                  this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
                  if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
                    this.homeService.cssForDisable = "disable";
                    setTimeout(() => {
                      this.modalWindowShowHide.showObituary = true;
                      this.modalWindowShowHide.showGameSummary = false;
                      this.modalWindowShowHide.showEvent = false;
                      this.homeService.cssForDisable = "";
                    },
                      3000);
                  }
                  this.modalWindowShowHide.showWaitScreen = false;
                })

            }
            else {
              this.messageText = this.translate.instant('gameLabel.warr3');
              this.msgTrue = true;
            }
          }

        break;
      case 6:
        //Help from Goverment
        let statusValueForGov = (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status);
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government_flag) {
          this.messageText = this.translate.instant('gameLabel.warr4');
          this.msgTrue = true
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdInvestmentValue > 0) {
          this.messageText = this.translate.instant('gameLabel.warr5');
          this.helfForGovernmentDecisionFlag = true;
        }
        else
          if (statusValueForGov >= 2) {
            if (!(this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government_flag) && this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government < 3) {
              this.modalWindowShowHide.showWaitScreen = true;
              this.agaAYearService.updatehelpFromData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                res => {
                  this.result = res
                  if (this.result.data.result.hasOwnProperty('difference')) {
                    if (Object.keys(this.result.data.result.difference).length !== 0) {
                      this.diiferenBetIdentity = this.result.data.result.difference;
                      let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
                      if (retuenValue !== 'false') {
                        this.homeService.allPerson = retuenValue;
                        this.homeService.changeAllPerson.emit(this.homeService.allPerson);
                      }
                    }
                  }
                  this.agaAYearService.nextEventResultIsEvent(this.result);
                  this.modalWindowShowHide.showWaitScreen = false;
                  this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
                  if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
                    this.homeService.cssForDisable = "disable";
                    setTimeout(() => {
                      this.modalWindowShowHide.showObituary = true;
                      this.modalWindowShowHide.showGameSummary = false;
                      this.modalWindowShowHide.showEvent = false;
                      this.homeService.cssForDisable = "";
                    },
                      3000);
                  }
                  this.modalWindowShowHide.showWaitScreen = false;
                })
            }
            else {
              this.messageText = this.translate.instant('gameLabel.warr6');
              this.msgTrue = true;
            }
          }
          else if (statusValueForGov <= 1) {
            if (!(this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government_flag) && this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government < 5) {
              this.modalWindowShowHide.showWaitScreen = true;
              this.agaAYearService.updatehelpFromData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
                res => {
                  this.result = res
                  if (this.result.data.result.hasOwnProperty('difference')) {
                    if (Object.keys(this.result.data.result.difference).length !== 0) {
                      this.diiferenBetIdentity = this.result.data.result.difference;
                      let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
                      if (retuenValue !== 'false') {
                        this.homeService.allPerson = retuenValue;
                        this.homeService.changeAllPerson.emit(this.homeService.allPerson);
                      }
                    }
                  }
                  this.agaAYearService.nextEventResultIsEvent(this.result);
                  this.modalWindowShowHide.showWaitScreen = false;
                  this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
                  if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
                    this.homeService.cssForDisable = "disable";
                    setTimeout(() => {
                      this.modalWindowShowHide.showObituary = true;
                      this.modalWindowShowHide.showGameSummary = false;
                      this.modalWindowShowHide.showEvent = false;
                      this.homeService.cssForDisable = "";
                    },
                      3000);
                  }
                  this.modalWindowShowHide.showWaitScreen = false;
                })
            }
            else {
              this.messageText = this.translate.instant('gameLabel.warr6');
              this.msgTrue = true;
            }
          }
        break;

    }
  }


  TakeHelpFromFriend() {
    this.helfFormFriendDecisionFlag = false;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_flag) {
      this.messageText = this.translate.instant('gameLabel.warr1');
      this.msgTrue = true;
    }
    else if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_flag && this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_count <= 2) {
      this.modalWindowShowHide.showWaitScreen = true;
      this.agaAYearService.updatehelpData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
        res => {
          this.result = res
          if (this.result.data.result.hasOwnProperty('difference')) {
            if (Object.keys(this.result.data.result.difference).length !== 0) {
              this.diiferenBetIdentity = this.result.data.result.difference;
              let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
              if (retuenValue !== 'false') {
                this.homeService.allPerson = retuenValue;
                this.homeService.changeAllPerson.emit(this.homeService.allPerson);
              }
            }
          }
          this.agaAYearService.nextEventResultIsEvent(this.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
            this.homeService.cssForDisable = "disable";
            setTimeout(() => {
              this.modalWindowShowHide.showObituary = true;
              this.modalWindowShowHide.showGameSummary = false;
              this.modalWindowShowHide.showEvent = false;
              this.homeService.cssForDisable = "";
            },
              3000);
          }
          this.modalWindowShowHide.showWaitScreen = false;
        })
    }
    else {
      this.messageText = this.translate.instant('gameLabel.warr1');
      this.msgTrue = true
    }
  }

  TakeHelpFromGoverment() {
    this.helfForGovernmentDecisionFlag = false
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government_flag) {
      this.messageText = this.translate.instant('gameLabel.warr4');
      this.msgTrue = true;
    }
    else if (!(this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government_flag) && this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government <= 3) {
      this.modalWindowShowHide.showWaitScreen = true;
      this.agaAYearService.updatehelpFromData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
        res => {
          this.result = res
          if (this.result.data.result.hasOwnProperty('difference')) {
            if (Object.keys(this.result.data.result.difference).length !== 0) {
              this.diiferenBetIdentity = this.result.data.result.difference;
              let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
              if (retuenValue !== 'false') {
                this.homeService.allPerson = retuenValue;
                this.homeService.changeAllPerson.emit(this.homeService.allPerson);
              }
            }
          }
          this.agaAYearService.nextEventResultIsEvent(this.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
            this.homeService.cssForDisable = "disable";
            setTimeout(() => {
              this.modalWindowShowHide.showObituary = true;
              this.modalWindowShowHide.showGameSummary = false;
              this.modalWindowShowHide.showEvent = false;
              this.homeService.cssForDisable = "";
            },
              3000);
          }
          this.modalWindowShowHide.showWaitScreen = false;
        })
    }
    else {
      this.messageText = this.translate.instant('gameLabel.warr7');
      this.msgTrue = true;
    }
  }

  openFinceWindowAndClosePopUp() {
    this.modalWindowShowHide.showActionFinanace = true;
  }

  gotoObituary() {
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showObituary = true;
    this.modalWindowShowHide.showEvent = false;
    this.homeService.goToObituaryButton = false;
    this.homeService.activeLinkClasslifeExepectancy = 'active';
    this.homeService.activeLinkClassCountryCapsule = "";
    this.homeService.activeLinkClassLifeSummery = "";
    this.homeService.activeLinkClassSdgIndicator = "";
    this.homeService.activeLinkClassfinanceStatus = "";
    this.modalWindowShowHide.showLifeExpectancyGraph = true;
    this.modalWindowShowHide.showCountryCapsule = false;
    this.modalWindowShowHide.showFinanceStatus = false;
    this.modalWindowShowHide.showSdgIndicator = false;
    this.modalWindowShowHide.showLifeSummery = false;
  }

  seeYouLife() {
    this.homeService.goToObituaryButton = true;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showFeddback = false;
    this.modalWindowShowHide.showBugReport = false;
    this.modalWindowShowHide.showEvent = true;
    this.modalWindowShowHide.showLetter = false;
    this.homeService.activeLinkClasslifeExepectancy = '';
    this.homeService.activeLinkClassCountryCapsule = "";
    this.homeService.activeLinkClassLifeSummery = "";
    this.homeService.activeLinkClassSdgIndicator = "";
    this.homeService.activeLinkClassfinanceStatus = "active";
    this.modalWindowShowHide.showLifeExpectancyGraph = false;
    this.modalWindowShowHide.showCountryCapsule = false;
    this.modalWindowShowHide.showFinanceStatus = true;
    this.modalWindowShowHide.showSdgIndicator = false;
    this.modalWindowShowHide.showLifeSummery = false;

  }

  successMsgWindowClose() {
    this.successMsgFlag = false;
  }

  closeFriendHelp() {
    this.helfFormFriendDecisionFlag = false;
  }

  closeGovHelp() {
    this.helfForGovernmentDecisionFlag = false;
  }

  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }

  closeMsg() {
    this.msgTrue = false;
  }

}
