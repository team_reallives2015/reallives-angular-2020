import { TestBed } from '@angular/core/testing';

import { AgaAYearService } from './aga-a-year.service';

describe('AgaAYearService', () => {
  let service: AgaAYearService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgaAYearService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
