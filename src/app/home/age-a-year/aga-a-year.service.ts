import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { Event } from '../event/Event';
import { Decision } from '../decision-window/Decision'
import { IdentityDifferenceFindService } from '../identity-difference-find.service';
import { EventService } from '../event/event.service';
import { WellBeingIndicatorService } from '../well-being-indicator/well-being-indicator.service';
import { HomeService } from '../home.service';
import { ConstantService } from 'src/app/shared/constant.service';

@Injectable({
  providedIn: 'root'
})
export class AgaAYearService {

  identity = [];
  identityValues = [];
  singleEvent = new Event();
  decision = new Decision();
  goObituaryButton: boolean = false;
  constructor(private http: HttpClient,
    private homeService: HomeService,
    private commonService: CommonService,
    private identityDifferenceFindService: IdentityDifferenceFindService,
    public eventService: EventService,
    public wellBeingIndicatorService: WellBeingIndicatorService,
    public constantService: ConstantService) { }


  nextEvent(game_id) {
    return this.http.post<Array<Object>>(`${this.commonService.url}reallives/event/next`, { game_id });
  }

  updatehelpData(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/finance/helpFromFriend/${gameId}`);

  }

  updatehelpFromData(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/finance/helpFromGovernment/${gameId}`);


  }
  setEvent(response) {
    this.singleEvent = new Event();
    this.singleEvent.gameEventId = response.game_event_id;
    this.singleEvent.eventId = "";
    this.singleEvent.text = response.lifeevent;
    this.singleEvent.age = response.age;
    this.singleEvent.oldTraits = response.oldtraits;
    this.singleEvent.newTraits = response.newtraits;
    this.singleEvent.factroid = response.factoid;
    this.singleEvent.link = response.link;
    this.singleEvent.addline = response.addline;
    this.singleEvent.expression = "";
    this.singleEvent.type = response.type;
    this.singleEvent.enableAction = response.action;
    return this.singleEvent;
  }

  setDecision(response) {
    this.decision = new Decision();
    this.decision.gameEventId = response.game_event_id;
    this.decision.age = response.age;
    this.decision.decisionText = response.lifeevent;
    if (response.factoid == '') {
      this.decision.factroid = '';
    }
    else {
      this.decision.factroid = response.factoid;
    }
    if (response.hasOwnProperty('organDonationFlag')) {
      this.decision.organDonationFlag = response.organDonationFlag;
      this.decision.choiceArray = this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation
    }
    else {
      this.decision.choiceArray = response.choices;
      this.decision.bloodDonationFlag = false;

    }
    if (response.hasOwnProperty('fieldOfStudyList')) {
      this.decision.fieldOfStudyListFlag = response.fieldOfStudyList;
    }
    else {
      this.decision.fieldOfStudyListFlag = false;
    }
    if (response.hasOwnProperty('bloodDonationFlag')) {
      this.decision.bloodDonationFlag = response.bloodDonationFlag;
    }
    else {
      this.decision.bloodDonationFlag = false;
    }
    if (response.hasOwnProperty('petTypeFlag')) {
      this.decision.petFlag = response.petTypeFlag;

    }
    else {
      this.decision.petFlag = false;
    }
    if (response.hasOwnProperty('textBoxFlag')) {
      this.decision.textAreaFlag = response.textBoxFlag;
    }
    else {
      this.decision.textAreaFlag = false;
    }

    if (response.hasOwnProperty('businessFlag')) {
      this.decision.businessFlag = response.businessFlag;
      this.decision.loanFullfill = response.loanFullfillFlag
      this.decision.loanTypeArr = response.loanArr;

    }
    else {
      this.decision.businessFlag = false;
    }
    return this.decision;
  }

  replaceDifferenToIdentity(diiferenBetIdentity, allPerson) {
    if (JSON.stringify(diiferenBetIdentity) !== '{}') {
      this.identity = (Object.keys(diiferenBetIdentity));
      for (let i = 0; i < this.identity.length; i++) {
        allPerson[this.identity[i]] = this.identityDifferenceFindService.findDifferencOfIdentity(allPerson[this.identity[i]], diiferenBetIdentity[this.identity[i]])
      }
      let allPersonkeys = Object.keys(allPerson)
      for (let identity of allPersonkeys) {
        if (!identity.includes(this.constantService.SIBLINGS)) {
          if (!diiferenBetIdentity.hasOwnProperty(identity)) {
            if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
              && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
              this.homeService.setlang = allPerson.selected_language
              delete allPerson[identity]
            }
          }
        }
      }
      return allPerson;
    }
    else {
      return false;
    }
  }

  nextEventResultIsEvent(result) {
    this.setEvent(result.data.result);
    this.eventService.allEvents.push(this.singleEvent);
    this.homeService.allEvent = this.eventService.allEvents;
    this.eventService.updateCurrentEvent(this.eventService.allEvents.length - 1);
    this.wellBeingIndicatorService.generateWellBeingIndicator(this.eventService.currentEvent);
    if (this.eventService.state == 'nextevent') {
      this.eventService.state = 'nextevent2';
    }
    else {
      this.eventService.state = 'nextevent';
    }
  }
}
