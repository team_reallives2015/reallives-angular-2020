import { Component, Input, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { FinanceService } from '../action-finance/finance.service';
import { UtilityService } from '../utility-bar/utility.service';
import { FamilyCapsuleService } from './family-capsule.service';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';


@Component({
  selector: 'app-family-capsule',
  templateUrl: './family-capsule.component.html',
  styleUrls: ['./family-capsule.component.css']
})
export class FamilyCapsuleComponent implements OnInit {

  @Input() data: boolean = false;

  mainPerson;
  allPerson;
  selfName;
  selfAge;
  selfSex;
  selfIncome;
  countryIncome;
  countryIncome80;
  countryIncome120;
  status;
  oldStatus: String = "";
  statusValue;
  mainPersonClass = "";
  mother;
  motherString;
  father;
  fatherString;
  wife;
  wifeString;
  husband;
  husbandString;
  siblings;
  siblingString;
  sibString;
  sib;
  children;
  childrenString;
  childString;
  child;
  grandChildren;
  grandChildrenString;
  grandChildString;
  grandChild;
  livingAtHomeArr = [];
  livingAwayArr = [];
  petArr = [];
  gender;
  identity;
  image;
  width;
  myPet = [];
  residence;
  dietIndex;
  diet;
  subscribe;
  currentStatus: string = "";
  PriviousStatus: string = "";
  currentStatusValue;
  statusMetadata;
  deadClassForSelf;
  result = 2
  constructor(public homeService: HomeService,
    public commonService: CommonService,
    public constantService: ConstantService,
    public modalWindowShowHide: modalWindowShowHide,
    public financeService: FinanceService,
    public utilityService: UtilityService,
    public familyCapsuleService: FamilyCapsuleService,
    public translate: TranslateService) {
    this.subscribe = this.homeService.changeAllPerson.subscribe(
      persons => {
        this.familiCapsuleData(persons);
        this.familyCapsuleService.calcuateEconomicalStatusData();
      }
    )
  }

  ngOnInit(): void {
    this.familiCapsuleData(this.homeService.allPerson);
    this.familyCapsuleService.calcuateEconomicalStatusData();
  }

  familiCapsuleData(allPerson) {
    this.livingAtHomeArr = [];
    this.livingAwayArr = [];
    this.allPerson = allPerson;
    this.mainPerson = allPerson['SELF'];
    this.selfName = this.mainPerson.first_name;
    this.selfAge = this.mainPerson.age;
    this.selfSex = this.mainPerson.sex;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
      this.deadClassForSelf = "p_disable";
    }
    else {
      this.deadClassForSelf = "";
    }
    this.selfIncome = parseInt("" + (this.mainPerson.expense.householdIncome * 12) / this.mainPerson.country.ExchangeRate);
    this.countryIncome = (this.mainPerson.country.ProdperCapita);
    this.countryIncome80 = (this.countryIncome * this.constantService.COUNTRY_INCOME_80) / 100;
    this.countryIncome120 = (this.countryIncome * this.constantService.COUNTRY_INCOME_120) / 100;
    this.myPet = this.mainPerson.my_pets
    let shelterIndex = this.mainPerson.expense.shelter.shelterIndex;
    this.residence = ' ' + this.financeService.shelter[shelterIndex].name;
    this.dietIndex = this.mainPerson.expense.diet.dietIndex;
    this.diet = ' ' + this.financeService.diet[this.dietIndex].name;
    if (this.mainPerson.residence == "") {
      this.residence = " N/A"
    }
    if (this.mainPerson.diet == "") {
      this.diet = " N/A"
    }
    //code for status
    //code for mother
    if (this.allPerson[this.constantService.FAMILY_MOTHER] != null) {
      this.mother = this.allPerson[this.constantService.FAMILY_MOTHER];
      if (!this.mother.dead) {
        if (this.mother.living_at_home) {
          this.livingAtHomeArr.push({
            "id": this.mother.identity,
            "name": this.mother.first_name,
            "age": this.mother.age,
            "identity": this.translate.instant('gameLabel.Mother'),
            "sex": "Female",
            "image": "assets/images/game_images/girl.png",
            "width": "17",
            "job": this.mother.job.JobId,
            "education": this.mother.education,
            "business": this.mother.business,
            "country": this.mother.country.countryid
          });
        }
        else {

          this.livingAwayArr.push({
            "name": this.mother.first_name,
            "age": this.mother.age,
            "identity": this.translate.instant('gameLabel.Mother'),
            "sex": "Female",
            "image": "assets/images/game_images/girl.png",
            "width": "17",
            "class": ""
          });
        }
      } else {
        this.livingAwayArr.push({
          "name": this.mother.first_name,
          "age": this.mother.age,
          "identity": this.translate.instant('gameLabel.Mother'),
          "sex": "Female",
          "image": "assets/images/game_images/girl.png",
          "width": "17",
          "class": "p_disable"
        });
      }
    }

    //code for father
    if (this.allPerson[this.constantService.FAMILY_FATHER] != null) {
      this.father = this.allPerson[this.constantService.FAMILY_FATHER];
      if (!this.father.dead) {
        if (this.father.living_at_home) {
          this.livingAtHomeArr.push({
            "id": this.father.identity,
            "name": this.father.first_name,
            "age": this.father.age,
            "identity": this.translate.instant('gameLabel.Father'),
            "sex": "Male",
            "image": "assets/images/game_images/fam.png",
            "width": "15",
            "job": this.father.job.JobId,
            "education": this.father.education,
            "business": this.father.business,
            "country": this.father.country.countryid


          });
        }
        else {
          this.livingAwayArr.push({
            "name": this.father.first_name,
            "age": this.father.age,
            "identity": this.translate.instant('gameLabel.Father'),
            "sex": "Male",
            "image": "assets/images/game_images/fam.png",
            "width": "15",
            "class": "",

          });
        }
      } else {
        this.livingAwayArr.push({
          "name": this.father.first_name,
          "age": this.father.age,
          "identity": this.translate.instant('gameLabel.Father'),
          "sex": "Female",
          "image": "assets/images/game_images/fam.png",
          "width": "15",
          "class": "p_disable",
        });
      }


    }

    //code for sibling
    if (this.mainPerson[this.constantService.FAMILY_SIBLING] != null) {
      this.siblings = this.mainPerson[this.constantService.FAMILY_SIBLING];

      for (var i = 0; i < this.siblings.length; i++) {
        this.sibString = this.mainPerson.siblings[i];
        this.sib = this.allPerson[this.sibString];
        if (this.sib.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Sister');
          this.image = "assets/images/game_images/girl.png";
          this.width = "17";
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Brother');
          this.image = "assets/images/game_images/fam.png";
          this.width = "15";
        }
        if (!this.sib.dead) {
          if (this.sib.living_at_home) {
            this.livingAtHomeArr.push({
              "id": this.sib.identity,
              "name": this.sib.first_name,
              "age": this.sib.age,
              "identity": this.identity,
              "sex": this.gender,
              "image": this.image,
              "width": this.width,
              "job": this.sib.job.JobId,
              "education": this.sib.education,
              "business": this.sib.business,
              "country": this.sib.country.countryid

            });
          }

          else {
            this.livingAwayArr.push({
              "name": this.sib.first_name,
              "age": this.sib.age,
              "identity": this.identity,
              "sex": this.gender,
              "image": this.image,
              "width": this.width,
              "class": ""
            });
          }
        } else {
          this.livingAwayArr.push({
            "name": this.sib.first_name,
            "age": this.sib.age,
            "identity": this.identity,
            "sex": this.gender,
            "image": this.image,
            "width": this.width,
            "class": "p_disable"
          });
        }

      }
    }

    //code for children
    if (this.mainPerson[this.constantService.FAMILY_CHILDREN] != null) {
      this.children = this.mainPerson[this.constantService.FAMILY_CHILDREN];

      for (var i = 0; i < this.children.length; i++) {
        this.childString = this.mainPerson.children[i];
        this.child = this.allPerson[this.childString];
        if (this.child.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Daughter');
          this.image = "assets/images/game_images/girl.png";
          this.width = "17";
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Son');
          this.image = "assets/images/game_images/fam.png";
          this.width = "15";
        }
        if (!this.child.dead) {
          if (this.child.living_at_home) {
            this.livingAtHomeArr.push({
              "id": this.child.identity,
              "name": this.child.first_name,
              "age": this.child.age,
              "identity": this.identity,
              "sex": this.gender,
              "image": this.image,
              "width": this.width,
              "job": this.child.job.JobId,
              "education": this.child.education,
              "business": this.child.business,
              "country": this.child.country.countryid


            });
          }

          else {
            this.livingAwayArr.push({
              "name": this.child.first_name,
              "age": this.child.age,
              "identity": this.identity,
              "sex": this.gender,
              "image": this.image,
              "width": this.width,
              "class": ""
            });
          }
        } else {
          this.livingAwayArr.push({
            "name": this.child.first_name,
            "age": this.child.age,
            "identity": this.identity,
            "sex": this.gender,
            "image": this.image,
            "width": this.width,
            "class": "p_disable"
          });
        }

      }
    }

    // code for grandChildren
    if (this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN] != null) {
      this.grandChildren = this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN];

      for (var i = 0; i < this.grandChildren.length; i++) {
        this.grandChildString = this.mainPerson.grand_children[i];
        this.grandChild = this.allPerson[this.grandChildString];
        if (this.grandChild.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Grand_Daughter');
          this.image = "assets/images/girl.png";
          this.width = "17";
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Grand_Son');
          this.image = "assets/images/fam.png";
          this.width = "15";
        }
        if (!this.grandChild.dead) {
          if (this.grandChild.living_at_home) {
            this.livingAtHomeArr.push({
              "id": this.grandChild.identity,
              "name": this.grandChild.first_name,
              "age": this.grandChild.age,
              "identity": this.identity,
              "sex": this.gender,
              "image": this.image,
              "width": this.width,
              "job": this.grandChild.job.JobId,
              "education": this.grandChild.education,
              "business": this.grandChild.business,
              "country": this.grandChild.country.countryid


            });
          }

          else {
            this.livingAwayArr.push({
              "name": this.grandChild.first_name,
              "age": this.grandChild.age,
              "identity": this.identity,
              "sex": this.gender,
              "image": this.image,
              "width": this.width,
              "class": ""
            });
          }
        } else {
          this.livingAwayArr.push({
            "name": this.grandChild.first_name,
            "age": this.grandChild.age,
            "identity": this.identity,
            "sex": this.gender,
            "image": this.image,
            "width": this.width,
            "class": "p_disable"
          });
        }

      }
    }


    //code for wife
    if (this.allPerson[this.constantService.FAMILY_WIFE] != null) {
      this.wife = this.allPerson[this.constantService.FAMILY_WIFE];
      if (!this.wife.dead) {
        if (this.wife.living_at_home) {

          this.livingAtHomeArr.push({
            "id": this.wife.identity,
            "name": this.wife.first_name,
            "age": this.wife.age,
            "identity": this.translate.instant('gameLabel.Wife'),
            "sex": "Female",
            "image": "assets/images/game_images/girl.png",
            "width": "17",
            "job": this.wife.job.JobId,
            "education": this.wife.education,
            "business": this.wife.business,
            "country": this.wife.country.countryid

          });
        }
        else {

          this.livingAwayArr.push({
            "name": this.wife.first_name,
            "age": this.wife.age,
            "identity": this.translate.instant('gameLabel.Wife'),
            "sex": "Female",
            "image": "assets/images/game_images/girl.png",
            "width": "17",
            "class": ""
          });
        }
      } else {
        this.livingAwayArr.push({
          "name": this.wife.first_name,
          "age": this.wife.age,
          "identity": this.translate.instant('gameLabel.Wife'),
          "sex": "Female",
          "image": "assets/images/game_images/girl.png",
          "width": "17",
          "class": "p_disable"
        });
      }


    }

    //code for husband
    if (this.allPerson[this.constantService.FAMILY_HUSBAND] != null) {
      this.husband = this.allPerson[this.constantService.FAMILY_HUSBAND];
      if (!this.husband.dead) {
        if (this.husband.living_at_home) {

          this.livingAtHomeArr.push({
            "id": this.husband.identity,
            "name": this.husband.first_name,
            "age": this.husband.age,
            "identity": this.translate.instant('gameLabel.Husband'),
            "sex": "Male",
            "image": "assets/images/game_images/fam.png",
            "width": "17",
            "job": this.husband.job.JobId,
            "education": this.husband.education,
            "business": this.husband.business,
            "country": this.husband.country.countryid


          });
        }
        else {

          this.livingAwayArr.push({
            "name": this.husband.first_name,
            "age": this.husband.age,
            "identity": this.translate.instant('gameLabel.Husband'),
            "sex": "Male",
            "image": "assets/images/game_images/fam.png",
            "width": "17",
            "class": ""
          });
        }

      }
      else {
        this.livingAwayArr.push({
          "name": this.husband.first_name,
          "age": this.husband.age,
          "identity": this.translate.instant('gameLabel.Husband'),
          "sex": "Male",
          "image": "assets/images/game_images/fam.png",
          "width": "17",
          "class": "p_disable"
        });
      }
    }
    this.familyCapsuleService.livingAtHomeArray = this.livingAtHomeArr


  }
  showFamilyModeWindowClick() {
    this.modalWindowShowHide.showFamilyCapsuleWindow = true;
  }
  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }

}
