import { Injectable } from '@angular/core';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { UtilityService } from '../utility-bar/utility.service';

@Injectable({
  providedIn: 'root'
})
export class FamilyCapsuleService {
  status;
  statusValue;
  firstValue;
  secondValue;
  thirdValue;
  fourthValue;
  statusMetadata
  oldStatusValue;
  currentStatusValue;
  currentStatus;
  PriviousStatus
  chanceOfPoorB: number = 0;
  borderColourStyle = "border:6px solid #e30000;";
  livingAtHomeArray;
  constructor(public homeService: HomeService,
    public constantService: ConstantService,
    public commonService: CommonService,
    public utilityService: UtilityService,
    public gameSummeryService: GameSummeryService,
    public translate: TranslateService) { }
  calcuateEconomicalStatusData() {
    this.PriviousStatus = (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status);
    this.oldStatusValue = this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points;
    this.statusMetadata = this.gameSummeryService.ecoStatusData;
    this.chanceOfPoorB = this.gameSummeryService.ecoStatusData.chanceOfPoor;
    this.commonService.calculateWealthStatus(this.gameSummeryService.bornCountryWelathMetadata, this.homeService.allPerson);
    this.commonService.calculateIncameStatus(this.statusMetadata, this.homeService.allPerson);
    let wealth = (this.commonService.wealthStatus);
    let income = (this.commonService.incomeStatus);
    if (wealth > income) {
      this.commonService.statusValue = this.translate.instant('gameLabel.Wealth')
      this.status = this.commonService.wealthStatus;
      this.statusValue = this.commonService.wealthValue;
    }
    else if (wealth < income) {
      this.commonService.statusValue = this.translate.instant('gameLabel.Income1')
      this.status = this.commonService.incomeStatus;
      this.statusValue = this.commonService.incomeValue
    }
    else if (wealth === income) {
      this.status = this.commonService.incomeStatus;
      if (this.commonService.wealthValue > this.commonService.incomeValue) {
        this.statusValue = this.commonService.wealthValue
        this.commonService.statusValue = this.translate.instant('gameLabel.Wealth')
      }
      else if (this.commonService.wealthValue < this.commonService.incomeValue) {
        this.statusValue = this.commonService.incomeValue
        this.commonService.statusValue = this.translate.instant('gameLabel.Income1')
      }
      else {
        this.statusValue = this.commonService.incomeValue
        this.commonService.statusValue = this.translate.instant('gameLabel.Income1')

      }

    }
    //check status cheche or not
    if (this.status === this.PriviousStatus) {
      let statusObj = {
        'status': null,
        'newPoints': this.statusValue,
        'oldPoints': this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points
      }
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points !== this.statusValue) {
        this.homeService.saveFamilyStatus(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, statusObj, 2, null).subscribe(
          res => {
            if (res === true) {
              this.homeService.allPerson[this.constantService.FAMILY_SELF].old_economic_points = this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points;
              this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points = this.statusValue;
            }
          })
      }
    }
    else if (this.status < this.PriviousStatus) {
      if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses * 12) < this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash && Number.isInteger(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth)) {
        this.currentStatus = this.status;
        let statusObj = {
          'status': this.status,
          'newPoints': this.statusValue,
          'oldPoints': this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points
        }
        this.homeService.saveFamilyStatus(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, statusObj, 1, this.homeService.allPerson).subscribe(
          res => {
            this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status = this.currentStatus;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].old_economic_points = this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points = this.statusValue;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities = res['amenities']
            this.utilityService.createUtility(this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities);
            this.homeService.statusEmitter.emit(this.homeService.allPerson);
          })
        // }
      }
      else {
        this.currentStatus = this.status;
        let statusObj = {
          'status': this.currentStatus,
          'newPoints': this.statusValue,
          'oldPoints': this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points
        }
        this.homeService.saveFamilyStatus(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, statusObj, 1, this.homeService.allPerson).subscribe(
          res => {
            this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status = this.currentStatus;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].old_economic_points = this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points = this.statusValue;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities = res['amenities']
            this.utilityService.createUtility(this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities);
            this.homeService.statusEmitter.emit(this.homeService.allPerson);

          })
      }
    }
    else if (this.status > this.PriviousStatus) {
      this.currentStatus = this.status;

      let statusObj = {
        'status': this.currentStatus,
        'newPoints': this.statusValue,
        'oldPoints': this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points
      }
      this.homeService.saveFamilyStatus(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, statusObj, 1, this.homeService.allPerson).subscribe(
        res => {
          this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status = this.currentStatus;
          this.homeService.allPerson[this.constantService.FAMILY_SELF].old_economic_points = this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points;
          this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points = this.statusValue;
          this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities = res['amenities']
          this.utilityService.createUtility(this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities);
          this.homeService.statusEmitter.emit(this.homeService.allPerson);

        })

    }
    else {
      this.currentStatus = (this.status);
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status !== this.status) {
        let statusObj = {
          'status': this.currentStatus,
          'newPoints': this.statusValue,
          'oldPoints': this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points
        }
        this.homeService.saveFamilyStatus(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, statusObj, 1, this.homeService.allPerson).subscribe(
          res => {
            this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economical_status = this.currentStatus;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].old_economic_points = this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].current_economic_points = this.statusValue;
            this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities = res['amenities']
            this.utilityService.createUtility(this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities);
            this.homeService.statusEmitter.emit(this.homeService.allPerson);
          })
      }
    }
  }


}
