import { TestBed } from '@angular/core/testing';

import { FamilyCapsuleService } from './family-capsule.service';

describe('FamilyCapsuleService', () => {
  let service: FamilyCapsuleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FamilyCapsuleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
