import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyCapsuleComponent } from './family-capsule.component';

describe('FamilyCapsuleComponent', () => {
  let component: FamilyCapsuleComponent;
  let fixture: ComponentFixture<FamilyCapsuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FamilyCapsuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyCapsuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
