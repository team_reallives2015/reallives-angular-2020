import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfCapsuleComponent } from './self-capsule.component';

describe('SelfCapsuleComponent', () => {
  let component: SelfCapsuleComponent;
  let fixture: ComponentFixture<SelfCapsuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelfCapsuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfCapsuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
