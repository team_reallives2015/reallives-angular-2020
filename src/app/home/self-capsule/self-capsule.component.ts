import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { HomeService } from '../home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { FinanceService } from '../action-finance/finance.service';


@Component({
  selector: 'app-self-capsule',
  templateUrl: './self-capsule.component.html',
  styleUrls: ['./self-capsule.component.css']
})
export class SelfCapsuleComponent implements OnInit {
  
  allPerson;
  mainPerson;
  firstName:string;
  lastName:string;
  sex;
  age;
  countryName:string;
  languageName:string;
  religion:string;
  cityName:string;
  gameId;
  shelterIndex;
  residence:string;
  dietIndex;
  diet;
  title;
  cityCountry:string;
  nameTooltip:string;
regionName:string;
name;
subscribe;
urabanRural;
  constructor(public homeService : HomeService ,
              public constantService: ConstantService,
              public translate: TranslateService,
              public modalWindowShowHide :modalWindowShowHide,
              public financeService :FinanceService) { 
               this.subscribe=this.homeService.changeAllPerson.subscribe(persons =>{
                this.homeService.allPerson=persons;
                this.selfCapsuleData(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
              })
              }
 
  @Input() data:boolean=false;
  ngOnInit(): void {
  this.mainPerson=this.homeService.allPerson[this.constantService.FAMILY_SELF];
  this.selfCapsuleData(this.mainPerson);
  }

  selfCapsuleData(person){    
    this.age=person.age;
    this.sex=person.sex;
    this.religion=person.religion.religion;
    this.countryName=person.country.country;
    this.cityName=person.city.cityName;
    this.regionName=person.region.regionName;
    this.languageName=person.language.languagename;
    this.shelterIndex=person.expense.shelter.shelterIndex;
    this.residence=' ' + this.financeService.shelter[this.shelterIndex].name;
    this.dietIndex=person.expense.diet.dietIndex;
    this.diet =' ' + this.financeService.diet[this.dietIndex].name;
    this.title=person.title;
    if(person.urban_rural==="R"){
      this.urabanRural=this.translate.instant('gameLabel.Rural');
    }
    else{
      this.urabanRural=this.translate.instant('gameLabel.Urban');

    }
    if(person.sex==="M"){
      this.sex=this.translate.instant('gameLabel.Male');
    }
    else if(person.sex==="F"){
      this.sex=this.translate.instant('gameLabel.Female');
    }
    this.cityCountry= this.cityName+" ("+this.urabanRural+"), "+this.countryName;
    this.nameTooltip=this.title+""+this.name;
    if(person.residence=="" ){
       this.residence=" NA"
     }
    if(person.diet=="" ){
      this.diet=" NA"
    }



  }

  OrganAndBloodDonationPopUp(){
    this.modalWindowShowHide.showOrganBloodDonationFlag=true;
  }
  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }
}
