import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyCapsuleWindowComponent } from './family-capsule-window.component';

describe('FamilyCapsuleWindowComponent', () => {
  let component: FamilyCapsuleWindowComponent;
  let fixture: ComponentFixture<FamilyCapsuleWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FamilyCapsuleWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyCapsuleWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
