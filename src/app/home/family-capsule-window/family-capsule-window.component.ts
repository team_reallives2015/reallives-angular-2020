import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { NumberFormatePipe } from 'src/app/shared/number-formate.pipe';
import { FinanceService } from '../action-finance/finance.service';
import { FamilyCapsuleService } from '../family-capsule/family-capsule.service';
import { HomeService } from '../home.service';
import { UtilityService } from '../utility-bar/utility.service';



@Component({
  selector: 'app-family-capsule-window',
  templateUrl: './family-capsule-window.component.html',
  styleUrls: ['./family-capsule-window.component.css']
})
export class FamilyCapsuleWindowComponent implements OnInit {
  classForHelath = "";
  classForHealthStatus = "";
  allPerson;
  mainPerson;
  education;
  self;
  mainPersonName;
  mainPersonAge;
  mother;
  motherString;
  father;
  fatherString;
  wife;
  wifeString;
  husband;
  husbandString;
  siblings;
  siblingString;
  sibString;
  sib;
  children;
  childrenString;
  childString;
  child;
  grandChildren;
  grandChildrenString;
  grandChildString;
  grandChild;
  livingAtHomeArr;
  livingAwayArr = [];
  level1Arr = [];
  level2Arr = [];
  level3Arr = [];
  level4Arr = [];
  gender;
  identity;
  image;
  width;
  class;
  householdIncome;
  householdExpenses;
  householdNetWorth;
  language;
  religion;
  currencyName;
  diet;
  shelter;
  options;
  registerCountryExchangeRate;
  registerCountryCode;
  selfIncome: number;
  countryIncome;
  deadClass;
  currencyCode;
  salaryinregistercountry;
  expensesinRegisterCountry;
  netWorthinRegisterCountry;
  siblingPopupFlag:boolean=false;
  childrenPopupFlag:boolean=false;
  grandChildPopupFlag:boolean=false;
  multi: any[];
  view: any[] = [455, 110];

  // options
  maxLenght: number = 40;
  yTrimValue: boolean = true;
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = false;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Country';
  showYAxisLabel: boolean = true;
  xAxisLabel = 'Population';
  showDataLabel = true;
  moveOut = "";
  colorScheme = {
    domain: ['#567caa', '#567caa', '#567caa']
  };
  schemeType: string = 'linear';
  formatDataLabel(value) {
    return "$" + value;
  }
  jobName = '';




  constructor(public modalWindowShowHide: modalWindowShowHide,
    public utilityService: UtilityService,
    public homeService: HomeService,
    public constantService: ConstantService,
    public financeService: FinanceService,
    public commmonService: CommonService,
    private translate: TranslateService,
    public familyCapsuleService: FamilyCapsuleService,
    public commonService: CommonService,
    public numberPipe: NumberFormatePipe) { }


  ngOnInit(): void {
    this.showModelWindowData(this.homeService.allPerson);
    this.familyCapsuleService.calcuateEconomicalStatusData();
    this.checkSiblingPopuop();
    this.checkChildrenPopup();
    this.checkGrandchilderenPopup();
    this.multi = [
      {
        name: this.translate.instant('gameLabel.Family_Average_Income'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: Math.round((this.selfIncome))
          }
        ]
      },

      {
        name: this.translate.instant('gameLabel.Country_Average_Income'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: Math.round((this.countryIncome))
          }
        ]
      },
      {
        name: this.translate.instant('gameLabel.World_Average_Income'),
        series: [
          {
            name: this.translate.instant('gameLabel.Income_in_USD'),
            value: Math.round((12000))
          }
        ]
      },
    ];

  }

  onSelect(data): void {
    // console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }


  showModelWindowData(allperson) {
    this.allPerson = allperson;
    this.mainPerson = this.allPerson[this.constantService.FAMILY_SELF];
    this.mainPersonName = this.mainPerson.first_name;
    this.householdIncome = this.mainPerson.expense.householdIncome.toFixed(2);
    this.householdExpenses = this.mainPerson.expense.householdExpenses.toFixed(2);
    this.householdNetWorth = this.mainPerson.expense.householdNetWorth.toFixed(2);
    this.registerCountryExchangeRate = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.language = this.mainPerson.language.languageName;
    this.religion = this.mainPerson.religion.religionName;
    this.currencyName = this.mainPerson.country.currencyName;
    this.mainPersonAge = this.mainPerson.age;
    this.selfIncome = parseInt("" + (this.mainPerson.expense.householdIncome * 12) / this.mainPerson.country.ExchangeRate);
    this.countryIncome = (this.mainPerson.country.ProdperCapita * this.mainPerson.country.AverageFamilyCount);
    this.level1Arr = [];
    this.level2Arr = [];
    this.level3Arr = [];
    this.level4Arr = [];
    this.currencyCode = this.mainPerson.country.currencyCode;
    this.registerCountryCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural,
      this.salaryinregistercountry = this.mainPerson.expense.householdIncome / this.mainPerson.country.ExchangeRate;
    this.expensesinRegisterCountry = this.mainPerson.expense.householdExpenses / this.mainPerson.country.ExchangeRate;
    this.netWorthinRegisterCountry = this.mainPerson.expense.householdNetWorth / this.mainPerson.country.ExchangeRate;
    this.utilityService.createUtility(this.mainPerson.amenities);


    //code for father

    this.father = this.allPerson[this.constantService.FAMILY_FATHER];
    if (!this.father.dead) {
      this.deadClass = "";
    }
    else if (this.father.dead) {
      this.deadClass = "card_disable";
      if (this.father.traits.health < 35) {
        this.classForHelath = " border-bottom colourRed1"
      }
      else {
        this.classForHelath = "border-bottom";
      }
    }
    let healthProblemFather = "";
    if (this.father.health_disease != null && !this.father.dead) {
      let keys = Object.keys(this.father.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.father.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemFather == "") {
            healthProblemFather = healthProblemFather + this.father.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemFather = healthProblemFather + ", " + this.father.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemFather === "") {
        healthProblemFather = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }

    }
    else {
      healthProblemFather = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
    }
    if (this.father.living_at_home &&!this.father.dead) {
      this.moveOut = this.translate.instant('gameLabel.moved_out')
    }
    else {
      this.moveOut = "";
    }
    let fatherEdu = this.commonService.setEducationForAll(this.father);
    this.jobName = this.setJobOrBusinessnameForOther(this.father)
    this.level1Arr.push({
      "name": this.father.full_name,
      "education": fatherEdu,
      "age": this.father.age,
      "identity": this.translate.instant('gameLabel.Father'),
      "sex": "Male",
      "image": "assets/images/game_images/fam.png",
      "class": "fmly_member men_bg",
      "jobName": this.jobName,
      "income": this.father.income.toFixed(2),
      "registerIncome": ((this.father.income / this.homeService.allPerson[this.constantService.FAMILY_FATHER].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.father.traits.health,
      "deadClass": this.deadClass,
      "healthProblem": healthProblemFather,
      "currencyCode": this.father.country.CurrencyPlural,
      "currencyName": this.father.country.currency_code,
      "moveOut": this.moveOut,
      "healthClass": this.classForHelath,
      "healthClassForStatus": this.classForHealthStatus
    })



    //code for mother
    this.mother = this.allPerson[this.constantService.FAMILY_MOTHER];
    if (!this.mother.dead) {
      this.deadClass = "";
      if (this.mother.traits.health < 35) {
        this.classForHelath = " border-bottom colourRed1"
      }
      else {
        this.classForHelath = "border-bottom";
      }
    }
    else if (this.mother.dead) {
      this.deadClass = "card_disable";
    }

    let healthProblemMother = "";
    if (this.mother.health_disease != null && !this.mother.dead) {
      let keys = Object.keys(this.mother.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.mother.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemMother == "") {
            healthProblemMother = healthProblemMother + this.mother.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemMother = healthProblemMother + ", " + this.mother.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemMother === "") {
        healthProblemMother = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      healthProblemMother = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
    }
    if (this.mother.living_at_home &&!this.mother.dead) {
      this.moveOut = this.translate.instant('gameLabel.moved_out')
    }
    else {
      this.moveOut = "";
    }
    let motherEdu = this.commonService.setEducationForAll(this.mother);
    this.jobName = this.setJobOrBusinessnameForOther(this.mother)
    this.level1Arr.push({
      "name": this.mother.full_name,
      "age": this.mother.age,
      "education": motherEdu,
      "identity": this.translate.instant('gameLabel.Mother'),
      "sex": "Female",
      "image": "assets/images/game_images/girl.png",
      "class": "fmly_member female_bg",
      "jobName": this.jobName,
      "income": this.mother.income.toFixed(2),
      "registerIncome": ((this.mother.income / this.homeService.allPerson[this.constantService.FAMILY_MOTHER].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.mother.traits.health,
      "deadClass": this.deadClass,
      "healthProblem": healthProblemMother,
      "currencyCode": this.mother.country.CurrencyPlural,
      "currencyName": this.mother.country.currency_code,
      "moveOut": this.moveOut,
      "healthClass": this.classForHelath,
      "healthClassForStatus": this.classForHealthStatus

    })




    //code for self

    this.self = this.allPerson[this.constantService.FAMILY_SELF];
    if (!this.self.dead) {
      this.deadClass = "";
      if (this.self.traits.health < 35) {
        this.classForHelath = " border-bottom colourRed1"
      }
      else {
        this.classForHelath = "border-bottom";
      }
    }
    else if (this.self.dead) {
      this.deadClass = "card_disable";

    }

    if (this.self.sex == "F") {
      this.gender = "Female";
      this.class = "fmly_member female_bg";
      this.image = "assets/images/game_images/girl.png"
    }
    else {
      this.gender = "Male";
      this.class = "fmly_member men_bg";
      this.image = "assets/images/game_images/fam.png"
    }
    let jobName;
    if (this.mainPerson.job.JobName == this.constantService.JOB_NO_JOB) {
      jobName = "";

    } else {
      if (this.mainPerson.job.JobName == this.constantService.JOB_UNEMPLOYED
        || this.mainPerson.job.JobName == this.constantService.JOB_DOMESTIC_CHORES) {
        if (this.mainPerson.business.Business != this.constantService.BUSINESS_NO_BUSINESS) {
          jobName = this.mainPerson.business.Business;
        }
        else {
          jobName = this.mainPerson.job.Job_displayName;
        }
      }
      else {
        jobName = this.mainPerson.job.Job_displayName;
      }
    }

    let healthProblemSelf = "";
    if (this.self.health_disease != null && !this.self.dead) {
      let keys = Object.keys(this.self.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.self.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemSelf == "") {
            healthProblemSelf = healthProblemSelf + this.self.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemSelf = healthProblemSelf + ", " + this.self.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemSelf === "") {
        healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
    }
    if (this.self.head_of_household) {
      this.moveOut = this.translate.instant('gameLabel.Family_head')
    }
    else {
      this.moveOut = this.translate.instant('gameLabel.moved_out')

    }
    let selfEdu = this.commonService.setEducationForAll(this.self);
    this.level1Arr.push({
      "name": this.self.full_name,
      "age": this.self.age,
      "education": selfEdu,
      "identity": this.translate.instant('gameLabel.You'),
      "sex": this.gender,
      "image": this.image,
      "class": this.class,
      "jobName": jobName,
      "income": this.self.income.toFixed(2),
      "registerIncome": ((this.self.income / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.self.traits.health,
      "deadClass": this.deadClass,
      "healthProblem": healthProblemSelf,
      "currencyCode": this.self.country.CurrencyPlural,
      "currencyName": this.self.country.currency_code,
      "moveOut": this.moveOut,
      "healthClass": this.classForHelath,
      "healthClassForStatus": this.classForHealthStatus
    })

    //code for wife
    if (this.allPerson[this.constantService.FAMILY_WIFE] != null) {
      this.wife = this.allPerson[this.constantService.FAMILY_WIFE];
      if (this.wife.dead) {
        this.deadClass = "card_disable";
      }
      else {
        this.deadClass = "";
        if (this.wife.traits.health < 35) {
          this.classForHelath = " border-bottom colourRed1"
        }
        else {
          this.classForHelath = "border-bottom";
        }
      }
      let healthProblemWife = "";
      if (this.wife.healthDisease != null && !this.wife.dead) {
        let keys = Object.keys(this.wife.health_disease);
        let le = keys.length;
        for (let h = 0; h < keys.length; h++) {
          if (this.wife.health_disease[keys[h]].status === "GOTSICK") {
            if (healthProblemWife == "") {
              healthProblemWife = healthProblemWife + this.wife.health_disease[keys[h]].diseaseName;
            }
            else {
              healthProblemWife = healthProblemWife + ", " + this.wife.health_disease[keys[h]].diseaseName;
            }
          }
        }
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }
      else {
        healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
      }
      if (this.wife.living_at_home &&!this.wife.dead) {
        this.moveOut = this.translate.instant('gameLabel.moved_out')
      }
      else {
        this.moveOut = "";
      }
      let wifeEdu = this.commonService.setEducationForAll(this.wife);
      this.jobName = this.setJobOrBusinessnameForOther(this.wife)
      if (this.wife.sex == "F") {
        this.gender = "Female";
        this.class = "fmly_member female_bg";
        this.image = "assets/images/game_images/girl.png"
      }
      else {
        this.gender = "Male";
        this.class = "fmly_member men_bg";
        this.image = "assets/images/game_images/fam.png"
      }
      this.level2Arr.push({
        "name": this.wife.full_name,
        "age": this.wife.age,
        "education": wifeEdu,
        "identity": this.translate.instant('gameLabel.Wife'),
        "sex": this.gender,
        "image": this.image,
        "class": this.class,
        "jobName": this.jobName,
        "income": this.wife.income.toFixed(2),
        "registerIncome": ((this.wife.income / this.homeService.allPerson[this.constantService.FAMILY_WIFE].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
        "health": this.wife.traits.health,
        "deadClass": this.deadClass,
        "healthProblem": healthProblemWife,
        "currencyCode": this.wife.country.CurrencyPlural,
        "currencyName": this.wife.country.currency_code,
        "moveOut": this.moveOut,
        "healthClass": this.classForHelath,
        "healthClassForStatus": this.classForHealthStatus

      })


    }


    //code for husband
    if (this.allPerson[this.constantService.FAMILY_HUSBAND] != null) {
      this.husband = this.allPerson[this.constantService.FAMILY_HUSBAND];
      if (this.husband.dead) {
        this.deadClass = "card_disable";
      }
      else {
        this.deadClass = "";
        if (this.husband.traits.health < 35) {
          this.classForHelath = " border-bottom colourRed1"
        }
        else {
          this.classForHelath = "border-bottom";
        }
      }
      let healthProblemWife = "";
      if (this.husband.healthDisease != null && !this.husband.dead) {
        let keys = Object.keys(this.husband.health_disease);
        let le = keys.length;
        for (let h = 0; h < keys.length; h++) {
          if (this.husband.health_disease[keys[h]].status === "GOTSICK") {
            if (healthProblemWife == "") {
              healthProblemWife = healthProblemWife + this.husband.health_disease[keys[h]].diseaseName;
            }
            else {
              healthProblemWife = healthProblemWife + ", " + this.husband.health_disease[keys[h]].diseaseName;
            }
          }
        }
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');;
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }
      else {
        healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
      }
      if (this.husband.living_at_home &&!this.husband.dead) {
        this.moveOut = this.translate.instant('gameLabel.moved_out')
      }
      else {
        this.moveOut = "";
      }
      let husEducation = this.commonService.setEducationForAll(this.husband);
      this.jobName = this.setJobOrBusinessnameForOther(this.husband)
      if (this.husband.sex == "F") {
        this.gender = "Female";
        this.class = "fmly_member female_bg";
        this.image = "assets/images/game_images/girl.png"
      }
      else {
        this.gender = "Male";
        this.class = "fmly_member men_bg";
        this.image = "assets/images/game_images/fam.png"
      }
      this.level2Arr.push({
        "name": this.husband.full_name,
        "age": this.husband.age,
        "education": husEducation,
        "identity": this.translate.instant('gameLabel.Husband'),
        "sex": this.gender,
        "image": this.image,
        "class": this.class,
        "jobName": this.jobName,
        "income": this.husband.income.toFixed(2),
        "registerIncome": ((this.husband.income / this.homeService.allPerson[this.constantService.FAMILY_HUSBAND].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
        "health": this.husband.traits.health,
        "deadClass": this.deadClass,
        "healthProblem": healthProblemWife,
        "currencyCode": this.husband.country.CurrencyPlural,
        "currencyName": this.husband.country.currency_code,
        "moveOut": this.moveOut,
        "healthClass": this.classForHelath,
        "healthClassForStatus": this.classForHealthStatus

      })
    }

    //code for sibling
    if (this.mainPerson[this.constantService.FAMILY_SIBLING] != null) {
      this.siblings = this.mainPerson[this.constantService.FAMILY_SIBLING];

      for (var i = 0; i < this.siblings.length; i++) {
        this.sibString = this.mainPerson.siblings[i];
        this.sib = this.allPerson[this.sibString];
        if (this.sib.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
          if (this.sib.traits.health < 35) {
            this.classForHelath = " border-bottom colourRed1"
          }
          else {
            this.classForHelath = "border-bottom";
          }
        }
        if (this.sib.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Sister'),
            this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Brother'),
            this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.sib.health_disease != null && !this.sib.dead) {
          let keys = Object.keys(this.sib.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.sib.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.sib.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.sib.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
        }
        if (this.sib.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.sib.job.Job_displayName;

        }
        let name = '';
        if (this.sib.dead && this.sib.age === 0) {
          name = this.translate.instant('gameLabel.Stillborn');
        }
        else {
          name = this.sib.full_name
        }
        if (this.sib.living_at_home &&!this.sib.dead) {
          this.moveOut = this.translate.instant('gameLabel.moved_out')
        }
        else {
          this.moveOut = "";
        }
        let sibEdu = this.commonService.setEducationForAll(this.sib);
        this.jobName = this.setJobOrBusinessnameForOther(this.sib)
        this.level2Arr.push({
          "name": name,
          "age": this.sib.age,
          "education": sibEdu,
          "identity": this.identity,
          "sex": this.gender,
          "class": this.class,
          "image": this.image,
          "jobName": this.jobName,
          "income": this.sib.income.toFixed(2),
          "registerIncome": ((this.sib.income / this.homeService.allPerson[this.sibString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.sib.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.sib.country.CurrencyPlural,
          "currencyName": this.sib.country.currency_code,
          "moveOut": this.moveOut,
          "healthClass": this.classForHelath,
          "healthClassForStatus": this.classForHealthStatus
        })

      }
    }


    //code for children
    if (this.mainPerson[this.constantService.FAMILY_CHILDREN] != null) {
      this.children = this.mainPerson[this.constantService.FAMILY_CHILDREN];

      for (var i = 0; i < this.children.length; i++) {
        this.childString = this.mainPerson.children[i];
        this.child = this.allPerson[this.childString];
        if (this.child.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
          if (this.child.traits.health < 35) {
            this.classForHelath = " border-bottom colourRed1"
          }
          else {
            this.classForHelath = "border-bottom";
          }
        }
        if (this.child.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Daughter');
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Son');
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.child.health_disease != null && !this.child.dead) {
          let keys = Object.keys(this.child.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.child.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.child.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.child.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
        }
        if (this.child.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.child.job.Job_displayName;

        }
        let name = '';
        if (this.child.dead && this.child.age === 0) {
          name = this.translate.instant('gameLabel.Stillborn');
        }
        else {
          name = this.child.full_name
        }
        if (this.child.living_at_home &&!this.child.dead) {
          this.moveOut = this.translate.instant('gameLabel.moved_out')
        }
        else {
          this.moveOut = "";
        }
        let childEdu = this.commonService.setEducationForAll(this.child);
        this.jobName = this.setJobOrBusinessnameForOther(this.child)
        this.level3Arr.push({
          "name": name,
          "age": this.child.age,
          "education": childEdu,
          "identity": this.identity,
          "sex": this.gender,
          "class": this.class,
          "image": this.image,
          "jobName": this.jobName,
          "income": this.child.income.toFixed(2),
          "registerIncome": ((this.child.income / this.homeService.allPerson[this.childString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.child.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.child.country.CurrencyPlural,
          "currencyName": this.child.country.currency_code,
          "moveOut": this.moveOut,
          "healthClass": this.classForHelath,
          "healthClassForStatus": this.classForHealthStatus
        })
      }
    }


    //code for grandChildren
    if (this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN] != null) {
      this.grandChildren = this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN];

      for (var i = 0; i < this.grandChildren.length; i++) {
        this.grandChildString = this.mainPerson.grand_children[i];
        this.grandChild = this.allPerson[this.grandChildString];
        if (this.grandChild.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
          if (this.grandChild.traits.health < 35) {
            this.classForHelath = " border-bottom colourRed1"
          }
          else {
            this.classForHelath = "border-bottom";
          }
        }
        if (this.grandChild.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Grand_Daughter');
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Grand_Son');;
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.grandChild.health_disease != null && !this.grandChild.dead) {
          let keys = Object.keys(this.grandChild.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.grandChild.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.grandChild.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.grandChild.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
        }
        if (this.grandChild.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.grandChild.job.Job_displayName;

        }
        let name = '';
        if (this.grandChild.dead && this.grandChild.age === 0) {
          name = this.translate.instant('gameLabel.Stillborn');
        }
        else {
          name = this.grandChild.full_name
        }

        if (this.grandChild.living_at_home &&!this.grandChild.dead) {
          this.moveOut = (this.translate.instant('gameLabel.moved_out'))
        }
        else {
          this.moveOut = "";
        }
        let granEdu = this.commmonService.setEducationForAll(this.grandChild);
        this.jobName = this.setJobOrBusinessnameForOther(this.grandChild)
        this.level4Arr.push({
          "name": name,
          "age": this.grandChild.age,
          "education": granEdu,
          "identity": this.identity,
          "sex": this.gender,
          "class": this.class,
          "image": this.image,
          "jobName": this.jobName,
          "income": this.grandChild.income.toFixed(2),
          "registerIncome": ((this.grandChild.income / this.homeService.allPerson[this.grandChildString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.grandChild.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.grandChild.country.CurrencyPlural,
          "currencyName": this.grandChild.country.currency_code,
          "motherName": this.homeService.allPerson[this.grandChild.mother].full_name,
          "fatherName": this.homeService.allPerson[this.grandChild.father].full_name,
          "moveOut": this.moveOut,
          "healthClass": this.classForHelath,
          "healthClassForStatus": this.classForHealthStatus
        })
      }
    }
  }

  clickViewSiblingFamily() {
    this.modalWindowShowHide.showSiblingFamily = true;
  }


  clickViewChildrenFamily() {
    this.modalWindowShowHide.showChildrenFamily = true;
  }

  clickViewGrandChildrenFamily() {
    this.modalWindowShowHide.showGrandChildrenFamily = true;
  }

  closeWindow() {
    this.modalWindowShowHide.showFamilyCapsuleWindow = false;
  }

  setJobOrBusinessnameForOther(person) {
    this.jobName = '';
    if (person.business.hasOwnProperty('businessesId')) {
      this.jobName = person.business['Business'] + " business";
    }
    else {
      if (person.job.JobName == this.constantService.JOB_NO_JOB) {
        this.jobName = "";

      } else {
        this.jobName = person.job.Job_displayName;

      }
    }
    return this.jobName

  }

  checkSiblingPopuop(){
  let  sib=this.homeService.allPerson[this.constantService.FAMILY_SELF].siblings;
  if(sib.length>0){
    for(let i=0;i<sib.length;i++){
      if(this.allPerson[sib[i]].married || this.allPerson[sib[i]].children.length>0){
      this.siblingPopupFlag=true;
      }
    }  }
  }

  checkChildrenPopup(){
    let  child=this.homeService.allPerson[this.constantService.FAMILY_SELF].children;
    if(child.length>0){
      for(let i=0;i<child.length;i++){
        if(this.allPerson[child[i]].married || this.allPerson[child[i]].children.length>0){
        this.childrenPopupFlag=true;
        }
      }  
    }
  }
checkGrandchilderenPopup(){
  let  gChild=this.homeService.allPerson[this.constantService.FAMILY_SELF].grand_children;
  if(gChild.length>0){
    for(let i=0;i<gChild.length;i++){
      if(this.allPerson[gChild[i]].married || this.allPerson[gChild[i]].children.length>0){
      this.grandChildPopupFlag=true;
      }
    }  }
  }

}
