import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-business-tab',
  templateUrl: './business-tab.component.html',
  styleUrls: ['./business-tab.component.css']
})
export class BusinessTabComponent implements OnInit {
  businessType;
  businessRegister;
  totalEmp;
  totalMaleEmp = 0;
  totalFemaleEmp = 0
  FamMember = 0;
  subscribe;
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public homeService: HomeService,
    public translation: TranslateService,
    public commonService: CommonService,
    public constantService: ConstantService) {
    this.subscribe = this.homeService.changeAllPerson.subscribe(persons => {
      this.homeService.allPerson = persons;
      this.setBusinessValue();
    })
  }

  ngOnInit(): void {
    this.modalWindowShowHide.showBusinessTabFlag = true
    this.setBusinessValue();
  }


  setBusinessValue() {
    this.getBusinessType(this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessType);
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.isRegistered) {
      this.businessRegister = this.translation.instant('gameLabel.Yes');
    }
    else {
      this.businessRegister = this.translation.instant('gameLabel.No');
    }

    this.totalEmp = this.homeService.allPerson[this.constantService.FAMILY_SELF].business['skilledEmp'].totalSkilled +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['semiSkilledEmp'].totalSemiskilled +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['unSkilledEmp'].totalUnskilled


    this.totalMaleEmp = this.homeService.allPerson[this.constantService.FAMILY_SELF].business['skilledEmp'].male +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['semiSkilledEmp'].male +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['unSkilledEmp'].male

    this.totalFemaleEmp = this.homeService.allPerson[this.constantService.FAMILY_SELF].business['skilledEmp'].female +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['semiSkilledEmp'].female +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['unSkilledEmp'].female

    this.FamMember = this.homeService.allPerson[this.constantService.FAMILY_SELF].business.famMember.length
  }



  getBusinessType(typeId) {
    if (typeId === 1) {
      this.businessType = this.translation.instant('gameLabel.Micro')
    }
    else if (typeId === 2) {
      this.businessType = this.translation.instant('gameLabel.Small')

    }
    else if (typeId === 3) {
      this.businessType = this.translation.instant('gameLabel.Medium')

    }
    else if (typeId === 4) {
      this.businessType = this.translation.instant('gameLabel.Large')

    }

  }
  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }


}
