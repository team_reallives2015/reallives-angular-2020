import { object } from '@amcharts/amcharts4/core';
import { Injectable } from '@angular/core';
import { ConstantService } from '../shared/constant.service';
import { AgaAYearService } from './age-a-year/aga-a-year.service';
import { EventService } from './event/event.service';
import { HomeService } from './home.service';
import { IdentityDifferenceFindService } from './identity-difference-find.service';

@Injectable({
  providedIn: 'root'
})
export class CommonActionService {
  diiferenBetIdentity;
  identity = [];
  identityValues = [];
  constructor(private homeService: HomeService,
    private agaAYearService: AgaAYearService,
    private eventService: EventService,
    private diiferenService: IdentityDifferenceFindService,
    public constantService: ConstantService) { }

  getActionEventAndIdentityDifference(result, allPerson) {
    this.diiferenBetIdentity = result.data.result.difference;
    //get Differen and merge
    let returnValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, allPerson);
    if (returnValue !== 'false') {
      this.homeService.allPerson = returnValue;
      let allPersonkeys = Object.keys(this.homeService.allPerson)
      for (let identity of allPersonkeys) {
        if (!this.diiferenBetIdentity.hasOwnProperty(identity)) {
          if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
            && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
            this.homeService.setlang = allPerson.selected_language
            delete this.homeService.allPerson[identity]
          }
        }
      }
      //emit change allPerson
      this.homeService.changeAllPerson.emit(this.homeService.allPerson);
    }
    //set event
    if (result.data.result.lifeevent !== null) {
      this.agaAYearService.nextEventResultIsEvent(result);
      this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
    }
  }


  getCreateLifeEvent(result) {
    //set event
    this.agaAYearService.nextEventResultIsEvent(result);
    this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);

  }




  getActionIdentityDiffrence(result, allPerson) {
    this.diiferenBetIdentity = result.data.result.difference;
    let returnValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, allPerson);
    if (returnValue !== 'false') {
      this.homeService.allPerson = returnValue;
      let allPersonkeys = Object.keys(this.homeService.allPerson)
      for (let identity of allPersonkeys) {
        if (!identity.includes(this.constantService.SIBLINGS)) {
          if (!this.diiferenBetIdentity.hasOwnProperty(identity)) {
            if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
              && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
              this.homeService.setlang = allPerson.selected_language
              delete this.homeService.allPerson[identity]
            }
          }
        }
      }
      //emit change allPerson
      this.homeService.changeAllPerson.emit(this.homeService.allPerson);
    }
  }


}
