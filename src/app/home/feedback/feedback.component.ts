import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { PostGameService } from '../post-game.service';
import { FeedBack } from './feedback';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
feedback=new FeedBack();
disablebuttonAfterOnClick: string;
  showMessageForSuggestFriend: boolean = false;
  addCssForDisable = "";
  suggestionDisabledFlag: boolean = false;
  disableSubmitFlag;
  upload_number = 2;
  private maxRateValue: number = 5;
  constructor(public modalWindowShowHide :modalWindowShowHide,
             public homeService:HomeService,
             public PostGameService :PostGameService,
             public constantService:ConstantService) { }
             @ViewChild('decisionforConfirm') public decisionforConfirm: ModalDirective;

  ngOnInit(): void {
    this.modalWindowShowHide.showFeddback=true;
    this.setInitialValues();
    this.PostGameService.getFeedBackData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res=>{
        if(res!=={}){
                this.feedback.sugesstionDislike = res.feedBack.dislikeSuggestion;
                this.feedback.suggestiontDesign = res.feedBack.suggestionDesign;
                this.feedback.suggestToOther = res.feedBack.suggestToOther;
                this.feedback.suggestionInsight = res.feedBack.insightsSuggestion;
                this.feedback.favourite = res.feedBack.favouritePart;
                this.feedback.fequency = res.feedBack.frequencySuggestion;
               this.feedback.suggestContentChange = res.feedBack.contentChangeSuggestion;
              this.feedback.suggestDesignChange = res.feedBack.uiUxSuggestion;
              this.feedback.emails = res.feedBack.suggestionEmails;
              }
              else{
                this.feedback.sugesstionDislike = "";
                this.feedback.suggestiontDesign = "";
                this.feedback.suggestToOther = "";
                this.feedback.suggestionInsight = "";
                this.feedback.favourite = "";
                this.feedback.fequency = "";
               this.feedback.suggestContentChange = "";
              this.feedback.suggestDesignChange = "";
              this.feedback.emails = "";
              }
      }
    )
  
  }

  setInitialValues(){
    this.feedback.rating = 0;
    this.disableSubmitFlag = true;
    this.addCssForDisable = "disable";
    this.suggestionDisabledFlag = true;
    this.feedback.contentRating = 0;
    this.feedback.suggestionInsight = "";
    this.feedback.suggestiontDesign = "";
    this.feedback.suggestToOther = "";
    this.feedback.sugesstionDislike = "";
    this.feedback.favourite = "";
    this.feedback.fequency = "";
    this.feedback.suggestContentChange = "";
    this.feedback.suggestDesignChange = "";
    this.feedback.emails = "";
  }

  checkvalid(email: any) {
    if (!(email.valid)) {
      this.disableSubmitFlag = true;
      this.addCssForDisable = "disable";
    }
    return email.valid;
  }

  ngDoCheck(): void {

    if (this.feedback.suggestToOther === "yes") {
      if (this.feedback.emails === "" || !(this.feedback.emails.replace(/\s/g, '').length)) {
        this.showMessageForSuggestFriend = true;
      } else {
        this.showMessageForSuggestFriend = false;
      }
    } else {
      this.showMessageForSuggestFriend = false;
    }
    if ((this.feedback.contentRating === 0
        && this.feedback.rating === 0
        && this.feedback.sugesstionDislike === ""
        && this.feedback.fequency === ""
        && this.feedback.suggestionInsight === ""
        && this.feedback.suggestiontDesign === ""
        && this.feedback.favourite === "" ) &&
      (this.feedback.suggestDesignChange === ""
        && this.feedback.suggestContentChange === ""
        && this.feedback.emails === "")) {
      this.disableSubmitFlag = true;
      this.addCssForDisable = "disable";
    }
    else {
      this.disableSubmitFlag = false;
      this.addCssForDisable = "";
    }
   // Only space validation
    if (!(this.feedback.suggestiontDesign.replace(/\s/g, '').length)
      && !(this.feedback.favourite.replace(/\s/g, '').length)
      && !(this.feedback.suggestionInsight.replace(/\s/g, '').length)
      && !(this.feedback.sugesstionDislike.replace(/\s/g, '').length)
      && !(this.feedback.fequency.replace(/\s/g, '').length)
      && !(this.feedback.suggestDesignChange.replace(/\s/g, '').length)
      && !(this.feedback.suggestContentChange.replace(/\s/g, '').length)
      && !(this.feedback.emails.replace(/\s/g, '').length)) {
      this.disableSubmitFlag = true;
      this.addCssForDisable = "disable";
    }
    else {
      this.disableSubmitFlag = false;
      this.addCssForDisable = "";
    }
    if (this.feedback.suggestToOther === "no") {
      this.feedback.emails = "";
    }


  }
  onChange(Value: string) {

    this.feedback.fequency = Value;

  }
  saveFeedBack() {
    this.disablebuttonAfterOnClick = 'disable';
    this.feedback.userName = this.homeService.allPerson[this.constantService.FAMILY_SELF].userName;
    this.feedback.country = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
     let feedBack={
       "feedBack":{ "dislikeSuggestion":this.feedback.sugesstionDislike,
       "suggestionDesign":this.feedback.suggestiontDesign ,
       "suggestToOther":this.feedback.suggestToOther ,
       "insightsSuggestion":this.feedback.suggestionInsight ,
      "favouritePart": this.feedback.favourite ,
       "frequencySuggestion":this.feedback.fequency ,
     "contentChangeSuggestion": this.feedback.suggestContentChange,
     "uiUxSuggestion":this.feedback.suggestDesignChange,
    "suggestionEmails":this.feedback.emails},
       "counrty":this.feedback.country,
       "username":this.feedback.userName
     }
    this.PostGameService.updateFeedBackData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,feedBack).subscribe(
      res => {
        this.decisionforConfirm.show();
        this.disableSubmitFlag = true;
        this.addCssForDisable = 'disable';
        this.disablebuttonAfterOnClick = '';
      }
    );
  }
}
