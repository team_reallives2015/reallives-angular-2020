import { HttpClient } from '@angular/common/http';
import { EventEmitter } from '@angular/core';
import { Injectable, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from '../shared/constant.service';
import { FinanceService } from './action-finance/finance.service';
import { AgaAYearService } from './age-a-year/aga-a-year.service';
import { EventService } from './event/event.service';
import { PostGameService } from './post-game.service';
import { GameSummeryService } from '../game-summery/game-summery.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  flagFromDeadScreens: boolean = false
  capturedImage;
  parameter;
  eventId;
  gameId;
  addCssForDisable: boolean = false;
  gender;
  obituaryText;
  headingText: String;
  status;
  education = "";
  residence;
  carrier;
  family;
  leisure;
  disease;
  vices;
  diet;
  charity;
  loanInvestments;
  amenities;
  pdfFlag: boolean = false;
  languageReligionString;
  obituaryWritenByMe;
  newSuggetion;
  diedEventAddline;
  organDonation;
  bloodDonation;
  full_name;
  organDonationArray = [];
  notShowDiv: boolean = false;
  saveObituary;
  str = "";
  str1 = "";
  str2 = "";
  str3 = "";
  str4 = "";
  str5 = "";
  str6 = "";
  str7 = "";
  str8 = "";
  str9 = "";
  str10 = "";
  str11 = "";
  str12 = "";
  str13 = "";
  str14 = "";
  str15 = "";
  str16 = "";
  str17 = "";
  str18 = "";
  str19 = "";
  str20 = "";
  str21 = "";
  str22 = "";
  str23 = "";
  str24 = ""
  str25 = "";
  str26 = "";
  str27 = "";
  str28 = "";
  str29 = "";
  str30 = "";
  str31 = "";
  str32 = "";
  str33 = "";
  str34 = "";
  str35 = "";
  str36 = "";
  str37 = "";
  str38 = "";
  allPerson;
  mainPerson;
  sveObituaryObject = {};
  messageText;
  messageType;
  messageClass = "alert-info";
  MessageShow;
  successMsgFlag;
  emailId;
  friendMailShowFlag: boolean = false;
  deadShelter;
  bornShelter;
  deadDiet;
  BornDiet;
  dietString;
  shelterString;
  flagForFirstJob: boolean = false;
  letterData;

  allEvent;
  firstName;
  lastName;
  clicktoGameSummary: boolean = false;
  goToObituaryButton: boolean = false;
  activeLinkClassCountryCapsule = "";
  activeLinkClassLifeSummery = "";
  activeLinkClassSdgIndicator = "";
  activeLinkClasslifeExepectancy = "";
  activeLinkClassfinanceStatus = "";
  actionLinkClassBusinessTab = ""
  @Output() changeSelfPerson = new EventEmitter();
  @Output() changeAllPerson = new EventEmitter();
  @Output() deadEmitter = new EventEmitter();
  @Output() statusEmitter = new EventEmitter();

  cssForDisable = "";
  sdgId: number = 0;
  setlang;
  displaySdgFlag: boolean = false;
  constructor(private http: HttpClient,
    public finanaceService: FinanceService,
    public translate: TranslateService,
    private commonService: CommonService,
    public postGameService: PostGameService,
    private constantService: ConstantService,
    public gameSummeryService:GameSummeryService) { }


  getPersonFromId(id) {
    return this.http.get<Object>(`${this.commonService.url}game/getperson/${id}`);
  }

  getAllEventList(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}gameEvent/getEvent/${gameId}`);
  }

  saveFamilyStatus(gameId: any, status: any, saveStatus, allPerson) {
    return this.http.post<boolean>(`${this.commonService.url}game/updateSocialEconomicalStatus/${gameId}/${saveStatus}`, { status, allPerson });
  }
  geteconomicalStatusMetadata(countryId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getEconomicStatusData/${countryId}`);
  }

  public cretaeObituary(allPerson) {
    let disease, livingWith = "", realtionString = "", education;
    // let bornEvent = this.allEvent[0].text;
    this.allPerson = allPerson;
    this.mainPerson = allPerson[this.constantService.FAMILY_SELF];
    this.allEvent = this.allEvent
    this.diedEventAddline = this.allEvent[this.allEvent.length - 1].text;
    this.diedEventAddline = this.diedEventAddline.toLowerCase();
    //get event Text
    if (this.mainPerson.adoption_flag) {
      this.str26 = this.translate.instant('gameLabel.obituary1');
      //  this.str26=" You faced a unique set of circumstances during the course of your life, which led you to give your child up for adoption."
    }


    if (this.mainPerson.abortion_age.length > 0) {
      let aboage = this.mainPerson.abortion_age[0];
      if (this.mainPerson.abortion_age.length === 1) {
        if (this.mainPerson.sex === "M") {
          this.translate.get('gameLabel.obituary98', { aboage: aboage }).subscribe(
            (str) => {
              this.str27 = str;
            })
        }
        else {
          this.translate.get('gameLabel.obituary2', { aboage: aboage }).subscribe(
            (str) => {
              this.str27 = str;
            })
        }

      }
      else {
        if (this.mainPerson.sex === "M") {
          this.translate.get('gameLabel.obituary99', { aboage: aboage }).subscribe(
            (str) => {
              this.str27 = str;
            })
        }
        else {
          this.translate.get('gameLabel.obituary97', { aboage: aboage }).subscribe(
            (str) => {
              this.str27 = str;
            })
        }
      }

      // this.str27=" You also took the decision to have an abortion at the age of " +aboage+"."
    }
    if (this.mainPerson.rape_age > 0) {
      let rapeAge = this.mainPerson.rape_Age;
      this.translate.get('gameLabel.obituary3', { rapeAge: rapeAge }).subscribe(
        (str) => {
          this.str28 = str;
        })
      // this.str28=" Sadly, you went through a horrible experience of being raped at the age of " +rapeAge+"."
    };

    //decide a or an
    let boenan, localan;
    if (this.mainPerson.born_diet_index < 6 && this.mainPerson.born_diet_index > 3) {
      boenan = this.translate.instant('gameLabel.an');
    }
    else {
      boenan = this.translate.instant('gameLabel.a');
    }


    if (this.mainPerson.expense.diet.dietIndex < 6 && this.mainPerson.expense.diet.dietIndex > 3) {
      localan = this.translate.instant('gameLabel.an');
    }
    else {
      localan = this.translate.instant('gameLabel.a');
    }

    //get diseaces List
    let healthProblemSelf = "";
    if (this.mainPerson.health_disease != null) {
      let keys = Object.keys(this.mainPerson.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.mainPerson.health_disease[keys[h]].status === "GOTSICK" || this.mainPerson.health_disease[keys[h]].status === "CURED") {
          if (healthProblemSelf == "") {
            healthProblemSelf = healthProblemSelf + this.mainPerson.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemSelf = healthProblemSelf + ", " + this.mainPerson.health_disease[keys[h]].diseaseName;
          }
        }
      }
      //todo add and
      if (healthProblemSelf !== "") {
        healthProblemSelf = healthProblemSelf.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
        this.translate.get('gameLabel.obituary4', { healthProblemSelf: healthProblemSelf }).subscribe(
          (str) => {
            this.str21 = str;
          })
        // this.str21=" Through the course of your lifetime, you suffered from "+healthProblemSelf+"."; 
      }

    }
    this.diedEventAddline = this.diedEventAddline.replace(/.\s*$/, "");
    let matchPattern = this.diedEventAddline.match(/\d+/g);
    if (matchPattern !== null) {
      this.str1 = this.mainPerson.full_name + ", " + this.diedEventAddline + this.translate.instant('gameLabel.in') + this.mainPerson.city.cityName + ", " + this.mainPerson.country.country + ". ";

    }
    else {
      this.translate.get('gameLabel.obituary5', { fullName: this.mainPerson.full_name, diedEventAddline: this.diedEventAddline, age: this.mainPerson.age, cityName: this.mainPerson.city.cityName, country: this.mainPerson.country.country }).subscribe(
        (str) => {
          this.str1 = str;
        })
      // this.str1=this.mainPerson.full_name +", "+this.diedEventAddline +" at the age of "+this.mainPerson.age+" in "+this.mainPerson.city.cityName+", "+this.mainPerson.country.country+". ";
    }
    if (this.mainPerson.age === 0) {
      this.str1 = this.mainPerson.full_name + ", " + this.diedEventAddline + this.translate.instant('gameLabel.in') + this.mainPerson.city.cityName + ", " + this.mainPerson.country.country + ". ";
    }
    //organ donation
    let organList = ""; let organString = "";
    if (this.mainPerson.age > 19) {
      for (let i = 0; i < this.mainPerson.organ_donation.length; i++) {
        if (this.mainPerson.organ_donation[i].organStatus) {
          if (organList === "") {
            organList = organList = organList + this.mainPerson.organ_donation[i].organName;
          }
          else {
            organList = organList + ", " + this.mainPerson.organ_donation[i].organName;
          }
        }
      }
      if (organList !== "") {
        organList = organList.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
        this.translate.get('gameLabel.obituary6', { organList: organList }).subscribe(
          (str) => {
            organString = str;
          })
        // organString="You have donated your " +organList+ " to help the needy in the society."
      }
      else {
        organString = this.translate.instant('gameLabel.obituary7');
        // organString="You never donated any vital organs that could have enabled someone to live a good life."
      }
    }
    //family
    if (!this.allPerson[this.mainPerson.mother].dead) {
      realtionString = this.translate.instant('gameLabel.mother') + " " + this.allPerson[this.mainPerson.mother].first_name;
    }
    if (!this.allPerson[this.mainPerson.father].dead) {
      if (realtionString !== "") {
        realtionString = realtionString + ", " + this.translate.instant('gameLabel.father') + " " + this.allPerson[this.mainPerson.father].first_name;
      }
      else {
        realtionString = realtionString + this.translate.instant('gameLabel.father') + " " + this.allPerson[this.mainPerson.father].first_name;
      }
    }

    if (this.mainPerson.wife !== null) {
      if (!this.allPerson[this.mainPerson.wife].dead) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + this.translate.instant('gameLabel.wife') + " " + this.allPerson[this.mainPerson.wife].first_name;

        }
        else {
          realtionString = realtionString + this.translate.instant('gameLabel.wife') + " " + this.allPerson[this.mainPerson.wife].first_name;

        }
      }
    }
    else if (this.mainPerson.husband !== null) {
      if (this.allPerson[this.mainPerson.husband].seekANewRomanceDecisionFactroid) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + this.translate.instant('gameLabel.husband') + " " + this.allPerson[this.mainPerson.husband].first_name;
        }
        else {
          realtionString = realtionString + this.translate.instant('gameLabel.husband') + " " + this.allPerson[this.mainPerson.husband].first_name;
        }
      }
    }
    if (this.allPerson[this.mainPerson.children] !== null) {
      let children = this.mainPerson[this.constantService.FAMILY_CHILDREN];
      let sibString, sib, sex, sonCount = 0, dauCount = 0;
      for (var i = 0; i < children.length; i++) {
        sibString = this.mainPerson.children[i];
        sib = this.allPerson[sibString];
        if (!sib.dead) {
          if (sib.sex === "M") {
            sonCount = sonCount + 1;
          }
          else {
            dauCount = dauCount + 1;
          }
        }
      }
      if (sonCount === 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + sonCount + " " + this.translate.instant('gameLabel.son')
        }
        else {
          realtionString = realtionString + sonCount + " " + this.translate.instant('gameLabel.son')

        }
      }
      else if (sonCount > 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + sonCount + " " + this.translate.instant('gameLabel.sons')
        }
        else {
          realtionString = realtionString + sonCount + " " + this.translate.instant('gameLabel.sons')

        }
      }

      if (dauCount === 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + dauCount + " " + this.translate.instant('gameLabel.daughter')
        }
        else {
          realtionString = realtionString + "" + dauCount + " " + this.translate.instant('gameLabel.daughter')

        }
      }
      else if (dauCount > 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + dauCount + " " + this.translate.instant('gameLabel.daughters')
        }
        else {
          realtionString = realtionString + "" + dauCount + " " + this.translate.instant('gameLabel.daughters')

        }

      }


    }
    if (this.allPerson[this.mainPerson.siblings] !== null) {
      let siblings = this.mainPerson[this.constantService.FAMILY_SIBLING];
      let sibString, sib, sex, sisCount = 0, brotherCount = 0;
      for (var i = 0; i < siblings.length; i++) {
        sibString = this.mainPerson.siblings[i];
        sib = this.allPerson[sibString];
        if (!sib.dead) {
          if (sib.sex === "M") {
            brotherCount = brotherCount + 1;
          }
          else {
            sisCount = sisCount + 1;
          }
        }
      }

      if (brotherCount === 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + brotherCount + " " + this.translate.instant('gameLabel.brother')
        }
        else {
          realtionString = realtionString + "" + brotherCount + " " + this.translate.instant('gameLabel.brother')

        }
      }
      else if (brotherCount > 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + brotherCount + " " + this.translate.instant('gameLabel.brothers')
        }
        else {
          realtionString = realtionString + "" + brotherCount + " " + this.translate.instant('gameLabel.brothers')

        }
      }


      if (sisCount === 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + sisCount + " " + this.translate.instant('gameLabel.sister')
        }
        else {
          realtionString = realtionString + "" + sisCount + " " + this.translate.instant('gameLabel.sister')

        }
      }
      else if (sisCount > 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + sisCount + " " + this.translate.instant('gameLabel.sisters')
        }
        else {
          realtionString = realtionString + "" + sisCount + " " + this.translate.instant('gameLabel.sisters')

        }
      }


    }




    if (this.allPerson[this.mainPerson.grand_children] !== null) {
      let grandChildren = this.mainPerson[this.constantService.FAMILY_GRANDCHILDREN];
      let sibString, sib, grandSonCount = 0, geandDauCount = 0;
      for (var i = 0; i < grandChildren.length; i++) {
        sibString = this.mainPerson.grand_children[i];
        sib = this.allPerson[sibString];
        if (!sib.dead) {
          if (sib.sex === "M") {
            grandSonCount = grandSonCount + 1;
          }
          else {
            grandSonCount = grandSonCount + 1;
          }
        }
      }

      if (grandSonCount === 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + grandSonCount + " " + this.translate.instant('gameLabel.grandchildren')
        }
        else {
          realtionString = realtionString + "" + grandSonCount + " " + this.translate.instant('gameLabel.grandchildren')

        }
      }
      else if (grandSonCount > 1) {
        if (realtionString !== "") {
          realtionString = realtionString + ", " + grandSonCount + " " + this.translate.instant('gameLabel.grandchildrens')
        }
        else {
          realtionString = realtionString + "" + grandSonCount + " " + this.translate.instant('gameLabel.grandchildrens')

        }
      }
    }
    realtionString = realtionString.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
    if (this.mainPerson.count_of_household_members === 1) {
      this.str2 = this.translate.instant('gameLabel.obituary8');
      // this.str2="At the time of your death, you were alone. ";
    }
    else if (this.mainPerson.count_of_household_members > 1) {
      this.str2 = this.translate.instant('gameLabel.obituary95');
      // this.str2="At the time of your death, you were alone. ";
    } {

    }
    let family;
    if (realtionString !== "") {
      this.translate.get('gameLabel.obituary9', { realtionString: realtionString }).subscribe(
        (str) => {
          family = str;
          this.str2 = this.str2 + family;
        })
      // this.str2=" Your family was with you at the time of your death. You are survived by your "+ realtionString+". ";
    }


    //mother father dead
    //     At a young age, you lost your mother, who was 55 years old.

    // At a young age, you lost your father, who was 55 years old.
    // You lost your mother when she was 70 years old.

    // You lost your father when he was 70 years old.\
    //mother
    let dead;
    if (this.allPerson[this.mainPerson.mother].current_economic_points > this.allPerson[this.mainPerson.father].current_economic_points) {
      dead = this.allPerson[this.mainPerson.mother].current_economic_points
    }
    else {
      dead = this.allPerson[this.mainPerson.father].current_economic_points

    }

    if (dead < 14) {
      if (this.allPerson[this.mainPerson.mother].dead && this.allPerson[this.mainPerson.father].dead) {
        this.translate.get('gameLabel.obituary10', { dead: dead }).subscribe(
          (str) => {
            this.str30 = str;
          })
          let orphaneString = this.gameSummeryService.orpaheStringArray(this.allPerson[this.constantService.FAMILY_SELF].orphanIndex)

          let sibDeadCount=this.allPerson[this.constantService.FAMILY_SELF].siblings.length
          if (sibDeadCount > 0) {
            this.translate.get('gameLabel.bornSummary65', { takenCareString: orphaneString }).subscribe(
              (str) => {
                this.str30 = this.str30 + " " + str;
              })

          }
          else {
            this.translate.get('gameLabel.bornSummary64', { takenCareString: orphaneString }).subscribe(
              (str) => {
                this.str30 = this.str30 + " " + str;
              })
          }
        // this.str30=" Sadly, both of your parents died when you were " +dead+ " years old, so you became an orphan."
      }
      else {
        if (this.allPerson[this.mainPerson.mother].dead) {
          this.translate.get('gameLabel.obituary11', { motherAge: this.allPerson[this.mainPerson.mother].age }).subscribe(
            (str) => {
              this.str30 = str;
            })
          // this.str30=" At a young age, you lost your mother, who was "+this.allPerson[this.mainPerson.mother].age+" years old."

        }
        if (this.allPerson[this.mainPerson.father].dead) {
          this.translate.get('gameLabel.obituary12', { fatherAge: this.allPerson[this.mainPerson.father].age }).subscribe(
            (str) => {
              this.str31 = str;
            })
          // this.str31="At a young age, you lost your father, who was "+this.allPerson[this.mainPerson.father].age+" years old."
        }
      }

    }
    else {
      if (this.allPerson[this.mainPerson.mother].dead) {
        this.translate.get('gameLabel.obituary13', { motherAge: this.allPerson[this.mainPerson.mother].age }).subscribe(
          (str) => {
            this.str30 = str;
          })
        // this.str30=" You lost your mother when she was "+this.allPerson[this.mainPerson.mother].age+" years old."

      }
      if (this.allPerson[this.mainPerson.father].dead) {
        this.translate.get('gameLabel.obituary14', { fatherAge: this.allPerson[this.mainPerson.father].age }).subscribe(
          (str) => {
            this.str31 = str;
          })
        // this.str31="You lost your father when he was "+this.allPerson[this.mainPerson.father].age+" years old."
      }
    }

    //help for friend/governement

    if (this.mainPerson.help_from_government > 0 && this.mainPerson.help_from_friend_count > 0) {
      this.str37 = this.translate.instant('gameLabel.obituary119')
    }
    else if (this.mainPerson.help_from_government) {
      this.str37 = this.translate.instant('gameLabel.obituary117')
    }
    else if (this.mainPerson.help_from_friend_count > 0) {
      this.str37 = this.translate.instant('gameLabel.obituary118')
    }



    //dhild dead
    if (this.mainPerson.children.length !== 0) {
      let childSex, sonCount = 0, daughterCount = 0, sonAge = "", daughterAge = "";
      for (let i = 0; i < this.mainPerson.children.length; i++) {
        let child = this.mainPerson.children[i];
        if (this.allPerson[child].dead) {
          if (this.allPerson[child].sex === "M") {
            sonCount = sonCount + 1;
            if (sonAge === '') {
              sonAge = this.allPerson[child].age;
            }
            else {
              sonAge = sonAge + ", " + this.allPerson[child].age;
              sonAge = sonAge.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
            }
          }
          else {
            daughterCount = daughterCount + 1;
            if (daughterAge === '') {
              daughterAge = this.allPerson[child].age;
            }
            else {
              daughterAge = daughterAge + ", " + this.allPerson[child].age;
              daughterAge = daughterAge.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
            }
          }
        }
      }
      if (sonCount === 1 && daughterCount === 1) {
        this.translate.get('gameLabel.obituary102', { sonAge: sonAge, daughterAge: daughterAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })

        //  let str="Your son and daughter died when they were {{sonAge}}, {{daughterAge}} years old."
      }
      else if (sonCount > 1 && daughterCount > 1) {
        this.translate.get('gameLabel.obituary103', { sonCount: sonCount, daughterCount: daughterCount, sonAge: sonAge, daughterAge: daughterAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
        // let str="Your {{sonCount}} sons and {{daughterCount}} daughters died when they were {{sonAge}}, {{daughterAge}} years old."
      }
      else if(sonCount > 1 && daughterCount === 1){
        this.translate.get('gameLabel.obituary121', { sonCount: sonCount, daughterCount: daughterCount, sonAge: sonAge, daughterAge: daughterAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
      else if(sonCount === 1 && daughterCount > 1){
        this.translate.get('gameLabel.obituary122', { sonCount: sonCount, daughterCount: daughterCount, sonAge: sonAge, daughterAge: daughterAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
      else if (sonCount === 0 && daughterCount > 1) {
        this.translate.get('gameLabel.obituary106', { count: sonCount, age: daughterAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
      else if (sonCount > 1 && daughterCount === 0) {
        this.translate.get('gameLabel.obituary107', { count: sonCount, age: sonAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
      else if (sonCount === 1 && daughterCount === 0) {
        this.translate.get('gameLabel.obituary104', { age: sonAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
      else if (sonCount === 0 && daughterCount === 1) {
        this.translate.get('gameLabel.obituary105', { age: daughterAge }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
    }
    //sib dead
    let stillbornFlag: boolean = false, brotehrCount = 0, sisterCount = 0
    if (this.mainPerson.siblings.length !== 0) {
      for (let i = 0; i < this.mainPerson.siblings.length; i++) {
        let sib = this.mainPerson.siblings[i];
        if (this.allPerson[sib].dead) {
          if (this.allPerson[sib].age > 0) {
            if (this.allPerson[sib].sex === "M") {
              brotehrCount = brotehrCount + 1;
            }
            else {
              sisterCount = sisterCount + 1;
            }
          }
          else {
            stillbornFlag = true;
          }
        }
      }
      let v1 = "", v2 = ""
      if (brotehrCount > 1 && sisterCount > 1) {
        this.translate.get('gameLabel.obituary111', { count1: brotehrCount, count2: sisterCount }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
        //  v1=this.translate.instant('gameLabel.obituary111');
        //   this.str31=this.str31+v1
      }
      else if (brotehrCount === 1 && sisterCount === 1) {
        v1 = this.translate.instant('gameLabel.obituary110');
        this.str31 = this.str31 + v1
      }
      else if (brotehrCount === 1 && sisterCount === 0) {
        v1 = this.translate.instant('gameLabel.obituary108');
        this.str31 = this.str31 + v1
      }
      else if (brotehrCount === 0 && sisterCount === 1) {
        v1 = this.translate.instant('gameLabel.obituary109');
        this.str31 = this.str31 + v1
      }
      else if (brotehrCount > 1 && sisterCount === 0) {
        this.translate.get('gameLabel.obituary113', { count: brotehrCount }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
      else if (brotehrCount === 0 && sisterCount > 1) {
        this.translate.get('gameLabel.obituary114', { count: sisterCount }).subscribe(
          (str) => {
            this.str31 = this.str31 + str;
          })
      }
      if (stillbornFlag) {
        v2 = this.translate.instant('gameLabel.obituary112');
        this.str31 = this.str31 + v2
      }
    }


    //loan or investment
    if (this.mainPerson.expense.householdNetWorth > 0) {
      let investment
      let DollerNetworth = (this.mainPerson.expense.householdNetWorth / this.mainPerson.country.ExchangeRate).toFixed(0)
      let dollerCash = (this.mainPerson.expense.householdCash / this.mainPerson.country.ExchangeRate).toFixed(0)
      let dollerAssets = (this.mainPerson.expense.householdAssets / this.mainPerson.country.ExchangeRate).toFixed(0)
      if (!this.mainPerson.head_of_household) {
        this.translate.get('gameLabel.obituary15', { householdNetWorth: this.mainPerson.expense.householdNetWorth, CurrencyPlural: this.mainPerson.country.CurrencyPlural, DollerNetworth: DollerNetworth }).subscribe(
          (str) => {
            this.str3 = str;
          })

        // this.str3=" Your family has a total net worth of "+ this.mainPerson.expense.householdNetWorth +" " +this.mainPerson.country.CurrencyPlural+" ($"+DollerNetworth+")  networth after you. ";

      } else {
        this.translate.get('gameLabel.obituary16', { householdNetWorth: this.mainPerson.expense.householdNetWorth, CurrencyPlural: this.mainPerson.country.CurrencyPlural, DollerNetworth: DollerNetworth }).subscribe(
          (str) => {
            this.str3 = str;
          })

        // this.str3=" Your family inherits "+ this.mainPerson.expense.householdNetWorth +" " +this.mainPerson.country.CurrencyPlural+" ($"+DollerNetworth+")  net worth of cash and investments after your death. ";
      }
    }
    else if (this.mainPerson.expense.householdNetWorth < 0) {
      if (this.mainPerson.expense.householdLiabilities > 0) {
        let liablities = (this.mainPerson.expense.householdLiabilities / this.mainPerson.country.ExchangeRate).toFixed(0)
        this.translate.get('gameLabel.obituary17', { householdLiabilities: this.mainPerson.expense.householdLiabilities, CurrencyPlural: this.mainPerson.country.CurrencyPlural, liablities: liablities }).subscribe(
          (str) => {
            this.str3 = str;
          })

        //  this.str3=" You left your family with a "+this.mainPerson.expense.householdLiabilities+" " +this.mainPerson.country.CurrencyPlural+" ($"+liablities+") debt that needs to be paid off.";
      }
    }
    if (this.mainPerson.age > 11) {
      if (this.mainPerson.job.JobName !== this.constantService.JOB_UNEMPLOYED && this.mainPerson.job.JobName !== this.constantService.JOB_NO_JOB && this.mainPerson.job.JobName !== this.constantService.JOB_DOMESTIC_CHORES && this.mainPerson.job.JobName !== this.constantService.JOB_STUDENT) {
        let dollerIncome = ((this.mainPerson.income * 12) / this.mainPerson.country.ExchangeRate).toFixed(0);

        this.translate.get('gameLabel.obituary18', { displayName: this.mainPerson.job.Job_displayName, income: (this.mainPerson.income * 12).toFixed(0), CurrencyPlural: this.mainPerson.country.CurrencyPlural, dollerIncome: dollerIncome }).subscribe(
          (str) => {
            this.str4 = str;
          })
        // this.str4=" At the time of your death, you were employed as a "+ this.mainPerson.job.Job_displayName+ " earning an annual salary of "+(this.mainPerson.income*12).toFixed(0)+" " +this.mainPerson.country.CurrencyPlural+" ($"+dollerIncome+") .";

      }
      else {
        this.str4 = this.translate.instant('gameLabel.obituary19');
        // this.str4=" At the time of your death, you weren't working.";
      }
    }
    let born = (this.mainPerson.born_economical_status);
    let current = (this.mainPerson.current_economical_status);

    if (born < current) {
      this.str18 = this.translate.instant('gameLabel.obituary20');
      //  this.str18=" Throughout your lifetime, you have successfully climbed the economic ladder. "
    }
    else if (born > current) {
      this.str18 = this.translate.instant('gameLabel.obituary21');
      // this.str18=" Unfortunately, you were unable to climb the economic ladder during your lifetime. "
    }
    else if (born === current) {
      this.translate.get('gameLabel.obituary22', { status: this.commonService.getSatusString(this.mainPerson.born_economical_status) }).subscribe(
        (str) => {
          this.str5 = str;
        })
      //  this.str5=" Your family was in "+this.commonService.getSatusString(this.mainPerson.born_economical_status)+ " category when you were born and remained on the same level of the economic ladder when you died. ";
    }
    //todo check a and an
    //  this.translate.get('gameLabel.obituary23',{boenan:boenan,diet:this.finanaceService.diet[this.mainPerson.born_diet_index].name,shelter:this.finanaceService.shelter[this.mainPerson.born_shelter_index].name}).subscribe(
    //   (str)=>{ 
    //     this.str6=str;
    //   })
    //  this.str6=" When you were born, your family had "+boenan+" "+this.finanaceService.diet[this.mainPerson.born_diet_index].name+ " food plan and lived in a " +this.finanaceService.shelter[this.mainPerson.born_shelter_index].name +" accommodation. ";
    this.getShelterString(this.mainPerson.expense.shelter.shelterIndex, this.mainPerson.born_shelter_index);
    this.str6 = this.shelterString;
    let sibCount
    if (this.mainPerson.siblings.length !== 0) {
      sibCount = this.mainPerson.siblings.length;
    }
    else {
      sibCount = this.translate.instant('gameLabel.no');
    }
    // this.translate.get('gameLabel.obituary24',{localan:localan,diet:this.finanaceService.diet[this.mainPerson.expense.diet.dietIndex].name,shelter:this.finanaceService.shelter[this.mainPerson.expense.shelter.shelterIndex].name}).subscribe(
    //   (str)=>{ 
    //     this.str7=str;
    //   })
    //  this.str7=" At the time of your death, your family had  "+localan+" "+this.finanaceService.diet[this.mainPerson.expense.diet.dietIndex].name+" food plan and stayed in the "+this.finanaceService.shelter[this.mainPerson.expense.shelter.shelterIndex].name+" accommodation. "
    this.getDietString(this.mainPerson.expense.diet.dietIndex, this.mainPerson.born_diet_index);
    this.str7 = this.dietString;

    let graduate, college, vocational, school;
    education = this.mainPerson.education;
    //graduate
    if (education.graduate.inGraduate) {
      graduate = this.translate.instant('gameLabel.Attending_graduate');
    }
    else if (education.graduate.graduateDegree && !education.graduate.graduateDropOut) {
      this.translate.get('gameLabel.obituary25', { fieldName: education.graduate.fieldName }).subscribe(
        (str) => {
          graduate = str;
        })
      // graduate="graduation with degree in "+education.graduate.fieldName;
    }
    else if (!education.graduate.graduateDegree && education.graduate.graduateDropOut) {
      this.translate.get('gameLabel.obituary26', { graduateCompletedYears: education.graduate.graduateCompletedYears }).subscribe(
        (str) => {
          graduate = str;
        })
      // graduate="graduate dropOut (" +education.graduate.graduateCompletedYears +" years completed)"
    }
    //college

    if (education.college.inCollege) {
      college = this.translate.instant('gameLabel.Attending_college');
    }
    else if (education.college.collegeGraduate) {
      this.translate.get('gameLabel.obituary27', { fieldName: education.college.fieldName }).subscribe(
        (str) => {
          college = str;
        })
      // college="College degree in "+education.college.fieldName;
    }
    else if (education.college.collegeDropOut && !education.college.collegeGraduate) {
      this.translate.get('gameLabel.obituary28', { collegeCompletedYears: education.college.collegeCompletedYears }).subscribe(
        (str) => {
          college = str;
        })
      // college="College dropOut (" +education.college.collegeCompletedYears +" years completed)"
    }
    //vocational
    if (education.vocational.vocationalDegree) {
      this.translate.get('gameLabel.obituary29', { fieldName: education.vocational.fieldName }).subscribe(
        (str) => {
          vocational = str;
        })

      // vocational="vocational degree in "+education.vocational.fieldName
    }



    //school
    if (education.school.inSchool) {
      school = this.translate.instant('gameLabel.obituary58');
      // school="Unfortunately, you did not finish your school education as you passed away"
      if (education.school.schoolCompletedYears > 0) {
        this.translate.get('gameLabel.obituary59', { years: education.school.schoolCompletedYears }).subscribe(
          (str) => {
            school = school + str;
          })
        // school=school+" You could complete only {{age}} years of schooling."
      }
    }
    else if (education.school.highSchoolGraduate && !education.school.schoolDropOut) {
      school = this.translate.instant('gameLabel.obituary115');
    }
    if (!education.school.highSchoolGraduate && education.school.schoolDropOut && !education.school.inSchool) {
      this.translate.get('gameLabel.School_drop_out', { years: education.school.schoolCompletedYears }).subscribe(
        (str) => {
          school = str;
        })
      // school="School drop out (" +education.school.schoolCompletedYears +" years completed)"
    }
    if (this.mainPerson.age > 6) {
      if (this.mainPerson.education.school.schoolDropOut) {
        this.str8 = this.translate.instant('gameLabel.obituary30');
        //  this.str8="Unfortunately, you were not able to complete your education. "
      }
      else {
        this.education = "";
        if (education.graduate.graduateDegree) {
          this.education = graduate;
        }
        else if (education.college.collegeGraduate) {
          if (this.education === '') {
            this.education = this.education + college;
          }

        }
        //  else if(this.education!==""){
        //   this.education=this.education +", "+school;
        // }
        // else{
        //   this.education=this.education +" "+school;

        // }

        if (education.vocational.vocationalDegree) {
          if (this.education === '') {
            this.education = this.education + vocational;
          }
          else {
            this.education = this.education + ", " + vocational;
            this.education = this.education.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
          }
        }
        if (!education.school.highSchoolGraduate && education.school.schoolDropOut) {
          this.str8 = this.translate.instant('gameLabel.obituary30');
          // this.str8="Unfortunately, you were not able to complete your education. " 
        }
        else {
          this.str8 = school;
          if (this.education !== '') {
            this.translate.get('gameLabel.obituary31', { education: this.education }).subscribe(
              (str) => {
                this.str8 = this.str8 + str;
              })
          }
          else {
            this.str8 = this.str8 + ".";
          }
          //  this.str8=" You earned a " +this.education+".";   
        }
      }
    }
    //job
    let age, jobName, salary, jobList = [], businessList = [], fbusinessName, jobType;
    if (this.mainPerson.age > 11) {
      let allCarrerCards = Object.assign([], this.allPerson[this.constantService.FAMILY_SELF].my_career).reverse();
      allCarrerCards.reverse();
      if (allCarrerCards.length > 1) {
        for (let i = 0; i < allCarrerCards.length; i++) {
          if (allCarrerCards[i].careerType === 'job') {
            jobType = allCarrerCards[i].careerType;
            if (allCarrerCards[i].jobId !== 999 && allCarrerCards[i].jobId !== 132 && allCarrerCards[i].jobId !== 131 && allCarrerCards[i].jobId !== 130) {
              this.flagForFirstJob = true;
              age = allCarrerCards[i].fromAge;
              jobName = allCarrerCards[i].jobName;
              salary = allCarrerCards[i].currentSalary;
              break;
            }
          }
          else if (allCarrerCards[i].careerType === 'business') {
            jobType = allCarrerCards[i].careerType;
            if (allCarrerCards[i].businessId !== 130) {
              age = allCarrerCards[i].toAge;
              fbusinessName = allCarrerCards[i].jobName;
              salary = allCarrerCards[i].startUpCapital;
              break;
            }
          }
        }
        //getJobList
        for (let i = 0; i < allCarrerCards.length; i++) {
          if (allCarrerCards[i].careerType === "job") {
            if (allCarrerCards[i].jobId !== 999 && allCarrerCards[i].jobId !== 132 && allCarrerCards[i].jobId !== 132 && allCarrerCards[i].jobId !== 131) {
              jobList.push(allCarrerCards[i].jobName)
            }
          }
          else if (allCarrerCards[i].careerType === "business") {
            if (allCarrerCards[i].businessId !== 130) {
              businessList.push(allCarrerCards[i].jobName);
            }
          }
        }
        jobList = jobList.filter((v, i, a) => a.indexOf(v) === i);
        businessList = businessList.filter((v, i, a) => a.indexOf(v) === i);
        if (!this.flagForFirstJob) {
          businessList.splice(0, 1);
        }
        else {
          jobList.splice(0, 1);

        }
        let jobs = jobList.join(', ');
        jobs = jobs.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');
        let business = businessList.join(', ');
        business = business.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + '$1');

        //todo add business statement
        let salaryindoller = (salary / this.mainPerson.country.ExchangeRate).toFixed(0);
        if (jobType === 'job') {
          if (age < 20 && !this.mainPerson.education.school.schoolDropOut) {
            this.translate.get('gameLabel.obituary32', { jobName: jobName, age: age, salary: salary.toFixed(0), salaryindoller: salaryindoller, CurrencyPlural: this.mainPerson.country.currency_code }).subscribe(
              (str) => {
                this.str9 = str;
              })
            //  this.str9=" Your first part-time job was a "+jobName +" when you were "+age+" years old. You earned " +salary.toFixed(0)+" ($"+salaryindoller+") in this job. " ;
          }
          else {
            this.translate.get('gameLabel.obituary33', { age: age, jobName: jobName, salary: salary.toFixed(0), salaryindoller: salaryindoller, CurrencyPlural: this.mainPerson.country.currency_code }).subscribe(
              (str) => {
                this.str9 = str;
              })
            // this.str9= " Your first full-time job in your life started at the age of "+age+" as a "+jobName +" earning " +salary.toFixed(0)+" ($"+salaryindoller+") .";
          }
        }
        else {
          this.translate.get('gameLabel.obituary62', { businessName: fbusinessName, age: age, yearlyInvestment: salary.toFixed(0), currency: this.mainPerson.country.currency_code }).subscribe(
            (str) => {
              this.str9 = str;
            })
          // You successfully started your first business {{businessName}} with yearly investment {{yearlyInvestment}}{{currency}}
          // You successfully started your first business +{{businessName}} with yearly investment +{{yearlyInvestment}}+{{currency}} !!
        }
        if (jobList.length > 0) {
          this.translate.get('gameLabel.obituary34', { str9: this.str9, jobs: jobs }).subscribe(
            (str) => {
              this.str9 = str;
            })
          //  this.str9=this.str9+" Thereafter, you worked as a "+jobs+" thoroughout your life. ";
        }

        //  else{
        //   this.str9=this.str9+". ";

        //  }
        if (businessList.length !== 0) {
          this.translate.get('gameLabel.obituary35', { business: business }).subscribe(
            (str) => {
              this.str10 = str;
            })
          //  this.str10="You also started your own business of "+business+ "."; 
          //  By the age of 20, you had established your own shoemaking business before you even held a job. You certainly had entrepreneurial instincts.
        }

      }
      else {
        if (this.str9 === '' && this.str10 === '') {
          this.str9 = this.translate.instant('gameLabel.obituary116')
        }
      }
    }
    //moveout prison immigration
    let allDisplayResidenceList, mhometype, selfHomeType, deadShelter, moveOutAge, changeCity, imigration, countryName, city = "", cCountry;
    //  if(this.mainPerson.head_of_household){
    let currentResidence = []
    currentResidence = JSON.parse(JSON.stringify(this.allPerson[this.constantService.FAMILY_SELF].current_residence));
    allDisplayResidenceList = JSON.parse(JSON.stringify(this.allPerson[this.constantService.FAMILY_SELF].my_residence));
    //  allDisplayResidenceList.push(this.allPerson[this.constantService.FAMILY_SELF].current_residence);
    for (let i = 0; i < allDisplayResidenceList.length; i++) {
      if (allDisplayResidenceList[i].livingWith === "OWN") {
        mhometype = allDisplayResidenceList[i - 1].shelterIndex;
        selfHomeType = allDisplayResidenceList[i].shelterIndex;
        moveOutAge = allDisplayResidenceList[i].fromAge;
        this.translate.get('gameLabel.obituary36', { moveOutAge: moveOutAge, mhometype: this.finanaceService.shelter[mhometype].name, selfHomeType: this.finanaceService.shelter[selfHomeType].name }).subscribe(
          (str) => {
            this.str11 = str;
          })
        //  this.str11=" At the age of "+moveOutAge+", you moved out of your parents' house which was "+this.finanaceService.shelter[mhometype].name+" to your own which was "+this.finanaceService.shelter[selfHomeType].name+". "
        break;
      }
      // }
    }
    allDisplayResidenceList.push(currentResidence)
    let cityList;
    let cityArray = [];
    if (this.allPerson[this.constantService.FAMILY_SELF].current_residence.actionType === "CHANGE_CITY") {
      cityList = this.allPerson[this.constantService.FAMILY_SELF].current_residence.cityName
      this.translate.get('gameLabel.obituary37', { cityList: cityList }).subscribe(
        (str) => {
          this.str25 = str;
        })  // this.str25=" During your lifetime you migrated to another city away from your parent's home."
    }
    for (let i = 0; i < allDisplayResidenceList.length; i++) {
      if (allDisplayResidenceList[i].actionType === "CHANGE_CITY") {
        cityArray.push(allDisplayResidenceList[i].cityName)
      }
    }
    if (cityArray.length > 0) {
      let unique = cityArray.map(item => item).filter((value, index, self) => self.indexOf(value) === index)
      cityList = unique.toString();
      cityList = cityList.replace(/, /g, '');
      cityList = cityList.replaceAll(',', ', ');
      cityList = cityList.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + " " + '$1');
      this.translate.get('gameLabel.obituary37', { cityList: cityList }).subscribe(
        (str) => {
          this.str25 = str;
        })
    }
    let countryArray = [];
    for (let i = 0; i < allDisplayResidenceList.length; i++) {
      if (allDisplayResidenceList[i].actionType === "LEGAL_EMIGRATE" || allDisplayResidenceList[i].actionType === "ILLEGAL_EMIGRATE") {
        if (allDisplayResidenceList[i].countryId !== this.mainPerson.country.countryid) {
          countryArray.push(allDisplayResidenceList[i].countryName);
        }
      }
    }
    if (this.allPerson[this.constantService.FAMILY_SELF].current_residence.actionType === "LEGAL_EMIGRATE" || this.allPerson[this.constantService.FAMILY_SELF].current_residence.actionType === "ILLEGAL_EMIGRATE") {
      countryArray.push(allDisplayResidenceList[i].countryName);

    }

    if (countryArray.length > 0) {
      let unique1 = countryArray.map(item => item).filter((value, index, self) => self.indexOf(value) === index)
      countryName = unique1.toString();
      countryName = countryName.replace(/,/g, '');
      countryName = countryName.replaceAll(',', ', ');
      countryName = countryName.replace(/,([^,]*)$/, " " + this.translate.instant('gameLabel.and') + " " + '$1');
      this.translate.get('gameLabel.obituary38', { countryName: countryName }).subscribe(
        (str) => {
          this.str29 = str;
        })
      // this.str29=" During your lifetime, you immigrated to other countries - "+countryName+".";
    }
    //BARICATE
    let prisonCount = 0;
    let totalAgeCount = 0;
    let fromAge = 0
    for (let i = 0; i < allDisplayResidenceList.length; i++) {
      if (allDisplayResidenceList[i].actionType === "PRISON") {
        prisonCount = prisonCount + 1;
        fromAge = allDisplayResidenceList[i].fromAge
        let age = (allDisplayResidenceList[i].toAge - allDisplayResidenceList[i].fromAge);
        totalAgeCount = totalAgeCount + age;
      }
    }
    if (prisonCount === 1) {
      this.translate.get('gameLabel.obituary39', { allDisplayResidenceList: fromAge, age: age }).subscribe(
        (str) => {
          this.str24 = str;
        })
    }
    else if (prisonCount > 1) {
      this.translate.get('gameLabel.obituary120', { age: totalAgeCount }).subscribe(
        (str) => {
          this.str24 = str;
        })
    }




    //Married
    if (this.mainPerson.age > 14) {
      let partnerName, allRelationCard = [], marriageCount = 0, realtionCount = 0, marriedCard = [], spouseDeadFlag: boolean = false, spouseAge = 0;
      let currentRelation;
      let deadSpouceName = [];
      let deadSPouceAge = []
      let result = [];
      let currentMarriedCard: boolean = false;
      let sex, heshe
      if (this.mainPerson.sex === "M") {
        sex = "wife"
        heshe = "she"
      }
      else {
        sex = "husband"
        heshe = "he"
      }
      //You lost your husband Kiran,Kishor when he was 23,45 years old.
      if (this.mainPerson.dead_spouse_list.length !== 0) {
        for (i = 0; i < this.mainPerson.dead_spouse_list.length; i++) {
          result = this.mainPerson.dead_spouse_list[i].split("_");
          deadSpouceName.push(result[0]);
          deadSPouceAge.push(result[1]);
        }
      }
      currentRelation = this.allPerson[this.constantService.FAMILY_SELF].current_my_relationship;
      currentRelation.toAge = this.mainPerson.age;
      allRelationCard = JSON.parse(JSON.stringify(this.allPerson[this.constantService.FAMILY_SELF].my_relationship));
      allRelationCard.push(currentRelation)
      for (let i = 0; i < allRelationCard.length; i++) {
        if (allRelationCard[i].relationType === 'MARRIED') {
          marriageCount = marriageCount + 1;
          partnerName = allRelationCard[i].LoverFullName;
          for (let i = 0; i < deadSpouceName.length; i++) {
            if (partnerName === deadSpouceName[i]) {
              spouseAge = deadSPouceAge[i]
              spouseDeadFlag = true;
            }
          }
          if (this.mainPerson.age === allRelationCard[i].toAge) {
            currentMarriedCard = true;
          }
          marriedCard.push({
            "partnerName": partnerName,
            "dead": spouseDeadFlag,
            "age": spouseAge,
            "currentMarriedCard": currentMarriedCard
          })
        }
        if (allRelationCard[i].relationType === "NEW_ROMANCE") {
          realtionCount = realtionCount + 1
        }
      }
      if (realtionCount === 1) {
        this.translate.get('gameLabel.obituary40', { realtionCount: realtionCount }).subscribe(
          (str) => {
            this.str19 = str;
          })
        // this.str19=" In your lifetime, you had "+realtionCount +" romantic relationship. "
      }
      else if (realtionCount > 1) {
        this.translate.get('gameLabel.obituary41', { realtionCount: realtionCount }).subscribe(
          (str) => {
            this.str19 = str;
          })
        // this.str19=" In your lifetime, you had "+realtionCount +" romantic relationships. "
      }
      if (this.mainPerson.married) {
        if (this.mainPerson.sex === 'M') {
          partnerName = this.allPerson[this.mainPerson.wife].first_name;
        }
        else {
          partnerName = this.allPerson[this.mainPerson.husband].first_name;
        }
        if (deadSpouceName.length !== 0) {
          for (let i = 0; i < deadSpouceName.length; i++) {
            if (partnerName === deadSpouceName[i]) {
              this.translate.get('gameLabel.obituary100', { sex: sex, deadSpouceNameString: deadSpouceName[i], heshe: heshe, deadSpouceAgeString: deadSPouceAge[i] }).subscribe(
                (str) => {
                  this.str35 = str;
                })
            }
          }
        }
        if (this.mainPerson.wife !== null || this.mainPerson.husband !== null) {
          this.translate.get('gameLabel.obituary42', { partnerName: partnerName }).subscribe(
            (str) => {
              this.str35 = str;
            })
        }
        //  else{
        // //  if(marriageCount==1 ){
        //   this.translate.get('gameLabel.obituary43',{partnerName:partnerName}).subscribe(
        //     (str)=>{ 
        //      this.str35=str;
        //     })
        //   }
        //  this.str14=" Your marriage to "+partnerName+" was happy, and you remained together until your death."
        //  }
      }
      if (marriageCount > 0) {
        if (marriageCount > 1) {
          this.translate.get('gameLabel.obituary96', { marriageCount: marriageCount }).subscribe(
            (str) => {
              this.str14 = str;
            })
        }
        else if (marriageCount === 1 && !this.mainPerson.married) {
          for (let k = 0; k < marriedCard.length; k++) {
            if (!marriedCard[k].currentMarriedCard) {
              if (marriedCard[k].dead) {
                this.translate.get('gameLabel.obituary100', { sex: sex, deadSpouceNameString: marriedCard[k].partnerName, heshe: heshe, deadSpouceAgeString: marriedCard[k].age }).subscribe(
                  (str) => {
                    this.str34 = this.str34 + str;
                  })
              }
              else {
                this.translate.get('gameLabel.obituary43', { partnerName: marriedCard[k].partnerName }).subscribe(
                  (str) => {
                    str = str.replaceAll(',', '');
                    this.str34 = this.str34 + str;

                  })
              }
            }
          }
        }
        if (marriageCount > 1) {
          for (let k = 0; k < marriedCard.length; k++) {
            if (!marriedCard[k].currentMarriedCard) {
              if (marriedCard[k].dead) {
                this.translate.get('gameLabel.obituary100', { sex: sex, deadSpouceNameString: marriedCard[k].partnerName, heshe: heshe, deadSpouceAgeString: marriedCard[k].age }).subscribe(
                  (str) => {
                    this.str34 = this.str34 + str;
                  })
              }
              else {
                this.translate.get('gameLabel.obituary43', { partnerName: marriedCard[k].partnerName }).subscribe(
                  (str) => {
                    this.str34 = this.str34 + str;
                  })
              }
            }
            else if (this.str35 === '') {
              if (deadSpouceName.length !== 0) {
                for (let i = 0; i < deadSpouceName.length; i++) {
                  if (partnerName === deadSpouceName[i]) {
                    this.translate.get('gameLabel.obituary100', { sex: sex, deadSpouceNameString: deadSpouceName[i], heshe: heshe, deadSpouceAgeString: deadSPouceAge[i] }).subscribe(
                      (str) => {
                        this.str35 = str;
                      })
                  }
                }
              }

            }
          }
        }
      }
      else {
        if (this.str14 === "") {
          this.str14 = this.translate.instant('gameLabel.obituary60');
          // this.str14=" You remained unmarried in life." 
        }
      }
    }
    //addiction
    let smoking = "", drug = "", alcohol = "";

    if (this.mainPerson.smoker) {
      smoking = this.translate.instant('gameLabel.obituary44');
      // smoking=" You took up smoking in your life because of peer pressure. ";
    }
    else if (!this.mainPerson.smoker && this.mainPerson.former_smoker) {
      smoking = this.translate.instant('gameLabel.obituary45');
      // smoking=" Although you started smoking, you were able to quit later. ";
    }

    if (this.mainPerson.drinker) {
      drug = this.translate.instant('gameLabel.obituary46');
      // drug=" You also started drinking alcohol because of peer pressure and became addicted to it.";
    }
    else if (!this.mainPerson.drinker && this.mainPerson.former_drinker) {
      drug = this.translate.instant('gameLabel.obituary47');
      // drug=" You managed to stop drinking successfully after becoming addicted to alcohol. ";
    }

    if (this.mainPerson.drug_user || this.mainPerson.drug_addict) {
      alcohol = this.translate.instant('gameLabel.obituary48');
      // alcohol=" You also began taking illicit drugs because of peer pressure and became addicted.";
    }
    else if (!this.mainPerson.drug_user && this.mainPerson.former_drug_user) {
      alcohol = this.translate.instant('gameLabel.obituary49');
      // alcohol=" After becoming addicted to illicit drugs, you succeeded in quitting them. ";
    }
    this.str15 = smoking + drug + alcohol;

    //blooddonation



    if (this.mainPerson.age > 17) {
      if (this.mainPerson.blood_donation_flag) {
        this.str16 = this.translate.instant('gameLabel.obituary50');
        // this.str16=" You enjoyed donating blood."

      }
      else {
        this.str16 = this.translate.instant('gameLabel.obituary51');
        // this.str16=" You never donated blood."
      }
    }
    this.str16 = this.str16 + organString;

    //charity
    if (this.mainPerson.charity_flag) {
      this.str17 = this.translate.instant('gameLabel.obituary52');
      // this.str17=" You donated a portion of your wealth to charity because you believed it was your duty to help others."

    }

    //pet
    let petList = "";
    if (this.mainPerson.age > 6) {
      if (this.mainPerson.my_pets.length !== 0) {
        for (let i = 0; i < this.mainPerson.my_pets.length; i++) {
          if (petList !== "") {
            petList = petList + ", " + this.mainPerson.my_pets[i].petType;
          }
          else {
            petList = petList + this.mainPerson.my_pets[i].petType;

          }
        }
      }

      if (this.mainPerson.dead_pet_list.length !== 0) {
        for (let i = 0; i < this.mainPerson.dead_pet_list.length; i++) {
          if (petList !== "") {
            petList = petList + ", " + this.mainPerson.dead_pet_list[i].petType;
          }
          else {
            petList = petList + this.mainPerson.dead_pet_list[i].petType;

          }
        }
      }

      if (petList === "") {
        this.str23 = this.translate.instant('gameLabel.obituary53');
        // this.str23="You never adopted or purchased a pet during your lifetime. ";
      }
      else {
        this.str23 = this.translate.instant('gameLabel.obituary54');
        // this.str23="Pets have been a part of your life since you were a child."
      }
    }

    //leisure traits
    //traits
    let happy = '', health = '', wealth = '', spritual = ''
    let privoiuseTraits = this.allEvent[this.allEvent.length - 1].oldTraits;

    if (privoiuseTraits.spiritual > 90) {
      spritual = this.translate.instant('gameLabel.obituary55');
      // spritual=" By nature, you were spiritual this.mainPerson. "
    }

    let leisureList = '';
    if (this.mainPerson.years_of_arts > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + " " + this.translate.instant('gameLabel.Art');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Art');
      }
    }
    if (this.mainPerson.years_of_church > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Religious_Activity');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Religious_Activity');
      }
    }

    if (this.mainPerson.years_of_music > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Musical');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Musical');
      }
    }
    if (this.mainPerson.years_of_socializing > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + " ," + this.translate.instant('gameLabel.Social_or_Political_Activities');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Social_or_Political_Activities');
      }
    }
    if (this.mainPerson.years_of_fashion > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Fashion_Clothing_Appearance');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Fashion_Clothing_Appearance');
      }
    }
    if (this.mainPerson.years_of_television > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Television_Viewing');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Television_Viewing');
      }
    }
    if (this.mainPerson.years_of_sports > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Sports');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Sports');
      }
    }
    if (this.mainPerson.years_of_endurance > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Physical_Endurance')
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Physical_Endurance')
      }
    }
    if (this.mainPerson.years_of_outdoors > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Outdoor_Activities');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Outdoor_Activities');
      }
    }
    // if(this.mainPerson.years_of_social_activism>1){
    //   leisureList=leisureList+",Fashion clothing apperance"
    //   }
    if (this.mainPerson.years_of_reading > 1) {
      if (leisureList !== '') {
        leisureList = leisureList + ", " + this.translate.instant('gameLabel.Reading_Study');
      }
      else {
        leisureList = leisureList + this.translate.instant('gameLabel.Reading_Study');
      }
    }
    let leisure = ""
    if (leisureList.length !== 0) {
      // leisureList = leisureList.replace(/,,/, ", ");
      // leisureList = leisureList.replace(/ /g, ", ");
      leisureList = leisureList.replace(/,([^,]*)$/, "  " + this.translate.instant('gameLabel.and') + '$1');
      this.translate.get('gameLabel.obituary56', { leisureList: leisureList }).subscribe(
        (str) => {
          leisure = str;
        })
      //  leisure="Your favorite outdoor activities included "+leisureList+". "
    }
    this.str13 = spritual + leisure
    // this.str22="You were born in Belarus, a RealLives player from United States. "

    this.str20 = this.translate.instant('gameLabel.obituary57');
    // this.str20= "This was your life. Life to learn from. Hope your next life will be very different bringing new experiences!!"

    this.sveObituaryObject['obituary'] = {
      'sub1': this.str1,
      'sub2': this.str18 + this.str5 + this.str6 + this.str7,
      'sub3': this.str30 + this.str31,
      'sub4': this.str37,
      'sub5': this.str2 + this.str26 + this.str27,
      'sub6': this.str23,
      'sub7': this.str4 + this.str3,
      'sub8': this.str8 + this.str9 + this.str10,
      'sub9': this.str11 + this.str25 + this.str29 + this.str24,
      'sub10': this.str19 + this.str28 + this.str14 + this.str33 + this.str34 + this.str35,
      'sub11': this.str15 + this.str21,
      'sub12': this.str17 + this.str16,
      'sub13': this.str13,
      'sub14': this.str20
    }
    return this.sveObituaryObject
    // this.postGameService.updateObituaryRetutnByMe(this.allPerson[this.constantService.FAMILY_SELF].game_id, this.sveObituaryObject, false).subscribe(
    //   res => {
    //     this.sveObituaryObject = res;
    //   })
  }

  getDietString(dietIndexDead, DietIndeaxBorn) {
    if (dietIndexDead == 0) {
      this.deadDiet = this.translate.instant('gameLabel.obituary93');
    }
    else if (dietIndexDead == 1) {
      this.deadDiet = this.translate.instant('gameLabel.obituary94');
    }
    else if (dietIndexDead == 2) {
      this.deadDiet = this.translate.instant('gameLabel.obituary61');
    }
    else if (dietIndexDead == 3) {
      this.deadDiet = this.translate.instant('gameLabel.obituary63');
    }
    else if (dietIndexDead == 4) {
      this.deadDiet = this.translate.instant('gameLabel.obituary64');
    }
    else if (dietIndexDead == 5) {
      this.deadDiet = this.translate.instant('gameLabel.obituary65');
    }
    else if (dietIndexDead == 6) {
      this.deadDiet = this.translate.instant('gameLabel.obituary66');
    }
    else if (dietIndexDead == 7) {
      this.deadDiet = this.translate.instant('gameLabel.obituary67');
    }
    //born
    if (DietIndeaxBorn == 0) {
      this.BornDiet = this.translate.instant('gameLabel.obituary68');
    }
    else if (DietIndeaxBorn == 1) {
      this.BornDiet = this.translate.instant('gameLabel.obituary69');
    }
    else if (DietIndeaxBorn == 2) {
      this.BornDiet = this.translate.instant('gameLabel.obituary70');
    }
    else if (DietIndeaxBorn == 3) {
      this.BornDiet = this.translate.instant('gameLabel.obituary71');
    }
    else if (DietIndeaxBorn == 4) {
      this.BornDiet = this.translate.instant('gameLabel.obituary72');
    }
    else if (DietIndeaxBorn == 5) {
      this.BornDiet = this.translate.instant('gameLabel.obituary73');
    }
    else if (DietIndeaxBorn == 6) {
      this.BornDiet = this.translate.instant('gameLabel.obituary74');
    }
    else if (DietIndeaxBorn == 7) {
      this.BornDiet = this.translate.instant('gameLabel.obituary75');
    }

    if (dietIndexDead == DietIndeaxBorn) {
      this.dietString = this.BornDiet + this.translate.instant('gameLabel.obituary76');
    }
    else {
      this.dietString = this.BornDiet + this.deadDiet
    }

  }

  getShelterString(shelterIndexDead, shelterIndexBorn) {
    if (shelterIndexDead == 0) {
      this.deadShelter = this.translate.instant('gameLabel.obituary77');
    }
    else if (shelterIndexDead == 1) {
      this.deadShelter = this.translate.instant('gameLabel.obituary78');
    }
    else if (shelterIndexDead == 2) {
      this.deadShelter = this.translate.instant('gameLabel.obituary79');
    }
    else if (shelterIndexDead == 3) {
      this.deadShelter = this.translate.instant('gameLabel.obituary80');
    }
    else if (shelterIndexDead == 4) {
      this.deadShelter = this.translate.instant('gameLabel.obituary81');
    }
    else if (shelterIndexDead == 5) {
      this.deadShelter = this.translate.instant('gameLabel.obituary82');
    }
    else if (shelterIndexDead == 6) {
      this.deadShelter = this.translate.instant('gameLabel.obituary83');
    }
    else if (shelterIndexDead == 7) {
      this.deadShelter = this.translate.instant('gameLabel.obituary84');



      //born
    }
    if (shelterIndexBorn == 0) {
      this.bornShelter = this.translate.instant('gameLabel.obituary85');
    }
    else if (shelterIndexBorn == 1) {
      this.bornShelter = this.translate.instant('gameLabel.obituary86');
    }
    else if (shelterIndexBorn == 2) {
      this.bornShelter = this.translate.instant('gameLabel.obituary87');
    }
    else if (shelterIndexBorn == 3) {
      this.bornShelter = this.translate.instant('gameLabel.obituary88');
    }
    else if (shelterIndexBorn == 4) {
      this.bornShelter = this.translate.instant('gameLabel.obituary89');
    }
    else if (shelterIndexBorn == 5) {
      this.bornShelter = this.translate.instant('gameLabel.obituary90');
    }
    else if (shelterIndexBorn == 6) {
      this.bornShelter = this.translate.instant('gameLabel.obituary91');
    }
    else if (shelterIndexBorn == 7) {
      this.bornShelter = this.translate.instant('gameLabel.obituary92');
    }


    if (shelterIndexBorn == shelterIndexDead) {
      this.bornShelter = this.bornShelter.replace(/\./g, '');

      this.shelterString = this.bornShelter + this.translate.instant('gameLabel.obituary76');
    }
    else {
      this.shelterString = this.bornShelter + this.deadShelter
    }

  }


}
