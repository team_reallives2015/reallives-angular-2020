import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from './home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { CommonService } from '../shared/common.service';
import { EventService } from './event/event.service';
import { Event } from './event/Event'
import { AgaAYearService } from './age-a-year/aga-a-year.service';
import { GameSummeryService } from '../game-summery/game-summery.service';
import { fromEvent, interval, Subject } from 'rxjs';
import { TranslationService } from '../translation/translation.service';
import { TranslateService } from '@ngx-translate/core';
import { browserRefresh } from '../app.component';
import { takeUntil } from 'rxjs/operators';
import { FinanceService } from './action-finance/finance.service';
import { CarrerService } from './action-carrer/carrer.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  falgForBusinessSdg: boolean = false;
  dispayCountryFlag: boolean = true;
  public browserRefresh: boolean;
  eventList;
  docInfoFlag: boolean = false;
  flagForCountryDoc: boolean = false;
  loadEvent: boolean = false;
  gameId;
  allPerson;
  parson;
  allEvents = [];
  data: boolean = false;
  livelifeClick: boolean = false;
  exitButtonFlag: boolean = false;
  goBackFlag: boolean = false;
  singleEvent;
  dead: boolean = false
  loanDisable;
  investmentDisable;
  subscriber;
  userName;
  flagForAssignMentsLoad;
  token
  checheFlag: boolean = false;
  scrHeight;
  scrWidth;
  showSuccessMsgFlag: boolean = false
  messageText;
  displaySdgFlag: boolean = false;
  private unsubscriber: Subject<void> = new Subject<void>();

  constructor(public homeService: HomeService,
    public translate: TranslateService,
    private router: Router,
    public modalWindowShowHide: modalWindowShowHide,
    public constantService: ConstantService,
    public commonService: CommonService,
    public carrerService: CarrerService,
    public eventService: EventService,
    public gameSummeryService: GameSummeryService,
    public agaAYearService: AgaAYearService,
    public translationService: TranslationService, public route: ActivatedRoute,
    public financeService: FinanceService) {
    this.subscriber = this.homeService.changeAllPerson.subscribe(
      persons => {
        this.homeService.allPerson = persons;
        this.checheFlag = true;
        this.dead = this.homeService.allPerson[this.constantService.FAMILY_SELF].dead;
        this.carrerService.oldBusinessFlag = this.commonService.checkBusinessTyoeOldOrNew(this.homeService.allPerson[this.constantService.FAMILY_SELF].business)
        if (this.dead) {
          this.checheFlag = false;
        }
        this.getAllDeadCheckCode();
        if (!this.dead) {
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId !== 130 && !this.carrerService.oldBusinessFlag) {
            if (this.homeService.displaySdgFlag) {
              this.dispayCountryFlag = true;
            }
            else {
              this.dispayCountryFlag = false;
            }
          }
          else {
            this.dispayCountryFlag = true;
          }
          if (this.homeService.displaySdgFlag) {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId === 130 || this.carrerService.oldBusinessFlag) {
              this.falgForBusinessSdg = true;
            }
            else {
              this.falgForBusinessSdg = false;
            }
          }
          else {
            this.falgForBusinessSdg = false;
          }
        }
      })
  }


  ngOnInit(): void {
    history.pushState(null, '');
    fromEvent(window, 'popstate')
      .pipe(takeUntil(this.unsubscriber))
      .subscribe((_) => {
        history.pushState(null, '');
      });
    this.modalWindowShowHide.showGotoHome = false;
    this.flagForAssignMentsLoad = this.route.snapshot.queryParamMap.get('flag')
    this.flagForAssignMentsLoad = this.getBoolean();
    if (!this.flagForAssignMentsLoad) {
      this.token = (this.route.snapshot.queryParamMap.get('token') || 0);
      localStorage.setItem('token', this.token);
      this.gameId = this.route.snapshot.queryParamMap.get('id')
      localStorage.setItem('gameid', this.gameId);
      this.gameSummeryService.getValidToken().subscribe(
        res => {
          this.token = res.data.result.token;
          localStorage.removeItem('token');
          localStorage.setItem('token', this.token);
          this.route.queryParamMap.subscribe(paramMap => {
            this.commonService.filter(paramMap, 'id');
            this.commonService.filter(paramMap, 'token');
            this.commonService.filter(paramMap, 'flag');
            this.commonService.removeParamFromUrl(paramMap, ['id', 'token', 'flag']);
          });
        })
    }
    else {
      this.gameId = localStorage.getItem('gameid');
      this.route.queryParamMap.subscribe(paramMap => {
        this.commonService.filter(paramMap, 'flag');
        this.commonService.removeParamFromUrl(paramMap, ['flag']);
      });
    }
    this.getPersonData();

  }
  private getBoolean(): boolean {
    return JSON.parse(this.flagForAssignMentsLoad);
  }
  getPersonData() {
    this.homeService.getPersonFromId(this.gameId).subscribe(res => {
      this.homeService.allPerson = res;
      this.flagForCountryDoc = this.homeService.allPerson.show_doc;
      this.userName = this.homeService.allPerson.username;
      this.homeService.setlang = this.homeService.allPerson.selected_language;
      this.translationService.setSelectedlanguage(this.homeService.allPerson.selected_language);
      this.translationService.changeLang.emit(this.homeService.allPerson.selected_language);
      this.dead = this.homeService.allPerson[this.constantService.FAMILY_SELF].dead;
      this.checkLoanAndInvestment();
      this.homeService.getAllEventList(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
        res => {
          this.eventList = res;
          for (let i = 0; i < this.eventList.length; i++) {
            this.singleEvent = new Event();
            this.singleEvent.gameEventId = this.eventList[i].game_event_id;
            this.singleEvent.eventId = this.eventList[i].lang_code;
            this.singleEvent.text = this.eventList[i].text;
            this.singleEvent.age = this.eventList[i].game_age;
            this.singleEvent.oldTraits = this.eventList[i].oldtraits;
            this.singleEvent.newTraits = this.eventList[i].newtraits;
            this.singleEvent.factroid = this.eventList[i].factroid;
            this.singleEvent.link = this.eventList[i].factroidLink;
            this.singleEvent.addline = this.eventList[i].addline;
            this.singleEvent.expression = this.eventList[i].expression
            this.singleEvent.type = this.eventList[i].type;
            this.allEvents.push(this.singleEvent);
            this.homeService.allEvent = this.allEvents;
          }
          this.eventService.currentEvent = this.homeService.allEvent[this.homeService.allEvent.length - 1];
          this.data = true;
          this.getAllDeadCheckCode();
          this.gameSummeryService.getBornSummary(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
            res => {
              this.gameSummeryService.bornSummary = res;
              this.getScreenSize();
              this.carrerService.oldBusinessFlag = this.commonService.checkBusinessTyoeOldOrNew(this.homeService.allPerson[this.constantService.FAMILY_SELF].business)
              if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId !== 130 && !this.carrerService.oldBusinessFlag) {
                if (this.homeService.displaySdgFlag) {
                  this.dispayCountryFlag = true;
                }
                else {
                  this.dispayCountryFlag = false;
                }
              }
              else {
                this.dispayCountryFlag = true;
              }
            })
        })
    })
  }
  getScreenSize() {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    if (this.scrWidth > 1225) {
      this.homeService.displaySdgFlag = true;
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId === 130 || this.carrerService.oldBusinessFlag) {
        this.falgForBusinessSdg = true;
      }
      else {
        this.falgForBusinessSdg = false;
      }
    }
    else {
      this.falgForBusinessSdg = false;
      this.falgForBusinessSdg = false;
    }
  }

  getAllDeadCheckCode() {
    if (this.dead && !this.homeService.clicktoGameSummary) {
      this.modalWindowShowHide.showGameSummary = false;
      this.modalWindowShowHide.showObituary = false;
      this.modalWindowShowHide.showFeddback = false;
      this.modalWindowShowHide.showBugReport = false;
      this.modalWindowShowHide.showEvent = true;
      this.modalWindowShowHide.showLetter = false;
      this.modalWindowShowHide.showAgeAYear = true;
      this.homeService.goToObituaryButton = false;
      this.homeService.activeLinkClasslifeExepectancy = "";
      this.homeService.activeLinkClassCountryCapsule = "";
      this.homeService.activeLinkClassLifeSummery = "active";
      this.homeService.actionLinkClassBusinessTab = ""
      this.homeService.activeLinkClassSdgIndicator = "";
      this.homeService.activeLinkClassfinanceStatus = "";
      this.modalWindowShowHide.showLifeExpectancyGraph = false;
      this.modalWindowShowHide.showCountryCapsule = false;
      this.modalWindowShowHide.showFinanceStatus = false;
      this.modalWindowShowHide.showSdgIndicator = false;
      this.modalWindowShowHide.showLifeSummery = true;
      setTimeout(() => {
        if (!this.checheFlag) {
          this.homeService.activeLinkClasslifeExepectancy = "";
          this.homeService.activeLinkClassCountryCapsule = "";
          this.homeService.activeLinkClassLifeSummery = "";
          this.homeService.activeLinkClassSdgIndicator = "";
          this.homeService.activeLinkClassfinanceStatus = "active";
          this.homeService.actionLinkClassBusinessTab = ""
          this.modalWindowShowHide.showLifeExpectancyGraph = false;
          this.modalWindowShowHide.showLifeSummery = false;
          this.modalWindowShowHide.showCountryCapsule = false;
          this.modalWindowShowHide.showFinanceStatus = true;
          this.modalWindowShowHide.showSdgIndicator = false;
          this.modalWindowShowHide.showBusinessTabFlag = false;
        }
        else {
          this.checheFlag = false;
        }

        this.modalWindowShowHide.showObituary = false;
        this.homeService.goToObituaryButton = true;
        this.modalWindowShowHide.showEvent = true;
        this.modalWindowShowHide.showGameSummary = false;
        this.modalWindowShowHide.showFeddback = false;
        this.modalWindowShowHide.showBugReport = false;
        this.modalWindowShowHide.showLetter = false;
      }, 100);
    }
    else if (!this.homeService.clicktoGameSummary) {
      this.modalWindowShowHide.showGameSummary = false;
      this.modalWindowShowHide.showObituary = false;
      this.modalWindowShowHide.showFeddback = false;
      this.modalWindowShowHide.showBugReport = false;
      this.modalWindowShowHide.showEvent = true;
      this.modalWindowShowHide.showLetter = false;
      this.modalWindowShowHide.showAgeAYear = true;
      if (!this.checheFlag) {
        this.homeService.activeLinkClassfinanceStatus = "active";
        this.homeService.activeLinkClassCountryCapsule = "";
        this.homeService.activeLinkClassLifeSummery = "";
        this.homeService.activeLinkClassSdgIndicator = "";
        this.homeService.activeLinkClasslifeExepectancy = ""
        this.homeService.actionLinkClassBusinessTab = ""
        this.modalWindowShowHide.showFinanceStatus = true;
        this.modalWindowShowHide.showCountryCapsule = false;
        this.modalWindowShowHide.showSdgIndicator = false;
        this.modalWindowShowHide.showLifeSummery = false;
        this.modalWindowShowHide.showBusinessTabFlag = false;
        this.modalWindowShowHide.showLifeExpectancyGraph = false;
      }
      else {
        this.checheFlag = false;
      }

    }

  }
  checkLoanAndInvestment() {
    let householdAssetsPer = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdAssets * 0.6;
    let maxAllowedLoan = householdAssetsPer - this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities > 0) {
      if (maxAllowedLoan <= 0) {
        this.loanDisable = true;
      }
      else {
        this.loanDisable = false;
      }
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash <= 9) {
      this.investmentDisable = true;
    }
    else {
      this.investmentDisable = false;
    }
  }
  seeYouLife() {
    this.homeService.goToObituaryButton = true;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showFeddback = false;
    this.modalWindowShowHide.showBugReport = false;
    this.modalWindowShowHide.showEvent = true;
    this.modalWindowShowHide.showLetter = false;
    this.agaAYearService.goObituaryButton = true;

  }

  gameSettingClick() {
    this.modalWindowShowHide.showGameSeetingsFlag = true;
  }


  countryCapsuleClick() {
    this.modalWindowShowHide.showCountryCapsule = true;
    this.modalWindowShowHide.showLifeSummery = false;
    this.modalWindowShowHide.showSdgIndicator = false;
    this.modalWindowShowHide.showLifeExpectancyGraph = false;
    this.modalWindowShowHide.showFinanceStatus = false;
    this.modalWindowShowHide.showBusinessTabFlag = false;
    this.homeService.activeLinkClassCountryCapsule = "active";
    this.homeService.activeLinkClassLifeSummery = "";
    this.homeService.activeLinkClassSdgIndicator = "";
    this.homeService.activeLinkClasslifeExepectancy = ""
    this.homeService.activeLinkClassfinanceStatus = "";
    this.homeService.actionLinkClassBusinessTab = ""



  }

  lifeExpectancyGraphClick() {
    this.modalWindowShowHide.showCountryCapsule = false;
    this.modalWindowShowHide.showLifeSummery = false;
    this.modalWindowShowHide.showSdgIndicator = false;
    this.modalWindowShowHide.showLifeExpectancyGraph = true;
    this.modalWindowShowHide.showFinanceStatus = false;
    this.modalWindowShowHide.showBusinessTabFlag = false;
    this.homeService.activeLinkClassCountryCapsule = "";
    this.homeService.activeLinkClassLifeSummery = "";
    this.homeService.activeLinkClassSdgIndicator = "";
    this.homeService.activeLinkClasslifeExepectancy = "active"
    this.homeService.activeLinkClassfinanceStatus = "";
    this.homeService.actionLinkClassBusinessTab = ""




  }

  lifeSummeryClick() {
    this.modalWindowShowHide.showLifeSummery = true;
    this.modalWindowShowHide.showCountryCapsule = false;
    this.modalWindowShowHide.showSdgIndicator = false;
    this.modalWindowShowHide.showLifeExpectancyGraph = false;
    this.modalWindowShowHide.showFinanceStatus = false;
    this.modalWindowShowHide.showBusinessTabFlag = false;
    this.homeService.activeLinkClassLifeSummery = "active";
    this.homeService.activeLinkClassCountryCapsule = "";
    this.homeService.activeLinkClassSdgIndicator = "";
    this.homeService.activeLinkClasslifeExepectancy = ""
    this.homeService.activeLinkClassfinanceStatus = "";
    this.homeService.actionLinkClassBusinessTab = ""


  }

  sdgIndicatorClick() {
    this.modalWindowShowHide.showSdgIndicator = true;
    this.modalWindowShowHide.showCountryCapsule = false;
    this.modalWindowShowHide.showLifeSummery = false;
    this.modalWindowShowHide.showLifeExpectancyGraph = false;
    this.modalWindowShowHide.showFinanceStatus = false;
    this.modalWindowShowHide.showBusinessTabFlag = false;
    this.homeService.activeLinkClassSdgIndicator = "active";
    this.homeService.activeLinkClassCountryCapsule = "";
    this.homeService.activeLinkClassLifeSummery = "";
    this.homeService.activeLinkClasslifeExepectancy = "";
    this.homeService.activeLinkClassfinanceStatus = "";
    this.homeService.actionLinkClassBusinessTab = ""

  }


  buasinessStatusClick() {
    this.modalWindowShowHide.showBusinessTabFlag = true;
    this.modalWindowShowHide.showSdgIndicator = false;
    this.modalWindowShowHide.showCountryCapsule = false;
    this.modalWindowShowHide.showLifeSummery = false;
    this.modalWindowShowHide.showLifeExpectancyGraph = false;
    this.modalWindowShowHide.showFinanceStatus = false;
    this.homeService.activeLinkClassfinanceStatus = "";
    this.homeService.activeLinkClassCountryCapsule = "";
    this.homeService.activeLinkClassLifeSummery = "";
    this.homeService.activeLinkClasslifeExepectancy = "";
    this.homeService.activeLinkClassSdgIndicator = "";
    this.homeService.actionLinkClassBusinessTab = "active"
  }

  financeStatusClick() {
    this.modalWindowShowHide.showSdgIndicator = false;
    this.modalWindowShowHide.showBusinessTabFlag = false;
    this.modalWindowShowHide.showCountryCapsule = false;
    this.modalWindowShowHide.showLifeSummery = false;
    this.modalWindowShowHide.showLifeExpectancyGraph = false;
    this.modalWindowShowHide.showFinanceStatus = true;
    this.homeService.activeLinkClassfinanceStatus = "active";
    this.homeService.activeLinkClassCountryCapsule = "";
    this.homeService.activeLinkClassLifeSummery = "";
    this.homeService.activeLinkClasslifeExepectancy = "";
    this.homeService.activeLinkClassSdgIndicator = "";
    this.homeService.actionLinkClassBusinessTab = ""


  }



  actionEducationClick() {

    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age === this.constantService.DEMOLIFEMAXAGE || this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.showSuccessMsgFlag = true;
      let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
      let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
      this.messageText = s1 + s2;
    }
    else {
      this.modalWindowShowHide.showActionEducation = true;
      this.modalWindowShowHide.showActionFinanace = false;
      this.modalWindowShowHide.showActionLeisure = false;
      this.modalWindowShowHide.showActionRelation = false;
      this.modalWindowShowHide.showActionResidence = false;
      this.modalWindowShowHide.showActionCareer = false;
    }
  }

  actionCareerClick() {
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age === this.constantService.DEMOLIFEMAXAGE || this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.showSuccessMsgFlag = true;
      let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
      let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
      this.messageText = s1 + s2;
    }
    else {
      this.modalWindowShowHide.showActionEducation = false;
      this.modalWindowShowHide.showActionFinanace = false;
      this.modalWindowShowHide.showActionLeisure = false;
      this.modalWindowShowHide.showActionRelation = false;
      this.modalWindowShowHide.showActionResidence = false;
      this.modalWindowShowHide.showActionCareer = true;
    }
  }

  actionResidenceClick() {
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age === this.constantService.DEMOLIFEMAXAGE || this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.showSuccessMsgFlag = true;
      let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
      let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
      this.messageText = s1 + s2;
    }
    else {
      this.modalWindowShowHide.showActionEducation = false;
      this.modalWindowShowHide.showActionFinanace = false;
      this.modalWindowShowHide.showActionLeisure = false;
      this.modalWindowShowHide.showActionRelation = false;
      this.modalWindowShowHide.showActionResidence = true;
      this.modalWindowShowHide.showActionCareer = false;
    }
  }

  actionRelationClick() {
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age === this.constantService.DEMOLIFEMAXAGE || this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.showSuccessMsgFlag = true;
      let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
      let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
      this.messageText = s1 + s2;
    }
    else {
      this.modalWindowShowHide.showActionEducation = false;
      this.modalWindowShowHide.showActionFinanace = false;
      this.modalWindowShowHide.showActionLeisure = false;
      this.modalWindowShowHide.showActionRelation = true;
      this.modalWindowShowHide.showActionResidence = false;
      this.modalWindowShowHide.showActionCareer = false;
    }
  }

  actionFinanceClick() {
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age === this.constantService.DEMOLIFEMAXAGE || this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.showSuccessMsgFlag = true;
      let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
      let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
      this.messageText = s1 + s2;
    }
    else {
      this.modalWindowShowHide.showActionEducation = false;
      this.modalWindowShowHide.showActionFinanace = true;
      this.modalWindowShowHide.showActionLeisure = false;
      this.modalWindowShowHide.showActionRelation = false;
      this.modalWindowShowHide.showActionResidence = false;
      this.modalWindowShowHide.showActionCareer = false;
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessId !== 130) {
        this.financeService.selectionFlag = true
      }
    }
  }

  actionLeisureClick() {
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].age === this.constantService.DEMOLIFEMAXAGE || this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) && (this.gameSummeryService.licenceId === null && this.gameSummeryService.maxLifeCount === 1)) {
      this.showSuccessMsgFlag = true;
      let s1 = this.translate.instant('gameLabel.demo6') + "<br>";
      let s2 = this.translate.instant('gameLabel.demo7') + "<br>";
      this.messageText = s1 + s2;
    }
    else {
      this.modalWindowShowHide.showActionEducation = false;
      this.modalWindowShowHide.showActionFinanace = false;
      this.modalWindowShowHide.showActionLeisure = true;
      this.modalWindowShowHide.showActionRelation = false;
      this.modalWindowShowHide.showActionResidence = false;
      this.modalWindowShowHide.showActionCareer = false;
    }
  }


  obituaryClick() {
    this.modalWindowShowHide.showObituary = true;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showLetter = false;
    this.modalWindowShowHide.showBugReport = false;
    this.modalWindowShowHide.showFeddback = false;
  }

  gameSummaryClick() {
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showGameSummary = true;
    this.modalWindowShowHide.showLetter = false;
    this.modalWindowShowHide.showBugReport = false;
    this.modalWindowShowHide.showFeddback = false;
  }

  letterClick() {
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showLetter = true;
    this.modalWindowShowHide.showBugReport = false;
    this.modalWindowShowHide.showFeddback = false;
  }

  bugClick() {
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showLetter = false;
    this.modalWindowShowHide.showBugReport = true;
    this.modalWindowShowHide.showFeddback = false;
  }

  feedBackClick() {
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showLetter = false;
    this.modalWindowShowHide.showBugReport = false;
    this.modalWindowShowHide.showFeddback = true;
  }

  learningToolClick() {
    this.homeService.flagFromDeadScreens = true;
    this.modalWindowShowHide.showCountryLearningTool = true;
    this.gameSummeryService.gameId = this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id;
    // this.router.navigate(['/learningTool'])
  }

  lifeDocClick() {
    if (this.flagForCountryDoc) {
      this.homeService.flagFromDeadScreens = true;
      this.modalWindowShowHide.showDocumentFlag = true;
      this.gameSummeryService.gameId = this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id;
    }
    else {
      this.docInfoFlag = true;

    }

    // this.router.navigate(['/downlodeDoc'])
  }

  liveALifeClick() {
    this.livelifeClick = true;
  }


  liveLifeYes() {
    this.livelifeClick = false;
    this.router.navigate(['/summary']);

  }

  liveLifeNo() {
    this.livelifeClick = false;

  }
  backToSummarryClick() {
    this.goBackFlag = true;
  }

  backToSummarryClickYes() {
    this.goBackFlag = false;
    this.router.navigate(['/summary']);


  }

  backToSummarryClickNo() {
    this.goBackFlag = false;

  }


  clickActionResidence() {
    this.modalWindowShowHide.showActionResidence = true;
  }

  clickActionRelation() {
    this.modalWindowShowHide.showActionRelation = true;
  }

  clickActionFinance(flag) {
    this.modalWindowShowHide.showActionFinanace = true;
    if (!this.carrerService.oldBusinessFlag) {
      this.financeService.selectionFlag = flag
    }
    else {
      this.financeService.selectionFlag = false;
    }
  }

  clickActionEducation() {
    this.modalWindowShowHide.showActionEducation = true;
  }

  clickActionCareer() {
    this.modalWindowShowHide.showActionCareer = true;
  }

  clickActionLeisure() {
    this.modalWindowShowHide.showActionLeisure = true;
  }

  showBornSummary() {
    this.modalWindowShowHide.showBornSummary = true;
  }

  clickToExitButton() {
    this.exitButtonFlag = true;
  }

  goToDashbordYes() {
    this.exitButtonFlag = false;
    this.commonService.close();
  }


  goToDashbordNo() {
    this.exitButtonFlag = false;
  }


  openFullscreen() {
    this.commonService.fullScreen();
  }

  closeFullscreen() {
    this.commonService.removeFullScreen();

  }
  ngOnDestroy() {
    this.subscriber.unsubscribe();
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  successWindowOk() {
    this.showSuccessMsgFlag = false;
  }

  buyNow() {
    this.constantService.removeToken();
    window.location.href = this.commonService.buyNowUrl;
  }

  closeDoc() {
    this.docInfoFlag = false;
  }
}
