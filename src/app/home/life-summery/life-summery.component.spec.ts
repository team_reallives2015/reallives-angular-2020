import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeSummeryComponent } from './life-summery.component';

describe('LifeSummeryComponent', () => {
  let component: LifeSummeryComponent;
  let fixture: ComponentFixture<LifeSummeryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeSummeryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeSummeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
