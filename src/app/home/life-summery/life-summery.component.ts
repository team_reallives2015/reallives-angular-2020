import { Component, OnInit } from '@angular/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { EventService } from '../event/event.service';


@Component({
  selector: 'app-life-summery',
  templateUrl: './life-summery.component.html',
  styleUrls: ['./life-summery.component.css']
})
export class LifeSummeryComponent implements OnInit {

  constructor(public modalWindowShowHide: modalWindowShowHide,
              public eventService :EventService) { }

  ngOnInit(): void {

  }

  goToThatEvent(event){


    if(this.eventService.currentEvent.gameEventId < event.gameEventId){
      for (let i=0;i < this.eventService.allEvents.length;i++)
      {
        if(this.eventService.allEvents[i].gameEventId == event.gameEventId)
        {
            this.eventService.updateCurrentEvent(i);
            if(this.eventService.state=='prevevent') {
              this.eventService.state='prevevent2';
            } else {
              this.eventService.state = 'prevevent';
            }
        }
        
      }
   
    }
    else if(this.eventService.currentEvent.gameEventId > event.gameEventId){
      for (let i=0;i < this.eventService.allEvents.length;i++)
      {
        if(this.eventService.allEvents[i].gameEventId == event.gameEventId)
        {
            this.eventService.updateCurrentEvent(i);
            if(this.eventService.state=='nextevent'){
              this.eventService.state='nextevent2';
            } else {
              this.eventService.state='nextevent';
            }
        }
      
      }
      
  }
 
}
}