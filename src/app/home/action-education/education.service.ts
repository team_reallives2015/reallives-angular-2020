import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';

@Injectable({
  providedIn: 'root'
})
export class EducationService {
body;
  constructor(private http: HttpClient,
              public homeService :HomeService,
              public commonService :CommonService,
              public constantService :ConstantService) { }

getGraduatedFieldOfStudy(lang){
   return this.http.get<Array<Object>>(`${this.commonService.url}game/education/graduateFieldOfStudy/${lang}`);
}


getVocationFieldStudy(lang){
  return this.http.get<Array<Object>>(`${this.commonService.url}game/education/vocationalFieldOfStudy/${lang}`);

}

getCollegeFieldStudy(lang){
  return this.http.get<Array<Object>>(`${this.commonService.url}game/education/collegeFieldOfStudy/${lang}`);

}

updateFieldOfStudy(gameId,personId,updateData,lang){
  return this.http.post<Array<Object>>(`${this.commonService.url}game/education/updateFieldOfStudy/${gameId}/${personId}/${lang}`,{updateData});

}

quitAllSchool(personId,gameId,age,quitData){
  return this.http.post<Array<Object>>(`${this.commonService.url}game/education/quit/${personId}/${gameId}/${age}`, {quitData});
}
}
