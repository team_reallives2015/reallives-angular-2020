import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionEducationComponent } from './action-education.component';

describe('ActionEducationComponent', () => {
  let component: ActionEducationComponent;
  let fixture: ComponentFixture<ActionEducationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionEducationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionEducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
