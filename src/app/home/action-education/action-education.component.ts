import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { interval } from 'rxjs';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { EventService } from '../event/event.service';
import { HomeService } from '../home.service';
import { EducationService } from './education.service';



@Component({
  selector: 'app-action-education',
  templateUrl: './action-education.component.html',
  styleUrls: ['./action-education.component.css']
})
export class ActionEducationComponent implements OnInit {
   allPerson;
   mainPerson;
   fieldStudyList;
  graduatedFieldStudyList:any;
  collegeFieldStudyList:any;
  vocationalFieldStudyList:any;
  enrollFlag:boolean=false;
  fieldOfStudyValue;
  selectedFieldOfStudyId;
  confirmBoxFlag:boolean=false;
  showAlertFlag:boolean=false;
  alertClass;
  alertMsg;
  alertType;
  langCode:string;
  public messageText;
  public messageClass;
  public MessageShow;
  public messageType;
  public msgText;
  public activeClass:string="";
  updateData;
  schoolType:String="";
  displaySchoolType:String="";
  updatedPesonFiledForEnroll;
  updatedPesonFiledForQuit;
  vocationalProgressYearsMax;
  vocationalProgressYears;
 collegeProgressYearsMax;
  collegeProgressYears;
  graduateProgressYearsMax;
  graduateProgressYears;
  quitData;
  quitFlag:boolean=false;
  collegefieldOfStudyName;
  graduatefieldOfStudyName;
  vocationalfieldOfStudyName;
  completedremain;
  checkMoveForward;
  schoolProgressYears;
  schoolMaximunYear;
  per;
  collegeEnrollButtonDisable:boolean=false;
  graduateEnrollButtonDisable:boolean=false;
  vocationalEnrollButtonDisable:boolean=false;
  reSelectFieldOfStudy:boolean=false;
  currentStudy;
  fieldOfStudyAllReadySelectedFlag:boolean=false;
  selectedFireldOfStudyName;
  result;
  diiferenBetIdentity;
  identity=[];
  identityValues=[];
  headingActiveClassForSchool="";
  headingActiveClassForCollege="";
  headingActiveClassForGraduate="";
  headingActiveClassForVocational="";



  constructor(public modalWindowShowHide: modalWindowShowHide,
             private educationService : EducationService,
             public homeService :HomeService,
             public constantService :ConstantService,
             public agaAYearService :AgaAYearService,
             public eventService :EventService,
             public translation:TranslateService,
             public translationService:TranslationService) 
             { 
              interval(30000).subscribe(x => {
                if(this.showAlertFlag){
                  this.showAlertFlag=false;
                }
                });
             }

  ngOnInit(): void {
    this.allPerson=this.homeService.allPerson;
    this.allPerson[this.constantService.FAMILY_SELF]=this.homeService.allPerson[this.constantService.FAMILY_SELF];
    if(this.homeService.allPerson[this.constantService.FAMILY_SELF].dead){
      // this.alertMsg = "You cannot take any action because you are dead";
      this.alertMsg=this.translation.instant('gameLabel.You_cannot_take_any_action_because_you_are_dead');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    if(this.homeService.allPerson[this.constantService.FAMILY_SELF].age<6&& !(this.homeService.allPerson[this.constantService.FAMILY_SELF].dead)){
      // this.alertMsg = "You cannot start School before age 6.";
      this.alertMsg=this.translation.instant('gameLabel.ageWarrning');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
      this.setSchoolData();
      this.setVocationalData();
     this.setCollegeData();
    this.setGraduateData();
    this.getFieldNameForCollegeGraduateAndVocational();
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.inSchool === true) {
      this.headingActiveClassForSchool = "dummy";
    }
    else {
      this.headingActiveClassForSchool="";
    }

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.inCollege === true) {
      this.headingActiveClassForCollege = "dummy";
    }
    else {
      this.headingActiveClassForCollege="";
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.inVocational === true) {
      this.headingActiveClassForVocational="dummy";
    }
    else {
      this.headingActiveClassForVocational="";

    }

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.inGraduate === true) {
      this.headingActiveClassForGraduate="dummy";

    }
    else {
      this.headingActiveClassForGraduate="";

    }

         
  }

  private setSchoolData() {
  this.schoolMaximunYear=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.schoolMaximumYears;
    let completed: number;
    let yearsback: number;
    let yearsforward:number;

  this.completedremain=(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.schoolMaximumYears+this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.yearsHeldBack)-( this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.schoolCompletedYears+this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.yearsMovedForward);
  yearsback=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.yearsHeldBack;
  yearsforward = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.yearsMovedForward;
  completed = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.schoolCompletedYears;
    this.checkMoveForward = completed+yearsforward;
    this.schoolProgressYears = (this.checkMoveForward);

  if(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.school.highSchoolGraduate)
  {
    this.schoolProgressYears=this.schoolMaximunYear;
  }

}

  private setCollegeData() {
    this.collegeProgressYearsMax=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeMaximumYears;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeCompletedYears >this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeMaximumYears) {
      this.collegeProgressYears=this.collegeProgressYearsMax;
    }
    else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeCompletedYears < this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeMaximumYears) {
     if(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeCompletedYears==1){
      this.collegeProgressYears=1;

     }
     else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeCompletedYears==2){
      this.collegeProgressYears=2;

     }
     else{
      this.collegeProgressYears=0;
     }
    }


  }
    private setGraduateData() {
       this.graduateProgressYearsMax=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateMaximumYears;

      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateCompletedYears >this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateMaximumYears) {
        this.graduateProgressYearsMax=this.graduateProgressYearsMax;
      }
      else  if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateCompletedYears <this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateMaximumYears) {
        if(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateCompletedYears==1){
          this.graduateProgressYears=1;
    
         }
         else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateCompletedYears==2){
          this.graduateProgressYears=2;
    
         }
         else{
          this.graduateProgressYears=0;
         }
      }
    }

  private setVocationalData() {
    this.vocationalProgressYearsMax=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalMaximumYears
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalCompletedYears > this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalMaximumYears) {
      this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalCompletedYears = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalMaximumYears;
      this.vocationalProgressYears=0.5;
      
    }
  else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalCompletedYears < this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalMaximumYears) {

      this.vocationalProgressYears=0;
    }
    else if( this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalCompletedYears = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalMaximumYears){
      this.vocationalProgressYears=1;

    }

  }

  private getFieldNameForCollegeGraduateAndVocational()
  {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeGraduate) {
         this.collegefieldOfStudyName =this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.fieldName;
    }

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateDegree) {
      this.graduatefieldOfStudyName = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.fieldName;
    }


    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalDegree)
    {
      this.vocationalfieldOfStudyName = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.fieldName;
    }
  }

  enrollVocationalSchool(){
    this.fieldOfStudyAllReadySelectedFlag=true;
    this.graduateEnrollButtonDisable = true;
    this.selectedFieldOfStudyId = null;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalFieldOfStudyId != 0
      && this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalDropOut === false 
      || this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalCompletedYears != 0) {
      // this.messageText="You are not eligible to choose different field of study. We will continue with perious field of study";
      this.messageText=this.translation.instant('gameLabel.elligibalWarrningSentense');
      this.reSelectFieldOfStudy=true;
      this.schoolType = "VOCATIONAL";
      this.currentStudy = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.vocationalFieldOfStudyId;
      this.selectedFireldOfStudyName=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.vocational.fieldName;
    } 
    else{
      this.fieldOfStudyAllReadySelectedFlag=false;
      this.educationService.getVocationFieldStudy(this.translationService.selectedLang.code).subscribe(
     vocational =>{
      this.vocationalFieldStudyList=vocational;
       this.fieldStudyList=this.vocationalFieldStudyList;
       this.schoolType="VOCATIONAL"
       this.displaySchoolType=this.translation.instant('gameLabel.vocational_school');
       this.enrollFlag=true;
               })
    }
  }

  enrollGraduate(){
    this.fieldOfStudyAllReadySelectedFlag=true;
    this.graduateEnrollButtonDisable = true;
    this.selectedFieldOfStudyId = null;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateFieldOfStudyId != 0
      && this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateDropOut === false
      || this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateCompletedYears != 0) {
      // this.messageText="You are not eligible to choose different field of study. We will continue with perious field of study";
      this.messageText=this.translation.instant('gameLabel.elligibalWarrningSentense');
      this.reSelectFieldOfStudy=true;
      this.schoolType = "GRADUATE";
      this.currentStudy = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.graduateFieldOfStudyId;
      this.selectedFireldOfStudyName=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.graduate.fieldName;
    } else {
      this.fieldOfStudyAllReadySelectedFlag=false;
      this.educationService.getGraduatedFieldOfStudy(this.translationService.selectedLang.code).subscribe(
        graduate =>{
          this.graduatedFieldStudyList=graduate;
    this.fieldStudyList=this.graduatedFieldStudyList;
    this.schoolType="GRADUATE"
    this.displaySchoolType=this.translation.instant('gameLabel.graduation_studies');
    this.enrollFlag=true; 
   })
  
  }
}
  
  enrollCollege(){
    this.fieldOfStudyAllReadySelectedFlag=true;
    this.collegeEnrollButtonDisable = true;
    this.selectedFieldOfStudyId = null;
    if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeFieldOfStudyId != 0)
      && this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeDropOut === false 
      || this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeCompletedYears != 0) {
      // this.messageText="You are not eligible to choose different field of study. We will continue with perious field of study";
      this.messageText=this.translation.instant('gameLabel.elligibalWarrningSentense');
      this.reSelectFieldOfStudy=true;
      this.schoolType = "COLLEGE";
      this.currentStudy = this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.collegeFieldOfStudyId;
      this.selectedFireldOfStudyName=this.homeService.allPerson[this.constantService.FAMILY_SELF].education.college.fieldName;

    }
    else{
    this.fieldOfStudyAllReadySelectedFlag=false;
        this.educationService.getCollegeFieldStudy(this.translationService.selectedLang.code).subscribe(
         college =>{
           this.collegeFieldStudyList=college;
    this.fieldStudyList=this.collegeFieldStudyList;
    this.schoolType="COLLEGE"
    this.displaySchoolType=this.translation.instant('gameLabel.college');
    this.enrollFlag=true; 
    })
   }
  }

   okClick(){
    this.reSelectFieldOfStudy=false;
    this.selectFieldOfStudyYesClick();

   }

  closeFieldOfStudyWindow(){
    this.enrollFlag=false;
  }

  allRadioButtonValue(event:any){
   this.schoolType=event.target.name;
   this.fieldOfStudyValue=event.target.value;
   this.confirmBoxFlag=true;
  if(this.schoolType==="VOCATIONAL"){
    this.displaySchoolType=this.translation.instant('gameLabel.vocational_school');
   this.translation.get('gameLabel.Do_you_really_want_to_apply_for_studying' , { subject: this.vocationalFieldStudyList[this.fieldOfStudyValue].EnrollName,educationType :this.displaySchoolType})
   .subscribe((s: string)=>{
    this.messageText=s;
  })
  }
  else  if(this.schoolType==="COLLEGE"){
    this.displaySchoolType=this.translation.instant('gameLabel.college');
    this.translation.get('gameLabel.Do_you_really_want_to_apply_for_studying' , { subject: this.collegeFieldStudyList[this.fieldOfStudyValue].EnrollName,educationType :this.displaySchoolType})
      .subscribe((s: string)=>{
        this.messageText=s;
      })


  }
  else  if(this.schoolType==="GRADUATE"){
    this.displaySchoolType=this.translation.instant('gameLabel.graduation_studies');
    this.translation.get('gameLabel.Do_you_really_want_to_apply_for_studying' , { subject: this.graduatedFieldStudyList[this.fieldOfStudyValue].EnrollName,educationType :this.displaySchoolType})
    .subscribe((s: string)=>{
      this.messageText=s;
    })
  }
  }

  selectFieldOfStudyYesClick(){
    this.modalWindowShowHide.showWaitScreen=true;
    this.confirmBoxFlag=false;
    this.enrollFlag=false;
    if(!this.fieldOfStudyAllReadySelectedFlag){
    if(this.schoolType==="VOCATIONAL"){
      this.selectedFieldOfStudyId=this.vocationalFieldStudyList[this.fieldOfStudyValue].EnrollId;
      this.selectedFireldOfStudyName=this.vocationalFieldStudyList[this.fieldOfStudyValue].EnrollName;
    }
    else  if(this.schoolType==="COLLEGE"){
      this.selectedFieldOfStudyId=this.collegeFieldStudyList[this.fieldOfStudyValue].EnrollId;
      this.selectedFireldOfStudyName=this.collegeFieldStudyList[this.fieldOfStudyValue].EnrollName;

    }
    else  if(this.schoolType==="GRADUATE"){
      this.selectedFieldOfStudyId=this.graduatedFieldStudyList[this.fieldOfStudyValue].EnrollId;
      this.selectedFireldOfStudyName=this.graduatedFieldStudyList[this.fieldOfStudyValue].EnrollName;


    }
  }
  else if(this.fieldOfStudyAllReadySelectedFlag){
    this.selectedFieldOfStudyId=this.currentStudy;
  }
    
    this.updateData={"type":this.schoolType,"fieldOfStudyId":this.selectedFieldOfStudyId,"fieldName":this.selectedFireldOfStudyName,"langCode":this.langCode};
    this.educationService.updateFieldOfStudy(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,this.updateData,this.translationService.selectedLang.code).subscribe(
      res=>{
        this.result=res;
        this.diiferenBetIdentity=this.result.data.result.difference;
      let returnValue=this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity,this.homeService.allPerson);
      if(returnValue!=='false'){
        this.homeService.allPerson=returnValue;
           this.homeService.changeAllPerson.emit(this.homeService.allPerson);
          
      }
      this.agaAYearService.nextEventResultIsEvent(this.result);
      this.eventService.enableActionAsPerEvent(this.eventService.currentEvent,this.homeService.allPerson[this.constantService.FAMILY_SELF]);
      if(this.schoolType==="VOCATIONAL"){
        this.headingActiveClassForVocational="dummy";
      }
      else  if(this.schoolType==="COLLEGE"){
        this.headingActiveClassForCollege="dummy";  
      }
      else  if(this.schoolType==="GRADUATE"){
        this.headingActiveClassForGraduate="dummy";  
      }
        // this.updatedPesonFiledForEnroll=res;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].education=this.updatedPesonFiledForEnroll.education;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].expense=this.updatedPesonFiledForEnroll.expense;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].income=this.updatedPesonFiledForEnroll.income;
        // this.homeService.changeSelfPerson.emit(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
        this.modalWindowShowHide.showWaitScreen=false;
        this.selectedFireldOfStudyName="";
      })
  }


  quitVocational(type){
    this.schoolType=type;
    // this.messageText="Do you really want to quit vocational?"
    this.messageText=this.translation.instant('gameLabel.Do_you_really_want_to_quit')+this.translation.instant('gameLabel.vocational_school')+this.translation.instant('gameLabel.?');
    this.quitFlag=true;
  }

  quitCollege(type){
    this.schoolType=type;
   // this.messageText="Do you really want to quit college?"
    this.messageText=this.translation.instant('gameLabel.Do_you_really_want_to_quit')+" "+this.translation.instant('gameLabel.college')+" "+this.translation.instant('gameLabel.?');
    this.quitFlag=true;
  }


  quitGraduate(type){
    this.schoolType=type;
    // this.messageText="Do you really want to quit graduate?"
    this.messageText=this.translation.instant('gameLabel.Do_you_really_want_to_quit')+" "+this.translation.instant('gameLabel.graduation_studies')+this.translation.instant('gameLabel.?');
    this.quitFlag=true;
  }

  quitSchool(type){
    this.schoolType=type;
    // this.messageText="Do you really want to quit school?"
    this.messageText=this.translation.instant('gameLabel.Do_you_really_want_to_quit')+" "+this.translation.instant('gameLabel.school')+this.translation.instant('gameLabel.?');
    this.quitFlag=true;
  }


  

  selectQuitNo(){
  this.quitFlag=false;

  }


  selectQuitYes(){
    this.modalWindowShowHide.showWaitScreen=true;
    this.quitData={type:this.schoolType,action:"QUIT"}
    this.educationService.quitAllSchool(this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,this.homeService.allPerson[this.constantService.FAMILY_SELF].age,this.quitData).subscribe(
      res=>{
        this.result=res;
        this.diiferenBetIdentity=this.result.data.result.difference;
      let returnValue=this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity,this.homeService.allPerson);
      if(returnValue!=='false'){
        this.homeService.allPerson=returnValue;
           this.homeService.changeAllPerson.emit(this.homeService.allPerson);
          
      }
      this.agaAYearService.nextEventResultIsEvent(this.result);
      this.eventService.enableActionAsPerEvent(this.eventService.currentEvent,this.homeService.allPerson[this.constantService.FAMILY_SELF]);
      if(this.schoolType==="VOCATIONAL"){
        this.headingActiveClassForVocational="";
      }
      else  if(this.schoolType==="COLLEGE"){
        this.headingActiveClassForCollege="";  
      }
      else  if(this.schoolType==="GRADUATE"){
        this.headingActiveClassForGraduate="";  
      }
        // this.updatedPesonFiledForQuit=res;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].education=this.updatedPesonFiledForQuit.education;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].expense=this.updatedPesonFiledForQuit.expense;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].income=this.updatedPesonFiledForQuit.income;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].job=this.updatedPesonFiledForQuit.job;
        // this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career=this.updatedPesonFiledForQuit.my_career;
        // this.homeService.changeSelfPerson.emit(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
        this.quitFlag=false;
        this.modalWindowShowHide.showWaitScreen=false;

      }
    )
  }

  selectFieldOfStudyNoClick(){
    this.confirmBoxFlag=false;
    this.enrollFlag=false;

  }


  closeClick(){
    this.modalWindowShowHide.showActionEducation=false;
  }

  closeAlert(){
    this.showAlertFlag=false;
  }
}
