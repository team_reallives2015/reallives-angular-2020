import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { EventService } from '../event/event.service';
import { HomeService } from '../home.service';
import { WellBeingIndicatorService } from './well-being-indicator.service';

@Component({
  selector: 'app-well-being-indicator',
  templateUrl: './well-being-indicator.component.html',
  styleUrls: ['./well-being-indicator.component.css']
})
export class WellBeingIndicatorComponent implements OnInit {

  windowFlag:boolean=false;
  constructor(public wellBeingIndicatorService :WellBeingIndicatorService,
             public modalWindowShowHide : modalWindowShowHide,
             public translate:TranslateService,
             public homeService:HomeService,
             public eventService:EventService) { }

  ngOnInit(): void {
    this.wellBeingIndicatorService.generateWellBeingIndicator(this.eventService.currentEvent);
  }

ShowIndicatorModalWindow(key){
this.modalWindowShowHide.showWellBeingIndicatorWindow=true;
this.wellBeingIndicatorService.selectedIndicator=key;
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.HEALTH')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.RESISTANCE')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.HAPPINESS')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.INTELLIGENCE')
  this.wellBeingIndicatorService.dispalyName= this.translate.instant('gameLabel.ARTISTIC')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.MUSICAL')
  this.wellBeingIndicatorService.dispalyName= this.translate.instant('gameLabel.ATHLETIC')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.STRENGTH')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.ENDURANCE')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.SPIRITUAL')
  this.wellBeingIndicatorService.dispalyName=this.translate.instant('gameLabel.WISDOM')

    }

}
