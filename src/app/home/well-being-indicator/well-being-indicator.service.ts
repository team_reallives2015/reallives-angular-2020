import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class WellBeingIndicatorService {
  selectedIndicator;
  dispalyName
  green = "#34db00";
  red = "#d32d53";
  blue = "#081d32";
  wellbeingIndicatorArray = []
  constructor(public translate: TranslateService) { }


  initializeArray() {
    this.wellbeingIndicatorArray = [
      {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.HEALTH'),
        "key": "Health",
        "tooltipplcament": "right",
        "discription": this.translate.instant('gameLabel.Health_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      },
      {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.RESISTANCE'),
        "key": "Resistance",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Resistance_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.HAPPINESS'),
        "key": "Happiness",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Happiness_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.INTELLIGENCE'),
        "key": "Intelligence",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Intelligence_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.ARTISTIC'),
        "key": "Artistic",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Artistic_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.MUSICAL'),
        "key": "Musical",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Musical_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.ATHLETIC'),
        "key": "Athletic",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Athletic_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.STRENGTH'),
        "key": "Strength",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Strength_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.ENDURANCE'),
        "key": "physicalEndurance",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Endurance_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }, {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.SPIRITUAL'),
        "key": "Spiritual",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Spiritual_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      },
      {
        "old": 0,
        "new1": 0,
        "title": this.translate.instant('gameLabel.WISDOM'),
        "key": "Wisdom",
        "tooltipplcament": "top",
        "discription": this.translate.instant('gameLabel.Wisdom_info'),
        "classForBorder": "progress_wraper",
        "classForNumber": ""
      }
    ];
  }

  generateWellBeingIndicator(event) {
    this.initializeArray();
    this.wellbeingIndicatorArray[0].old = event.oldTraits.health;
    this.wellbeingIndicatorArray[0].new1 = event.newTraits.health;
    if (event.newTraits.health < 35) {
      this.wellbeingIndicatorArray[0].classForBorder = "progress_wraper red_brder"
    }
    else {
      this.wellbeingIndicatorArray[0].classForBorder = "progress_wraper green_brder"
    }


    if (this.wellbeingIndicatorArray[0].new1 < this.wellbeingIndicatorArray[0].old) {
      this.wellbeingIndicatorArray[0].classForNumber = "text-red"
    }
    else if (this.wellbeingIndicatorArray[0].new1 > this.wellbeingIndicatorArray[0].old) {
      this.wellbeingIndicatorArray[0].classForNumber = "text-green"
    }

    this.wellbeingIndicatorArray[1].old = event.oldTraits.resistance;
    this.wellbeingIndicatorArray[1].new1 = event.newTraits.resistance;

    if (this.wellbeingIndicatorArray[1].new1 < this.wellbeingIndicatorArray[1].old) {
      this.wellbeingIndicatorArray[1].classForNumber = "text-red"
    }
    else if (this.wellbeingIndicatorArray[1].new1 > this.wellbeingIndicatorArray[1].old) {
      this.wellbeingIndicatorArray[1].classForNumber = "text-green"
    }

    if (event.newTraits.resistance < 35) {
      this.wellbeingIndicatorArray[1].classForBorder = "progress_wraper red_brder"
    }
    else {
      this.wellbeingIndicatorArray[1].classForBorder = "progress_wraper green_brder"
    }


    if (this.wellbeingIndicatorArray[1].new1 < this.wellbeingIndicatorArray[1].old) {
      this.wellbeingIndicatorArray[1].classForNumber = "text-red"
    }
    else if (this.wellbeingIndicatorArray[1].new1 > this.wellbeingIndicatorArray[1].old) {
      this.wellbeingIndicatorArray[1].classForNumber = "text-green"
    }


    this.wellbeingIndicatorArray[2].old = event.oldTraits.happiness;
    this.wellbeingIndicatorArray[2].new1 = event.newTraits.happiness;

    if (event.newTraits.happiness < 35) {
      this.wellbeingIndicatorArray[2].classForBorder = "progress_wraper red_brder"
    }
    else {
      this.wellbeingIndicatorArray[2].classForBorder = "progress_wraper green_brder"
    }

    if (this.wellbeingIndicatorArray[2].new1 < this.wellbeingIndicatorArray[2].old) {
      this.wellbeingIndicatorArray[2].classForNumber = "text-red"
    }
    else if (this.wellbeingIndicatorArray[2].new1 > this.wellbeingIndicatorArray[2].old) {
      this.wellbeingIndicatorArray[2].classForNumber = "text-green"
    }


    this.wellbeingIndicatorArray[3].old = event.oldTraits.intelligence;
    this.wellbeingIndicatorArray[3].new1 = event.newTraits.intelligence;

    if (this.wellbeingIndicatorArray[3].new1 < this.wellbeingIndicatorArray[3].old) {
      this.wellbeingIndicatorArray[3].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[3].new1 > this.wellbeingIndicatorArray[3].old) {
      this.wellbeingIndicatorArray[3].classForBorder = "progress_wraper green_brder"
    }




    this.wellbeingIndicatorArray[4].old = event.oldTraits.artistic;
    this.wellbeingIndicatorArray[4].new1 = event.newTraits.artistic;

    if (this.wellbeingIndicatorArray[4].new1 < this.wellbeingIndicatorArray[4].old) {
      this.wellbeingIndicatorArray[4].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[4].new1 > this.wellbeingIndicatorArray[4].old) {
      this.wellbeingIndicatorArray[4].classForBorder = "progress_wraper green_brder"
    }


    this.wellbeingIndicatorArray[5].old = event.oldTraits.musical;
    this.wellbeingIndicatorArray[5].new1 = event.newTraits.musical;

    if (this.wellbeingIndicatorArray[5].new1 < this.wellbeingIndicatorArray[5].old) {
      this.wellbeingIndicatorArray[5].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[5].new1 > this.wellbeingIndicatorArray[5].old) {
      this.wellbeingIndicatorArray[5].classForBorder = "progress_wraper green_brder"
    }

    this.wellbeingIndicatorArray[6].old = event.oldTraits.athletic;
    this.wellbeingIndicatorArray[6].new1 = event.newTraits.athletic;
    if (this.wellbeingIndicatorArray[6].new1 < this.wellbeingIndicatorArray[6].old) {
      this.wellbeingIndicatorArray[6].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[6].new1 > this.wellbeingIndicatorArray[6].old) {
      this.wellbeingIndicatorArray[6].classForBorder = "progress_wraper green_brder"
    }

    this.wellbeingIndicatorArray[7].old = event.oldTraits.strength;
    this.wellbeingIndicatorArray[7].new1 = event.newTraits.strength;

    if (this.wellbeingIndicatorArray[7].new1 < this.wellbeingIndicatorArray[7].old) {
      this.wellbeingIndicatorArray[7].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[7].new1 > this.wellbeingIndicatorArray[7].old) {
      this.wellbeingIndicatorArray[7].classForBorder = "progress_wraper green_brder"
    }



    this.wellbeingIndicatorArray[8].old = event.oldTraits.physicalEndurance;
    this.wellbeingIndicatorArray[8].new1 = event.newTraits.physicalEndurance;
    if (this.wellbeingIndicatorArray[8].new1 < this.wellbeingIndicatorArray[8].old) {
      this.wellbeingIndicatorArray[8].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[8].new1 > this.wellbeingIndicatorArray[8].old) {
      this.wellbeingIndicatorArray[8].classForBorder = "progress_wraper green_brder"
    }

    this.wellbeingIndicatorArray[9].old = event.oldTraits.spiritual;
    this.wellbeingIndicatorArray[9].new1 = event.newTraits.spiritual;
    if (this.wellbeingIndicatorArray[9].new1 < this.wellbeingIndicatorArray[9].old) {
      this.wellbeingIndicatorArray[9].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[9].new1 > this.wellbeingIndicatorArray[9].old) {
      this.wellbeingIndicatorArray[9].classForBorder = "progress_wraper green_brder"
    }

    this.wellbeingIndicatorArray[10].old = event.oldTraits.wisdom;
    this.wellbeingIndicatorArray[10].new1 = event.newTraits.wisdom;

    if (this.wellbeingIndicatorArray[10].new1 < this.wellbeingIndicatorArray[10].old) {
      this.wellbeingIndicatorArray[10].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellbeingIndicatorArray[10].new1 > this.wellbeingIndicatorArray[10].old) {
      this.wellbeingIndicatorArray[10].classForBorder = "progress_wraper green_brder"
    }
  }
}
