import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WellBeingIndicatorComponent } from './well-being-indicator.component';

describe('WellBeingIndicatorComponent', () => {
  let component: WellBeingIndicatorComponent;
  let fixture: ComponentFixture<WellBeingIndicatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WellBeingIndicatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WellBeingIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
