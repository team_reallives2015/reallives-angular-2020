import { TestBed } from '@angular/core/testing';

import { WellBeingIndicatorService } from './well-being-indicator.service';

describe('WellBeingIndicatorService', () => {
  let service: WellBeingIndicatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WellBeingIndicatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
