import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';


@Injectable({
  providedIn: 'root'
})
export class CountryCapsuleService {

  private appId: string;
    private appCode: string;

    public weather: any;
  constructor(private http :HttpClient,
              private commonService :CommonService,
              private constantService :ConstantService) { 
                this.appId = "APP-ID-HERE";
        this.appCode = "APP-CODE-HERE";
        this.weather = [];
              }

getAllCountriesList(gameId){
  return this.http.get<Array<Object>>(`${this.commonService.url}game/residence/getAllCountries/${gameId}`);

}

getCurrentTimeStamp(lat,long,timestamp)
{
  return this.http.get('https://maps.googleapis.com/maps/api/timezone/json?location='+lat+','+long+'&timestamp='+timestamp+'&key=AIzaSyBWgKrffZyYKsciFmx0xf9DwKDZMacosxI',{ headers: { 'Anonymous': '' } });
}

getWeatherCondition(city,countryCode)
  {
    return this.http.get('https://api.openweathermap.org/data/2.5/weather?q='+city+','+countryCode+'&appid='+this.constantService.TempratureAppId,{ headers: { 'Anonymous': '' } });


  }
  
  
  // getAllPersonCountries(realivesGameId)
  // {

  //   return this.httpWithAuthorizationService.httpGet(this.constantService.URL + "/personCountrieslist/"+realivesGameId)
  //     .map(this.constantService.extractData)
  //     .catch(this.constantService.handleError);
  // }

 
}
