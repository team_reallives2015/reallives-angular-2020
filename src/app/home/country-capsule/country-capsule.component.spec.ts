import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCapsuleComponent } from './country-capsule.component';

describe('CountryCapsuleComponent', () => {
  let component: CountryCapsuleComponent;
  let fixture: ComponentFixture<CountryCapsuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryCapsuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCapsuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
