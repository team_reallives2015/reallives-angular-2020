import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { CountryCapsuleService } from './country-capsule.service';
declare var require: any





@Component({
  selector: 'app-country-capsule',
  templateUrl: './country-capsule.component.html',
  styles: [`
  .sebm-google-map-container {
    height: 530px;
    width: 100%;
  }

  .map-container {
    height: 195px;
    width: 100%;
  }

  .world-container {
    height: 120px;
    width: 100%;
  }

`]
})

export class CountryCapsuleComponent implements OnInit {
  @Input() data: boolean = false;
  allPerson;
  mainPerson;
  checkFormat;
  readMoreFlag: boolean = false;
  longitude;
  latitude;
  mapZoom = 3;
  showMarkerInfo: boolean = false;
  markers = [];
  cityName;
  displayCapital: boolean;
  displayCity: boolean;
  clock: any;
  clock1: any;
  timezonedata1: any;
  private weatherData: any;
  private currentCondition: any;
  temprature: any;
  registertemprature: any;
  public lifeExpectancy: number;
  public registerLifeExpectancy: number;
  private firstCountryName: any;
  timezonedata;
  selfTimer;
  registerTimer;
  allCountries = [];
  currentCountry;
  checkEmgrationFlag: boolean = false;
  govermentSocialServices;
  changedCounty;
  countryname;
  currency_Name;
  healthServicesUrban;
  healthServicesRural;
  terrorismRank;
  terrorismScore;
  peaceRank;
  peaceScore;
  Continents;
  Population;
  Capital;
  birthRate;
  primarySchool;
  currencyPlural;
  politicalRight;
  corruption;
  exchangeRate;
  currency_code;
  activeClassHealth = 'show active';
  activeClassSocial = '';
  activeClassPolitical = '';
  activeClassEconomical = '';
  activeClassHealthForTab = 'active';
  activeClassSocialForTab = '';
  activeClassPoliticalForTab = '';
  activeClassEcomnomicalForTab = '';
  showPoliticFlag: boolean = true;
  showSocialFlag: boolean = true;
  showHelathFlag: boolean = true;
  showEconomicalFlag: boolean = true;
  dispaly;
  myUrl;
  displayMap = true;
  displayWiki = false;
  displayLonly = false;
  checkFlag = true;
  dispalyEncyclopedia = false;
  dispalyNationalGeographic = false;
  displayUnesco = false;
  dispalyClimate: boolean = false;
  disaplayHealth: boolean = false;
  dispalyHdr: boolean = false;
  climateClassForTab;
  healthClassForTab;
  hdrClassForTab
  mapClassForTab = "active";
  lonlyClassForTab;
  wikiClassForTab;
  cityclassForTab;
  capitalclassForTab;
  encyclopediaClassForTab;
  nationalClassForTab
  unescoClassForTab
  wikiUrl;
  classvalue;
  sdgUrl = "https://dashboards.sdgindex.org/profiles/"
  ciaUrl = "https://www.cia.gov/the-world-factbook/countries/";
  encyclopediaUrl = "https://www.britannica.com/place/";
  nationalGeographicUrl = "https://kids.nationalgeographic.com/geography/countries/article/";
  clicmateUrl = "https://en.wikipedia.org/wiki/Category:Ethnic_groups_in_";
  helathUrl = "https://data.unicef.org/country/";
  moment = require('moment-timezone');
  moment1 = require('moment-timezone');
  hdrUrl = "https://hdr.undp.org/en/countries/profiles/"
  sdgCountry;
  countryId;
  cityLink;
  registerCountryCode;
  registerCountryCurrentcyName;
  private subscription;
  usCountryName
  registerCountryExchangeRate
  registerCountryName
  currencyCode
  countryName
  countryShort;
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public homeService: HomeService,
    public commonService: CommonService,
    public constantService: ConstantService,
    public countryCapsuleService: CountryCapsuleService,
    private sanitizer: DomSanitizer) {
    this.subscription = this.homeService.changeAllPerson.subscribe(data => {
      this.homeService.allPerson = data;
      this.allPerson = this.homeService.allPerson;
      this.mainPerson = this.homeService.allPerson[this.constantService.FAMILY_SELF];
      this.currentCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country;
      this.getCountryList(this.mainPerson.game_id);
    })
  }

  ngOnInit(): void {
    this.wikiUrl = "https://en.wikipedia.org/wiki/";
    this.allPerson = this.homeService.allPerson;
    this.mainPerson = this.homeService.allPerson[this.constantService.FAMILY_SELF];
    this.currentCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country;
    this.latitude = this.homeService.allPerson[this.constantService.FAMILY_SELF].city.latitude
    this.longitude = this.homeService.allPerson[this.constantService.FAMILY_SELF].city.longitude
    this.cityName = this.homeService.allPerson[this.constantService.FAMILY_SELF].city.cityName;
    this.cityLink = "https://en.wikipedia.org/wiki/";
    // this.getCountryList(this.mainPerson.game_id);  
    this.registerCountryCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.currency_code;
    this.registerCountryCurrentcyName = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural;
    this.usCountryName = this.constantService.US_COUNTRYNAME;
    this.registerCountryExchangeRate = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.registerCountryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    this.currencyCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code;
    this.countryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country;

    this.countryCapsuleService.getAllCountriesList(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.allCountries = res;
        if (this.allCountries.length > 1) {
          this.checkEmgrationFlag = true;
          let mother = this.homeService.allPerson[this.constantService.FAMILY_SELF].mother;
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country === this.homeService.allPerson[mother].country.country) {
            this.currentCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country;
            //  this.checkEmgrationFlag=false
          }
          //  else{
          //    this.checkEmgrationFlag=true;
          //  }
        }
        else {
          this.checkEmgrationFlag = false;
        }
        for (let i = 0; i < this.allCountries.length; i++) {
          if (this.allCountries[i].country == this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country) {
            this.currentCountry = this.allCountries[i];
            this.currentCountry.cityName = this.cityName;
            this.currentCountry.cityWebsiteLink = this.cityLink;
            this.currentCountry.cityLatitude = this.latitude;
            this.currentCountry.cityLongitude = this.longitude;
            this.calculateGovermentSocialServices(this.currentCountry);
            this.changeMarkerData(this.currentCountry);
            this.getLifeExpectancyData();
            this.getCurrentCountryTemprature();
            this.getRegisterCountryTemprature();
            // this.displayTime(this.currentCountry);
            // this.displayRegisterTime(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country);

          }
        }
      }
    )

  }
  changeMarkerData(changedCounty) {
    this.latitude = changedCounty.cityLatitude
    this.longitude = changedCounty.cityLongitude
    this.cityName = changedCounty.cityName;

  }


  getCountryList(gameId) {
    this.countryCapsuleService.getAllCountriesList(gameId).subscribe(
      res => {
        this.allCountries = res;
        if (this.allCountries.length > 1) {
          let mother = this.homeService.allPerson[this.constantService.FAMILY_SELF].mother;
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country === this.homeService.allPerson[mother].country.country) {
            this.currentCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country;
            this.checkEmgrationFlag = false
          }
          else {
            this.checkEmgrationFlag = true;
          }
        }
        for (let i = 0; i < this.allCountries.length; i++) {
          if (this.allCountries[i].country == this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country) {
            this.currentCountry = this.allCountries[i];
            this.currentCountry.cityName = this.cityName;
            this.currentCountry.cityWebsiteLink = this.cityLink;
            this.currentCountry.cityLatitude = this.latitude;
            this.currentCountry.cityLongitude = this.longitude;
            this.calculateGovermentSocialServices(this.currentCountry);
            this.changeMarkerData(this.currentCountry);
            this.getLifeExpectancyData();
            this.getCurrentCountryTemprature();
            this.getRegisterCountryTemprature();
            // this.displayTime(this.currentCountry);
            // this.displayRegisterTime(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country);

          }
        }
      }
    )
  }


  selected(current) {
    this.currentCountry = current;
    this.changeMarkerData(this.currentCountry);
  }


  displayTime(country) {
    let newvalue: number = new Date().getTime() / 1000;
    this.countryCapsuleService.getCurrentTimeStamp(country.lat, country.lng, newvalue).subscribe((r: any) => {
      this.timezonedata = r.timeZoneId;
    });
    setInterval(() => {
      this.selfTimer = this.moment.tz(Date.now(), this.timezonedata);
    }, 1000); // set it every one seconds}
  }


  displayRegisterTime(country) {
    let newvalue: number = new Date().getTime() / 1000;

    this.countryCapsuleService.getCurrentTimeStamp(country.lat, country.lng, newvalue).subscribe((r: any) => {
      this.timezonedata1 = r.timeZoneId;
    });

    setInterval(() => {
      this.registerTimer = this.moment1.tz(Date.now(), this.timezonedata1);
    }, 1000); // set it every one seconds}
  }

  getCurrentCountryTemprature() {
    this.countryCapsuleService.getWeatherCondition(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Capital, this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Code).subscribe(r => {
      this.weatherData = r;
      this.currentCondition = this.weatherData.main;
      //This -273.15 is used for conversion kelvin to celsius
      this.temprature = (this.currentCondition.temp - 273.15).toFixed(2);
    });
  }

  getRegisterCountryTemprature() {
    this.countryCapsuleService.getWeatherCondition(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.Capital, this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.Code).subscribe(r => {
      this.weatherData = r;
      this.currentCondition = this.weatherData.main;
      //This -273.15 is used for conversion kelvin to celsius
      this.registertemprature = (this.currentCondition.temp - 273.15).toFixed(2);
    });
  }

  displayData(check) {

    switch (check) {
      case "map":
        this.changeMarkerData(this.currentCountry);
        //   this.latitude=this.currentCountry.cityLatitude;
        //   this.longitude=this.currentCountry.cityLongitude;
        // this.cityName=this.currentCountry.cityName;  
        this.displayMap = true;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.displayUnesco = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "active";
        this.healthClassForTab = "";

        this.climateClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = "",
          this.encyclopediaClassForTab = "";
        break;
      case "wiki":
        this.displayMap = false;
        this.displayWiki = true;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.displayUnesco = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "active";
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        this.nationalClassForTab = "",
          this.encyclopediaClassForTab = "";
        break;
      case "Lonly":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayCapital = false;
        this.displayLonly = true;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.displayUnesco = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "active";
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = "",
          this.encyclopediaClassForTab = "";
        break;
      case "capital":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = true;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.displayUnesco = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "active";
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = "",
          this.encyclopediaClassForTab = "";
        break;
      case "city":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = true;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.displayUnesco = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "active";
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = "",
          this.encyclopediaClassForTab = "";
        break;
      case "encyclopedia":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = true;
        this.dispalyEncyclopedia = true;
        this.dispalyNationalGeographic = false;
        this.displayUnesco = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = ""
        this.encyclopediaClassForTab = "active"
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        break;
      case "nationalGeographic":
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = true;
        this.displayUnesco = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";

        this.nationalClassForTab = "active"
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        this.encyclopediaClassForTab = ""
        break;
      case 'sdg':
        if(this.currentCountry.countryid===122){
          this.sdgCountry="korea-dem-rep"
         }
         else if(this.currentCountry.countryid===150){
           this.sdgCountry="korea-rep"
         }
         else{
           this.sdgCountry= this.currentCountry.country.split(" ").join('-').toLowerCase();
         }
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.displayUnesco = true;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.unescoClassForTab = "active"
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = ""
        this.encyclopediaClassForTab = ""
        break;
      case 'climate':
        this.dispalyClimate = true;
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.disaplayHealth = false;
        this.displayUnesco = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.climateClassForTab = "active";
        this.healthClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = ""
        this.encyclopediaClassForTab = ""
        break;
      case 'health':
        this.countryShort = (this.currentCountry.country.slice(0, 3));
        this.countryShort = this.countryShort.toLowerCase();
        this.disaplayHealth = true;
        this.dispalyClimate = false;
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.displayUnesco = false;
        this.dispalyHdr = false;
        this.hdrClassForTab = "";
        this.healthClassForTab = "active";
        this.climateClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = ""
        this.encyclopediaClassForTab = ""
        break;
      case 'hdr':
        this.countryShort = (this.currentCountry.country.slice(0, 3));
        this.countryShort = this.countryShort.toLowerCase();
        this.disaplayHealth = false;
        this.dispalyClimate = false;
        this.displayMap = false;
        this.displayWiki = false;
        this.displayLonly = false;
        this.displayCapital = false;
        this.displayCity = false;
        this.dispalyEncyclopedia = false;
        this.dispalyNationalGeographic = false;
        this.displayUnesco = false;
        this.dispalyHdr = true;
        this.hdrClassForTab = "active";
        this.healthClassForTab = "";
        this.climateClassForTab = "";
        this.unescoClassForTab = ""
        this.cityclassForTab = "";
        this.mapClassForTab = "";
        this.lonlyClassForTab = "";
        this.capitalclassForTab = "";
        this.wikiClassForTab = "";
        this.nationalClassForTab = ""
        this.encyclopediaClassForTab = ""
        break;
    }
  }


  getLifeExpectancyData() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex == this.constantService.MALE) {
      this.lifeExpectancy = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.MaleLifeExpectancy);
      this.registerLifeExpectancy = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.MaleLifeExpectancy);
    }
    else {
      this.lifeExpectancy = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.FemaleLifeExpectancy);
      this.registerLifeExpectancy = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.FemaleLifeExpectancy);
    }
  }


  private calculateGovermentSocialServices(changedCounty: any) {
    this.govermentSocialServices = changedCounty.HDI / (Math.sqrt(Math.sqrt(Math.sqrt(changedCounty.ppp))));
    if (this.govermentSocialServices > 0.26) {
      this.govermentSocialServices = 1;
    }
    else if (this.govermentSocialServices > 0.25) {
      this.govermentSocialServices = 2;
    }
    else if (this.govermentSocialServices > 0.24) {
      this.govermentSocialServices = 3;
    }
    else if (this.govermentSocialServices > 0.23) {
      this.govermentSocialServices = 4;
    }
    else if (this.govermentSocialServices > 0.22) {
      this.govermentSocialServices = 5;
    }
    if (this.govermentSocialServices > 0.21) {
      this.govermentSocialServices = 6;
    }
    else {
      this.govermentSocialServices = 7;
      this.govermentSocialServices = 8 - this.govermentSocialServices;
    }

  }

  checkTabView(tabName) {
    if (tabName === 'Political') {
      this.activeClassHealth = "";
      this.activeClassPolitical = "show active";
      this.activeClassSocial = "";
      this.activeClassEconomical = "";
      this.activeClassEcomnomicalForTab = "";
      this.activeClassHealthForTab = "";
      this.activeClassPoliticalForTab = "active";
      this.activeClassSocialForTab = "";


    }
    else if (tabName === 'Social') {
      this.activeClassHealth = "";
      this.activeClassPolitical = "";
      this.activeClassSocial = "show active";
      this.activeClassEconomical = "";
      this.activeClassEcomnomicalForTab = "";
      this.activeClassHealthForTab = "";
      this.activeClassPoliticalForTab = "";
      this.activeClassSocialForTab = "active";

    }
    else if (tabName === 'Economical') {
      this.activeClassHealth = "";
      this.activeClassPolitical = "";
      this.activeClassSocial = "";
      this.activeClassEconomical = "show active";
      this.activeClassEcomnomicalForTab = "active";
      this.activeClassHealthForTab = "";
      this.activeClassPoliticalForTab = "";
      this.activeClassSocialForTab = "";

    }
    else if (tabName === 'Health') {
      this.activeClassHealth = "show active";
      this.activeClassPolitical = "";
      this.activeClassSocial = "";
      this.activeClassEconomical = "";
      this.activeClassEcomnomicalForTab = "";
      this.activeClassHealthForTab = "active";
      this.activeClassPoliticalForTab = "";
      this.activeClassSocialForTab = "";

    }
  }


  sanitize(url: string, option: string) {
    this.displayMap = false;
    this.displayData(option);
    this.myUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);

  }

  checkPopulationFormat(Population) {

    if (Population != undefined) {

      let abs = Math.abs(Population);
      if (abs >= Math.pow(10, 12)) {
        // trillion
        Population = (Population / Math.pow(10, 12)).toFixed(1);
        this.checkFormat = "Trillion (T)";
      }
      else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
        // billion
        Population = (Population / Math.pow(10, 9)).toFixed(1);
        this.checkFormat = "Billion (B)";
      }
      else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
        // million
        Population = (Population / Math.pow(10, 6)).toFixed(1);
        this.checkFormat = "Million (M)";
      }
      else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
        // thousand
        Population = (Population / Math.pow(10, 3)).toFixed(1);
        this.checkFormat = "Thousand (K)";
      }
    }

    return Population;
  }


  placeMarker(position: any) {
  }

  clickReadMore() {
    this.readMoreFlag = true;
    this.countryShort = (this.currentCountry.country.slice(0, 3));
    this.countryShort = this.countryShort.toLowerCase();
    this.changeMarkerData(this.currentCountry);
    this.displayMap = true;
    this.displayData('map');
  }

  closeReadMore() {
    this.readMoreFlag = false;


  }


  checkCssForSpecific(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind";
    }
    else if (value1 === value2) {
      this.classvalue = "";
    }
    return this.classvalue;
  }

  checkCss1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }
  checkCssForSpecific1(x, y) {
    this.classvalue = "";
    let value1: any;
    let value2: any;
    value1 = parseFloat(x);
    value2 = parseFloat(y);
    if (value1 > value2) {
      this.classvalue = "bad_ind text-white";
    }
    else if (value1 < value2) {
      this.classvalue = "good_ind text-white";
    }
    else if (value1 === value2) {
      this.classvalue = "text-white";
    }
    return this.classvalue;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}


