import { TestBed } from '@angular/core/testing';

import { CountryCapsuleService } from './country-capsule.service';

describe('CountryCapsuleService', () => {
  let service: CountryCapsuleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountryCapsuleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
