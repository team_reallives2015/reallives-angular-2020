import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service'
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { CommonActionService } from '../common-action.service';
import { EventService } from '../event/event.service';
import { HomeService } from '../home.service';
import { IdentityDifferenceFindService } from '../identity-difference-find.service';
import { WellBeingIndicatorService } from '../well-being-indicator/well-being-indicator.service';
import { Decision } from './Decision';
import { DecisionService } from './decision.service';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';


@Component({
  selector: 'app-decision-window',
  templateUrl: './decision-window.component.html',
  styleUrls: ['./decision-window.component.css']
})
export class DecisionWindowComponent implements OnInit {
  loanPotId;
  businessCardSize = '';
  directPayFlag: boolean = false;
  value = ""
  index;
  emiCash;
  loanId;
  familyLoan = 0;
  repayFlag: boolean = false;
  showPayButton: boolean = false;
  confirmFlag: boolean = false;
  saveButtonFlag: boolean = false;
  takeLoanButtonFlag: boolean = false;
  paidEmiCount = 0;
  loanName = '';
  payButtonDisable: boolean = false;
  maxValue = 0;
  maxSharkLoan = 0;
  maxBankLoan = 0;
  payAllEmiFlag: boolean = false;
  loanPayCount = 0;
  loanClass = "";
  takenLoanAmt = 0;
  selectedBusinessValue = 0;
  selectedHcash = 0
  selectedBankLoan = 0;
  selectedSharkLoan = 0;
  totalTakenLoanAmount = 0;
  showLoanWindow: boolean = false;
  takenSharkAmt = 0;
  aveLoanAmount = 0;
  takenFriendLoan = 0;
  sharkLoanAmount = 0;
  takeLoan: boolean = false;
  selectedEmiValue = 0;
  selectedEmiIndex = null
  warrMsg = "";
  warrFlag: boolean = false;
  buttonDisableFlag: boolean = false;
  businessFlag: boolean = false;
  allPerson;
  mainPerson;
  displayDecision = new Decision();
  selectedActionForRedioButton = 1;
  selectedActionForeducationFieldOfStudy = 1;
  selectedActionForBloodDonation = 1;
  selectedActionForPet;
  selecte
  result;
  diiferenBetIdentity;
  ageAYearClickFlag: boolean = false;
  ageAYearDoneFlag: boolean = false;
  identity = [];
  identityValues = [];
  hideDecisionId;
  simpleYesNoDecisionModelShowFlag: boolean = false;
  simpleRedioButtonDecisionShowFlag: boolean = false;
  fieldOfStusyDecisionShowFlag: boolean = false;
  bloodDonationDecisionFlag: boolean = false;
  organDonationDecisionFlag: boolean = false;
  newOrganObject = [];
  bloodYear = 0;
  selectedOrganArray = [];
  petTextAreaFlag: boolean = false;
  petDecisionFlag: boolean = false;
  textAreaValid: string = "";
  textAreaOkButton: boolean = true;
  selectedPetValue;
  totalHCash = 0;
  totalBusinessCash = 0

  constructor(public modalWindowShowHide: modalWindowShowHide,
    config: NgbModalConfig,
    public agaAYearService: AgaAYearService,
    public decisionService: DecisionService,
    public homeService: HomeService,
    public constantService: ConstantService,
    public identityDifferenceFindService: IdentityDifferenceFindService,
    public eventService: EventService,
    public wellBeingIndicatorService: WellBeingIndicatorService,
    public commonActionService: CommonActionService,
    public gameSummeryService: GameSummeryService,
    public TranslateService: TranslationService,
    public translation: TranslateService,
    public commonService: CommonService
  ) { }
  @ViewChild('decisionBoxWindow') decisionBoxWindow: ModalDirective;
  @ViewChild('decisionForImageRedioButton') decisionForImageRedioButton: ModalDirective;
  @ViewChild('decisionForDesignRedioButton') decisionForDesignRedioButton: ModalDirective;
  @ViewChild('decisionForSimpleYesNo') decisionForSimpleYesNo: ModalDirective;
  @ViewChild('decisionForSimpleRedioButton') decisionForSimpleRedioButton: ModalDirective;
  @ViewChild('decisionForFieldOfStudy') decisionForFieldOfStudy: ModalDirective;
  @ViewChild('decisionForBloodDonation') decisionForBloodDonation: ModalDirective;
  @ViewChild('decisionForOrganDonation') decisionForOrganDonation: ModalDirective;
  @ViewChild('decisionForpet') decisionForpet: ModalDirective;
  @ViewChild('decisionForTextArea') decisionForTextArea: ModalDirective;
  isModalShown = false;

  doCheck() {
    if (this.textAreaValid.length > 0) {
      this.textAreaOkButton = false;
    }
    else {
      this.textAreaOkButton = true;

    }
  }
  check(index, organStatus) {
    if (!this.newOrganObject[index].active) {
      this.newOrganObject[index].active = "active";
      this.displayDecision.choiceArray = organStatus
      this.newOrganObject[index].organStatus = organStatus;
    }
    else if (this.newOrganObject[index].active) {
      this.newOrganObject[index].active = "";
      this.displayDecision.choiceArray = organStatus
      this.newOrganObject[index].organStatus = organStatus;
    }
  }

  ngOnInit() {
    this.allPerson = this.homeService.allPerson;
    this.mainPerson = this.homeService.allPerson[this.constantService.FAMILY_SELF];
    this.selectedActionForPet = this.translation.instant('gameLabel.Dog')
    this.choiceDecision();
  }

  choiceDecision() {
    this.displayDecision = this.agaAYearService.decision;
    if (this.displayDecision.choiceArray.length == 2 && !this.displayDecision.textAreaFlag && !this.displayDecision.businessFlag) {
      this.displayYesNoDecision();
    }
    else if (this.displayDecision.choiceArray.length > 2 && !this.displayDecision.fieldOfStudyListFlag && !this.displayDecision.bloodDonationFlag && !this.displayDecision.organDonationFlag && !this.displayDecision.petFlag && !this.displayDecision.textAreaFlag) {
      for (let i = 0; i < this.displayDecision.choiceArray.length; i++) {
        if (i == 0) {
          this.selectedActionForRedioButton = this.displayDecision.choiceArray[i].Value;
          break;
        }
      }
      this.displaySimpleRedioButtonDecision();
    }
    else if (this.displayDecision.choiceArray.length > 2 && this.displayDecision.fieldOfStudyListFlag) {
      for (let i = 0; i < this.displayDecision.choiceArray.length; i++) {
        if (i == 0) {
          this.selectedActionForeducationFieldOfStudy = this.displayDecision.choiceArray[i].EnrollId;
          break;
        }
      }
      this.dispalyFielOfStudySecision();
    }
    else if (this.displayDecision.choiceArray.length > 2 && this.displayDecision.bloodDonationFlag) {
      for (let i = 0; i < this.displayDecision.choiceArray.length; i++) {
        if (i == 0) {
          this.bloodYear = this.displayDecision.choiceArray[i].Value;
          break;
        }
      }
      this.dispalyBloodDonationDecision();
    }
    else if (this.displayDecision.choiceArray.length > 2 && this.displayDecision.organDonationFlag) {
      this.dispalyOrganDonationDecision();
    }
    else if (this.displayDecision.choiceArray.length > 2 && this.displayDecision.petFlag) {
      for (let i = 0; i < this.displayDecision.choiceArray.length; i++) {
        if (i == 0) {
          this.selectedPetValue = this.displayDecision.choiceArray[i].Value;
          break;
        }
      }
      this.petDecision();
    }
    else if (this.displayDecision.textAreaFlag) {
      this.textAreaDecision();
    }
    else if (this.displayDecision.businessFlag) {
      if (!this.homeService.displaySdgFlag) {
        this.businessCardSize = "col-md-6";
      }
      else {
        this.businessCardSize = "col-md-5";

      }
      this.businessFlag = true;
      this.takeLoanClick();
      this.totalHCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash);
      this.totalBusinessCash = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessValue)
      this.payDirectlyButtonShow();
      this.cardBorderSet();
    }
    else {
      this.displayYesNoDecision();
    }
  }

  displayYesNoDecision() {
    this.simpleYesNoDecisionModelShowFlag = true;
    // this.decisionForSimpleYesNo.show();
  }

  yesNoDecisionClick(choice) {
    this.modalWindowShowHide.showWaitScreen = true;
    this.decisionForSimpleYesNo.hide();
    this.simpleYesNoDecisionModelShowFlag = false;
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, choice).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null && this.homeService.allPerson[this.constantService.FAMILY_SELF].has_lover && Object.values(this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover]).length === 0) {
              this.gameSummeryService.getPersonFromId(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(res => {
                this.allPerson = res;
                this.homeService.allPerson = res;
                this.homeService.changeAllPerson.emit(this.homeService.allPerson);
                this.agaAYearService.nextEventResultIsEvent(this.result);
                this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
                this.modalWindowShowHide.showWaitScreen = false;
                this.modalWindowShowHide.showDecisionFlag = false;
              })
            } else {
              this.agaAYearService.nextEventResultIsEvent(this.result);
              this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
              this.modalWindowShowHide.showWaitScreen = false;
              this.modalWindowShowHide.showDecisionFlag = false;
            }
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }
      })
  }

  yesNoDecisionNoClick(choice) {
    this.decisionForSimpleYesNo.hide();
    this.simpleYesNoDecisionModelShowFlag = false;
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, choice).subscribe(
      res => {
        this.result = res;
        this.diiferenBetIdentity = this.result.data.result.difference;
        this.diiferenBetIdentity = this.result.data.result.difference;
        let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
        if (retuenValue !== 'false') {
          this.homeService.changeAllPerson.emit(this.allPerson);
        }
        if (this.result.data.result.type === 'event') {
          this.agaAYearService.nextEventResultIsEvent(this.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else if (this.result.data.result.type === 'Decision') {
          this.agaAYearService.setDecision(this.result.data.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.choiceDecision();
        }
        else {
          this.modalWindowShowHide.showDecisionFlag = false;
        }
      })
  }

  simpleRedioButtonDecisionClick(choice) {
    this.modalWindowShowHide.showWaitScreen = true;
    this.decisionForSimpleRedioButton.hide();
    this.simpleRedioButtonDecisionShowFlag = false;
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, choice).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            this.agaAYearService.nextEventResultIsEvent(this.result);
            this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showDecisionFlag = false;
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }
      })
  }

  setSelectedBloodYear(year) {
    this.bloodYear = year;
  }

  EducationFieldOfStudyDecisionClick(choice) {
    this.modalWindowShowHide.showWaitScreen = true;
    this.fieldOfStusyDecisionShowFlag = false;
    this.decisionForFieldOfStudy.hide();
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, choice).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;

          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            this.agaAYearService.nextEventResultIsEvent(this.result);
            this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showDecisionFlag = false;
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }

      })
  }

  bloodDonationDecisionClick(choice) {
    this.modalWindowShowHide.showWaitScreen = true;
    this.bloodDonationDecisionFlag = false;
    this.decisionForBloodDonation.hide();
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, choice).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            this.agaAYearService.nextEventResultIsEvent(this.result);
            this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showDecisionFlag = false;
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }

      })
  }
  displaySimpleRedioButtonDecision() {
    this.simpleRedioButtonDecisionShowFlag = true;
    // this.decisionForSimpleRedioButton.show();
  }

  dispalyFielOfStudySecision() {
    this.fieldOfStusyDecisionShowFlag = true;
    // this.decisionForFieldOfStudy.show();
  }

  dispalyBloodDonationDecision() {
    this.bloodDonationDecisionFlag = true;
    // this.decisionForBloodDonation.show();
  }
  dispalyOrganDonationDecision() {
    this.organDonationDecisionFlag = true;
    // this.decisionForOrganDonation.show();
    this.newOrganObject = JSON.parse(JSON.stringify(this.displayDecision.choiceArray));
    for (let i = 0; i < this.newOrganObject.length; i++) {
      if (this.newOrganObject[i].organStatus) {
        this.newOrganObject[i].active = "active";
      }
      else {
        this.newOrganObject[i].active = "";
      }
    }

  }

  organDonationDecisionClick() {
    let count = 0;
    this.modalWindowShowHide.showWaitScreen = true;
    this.organDonationDecisionFlag = false;
    this.decisionForOrganDonation.hide();
    for (let i = 0; i < this.newOrganObject.length; i++) {
      if (this.newOrganObject[i].organStatus) {
        this.selectedOrganArray.push(this.newOrganObject[i].organName)
      }
    }
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, this.selectedOrganArray).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            this.agaAYearService.nextEventResultIsEvent(this.result);
            this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showOrganDonationCertificate = true;
            this.modalWindowShowHide.showDecisionFlag = false;
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }
      })
  }

  closeOrganDonationDecision() {
    this.organDonationDecisionFlag = false;
    this.decisionForOrganDonation.hide();
    this.modalWindowShowHide.showDecisionFlag = false;
  }

  closeBloodDonationDecision() {
    this.bloodDonationDecisionFlag = false;
    this.decisionForBloodDonation.hide();
    this.modalWindowShowHide.showDecisionFlag = false;
  }

  simpleRedioButtonDecisionYesClick(choice) {
    this.simpleRedioButtonDecisionShowFlag = false;
    this.decisionForSimpleRedioButton.hide();
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, choice).subscribe(
      res => {
        this.result = res;
        this.diiferenBetIdentity = this.result.data.result.difference;
        let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
        if (retuenValue !== 'false') {
          this.homeService.allPerson = retuenValue;
          this.homeService.changeAllPerson.emit(this.homeService.allPerson);
        }
        if (this.result.data.result.age_a_year) {
          this.ageAYearClickFlag = false;
        }
        else {
          this.ageAYearClickFlag = true;
        }
        if (this.result.data.result.type === 'event') {
          this.agaAYearService.nextEventResultIsEvent(this.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
          if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
            this.homeService.cssForDisable = "disable";
            setTimeout(() => {
              this.modalWindowShowHide.showObituary = true;
              this.modalWindowShowHide.showGameSummary = false;
              this.modalWindowShowHide.showEvent = false;
              this.homeService.cssForDisable = "";
            },
              3000);
          }
        }
        else if (this.result.data.result.type === 'Decision') {
          this.agaAYearService.setDecision(this.result.data.result);
          this.modalWindowShowHide.showWaitScreen = false;
          this.choiceDecision();
        }
        else {
          this.modalWindowShowHide.showDecisionFlag = false;
        }
      })
  }

  petDecision() {
    this.petDecisionFlag = true;
    //  this.decisionForpet.show();
  }

  closePetDecision() {
    this.petDecisionFlag = false;
    this.decisionForpet.hide();
    this.modalWindowShowHide.showDecisionFlag = false;
  }

  setSelectedPet(pet) {
    this.selectedPetValue = pet;
  }

  clickPetDecision() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.petDecisionFlag = false;
    this.decisionForpet.hide();
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, this.selectedPetValue).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;

        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            this.agaAYearService.nextEventResultIsEvent(this.result);
            this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showDecisionFlag = false;
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }
      })
  }

  textAreaDecision() {
    this.petTextAreaFlag = true;
    // this.decisionForTextArea.show();
    this.textAreaValid = "";
  }

  closeTextAreaDecision() {
    this.petTextAreaFlag = false;
    this.decisionForTextArea.hide();
    this.modalWindowShowHide.showDecisionFlag = false;
  }

  clickTextDecision() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.petTextAreaFlag = false;
    this.decisionForTextArea.hide();
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, this.textAreaValid).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            this.agaAYearService.nextEventResultIsEvent(this.result);
            this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
            this.modalWindowShowHide.showWaitScreen = false;
            this.modalWindowShowHide.showDecisionFlag = false;
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }
      })
  }

  //business decision logic
  yesNoBusinessDecisionClick() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.businessFlag = false;
    this.decisionService.decisionYesNoResultUpdate(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.agaAYearService.decision.gameEventId, this.displayDecision.loanTypeArr).subscribe(
      res => {
        this.result = res;
        if (this.result.data.result.type === null) {
          this.modalWindowShowHide.showWaitScreen = false;
          this.modalWindowShowHide.showDecisionFlag = false;
        }
        else {
          this.diiferenBetIdentity = this.result.data.result.difference;
          let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.allPerson);
          if (retuenValue !== 'false') {
            this.homeService.changeAllPerson.emit(retuenValue);
            this.homeService.allPerson = retuenValue;
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].lover !== null && this.homeService.allPerson[this.constantService.FAMILY_SELF].has_lover && Object.values(this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].lover]).length === 0) {
              this.gameSummeryService.getPersonFromId(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(res => {
                this.allPerson = res;
                this.homeService.allPerson = res;
                this.homeService.changeAllPerson.emit(this.homeService.allPerson);
                this.agaAYearService.nextEventResultIsEvent(this.result);
                this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
                this.modalWindowShowHide.showWaitScreen = false;
                this.modalWindowShowHide.showDecisionFlag = false;
              })
            } else {
              this.agaAYearService.nextEventResultIsEvent(this.result);
              this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
              this.modalWindowShowHide.showWaitScreen = false;
              this.modalWindowShowHide.showDecisionFlag = false;
            }
          }
          else if (this.result.data.result.type === 'Decision') {
            this.agaAYearService.setDecision(this.result.data.result);
            this.modalWindowShowHide.showWaitScreen = false;
            this.choiceDecision();
          }
          else if (this.result.data.result.type === 'endAgeYear') {
            if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
              this.homeService.cssForDisable = "disable";
              setTimeout(() => {
                this.modalWindowShowHide.showObituary = true;
                this.modalWindowShowHide.showGameSummary = false;
                this.modalWindowShowHide.showEvent = false;
                this.homeService.cssForDisable = "";
              },
                3000);
            }
          }
          else {
            this.modalWindowShowHide.showDecisionFlag = false;
          }
        }
      })
  }
  payDirectlyAlertMsg(i, emi, loanId) {
    this.directPayFlag = true;
    this.index = i;
    this.emiCash = emi;
    this.loanId = loanId;
    this.warrMsg = this.translation.instant('gameLabel.emiWarr2')
    this.confirmFlag = true;
  }
  payDirectlyYes() {
    this.confirmFlag = false;
    this.payDirectly(this.index, this.emiCash, this.loanId);
    this.directPayFlag = false;
  }

  payDirectlyNo() {
    this.confirmFlag = false;
    this.directPayFlag = false;
  }

  payDirectly(i, emi, loanId) {
    if (this.displayDecision.loanTypeArr[i].potentialLoanId === 7) {
      this.displayDecision.loanTypeArr[i].payButtonDisable = true;
    }
    this.selectedEmiValue = emi;
    this.selectedEmiIndex = i;
    for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
      if (loanId === this.displayDecision.loanTypeArr[i].loanId) {
        this.displayDecision.loanTypeArr[i].disabled = 'mb-3 card';
        this.displayDecision.loanTypeArr[i].dis = true;
      }
      else {
        this.displayDecision.loanTypeArr[i].dis = false;
      }
    }
    this.selectedBusinessValue = this.selectedEmiValue;
    this.totalHCash = this.totalHCash - this.selectedHcash
    this.totalBusinessCash = this.totalBusinessCash - this.selectedBusinessValue
    this.aveLoanAmount = this.aveLoanAmount - this.selectedBankLoan;
    this.sharkLoanAmount = this.sharkLoanAmount - this.selectedSharkLoan
    this.takenSharkAmt = this.takenSharkAmt + this.selectedSharkLoan;
    this.takenLoanAmt = this.takenLoanAmt + this.selectedBankLoan;
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse = this.selectedHcash;
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse = this.selectedBusinessValue;
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].check = true;
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].ignore = false;
    this.displayDecision.loanTypeArr[i].notPay = false;
    this.showLoanWindow = false;
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan = this.selectedBankLoan
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan = this.selectedSharkLoan
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].paidValue = this.selectedHcash + this.selectedBusinessValue + this.selectedBankLoan + this.selectedSharkLoan
    this.selectedHcash = 0;
    this.selectedBusinessValue = 0;
    this.selectedBankLoan = 0;
    this.selectedSharkLoan = 0;
    this.selectedEmiValue = 0;
    this.selectedEmiIndex = null;
    this.takeLoan = false;
    this.loanName = '';
    this.takeLoanButtonFlag = false;
    this.paidEmiCount = this.paidEmiCount + 1;
    this.payDirectlyButtonShow();
    this.cardBorderSet();

  }
  payDirectlyButtonShow() {
    for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
      let total = this.totalBusinessCash
      if (total >= this.displayDecision.loanTypeArr[i].value) {
        this.displayDecision.loanTypeArr[i].showPayButton = true;
      }
      else {
        this.displayDecision.loanTypeArr[i].showPayButton = false;

      }
      if (this.displayDecision.loanTypeArr[i].loanId === 7) {
        this.displayDecision.loanTypeArr[i].borderClass = "redBorder card w-100 for-bx-shadow  mb0  "
      }
      else {
        this.displayDecision.loanTypeArr[i].borderClass = "card w-100 for-bx-shadow mb0 "
      }
    }
    this.cardBorderSet();
  }

  cardBorderSet() {
    for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
      if (this.displayDecision.loanTypeArr[i].potentialLoanId === 7) {
        this.displayDecision.loanTypeArr[i].borderClass = "redBorder card w-100 for-bx-shadow mb0 "
      }
      else {
        this.displayDecision.loanTypeArr[i].borderClass = "card w-100 for-bx-shadow mb0 "
      }
      if (this.displayDecision.loanTypeArr[i].potentialLoanId === 8 || this.displayDecision.loanTypeArr[i].potentialLoanId === 9) {
        this.displayDecision.loanTypeArr[i].option = true;
      }
      else {
        this.displayDecision.loanTypeArr[i].option = false;
      }

      if (this.displayDecision.loanTypeArr[i].potentialLoanId === 7 || this.displayDecision.loanTypeArr[i].potentialLoanId === 8 || this.displayDecision.loanTypeArr[i].potentialLoanId === 9) {
        this.displayDecision.loanTypeArr[i].sharkFlag = true;
        this.displayDecision.loanTypeArr[i].payButtonDisable = false;
      }
      else {
        this.displayDecision.loanTypeArr[i].sharkFlag = false;
        this.displayDecision.loanTypeArr[i].payButtonDisable = false;
      }


    }
  }
  payLoan(index, value, loanId) {
  this.loanPotId=this.displayDecision.loanTypeArr[index].potentialLoanId;
    this.value = this.translation.instant('gameLabel.Pay installment')
    this.repayFlag = false
    this.loanName = this.displayDecision.loanTypeArr[index].name;
    if (this.displayDecision.loanTypeArr[index].potentialLoanId === 7) {
      this.displayDecision.loanTypeArr[index].payButtonDisable = true;
    }
    this.maxValue = this.selectedEmiValue
    for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
      if (loanId !== this.displayDecision.loanTypeArr[i].loanId) {
        this.displayDecision.loanTypeArr[i].disabled = 'mb-3 card_disable';
        this.displayDecision.loanTypeArr[i].dis = true;
      }
      else {
        this.displayDecision.loanTypeArr[i].disabled = 'mb-3';
        this.displayDecision.loanTypeArr[i].dis = true;
      }
    }
    if (!this.displayDecision.loanTypeArr[index].check) {
      this.selectedEmiValue = value;
      this.totalTakenLoanAmount = 0;
      this.classForRedGreen();
      this.selectedEmiValue = value;
      this.selectedEmiIndex = index;
      let total = this.totalBusinessCash + this.totalHCash;
      if (total < this.selectedEmiValue) {
        this.takeLoanButtonFlag = true
      }
      else {
        this.takeLoanButtonFlag = false;
      }
    }

  }
  takeLoanClick() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities <= 0) {
      let householdAssetsPer = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6);
      this.aveLoanAmount = householdAssetsPer;
      this.sharkLoanAmount = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash * 1.2);
      this.takenLoanAmt = 0;
      this.takenSharkAmt = 0;
    }
    else {
      for (let i = 0; i < this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.length; i++) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 7) {
          this.takenSharkAmt = this.takenSharkAmt + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance;
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 8) {
          this.takenFriendLoan = this.takenFriendLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 9) {
          this.familyLoan = this.familyLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance
        }
      }

      this.takenLoanAmt = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities - (this.takenSharkAmt + this.takenFriendLoan + this.familyLoan)
      this.sharkLoanAmount = this.takenSharkAmt
      let avaLoan = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6) - Math.round(this.takenLoanAmt);
      let aveShark = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash * 1.2) - Math.round(this.sharkLoanAmount);
      if (avaLoan > this.takenLoanAmt) {
        this.aveLoanAmount = avaLoan - this.takenLoanAmt
      }
      if (aveShark > this.takenSharkAmt) {
        this.sharkLoanAmount = aveShark - this.takenSharkAmt;
      }
    }

  }
  setLoanValues() {
    if (this.repayFlag) {
      this.totalTakenLoanAmount = this.selectedBusinessValue + this.selectedHcash
      this.classForRedGreen();

    }
    else {
      this.totalTakenLoanAmount = this.selectedBankLoan + this.selectedSharkLoan + this.selectedBusinessValue + this.selectedHcash
      this.classForRedGreen();
    }
  }
  takeLoanOk() {
    if (!this.repayFlag) {
      for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
        this.displayDecision.loanTypeArr[i].disabled = 'mb-3';
        this.displayDecision.loanTypeArr[i].dis = false;
      }
      if (this.totalTakenLoanAmount >= this.selectedEmiValue) {
        if (this.selectedBusinessValue < this.selectedEmiValue) {
          if (this.selectedHcash > 0) {
            let diff = this.selectedEmiValue - this.selectedBusinessValue;
            if (this.selectedHcash >= diff) {
              this.selectedHcash = diff;
            }
          }
        }
        else if (this.selectedBusinessValue >= this.selectedEmiValue) {
          this.selectedBusinessValue = this.selectedEmiValue
          this.selectedHcash = 0;
        }
        this.totalHCash = this.totalHCash - this.selectedHcash
        this.totalBusinessCash = this.totalBusinessCash - this.selectedBusinessValue
        this.aveLoanAmount = this.aveLoanAmount - this.selectedBankLoan;
        this.sharkLoanAmount = this.sharkLoanAmount - this.selectedSharkLoan
        this.takenSharkAmt = this.takenSharkAmt + this.selectedSharkLoan;
        this.takenLoanAmt = this.takenLoanAmt + this.selectedBankLoan;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse = this.selectedHcash;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse = this.selectedBusinessValue;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].check = true;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].ignore = false;
        this.showLoanWindow = false;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan = this.selectedBankLoan
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan = this.selectedSharkLoan
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].paidValue = this.selectedHcash + this.selectedBusinessValue + this.selectedBankLoan + this.selectedSharkLoan
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].payButtonDisable = false;

      }
      else {
        this.showLoanWindow = false;
        if (this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse === 0) {
          this.totalHCash = this.totalHCash - this.selectedHcash
        }
        else if (this.selectedHcash > this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse) {
          this.totalHCash = this.totalHCash - (this.selectedHcash - this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse)
        }
        else if (this.selectedHcash < this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse) {
          this.totalHCash = this.totalHCash + (this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse - this.selectedHcash)
        }


        if (this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse === 0) {
          this.totalBusinessCash = this.totalBusinessCash - (this.selectedBusinessValue)
        }
        else if (this.selectedBusinessValue > this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse) {
          this.totalBusinessCash = this.totalBusinessCash - (this.selectedBusinessValue - this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse)
        }
        else if (this.selectedBusinessValue < this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse) {
          this.totalBusinessCash = this.totalBusinessCash + (this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse - this.selectedBusinessValue)
        }

        if (this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan === 0) {
          this.sharkLoanAmount = this.sharkLoanAmount - (this.selectedSharkLoan)
          this.takenSharkAmt = this.takenSharkAmt - (this.selectedSharkLoan)
        }
        else if (this.selectedSharkLoan > this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan) {
          this.sharkLoanAmount = this.sharkLoanAmount - (this.selectedSharkLoan - this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan)
          this.takenSharkAmt = this.takenSharkAmt - (this.selectedSharkLoan - this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan)
        }
        else if (this.selectedSharkLoan < this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan) {
          this.sharkLoanAmount = this.sharkLoanAmount + (this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan - this.selectedSharkLoan)
          this.takenSharkAmt = this.takenSharkAmt + (this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan - this.selectedSharkLoan)
        }

        if (this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan === 0) {
          this.aveLoanAmount = this.aveLoanAmount - (this.selectedBankLoan)
          this.takenLoanAmt = this.takenLoanAmt - (this.selectedBankLoan)
        }
        else if (this.selectedBankLoan > this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan) {
          this.aveLoanAmount = this.aveLoanAmount - (this.selectedSharkLoan - this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan)
          this.takenLoanAmt = this.takenLoanAmt - (this.selectedSharkLoan - this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan)
        }
        else if (this.selectedBankLoan < this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan) {
          this.aveLoanAmount = this.aveLoanAmount + (this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan - this.selectedSharkLoan)
          this.takenLoanAmt = this.takenLoanAmt + (this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan - this.selectedSharkLoan)
        }
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].paidValue = this.selectedHcash + this.selectedBusinessValue + this.selectedBankLoan + this.selectedSharkLoan

        this.setZero();
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].householdCashUse = this.selectedHcash;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].businessCashUse = this.selectedBusinessValue;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].check = false;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].ignore = true;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].notPay = false;
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].bankLoan = this.selectedBankLoan
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].sharkLoan = this.selectedSharkLoan
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].paidValue = this.selectedHcash + this.selectedBusinessValue + this.selectedBankLoan + this.selectedSharkLoan
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].payButtonDisable = false;

      }
      this.selectedHcash = 0;
      this.selectedBusinessValue = 0;
      this.totalTakenLoanAmount = 0;
      this.selectedBankLoan = 0;
      this.selectedSharkLoan = 0;
      this.selectedEmiValue = 0;
      this.selectedEmiIndex = null;
      this.takeLoan = false;
      this.loanName = '';
      this.takeLoanButtonFlag = false;
      this.paidEmiCount = this.paidEmiCount + 1;
      this.payDirectlyButtonShow();
    }
    else {
      this.repayFlag = false;
      if (this.selectedBusinessValue > this.selectedEmiValue) {
        this.selectedBusinessValue = this.selectedEmiValue
      } else {
        let total = this.selectedBusinessValue + this.selectedHcash
        if (total > this.selectedEmiValue) {
          let diff = this.selectedEmiValue - this.selectedBusinessValue
          if (diff > 0) {
            this.selectedHcash = diff
          }
          else {
            this.selectedHcash = 0
          }
        }
      }
      if (this.displayDecision.loanTypeArr[this.selectedEmiIndex].hasOwnProperty('repayObj')) {
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].repayObj.businessCash = this.displayDecision.loanTypeArr[this.selectedEmiIndex].repayObj.businessCash + Math.round(this.selectedBusinessValue);
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].repayObj.householdCash = this.displayDecision.loanTypeArr[this.selectedEmiIndex].repayObj.householdCash + Math.round(this.selectedHcash);
      }
      else {
        let repay = {
          "businessCash": Math.round(this.selectedBusinessValue),
          "householdCash": Math.round(this.selectedHcash)
        }
        this.displayDecision.loanTypeArr[this.selectedEmiIndex].repayObj = repay
      }
      this.displayDecision.loanTypeArr[this.selectedEmiIndex].remainingLoan = this.displayDecision.loanTypeArr[this.selectedEmiIndex].remainingLoan - (this.selectedBusinessValue + this.selectedHcash)
      this.totalHCash = this.totalHCash - this.selectedHcash;
      this.totalBusinessCash = this.totalBusinessCash - this.selectedBusinessValue;
      this.displayDecision.loanTypeArr[this.selectedEmiIndex].payButtonDisable = false;
      this.selectedHcash = 0;
      this.selectedBusinessValue = 0;
      this.selectedBankLoan = 0;
      this.selectedSharkLoan = 0;
      this.selectedEmiValue = 0;
      this.totalTakenLoanAmount = 0;
      this.takeLoan = false;
      this.loanName = '';
      this.selectedEmiIndex = null;
      this.takeLoanButtonFlag = false;
      for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
        this.displayDecision.loanTypeArr[i].disabled = 'mb-3 ';
        this.displayDecision.loanTypeArr[i].dis = false;
      }
      this.payDirectlyButtonShow();
    }
    this.cardBorderSet();

  }
  clickGetLoan() {
    this.takeLoan = true
    let diff = this.selectedEmiValue - (this.totalBusinessCash + this.totalHCash)
    diff = Math.round(diff)
    if (diff > this.aveLoanAmount) {
      this.maxBankLoan = this.aveLoanAmount
    }
    else {
      this.maxBankLoan = diff
    }
    if (diff > this.sharkLoanAmount) {
      this.maxSharkLoan = this.sharkLoanAmount
    }
    else {
      this.maxSharkLoan = diff
    }
  }
  takeLoanCancle() {
    this.displayDecision.loanTypeArr[this.selectedEmiIndex].payButtonDisable = false;
    this.showLoanWindow = false;
    this.selectedEmiValue = 0;
    this.selectedBankLoan = 0;
    this.selectedHcash = 0;
    this.selectedBusinessValue = 0;
    this.totalTakenLoanAmount = 0;
    this.selectedSharkLoan = 0;
    this.selectedEmiIndex = null;
    this.takeLoanButtonFlag = false;
    this.takeLoan = false;
    this.repayFlag = false;
    for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
      this.displayDecision.loanTypeArr[i].disabled = 'mb-3';
      this.displayDecision.loanTypeArr[i].dis = false;
    }
    this.cardBorderSet();
  }
  classForRedGreen() {
    if (this.selectedEmiValue >= this.totalTakenLoanAmount) {
      this.loanClass = "red"
    }
    else if (this.selectedEmiValue < this.totalTakenLoanAmount) {
      this.loanClass = "green"
    }
    else {
      this.loanClass = ""

    }
  }
  setZero() {
    if (this.totalHCash < 0) {
      this.totalHCash = 0
    }
    if (this.totalBusinessCash < 0) {
      this.totalBusinessCash = 0
    }
    if (this.selectedBusinessValue < 0) {
      this.selectedBusinessValue = 0;
    }
    if (this.selectedHcash < 0) {
      this.selectedHcash = 0;
    }
    if (this.selectedBankLoan < 0) {
      this.selectedBankLoan = 0;
    }
    if (this.selectedSharkLoan < 0) {
      this.selectedSharkLoan = 0;
    }
  }
  payAllEmi() {
    this.payAllEmiFlag = true
    for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
      this.displayDecision.loanTypeArr[i].disabled = 'mb-3 ';
      this.displayDecision.loanTypeArr[i].dis = false;
    }
  }
  payAllEmiOk() {
    if (this.paidEmiCount === this.displayDecision.loanTypeArr.length) {
      this.payAllEmiFlag = false
      this.saveButtonFlag = true
      this.yesNoBusinessDecisionClick();

    }
    else {
      this.translation.get('gameLabel.emiWarr1', { paidEmiCount: (this.paidEmiCount), length: this.displayDecision.loanTypeArr.length }).subscribe((s: string) => {
        this.warrMsg = s
      })
      this.confirmFlag = true;
    }
  }
  payAllEmiOkClick() {
    this.payAllEmiFlag = false
    this.saveButtonFlag = true
    this.confirmFlag = false;
    this.yesNoBusinessDecisionClick();

  }
  payAllEmiCancel() {
    this.confirmFlag = false;
    this.payAllEmiFlag = false
    this.saveButtonFlag = false
  }

  repayCapital(i, value, loanId) {
    this.value = this.translation.instant('gameLabel.Pay capital')
    this.loanName = this.displayDecision.loanTypeArr[i].name
    if (value === 0) {
      this.displayDecision.loanTypeArr[i].payButtonDisable = true;
    }
    else {
      for (let i = 0; i < this.displayDecision.loanTypeArr.length; i++) {
        if (loanId !== this.displayDecision.loanTypeArr[i].loanId) {
          this.displayDecision.loanTypeArr[i].disabled = 'mb-3 card_disable';
          this.displayDecision.loanTypeArr[i].dis = true;
        }
        else {
          this.displayDecision.loanTypeArr[i].disabled = 'mb-3 ';
          this.displayDecision.loanTypeArr[i].dis = true;
        }
      }
      this.repayFlag = true;
      this.selectedEmiIndex = i
      this.selectedEmiValue = value
      this.classForRedGreen();
      if (this.repayFlag) {
        this.takeLoanButtonFlag = false;
      }
    }
  }

  notPayEmi(i, loanId) {
    this.displayDecision.loanTypeArr[i].notPay = true;
  }
}

