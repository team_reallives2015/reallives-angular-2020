import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DecisionWindowComponent } from './decision-window.component';

describe('DecisionWindowComponent', () => {
  let component: DecisionWindowComponent;
  let fixture: ComponentFixture<DecisionWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DecisionWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DecisionWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
