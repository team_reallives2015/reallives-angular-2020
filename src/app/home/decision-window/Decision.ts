export class Decision {

  gameEventId: string;
  decisionId: string;
  decisionText: string;
  age: number;
  factroid: string;
  imageFlag: boolean = false;
  choiceArray: any;
  fieldOfStudyListFlag: boolean = false;
  bloodDonationFlag: boolean = false;
  organDonationFlag: boolean = false;
  petFlag: boolean = false;
  textAreaFlag: boolean = false;
  businessFlag: boolean = false;
  loanFullfill: boolean = false;
  loanTypeArr = [];;
}
