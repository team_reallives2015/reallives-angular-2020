import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { Decision } from './Decision';

@Injectable({
  providedIn: 'root'
})
export class DecisionService {

  decison=new Decision();
  organDonationFlag:boolean=false;
  constructor(private http :HttpClient,
              private constantService :ConstantService,
              private commonService :CommonService) { }


  decisionYesNoResultUpdate(game_id,game_event_id,selection){
    return this.http.post<Array<Object>>(`${this.commonService.url}reallives/event/decided`,{game_id,game_event_id,selection});
  }
}
