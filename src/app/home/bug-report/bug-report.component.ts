import { isNotEmpty } from '@amcharts/amcharts4/.internal/core/utils/Utils';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { PostGameService } from '../post-game.service';
import { bugreport } from './bugReport';

@Component({
  selector: 'app-bug-report',
  templateUrl: './bug-report.component.html',
  styleUrls: ['./bug-report.component.css']
})
export class BugReportComponent implements OnInit {
  saveDisableEnableFlag: string;
  disablebuttonAfterOnClick: string;
  bugreport = new bugreport();
  constructor(public modalWindowShowHide :modalWindowShowHide,
             public homeService :HomeService,
             public constantService :ConstantService,
             public postGameService :PostGameService) { }
  @ViewChild('decisionforConfirm') public decisionforConfirm: ModalDirective;

  ngOnInit(): void {
    this.modalWindowShowHide.showBugReport=true;
    this.bugreport.bugDiscription = "";
     this.saveDisableEnableFlag = "disable";
  this.postGameService.getBugReportData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
    res=>{
     let leng= Object.keys(res).length
    if (leng===0){
      this.bugreport.bugDiscription = "";   
     }
     else{
      this.bugreport.bugDiscription = res;   
     }
     })

    // this.postGameService.getBugReportData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
    //   data => {
    //    
    //   })
  }
  ngDoCheck() {
    if ((this.bugreport.bugDiscription === null) ||(this.bugreport.bugDiscription === "") ||((this.bugreport.bugDiscription === undefined))) {
      this.saveDisableEnableFlag = "disable";
    } else {
      this.saveDisableEnableFlag = "";
    }
  }

  saveBugReport() {
    this.disablebuttonAfterOnClick = "disable";
    this.bugreport.userName = this.homeService.allPerson[this.constantService.FAMILY_SELF].userName;
    this.bugreport.country = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    this.bugreport.gameId = this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id;
    let bug={
      "bug":this.bugreport.bugDiscription,
      "username":this.bugreport.userName,
      "country":this.bugreport.country
    }
    this.postGameService.updateBugReportData(this.bugreport.gameId,bug).subscribe(
      data => {
        this.disablebuttonAfterOnClick = "";
        this.decisionforConfirm.show();
      }
    )
  }




}
