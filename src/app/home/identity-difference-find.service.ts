import { Injectable } from '@angular/core';
import { ConstantService } from '../shared/constant.service';

@Injectable({
  providedIn: 'root'
})
export class IdentityDifferenceFindService {
  

  mergeIdentity;
  constructor(private constantService :ConstantService) { }


  findDifferencOfIdentity(localIdentity,differenIdentity){
    this.mergeIdentity = {
      ...localIdentity,
      ...differenIdentity
  };


   return this.mergeIdentity
  }
}
