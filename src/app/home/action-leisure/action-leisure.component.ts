import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { interval } from 'rxjs';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { CommonActionService } from '../common-action.service';
import { HomeService } from '../home.service';
import { LeisureService } from './leisure.service';


@Component({
  selector: 'app-action-leisure',
  templateUrl: './action-leisure.component.html',
  styleUrls: ['./action-leisure.component.css']
})
export class ActionLeisureComponent implements OnInit {
  showAlertFlag:boolean=false;
  alertClass;
  alertMsg;
  alertType;
  allPerson;
  mainPerson;
  leisureData=[];
  selectedLeisureCount:number=0;
  availableLeisureCount:number=0;;
value;
buttonFlag;
checkleisuretime:boolean=false;;
showSuccessMsgFlag:boolean=false;
public messageText;
  public messageClass;
  public MessageShow;
  public messageType;
  public msgText;
  public activeClass:string="";
  public leisure:any;
  public displayLeisure;
  public updatedFiledObject;
  public result;
  public closePopUp:boolean=false;


  constructor(public modalWindowShowHide: modalWindowShowHide,
             public leisureService : LeisureService,
              public homeService: HomeService,
              public constantService: ConstantService,
              private translation :TranslateService,
                            private commonActionService :CommonActionService)
               { 
                interval(30000).subscribe(x => {
                  if(this.showAlertFlag){
                    this.showAlertFlag=false;
                  }
                  });
               }
 

 check(index,key,selected){
                if(selected)
                { 
                  this.selectedLeisureCount=this.selectedLeisureCount+1;
                  if(this.selectedLeisureCount>this.availableLeisureCount)
                  {
                    this.msgText=this.translation.instant('gameLabel.leisureWarrning');
                  this.showSuccessMsgFlag=true;
                   this.leisure[key].selected=selected;
                   this.leisureData[index].active="active";

                  }
                  else{
                    this.leisure[key].selected=selected;
                    this.leisureData[index].active="active";
                  }
                }
                else{
                  this.selectedLeisureCount=this.selectedLeisureCount-1;
                  this.leisure[key].selected=selected;
                  this.leisureData[index].active="";
                  this.activeClass="";

                }
if(this.selectedLeisureCount<=this.availableLeisureCount)
{
  this.buttonFlag=false;
  this.checkleisuretime=false;
}
else{
      this.buttonFlag=true;
  this.checkleisuretime=true;
}

}


  ngOnInit(): void {
    this.allPerson=this.homeService.allPerson;
    if(this.homeService.allPerson[this.constantService.FAMILY_SELF].dead){
      this.alertMsg = this.translation.instant('gameLabel.You cannot take any action because you are dead!');
      this.alertClass = "Warning: ";
      this.alertType = "alert-warning";
      this.showAlertFlag = true;
    }
    else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].age<8 && !this.homeService.allPerson[this.constantService.FAMILY_SELF].dead){
      this.alertMsg = this.translation.instant('gameLabel.You cannot choose any leisure activities before you complete 7 years.');
       this.alertClass = "Warning: ";
      this.alertType = "alert-warning";
      this.showAlertFlag = true;

    }
    if(this.homeService.allPerson[this.constantService.FAMILY_SELF].age<8)
    {
      this.buttonFlag=true;
    }
    this.selectedLeisureCount=this.homeService.allPerson[this.constantService.FAMILY_SELF].current_selected_leisure;
    this.availableLeisureCount=this.homeService.allPerson[this.constantService.FAMILY_SELF].leisure_time_available;
    if( this.selectedLeisureCount=== this.availableLeisureCount){
      this.buttonFlag=true;
    }
    this.getAllLeisure();
  }

  setTranslationForAllLeaisure(){

  }

  getAllLeisure(){
       this.modalWindowShowHide.showWaitScreen=true;
        this.leisureService.getAllLeisure(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
          test=>{
           this.leisure=test;
           for( let i in this.leisure) {   
             this.leisureData.push(this.leisure[i]);
          }
        for(let i=0;i<this.leisureData.length;i++)
        {
          if(this.leisureData[i].leisure_code==='CHURCH'){
            this.leisureData.splice(i, 1);
          }
         if(this.leisureData[i].leisure_code==='ENDURANCE')
         {
          this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Physical_Training')
          if(this.leisureData[i].selected){
            if(this.leisureData[i].enabled){
            this.leisureData[i].active="active";
            }
            else{
              this.leisureData[i].active="";
              this.leisureData[i].selected=false;
              this.selectedLeisureCount=this.selectedLeisureCount-1;
              this.leisureChangeByGame();
            }
          }
          else{
            this.leisureData[i].active="";
          }
         }
         if(this.leisureData[i].leisure_name==='PLAY')
         {
           this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Playing_and_Socializing')
           if(this.leisureData[i].selected){
            if(this.leisureData[i].enabled){
            this.leisureData[i].active="active";
            }
            else{
              this.leisureData[i].active="";
              this.leisureData[i].selected=false;
              this.selectedLeisureCount=this.selectedLeisureCount-1;
              this.leisureChangeByGame();
            }
          }
          else{
            this.leisureData[i].active="";
          }
         }
          if(this.leisureData[i].leisure_code==='ART')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Art')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='MUSICAL')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Music')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='READING_STUDY')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Reading_Study')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='FASHION_CLOTHING_APPEARANCE')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Fashion_Clothing_Appearance')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='TELEVISION_VIEWING')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Television_Viewing')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='SPORTS')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Sports')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='OUTDOOR_ACTIVITIES')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Outdoor_Activities')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='VOLUNTEERING')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Volunteering')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='RELIGIOUS_ACTIVITY')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Religious_Activity')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }
          if(this.leisureData[i].leisure_code==='SOCIAL_OR_POLITICAL_ACTIVITIES')
          {
            this.leisureData[i].leisure_name=this.translation.instant('gameLabel.Social_or_Political_Activities')
            if(this.leisureData[i].selected){
              if(this.leisureData[i].enabled){
              this.leisureData[i].active="active";
              }
              else{
                this.leisureData[i].active="";
                this.leisureData[i].selected=false;
                this.selectedLeisureCount=this.selectedLeisureCount-1;
                this.leisureChangeByGame();
              }
            }
            else{
              this.leisureData[i].active="";
            }
          }

        }
        this.modalWindowShowHide.showWaitScreen=false;

      })
  }  

  saveLeisures(){
    this.modalWindowShowHide.showWaitScreen=true;
     this.leisureService.saveLeisure(this.leisure,this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,this.selectedLeisureCount,this.availableLeisureCount).subscribe(
       res =>{
        this.result=res;
        this.commonActionService.getActionIdentityDiffrence(this.result,this.homeService.allPerson);
          this.msgText=this.translation.instant('gameLabel.Successfully saved your selected leisure activities!');
           this.showSuccessMsgFlag=true;
           this.closePopUp=true;
           this.buttonFlag=true;
           this.modalWindowShowHide.showWaitScreen=false;
       }
     )
  }


  leisureChangeByGame(){
    this.leisureService.saveLeisure(this.leisure,this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,this.selectedLeisureCount,this.availableLeisureCount).subscribe(
      res =>{
       this.result=res;
       this.commonActionService.getActionIdentityDiffrence(this.result,this.homeService.allPerson);
          this.buttonFlag=true;
      }
    )
  }

  successWindowOk(){
    this.showSuccessMsgFlag=false;
    if(this.closePopUp){
    this.modalWindowShowHide.showActionLeisure=false;
    }
  }
  
  close(){
    this.modalWindowShowHide.showActionLeisure=false;
  }
}

