import { TestBed } from '@angular/core/testing';

import { LeisureService } from './leisure.service';

describe('LeisureService', () => {
  let service: LeisureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeisureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
