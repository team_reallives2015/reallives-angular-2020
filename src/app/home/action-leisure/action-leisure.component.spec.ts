import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionLeisureComponent } from './action-leisure.component';

describe('ActionLeisureComponent', () => {
  let component: ActionLeisureComponent;
  let fixture: ComponentFixture<ActionLeisureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionLeisureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionLeisureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
