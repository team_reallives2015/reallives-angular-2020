import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class LeisureService {

  constructor(private http: HttpClient,
    private commonService: CommonService) { }


    getAllLeisure(gameId){
      return this.http.get<Array<Object>>(`${this.commonService.url}game/getAllLeisureSettingsObject/${gameId}`);
    }
    
    saveLeisure(leisure,gameId,selectedCount,availabelCount){
      return this.http.post<boolean>(`${this.commonService.url}game/saveLeisure/${gameId}`,{leisure});
    }
    }
