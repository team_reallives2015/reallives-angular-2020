import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class CharityService {

  constructor(public commonService: CommonService,
    public http: HttpClient) { }


  getAllCharity(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/getCharity/${gameId}`);
  }

  saveCharity(charity, gameId) {
    return this.http.post<boolean>(`${this.commonService.url}game/saveCharity/${gameId}`, { charity });
  }
}
