import { Component, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { CharityService } from './charity.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { EventService } from '../event/event.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-charity',
  templateUrl: './charity.component.html',
  styleUrls: ['./charity.component.css']
})
export class CharityComponent implements OnInit {
  showAlertFlag:boolean=false;
  selectedCharityId;
  alertMsg='';
  alertClass="";
  charityObject = [];
  selectedActionForRedioButton;
  showDecisionFlag: boolean = false;
  selectedCharityType = ''
  result;
  diiferenBetIdentity;
  messageText;
  successMsgFlag: boolean = false;
  notMakingCharityFlag: boolean = false;
  min = 0;
  max = 0;
  title;
  selectedCharityAmount = 0;
  donationCertificateFlag: boolean = false;
  certificateButtonFlag:boolean=false;
  constructor(public charityService: CharityService,
    public translate: TranslateService,
    public homeService: HomeService,
    public eventService: EventService,
    public agaAYearService: AgaAYearService,
    public constantService: ConstantService,
    public modalWindowShowHide: modalWindowShowHide,
    public commonService :CommonService) { }
  ngOnInit(): void {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 10) {
      this.successMsgFlag = true;
      this.messageText = this.translate.instant('gameLabel.charity_wrrr');
      this.notMakingCharityFlag=true;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
      this.messageText = this.translate.instant('gameLabel.You_cannot_take_any_action_because_you_are_dead');
      this.successMsgFlag = true;
    }
    else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash<=1){
      this.successMsgFlag = true;
      this.messageText = this.translate.instant('gameLabel.You dont have enough money to make a charitable donation');
    }
    else {
      if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
        if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId === 130) &&
          (this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobId === 999 ||
            this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobId === 131 ||
            this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobId === 133 ||
            this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobId === 132)) {
          this.successMsgFlag = true;
          this.messageText = this.translate.instant('gameLabel.You are currently unemployed, so you cannot donate to charity');
          this.notMakingCharityFlag = true;
        }
        else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash<=1){
          this.successMsgFlag = true;
          this.messageText = this.translate.instant('gameLabel.You dont have enough money to make a charitable donation');
        }
      }
    }
    this.cretaeCharityObject();
    this.checkViewCertificateButton();
  }


  checkViewCertificateButton(){
    if((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity).hasOwnProperty('totalFood') || 
     ( this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity).hasOwnProperty('totalOrphanage')  ||
     ( this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity).hasOwnProperty('totalShelter')   ||
      (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity).hasOwnProperty('totalEnvironment'))
{
         this.certificateButtonFlag=false
      }
      else{
        this.certificateButtonFlag=true;
      }
  }

  cretaeCharityObject() {
    this.modalWindowShowHide.showWaitScreen = true
    this.charityService.getAllCharity(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        let obj = [], img, j = 1, desc, charityName;;
        obj = res;
        for (let i = 0; i < obj.length; i++) {
          let no = "charity" + j;
          img = "assets/images/organs/" + no + ".svg"
          if (obj[i].charityId === 0) {
            desc = this.translate.instant('gameLabel.charity_hunger')
            charityName = this.translate.instant('gameLabel.charity_type1')
          }
          else if (obj[i].charityId === 1) {
            desc = this.translate.instant('gameLabel.charity_shelter')
            charityName = this.translate.instant('gameLabel.charity_type2')
          }
          else if (obj[i].charityId === 2) {
            desc = this.translate.instant('gameLabel.charity_env')
            charityName = this.translate.instant('gameLabel.charity_type3')
          }
          else if (obj[i].charityId === 3) {
            desc = this.translate.instant('gameLabel.charity_orph')
            charityName = this.translate.instant('gameLabel.charity_type4')
          }
      if(obj[i].max>0){
          this.charityObject.push(
            {
              "value": "",
              "charityName": charityName,
              "min": obj[i].min,
              "max": obj[i].max,
              "img": img,
              'desc': desc,
              'charityId':obj[i].charityId
            }
          )
      }
          j++;

        }
        this.modalWindowShowHide.showWaitScreen = false
      }
    )
  }


  setSlectedOption(index) {
    this.showDecisionFlag = true
    this.selectedCharityAmount = 0;
    this.selectedCharityId=0;
    for (let i = 0; i < this.charityObject.length; i++) {
      if (i === index) {
        this.selectedCharityType = this.charityObject[i].charityName
        this.min = (this.charityObject[i].min)
        this.max = (this.charityObject[i].max)
        this.charityObject[i].value = "Active"
        this.selectedCharityAmount=0;
        this.selectedCharityId=this.charityObject[i].charityId
      }
      else {
        this.charityObject[i].value = ""
      }
    }
    this.translate.get('gameLabel.charity_title', { selectedCharityType: this.selectedCharityType }).subscribe((s: String) => {
      this.title = s;
    })
  }

  save() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash > this.selectedCharityAmount) {
      this.modalWindowShowHide.showWaitScreen = true
      this.showDecisionFlag = false;
      // this.modalWindowShowHide.showCharity = false;
      let charity = {
        "charityCost": this.selectedCharityAmount,
        "charityId": this.selectedCharityId
      }
      this.charityObject[this.selectedCharityId].value = ""
      this.charityService.saveCharity(charity, this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
        res => {
          this.result = res;
          if (this.result.data.result.hasOwnProperty('difference')) {
            if (Object.keys(this.result.data.result.difference).length !== 0) {
              this.diiferenBetIdentity = this.result.data.result.difference;
              let retuenValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
              if (retuenValue !== 'false') {
                this.homeService.allPerson = retuenValue;
                this.homeService.changeAllPerson.emit(this.homeService.allPerson);
              }
            }
          }
          if (this.result.data.result.type === 'event' || this.result.data.result.type === 'result') {
            this.agaAYearService.nextEventResultIsEvent(this.result);
            this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
          }
          this.donationCertificateFlag=true;
          this.checkViewCertificateButton();
          this.modalWindowShowHide.showWaitScreen = false
        }

      )
    }
    else {
      this.successMsgFlag = true;
      this.messageText = this.translate.instant('gameLabel.You dont have enough money to make a charitable donation');

    }

  }

  OnCloseWindowPopup() {
    this.showDecisionFlag = false
    this.modalWindowShowHide.showCharity = false;
  }

  cancel() {
    for (let i = 0; i < this.charityObject.length; i++) {
      this.charityObject[i].value = ""
    }
    this.showDecisionFlag = false;
  }

  successMsgWindowClose() {
    this.successMsgFlag = false;
    this.modalWindowShowHide.showCharity=false;
  }

  viewCertificate() {
    this.donationCertificateFlag = true
  }

  closeCertificate() {
    this.donationCertificateFlag = false;
    this.modalWindowShowHide.showCharity = false
  }
}
