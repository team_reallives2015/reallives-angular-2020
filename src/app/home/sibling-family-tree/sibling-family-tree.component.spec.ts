import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiblingFamilyTreeComponent } from './sibling-family-tree.component';

describe('SiblingFamilyTreeComponent', () => {
  let component: SiblingFamilyTreeComponent;
  let fixture: ComponentFixture<SiblingFamilyTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiblingFamilyTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiblingFamilyTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
