export class Event {
  gameEventId: string;
  eventId: string;
  text: string;
  age: number;
  oldTraits: any;
  newTraits: any;
  factroid: string = '';
  link: string;
  addline: string;
  expression: string;
  type: string;
  enableAction: String
  identity: String;
}
