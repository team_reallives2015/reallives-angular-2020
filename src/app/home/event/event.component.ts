import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { HomeService } from '../home.service';
import { WellBeingIndicatorService } from '../well-being-indicator/well-being-indicator.service';
import { EventService } from './event.service';
import { FamilyCapsuleService } from '../family-capsule/family-capsule.service';
import { CommonService } from 'src/app/shared/common.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  animations: [
    trigger('movementtrigger', [
      state('nextevent', style({ opacity: 1 })),
      state('prevevent', style({ opacity: 1 })),
      state('nextevent2', style({ opacity: 1 })),
      state('prevevent2', style({ opacity: 1 })),
      transition('nextevent => prevevent', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ]),
      transition('prevevent => nextevent', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ]),
      transition('prevevent => prevevent2', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ]),
      transition('prevevent2 => prevevent', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ]),
      transition('nextevent => nextevent2', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ]),
      transition('nextevent2 => nextevent', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ]),
      transition('prevevent2 => nextevent', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ]),
      transition('nextevent2 => prevevent', [
        animate(1000, keyframes([
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, 90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 0, transform: 'rotate3d(0, 1, 0, -90deg) translate3d(0, 0, 410px)' }),
          style({ opacity: 1, transform: 'rotate3d(0, 1, 0, 0deg) translate3d(0, 0, 410px)' })
        ]))
      ])
    ])
  ]

})
export class EventComponent implements OnInit {
  subscribe;
  subscribe1;
  allPerson;
  mainPerson;
  eventlength: number;
  readMoreFlag: boolean = true;
  indexdata;
  @Input() data: boolean = false;
  addCssForDisableNext = "";
  addCssForDisablePrivoius = "";
  dummyEvent;
  event;
  updatedTextForEvent;
  singleEvent;
  typeExpression;
  public messageText;
  public messageClass;
  public MessageShow;
  public messageType;
  public msgText;
  public activeClass: string = "";
  succesWindowFlag: boolean = false;
  updateEventTextFlag: boolean = false;
  updateEventTextSaveButonFlag: boolean = false;
  oldExpression: string;
  cnt = 0;
  changeExpFlag: boolean = false;
  factroidLength = 0;
  bornCountry;
  registerCountry;
  bOne: number = 0;
  bTwo: number = 0;
  bThree: number = 0;
  bFour: number = 0;
  rOne: number = 0;
  rTwo: number = 0;
  rThree: number = 0;
  rFour: number = 0;
  registerStatusData;
  chanceOfPoorB: number = 0;
  chanceOfPoorR: number = 0;
  firstR: number = 0;
  moreInfoFlag: boolean = false;
  moreInfoTitle;
  moreInforStm;
  statusClassOne = "";
  statusClassTwo = "";
  statusClassThree = "";
  statusClassFour = "";
  statusClassFive = ""
  constructor(public eventService: EventService,
    public homeService: HomeService,
    public constanrService: ConstantService,
    public wellBeingIndicatorService: WellBeingIndicatorService,
    public agaAYearService: AgaAYearService,
    public constantService: ConstantService,
    public modelWindowShowHideService: modalWindowShowHide,
    public familyCapsuleService: FamilyCapsuleService,
    public commonService: CommonService,
    public translate: TranslateService) {

    this.subscribe = this.homeService.changeAllPerson.subscribe(
      persons => {
        this, homeService.allPerson = persons
        this.calculateTableValues();
      }
    )
    this.subscribe1 = this.homeService.statusEmitter.subscribe(
      persons => {
        this, homeService.allPerson = persons
        this.calculate();
      }
    )
  }

  ngOnInit(): void {
    this.allPerson = this.homeService.allPerson;
    this.mainPerson = this.allPerson[this.constanrService.FAMILY_SELF];
    this.cnt = 0;
    this.eventService.loadEvent();
    this.calculateTableValues();
    this.calculate();
  }

  nextEvent() {
    this.indexdata = 1;
    let i = 0, index = 0;
    for (i; i < this.eventService.allEvents.length; i++) {
      if (this.eventService.currentEvent.gameEventId === this.eventService.allEvents[i].gameEventId) {
        index = i + 1;
        if (index === this.eventService.allEvents.length) {
          this.addCssForDisableNext = "disable";
          this.addCssForDisablePrivoius = "pointer";
          this.eventService.state == 'nextevent2'
        }
        else if (index < this.eventService.allEvents.length) {
          this.eventService.updateCurrentEvent(index);
          this.wellBeingIndicatorService.generateWellBeingIndicator(this.eventService.allEvents[index]);
          this.addCssForDisableNext = "pointer";
          this.addCssForDisablePrivoius = "pointer";
          //for animation
          if (this.eventService.state == 'nextevent') {
            this.eventService.state = 'nextevent2';
          } else {
            this.eventService.state = 'nextevent';
          }
        }
        else {
          this.addCssForDisableNext = "disable";
          this.addCssForDisablePrivoius = "pointer";
        }
        break;
      }
    }
  }
  previousEvent() {
    this.indexdata = 0;
    let i = 0, index = 0;
    for (i = 0; i < this.eventService.allEvents.length; i++) {
      if (this.eventService.currentEvent.gameEventId === this.eventService.allEvents[i].gameEventId) {
        {
          index = i - 1;
          this.indexdata = index;
          if (index >= 0) {
            this.eventService.updateCurrentEvent(index);
            this.wellBeingIndicatorService.generateWellBeingIndicator(this.eventService.allEvents[index]);
            this.addCssForDisablePrivoius = "pointer";
            this.addCssForDisableNext = "pointer";
            //for animation
            if (this.eventService.state == 'prevevent') {
              this.eventService.state = 'prevevent2';
            } else {
              this.eventService.state = 'prevevent';
            }
          }
          else {
            this.addCssForDisablePrivoius = "disable";
            this.addCssForDisableNext = "pointer";
          }
          break;

        }

      }
    }



  }
  saveExpression() {
    if (!this.changeExpFlag) {
      this.oldExpression = this.eventService.currentEvent.expression;
    }
    for (let i = 0; i < this.eventService.allEvents.length; i++) {
      if (this.eventService.currentEvent.gameEventId === this.eventService.allEvents[i].gameEventId) {
        if (this.eventService.currentEvent.expression === this.oldExpression) {
          this.succesWindowFlag = true;
          this.messageText = this.translate.instant('gameLebel.your_expression_same_as_privoius_expression.');
        }
        else {
          this.eventService.saveExpression(this.eventService.currentEvent.gameEventId, this.eventService.currentEvent.expression).subscribe(
            res => {
              if (res) {
                this.succesWindowFlag = true;
                this.changeExpFlag = false;
                this.messageText = this.translate.instant('gameLabel.Successfully_saved_your_expression_to_your_life_summary');
                this.cnt = 0;
              }
              else {
                alert("Error Occures");
              }
            })
        }
        break;
      }
    }

  }
  okClick() {
    this.succesWindowFlag = false;
  }

  change() {
    if (this.updatedTextForEvent === '' || this.updatedTextForEvent === null) {
      this.updateEventTextSaveButonFlag = false;
    }
    else {
      this.updateEventTextSaveButonFlag = true;

    }
  }

  chaneExpression() {
    this.cnt = this.cnt + 1;
    if (this.cnt === 1) {
      this
      this.oldExpression = this.eventService.currentEvent.expression;
      this.oldExpression = this.oldExpression.slice(0, -1)
      this.changeExpFlag = true;
    }
    let str: string = this.eventService.currentEvent.expression;
    this.eventService.currentEvent.expression = str.trim();
  }
  updateEventTextButtonClick() {
    this.updateEventTextFlag = true;
  }

  close() {
    this.updateEventTextFlag = false;
  }
  saveUpdatedTextForEvent() {
    let updatedText;
    updatedText = {
      "oldText": this.eventService.currentEvent.text,
      "newText": this.updatedTextForEvent
    }
    this.eventService.updateTextOfEventForUser(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, updatedText).subscribe(
      res => {
        alert("save Succesfully");
      }
    )
  }

  clickResidenceAction() {
    this.modelWindowShowHideService.showActionResidence = true;
  }

  calculateTableValues() {

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country.length > 8) {
      this.bornCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Code;
    }
    else {
      this.bornCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country.length > 8) {
      this.registerCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.Code;
    }
    else {
      this.registerCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    }
    this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.countryid).subscribe(
      res => {
        this.registerStatusData = res[0]
        //value for register
        this.chanceOfPoorR = this.registerStatusData.chanceOfPoor * 100;
        let meanIncomeR = this.registerStatusData.meanIncome
        let incomeDeviationR = this.registerStatusData.incomeDeviation
        let f1R = (meanIncomeR + (this.constantService.Economically_Challenged) * incomeDeviationR)
        let f3R = (Math.exp(f1R))
        let firstR = (f3R);
        this.firstR = firstR;
        let secondR = (Math.exp(meanIncomeR + (this.constantService.Bearly_Managed * incomeDeviationR)));
        let thirdR = (Math.exp(meanIncomeR + (this.constantService.Well_to_do * incomeDeviationR)));
        let fourthR = (Math.exp(meanIncomeR + (this.constantService.Rich * incomeDeviationR)));
        this.rOne = (firstR + secondR) / (firstR);
        this.rTwo = (secondR + thirdR) / (firstR);
        this.rThree = (thirdR + fourthR) / (firstR);
        this.rFour = (fourthR) / (firstR);

        //value for born
        let first = this.commonService.firstValueI;
        let second = this.commonService.secondValueI;
        let third = this.commonService.thirdValueI;
        let fourth = this.commonService.fourthValueI;
        this.bOne = (first + second) / (first);
        this.bTwo = (second + third) / (first);
        this.bThree = (third + fourth) / (first);
        this.bFour = (fourth) / (first);

      }
    )
  }

  calculate() {
    let status = this.homeService.allPerson[this.constanrService.FAMILY_SELF].current_economical_status
    if (status === 0) {
      this.statusClassOne = "boootom";
      this.statusClassTwo = "";
      this.statusClassThree = "";
      this.statusClassFour = "";
      this.statusClassFive = "";
    }
    else if (status === 1) {
      this.statusClassOne = "";
      this.statusClassTwo = "boootom1";
      this.statusClassThree = "";
      this.statusClassFour = "";
      this.statusClassFive = "";
    }
    else if (status === 2) {
      this.statusClassOne = "";
      this.statusClassTwo = "";
      this.statusClassThree = "boootom2";
      this.statusClassFour = "";
      this.statusClassFive = "";
    }
    else if (status === 3) {
      this.statusClassOne = "";
      this.statusClassTwo = "";
      this.statusClassThree = "";
      this.statusClassFour = "boootom3";
      this.statusClassFive = "";
    }
    else if (status === 4) {
      this.statusClassOne = "";
      this.statusClassTwo = "";
      this.statusClassThree = "";
      this.statusClassFour = "";
      this.statusClassFive = "boootom4";
    }
  }
  moreInfo() {
    this.moreInfoFlag = true;
    let s1 = this.translate.instant('gameLabel.status_stm_tlp1');
    let s2 = this.translate.instant('gameLabel.status_stm_tlp2');
    let s3 = this.translate.instant('gameLabel.status_stm_tlp3');
    let s4 = this.translate.instant('gameLabel.status_stm_tlp4');
    let s5 = this.translate.instant('gameLabel.status_stm_tlp5');
    this.moreInfoTitle = s1 + "<br><br>"
    this.moreInforStm = s2 + "<br><br>" + s3 + "<br><br>" + s4 + "<br><br>" + s5;
  }

  successMsgWindowClose() {
    this.moreInfoFlag = false;
  }
  ngOnDestroy() {
    this.subscribe.unsubscribe();
    this.subscribe1.unsubscribe();

  }
}
