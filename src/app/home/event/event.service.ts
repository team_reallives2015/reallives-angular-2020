import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { HomeService } from '../home.service';
import { WellBeingIndicatorService } from '../well-being-indicator/well-being-indicator.service';
import { Event }  from './Event'



 
@Injectable({
  providedIn: 'root'
})
export class EventService {
  allEvents=[];
  expressionFlag:boolean=true;
  currentEvent=new Event();
  allEventLength;
  state: string = 'nextevent';
  disableBackButton;
  disableNextButton;
  singleEvent=new Event();
  eventList=[];
  addCssForDisableNext="";
  newExpression;
  


  constructor(public wellBeingIndicatorService : WellBeingIndicatorService,
    public homeService : HomeService,
    public modalWindowShowHide :modalWindowShowHide,
    private http:HttpClient,
     public commonService :CommonService,
     private constantService :ConstantService) { 

  }


  loadEvent(){
    this.allEvents=[];
        this.allEvents=this.homeService.allEvent;
        this.updateCurrentEvent(this.allEvents.length-1);
        this.wellBeingIndicatorService.generateWellBeingIndicator(this.currentEvent);
    }

   


  updateCurrentEvent(index) {
    this.currentEvent = this.allEvents[index];   
  }



  saveExpression(gameEventId,expression){
    return this.http.post<Array<Object>>(`${this.commonService.url}gameEvent/saveExpression/${gameEventId}`,{expression});
  }

  updateTextOfEventForUser(gameId,languageSuggestion){
    return this.http.post<any>(`${this.commonService.url}reallives/translation/languageSuggestion/${gameId}`,{languageSuggestion});
  }

  enableActionAsPerEvent(event,self){
    switch (event.enableAction) {
      case this.constantService.FINANCE_MODEL_WINDOW_POPUP :
        setTimeout(() => {
            this.modalWindowShowHide.showActionFinanace = true;
          },
          3000);
        break;
      case this.constantService.CAREER_MODEL_WINDOW_POPUP :
        setTimeout(() => {
            this.modalWindowShowHide.showActionCareer = true;
          },
          3000);
        break;
      case this.constantService.RELATION_MODEL_WINDOW_POPUP :
        setTimeout(() => {
            this.modalWindowShowHide.showActionRelation = true;
          },
          3000);
        break;
      case this.constantService.RESIDENCE_MODEL_WINDOW_POPUP :
        setTimeout(() => {
            this.modalWindowShowHide.showActionResidence = true;
          },
          3000);
        break;
      case this.constantService.EDUCATION_MODEL_WINDOW_SHOW :
        setTimeout(() => {
            this.modalWindowShowHide.showActionEducation = true;
          },
          3000);
        break;
      case this.constantService.LEISURE_MODEL_WINDOW_SHOW :
        if(self.leisure_time_available>0){
        setTimeout(() => {
            this.modalWindowShowHide.showActionLeisure = true;
          },
          3000);
        }
        break;
      case this.constantService.NO_ACTION :
        break;
  
    }
  }

 

    }
