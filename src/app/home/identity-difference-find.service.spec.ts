import { TestBed } from '@angular/core/testing';

import { IdentityDifferenceFindService } from './identity-difference-find.service';

describe('IdentityDifferenceFindService', () => {
  let service: IdentityDifferenceFindService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IdentityDifferenceFindService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
