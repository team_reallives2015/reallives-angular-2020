import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { PostGameService } from '../post-game.service';


@Component({
  selector: 'app-write-letter',
  templateUrl: './write-letter.component.html',
  styleUrls: ['./write-letter.component.css']
})
export class WriteLetterComponent implements OnInit {
  rightALetterData="";
  editorDisableFlag:boolean=true;
  letterData="";
  addCssForDisable="";
  successText: string = '';
  constructor(public modalWindowShowHide : modalWindowShowHide,
              public postGameService : PostGameService,
              public homeService :HomeService,
              public constantService :ConstantService) { }
              @ViewChild('decisionforConfirm') public decisionforConfirm: ModalDirective;

  ngOnInit(): void {
    this.modalWindowShowHide.showLetter=true;
    this.postGameService.getLetterData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      data =>{
        if(data.length===0){
          this.letterData="";
        }else{
        this.rightALetterData=data[0];
        this.letterData=this.rightALetterData;
        if(this.letterData==null || this.letterData==''){
          this.addCssForDisable="disable";
        }
        else
        {
          this.addCssForDisable="";
        }
      }
    });
  }

  change(){
    let str: string = this.letterData;
    this.letterData = str.trim();
    if(this.letterData === null || this.letterData === '' ||  !(this.letterData.replace(/\s/g, '').length) || this.letterData.length === 0){
      this.addCssForDisable = "disable";
    }
    else
    {
      this.addCssForDisable = "";
    }
  }

  saveLetter(){
    let str: string = this.letterData;
    this.letterData = str.trim();
    if(this.letterData === null || this.letterData === '' || this.letterData === ' ' || !(this.letterData.replace(/\s/g, '').length)) {

      this.successText = 'Right correct Data';
      this.decisionforConfirm.show();
      // alert("Right correct Data");
    }else {
      this.postGameService.updateLetterData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,this.letterData).subscribe(
        data => {
          this.successText = 'Your letter saved succesfully.';
          this.decisionforConfirm.show();
          this.editorDisableFlag = true;
        }
      )
    }

}

editLetter(){
  // this.postGameService.getLetterData(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
  //   data =>{
  //     this.rightALetterData=data[0];
      this.letterData=this.rightALetterData;
      this.editorDisableFlag=false;
  //   }
  // )

}
}
