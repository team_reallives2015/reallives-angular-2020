import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionResidenceComponent } from './action-residence.component';

describe('ActionResidenceComponent', () => {
  let component: ActionResidenceComponent;
  let fixture: ComponentFixture<ActionResidenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionResidenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionResidenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
