import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';

@Injectable({
  providedIn: 'root'
})
export class ResidenceService {

  constructor(private http: HttpClient,
    public homeService :HomeService,
    public commonService :CommonService,
    public constantService :ConstantService) { }


    getAllResidenceList(gameId){
      return this.http.get<Array<Object>>(`${this.commonService.url}game/residence/getAllResidenceData/${gameId}`);   
     }

     saveResidence(residence){
        return this.http.post<Array<Object>>(`${this.commonService.url}game/residence/saveResidence`,{residence});

     }

     getCountryList(){
      return this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/residence/country/true`);
    }
    
     getCity(countryId){
        return  this.http.get<Array<Object>>(`${this.commonService.url}game/actionbar/residence/city/${countryId}`);
      }

      getActionNextEvent(nextEventObject){
        return  this.http.post<Array<Object>>(`${this.commonService.url}game/actionEventNext`,{nextEventObject});
      }
}

