import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { interval } from 'rxjs';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { FinanceService } from '../action-finance/finance.service';
import { AgaAYearService } from '../age-a-year/aga-a-year.service';
import { CommonActionService } from '../common-action.service';
import { EventService } from '../event/event.service';
import { HomeService } from '../home.service';
import { ResidenceService } from './residence.service';


@Component({
  selector: 'app-action-residence',
  templateUrl: './action-residence.component.html',
  styleUrls: ['./action-residence.component.css']
})
export class ActionResidenceComponent implements OnInit {

  allPerson;
  mainPerson;
  allResidenceList;
  text;
  messageText;
  messageType;
  messageClass = "alert-info";
  MessageShow;
  livingWith;
  showAlertFlag: boolean = false;
  alertClass;
  alertMsg;
  alertType;
  confirmBoxMoveOutShowFlag: boolean = false;
  confirmBoxMoveInShowFlag: boolean = false;
  saveResidenceObject;
  firstCardData;
  updatedObject;
  successMsgFlag: boolean = false;
  allDisplayResidenceList;
  confirmBoxMoveChangeCityFlag: boolean = false;
  confirmBoxChangeCountryShowFlag: boolean = false;
  dispalyCityListFlag: boolean = false;
  displayCountryListFlag: boolean = false;
  cityList;
  countryList;
  selectedCityId;
  selectedCityObject;
  selectedCountryObject;
  selectedCOuntryId;
  setSelectedCityCountryShow: boolean = false;
  type;
  showCityList: boolean = false;
  showCountryList: boolean = false;
  countryId;
  confirmChangeCityThroughCoutry: boolean = false;
  moneyNeeded;
  moneyNeededlocal;
  firstValue;
  newCountryPPP;
  illegalEmigrateFlag: boolean = false;
  illegalYesFlag: boolean = false;
  map: boolean = false;
  lat;
  lng;
  zoom = 3;
  labelName;
  beforChangeCountryObjectForMap;
  start_end_mark = [];
  latlng = [];
  result;
  diiferenBetIdentity;
  identity = [];
  identityValues = [];
  secondLangCode;
  previous;
  subscribe;
  constructor(public modalWindowShowHide: modalWindowShowHide,
    private residenceService: ResidenceService,
    public homeService: HomeService,
    public constantService: ConstantService,
    public commonService: CommonService,
    public translation: TranslateService,
    public agaAYearService: AgaAYearService,
    public eventService: EventService,
    private commonActionService: CommonActionService,
    public financeService: FinanceService,
    public gameSummary: GameSummeryService) {
    this.subscribe = this.homeService.changeAllPerson.subscribe(
      data => {
        this.homeService.allPerson = data;
      })
    interval(30000).subscribe(x => {
      if (this.showAlertFlag) {
        this.showAlertFlag = false;
      }
    });
  }

  ngOnInit(): void {
    this.allPerson = this.homeService.allPerson;
    this.countryId = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid;;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence.length === 1) {
      this.allDisplayResidenceList = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence.length > 1) {
      this.allDisplayResidenceList = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence));
      this.allDisplayResidenceList = this.allDisplayResidenceList.reverse();
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
      this.alertMsg = this.translation.instant('gameLabel.You_cannot_take_any_action_because_you_are_dead');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 11) {
      this.alertMsg = this.translation.instant('gameLabel.You_cannot_move_out_before_you_are_11_year_old.');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    if (this.allPerson[this.constantService.FAMILY_MOTHER].living_at_home || !this.homeService.allPerson[this.constantService.FAMILY_FATHER].dead) {
      this.livingWith = this.constantService.ACTION_RESIDENCE_PARENT_HOUSE;
    }
    else if (this.allPerson[this.constantService.FAMILY_MOTHER].dead || this.homeService.allPerson[this.constantService.FAMILY_FATHER].dead) {
      this.livingWith = this.constantService.ACTION_RESIDENCE_OWN_HOUSE;
    }
    else {
      this.livingWith = this.constantService.ACTION_RESIDENCE_OWN_HOUSE;
    }
  }

  moveOut() {
    if ((((this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobName !== this.constantService.JOB_NO_JOB) && (this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobName !== this.constantService.JOB_STUDENT) && ((this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobName !== this.constantService.JOB_STUDENT)) &&
      (this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobName !== this.constantService.JOB_UNEMPLOYED)
      && ((this.homeService.allPerson[this.constantService.FAMILY_SELF].job.JobName !== this.constantService.JOB_DOMESTIC_CHORES)))
      || ((this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId !== 130))) || (this.homeService.allPerson[this.constantService.FAMILY_SELF].sex === 'F' && this.homeService.allPerson[this.constantService.FAMILY_SELF].married && this.homeService.allPerson[this.constantService.FAMILY_SELF].husband !== null)) {
      this.messageText = this.translation.instant('gameLabel.Are_you_sure_you_want_to_get_out_of_your_parents_house?');
      this.confirmBoxMoveOutShowFlag = true;
      this.confirmBoxMoveInShowFlag = false;
      this.confirmBoxMoveChangeCityFlag = false;
      this.confirmBoxChangeCountryShowFlag = false;
      this.illegalEmigrateFlag = false;
    }
    else {
      this.messageText = this.translation.instant('gameLabel.You_cannot_move_out_because_you_do_not_have_a_job._Go_to_career_action_and_take_a_job_or_start_a_business.');
      this.messageClass = "errorpopup";
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }

  }

  moveIn() {

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId !== 130) {
      this.messageText = this.translation.instant('gameLabel.You_cannot_move-in_as_you_have_an_active_business.');
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    } else if ((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdInvestmentValue > 0 ||
      this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities > 0)
      &&
      !(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country === this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].country.country)) {
      this.messageText = this.translation.instant('gameLabel.You_have_outstanding_investments_or_loans.');
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].not_apply_for_inheritance) {
      if (this.commonService.getRandomValue(0, 1) > 0.8) {
        this.confirmBoxMoveOutShowFlag = false;
      }
      else {
        this.messageText = this.translation.instant('gameLabel.Do_you_want_to_shift_back_to_your_parents_home?');
        this.confirmBoxMoveInShowFlag = true;

      }
    }
    else if (this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].mother].dead && this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].father].dead) {
      this.messageText = this.translation.instant('gameLabel.You can not move in beacause your parents are no more');
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
    else {
      this.messageText = this.translation.instant('gameLabel.Do_you_want_to_shift_back_to_your_parents_home?');
      this.confirmBoxMoveInShowFlag = true;

    }
  }

  changeCity() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId !== 130) {
      this.messageText = this.translation.instant('gameLabel.You_cannot_change_your_current_city_as_you_have_an_active_business.');
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
      this.confirmBoxMoveChangeCityFlag = false;
    } else {
      this.messageText = this.messageText = this.translation.instant('gameLabel.Do_you_really_want_to_change_your_present_city?');

      this.confirmBoxMoveChangeCityFlag = true;
    }
  }

  moveBackToHomeCountry() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.confirmBoxMoveInShowFlag = false;
    this.livingWith = this.constantService.ACTION_RESIDENCE_OWN_HOUSE;
    this.saveResidenceObject = { "actionType": "MOVE_IN", "gameId": this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, "personId": this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id, "shelterIndex": this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex }
    this.residenceService.saveResidence(this.saveResidenceObject).subscribe(res => {
      this.result = res;
      this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
      this.allDisplayResidenceList = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence));
      this.allDisplayResidenceList = this.allDisplayResidenceList.reverse();
      this.messageText = this.translation.instant('gameLabel.You_have_successfully_moved_back_into_your_home_country!');;
      this.messageType = "Success: ";
      this.messageClass = "sucesspopup";
      this.successMsgFlag = true;
      this.modalWindowShowHide.showWaitScreen = false;
    })
  }
  changeCountry() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId !== 130) {
      this.messageText = this.translation.instant('gameLabel.You_cannot_emigrate_as_you_have_an_active_business_in_your_current_country.');
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
      this.confirmBoxChangeCountryShowFlag = false;
    } else {
      this.messageText = this.translation.instant('gameLabel.Do_you_really_want_to_change_your_present_country?');
      this.confirmBoxChangeCountryShowFlag = true;
    }


  }

  confirmMoveOutYes() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.confirmBoxMoveOutShowFlag = false;
    this.saveResidenceObject = { "actionType": "MOVE_OUT", "gameId": this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, "personId": this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id, "shelterIndex": this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex }
    this.residenceService.saveResidence(this.saveResidenceObject).subscribe(res => {
      this.result = res;
      this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
      this.gameSummary.getWelathMetaData(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
        res => {

          if (res !== 'false') {
            this.gameSummary.bornCountryWelathMetadata = res;
          }
          else {
            this.gameSummary.bornCountryWelathMetadata = false;
          }
          this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
            res => {
              this.gameSummary.ecoStatusData = res[0];
              this.allDisplayResidenceList = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence.reverse();
              this.livingWith = this.constantService.ACTION_RESIDENCE_OWN_HOUSE;
              //handle second event
              if (this.result.data.result.hasOwnProperty('actionNextEvent')) {
                let result1;
                this.secondLangCode = this.result.data.result.actionLangCode;
                let nextEventObject = { langCode: this.secondLangCode, gameId: this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id };
                this.residenceService.getActionNextEvent(nextEventObject).subscribe(
                  res1 => {
                    result1 = res1;
                    this.commonActionService.getActionEventAndIdentityDifference(result1, this.homeService.allPerson);
                    this.allDisplayResidenceList = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence));
                    this.allDisplayResidenceList = this.allDisplayResidenceList.reverse();
                    this.livingWith = this.constantService.ACTION_RESIDENCE_OWN_HOUSE;
                    this.messageText = this.translation.instant('gameLabel.You_have_successfully_moved_from_your_family_home!');
                    this.messageType = "Success: ";
                    this.messageClass = "sucesspopup";
                    this.successMsgFlag = true;
                    this.modalWindowShowHide.showWaitScreen = false;
                  })
              }
              else {
                this.messageText = this.translation.instant('gameLabel.You_have_successfully_moved_from_your_family_home!');
                this.messageType = "Success: ";
                this.messageClass = "sucesspopup";
                this.successMsgFlag = true;
                this.modalWindowShowHide.showWaitScreen = false;
              }
            })
        })
    })
  }

  confirmMoveOutNo() {
    this.confirmBoxMoveOutShowFlag = false;
  }

  confirmMoveInYes() {
    this.modalWindowShowHide.showWaitScreen = true;
    this.confirmBoxMoveInShowFlag = false;
    this.saveResidenceObject = { "actionType": "MOVE_IN", "gameId": this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, "personId": this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id, "shelterIndex": this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex }
    this.residenceService.saveResidence(this.saveResidenceObject).subscribe(res => {
      this.result = res;
      //for purposr to claculate status from new country data these changes done
      //get difference
      this.diiferenBetIdentity = this.result.data.result.difference;
      //get Differen and merge
      let returnValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
      // if(returnValue!=='false'){
      this.homeService.allPerson = returnValue;
      let allPersonkeys = Object.keys(this.homeService.allPerson)
      for (let identity of allPersonkeys) {
        if (!this.diiferenBetIdentity.hasOwnProperty(identity)) {
          if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
            && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
              this.homeService.setlang = this.homeService.allPerson.selected_language
            delete this.homeService.allPerson[identity]
          }
        }
      }

      //
      this.gameSummary.getWelathMetaData(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
        res => {

          if (res !== 'false') {
            this.gameSummary.bornCountryWelathMetadata = res;
          }
          else {
            this.gameSummary.bornCountryWelathMetadata = false;
          }
          this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
            res => {
              this.gameSummary.ecoStatusData = res[0];
              //emit emiter for difference
              this.homeService.changeAllPerson.emit(this.homeService.allPerson);
              //
              //set event
              this.agaAYearService.nextEventResultIsEvent(this.result);
              this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
              this.allDisplayResidenceList = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence));
              this.allDisplayResidenceList = this.allDisplayResidenceList.reverse();
              this.livingWith = this.constantService.ACTION_RESIDENCE_PARENT_HOUSE;
              this.messageText = this.translation.instant('gameLabel.You_have_successfully_moved_back_into_your_family_home!');;
              this.messageType = "Success: ";
              this.messageClass = "sucesspopup";
              this.successMsgFlag = true;
              this.modalWindowShowHide.showWaitScreen = false;
            })
        })
    })
  }

  confirmMoveInNo() {
    this.confirmBoxMoveInShowFlag = false;
  }

  confirmChangeCityYes() {
    this.showCountryList = false;
    this.confirmBoxMoveChangeCityFlag = false;
    this.dispalyCityListFlag = true;
    this.type = this.translation.instant('gameLabel.Select_a_city_in_which_you_can_migrate.');
    this.showCityList = true;
    this.residenceService.getCity(this.countryId).subscribe(
      res => {
        this.cityList = res;
      })
  }

  closeCityList() {
    this.dispalyCityListFlag = false;;
  }

  selectCityCountryRadioButtonValue(event) {
    if (event.target.name === 'city') {
      this.selectedCityId = event.target.value;
      if (this.confirmChangeCityThroughCoutry) {

        // this.newCountryPPP = this.selectedCountryObject.ppp;
        // this.moneyNeeded = this.newCountryPPP * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate / 100;

        //calculate moneyNedded
        let meanIncome = this.selectedCountryObject.meanIncome
        let incomeDeviation = this.selectedCountryObject.incomeDeviation
        let f1 = (meanIncome + (this.constantService.Economically_Challenged) * incomeDeviation)
        let f3 = (Math.exp(f1))
        this.moneyNeeded = (f3 / 2);
        let cash = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate);
        this.moneyNeededlocal = this.moneyNeeded * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
        //
        if (cash < this.moneyNeeded) {
          this.translation.get('gameLabel.You_do_not_have_enough_money_available_to_emigrate_legally_to_will_you_attempt_to_enter_illegally?', { country: this.selectedCountryObject.country }).subscribe((s: String) => {
            this.messageText = s;
            this.confirmBoxMoveOutShowFlag = false;
            this.confirmBoxMoveInShowFlag; false;
            this.confirmBoxMoveChangeCityFlag = false;
            this.confirmBoxChangeCountryShowFlag = false;
            this.dispalyCityListFlag = false;
            this.illegalEmigrateFlag = true;
          })
        }
        else {
          this.messageText = this.translation.instant('gameLabel.Do_you_really_want_to_emigrate?');
          this.setSelectedCityCountryShow = true;
        }
      }
      else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].city.cityid === this.cityList[this.selectedCityId].cityid) {
        this.messageText = this.translation.instant('gameLabel.You_cannot_select_a_city_you_are_trying_to_move.');
        this.successMsgFlag = true;
      }
      else {
        this.messageText = this.translation.instant('gameLabel.Do_you_really_want_to_emigrate?');
        this.setSelectedCityCountryShow = true;
      }


    }
    else if (event.target.name === 'country') {
      this.selectedCOuntryId = event.target.value;
      this.confirmChangeCityThroughCoutry = true;
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid === this.countryList[this.selectedCOuntryId].countryid) {
        this.messageText = this.translation.instant('gameLabel.Cannot_select_the_country_you_are_trying_to_migrate.');
        this.successMsgFlag = true;
      }
      else {
        this.messageText = this.translation.instant('gameLabel.Do_you_really_want_to_emigrate?');
        this.selectedCountryObject = this.countryList[this.selectedCOuntryId];
        this.countryId = this.selectedCountryObject.countryid;
        this.confirmChangeCityYes();
      }


    }

  }


  elligalChangeCountryYes() {
    this.illegalEmigrateFlag = false;
    this.dispalyCityListFlag = false;
    this.illegalYesFlag = true;
    this.moneyNeededlocal = (this.moneyNeededlocal / 10);
    let cash = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash;
    if (cash < this.moneyNeededlocal) {
      this.messageText = this.translation.instant('gameLabel.You_dont_have_enough_money_available_to_emigrate_illegally.');
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.successMsgFlag = true;
    }
    else {
      this.setSelectedCityCountryYes();
    }
  }

  elligalChangeCountryNo() {
    this.illegalEmigrateFlag = false;
    this.dispalyCityListFlag = false;
  }

  setSelectedCityCountryYes() {
    this.setSelectedCityCountryShow = false;
    this.dispalyCityListFlag = false;;
    this.modalWindowShowHide.showWaitScreen = true;
    this.confirmBoxMoveChangeCityFlag = false;
    this.selectedCityObject = this.cityList[this.selectedCityId];
    //check change country or city
    if (!this.confirmChangeCityThroughCoutry) {
      this.saveResidenceObject =
      {
        "actionType": "CHANGE_CITY",
        "gameId": this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,
        "personId": this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,
        "shelterIndex": this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex,
        "cityId": this.selectedCityObject.cityid,
        "cityName": this.selectedCityObject.cityName
      }
      this.residenceService.saveResidence(this.saveResidenceObject).subscribe(res => {
        this.result = res;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allDisplayResidenceList = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence));
        this.allDisplayResidenceList = this.allDisplayResidenceList.reverse();
        this.messageText = this.translation.instant('gameLabel.Changed_the_city_of_residence_successfully!');
        this.messageType = "Success: ";
        this.messageClass = "sucesspopup";
        this.successMsgFlag = true;
        this.modalWindowShowHide.showWaitScreen = false;
      })
    }
    else {
      //check change country legal or elligal
      if (this.illegalYesFlag) {
        this.saveResidenceObject = {
          "actionType": this.constantService.ACTION_RESIDENCE_ILLEGAL_EMIGRATE,
          "gameId": this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,
          "personId": this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,
          "shelterIndex": this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex,
          "cityId": this.selectedCityObject.cityid,
          "cityName": this.selectedCityObject.cityName,
          "countryId": this.selectedCountryObject.countryid,
          "countryName": this.selectedCountryObject.country,
          "moneyNeeded": this.moneyNeededlocal
        }
      }
      else {
        this.saveResidenceObject = {
          "actionType": this.constantService.ACTION_RESIDENCE_LEGAL_EMIGRATE,
          "gameId": this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,
          "personId": this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,
          "shelterIndex": this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex,
          "cityId": this.selectedCityObject.cityid,
          "cityName": this.selectedCityObject.cityName,
          "countryId": this.selectedCountryObject.countryid,
          "countryName": this.selectedCountryObject.country,
          "moneyNeeded": this.moneyNeededlocal
        }
      }
      this.residenceService.saveResidence(this.saveResidenceObject).subscribe(res => {
        this.result = res;
        //for purposr to claculate status from new country data these changes done
        //get difference
        this.diiferenBetIdentity = this.result.data.result.difference;
        //get Differen and merge
        let returnValue = this.agaAYearService.replaceDifferenToIdentity(this.diiferenBetIdentity, this.homeService.allPerson);
        // if(returnValue!=='false'){
        this.homeService.allPerson = returnValue;
        let allPersonkeys = Object.keys(this.homeService.allPerson)
        for (let identity of allPersonkeys) {
          if (!this.diiferenBetIdentity.hasOwnProperty(identity)) {
            if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
              && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
                this.homeService.setlang = this.homeService.allPerson.selected_language
              delete this.homeService.allPerson[identity]
            }
          }
        }

        //
        this.gameSummary.getWelathMetaData(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
          res => {

            if (res !== 'false') {
              this.gameSummary.bornCountryWelathMetadata = res;
            }
            else {
              this.gameSummary.bornCountryWelathMetadata = false;
            }
            this.homeService.geteconomicalStatusMetadata(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid).subscribe(
              res => {
                this.gameSummary.ecoStatusData = res[0];
                //emit emiter for difference
                this.homeService.changeAllPerson.emit(this.homeService.allPerson);
                //
                //set event
                this.agaAYearService.nextEventResultIsEvent(this.result);
                this.eventService.enableActionAsPerEvent(this.eventService.currentEvent, this.homeService.allPerson[this.constantService.FAMILY_SELF]);
                //code to get status and wealth data for imigrated country
                if (this.result.data.result.langCode === "COMMON_ACTION_RESIDENCE_EMIGRATE_02") {
                  this.modalWindowShowHide.showWaitScreen = false;
                  this.messageText = this.translation.instant('gameLabel.You_have_been_arrested_for_entering_illegally_into_the_country!');
                  this.messageType = "Warning: ";
                  this.messageClass = "errorpopup";
                  this.successMsgFlag = true;
                }
                else {
                  this.allDisplayResidenceList = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_residence;
                  this.allDisplayResidenceList = this.allDisplayResidenceList.reverse()
                  this.messageText = this.translation.instant('gameLabel.Emigrated_successfully!');
                  this.messageType = "Success: ";
                  this.messageClass = "sucesspopup";
                  this.successMsgFlag = true;
                  if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
                    this.homeService.cssForDisable = "disable";
                    this.modalWindowShowHide.showActionResidence = false;
                    setTimeout(() => {

                      this.modalWindowShowHide.showObituary = true;
                      this.modalWindowShowHide.showEvent = false;
                      this.modalWindowShowHide.showGameSummary = false;
                      this.homeService.cssForDisable = "";
                    },
                      5000);
                  } else {
                    this.latlng = [
                      [this.homeService.allPerson[this.constantService.FAMILY_SELF].previous_countries.latitude, this.homeService.allPerson[this.constantService.FAMILY_SELF].previous_countries.longitude],
                      [this.homeService.allPerson[this.constantService.FAMILY_SELF].country.lat, this.homeService.allPerson[this.constantService.FAMILY_SELF].country.lng]
                    ];
                    this.start_end_mark.push(this.latlng[0]);
                    this.start_end_mark.push(this.latlng[this.latlng.length - 1])
                    this.gameSummary.mapDisplay = true;
                    this.gameSummary.country = this.homeService.allPerson[this.constantService.FAMILY_SELF].country;
                    this.gameSummary.registerCountry = this.homeService.allPerson[this.constantService.FAMILY_SELF].previous_countries;
                    this.map = true;
                    this.modalWindowShowHide.showWaitScreen = false;
                  }
                }

              })
          })
      })

    }

  }

  setSelectedCityCountryNo() {
    this.setSelectedCityCountryShow = false;
    this.dispalyCityListFlag = false;
  }


  setSelectedCoutry() {
    this.setSelectedCityCountryShow = false;
    this.confirmBoxMoveChangeCityFlag = false;

  }

  confirmChangeCityNo() {
    this.confirmBoxMoveChangeCityFlag = false;
    this.dispalyCityListFlag = false;
  }

  confirmChangeCountryYes() {
    this.modalWindowShowHide.showWaitScreen = true;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdInvestmentValue > 0 || this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities > 0) {
      this.saveResidenceObject = {
        "actionType": this.constantService.ACTION_RESIDENCE_LOAN_INVESTMENT,
        "gameId": this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id,
        "personId": this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,
        "shelterIndex": this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex,
        "cityId": "",
        "cityName": "",
        "countryId": "",
        "countryName": "",
        "moneyNeeded": ""
      }
      this.confirmBoxChangeCountryShowFlag = false;
      this.residenceService.saveResidence(this.saveResidenceObject).subscribe(
        res => {
          this.result = res;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.livingWith = this.constantService.ACTION_RESIDENCE_OWN_HOUSE;
          this.messageText = this.translation.instant('gameLabel.You_have_an_outstanding_investments_or_loans_to_be_paid.');
          this.messageType = "Warning: ";
          this.messageClass = "errorpopup";
          this.successMsgFlag = true;
          this.modalWindowShowHide.showWaitScreen = false;
        });
    }
    else {
      this.type = this.translation.instant('gameLabel.Select_a_country_in_which_you_can_emigrate.');
      this.showCityList = false;
      this.dispalyCityListFlag = true;
      this.showCountryList = true;
      this.confirmBoxChangeCountryShowFlag = false;

      this.residenceService.getCountryList().subscribe(
        res => {
          this.countryList = res;
          this.modalWindowShowHide.showWaitScreen = false;
        }
      )
    }
  }

  confirmChangeCountryNo() {
    this.confirmBoxChangeCountryShowFlag = false;
  }

  closeMap() {
    this.map = false;
  }


  markerClick(infowindow, i) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
    if (i == 0) {
      this.labelName = this.homeService.allPerson[this.constantService.FAMILY_SELF].previous_countries.countryName;
    }
    else if (i == 1) {
      this.labelName = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country;
    }
  }




  successMsgWindowClose() {
    this.successMsgFlag = false;
    this.modalWindowShowHide.showActionResidence = false;
  }

  close() {
    this.modalWindowShowHide.showActionResidence = false;
  }
  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }
}
