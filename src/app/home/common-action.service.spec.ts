import { TestBed } from '@angular/core/testing';

import { CommonActionService } from './common-action.service';

describe('CommonActionService', () => {
  let service: CommonActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonActionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
