import { ElementRef } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { EventService } from '../event/event.service';
import { HomeService } from '../home.service';
import { PostGameService } from '../post-game.service';
import jsPDF from 'jspdf';
import htmlToPdfmake from 'html-to-pdfmake';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { FinanceService } from '../action-finance/finance.service';
import { CommonService } from 'src/app/shared/common.service';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { TranslationService } from 'src/app/translation/translation.service';
declare let html2canvas: any;

/* ES5 */
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-obituary',
  templateUrl: './obituary.component.html',
  styleUrls: ['./obituary.component.css']
})
export class ObituaryComponent implements OnInit {
  capturedImage;
  parameter;
  eventId;
  gameId;
  addCssForDisable: boolean = false;
  gender;
  obituaryText;
  headingText: String;
  status;
  education = "";
  residence;
  carrier;
  family;
  leisure;
  disease;
  vices;
  diet;
  charity;
  loanInvestments;
  amenities;
  pdfFlag: boolean = false;
  languageReligionString;
  obituaryWritenByMe;
  newSuggetion;
  diedEventAddline;
  organDonation;
  bloodDonation;
  full_name;
  organDonationArray = [];
  notShowDiv: boolean = false;
  str = "";
  str1 = "";
  str2 = "";
  str3 = "";
  str4 = "";
  str5 = "";
  str6 = "";
  str7 = "";
  str8 = "";
  str9 = "";
  str10 = "";
  str11 = "";
  str12 = "";
  str13 = "";
  str14 = "";
  str15 = "";
  str16 = "";
  str17 = "";
  str18 = "";
  str19 = "";
  str20 = "";
  str21 = "";
  str22 = "";
  str23 = "";
  str24 = ""
  str25 = "";
  str26 = "";
  str27 = "";
  str28 = "";
  str29 = "";
  str30 = "";
  str31 = "";
  str32 = "";
  str33 = "";
  str34 = "";
  str35 = "";
  str36 = "";
  str37 = "";
  str38 = "";
  allPerson;
  mainPerson;
  sveObituaryObject = {};
  messageText;
  messageType;
  messageClass = "alert-info";
  MessageShow;
  successMsgFlag;
  emailId;
  friendMailShowFlag: boolean = false;
  deadShelter;
  bornShelter;
  deadDiet;
  BornDiet;
  dietString;
  shelterString;
  flagForFirstJob: boolean = false;
  letterData;
  msgText = ""
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public postGameService: PostGameService,
    public constantService: ConstantService,
    public homeService: HomeService,
    public eventService: EventService,
    public finanaceService: FinanceService,
    public commonService: CommonService,
    public translate: TranslateService,
    public gameSummary: GameSummeryService,
    public translationService: TranslationService,
    public translation: TranslateService
  ) { }
  @ViewChild('decisionforConfirm') public decisionforConfirm: ModalDirective;
  @ViewChild('content', { static: false }) content: ElementRef;
  @ViewChild('pdf') pdf: ElementRef;

  ngOnInit(): void {
    this.modalWindowShowHide.showObituary = true;
    this.postGameService.getObituary(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        if (res === false) {
          let saveObj = this.homeService.cretaeObituary(this.homeService.allPerson);
          this.postGameService.updateObituaryRetutnByMe(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, saveObj, false).subscribe(
            res => {
              this.sveObituaryObject = res;
            })
        }
        else {
          let obObj = res
          this.sveObituaryObject = obObj['systemGeneratedObituary'].obituary;
          this.obituaryWritenByMe = obObj['userWrittenObituary'];
          let str: string = this.obituaryWritenByMe;
          if (this.obituaryWritenByMe == null || this.obituaryWritenByMe == '') {
            this.addCssForDisable = false;
          }
          else {
            this.addCssForDisable = true;
          }
        }
      })
    // this.cretaeObituary(this.homeService.allPerson);


  }

  saveObituaryWrittenByMe() {
    this.newSuggetion = this.obituaryWritenByMe;
    let str: string = this.obituaryWritenByMe;
    this.obituaryWritenByMe = str.trim();
    if (this.obituaryWritenByMe == null || this.obituaryWritenByMe == '') {
      this.msgText = this.translate.instant('gameLabel.Please_write_something')
      this.decisionforConfirm.show();
    }
    else {
      this.postGameService.updateObituaryRetutnByMe(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.obituaryWritenByMe, true).subscribe(
        data => {
          if (data === true) {
            this.msgText = this.translate.instant('gameLabel.Your_obituary_saved_succesfully');
            this.decisionforConfirm.show();
          }
          else {
            alert("Something wents wrong")
          }
        });
    }
  }

  public toCanvas() {
    var container = document.getElementById("pdf"); // full page 
    html2canvas(container, { allowTaint: true }).then(function (canvas) {
      var link = document.createElement("a");
      document.body.appendChild(link);
      link.download = "obituary_image.png";
      link.href = canvas.toDataURL("image/png");
      link.target = '_blank';
      link.click();
    });
  }

  public sendMail() {
    this.postGameService.senfObituaryMail(this.sveObituaryObject, 'obituary').subscribe(
      res => {
        if (res) {
          this.messageText = this.translation.instant('gameLabel.Send_mail_succesfully');
          this.successMsgFlag = true;
        } else {
          this.messageText = this.translation.instant('gameLabel.email_not_found');
          this.successMsgFlag = true;
        }
      }
    )
  }


  public sendMailToFriend() {
    this.friendMailShowFlag = true;
    this.emailId = '';
  }
  send() {
    this.friendMailShowFlag = false;
    this.sveObituaryObject['emailId'] = this.emailId;
    this.postGameService.senfObituaryMail(this.sveObituaryObject, 'obituary').subscribe(
      res => {
        if (res) {
          this.messageText = this.translation.instant('gameLabel.Send_mail_succesfully');
          this.successMsgFlag = true;
        } else {
          this.messageText = this.translation.instant('gameLabel.email_not_found');
          this.successMsgFlag = true;
        }
      }
    )

  }

  close() {
    this.friendMailShowFlag = false;
    this.emailId = '';
  }
  successMsgWindowClose() {
    this.successMsgFlag = false;
  }

  public download() {
    this.modalWindowShowHide.showObituaryPdf = true;
  }

  public downloadAsPDF() {
    const doc = new jsPDF();
    const pdfTable = this.pdf.nativeElement;
    var html = htmlToPdfmake(pdfTable.innerHTML);
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).download(this.homeService.allPerson[this.constantService.FAMILY_SELF].first_name + '_' + this.homeService.allPerson[this.constantService.FAMILY_SELF].last_name + '.pdf');


  }



  getDietString(dietIndexDead, DietIndeaxBorn) {
    if (dietIndexDead == 0) {
      this.deadDiet = this.translate.instant('gameLabel.obituary93');
    }
    else if (dietIndexDead == 1) {
      this.deadDiet = this.translate.instant('gameLabel.obituary94');
    }
    else if (dietIndexDead == 2) {
      this.deadDiet = this.translate.instant('gameLabel.obituary61');
    }
    else if (dietIndexDead == 3) {
      this.deadDiet = this.translate.instant('gameLabel.obituary63');
    }
    else if (dietIndexDead == 4) {
      this.deadDiet = this.translate.instant('gameLabel.obituary64');
    }
    else if (dietIndexDead == 5) {
      this.deadDiet = this.translate.instant('gameLabel.obituary65');
    }
    else if (dietIndexDead == 6) {
      this.deadDiet = this.translate.instant('gameLabel.obituary66');
    }
    else if (dietIndexDead == 7) {
      this.deadDiet = this.translate.instant('gameLabel.obituary67');
    }
    //born
    if (DietIndeaxBorn == 0) {
      this.BornDiet = this.translate.instant('gameLabel.obituary68');
    }
    else if (DietIndeaxBorn == 1) {
      this.BornDiet = this.translate.instant('gameLabel.obituary69');
    }
    else if (DietIndeaxBorn == 2) {
      this.BornDiet = this.translate.instant('gameLabel.obituary70');
    }
    else if (DietIndeaxBorn == 3) {
      this.BornDiet = this.translate.instant('gameLabel.obituary71');
    }
    else if (DietIndeaxBorn == 4) {
      this.BornDiet = this.translate.instant('gameLabel.obituary72');
    }
    else if (DietIndeaxBorn == 5) {
      this.BornDiet = this.translate.instant('gameLabel.obituary73');
    }
    else if (DietIndeaxBorn == 6) {
      this.BornDiet = this.translate.instant('gameLabel.obituary74');
    }
    else if (DietIndeaxBorn == 7) {
      this.BornDiet = this.translate.instant('gameLabel.obituary75');
    }

    if (dietIndexDead == DietIndeaxBorn) {
      this.dietString = this.BornDiet + this.translate.instant('gameLabel.obituary76');
    }
    else {
      this.dietString = this.BornDiet + this.deadDiet
    }

  }

  getShelterString(shelterIndexDead, shelterIndexBorn) {
    if (shelterIndexDead == 0) {
      this.deadShelter = this.translate.instant('gameLabel.obituary77');
    }
    else if (shelterIndexDead == 1) {
      this.deadShelter = this.translate.instant('gameLabel.obituary78');
    }
    else if (shelterIndexDead == 2) {
      this.deadShelter = this.translate.instant('gameLabel.obituary79');
    }
    else if (shelterIndexDead == 3) {
      this.deadShelter = this.translate.instant('gameLabel.obituary80');
    }
    else if (shelterIndexDead == 4) {
      this.deadShelter = this.translate.instant('gameLabel.obituary81');
    }
    else if (shelterIndexDead == 5) {
      this.deadShelter = this.translate.instant('gameLabel.obituary82');
    }
    else if (shelterIndexDead == 6) {
      this.deadShelter = this.translate.instant('gameLabel.obituary83');
    }
    else if (shelterIndexDead == 7) {
      this.deadShelter = this.translate.instant('gameLabel.obituary84');



      //born
    }
    if (shelterIndexBorn == 0) {
      this.bornShelter = this.translate.instant('gameLabel.obituary85');
    }
    else if (shelterIndexBorn == 1) {
      this.bornShelter = this.translate.instant('gameLabel.obituary86');
    }
    else if (shelterIndexBorn == 2) {
      this.bornShelter = this.translate.instant('gameLabel.obituary87');
    }
    else if (shelterIndexBorn == 3) {
      this.bornShelter = this.translate.instant('gameLabel.obituary88');
    }
    else if (shelterIndexBorn == 4) {
      this.bornShelter = this.translate.instant('gameLabel.obituary89');
    }
    else if (shelterIndexBorn == 5) {
      this.bornShelter = this.translate.instant('gameLabel.obituary90');
    }
    else if (shelterIndexBorn == 6) {
      this.bornShelter = this.translate.instant('gameLabel.obituary91');
    }
    else if (shelterIndexBorn == 7) {
      this.bornShelter = this.translate.instant('gameLabel.obituary92');
    }


    if (shelterIndexBorn == shelterIndexDead) {
      this.bornShelter = this.bornShelter.replace(/\./g, '');

      this.shelterString = this.bornShelter + this.translate.instant('gameLabel.obituary76');
    }
    else {
      this.shelterString = this.bornShelter + this.deadShelter
    }

  }

  viewBornSummary() {
    this.modalWindowShowHide.showBornSummary = true;
  }

  bugClick() {
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showLetter = false;
    this.modalWindowShowHide.showBugReport = true;
    this.modalWindowShowHide.showFeddback = false;
  }

  feedBackClick() {
    this.modalWindowShowHide.showObituary = false;
    this.modalWindowShowHide.showGameSummary = false;
    this.modalWindowShowHide.showLetter = false;
    this.modalWindowShowHide.showBugReport = false;
    this.modalWindowShowHide.showFeddback = true;
  }

}





