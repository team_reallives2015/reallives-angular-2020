import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { ConstantService } from 'src/app/shared/constant.service';
import { EventService } from '../event/event.service';
import { HomeService } from '../home.service';
import { PostGameService } from '../post-game.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import jsPDF from 'jspdf';
import htmlToPdfmake from 'html-to-pdfmake';

@Component({
  selector: 'app-obituary-pdf',
  templateUrl: './obituary-pdf.component.html',
  styleUrls: ['./obituary-pdf.component.css']
})
export class ObituaryPdfComponent implements OnInit {
  parameter;
  eventId;
  gameId;
  addCssForDisable="";
  gender;
  obituaryText;
  headingText:String;
  status;
  education;
  residence;
  carrier;
  family;
  leisure;
  disease;
  vices;
  diet;
  charity;
  loanInvestments;
  amenities;
  pdfFlag: boolean = false;
  languageReligionString;
  obituaryWritenByMe;
 newSuggetion;
diedEventAddline;
  organDonation;
  bloodDonation;
  full_name;
  organDonationArray=[];
  constructor(public modalWindowShowHide : modalWindowShowHide,
    public postGameService :PostGameService,
    public constantService :ConstantService,
    public homeService :HomeService,
    public eventService :EventService) { }
    @ViewChild('content', {static: false}) content: ElementRef;

   
  ngOnInit(): void {
    this.modalWindowShowHide.showObituaryPdf=true;
    // this.full_name=this.homeService.allPerson[this.constantService.FAMILY_SELF].full_name;
    // if(this.obituaryWritenByMe==null || this.obituaryWritenByMe==''){
    //   this.addCssForDisable="disable";
    // }

    // this.modalWindowShowHide.showObituaryPdf=false;
    // if(this.homeService.allPerson[this.constantService.FAMILY_SELF].sex===this.constantService.MALE){
    //      this.gender="male";
    // }
    // else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].sex===this.constantService.FEMALE){
    //   this.gender="female";
    // }
    // if(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.countryid === 74)
    // {
    //   this.languageReligionString = "Your country region's official language is " + this.homeService.allPerson[this.constantService.FAMILY_SELF].language.languagename + " and Your religion was " + this .homeService.allPerson[this.constantService.FAMILY_SELF].religion.religion+".";

    // }else {
    //   this.languageReligionString = "Your country's official language is " + this.homeService.allPerson[this.constantService.FAMILY_SELF].language.languagename + " and Your religion was " + this .homeService.allPerson[this.constantService.FAMILY_SELF].religion.religion+".";

    // }
    // this.gameId = this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id;
    // this.postGameService.getObituary(this.gameId).subscribe(
    //   res=>{
    //     this.obituaryText=res;
    //     if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 18)
    //     {
    //       this.diedEventAddline=this.eventService.allEvents[this.eventService.allEvents.length-1].text;
    //       this.diedEventAddline = 'Unfortunately,' + ' ' + this.diedEventAddline.toLowerCase();
    //     }  else {
    //       this.diedEventAddline=this.eventService.allEvents[this.eventService.allEvents.length-1].text;
    //     }

    //     if(this.eventService.allEvents[this.eventService.allEvents.length-1].eventId === 'COMMON_FATE_DISASTER_DIED' )
    //     {
    //       if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 18)
    //       {
    //         this.diedEventAddline=this.eventService.allEvents[this.eventService.allEvents.length-1].text;
    //         this.diedEventAddline = 'Unfortunately,' + ' ' + this.diedEventAddline.toLowerCase();

    //       }else{
    //         this.diedEventAddline=this.eventService.allEvents[this.eventService.allEvents.length-1].text;
    //       }
    //     }


        // 
        

        this.headingText="sdkfhjsdjhfsjdhf";
        this.diedEventAddline="sdgsdg";
        this.languageReligionString="Hindu";
        this.status="low";
        this.diet="dfgjfdgjdfjg";
        this.charity="fdgjdfkjg";
        this.amenities="sdjfhdskjhfjsdhf";
    this.family="dkjhfgfdjhgjhfd";
    this.education="fgkdfgj";
    this.residence="sdfjkjdhsfsdjkh";
    this.carrier="dfgjfdhg";
    this.bloodDonation="3";      
        this.downloadAsPDF();
      // })
  }
  

  @ViewChild('pdfTable') pdfTable: ElementRef;
  
  public downloadAsPDF() {
    const doc = new jsPDF();
    const pdfTable = this.pdfTable.nativeElement;
    var html = htmlToPdfmake(pdfTable.innerHTML);
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).open(); 
     
  }

  onCloseObituaryPdf(){
    this.modalWindowShowHide.showObituaryPdf=false;
  }
}
