import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObituaryPdfComponent } from './obituary-pdf.component';

describe('ObituaryPdfComponent', () => {
  let component: ObituaryPdfComponent;
  let fixture: ComponentFixture<ObituaryPdfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObituaryPdfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObituaryPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
