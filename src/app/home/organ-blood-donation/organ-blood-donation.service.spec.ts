import { TestBed } from '@angular/core/testing';

import { OrganBloodDonationService } from './organ-blood-donation.service';

describe('OrganBloodDonationService', () => {
  let service: OrganBloodDonationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrganBloodDonationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
