import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganBloodDonationComponent } from './organ-blood-donation.component';

describe('OrganBloodDonationComponent', () => {
  let component: OrganBloodDonationComponent;
  let fixture: ComponentFixture<OrganBloodDonationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganBloodDonationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganBloodDonationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
