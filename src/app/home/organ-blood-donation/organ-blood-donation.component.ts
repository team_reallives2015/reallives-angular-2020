import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { HomeService } from '../home.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { OrganBloodDonationService } from './organ-blood-donation.service';
import { interval } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-organ-blood-donation',
  templateUrl: './organ-blood-donation.component.html',
  styleUrls: ['./organ-blood-donation.component.css']
})
export class OrganBloodDonationComponent implements OnInit {

  blood_donation_yearArray = [];
  allPerson;
  newOrganObject=[];
   liverImage;
    eyesImage;
    heartImage;
    kidneyImage;
    lungsImage;
    skinImage;
    bloodYear=0;
    activeClass="";
    bloodDonationData;
    successMsgFlag:boolean=false;
    messageText;
    messageType;
    showAlertFlag:boolean=false;
  alertClass;
  alertMsg;
  alertType;
  bloodDonationDecisionFlag:boolean=false;
  disableSaveButton:boolean=false
    
  constructor(public modalWindowShowHide :modalWindowShowHide ,
              public commonService :CommonService,
              public translate :TranslateService,
              public homeService :HomeService,
              public constantService:ConstantService,
              config: NgbModalConfig,
              private organBloodDonationService :OrganBloodDonationService) 
              { 
                interval(30000).subscribe(x => {
                  if(this.showAlertFlag){
                    this.showAlertFlag=false;
                  }
                  });
              }
              @ViewChild('decisionModelOrganDonationCertificate') decisionModelOrganDonationCertificate: ModalDirective;
              @ViewChild('decisionModelBloodDonationYear') decisionModelBloodDonationYear: ModalDirective;
               @ViewChild('decisionForOrganBloodDonation') decisionForOrganBloodDonation:ModalDirective;

check(index,organStatus){
 
   if(!this.newOrganObject[index].active){
    this.newOrganObject[index].active="active";
    this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation[index].organStatus=organStatus
    this.newOrganObject[index].organStatus=organStatus;

   }
   else if(this.newOrganObject[index].active){
    this.newOrganObject[index].active="";
    this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation[index].organStatus=organStatus
    this.newOrganObject[index].organStatus=organStatus;

   }
}

  ngOnInit(): void {
    if((this.homeService.allPerson[this.constantService.FAMILY_SELF].age<18 )
    && !(this.homeService.allPerson[this.constantService.FAMILY_SELF].dead))
  {
    this.disableSaveButton=true;
    this.alertMsg =this.translate.instant('gameLabel.You_cannot_take_any_action_before_age_18');
    this.messageType = "Warning: ";
    this.alertClass = "alert-warning";
    this.showAlertFlag = true;
  }

  if(this.homeService.allPerson[this.constantService.FAMILY_SELF].dead){
    this.alertMsg = this.translate.instant('gameLabel.You_cannot_take_any_action_because_you_are_dead');
    this.alertClass = "alert-warning";
    this.showAlertFlag = true;
    this.disableSaveButton=true;
  }
  else if(this.homeService.allPerson[this.constantService.FAMILY_SELF].age>50){
      this.alertMsg = this.translate.instant('gameLabel.Your_organs_can_be_donated_until_you_are_50_years_old_After_that_it_is_not_possible_to_donate_organs');
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
      this.disableSaveButton=true;
    }

    this.initializeOrganObjectFromSelf();
    this.blood_donation_yearArray = [
      {'year': 1, 'display': this.translate.instant('gameLabel.1_Year')},
      {'year': 3, 'display': this.translate.instant('gameLabel.3_Year')},
      {'year': 5, 'display': this.translate.instant('gameLabel.5_Year')},
      {'year': 7, 'display': this.translate.instant('gameLabel.7_Year')},
    ];
    if(!this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag && this.homeService.allPerson[this.constantService.FAMILY_SELF].age<18)
    {
      this.activeClass=""

    }else{
      this.activeClass="active"
    }
    this.bloodDonationDecisionFlag=true;
  }

  initializeOrganObjectFromSelf(){
     this.newOrganObject = JSON.parse(JSON.stringify(this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation));
    for(let i=0;i<this.newOrganObject.length;i++){
      if(this.newOrganObject[i].organStatus){
        this.newOrganObject[i].active="active";
       }
       else{
        this.newOrganObject[i].active="";
       }
    }


  }

  saveOrganData(){
    let count = 0;
    this.modalWindowShowHide.showWaitScreen = true;
    this.organBloodDonationService.updateOrganData(this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation).subscribe(
      res=>{
        this.modalWindowShowHide.showWaitScreen = false;   
      for(let i=0;i<this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation.length;i++){
          if(this.homeService.allPerson[this.constantService.FAMILY_SELF].organ_donation[i].organStatus){
            count++;
          }
      }
      this.modalWindowShowHide.showOrganBloodDonationFlag=false;
      if(count>0){
        this.modalWindowShowHide.showOrganDonationCertificate=true;
      }
    })
  }

  selectedValueForBlood(){
    
    if(!this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag)
    {
      this.activeClass="active"
       this.decisionModelBloodDonationYear.show();
    }else{
      this.activeClass=""
      this.modalWindowShowHide.showWaitScreen = true;
      this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year = 0;
      this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag = false;
      this.bloodDonationData={'flag': this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag,'years':this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year}
      this.organBloodDonationService.updateBloodDonation(this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,this.bloodDonationData).subscribe
      (res => {
        this.modalWindowShowHide.showWaitScreen = false;
        this.successMsgFlag=true;
        this.messageText=""
      });
    }
    if( this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year==0){
      this.bloodYear=this.blood_donation_yearArray[0].year;
    }
    else{
      this.bloodYear=this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year;
    }
  }

 

  setSelectedBloodYear(year)
  {
    this.bloodYear = year;
  }

  selectedYear()
  {
    this.modalWindowShowHide.showWaitScreen = true;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year = this.bloodYear;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag = true;
    let years=this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year;
     this.bloodDonationData={"flag": this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag,"years":this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year}

    this.organBloodDonationService.updateBloodDonation(this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id,this.bloodDonationData).subscribe
    (res => {
      this.successMsgFlag=true;
      this.messageText=""
      this.decisionModelBloodDonationYear.hide();
    this.modalWindowShowHide.showWaitScreen = false;
    
    });
  }

  successMsgWindowClose(){
    this.successMsgFlag=false;
  }

  cancelSelectedYear()  
  {
    this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_year = 0;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag = false;
    if(!this.homeService.allPerson[this.constantService.FAMILY_SELF].blood_donation_flag)
    {
      this.activeClass=""

    }else{
      this.activeClass="active"
    }
    this.decisionModelBloodDonationYear.hide();
  }

  closeWindow(){
    this.modalWindowShowHide.showOrganBloodDonationFlag=false;
  }

  hideModal(): void {
    this.decisionModelBloodDonationYear.hide();
  }

  OnCloseWindowPopup(){
    this.modalWindowShowHide.showWaitScreen = false;
    this.modalWindowShowHide.showOrganBloodDonationFlag=false;

  }
}
