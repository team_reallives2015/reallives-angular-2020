import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';


@Injectable({
  providedIn: 'root'
})
export class OrganBloodDonationService {

  constructor(private http:HttpClient,
             private commonService :CommonService) { }

  updateOrganData(personId,organObject){
    return this.http.post<Array<Object>>(`${this.commonService.url}game/organDonation/updateOrgan/${personId}`,{organObject});
  }

  updateBloodDonation(personId,bloodDonationData){
    return this.http.post<Array<Object>>(`${this.commonService.url}game/organDonation/updateBloodDonation/${personId}`,{bloodDonationData},);
  }
}