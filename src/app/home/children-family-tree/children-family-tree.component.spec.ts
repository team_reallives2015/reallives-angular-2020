import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildrenFamilyTreeComponent } from './children-family-tree.component';

describe('ChildrenFamilyTreeComponent', () => {
  let component: ChildrenFamilyTreeComponent;
  let fixture: ComponentFixture<ChildrenFamilyTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildrenFamilyTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildrenFamilyTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
