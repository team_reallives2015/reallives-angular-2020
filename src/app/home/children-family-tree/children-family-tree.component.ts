import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { NumberFormatePipe } from 'src/app/shared/number-formate.pipe';
import { FinanceService } from '../action-finance/finance.service';
import { FamilyCapsuleService } from '../family-capsule/family-capsule.service';
import { HomeService } from '../home.service';
import { UtilityService } from '../utility-bar/utility.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';


@Component({
  selector: 'app-children-family-tree',
  templateUrl: './children-family-tree.component.html',
  styleUrls: ['./children-family-tree.component.css']
})
export class ChildrenFamilyTreeComponent implements OnInit {
  classForHelath = "";
  classForHealthStatus = "";
  allPerson;
  mainPerson;
  education;
  self;
  mainPersonName;
  mainPersonAge;
  mother;
  motherString;
  father;
  fatherString;
  wife;
  wifeString;
  husband;
  husbandString;
  siblings;
  siblingString;
  sibString;
  sib;
  children;
  childrenString;
  childString;
  child;
  grandChildren;
  grandChildrenString;
  grandChildString;
  grandChild;
  livingAtHomeArr;
  livingAwayArr = [];
  level1Arr = [];
  level2Arr = [];
  level3Arr = [];
  level4Arr = [];
  gender;
  identity;
  image;
  width;
  class;
  householdIncome;
  householdExpenses;
  householdNetWorth;
  language;
  religion;
  currencyName;
  diet;
  shelter;
  options;
  registerCountryExchangeRate;
  registerCountryCode;
  selfIncome: number;
  countryIncome;
  deadClass;
  currencyCode;
  salaryinregistercountry;
  expensesinRegisterCountry;
  netWorthinRegisterCountry;
  classForBorder;
  siblingGender;
  childArray = [];
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public homeService: HomeService,
    public constantService: ConstantService,
    public utilityService: UtilityService,
    public financeService: FinanceService,
    public commmonService: CommonService,
    private translate: TranslateService,
    public familyCapsuleService: FamilyCapsuleService,
    public commonService: CommonService,
    public numberPipe: NumberFormatePipe) { }

  ngOnInit(): void {
    this.modalWindowShowHide.showChildrenFamily = true
    this.createChildrenArray();
    this.showWindow(this.homeService.allPerson['SELF'].children[0], 0);

  }

  createChildrenArray() {
    for (let i = 0; i < this.homeService.allPerson[this.constantService.FAMILY_SELF].children.length; i++) {
      let sex, activeClass = "", name
      let child = this.homeService.allPerson[this.homeService.allPerson[this.constantService.FAMILY_SELF].children[i]]
      if (child.sex === "M") {
        sex = this.translate.instant('gameLabel.son')
      }
      else {
        sex = this.translate.instant('gameLabel.daughter')

      }
      this.childArray.push({
        "sex": sex,
        "activeClass": "nav-link",
        "name": child.first_name,
        "identity": child.identity
      })


    }
  }
  showWindow(identity, index) {
    let k, identityHusWife, displayIden;
    for (k = 0; k < this.childArray.length; k++) {
      if (index === k) {
        this.childArray[k].activeClass = "nav-link active"
      }
      else {
        this.childArray[k].activeClass = "nav-link text-white"
      }
    }
    let person = this.homeService.allPerson[identity]
    if (person.sex === 'M') {
      this.siblingGender = this.translate.instant('gameLabel.Your_grand_son') + " " + person.first_name
      identityHusWife = this.translate.instant('gameLabel.Daughter_in_law')
      displayIden = this.translate.instant('gameLabel.Your_son')


    }
    else {
      this.siblingGender = this.translate.instant('gameLabel.Your_grand_daughter') + " " + person.first_name
      identityHusWife = this.translate.instant('gameLabel.Son_in_law')
      displayIden = this.translate.instant('gameLabel.Your_daughter')


    }
    this.classForBorder = "borderForSelect"
    this.mainPerson = person;
    this.mainPersonName = this.mainPerson.first_name;
    this.registerCountryExchangeRate = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    // this.currencyName= this.mainPerson.country.currency Name;
    this.currencyName = "dgdgsdg"
    this.mainPersonAge = this.mainPerson.age;
    this.level1Arr = [];
    this.level2Arr = [];
    this.level3Arr = [];
    this.level4Arr = [];
    this.currencyCode = this.mainPerson.country.currency_code;

    //code for self

    this.self = this.mainPerson;
    if (!this.self.dead) {
      this.deadClass = "";
      if (this.self.traits.health < 35) {
        this.classForHelath = " border-bottom colourRed1"
      }
      else {
        this.classForHelath = "border-bottom";
      }
    }
    else if (this.self.dead) {
      this.deadClass = "card_disable";

    }

    if (this.self.sex == "F") {
      this.gender = "Female";
      this.class = "fmly_member female_bg";
      this.image = "assets/images/game_images/girl.png"
    }
    else {
      this.gender = "Male";
      this.class = "fmly_member men_bg";
      this.image = "assets/images/game_images/fam.png"
    }
    let jobName;
    if (this.mainPerson.job.JobName == this.constantService.JOB_NO_JOB) {
      jobName = "";

    } else {
      if (this.mainPerson.job.JobName == this.constantService.JOB_UNEMPLOYED
        || this.mainPerson.job.JobName == this.constantService.JOB_DOMESTIC_CHORES) {
        if (this.mainPerson.business.Business != this.constantService.BUSINESS_NO_BUSINESS) {
          jobName = this.mainPerson.business.Business;
        }
        else {
          jobName = this.mainPerson.job.Job_displayName;
        }
      }
      else {
        jobName = this.mainPerson.job.Job_displayName;
      }
    }
    let name = '';
    if (this.self.dead && this.self.age === 0) {
      name = this.translate.instant('gameLabel.Stillborn');
    }
    else {
      name = this.self.full_name
    }
    let healthProblemSelf = "";
    if (this.self.health_disease != null && !this.self.dead) {
      let keys = Object.keys(this.self.health_disease);
      let le = keys.length;
      for (let h = 0; h < keys.length; h++) {
        if (this.self.health_disease[keys[h]].status === "GOTSICK") {
          if (healthProblemSelf == "") {
            healthProblemSelf = healthProblemSelf + this.self.health_disease[keys[h]].diseaseName;
          }
          else {
            healthProblemSelf = healthProblemSelf + ", " + this.self.health_disease[keys[h]].diseaseName;
          }
        }
      }
      if (healthProblemSelf === "") {
        healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
      }
      else {
        this.classForHealthStatus = "border-bottom colourRed1"
      }
    }
    else {
      healthProblemSelf = this.translate.instant('gameLabel.None');
        this.classForHealthStatus = "border-bottom"
    }
    let selfEdu = this.commonService.setEducationForAll(this.self);
    this.level1Arr.push({
      "name": name,
      "age": this.self.age,
      "education": selfEdu,
      "identity": displayIden,
      "sex": this.gender,
      "image": this.image,
      "class": this.class,
      "jobName": jobName,
      "income": this.self.income.toFixed(2),
      "registerIncome": ((this.self.income / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
      "health": this.self.traits.health,
      "deadClass": this.deadClass,
      "healthProblem": healthProblemSelf,
      "currencyCode": this.self.country.CurrencyPlural,
      "currencyName": this.self.country.currency_code,
      "healthClass": this.classForHelath,
      "healthClassForStatus": this.classForHealthStatus
    })


    //code for wife
    if (this.mainPerson.wife != null) {
      let w = this.mainPerson.wife
      this.wife = this.homeService.allPerson[w];
      if (this.wife.dead) {
        this.deadClass = "card_disable";
      }
      else {
        this.deadClass = "";
        if (this.wife.traits.health < 35) {
          this.classForHelath = " border-bottom colourRed1"
        }
        else {
          this.classForHelath = "border-bottom";
        }
      }
      let healthProblemWife = "";
      if (this.wife.healthDisease != null && !this.wife.dead) {
        let keys = Object.keys(this.wife.health_disease);
        let le = keys.length;
        for (let h = 0; h < keys.length; h++) {
          if (this.wife.health_disease[keys[h]].status === "GOTSICK") {
            if (healthProblemWife == "") {
              healthProblemWife = healthProblemWife + this.wife.health_disease[keys[h]].diseaseName;
            }
            else {
              healthProblemWife = healthProblemWife + ", " + this.wife.health_disease[keys[h]].diseaseName;
            }
          }
        }
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }
      else {
        healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
      }
      let wifeEdu = this.commonService.setEducationForAll(this.wife);
      this.level1Arr.push({
        "name": this.wife.full_name,
        "age": this.wife.age,
        "education": wifeEdu,
        "identity": identityHusWife,
        "sex": "Female",
        "image": "assets/images/game_images/girl.png",
        "class": "fmly_member female_bg",
        "jobName": this.wife.job.Job_displayName,
        "income": this.wife.income.toFixed(2),
        "registerIncome": ((this.wife.income / this.wife.country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
        "health": this.wife.traits.health,
        "deadClass": this.deadClass,
        "healthProblem": healthProblemWife,
        "currencyCode": this.wife.country.CurrencyPlural,
        "currencyName": this.wife.country.currency_code,
        "healthClass": this.classForHelath,
        "healthClassForStatus": this.classForHealthStatus
      })
    }
    //code for husband
    if (this.mainPerson.husband != null) {
      this.husband = this.homeService.allPerson[this.mainPerson.husband];
      if (this.husband.dead) {
        this.deadClass = "card_disable";
      }
      else {
        this.deadClass = "";
      }
      let healthProblemWife = "";
      if (this.husband.healthDisease != null && !this.husband.dead) {
        let keys = Object.keys(this.husband.health_disease);
        let le = keys.length;
        for (let h = 0; h < keys.length; h++) {
          if (this.husband.health_disease[keys[h]].status === "GOTSICK") {
            if (healthProblemWife == "") {
              healthProblemWife = healthProblemWife + this.husband.health_disease[keys[h]].diseaseName;
            }
            else {
              healthProblemWife = healthProblemWife + ", " + this.husband.health_disease[keys[h]].diseaseName;
            }
          }
        }
        if (healthProblemWife === "") {
          healthProblemWife = this.translate.instant('gameLabel.None');;
          this.classForHealthStatus = "border-bottom"
        }
        else {
          this.classForHealthStatus = "border-bottom colourRed1"
        }
      }
      else {
        healthProblemWife = this.translate.instant('gameLabel.None');
          this.classForHealthStatus = "border-bottom"
      }
      let husEducation = this.commonService.setEducationForAll(this.husband);
      this.level1Arr.push({
        "name": this.husband.full_name,
        "age": this.husband.age,
        "education": husEducation,
        "identity": identityHusWife,
        "sex": "Male",
        "image": "assets/images/game_images/fam.png",
        "class": "fmly_member men_bg",
        "jobName": this.husband.job.Job_displayName,
        "income": this.husband.income.toFixed(2),
        "registerIncome": ((this.husband.income / this.husband.country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
        "health": this.husband.traits.health,
        "deadClass": this.deadClass,
        "healthProblem": healthProblemWife,
        "currencyCode": this.husband.country.CurrencyPlural,
        "currencyName": this.husband.country.currency_code,
        "healthClass": this.classForHelath,
        "healthClassForStatus": this.classForHealthStatus
      })
    }

    //code for children
    if (this.mainPerson.children.length > 0) {
      this.children = this.mainPerson.children;

      for (var i = 0; i < this.children.length; i++) {
        this.childString = this.mainPerson.children[i];
        this.child = this.homeService.allPerson[this.childString];
        if (this.child.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
          if (this.child.traits.health < 35) {
            this.classForHelath = " border-bottom colourRed1"
          }
          else {
            this.classForHelath = "border-bottom";
          }
        }
        if (this.child.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Daughter');
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Son');
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.child.health_disease != null && !this.child.dead) {
          let keys = Object.keys(this.child.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.child.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.child.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.child.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
        }
        if (this.child.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.child.job.Job_displayName;

        }
        let name = '';
        if (this.child.dead && this.child.age === 0) {
          name = this.translate.instant('gameLabel.Stillborn');
        }
        else {
          name = this.child.full_name
        }

        let childEdu = this.commonService.setEducationForAll(this.child);
        this.level2Arr.push({
          "name": name,
          "age": this.child.age,
          "education": childEdu,
          "identity": this.identity,
          "sex": this.gender,
          "class": this.class,
          "image": this.image,
          "jobName": jobName,
          "income": this.child.income.toFixed(2),
          "registerIncome": ((this.child.income / this.homeService.allPerson[this.childString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.child.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.child.country.CurrencyPlural,
          "currencyName": this.child.country.currency_code,
          "healthClass": this.classForHelath,
          "healthClassForStatus": this.classForHealthStatus
        })
      }
    }

    //code for grandChildren
    if (this.mainPerson.grand_children.length > 0) {
      this.grandChildren = this.mainPerson.grand_children;

      for (var i = 0; i < this.grandChildren.length; i++) {
        this.grandChildString = this.mainPerson.grand_children[i];
        this.grandChild = this.homeService.allPerson[this.grandChildString];
        if (this.grandChild.dead) {
          this.deadClass = "card_disable";
        }
        else {
          this.deadClass = "";
          if (this.grandChild.traits.health < 35) {
            this.classForHelath = " border-bottom colourRed1"
          }
          else {
            this.classForHelath = "border-bottom";
          }
        }
        if (this.grandChild.sex == "F") {
          this.gender = "Female";
          this.identity = this.translate.instant('gameLabel.Grand_Daughter');
          this.class = "fmly_member female_bg";
          this.image = "assets/images/game_images/girl.png"
        }
        else {
          this.gender = "Male";
          this.identity = this.translate.instant('gameLabel.Grand_Son');;
          this.class = "fmly_member men_bg";
          this.image = "assets/images/game_images/fam.png"
        }
        let healthProblem = "";
        if (this.grandChild.health_disease != null && !this.grandChild.dead) {
          let keys = Object.keys(this.grandChild.health_disease);
          let le = keys.length;
          for (let h = 0; h < keys.length; h++) {
            if (this.grandChild.health_disease[keys[h]].status === "GOTSICK") {
              if (healthProblem == "") {
                healthProblem = healthProblem + this.grandChild.health_disease[keys[h]].diseaseName;
              }
              else {
                healthProblem = healthProblem + ", " + this.grandChild.health_disease[keys[h]].diseaseName;
              }
            }
          }
          if (healthProblem === "") {
            healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
          }
          else {
            this.classForHealthStatus = "border-bottom colourRed1"
          }
        }
        else {
          healthProblem = this.translate.instant('gameLabel.None');
            this.classForHealthStatus = "border-bottom"
        }
        if (this.grandChild.job.JobName == this.constantService.JOB_NO_JOB) {
          jobName = "";

        } else {
          jobName = this.grandChild.job.Job_displayName;

        }
        let name = '';
        if (this.grandChild.dead && this.grandChild.age === 0) {
          name = this.translate.instant('gameLabel.Stillborn');
        }
        else {
          name = this.grandChild.full_name
        }

        let granEdu = this.commmonService.setEducationForAll(this.grandChild);
        this.level3Arr.push({
          "name": name,
          "age": this.grandChild.age,
          "education": granEdu,
          "identity": this.identity,
          "sex": this.gender,
          "class": this.class,
          "image": this.image,
          "jobName": jobName,
          "income": this.grandChild.income.toFixed(2),
          "registerIncome": ((this.grandChild.income / this.homeService.allPerson[this.grandChildString].country.ExchangeRate) * this.registerCountryExchangeRate).toFixed(2),
          "health": this.grandChild.traits.health,
          "deadClass": this.deadClass,
          "healthProblem": healthProblem,
          "currencyCode": this.grandChild.country.CurrencyPlural,
          "currencyName": this.grandChild.country.currency_code,
          "motherName": this.homeService.allPerson[this.grandChild.mother].full_name,
          "fatherName": this.homeService.allPerson[this.grandChild.father].full_name,
          "healthClass": this.classForHelath,
          "healthClassForStatus": this.classForHealthStatus
        })
      }
    }

  }
  closeWindow() {
    this.modalWindowShowHide.showChildrenFamily = false;
  }
}
