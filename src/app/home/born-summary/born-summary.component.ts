import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { PostGameService } from '../post-game.service';

@Component({
  selector: 'app-born-summary',
  templateUrl: './born-summary.component.html',
  styleUrls: ['./born-summary.component.css']
})
export class BornSummaryComponent implements OnInit {
  bornSummary;
  messageText;
  successMsgFlag:boolean=false
  friendMailShowFlag:boolean=false;
  emailId
  responseFlag:boolean=false;
  constructor(public modelWindowService:modalWindowShowHide,
    public homeService:HomeService,
    public constantService:ConstantService,
    public gameSummeryService :GameSummeryService,
    public postGameService :PostGameService,
    public translate :TranslateService) { }

  ngOnInit(): void {
    this.modelWindowService.showBornSummary=true;
    this.gameSummeryService.getBornSummary(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res=>{
        this.bornSummary=res;
        this.responseFlag=true;
      })
  }

  sendMail(){
    let obituaryData={}
    obituaryData['obituary']= this.bornSummary;
    this.postGameService.senfObituaryMail(obituaryData,'summary').subscribe(
      res=>{
        if(res){
          this.messageText = this.translate.instant('gameLabel.Send_mail_succesfully');
          this.successMsgFlag=true;   
        }else{
          this.messageText = this.translate.instant('gameLabel.email_not_found');
          this.successMsgFlag=true;  
              }
      }
    )
  }
  successMsgWindowClose(){
    this.successMsgFlag=false;
  }

  sendmailToFriend(){
    this.friendMailShowFlag=true;
    this.emailId='';
  }

  send(){
    this.friendMailShowFlag=false;
    let obituaryData={}
 obituaryData['emailId']=this.emailId;
 obituaryData['obituary']= this.bornSummary;
  this.postGameService.senfObituaryMail(obituaryData,'summary').subscribe(
    res=>{
      if(res){
        this.messageText = this.translate.instant('gameLabel.Send_mail_succesfully');
        this.successMsgFlag=true;   
      }else{
        this.messageText = this.translate.instant('gameLabel.email_not_found');
        this.successMsgFlag=true;  
            }
    })
  }

  closeMail(){
    this.friendMailShowFlag=false;
    this.emailId='';
  }

  OnCloseWindowPopup(){
    this.modelWindowService.showBornSummary=false;
  }

}
