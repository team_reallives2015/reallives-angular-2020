import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BornSummaryComponent } from './born-summary.component';

describe('BornSummaryComponent', () => {
  let component: BornSummaryComponent;
  let fixture: ComponentFixture<BornSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BornSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BornSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
