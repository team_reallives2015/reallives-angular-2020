import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from '../shared/common.service';
import { bugreport } from './bug-report/bugReport';

@Injectable({
  providedIn: 'root'
})
export class PostGameService {
  bugreport = new bugreport();
  constructor(private http: HttpClient,
    private commonService: CommonService) { }

  getObituary(gameId) {
    return this.http.get<any>(`${this.commonService.url}game/getObituaryData/${gameId}`);

  }


  updateObituaryRetutnByMe(gameId, obituaryData, flag) {
    return this.http.post<any>(`${this.commonService.url}game/obituarySave/${gameId}/${flag}`, { obituaryData });

  }

  getLetterData(gameId) {
    return this.http.get<string>(`${this.commonService.url}game/getAllLetters/${gameId}`);

  }

  updateLetterData(gameId, letter) {
    return this.http.post<Boolean>(`${this.commonService.url}game/writeALetter/${gameId}`, { letter });
  }
  getBugReportData(gameId) {
    return this.http.get<any>(`${this.commonService.url}game/bug/getBugReport/${gameId}`);
  }

  updateBugReportData(gameId, bug) {
    return this.http.post<Boolean>(`${this.commonService.url}game/bugReport/${gameId}`, { bug });
  }
  getFeedBackData(gameId) {
    return this.http.get<any>(`${this.commonService.url}game/feedback/getFeedback/${gameId}`);

  }
  updateFeedBackData(gameId, feedback) {
    return this.http.post<any>(`${this.commonService.url}game/feedback/${gameId}`, { feedback });
  }

  getGameSummaryData(gameId) {
    return this.http.get<any>(`${this.commonService.url}game/gameSummarySuggestionGet/${gameId}`);
  }
  updateGameSummaryData(gameId, suggestionData) {
    return this.http.post<any>(`${this.commonService.url}game/gameSummarySuggestionSave/${gameId}`, { suggestionData });
  }

  senfObituaryMail(obituaryData, type) {
    return this.http.post<any>(`${this.commonService.url}game/sendObituaryMail/${type}`, { obituaryData });

  }

}
