import { Component, Input, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-life-expectancy-graph',
  templateUrl: './life-expectancy-graph.component.html',
  styleUrls: ['./life-expectancy-graph.component.css']
})
export class LifeExpectancyGraphComponent implements OnInit {
  @Input() data: boolean = false;
  sex;
  registerCountry;
  bornCountry;
  maleWorlLifeExpectancy = 70.8;
  femalWorldExpectancy = 75.6;
  single: any[];
  multi: any[];
  view: any[] = [300, 200];
  constructor(public homeService: HomeService,
    public translate: TranslateService,
    public constantService: ConstantService,
    public modalWindowShowHide: modalWindowShowHide) { }

  // options
  showXAxis = false;
  showYAxis = false;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';
  showDataLabel = true;
  colorScheme = {
    domain: ['#00FFFF', '#5AA454', '#A10A28', '#C7B42C']
  };
  formatDataLabel(value) {
    return value;
  }
  ngOnInit(): void {
    this.modalWindowShowHide.showLifeExpectancyGraph = true;
    this.setGraphValueForLifeExpectancy();
    this.setGraphValueForIncom();
  }

  setGraphValueForLifeExpectancy() {
    this.sex = this.homeService.allPerson[this.constantService.FAMILY_SELF].sex;
    if (this.sex === "M") {
      this.single = [{
        name: this.translate.instant('gameLabel.You'),
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].age
      },
      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country,
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.MaleLifeExpectancy
      },
      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country,
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.MaleLifeExpectancy
      },
      {
        name: this.translate.instant('gameLabel.World'),
        value: this.maleWorlLifeExpectancy
      }]
    }
    else if (this.sex === "F") {
      this.single = [{
        name: this.translate.instant('gameLabel.You'),
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].age
      },
      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country,
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.FemaleLifeExpectancy
      },
      {
        name: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country,
        value: this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.FemaleLifeExpectancy
      },
      {
        name: this.translate.instant('gameLabel.World'),
        value: this.femalWorldExpectancy
      }]

    }
  }
  setGraphValueForIncom() {
    let selfIncome = parseInt("" + (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome * 12) / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate);
    let countryIncome = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ProdperCapita);
    this.multi = [{
      name: this.translate.instant('gameLabel.Family_Average_Income'),
      value: selfIncome
    },
    {
      name: this.translate.instant('gameLabel.Country_Average_Income'),
      value: countryIncome
    },
    {
      name: this.translate.instant('gameLabel.World_Average_Income'),
      value: 10000
    }]
  }

  onSelect(event) {
    //  console.log(event);
  }
}
