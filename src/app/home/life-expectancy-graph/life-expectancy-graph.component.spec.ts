import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeExpectancyGraphComponent } from './life-expectancy-graph.component';

describe('LifeExpectancyGraphComponent', () => {
  let component: LifeExpectancyGraphComponent;
  let fixture: ComponentFixture<LifeExpectancyGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeExpectancyGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeExpectancyGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
