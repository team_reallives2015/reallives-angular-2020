import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinaceStatusComponent } from './finace-status.component';

describe('FinaceStatusComponent', () => {
  let component: FinaceStatusComponent;
  let fixture: ComponentFixture<FinaceStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinaceStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinaceStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
