import { TestBed } from '@angular/core/testing';

import { FinanceStatusService } from './finance-status.service';

describe('FinanceStatusService', () => {
  let service: FinanceStatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FinanceStatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
