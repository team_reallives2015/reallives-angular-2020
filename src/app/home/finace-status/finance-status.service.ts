import { Injectable } from '@angular/core';
import { ConstantService } from 'src/app/shared/constant.service';

@Injectable({
  providedIn: 'root'
})
export class FinanceStatusService {
  totalLoanValue = 0;
  classForNetToIncome;
  classForEarningMembers;
  classForSavingRate;
  classForSavings;
  classForShelter;
  classForDiet;
  classForNetworth;
  bclassForNetToIncome;
  bclassForEarningMembers;
  bclassForSavingRate;
  bclassForSavings;
  bclassForShelter;
  bclassForDiet;
  bclassForNetworth;
  constructor(public constantService: ConstantService) { }
  calculateLoan(allPerson) {
    if (allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities > 0) {
      if (allPerson[this.constantService.FAMILY_SELF].my_loan_trans.length !== 0) {
        for (let i = 0; i < allPerson[this.constantService.FAMILY_SELF].my_loan_trans.length; i++) {
          this.totalLoanValue = allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].interest;
        }
      }
    }
  }

  checkClassForRedGreen(countOfEarningMember, currentSavingRate, allPerson, flag) {
    //totalaearning members
    if (!flag) {
      if (countOfEarningMember > 0) {
        this.classForEarningMembers = "text-green"
      }
      else {
        this.classForEarningMembers = "text-red"
      }
    }
    if (flag) {
      if (countOfEarningMember > 0) {
        this.bclassForEarningMembers = "text-green"
      }
      else {
        this.bclassForEarningMembers = "text-red"
      }
    }

    //diet
    if (allPerson[this.constantService.FAMILY_SELF].expense.diet.dietIndex < 4) {
      this.classForDiet = "text-red"
      this.bclassForDiet = "text-red"

    }
    else {
      this.classForDiet = "text-green"
      this.bclassForDiet = "text-green"
    }

    //shelter
    if (allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex < 3) {
      this.classForShelter = "text-red"
      this.bclassForShelter = "text-red"
    }
    else {
      this.classForShelter = "text-green"
      this.bclassForShelter = "text-green"
    }

    //savingRate
    if (currentSavingRate <= 0) {
      this.classForSavingRate = "text-red"
      this.bclassForSavingRate = "text-red"
    }
    else {
      this.classForSavingRate = "text-green"
      this.bclassForSavingRate = "text-green"

    }

    //welthRatio
    if (allPerson[this.constantService.FAMILY_SELF].expense.incomeToWealthRatio < 0) {
      this.classForNetToIncome = "text-red";
      this.bclassForNetToIncome = "text-red";
    }
    else {
      this.classForNetToIncome = "text-green";
      this.bclassForNetToIncome = "text-green";

    }
    //savings
    let savings = allPerson[this.constantService.FAMILY_SELF].expense.householdIncome - allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses;
    if (savings <= 0) {
      this.classForSavings = "text-red";
      this.bclassForSavings = "text-red";
    } else {
      this.classForSavings = "text-green";
      this.bclassForSavings = "text-green";

    }

    //networth
    if (allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth < 0) {
      this.classForNetworth = "text-red";
      this.bclassForNetworth = "text-red";
    }
    else {
      this.classForNetworth = "text-green";
      this.bclassForNetworth = "text-green";
    }
  }

}
