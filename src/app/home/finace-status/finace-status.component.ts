import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { FinanceService } from '../action-finance/finance.service';
import { HomeService } from '../home.service';
import { FinanceStatusService } from './finance-status.service';

@Component({
  selector: 'app-finace-status',
  templateUrl: './finace-status.component.html',
  styleUrls: ['./finace-status.component.css']
})
export class FinaceStatusComponent implements OnInit {
  familyMemberCount=0;
  usCountryName
  registerCountryExchangeRate
  registerCountryName
  currencyCode
  countryName
  registerCountryCode
  registerCountryCurrentcyName
  showInvestmentButton: boolean = false;
  showLoanButton: boolean = false;
  countOfEarningMember = 0;
  subscribe;
  currentSavingRate: number;
  expenceFoodPer: number;
  expenceFoodPerInRes: number;
  expenceShelterPer: number;
  fMamber = '#FF0000';
  diet;
  shelter;
  statusSatement = "";
  moreInforStm;
  moreInfoFlag: boolean = false;
  moreInfoTitle;
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public homeService: HomeService,
    public constantService: ConstantService,
    public financeService: FinanceService,
    public commonService: CommonService,
    public translate: TranslateService,
    public finanaceStatusService: FinanceStatusService) {
    this.subscribe = this.homeService.changeAllPerson.subscribe(persons => {
      this.homeService.allPerson = persons;
      this.finanaceStatus();
      this.finanaceStatusService.checkClassForRedGreen(this.countOfEarningMember, this.currentSavingRate, this.homeService.allPerson, false);
      this.finanaceStatusService.checkClassForRedGreen(this.countOfEarningMember, this.currentSavingRate, this.homeService.allPerson, true);
      this.getFamilyMemberCount();

    })
  }

  ngOnInit(): void {
    this.finanaceStatus();
    this.finanaceStatusService.checkClassForRedGreen(this.countOfEarningMember, this.currentSavingRate, this.homeService.allPerson, false);
    this.finanaceStatusService.checkClassForRedGreen(this.countOfEarningMember, this.currentSavingRate, this.homeService.allPerson, true);
    this.getFamilyMemberCount();
  }

  finanaceStatus() {
    this.translate.get('gameLabel.status_stm', { status: "'" + this.commonService.statusValue + "'" }).subscribe((s: string) => {
      this.statusSatement = s;
    })
    this.countOfEarningMember = 0;
    this.registerCountryCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.currency_code;
    this.registerCountryCurrentcyName = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural;
    this.usCountryName = this.constantService.US_COUNTRYNAME;
    this.registerCountryExchangeRate = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.registerCountryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    this.currencyCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code;
    this.countryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country;
    this.diet = this.financeService.diet[this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.dietIndex].name
    this.shelter = this.financeService.shelter[this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex].name

    //
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense > 0 && this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome > 0) {
      this.expenceFoodPer = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense * 100 / this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome;
      this.expenceFoodPerInRes = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate;
      //  this.expenceFoodPerInRes=(usd*this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate);
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense <= 0 || this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.expenceFoodPer = 0;
      this.expenceFoodPerInRes = 0;
    }
    //

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense > 0 && this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome > 0) {
      this.expenceShelterPer = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense * 100 / this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense <= 0 || this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.expenceShelterPer = 0;
    }






    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome >= 0 && this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses <= 0
      && this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.currentSavingRate = 0;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome <= 0) {
      this.currentSavingRate = 0;
    }
    else {
      this.currentSavingRate = (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome - this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses) / (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome) * 100
    }


    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdInvestmentValue > 0) {
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
        this.showInvestmentButton = true;
      }
    }
    else {
      this.showInvestmentButton = false;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities > 0) {
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household) {
        this.showLoanButton = true;
      }
    }
    else {
      this.showLoanButton = false;
    }
    this.finanaceStatusService.calculateLoan(this.homeService.allPerson);

    let allPersonkeys = Object.keys(this.homeService.allPerson)
    for (let identity of allPersonkeys) {
      if (identity !== "selected_language" && identity !== "sdgId" && identity !== "sdgDetails" && identity !== "school_name"
        && identity !== "show_doc" && identity !== "user_fullname" && identity !== "username") {
        if (this.homeService.allPerson[identity].living_at_home) {
          if (this.homeService.allPerson[identity].income > 0) {
            this.countOfEarningMember = this.countOfEarningMember + 1;
          }
        }
      }
    }

  }

  moreInfo() {
    this.moreInfoFlag = true;
    let s1 = this.translate.instant('gameLabel.status_stm_tlp1');
    let s2 = this.translate.instant('gameLabel.status_stm_tlp2');
    let s3 = this.translate.instant('gameLabel.status_stm_tlp3');
    let s4 = this.translate.instant('gameLabel.status_stm_tlp4');
    let s5 = this.translate.instant('gameLabel.status_stm_tlp5');
    this.moreInfoTitle = s1 + "<br><br>"
    this.moreInforStm = s2 + "<br><br>" + s3 + "<br><br>" + s4 + "<br><br>" + s5;
  }

  successMsgWindowClose() {
    this.moreInfoFlag = false;
  }

  openLoanAndInvestMent() {
    this.modalWindowShowHide.showActionFinanace = true;
  }

  getFamilyMemberCount(){
    let count=0;
    if(this.homeService.allPerson[this.constantService.FAMILY_MOTHER].living_at_home &&!this.homeService.allPerson[this.constantService.FAMILY_MOTHER].dead){
      count=count+1;
    }
    if(this.homeService.allPerson[this.constantService.FAMILY_FATHER].living_at_home &&!this.homeService.allPerson[this.constantService.FAMILY_FATHER].dead){
      count=count+1;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_HUSBAND] != null) {
      if(this.homeService.allPerson[this.constantService.FAMILY_HUSBAND].living_at_home  && !this.homeService.allPerson[this.constantService.FAMILY_HUSBAND].dead){
        count=count+1;
      }
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_WIFE] != null) {
      if(this.homeService.allPerson[this.constantService.FAMILY_WIFE].living_at_home && !this.homeService.allPerson[this.constantService.FAMILY_WIFE].dead){
        count=count+1;
      }
    }
    let mainPerson=this.homeService.allPerson[this.constantService.FAMILY_SELF];
    if (mainPerson[this.constantService.FAMILY_SIBLING] != null) {
      let siblings = mainPerson[this.constantService.FAMILY_SIBLING];
      for (var i = 0; i <siblings.length; i++) {
        let sibString = mainPerson.siblings[i];
        let sib = this.homeService.allPerson[sibString];
        if(sib.living_at_home && !sib.dead){
          count=count+1;
        }
      }
    }
    if (mainPerson[this.constantService.FAMILY_CHILDREN] != null) {
      let childrens = mainPerson[this.constantService.FAMILY_CHILDREN];
      for (var i = 0; i <childrens.length; i++) {
        let childString = mainPerson.children[i];
        let child = this.homeService.allPerson[childString];
        if(child.living_at_home && !child.dead){
          count=count+1;
        }
      }
    }
    if (mainPerson[this.constantService.FAMILY_GRANDCHILDREN] != null) {
      let grandChildrens = mainPerson[this.constantService.FAMILY_GRANDCHILDREN];
      for (var i = 0; i <grandChildrens.length; i++) {
        let grandchildString = mainPerson.grand_children[i];
        let grandchild = this.homeService.allPerson[grandchildString];
        if(grandchild.living_at_home &&!grandchild.dead){
          count=count+1;
        }
      }
    }
   this.familyMemberCount=count+1;
    }
  

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }
}
