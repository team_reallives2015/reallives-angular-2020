import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { interval } from 'rxjs';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { CommonActionService } from '../common-action.service';
import { HomeService } from '../home.service';
import { FinanceService } from './finance.service';
import { CarrerService } from '../action-carrer/carrer.service';

@Component({
  selector: 'app-action-finance',
  templateUrl: './action-finance.component.html',
  styleUrls: ['./action-finance.component.css']
})
export class ActionFinanceComponent implements OnInit {
  changeExpenceButtonClass="for-bx-shadow btn btn_blue1 font_s4 active font_s4-ipad"
  financePlaceholder = ""
  cardSize = "";
  cardSizeFinance = ""
  businessType;
  businessRegister;
  totalEmp;
  totalMaleEmp = 0;
  totalFemaleEmp = 0
  FamMember = 0;

  headButtonWarrFlag: boolean = false;
  headWarr = ''
  allPerosn;
  mainPerson;
  jobType;
  selfJobName;
  currencyCode;
  currencyName;
  registerCountryIncome;
  registerCountryCode;
  registerCountryCurrentcyName;
  currentIncomeFamilyArray = [];
  countryName;
  registerCountryName;
  usCountryName;
  registerCountryExchangeRate;
  chagneExpenceWindowFlag: boolean = false;
  classActive = "shw_div active";
  classInActive = "hide_div"
  selectedLevel;
  dietIndex;
  charityIndex;
  consumerIndex;
  shelterIndex;
  otherCost: number = 0;
  dietCost: number = 0;
  consumerCost: number = 0;
  shelterCost: number = 0;
  charityCost: number = 0;
  totalCost: number = 0;
  level1Arr = [];
  level2Arr = [];
  level3Arr = [];
  level3CheckArr = [];
  level4Arr = [];
  dietName;
  shelterName;
  charityName;
  consumerName;
  messageText;
  messageType;
  MessageShow: boolean = false;
  updatedObject;
  allCarrerCards: [] = [];
  firstCards: [] = [];
  cardLength;
  default = 0;
  showAlertFlag: boolean = false;
  alertClass;
  alertMsg;
  alertType;
  showLoanClass = "shw_div";
  showAllClass = "shw_div active";
  showLoanMetaData: boolean = false;
  showInvestmentMetaData: boolean = false;
  allLoanMetaDataList = [];
  finalMaxLoanForIndex0;
  max;
  householdNetworth;
  finalmax;
  maximum;
  investmentMetadata = [];
  value;
  investMentId;
  investMentAmount;
  investMentType;
  allInvesmentCards = [];
  allLoansCards = [];
  allCards = [];
  familyloans = [];
  businessLoans = [];
  displayOnlyInvestmentCard: boolean = false;
  displayOnlyLoansCard: boolean = false;
  displayAllCards: boolean = false
  displayBusinessLoansCards: boolean = false;
  dispalyFamilyLoansCards: boolean = false;
  loan; investment;
  amount;
  flag;
  allJobCardsForSelf: any = [];
  firstCardForSelf: any = [];
  showAllFamilyJobList: boolean = false;
  lineClass;
  result;
  charityIndexChangeFlag: boolean = false;
  loanDisable: boolean = false;
  investmentDisable: boolean = false;
  successMsgFlag: boolean = false;
  messageClass;
  showChangeExpenditureDecisionFlag: boolean = false;
  loanMin;
  min;
  clickFirst: boolean = false;
  displayBusinessBankLoan: boolean = false;
  dispalySharkLoan: boolean = false
  disaplayAllbusinessCards: boolean = false;
  dispalyFamilyAndFriendLoan: boolean = false;
  businessSharkCards = [];
  allBusinessBankCards = [];
  businessallCards = [];
  businessFriendLoans = [];
  totalBusinessLoan = 0;
  allLoanInvestMentCards = [];
  oldBusinessFlag: boolean = false;
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public homeService: HomeService,
    public carrerService: CarrerService,
    public constantService: ConstantService,
    public financeService: FinanceService,
    public commonService: CommonService,
    public commonActionService: CommonActionService,
    public translation: TranslateService,
    public translationService: TranslationService) {
    interval(30000).subscribe(x => {
      if (this.showAlertFlag) {
        this.showAlertFlag = false;
      }
    });
  }

  ngOnInit(): void {
    this.financeService.selectionFlag = false
    this.allPerosn = this.homeService.allPerson;
    this.currencyCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.currency_code;
    this.currencyName = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.CurrencyPlural;
    this.registerCountryCode = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.currency_code;
    this.registerCountryCurrentcyName = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.CurrencyPlural;
    this.countryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.country;
    this.registerCountryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.country;
    this.usCountryName = this.constantService.US_COUNTRYNAME;
    if (this.countryName.length > 4 || this.registerCountryName.length > 4) {
      this.countryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].country.Code;
      this.registerCountryName = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.Code;
    }
    this.registerCountryExchangeRate = this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate;
    this.registerCountryIncome = ((this.homeService.allPerson[this.constantService.FAMILY_SELF].income / this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate) * this.homeService.allPerson[this.constantService.FAMILY_SELF].register_country.ExchangeRate).toFixed(2);
    this.financeService.getAllLoanInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
      res => {
        this.allLoanInvestMentCards = res;
        this.carrerService.oldBusinessFlag = this.commonService.checkBusinessTyoeOldOrNew(this.homeService.allPerson[this.constantService.FAMILY_SELF].business);
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId !== 130 && !this.carrerService.oldBusinessFlag) {
          this.financeService.selectionFlag = true
          this.businessCall();
        }
        else {
          this.financeService.selectionFlag = false
        }
        this.financeCall();
      })
    this.financePlaceholder = this.translation.instant('gameLabel.Total_Balance');
    if(this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household){
      this.changeExpenceButtonClass="for-bx-shadow btn btn_blue1-single font_s4 active font_s4-ipad"
    }
  }



  financeCall() {
    this.currentIncomefamilyArray(this.homeService.allPerson);
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessesId === 130) {
      this.showBusinessLoans();
    }
    this.showAll();
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.Business === this.constantService.BUSINESS_NO_BUSINESS) {
      this.jobType = 'job';
      this.selfJobName = this.homeService.allPerson[this.constantService.FAMILY_SELF].job.Job_displayName;
    }
    else {
      this.jobType = 'business';
      this.selfJobName = this.homeService.allPerson[this.constantService.FAMILY_SELF].business.Business;
    }

    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].dead) {
      this.messageText = this.translation.instant('gameLabel.You_cannot_take_any_action_because_you_are_dead');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    else if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household && !this.carrerService.loanFlag && !this.financeService.selectionFlag) {
      this.messageText = this.translation.instant('gameLabel.head_of_the_household_warrning');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].age < 14 && !this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household && !this.financeService.selectionFlag) {
      this.messageText = this.translation.instant('gameLabel.You_cannot_take_any_action_till_you_are_15_years_old.');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    this.disableLoanAndInvestment();
    if (!this.homeService.displaySdgFlag) {
      this.cardSizeFinance = "col-md-5 mb-3"
    }
    else {
      this.cardSizeFinance = "col-md-4 mb-3"
    }
  }

  businessCall() {
    this.showAllBusinessLoans();
    this.setBusinessValue();
    if (!this.homeService.displaySdgFlag) {
      this.cardSize = "col-md-5 mb-3"
    }
    else {
      this.cardSize = "col-md-4 mb-3"
    }
  }



  setBusinessValue() {
    this.getBusinessType(this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessType);
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.isRegistered) {
      this.businessRegister = this.translation.instant('gameLabel.Yes');
    }
    else {
      this.businessRegister = this.translation.instant('gameLabel.No')
    }

    this.totalEmp = this.homeService.allPerson[this.constantService.FAMILY_SELF].business['skilledEmp'].totalSkilled +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['semiSkilledEmp'].totalSemiskilled +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['unSkilledEmp'].totalUnskilled


    this.totalMaleEmp = this.homeService.allPerson[this.constantService.FAMILY_SELF].business['skilledEmp'].male +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['semiSkilledEmp'].male +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['unSkilledEmp'].male

    this.totalFemaleEmp = this.homeService.allPerson[this.constantService.FAMILY_SELF].business['skilledEmp'].female +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['semiSkilledEmp'].female +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business['unSkilledEmp'].female

    this.FamMember = this.homeService.allPerson[this.constantService.FAMILY_SELF].business.famMember.length
    this.totalBusinessLoan = this.homeService.allPerson[this.constantService.FAMILY_SELF].business.bankLoan +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business.sharkLoan +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business.friendLoan +
      this.homeService.allPerson[this.constantService.FAMILY_SELF].business.familyLoan

  }

  getBusinessType(typeId) {
    if (typeId === 1) {
      this.businessType = this.translation.instant('gameLabel.Micro')
    }
    else if (typeId === 2) {
      this.businessType = this.translation.instant('gameLabel.Small')

    }
    else if (typeId === 3) {
      this.businessType = this.translation.instant('gameLabel.Medium')

    }
    else if (typeId === 4) {
      this.businessType = this.translation.instant('gameLabel.Large')

    }

  }

  currentIncomefamilyArray(allPerosn) {
    let mainPerson = allPerosn[this.constantService.FAMILY_SELF]
    // code for wife/husband
    if (mainPerson.sex === this.constantService.FEMALE) {
      if (mainPerson['husband'] != null) {
        let husband = allPerosn[this.constantService.FAMILY_HUSBAND];
        if (husband.living_at_home && !husband.dead) {
          this.currentIncomeFamilyArray.push({
            "fullName": husband.full_name,
            "identity": this.translation.instant('gameLabel.Husband'),
            "jobName": husband.job.Job_displayName,
            "income": (husband.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((husband.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName
          })

        }
      }
    }
    else if (mainPerson.sex === this.constantService.MALE) {
      if (mainPerson['wife'] != null) {
        let wife = allPerosn[this.constantService.FAMILY_WIFE];
        if (wife.living_at_home && !wife.dead) {
          this.currentIncomeFamilyArray.push({
            "fullName": wife.full_name,
            "identity": this.translation.instant('gameLabel.Wife'),
            "jobName": wife.job.Job_displayName,
            "income": (wife.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((wife.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName
          })

        }
      }

    }

    //code fro mother 
    if (mainPerson['mother'] != null) {
      let mother = allPerosn[this.constantService.FAMILY_MOTHER];
      if (mother.living_at_home && !mother.dead) {
        this.currentIncomeFamilyArray.push({
          "fullName": mother.full_name,
          "identity": this.translation.instant('gameLabel.Mother'),
          "jobName": mother.job.Job_displayName,
          "income": (mother.income).toFixed(2),
          "code": this.currencyCode,
          "currencyName": this.currencyName,
          "registerIncome": ((mother.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "registerCountryCode": this.registerCountryCode,
          "registerCountryCurrency": this.registerCountryCurrentcyName
        })

      }
    }

    //code fro father 
    if (mainPerson['father'] != null) {
      let father = allPerosn[this.constantService.FAMILY_FATHER];
      if (father.living_at_home && !father.dead) {
        this.currentIncomeFamilyArray.push({
          "fullName": father.full_name,
          "identity": this.translation.instant('gameLabel.Father'),
          "jobName": father.job.Job_displayName,
          "income": (father.income).toFixed(2),
          "code": this.currencyCode,
          "currencyName": this.currencyName,
          "registerIncome": ((father.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "registerCountryCode": this.registerCountryCode,
          "registerCountryCurrency": this.registerCountryCurrentcyName,

        })


      }

    }

    //code for siblings
    if (mainPerson['siblings'].length !== 0) {
      let siblings = mainPerson[this.constantService.FAMILY_SIBLING];
      for (let i = 0; i < siblings.length; i++) {
        let identity;
        let sibString = allPerosn[this.constantService.FAMILY_SELF].siblings[i];
        let sib = allPerosn[sibString];
        if (sib.living_at_home && !sib.dead) {
          if (sib.sex === this.constantService.FEMALE) {
            identity = this.translation.instant('gameLabel.Brother');
          }
          else {
            identity = this.translation.instant('gameLabel.Sister');
          }
          this.currentIncomeFamilyArray.push({
            "fullName": sib.full_name,
            "identity": identity,
            "jobName": sib.job.Job_displayName,
            "income": (sib.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((sib.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName
          })
        }

      }
    }

    //code for children
    if (mainPerson['children'].length != 0) {
      let children = mainPerson[this.constantService.FAMILY_CHILDREN];
      for (let i = 0; i < children.length; i++) {
        let identity;
        let childString = allPerosn[this.constantService.FAMILY_SELF].children[i];
        let child = allPerosn[childString];
        if (child.living_at_home && !child.dead) {
          if (child.sex === this.constantService.FEMALE) {
            identity = this.translation.instant('gameLabel.Daughter');
          }
          else {
            identity = this.translation.instant('gameLabel.Son');
          }
          this.currentIncomeFamilyArray.push({
            "fullName": child.full_name,
            "identity": identity,
            "jobName": child.job.Job_displayName,
            "income": (child.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((child.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName
          })
        }
      }
    }
    //code for grand child
    if (mainPerson['grand_children'].length != 0) {
      let grandChilds = mainPerson[this.constantService.FAMILY_GRANDCHILDREN];
      for (let i = 0; i < grandChilds.length; i++) {
        let identity;
        let grandChildString = allPerosn[this.constantService.FAMILY_SELF].grand_children[i];
        let grandChild = allPerosn[grandChildString];
        if (grandChild.living_at_home && !grandChild.dead) {
          if (grandChild.sex === this.constantService.FEMALE) {
            identity = this.translation.instant('gameLabel.Grand_Daughter');
          }
          else {
            identity = this.translation.instant('gameLabel.Grand_Son');
          }
          this.currentIncomeFamilyArray.push({
            "fullName": grandChild.full_name,
            "identity": identity,
            "jobName": grandChild.job.Job_displayName,
            "income": (grandChild.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((grandChild.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName
          })
        }
      }
    }
  }

  displayAllJobListForOther(allPerosn) {
    this.level1Arr = [];
    this.level2Arr = [];
    this.level3Arr = [];
    this.level4Arr = [];
    let mainPerson = allPerosn[this.constantService.FAMILY_SELF]


    // code for wife/husband
    if (mainPerson.sex === this.constantService.MALE) {
      if (mainPerson['husband'] != null) {
        let husband = allPerosn[this.constantService.FAMILY_HUSBAND];
        if (husband.job.Job_displayName !== husband.previous_job_name) {
          this.level1Arr.push({
            "fullName": husband.full_name,
            "identity": this.translation.instant('gameLabel.Husband'),
            "jobName": husband.job.Job_displayName,
            "income": (husband.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((husband.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": husband.previous_job_name,
            "priviousJobSalary": husband.previous_job_salary,
            "registerIncomeprivious": ((husband.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": true
          })

        }
        else {
          this.level1Arr.push({
            "fullName": husband.full_name,
            "identity": this.translation.instant('gameLabel.Husband'),
            "jobName": husband.job.Job_displayName,
            "income": (husband.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((husband.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": husband.previous_job_name,
            "priviousJobSalary": husband.previous_job_salary,
            "registerIncomeprivious": ((husband.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": false

          })
        }
      }
    }
    else if (mainPerson.sex === this.constantService.FEMALE) {
      if (mainPerson['wife'] != null) {
        let wife = allPerosn[this.constantService.FAMILY_WIFE];
        if (wife.job.Job_displayName !== wife.previous_job_name) {

          this.level1Arr.push({
            "fullName": wife.full_name,
            "identity": this.translation.instant('gameLabel.Wife'),
            "jobName": wife.job.Job_displayName,
            "income": (wife.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((wife.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": wife.previous_job_name,
            "priviousJobSalary": wife.previous_job_salary,
            "registerIncomeprivious": ((wife.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": true
          })

        }
        else {
          this.level1Arr.push({
            "fullName": wife.full_name,
            "identity": this.translation.instant('gameLabel.Wife'),
            "jobName": wife.job.Job_displayName,
            "income": (wife.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((wife.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": wife.previous_job_name,
            "priviousJobSalary": wife.previous_job_salary,
            "registerIncomeprivious": ((wife.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": false
          })
        }
      }

    }

    //code fro mother 
    if (mainPerson['mother'] != null) {
      let mother = allPerosn[this.constantService.FAMILY_MOTHER];
      if (mother.job.Job_displayName !== mother.previous_job_name) {

        this.level1Arr.push({
          "fullName": mother.full_name,
          "identity": this.translation.instant('gameLabel.Mother'),
          "jobName": mother.job.Job_displayName,
          "income": (mother.income).toFixed(2),
          "code": this.currencyCode,
          "currencyName": this.currencyName,
          "registerIncome": ((mother.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "registerCountryCode": this.registerCountryCode,
          "registerCountryCurrency": this.registerCountryCurrentcyName,
          "priviousJob": mother.previous_job_name,
          "priviousJobSalary": mother.previous_job_salary,
          "registerIncomeprivious": ((mother.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "flagForDisplay": true
        })
      }
      else {
        this.level1Arr.push({
          "fullName": mother.full_name,
          "identity": this.translation.instant('gameLabel.Mother'),
          "jobName": mother.job.Job_displayName,
          "income": (mother.income).toFixed(2),
          "code": this.currencyCode,
          "currencyName": this.currencyName,
          "registerIncome": ((mother.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "registerCountryCode": this.registerCountryCode,
          "registerCountryCurrency": this.registerCountryCurrentcyName,
          "priviousJob": mother.previous_job_name,
          "priviousJobSalary": mother.previous_job_salary,
          "registerIncomeprivious": ((mother.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "flagForDisplay": false
        })
      }
    }

    //code fro father 
    if (mainPerson['father'] != null) {
      let father = allPerosn[this.constantService.FAMILY_FATHER];
      if (father.job.Job_displayName !== father.previous_job_name) {

        this.level1Arr.push({
          "fullName": father.full_name,
          "identity": this.translation.instant('gameLabel.Father'),
          "jobName": father.job.Job_displayName,
          "income": (father.income).toFixed(2),
          "code": this.currencyCode,
          "currencyName": this.currencyName,
          "registerIncome": ((father.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "registerCountryCode": this.registerCountryCode,
          "registerCountryCurrency": this.registerCountryCurrentcyName,
          "priviousJob": father.previous_job_name,
          "priviousJobSalary": father.previous_job_salary,
          "registerIncomeprivious": ((father.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "flagForDisplay": true
        })
      }
      else {
        this.level1Arr.push({
          "fullName": father.full_name,
          "identity": this.translation.instant('gameLabel.Father'),
          "jobName": father.job.Job_displayName,
          "income": (father.income).toFixed(2),
          "code": this.currencyCode,
          "currencyName": this.currencyName,
          "registerIncome": ((father.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "registerCountryCode": this.registerCountryCode,
          "registerCountryCurrency": this.registerCountryCurrentcyName,
          "priviousJob": father.previous_job_name,
          "priviousJobSalary": father.previous_job_salary,
          "registerIncomeprivious": ((father.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
          "flagForDisplay": false
        })
      }

    }

    //code for siblings
    if (mainPerson['siblings'].length !== 0) {
      let siblings = mainPerson[this.constantService.FAMILY_SIBLING];
      for (let i = 0; i < siblings.length; i++) {
        let identity;
        let sibString = this.homeService.allPerson[this.constantService.FAMILY_SELF].siblings[i];
        let sib = allPerosn[sibString];
        if (sib.sex === this.constantService.FEMALE) {
          identity = this.translation.instant('gameLabel.Sister');
        }
        else {
          identity = this.translation.instant('gameLabel.Brother');
        }

        if (sib.job.Job_displayName !== sib.previous_job_name) {

          this.level2Arr.push({
            "fullName": sib.full_name,
            "identity": identity,
            "jobName": sib.job.Job_displayName,
            "income": (sib.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((sib.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": sib.previous_job_name,
            "priviousJobSalary": sib.previous_job_salary,
            "registerIncomeprivious": ((sib.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": true

          })
        }
        else {
          this.level2Arr.push({
            "fullName": sib.full_name,
            "identity": identity,
            "jobName": sib.job.Job_displayName,
            "income": (sib.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((sib.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": sib.previous_job_name,
            "priviousJobSalary": sib.previous_job_salary,
            "registerIncomeprivious": ((sib.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": false

          })

        }

      }
    }

    //code for children
    if (mainPerson['children'].length != 0) {
      let children = mainPerson[this.constantService.FAMILY_CHILDREN];
      for (let i = 0; i < children.length; i++) {
        let identity;
        let childString = this.homeService.allPerson[this.constantService.FAMILY_SELF].children[i];
        let child = allPerosn[childString];
        if (child.sex === this.constantService.MALE) {
          identity = this.translation.instant('gameLabel.Son');
        }
        else {
          identity = this.translation.instant('gameLabel.Daughter');
        }
        if (child.job.Job_displayName !== child.previous_job_name) {

          this.level1Arr.push({
            "fullName": child.full_name,
            "identity": identity,
            "jobName": child.job.Job_displayName,
            "income": (child.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((child.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": child.previous_job_name,
            "priviousJobSalary": child.previous_job_salary,
            "registerIncomeprivious": ((child.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": true

          })
        }
        else {
          this.level1Arr.push({
            "fullName": child.full_name,
            "identity": identity,
            "jobName": child.job.Job_displayName,
            "income": (child.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((child.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": child.previous_job_name,
            "priviousJobSalary": child.previous_job_salary,
            "registerIncomeprivious": ((child.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": false

          })

        }
      }
    }
    //code for grand child
    if (mainPerson['grand_children'].length != 0) {
      let grandChilds = mainPerson[this.constantService.FAMILY_GRANDCHILDREN];
      for (let i = 0; i < grandChilds.length; i++) {
        let identity;
        let grandChildString = this.homeService.allPerson[this.constantService.FAMILY_SELF].grandChild;
        let grandChild = allPerosn[grandChildString];
        if (grandChild.sex === this.constantService.MALE) {
          identity = this.translation.instant('gameLabel.Grand_Son');
        }
        else {
          identity = this.translation.instant('gameLabel.Grand_Daughter');
        }
        if (grandChild.job.Job_displayName !== grandChild.previous_job_name) {

          this.level2Arr.push({
            "fullName": grandChild.full_name,
            "identity": identity,
            "jobName": grandChild.job.Job_displayName,
            "income": (grandChild.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((grandChild.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": grandChild.previous_job_name,
            "priviousJobSalary": grandChild.previous_job_salary,
            "registerIncomeprivious": ((grandChild.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": true
          })
        }

        else {
          this.level2Arr.push({
            "fullName": grandChild.full_name,
            "identity": identity,
            "jobName": grandChild.job.Job_displayName,
            "income": (grandChild.income).toFixed(2),
            "code": this.currencyCode,
            "currencyName": this.currencyName,
            "registerIncome": ((grandChild.income / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "registerCountryCode": this.registerCountryCode,
            "registerCountryCurrency": this.registerCountryCurrentcyName,
            "priviousJob": grandChild.previous_job_name,
            "priviousJobSalary": grandChild.previous_job_salary,
            "registerIncomeprivious": ((grandChild.previous_job_salary / mainPerson.country.ExchangeRate) * mainPerson.register_country.ExchangeRate).toFixed(2),
            "flagForDisplay": false
          })
        }
      }
    }
  }

  displayAllJobListForSelf() {
    this.allJobCardsForSelf = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
    this.cardLength = this.allJobCardsForSelf.length;
    if (this.cardLength == 1) {
      this.firstCardForSelf.push(this.allJobCardsForSelf[this.default]);
      this.allJobCardsForSelf = [];
    }
    else if (this.allJobCardsForSelf.length > 1) {
      this.firstCardForSelf.push(this.allJobCardsForSelf[this.cardLength - 1]);
      this.firstCardForSelf.currentSalary = this.homeService.allPerson[this.constantService.FAMILY_SELF].income
      this.allJobCardsForSelf.reverse();
      // for(let i=0;i<this.allJobCardsForSelf.length;i++){
      //   this.displayJobAsPerIncome(this.allJobCardsForSelf[i].currentSalary,this.homeService.allPerson[this.constantService.FAMILY_SELF]);
      // }
    }
  }

  allIncomeDisplay() {

    this.showAllFamilyJobList = true;
    this.displayAllJobListForSelf();
    this.displayAllJobListForOther(this.homeService.allPerson);
  }

  closeAllIncomeDisplay() {
    this.showAllFamilyJobList = false;

  }

  fillAllCost(mainPerson) {
    //diet
    this.financeService.diet[0].cost = (mainPerson.expense.diet.starvationDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members);
    this.financeService.diet[1].cost = mainPerson.expense.diet.survivalDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members;
    this.financeService.diet[2].cost = mainPerson.expense.diet.meagerDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members;
    this.financeService.diet[3].cost = mainPerson.expense.diet.minimalDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members;
    this.financeService.diet[4].cost = mainPerson.expense.diet.adequateDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members;
    this.financeService.diet[5].cost = mainPerson.expense.diet.ampleDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members;
    this.financeService.diet[6].cost = mainPerson.expense.diet.lavishDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members;
    this.financeService.diet[7].cost = mainPerson.expense.diet.gourmandDietCost * this.homeService.allPerson[this.constantService.FAMILY_SELF].count_of_household_members;
    //shelter
    this.financeService.shelter[0].cost = mainPerson.expense.shelter.homelessShelterCost.toFixed(2);
    this.financeService.shelter[1].cost = mainPerson.expense.shelter.illegalTemperoryShelterCost.toFixed(2);
    this.financeService.shelter[2].cost = mainPerson.expense.shelter.simplestSharedShelterCost.toFixed(2);
    this.financeService.shelter[3].cost = mainPerson.expense.shelter.simplestShelterCost.toFixed(2);
    this.financeService.shelter[4].cost = mainPerson.expense.shelter.modestShelterCost.toFixed(2);
    this.financeService.shelter[5].cost = mainPerson.expense.shelter.moderateShelterCost.toFixed(2);
    this.financeService.shelter[6].cost = mainPerson.expense.shelter.ampleShelterCost.toFixed(2);
    this.financeService.shelter[7].cost = mainPerson.expense.shelter.luxuriousShelterCost.toFixed(2);
    //consumer
    this.financeService.consumer[0].cost = mainPerson.expense.consumerItems.miserlyConsumerCost.toFixed(2);
    this.financeService.consumer[1].cost = mainPerson.expense.consumerItems.carefulConsumerCost.toFixed(2);
    this.financeService.consumer[2].cost = mainPerson.expense.consumerItems.normalConsumerCost.toFixed(2);
    this.financeService.consumer[3].cost = mainPerson.expense.consumerItems.enthusiasticConsumerCost.toFixed(2);
    this.financeService.consumer[4].cost = mainPerson.expense.consumerItems.spendThriftConsumerCost.toFixed(2);
    //charity
    this.financeService.charity[0].cost = mainPerson.expense.charity.nothingCharityCost.toFixed(2);
    this.financeService.charity[1].cost = mainPerson.expense.charity.tokenCharityCost.toFixed(2);
    this.financeService.charity[2].cost = mainPerson.expense.charity.respectableCharityCost.toFixed(2);
    this.financeService.charity[3].cost = mainPerson.expense.charity.substantialCharityCost.toFixed(2);
    this.financeService.charity[4].cost = mainPerson.expense.charity.saintlyCharityCost.toFixed(2);



  }

  showExpenceChangeWindow() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_flag || this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government_flag) {
      this.messageText = this.translation.instant('gameLabel.assistance_warr');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    }
    else {
      this.chagneExpenceWindowFlag = true;
      this.fillAllCost(this.homeService.allPerson[this.constantService.FAMILY_SELF]);
      this.dietIndex = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.dietIndex;
      this.shelterIndex = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex;
      this.charityIndex = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity.charityIndex;
      this.consumerIndex = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.consumerIndex;
      this.otherCost = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.otherExpenses.toFixed(2);
      this.totalCost = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses.toFixed(2)
      this.selectCostAndName(this.dietIndex, "diet");
      this.selectCostAndName(this.shelterIndex, "shelter");
      this.selectCostAndName(this.charityIndex, "charity");
      this.selectCostAndName(this.consumerIndex, "consumer");
      this.CalculateTotalExpense();
      this.classActive = 'hide_div';
      this.classInActive = "shw_div active";
    }
  }

  checkCharityIndexValueChange() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity.charityIndex != this.charityIndex) {
      if (this.charityIndex === 0) {
        this.charityIndexChangeFlag = false;
      }
      else {
        this.charityIndexChangeFlag = true;
      }
    }
    else {
      this.charityIndexChangeFlag = false;
    }
  }

  saveChangeExpenceToPerson() {
    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.dietIndex = this.dietIndex;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.diet.householdFoodExpense = this.dietCost;


    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.shelterIndex = this.shelterIndex;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.shelter.householdHousingExpense = this.shelterCost;


    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity.charityIndex = this.charityIndex;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.charity.householdCharityExpense = this.charityCost;


    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.consumerIndex = this.consumerIndex;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.consumerItems.householdConsumerExpense = this.consumerCost;

    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.otherExpenses = this.otherCost;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdExpenses = this.totalCost;

  }

  CalculateTotalExpense() {
    if (!this.clickFirst) {
      this.otherCost = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.otherExpenses;
    }
    else {
      if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth < 0) {
        this.otherCost = 0;
      }
      else {
        this.otherCost = Math.round(((this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.03)));
        let othercost1 = this.dietCost + this.consumerCost + this.shelterCost;
        if (othercost1 < this.otherCost) {
          this.otherCost = othercost1;
        }
      }
    }
    this.totalCost = Math.round((this.dietCost + this.charityCost + this.consumerCost + this.shelterCost + this.otherCost));
  }

  selectCostAndName(index, indexName) {
    switch (indexName) {
      case "diet":
        this.dietCost = this.financeService.diet[index].cost;
        this.dietName = this.financeService.diet[index].name;
        break;
      case "shelter":
        this.shelterCost = this.financeService.shelter[index].cost * 1;
        this.shelterName = this.financeService.shelter[index].name;
        break;
      case "charity":
        this.charityCost = this.financeService.charity[index].cost * 1;
        this.charityName = this.financeService.charity[index].name;
        break;
      case "consumer":
        this.consumerCost = this.financeService.consumer[index].cost * 1;
        this.consumerName = this.financeService.consumer[index].name;
        break;
    }

  }

  selected(value, index) {
    this.selectCostAndName(index, value);
    this.CalculateTotalExpense();
    this.clickFirst = true;

  }

  saveExpenceClick() {
    this.clickFirst = false;
    this.modalWindowShowHide.showWaitScreen = true;
    let hdcash = (12 * this.totalCost);
    let income = 12 * parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdIncome);
    let cash = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash) + income;
    if (hdcash > cash) {
      this.modalWindowShowHide.showWaitScreen = false;
      this.messageText = this.translation.instant('gameLabel.Please_check_your_available_cash_You_dont_have_sufficient_cash_to_pay_for_expenses_for_1_year_as_per_selected_plan.');
      this.successMsgFlag = true;
      this.messageType = "Warning: ";
      this.messageClass = "errorpopup";
      this.chagneExpenceWindowFlag = true;
      //    this.classActive='shw_div active';
      //  this.classInActive="hide_div";
    }
    else {
      this.checkCharityIndexValueChange();
      this.saveChangeExpenceToPerson();
      let updatedExpenceObject = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense;
      updatedExpenceObject.charityFlag = this.charityIndexChangeFlag;
      if (this.charityIndexChangeFlag) {
        this.financeService.updateExpence(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].expense).subscribe(
          res => {
            this.result = res;
            this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
            this.messageText = this.translation.instant('gameLabel.exepeses_Save');
            this.successMsgFlag = true;
            this.messageType = "Warning: ";
            this.messageClass = "errorpopup";
            this.modalWindowShowHide.showWaitScreen = false;
            this.chagneExpenceWindowFlag = false;
            this.classActive = 'shw_div active';
            this.classInActive = "hide_div";
          })
      }
      else {
        this.financeService.updateExpence(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].expense).subscribe(
          res => {
            this.result = res;
            this.commonActionService.getActionIdentityDiffrence(this.result, this.homeService.allPerson);
            this.messageText = this.translation.instant('gameLabel.exepeses_Save');
            this.successMsgFlag = true;
            this.messageType = "Success: ";
            this.messageClass = "Successpopup";
            this.modalWindowShowHide.showWaitScreen = false;
            this.chagneExpenceWindowFlag = false;
            this.classActive = 'shw_div active';
            this.classInActive = "hide_div";
          })
      }

    }
  }

  successMsgWindowClose() {
    this.MessageShow = false;
    this.successMsgFlag = false;
    // this.modalWindowShowHide.showActionFinanace=false;
  }

  cancelExpenceChangeWindow() {
    this.clickFirst = false;
    this.chagneExpenceWindowFlag = false;
    this.classActive = 'shw_div active';
    this.classInActive = "hide_div";

  }
  close() {
    this.modalWindowShowHide.showActionFinanace = false;
  }
  displayJobListForAll() {
    this.allCarrerCards = Object.assign([], this.homeService.allPerson[this.constantService.FAMILY_SELF].my_career);
    this.cardLength = this.allCarrerCards.length;
    if (this.cardLength == 1) {
      this.firstCards.push(this.allCarrerCards[this.default]);
      this.allCarrerCards = [];
    }
    else if (this.allCarrerCards.length > 1) {
      this.firstCards.push(this.allCarrerCards[this.cardLength - 1]);
      this.allCarrerCards.reverse();
    }
  }
  disableLoanAndInvestment() {
    let takenSharkAmt = 0, takenFriendLoan = 0, takenFamilyLoan = 0, maxAllowedLoan = 0, takenBankLoan = 0;
    let householdAssetsPer = this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6;
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities <= 0) {
      maxAllowedLoan = householdAssetsPer
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities > 0) {
      for (let i = 0; i < this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.length; i++) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 7) {
          takenSharkAmt = Math.round(takenSharkAmt + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance);
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 6) {
          takenBankLoan = Math.round(takenBankLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance)
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 8) {
          takenFriendLoan = Math.round(takenFriendLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance)
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 9) {
          takenFamilyLoan = Math.round(takenFamilyLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance)
        }
      }
      let total = takenSharkAmt + takenFriendLoan + takenFamilyLoan;
      maxAllowedLoan = takenBankLoan;
      if (maxAllowedLoan >= householdAssetsPer) {
        maxAllowedLoan = 0
      }
      if (maxAllowedLoan <= 0 && !this.financeService.selectionFlag) {
        this.loanDisable = true;
        this.translation.get('gameLabel.loanWarrning', { householdLiabilities: Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities) }).subscribe(
          (s: String) => {
            this.messageType = "Warning: ";
            this.successMsgFlag = true;
            this.messageText = s;
            this.messageClass = "successpopup";
          })
      }
      else {
        this.loanDisable = false;
      }
    }
    if (maxAllowedLoan <= 0 && !this.financeService.selectionFlag) {
      this.messageText = this.translation.instant('gameLabel.lonWarr');
      this.messageType = "Warning: ";
      this.successMsgFlag = true;
      this.messageClass = "successpopup";
    }
    else {
      this.loanDisable = false;
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash <= 0 && !this.financeService.selectionFlag) {
      this.messageText = this.messageText + this.translation.instant('gameLabel.investmentWarr')
      this.investmentDisable = true;
    }
    else {
      this.investmentDisable = false;
    }
  }

  getALoanClick() {
    let takenSharkAmt = 0, takenFriendLoan = 0, familyLoan = 0, maxAllowedLoan = 0, takenBankLoan = 0
    let householdAssetsPer = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6);
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities <= 0) {
      maxAllowedLoan = Math.round(householdAssetsPer)
    }
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities > 0) {
      for (let i = 0; i < this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.length; i++) {
        if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 7) {
          takenSharkAmt = takenSharkAmt + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance;
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 6) {
          takenBankLoan = Math.round(takenBankLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance)
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 8) {
          takenFriendLoan = takenFriendLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance
        }
        else if (this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].potentialLoanId === 9) {
          familyLoan = familyLoan + this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans[i].balance
        }
      }

      maxAllowedLoan = takenBankLoan;
      if (maxAllowedLoan < householdAssetsPer) {
        maxAllowedLoan = Math.round(householdAssetsPer - maxAllowedLoan)
      }
      else {
        maxAllowedLoan = 0
      }
      if (maxAllowedLoan <= 0) {
        this.loanDisable = true;
        this.translation.get('gameLabel.loanWarrning', { householdLiabilities: this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdLiabilities }).subscribe(
          (s: String) => {
            this.messageText = s;
            this.successMsgFlag = true;
            this.messageType = "Warning: ";
            this.messageClass = "errorpopup";
          }
        )
      }
      else {
        this.showLoanMetaData = true;
        this.showInvestmentMetaData = false;
        this.financeService.getAllLoanMetaData(this.translationService.selectedLang.code).subscribe(
          data => {
            this.loan = data;
            this.allLoanMetaDataList = [];
            let pppDivisor = (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ProdperCapita / 41950) / (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ppp / 54800)
            this.finalMaxLoanForIndex0 = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6);
            for (let i = 0; i < this.loan.length; i++) {
              this.finalmax = 0;
              let min = this.loan[i].minimun * pppDivisor;
              this.loanMin = min * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
              let m = this.loan[i].maximum * pppDivisor
              this.max = maxAllowedLoan;
              let temMax = m * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate;
              if (this.max < temMax) {
                this.finalmax = this.max;
              }
              else {
                this.finalmax = temMax;
              }

              if (this.finalmax >= 100000000) {
                this.finalmax = 100000000;
              }
              this.finalmax = parseInt(this.finalmax);
              if (!(this.loan[i].potentialLoanId === 6 || this.loan[i].potentialLoanId === 7 || this.loan[i].potentialLoanId === 8 || this.loan[i].potentialLoanId === 9)) {
                this.allLoanMetaDataList.push({
                  "potentialLoanId": this.loan[i].potentialLoanId,
                  "loanType": this.loan[i].type,
                  "minimum": 0,
                  "loanValue": 0,
                  "maximum": this.finalmax,
                  "interestRate": this.loan[i].interestRate,
                  "term": this.loan[i].trem
                });
              }
            }
            this.allLoanMetaDataList[0].maximum = this.finalMaxLoanForIndex0;
          })
      }
    }
    else {
      this.showLoanMetaData = true;
      this.showInvestmentMetaData = false;
      this.financeService.getAllLoanMetaData(this.translationService.selectedLang.code).subscribe(
        data => {
          this.loan = data;
          this.allLoanMetaDataList = [];
          let pppDivisor = (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ProdperCapita / 41950) / (this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ppp / 54800)
          this.finalMaxLoanForIndex0 = Math.round(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth * 0.6);
          for (let i = 0; i < this.loan.length; i++) {
            this.finalmax = 0;
            let min = this.loan[i].minimun * pppDivisor;
            this.loanMin = min * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate
            let m = this.loan[i].maximum * pppDivisor
            this.max = maxAllowedLoan;
            let temMax = m * this.homeService.allPerson[this.constantService.FAMILY_SELF].country.ExchangeRate;
            if (this.max < temMax) {
              this.finalmax = this.max;
            }
            else {
              this.finalmax = temMax;
            }

            if (this.finalmax >= 100000000) {
              this.finalmax = 100000000;
            }
            this.finalmax = parseInt(this.finalmax);
            if (!(this.loan[i].potentialLoanId === 6 || this.loan[i].potentialLoanId === 7 || this.loan[i].potentialLoanId === 8 || this.loan[i].potentialLoanId === 9)) {
              this.allLoanMetaDataList.push({
                "potentialLoanId": this.loan[i].potentialLoanId,
                "loanType": this.loan[i].type,
                "minimum": 0,
                "loanValue": 0,
                "maximum": this.finalmax,
                "interestRate": this.loan[i].interestRate,
                "term": this.loan[i].trem
              });
            }
          }
          this.allLoanMetaDataList[0].maximum = this.finalMaxLoanForIndex0;
        })
    }
  }

  getAInvestMentClick() {
    this.showLoanMetaData = false;
    this.showInvestmentMetaData = true;
    this.financeService.getAllInvestMentMetaData(this.translationService.selectedLang.code).subscribe(
      data => {
        this.investment = data;
        this.investmentMetadata = [];
        var i = 0;

        for (i = 0; i < data.length; i++) {
          let cash = parseInt(this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash);
          // if (cash >= 100000000) {
          //   cash = 100000000;
          // }
          this.investmentMetadata.push({
            "potentialInvestmentId": this.investment[i].potentialInvestmentId,
            "investmentType": this.investment[i].investmentType,
            "minimum": 0,
            "investmentValue": 0,
            "householdCash": cash
          })
        }
        this.value = 10;
      });
  }

  saveInvestment(investAmount, potentialInvestmentId, investmentType) {
    this.showInvestmentMetaData = false;
    this.showLoanMetaData = false;
    if (investAmount > this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash) {
      this.messageText = this.translation.instant('gameLabel.You_dont_have_sufficient_cash_to_invest');
      this.successMsgFlag = true;
      this.messageType = "Success:";
      this.messageClass = "successpopup";
    }
    else {
      this.investMentId = potentialInvestmentId;
      this.investMentType = investmentType;
      this.investMentAmount = investAmount;
      this.modalWindowShowHide.showWaitScreen = true;

      let investment = {
        "investmentAmount": this.investMentAmount,
        "potentialInvestmentId": this.investMentId,
        "investmentType": this.investMentType
      }
      this.financeService.saveInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id, investment).subscribe(
        data => {
          this.modalWindowShowHide.showWaitScreen = true;
          this.result = data;
          this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
          this.allInvesmentCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_investment_trans.reverse();
          this.financeService.getAllLoanInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
            res => {
              this.allCards = res;
              this.allLoanInvestMentCards = this.allCards
              this.allCards = this.allCards.reverse();
              this.displayOnlyInvestmentCard = true;
              this.translation.get('gameLabel.investment_success', { investAmount: this.investMentAmount, CurrencyPlural: this.homeService.allPerson[this.constantService.FAMILY_SELF].country.CurrencyPlural, investmentType: this.investMentType }).subscribe(
                (s: String) => {
                  this.messageText = s;
                  this.successMsgFlag = true;
                  this.messageType = "Warrning: ";
                  this.messageClass = "successpopup";
                  this.displayAllCards = false;
                  this.displayOnlyLoansCard = false;
                  this.displayBusinessLoansCards = false;
                  this.dispalyFamilyLoansCards = false;
                  this.showLoanClass = "shw_div ";
                  this.showAllClass = "shw_div active";
                  this.modalWindowShowHide.showWaitScreen = false;
                })
            })
        })
    }
  }

  onLoanSave(loanAmount, potentialLoanId, loanType, term, interestRate,) {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdNetWorth > 0) {
      this.showInvestmentMetaData = false;
      this.showLoanMetaData = false;
      this.modalWindowShowHide.showWaitScreen = true;
      let loan = { "loanAmount": loanAmount, "potentialLoanId": potentialLoanId, "loanType": loanType, "term": term, "interestRate": interestRate }
      this.financeService.saveLoan(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id, loan).subscribe(data => {
        this.result = data;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allLoansCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.reverse();
        this.financeService.getAllLoanInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
          res => {
            this.allLoanInvestMentCards = res;
            this.familyloans = [];
            this.businessLoans = [];
            for (let i = 0; i < this.allLoanInvestMentCards.length; i++) {
              if (this.allLoanInvestMentCards[i].type === 'Loan')
                if (this.allLoanInvestMentCards[i].potentialId === 6 || this.allLoanInvestMentCards[i].potentialId === 7 || this.allLoanInvestMentCards[i].potentialId === 8 || this.allLoanInvestMentCards[i].potentialId === 9) {
                  if (this.allLoanInvestMentCards[i].loanCalc) {
                    this.allCards.push(this.allLoanInvestMentCards[i])
                    this.businessLoans.push(this.allLoanInvestMentCards[i])
                  }
                }
                else {
                  this.allCards.push(this.allLoanInvestMentCards[i])
                  this.familyloans.push(this.allLoanInvestMentCards[i])
                }
            }
            this.businessLoans = this.businessLoans.reverse();
            this.familyloans = this.familyloans.reverse();
            this.allCards = this.allCards.reverse();
            this.modalWindowShowHide.showWaitScreen = false;
            this.messageText = loanAmount + " " + this.homeService.allPerson[this.constantService.FAMILY_SELF].country.CurrencyPlural + " loan taken from " + loanType;
            this.successMsgFlag = true;
            this.messageType = "success:";
            this.messageClass = "successpopup";
            this.showLoanMetaData = false;
            this.showInvestmentMetaData = false;
            this.showLoanClass = "shw_div ";
            this.showAllClass = "shw_div active";
            this.displayAllCards = false;
            this.displayOnlyLoansCard = false;
            this.displayOnlyInvestmentCard = false;
            this.displayBusinessLoansCards = false;
            this.dispalyFamilyLoansCards = true;
            this.disableLoanAndInvestment();
          });
      })
    }
    else {
      this.messageText = this.translation.instant('gameLabel.Your_net_worth_is_negative_so_you_can_not_get_a_loan');
      this.successMsgFlag = true;
      this.messageType = "Warrning: ";
      this.messageClass = "errorpopup";
    }

  }

  payOffLoan(loanId, amount, type) {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].expense.householdCash < amount) {
      this.messageText = this.translation.instant('gameLabel.You_dont_have_sufficient_cash_to_pay_off');
      this.successMsgFlag = true;
      this.messageType = "Warrning: ";
      this.messageClass = "errorpopup";
    }
    else if (amount <= 0) {
      amount = Number.parseInt(amount);
      this.messageText = this.translation.instant('gameLabel.warrning')
      this.successMsgFlag = true;
      this.messageType = "Warrning: ";
      this.messageClass = "errorpopup";
    }
    else {
      let loanTransaction = { "loanId": loanId, "amount": amount, "loanType": type }
      this.modalWindowShowHide.showWaitScreen = true;
      this.financeService.updateLoan(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, loanTransaction).subscribe(data => {
        this.result = data;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allLoansCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.reverse();
        let allCards = [];
        this.allCards = [];
        this.allLoanInvestMentCards = [];
        this.financeService.getAllLoanInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
          res => {
            allCards = res;
            this.allLoanInvestMentCards = allCards
            this.familyloans = [];
            this.businessLoans = [];
            for (let i = 0; i < allCards.length; i++) {
              if (allCards[i].type === 'Loan')
                if (allCards[i].potentialId === 6 || allCards[i].potentialId === 7 || allCards[i].potentialId === 8 || allCards[i].potentialId === 9) {
                  if (allCards[i].loanCalc) {
                    this.allCards.push(allCards[i])
                    this.businessLoans.push(allCards[i])
                  }
                }
                else {
                  this.allCards.push(allCards[i])
                  this.familyloans.push(allCards[i])
                }
            }
            this.businessLoans = this.businessLoans.reverse();
            this.familyloans = this.familyloans.reverse();
            this.allCards = this.allCards.reverse();
            this.modalWindowShowHide.showWaitScreen = false;
            this.messageText = amount + ' ' + this.homeService.allPerson[this.constantService.FAMILY_SELF].country.CurrencyPlural + "  paid off for " + type;
            this.successMsgFlag = true;
            this.messageType = "Success: ";
            this.messageClass = "successPopUp";
            this.disableLoanAndInvestment();
          })
      });
    }

  }


  payOffBusiennssLoan(loanId, amount, type) {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].business.businessValue < amount) {
      this.messageText = "you do not hae sufficient business cash to pay loan"
      this.successMsgFlag = true;
      this.messageType = "Warrning: ";
      this.messageClass = "errorpopup";
    }
    else if (amount <= 0) {
      amount = Number.parseInt(amount);
      this.messageText = this.translation.instant('gameLabel.warrning')
      this.successMsgFlag = true;
      this.messageType = "Warrning: ";
      this.messageClass = "errorpopup";
    }
    else {
      let loanTransaction = { "loanId": loanId, "amount": amount, "loanType": type }
      this.modalWindowShowHide.showWaitScreen = true;
      this.financeService.updateLoan(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, loanTransaction).subscribe(data => {
        this.result = data;
        this.allCards = [];
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allLoansCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.reverse();
        this.financeService.getAllLoanInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
          res => {
            let allCards = [];
            allCards = res;
            this.businessallCards = [];
            this.allBusinessBankCards = [];
            this.businessSharkCards = [];
            this.businessFriendLoans = [];
            for (let i = 0; i < allCards.length; i++) {
              if (allCards[i].type === "Loan") {
                if ((allCards[i].potentialId === 6 || allCards[i].potentialId === 7 || allCards[i].potentialId === 8 || allCards[i].potentialId === 9) && !allCards[i].loanCalc) {
                  if (allCards[i].balance > 0) {
                    allCards[i].lineClass = "pinkborbottom";
                    this.businessallCards.push(allCards[i]);
                    if (allCards[i].potentialId === 6 && !allCards[i].loanCalc) {
                      this.allBusinessBankCards.push(allCards[i])
                    }
                    else if (allCards[i].potentialId === 7 && !allCards[i].loanCalc) {
                      this.businessSharkCards.push(allCards[i])

                    }
                    else if ((allCards[i].potentialId === 8 || allCards[i].potentialId === 9) && !allCards[i].loanCalc) {
                      this.businessFriendLoans.push(allCards[i])
                    }
                  }
                }

              }
            }
            this.businessallCards = this.businessallCards.reverse();
            this.businessSharkCards = this.businessSharkCards.reverse();
            this.businessFriendLoans = this.businessFriendLoans.reverse();
            this.allBusinessBankCards = this.allBusinessBankCards.reverse();
            this.modalWindowShowHide.showWaitScreen = false;
            this.messageText = amount + ' ' + this.homeService.allPerson[this.constantService.FAMILY_SELF].country.CurrencyPlural + "  paid off for loan " + type;
            this.successMsgFlag = true;
            this.messageType = "Success: ";
            this.messageClass = "successPopUp";
          })
      });
    }

  }


  withdrowInvestment(my_investment_id, amount, type) {
    if (amount <= 0) {
      amount = Number.parseInt(amount);
      this.messageText = this.translation.instant('gameLabel.warrning');
      this.successMsgFlag = true;
      this.messageType = "Warrning: ";
      this.messageClass = "errorpopup";
    }
    else {
      this.modalWindowShowHide.showWaitScreen = true;
      let investmentTransaction = { "investmentId": my_investment_id, "amount": amount, investmentType: type }
      this.financeService.updateInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id, investmentTransaction).subscribe(data => {
        this.modalWindowShowHide.showWaitScreen = false;
        this.result = data;
        this.commonActionService.getActionEventAndIdentityDifference(this.result, this.homeService.allPerson);
        this.allInvesmentCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_investment_trans.reverse();
        this.financeService.getAllLoanInvestMent(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(
          res => {
            this.allCards = res;
            this.allLoanInvestMentCards = this.allCards
            this.allCards = this.allCards.reverse();
            this.messageText = amount + ' ' + this.homeService.allPerson[this.constantService.FAMILY_SELF].country.CurrencyPlural + " converted to cash from " + type;
            this.successMsgFlag = true;
            this.messageType = "Success: ";
            this.messageClass = "successPopUp";
            // this.amount=0;
          });
      })
    }
  }


  showOnlyLoan() {
    this.displayOnlyLoansCard = true;
    this.displayOnlyInvestmentCard = false;
    this.displayAllCards = false
    this.allLoansCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.reverse();

  }

  showFamilyLoans() {
    this.dispalyFamilyLoansCards = true;
    this.displayBusinessLoansCards = false;
    this.displayOnlyInvestmentCard = false;
    this.displayOnlyLoansCard = false;
    this.displayAllCards = false
  }


  showBusinessLoans() {
    this.dispalyFamilyLoansCards = false;
    this.displayBusinessLoansCards = true;
    this.displayOnlyInvestmentCard = false;
    this.displayOnlyLoansCard = false;
    this.displayAllCards = false
  }

  showOnlyInvestMent() {
    this.allInvesmentCards = [];
    this.dispalyFamilyLoansCards = false;
    this.displayBusinessLoansCards = false;
    this.displayOnlyInvestmentCard = true;
    this.displayOnlyLoansCard = false;
    this.displayAllCards = false
    this.allInvesmentCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_investment_trans.reverse();
  }

  showAll() {
    this.familyloans = [];
    this.businessLoans = [];
    this.dispalyFamilyLoansCards = false;
    this.displayBusinessLoansCards = false;
    this.displayAllCards = true;
    this.displayOnlyLoansCard = false;
    this.displayOnlyInvestmentCard = false;
    let allCards = [];
    allCards = this.allLoanInvestMentCards
    for (let i = 0; i < allCards.length; i++) {
      if (allCards[i].type === "Investment") {
        this.allCards.push(allCards[i])
        allCards[i].lineClass = "blueborbottom";
      }
      else if (allCards[i].type === "Loan") {
        if (allCards[i].potentialId === 6 || allCards[i].potentialId === 7 || allCards[i].potentialId === 8 || allCards[i].potentialId === 9) {
          if (allCards[i].loanCalc) {
            allCards[i].lineClass = "pinkborbottom";
            this.allCards.push(allCards[i])
            this.businessLoans.push(allCards[i])
          }
        }
        else {
          this.allCards.push(allCards[i])
        }
      }
    }
    this.allCards = this.allCards.reverse();
    this.businessLoans = this.businessLoans.reverse();
    this.familyloans = this.familyloans.reverse();

    this.allInvesmentCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_investment_trans.reverse();
    this.allLoansCards = this.homeService.allPerson[this.constantService.FAMILY_SELF].my_loan_trans.reverse();

  }


  showAllBusinessLoans() {
    this.businessallCards = [];
    this.allBusinessBankCards = [];
    this.businessSharkCards = [];
    this.businessFriendLoans = [];
    this.displayBusinessBankLoan = false;
    this.dispalySharkLoan = false;
    this.disaplayAllbusinessCards = true;
    this.dispalyFamilyAndFriendLoan = false
    let allCards = [];
    allCards = this.allLoanInvestMentCards;
    for (let i = 0; i < allCards.length; i++) {
      if (allCards[i].type === "Loan") {
        if ((allCards[i].potentialId === 6 || allCards[i].potentialId === 7 || allCards[i].potentialId === 8 || allCards[i].potentialId === 9) && !allCards[i].loanCalc) {
          if (allCards[i].balance > 0) {
            allCards[i].lineClass = "pinkborbottom";
            this.businessallCards.push(allCards[i]);
            if (allCards[i].potentialId === 6 && !allCards[i].loanCalc) {
              this.allBusinessBankCards.push(allCards[i])
            }
            else if (allCards[i].potentialId === 7 && !allCards[i].loanCalc) {
              this.businessSharkCards.push(allCards[i])

            }
            else if ((allCards[i].potentialId === 8 || allCards[i].potentialId === 9) && !allCards[i].loanCalc) {
              this.businessFriendLoans.push(allCards[i])
            }
          }
        }
      }
    }
    this.businessallCards = this.businessallCards.reverse();
    this.businessSharkCards = this.businessSharkCards.reverse();
    this.businessFriendLoans = this.businessFriendLoans.reverse();
    this.allBusinessBankCards = this.allBusinessBankCards.reverse();
  }

  showFriendAndFamilyLoans() {
    this.displayBusinessBankLoan = false;
    this.dispalySharkLoan = false;
    this.disaplayAllbusinessCards = false;
    this.dispalyFamilyAndFriendLoan = true
  }
  showOnlyBusinessbankLoan() {
    this.displayBusinessBankLoan = true;
    this.dispalySharkLoan = false;
    this.disaplayAllbusinessCards = false;
    this.dispalyFamilyAndFriendLoan = false
  }

  showSharkLoans() {
    this.displayBusinessBankLoan = false;
    this.dispalySharkLoan = true;
    this.disaplayAllbusinessCards = false;
    this.dispalyFamilyAndFriendLoan = false
  }
  cancelMetadataListClick() {
    this.showInvestmentMetaData = false;
    this.showLoanMetaData = false;
  }







  closeAlert() {
    this.showAlertFlag = false;
  }

  changeExpenditure() {
    if (this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_friend_flag || this.homeService.allPerson[this.constantService.FAMILY_SELF].help_from_government_flag) {
      this.messageText = this.translation.instant('gameLabel.assistance_warr');
      this.alertType = "Warning: ";
      this.alertClass = "alert-warning";
      this.showAlertFlag = true;
    } else if (!this.homeService.allPerson[this.constantService.FAMILY_SELF].finance_handle_self) {
      this.showChangeExpenditureDecisionFlag = true;
      this.messageText = this.translation.instant('gameLabel.Confirm_changes_in_expenditure?');
    }
    else {
      this.homeService.allPerson[this.constantService.FAMILY_SELF].finance_handle_self = false;
      this.showChangeExpenditureDecisionFlag = false;
      this.financeService.updateExpendetureFlagForSelf(this.homeService.allPerson[this.constantService.FAMILY_SELF].finance_handle_self, this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id).subscribe(
        data => {
        })
    }
  }

  changeExpendituareYes() {
    this.showChangeExpenditureDecisionFlag = false;
    this.homeService.allPerson[this.constantService.FAMILY_SELF].finance_handle_self = true;
    this.financeService.updateExpendetureFlagForSelf(this.homeService.allPerson[this.constantService.FAMILY_SELF].finance_handle_self, this.homeService.allPerson[this.constantService.FAMILY_SELF].person_id).subscribe(
      data => {
      })
  }

  changeExpendituareNo() {
    this.homeService.allPerson[this.constantService.FAMILY_SELF].finance_handle_self = false;
    this.showChangeExpenditureDecisionFlag = false;
  }

  closeExpenditure() {
    this.chagneExpenceWindowFlag = false;
  }


  cancleChangeExpenditure() {
    this.classActive = "shw_div active";
    this.chagneExpenceWindowFlag = false;

  }

  fianaceButton() {
    this.financeService.selectionFlag = false
  }

  businessButton() {
    this.financeService.selectionFlag = true

  }


  clickHeadButton() {
    this.alertClass = "alert-success"
    this.headButtonWarrFlag = true;
    this.showChangeExpenditureDecisionFlag = true;
    this.messageText = this.translation.instant('gameLabel.Being the head of the household will let you control all the finances of your family. Do you want to become the head of the household?')
  }
  clickHeadButtonYes() {
    this.modalWindowShowHide.showWaitScreen = true
    this.headButtonWarrFlag = false;
    this.showChangeExpenditureDecisionFlag = false;
    this.financeService.updateHeadOfHouseholdFlag(this.homeService.allPerson[this.constantService.FAMILY_SELF].game_id).subscribe(data => {
      this.commonActionService.getActionEventAndIdentityDifference(data, this.homeService.allPerson);
      if(this.homeService.allPerson[this.constantService.FAMILY_SELF].head_of_household){
        this.changeExpenceButtonClass="for-bx-shadow btn btn_blue1-single font_s4 active font_s4-ipad"
      }
      this.modalWindowShowHide.showWaitScreen = false
      this.modalWindowShowHide.showActionFinanace = false;
    })
  }
  clickHeadButtonNo() {
    this.showChangeExpenditureDecisionFlag = false;
    this.headButtonWarrFlag = false;
  }
}
