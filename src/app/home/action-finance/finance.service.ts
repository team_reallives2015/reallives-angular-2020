import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/common.service';
import { ConstantService } from 'src/app/shared/constant.service';
import { TranslationService } from 'src/app/translation/translation.service';

@Injectable({
  providedIn: 'root'
})
export class FinanceService {
  public diet = [];
  public shelter = [];
  public charity = [];
  public consumer = [];
  selectionFlag: boolean = false;
  subscriber;
  constructor(private http: HttpClient,
    private commonService: CommonService,
    public translate: TranslateService,
    public translation: TranslationService) {
    this.subscriber = this.translation.changeLang.subscribe(
      res => {
        res = this.translation.selectedLang.code;
        this.setArray();
      }
    )
    this.setArray();
  }

  updateExpence(gameId, expense) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/finance/changeExpenditure/${gameId}`, { expense });

  }

  getAllLoanMetaData(lang) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/finance/getAllLoanMetadata/${lang}`);

  }

  getAllInvestMentMetaData(lang) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/finance/getAllInvestmentMetadata/${lang}`);

  }

  saveInvestMent(gameId, personId, investment) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/finance/saveInvestment/${gameId}/${personId}`, { investment });

  }

  updateInvestMent(gameId, investmentTransaction) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/finance/updateInvestment/${gameId}`, { investmentTransaction });

  }

  saveLoan(gameId, personId, loan) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/finance/saveLoan/${gameId}/${personId}`, { loan });

  }
  updateLoan(gameId, loanTransaction) {
    return this.http.post<Array<Object>>(`${this.commonService.url}game/finance/updateLoan/${gameId}`, { loanTransaction });

  }

  getAllLoanInvestMent(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/finance/getAllInvestmentLoan/${gameId}`);
  }

  updateExpendetureFlagForSelf(flag, personId) {
    return this.http.get<boolean>(`${this.commonService.url}game/finance/setSelfFinanceHandler/${flag}/${personId}`);
  }

  updateHeadOfHouseholdFlag(gameId) {
    return this.http.get<Array<Object>>(`${this.commonService.url}game/headOfHousehold/${gameId}`);
  }

  setArray() {
    this.diet = [
      {
        "id": 0,
        "name": this.translate.instant('gameLabel.Starvation'),
        "cost": 0
      },
      {
        "id": 1,
        "name": this.translate.instant('gameLabel.Survival'),
        "cost": 0
      },
      {
        "id": 2,
        "name": this.translate.instant('gameLabel.Meager'),
        "cost": 0
      },
      {
        "id": 3,
        "name": this.translate.instant('gameLabel.Minimal'),
        "cost": 0
      },
      {
        "id": 4,
        "name": this.translate.instant('gameLabel.Adequate'),
        "cost": 0
      },
      {
        "id": 5,
        "name": this.translate.instant('gameLabel.Optimal'),
        "cost": 0
      },
      {
        "id": 6,
        "name": this.translate.instant('gameLabel.Lavish'),
        "cost": 0
      },
      {
        "id": 7,
        "name": this.translate.instant('gameLabel.Gourmand'),
        "cost": 0
      }
    ];
    this.shelter = [
      {
        "id": 0,
        "name": this.translate.instant('gameLabel.Homeless'),
        "cost": 0
      },
      {
        "id": 1,
        "name": this.translate.instant('gameLabel.Temporary'),
        "cost": 0
      },
      {
        "id": 2,
        "name": this.translate.instant('gameLabel.Simplest_Shared'),
        "cost": 0
      },
      {
        "id": 3,
        "name": this.translate.instant('gameLabel.Simplest'),
        "cost": 0
      },
      {
        "id": 4,
        "name": this.translate.instant('gameLabel.Modest'),
        "cost": 0
      },
      {
        "id": 5,
        "name": this.translate.instant('gameLabel.Moderate'),
        "cost": 0
      },
      {
        "id": 6,
        "name": this.translate.instant('gameLabel.Ample'),
        "cost": 0
      },
      {
        "id": 7,
        "name": this.translate.instant('gameLabel.Luxurious'),
        "cost": 0
      }
    ];
    this.charity = [
      {
        "id": 0,
        "name": this.translate.instant('gameLabel.Nothing'),
        "cost": 0
      },
      {
        "id": 1,
        "name": this.translate.instant('gameLabel.Token'),
        "cost": 0
      },
      {
        "id": 2,
        "name": this.translate.instant('gameLabel.Respectable'),
        "cost": 0
      },
      {
        "id": 3,
        "name": this.translate.instant('gameLabel.Substantial'),
        "cost": 0
      },
      {
        "id": 4,
        "name": this.translate.instant('gameLabel.Saintly'),
        "cost": 0
      }
    ];
    this.consumer = [
      {
        "id": 0,
        "name": this.translate.instant('gameLabel.Miserly'),
        "cost": 0
      },
      {
        "id": 1,
        "name": this.translate.instant('gameLabel.Careful_Shopper'),
        "cost": 0
      },
      {
        "id": 2,
        "name": this.translate.instant('gameLabel.Like_Others_in_the_Community'),
        "cost": 0
      },
      {
        "id": 3,
        "name": this.translate.instant('gameLabel.Enthusiastic_Shopper'),
        "cost": 0
      },
      {
        "id": 4,
        "name": this.translate.instant('gameLabel.Spend_Thrift'),
        "cost": 0
      }
    ];
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }

}
