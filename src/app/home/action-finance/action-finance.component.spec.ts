import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionFinanceComponent } from './action-finance.component';

describe('ActionFinanceComponent', () => {
  let component: ActionFinanceComponent;
  let fixture: ComponentFixture<ActionFinanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionFinanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
