import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { TranslationService } from 'src/app/translation/translation.service';
import { HomeService } from '../home.service';
import { SdgIndicatorsService } from './sdg-indicators.service';


@Component({
  selector: 'app-sdg-indicators',
  templateUrl: './sdg-indicators.component.html',
  styleUrls: ['./sdg-indicators.component.css']
})
export class SdgIndicatorsComponent implements OnInit {

  allPerson;
  mainPerson;
  selfSdg;
  registerCountrySdg;
  selfSgeInfo;
  registerSdgInfo;
  selfSdgDeatails;
  registeredSdgDetails;
  sdgImage;
  sdgDescription;
  sdgName;
  sdgOrigninalName;
  registerCountrySdgName;
  hideShowFlag:boolean=false;
  sdgComment:string="";
  saveComment:boolean=false;
  allSdgComment;
  showSdgModelFlag:boolean=false;
  successMsgFlag:boolean=false;
  messageText;
  bornSdgSubGoalDeatails;
  regSdgSubGoalDeatails;
  sdgDetails;
    bornSdgInfo:any
    regSdgInfo:any
    sdgInfo:any;
    values;
    valuesR
  constructor(public modalWindowShowHide: modalWindowShowHide,
              private homeService: HomeService,
              private gameSummeryService : GameSummeryService,
              private sdgIndicatorsService :SdgIndicatorsService,
              public tralslate :TranslateService,
              public translationService :TranslationService
              ) { }

  ngOnInit(): void {
   this.allPerson=this.homeService.allPerson;
   this.mainPerson=this.allPerson['SELF'];
   this.gameSummeryService.getSDGInfo(this.mainPerson.country.countryid,true,this.translationService.selectedLang.code).subscribe( sdgInfo=>{
    this.sdgDetails=sdgInfo['goals'];
    this.bornSdgSubGoalDeatails=sdgInfo['targets'];
    this.gameSummeryService.getSDGInfo(this.mainPerson.register_country.countryid,false,this.translationService.selectedLang.code).subscribe( regSdgInfo=>{
      this.regSdgSubGoalDeatails=regSdgInfo['targets'];
      this.registerCountrySdg=regSdgInfo;
      // this.getAllSdgComment();
    })
  })
  }

  getAllSdgComment(){
    this.showSdgModelFlag=true;
    this.sdgIndicatorsService.getAllSdgComment(this.mainPerson.game_id).subscribe(
      res=>{
       this.allSdgComment=res;
      }
    )
  }

  modelClose(){
    this.showSdgModelFlag=false;

  }

  checkValues(sdgComment){
    if(sdgComment!==""){
      this.saveComment=true;
    }
    else{
      this.saveComment=false;
    }
  }

  close(){
    this.hideShowFlag=false;
  }


  saveSdgComment(){
    if(this.saveComment){
    this.sdgIndicatorsService.saveSdgComment(this.sdgComment,this.mainPerson.game_id).subscribe(
      res=>{
        if(res==true){
          this.successMsgFlag=true;
          this.messageText= this.tralslate.instant('gameLabel.Save_Comment_Succesfully');
          this.getAllSdgComment();
          this.sdgComment="";
        }
        else{
          alert("something went wrong");
        }
      }
    )
    }
    else{
      alert(this.tralslate.instant('gameLabel.plz_enter_somthings'));
    }
  }


  successMsgWindowClose(){
    this.successMsgFlag=false;
  }

  sdgSelect(sdgId:number){
    this.bornSdgInfo=[];
    this.regSdgInfo=[];
      this.sdgInfo = this.bornSdgSubGoalDeatails;
   this.values= Object.values(this.sdgInfo)
   for(let  i=0;i<this.values.length;i++){
    if(this.values[i].SDG_Id===sdgId){
      if( this.values[i].score!=='NA'){
      this.values[i].score=parseFloat(this.values[i].score);
      this.values[i].score=this.values[i].score.toFixed(2);
      }
      else{
        this.values[i].score=this.values[i].score;
      }
     this.bornSdgInfo.push(this.values[i]);
   }
  }
  this.valuesR= Object.values(this.regSdgSubGoalDeatails)
  for(let  i=0;i<this.valuesR.length;i++){
   if(this.valuesR[i].SDG_Id===sdgId){
    if( this.values[i].score!=='NA'){
    this.valuesR[i].score=parseFloat(this.valuesR[i].score);
    this.valuesR[i].score=this.valuesR[i].score.toFixed(2);
    }
    else{
      this.valuesR[i].score=this.valuesR[i].score;
    }
    this.regSdgInfo.push(this.valuesR[i]);
  }
 }
      this.sdgImage="assets/images/sdgicon/sdg"+sdgId+"_"+this.translationService.selectedLang.code+".png";
      this.sdgDescription = this.sdgDetails[sdgId-1].SDG_discription;
      this.sdgName = this.sdgDetails[sdgId-1].SDG_title;
      this.sdgOrigninalName=this.sdgDetails[sdgId].SDG_name;
  this.hideShowFlag=true;
  
}


getRegisterCountrySdgScore(i)
{
  let v
v= this.regSdgInfo[i].score;
   return v;
 }
 getRegisterCountrySdgColur(i)
 {
   let v
 v= this.regSdgInfo[i].color;
    return v;
  }
getNewSdgColor(colour){
 let classvalue="";
  if(colour==="green"){
    classvalue = "clr_code bg-success d-inline-block"
  }
  else if(colour==="red"){
   classvalue = "clr_code bg-red d-inline-block"
 }
 else if(colour==="orange"){
   classvalue = "clr_code bg-oragne d-inline-block"
 }
 else if(colour==="yellow"){
   classvalue = "clr_code bg-yellow d-inline-block"
 }
 else if(colour==="grey"){
   classvalue = "clr_code bg-secondary d-inline-block"
 }
  return classvalue;
}


}
