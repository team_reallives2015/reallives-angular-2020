import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Injectable({
  providedIn: 'root'
})
export class SdgIndicatorsService {

  constructor(private http :HttpClient,
              private commonService : CommonService) { }


saveSdgComment(comment,gameId){
    return this.http.post<boolean>(`${this.commonService.url}game/saveSdgComment/${gameId}`,{comment});
}

getAllSdgComment(gameId){
  return this.http.get<Object>(`${this.commonService.url}game/getAllSdgComments/${gameId}`);
}
}