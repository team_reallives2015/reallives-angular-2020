import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SdgIndicatorsComponent } from './sdg-indicators.component';

describe('SdgIndicatorsComponent', () => {
  let component: SdgIndicatorsComponent;
  let fixture: ComponentFixture<SdgIndicatorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SdgIndicatorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SdgIndicatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
