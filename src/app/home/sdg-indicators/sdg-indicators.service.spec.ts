import { TestBed } from '@angular/core/testing';

import { SdgIndicatorsService } from './sdg-indicators.service';

describe('SdgIndicatorsService', () => {
  let service: SdgIndicatorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SdgIndicatorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
