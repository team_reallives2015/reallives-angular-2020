import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  amenities;;
  dispalyUtilityArr=[];
  UtilityArr=[];
  constructor( public translate :TranslateService) { }


  createUtility(utility){
    this.amenities=utility;
     this.UtilityArr=[
       {
         DisplayName:this.translate.instant('gameLabel.COMPUTERS'),
         utilityValue:this.amenities.computers,
         image :"assets/images/computer_utility.png",
         familyModalImage :"assets/images/computer_utility_b.png",
         hoverValue:"",
         redGreeClassValue :"",
         value: "",
         boolean:false,
         checkUncheckClass:""
       },
       {
        DisplayName:this.translate.instant('gameLabel.REFRIGERATORS'),
        utilityValue:this.amenities.refrigerators,
        image :"assets/images/refrigerator_utility.png",
        familyModalImage :"assets/images/refrigerator_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:false,
        checkUncheckClass:""

      },
      {
        DisplayName:this.translate.instant('gameLabel.SAFE_WATER'),
        utilityValue:this.amenities.safeWater,
        image :"assets/images/surro_icon1.png",
        familyModalImage :"assets/images/safewater_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:true,
        checkUncheckClass:""

      },
      {
        DisplayName:this.translate.instant('gameLabel.PUBLIC_SANITATION'),
        utilityValue:this.amenities.basicSanitation,
        image :"assets/images/surro_icon2.png",
        familyModalImage :"assets/images/sanitation_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:true,
        checkUncheckClass:"",
        
      },
      {
        DisplayName:this.translate.instant('gameLabel.MEDICAL_CARE'),
        utilityValue:this.amenities.healthService,
        image :"assets/images/surro_icon3.png",
        familyModalImage :"assets/images/medical_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:true,
        checkUncheckClass:""

      },
      {
        DisplayName:this.translate.instant('gameLabel.TELEVISIONS'),
        utilityValue:this.amenities.televisions,
        image :"assets/images/surro_icon7.png",
        familyModalImage :"assets/images/television_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:false,
        checkUncheckClass:""

      },
      {
        DisplayName:this.translate.instant('gameLabel.RADIOS'),
        utilityValue:this.amenities.radios,
        image :"assets/images/surro_icon4.png",
        familyModalImage :"assets/images/radio_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:false,
        checkUncheckClass:""

      },       

      {
        DisplayName:this.translate.instant('gameLabel.TELEPHONES'),
        utilityValue:this.amenities.telephones,
        image :"assets/images/surro_icon5.png",
        familyModalImage :"assets/images/telephone_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:false,
        checkUncheckClass:""

      },
      {
        DisplayName:this.translate.instant('gameLabel.CELLPHONES'),
        utilityValue:this.amenities.mobiles,
        image :"assets/images/surro_icon6.png",
        familyModalImage :"assets/images/mobile_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:false,
        checkUncheckClass:""

      },
      {
        DisplayName:this.translate.instant('gameLabel.CARS'),
        utilityValue:this.amenities.vehicles,
        image :"assets/images/surro_icon9.png",
        familyModalImage :"assets/images/car_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:false,
        checkUncheckClass:""

      },
      {
        DisplayName:this.translate.instant('gameLabel.INTERNET'),
        utilityValue:this.amenities.internet,
        image :"assets/images/surro_icon8.png",
        familyModalImage :"assets/images/internet_utility_b.png",
        hoverValue:"",
        redGreeClassValue :"",
        value: "",
        boolean:true,
        checkUncheckClass:""

      }
      
     ]
     this.dispalyUtilityArr=this.UtilityArr;
     let size=this.dispalyUtilityArr.length;
     for(let i=0;i<size;i++)
     {
      if(this.dispalyUtilityArr[i].utilityValue ==="true" || this.dispalyUtilityArr[i].utilityValue >0){
        this.dispalyUtilityArr[i].hoverValue=" Available";
        this.dispalyUtilityArr[i].redGreeClassValue="indication avalable";
      }
      else {
        this.dispalyUtilityArr[i]. hoverValue=" Not Available";
        this.dispalyUtilityArr[i].redGreeClassValue=" indication not_avalable";

      }
        if(this.dispalyUtilityArr[i].boolean==true){
             if(this.dispalyUtilityArr[i].utilityValue){
              this.dispalyUtilityArr[i].checkUncheckClass="material-icons";
              this.dispalyUtilityArr[i].value="check";
             }
             else if(!this.dispalyUtilityArr[i].utilityValue){
              this.dispalyUtilityArr[i].checkUncheckClass="material-icons";
              this.dispalyUtilityArr[i].value="close";
             }
        }
        else if(this.dispalyUtilityArr[i].boolean==false){
          this.dispalyUtilityArr[i].checkUncheckClass="";
          this.dispalyUtilityArr[i].value=this.dispalyUtilityArr[i].utilityValue
        }

        
     }
    //  indication not_avalable  indication avalable
  }

//   "amenities" : {
//     "computers" : 3,
//     "refrigerators" : 2,
//     "safeWater" : false,
//     "basicSanitation" : false,
//     "healthService" : true,
//     "televisions" : 2,
//     "radios" : 2,
//     "telephones" : 1,
//     "mobiles" : 2,
//     "vehicles" : 2,
//     "internet" : false
// },
}
