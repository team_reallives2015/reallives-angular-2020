import { Component, OnInit, Input } from '@angular/core';
import { ConstantService } from 'src/app/shared/constant.service';
import { HomeService } from '../home.service';
import { UtilityService } from './utility.service';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-utility-bar',
  templateUrl: './utility-bar.component.html',
  styleUrls: ['./utility-bar.component.css']
})
export class UtilityBarComponent implements OnInit {

  gameId;
  allP;
  mainPerson;
  subscription;
  @Input() data: boolean = false;
  constructor(public utilityService: UtilityService,
    public homeService: HomeService,
    public modalWindowShowHide: modalWindowShowHide,
    public constantService: ConstantService,
    public translate:TranslateService) {
    this.subscription = this.homeService.changeAllPerson.subscribe(persons => {
      this.homeService.allPerson = persons;
      this.utilityService.createUtility(this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities);
    })
  }
  ngOnInit(): void {
    this.utilityService.createUtility(this.homeService.allPerson[this.constantService.FAMILY_SELF].amenities);
  }


  OrganAndBloodDonationPopUp() {
    this.modalWindowShowHide.showOrganBloodDonationFlag = true;
  }

  petPopUp() {
    this.modalWindowShowHide.showPetFlag = true;
  }

  charityClick() {
    this.modalWindowShowHide.showCharity = true;
  }





  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
