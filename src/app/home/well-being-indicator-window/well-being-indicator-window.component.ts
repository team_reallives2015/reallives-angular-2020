import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Data } from '@angular/router';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import { modalWindowShowHide } from 'src/app/modal-window-show-hide.service';
import { EventService } from '../event/event.service';
import { WellBeingIndicatorService } from '../well-being-indicator/well-being-indicator.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-well-being-indicator-window',
  templateUrl: './well-being-indicator-window.component.html',
  styleUrls: ['./well-being-indicator-window.component.css']
})
export class WellBeingIndicatorWindowComponent implements OnInit {
  lineChartData: Data[] = [];
  lineChartLabels: Label[] = [];
  event;
  selectedIndicator;
  wellBeingIndicators = [
    {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.HEALTH'),
      "key": this.translate.instant('gameLabel.HEALTH'),
      "class": "text-primary2",
      "tooltipplcament": "right",
      "discription": this.translate.instant('gameLabel.Health_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""
    },

    {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.RESISTANCE'),
      "key": this.translate.instant('gameLabel.RESISTANCE'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Resistance_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""
    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.HAPPINESS'),
      "key": this.translate.instant('gameLabel.HAPPINESS'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Happiness_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.INTELLIGENCE'),
      "key": this.translate.instant('gameLabel.INTELLIGENCE'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Intelligence_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.ARTISTIC'),
      "key": this.translate.instant('gameLabel.ARTISTIC'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Artistic_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.MUSICAL'),
      "key": this.translate.instant('gameLabel.MUSICAL'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Musical_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.ATHLETIC'),
      "key": this.translate.instant('gameLabel.ATHLETIC'),
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Athletic_info'),
      "class": "",
      "classForBorder": "progress_wraper",
      "classForNumber": ""
    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.STRENGTH'),
      "key": this.translate.instant('gameLabel.STRENGTH'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Strength_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.ENDURANCE'),
      "key": this.translate.instant('gameLabel.key'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Endurance_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

      //  "physicalEndurance"
    }, {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.SPIRITUAL'),
      "key": this.translate.instant('gameLabel.SPIRITUAL'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Spiritual_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

    },
    {
      "old": 0,
      "new1": 0,
      "title": this.translate.instant('gameLabel.WISDOM'),
      "key": this.translate.instant('gameLabel.WISDOM'),
      "class": "",
      "tooltipplcament": "top",
      "discription": this.translate.instant('gameLabel.Wisdom_info'),
      "classForBorder": "progress_wraper",
      "classForNumber": ""

    }
  ];
  // Define colors of chart segments
  lineChartColors: Color[] = [
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: '#567caa',
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
    }
  ];
  // Set true to show legends
  lineChartLegend = false;
  // Define type of chart
  lineChartType = 'line';
  lineChartPlugins = [];
  age
  constructor(public modalWindowShowHide: modalWindowShowHide,
    public wellBeingIndicatorService: WellBeingIndicatorService,
    public eventService: EventService,
    public translate: TranslateService) { }
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  ;

  ngOnInit(): void {
    this.generateGraph();
    this.changeIndicators(this.eventService.currentEvent);
    this.event = this.eventService.currentEvent;

  }

  generateGraph() {
    this.lineChartData = [];
    this.lineChartLabels = [];
    let dummyData = [];
    if (this.wellBeingIndicatorService.selectedIndicator === "physicalEndurance") {
      this.wellBeingIndicatorService.selectedIndicator = "physicalEndurance";
      this.wellBeingIndicatorService.dispalyName = this.translate.instant('gameLabel.ENDURANCE')
      this.selectedIndicator = "physicalEndurance";
    }
    else {
      this.wellBeingIndicatorService.dispalyName = this.wellBeingIndicatorService.selectedIndicator
      this.selectedIndicator = this.wellBeingIndicatorService.selectedIndicator;
      this.selectedIndicator = (this.selectedIndicator).toLowerCase()
    }


    for (let j = 0; j < this.wellBeingIndicators.length; j++) {
      if (this.wellBeingIndicators[j].key === this.wellBeingIndicatorService.selectedIndicator) {
        this.wellBeingIndicators[j].class = "text-primary2"
      }
      else {
        this.wellBeingIndicators[j].class = ""
      }
    }

    for (let i = 0; i < this.eventService.allEvents.length; i++) {
      dummyData.push(this.eventService.allEvents[i].newTraits[this.selectedIndicator]);
      this.lineChartLabels.push(this.eventService.allEvents[i].age)
    }
    this.lineChartData = [{ data: dummyData }];

  }

  // Define chart options
  public lineChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        display: true,
        ticks: {
          max: 100
        }
      }]
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          return "At Age " + tooltipItem.xLabel + ":" + tooltipItem.yLabel

        }
      }
    },
  };
  public chartClicked(e: any): void {
    let selectedId = e.active[0]._index;
    for (let i = 0; i < this.eventService.allEvents.length; i++) {
      if (selectedId === i) {
        this.event = this.eventService.allEvents[i];
        this.changeIndicators(this.event);
        break;
      }
    }
  }
  changeIndicators(event) {
    this.wellBeingIndicators[0].old = event.oldTraits.health;
    this.wellBeingIndicators[0].new1 = event.newTraits.health;
    if (event.newTraits.health < 35) {
      this.wellBeingIndicators[0].classForBorder = "progress_wraper red_brder"
    }
    else {
      this.wellBeingIndicators[0].classForBorder = "progress_wraper green_brder"
    }


    if (this.wellBeingIndicators[0].new1 < this.wellBeingIndicators[0].old) {
      this.wellBeingIndicators[0].classForNumber = "text-red"
    }
    else if (this.wellBeingIndicators[0].new1 > this.wellBeingIndicators[0].old) {
      this.wellBeingIndicators[0].classForNumber = "text-green"
    }

    this.wellBeingIndicators[1].old = event.oldTraits.resistance;
    this.wellBeingIndicators[1].new1 = event.newTraits.resistance;

    if (this.wellBeingIndicators[1].new1 < this.wellBeingIndicators[1].old) {
      this.wellBeingIndicators[1].classForNumber = "text-red"
    }
    else if (this.wellBeingIndicators[1].new1 > this.wellBeingIndicators[1].old) {
      this.wellBeingIndicators[1].classForNumber = "text-green"
    }

    if (event.newTraits.resistance < 35) {
      this.wellBeingIndicators[1].classForBorder = "progress_wraper red_brder"
    }
    else {
      this.wellBeingIndicators[1].classForBorder = "progress_wraper green_brder"
    }


    if (this.wellBeingIndicators[1].new1 < this.wellBeingIndicators[1].old) {
      this.wellBeingIndicators[1].classForNumber = "text-red"
    }
    else if (this.wellBeingIndicators[1].new1 > this.wellBeingIndicators[1].old) {
      this.wellBeingIndicators[1].classForNumber = "text-green"
    }


    this.wellBeingIndicators[2].old = event.oldTraits.happiness;
    this.wellBeingIndicators[2].new1 = event.newTraits.happiness;

    if (event.newTraits.happiness < 35) {
      this.wellBeingIndicators[2].classForBorder = "progress_wraper red_brder"
    }
    else {
      this.wellBeingIndicators[2].classForBorder = "progress_wraper green_brder"
    }

    if (this.wellBeingIndicators[2].new1 < this.wellBeingIndicators[2].old) {
      this.wellBeingIndicators[2].classForNumber = "text-red"
    }
    else if (this.wellBeingIndicators[2].new1 > this.wellBeingIndicators[2].old) {
      this.wellBeingIndicators[2].classForNumber = "text-green"
    }


    this.wellBeingIndicators[3].old = event.oldTraits.intelligence;
    this.wellBeingIndicators[3].new1 = event.newTraits.intelligence;

    if (this.wellBeingIndicators[3].new1 < this.wellBeingIndicators[3].old) {
      this.wellBeingIndicators[3].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[3].new1 > this.wellBeingIndicators[3].old) {
      this.wellBeingIndicators[3].classForBorder = "progress_wraper green_brder"
    }




    this.wellBeingIndicators[4].old = event.oldTraits.artistic;
    this.wellBeingIndicators[4].new1 = event.newTraits.artistic;

    if (this.wellBeingIndicators[4].new1 < this.wellBeingIndicators[4].old) {
      this.wellBeingIndicators[4].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[4].new1 > this.wellBeingIndicators[4].old) {
      this.wellBeingIndicators[4].classForBorder = "progress_wraper green_brder"
    }


    this.wellBeingIndicators[5].old = event.oldTraits.musical;
    this.wellBeingIndicators[5].new1 = event.newTraits.musical;

    if (this.wellBeingIndicators[5].new1 < this.wellBeingIndicators[5].old) {
      this.wellBeingIndicators[5].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[5].new1 > this.wellBeingIndicators[5].old) {
      this.wellBeingIndicators[5].classForBorder = "progress_wraper green_brder"
    }

    this.wellBeingIndicators[6].old = event.oldTraits.athletic;
    this.wellBeingIndicators[6].new1 = event.newTraits.athletic;
    if (this.wellBeingIndicators[6].new1 < this.wellBeingIndicators[6].old) {
      this.wellBeingIndicators[6].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[6].new1 > this.wellBeingIndicators[6].old) {
      this.wellBeingIndicators[6].classForBorder = "progress_wraper green_brder"
    }

    this.wellBeingIndicators[7].old = event.oldTraits.strength;
    this.wellBeingIndicators[7].new1 = event.newTraits.strength;

    if (this.wellBeingIndicators[7].new1 < this.wellBeingIndicators[7].old) {
      this.wellBeingIndicators[7].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[7].new1 > this.wellBeingIndicators[7].old) {
      this.wellBeingIndicators[7].classForBorder = "progress_wraper green_brder"
    }



    this.wellBeingIndicators[8].old = event.oldTraits.physicalEndurance;
    this.wellBeingIndicators[8].new1 = event.newTraits.physicalEndurance;
    if (this.wellBeingIndicators[8].new1 < this.wellBeingIndicators[8].old) {
      this.wellBeingIndicators[8].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[8].new1 > this.wellBeingIndicators[8].old) {
      this.wellBeingIndicators[8].classForBorder = "progress_wraper green_brder"
    }

    this.wellBeingIndicators[9].old = event.oldTraits.spiritual;
    this.wellBeingIndicators[9].new1 = event.newTraits.spiritual;
    if (this.wellBeingIndicators[9].new1 < this.wellBeingIndicators[9].old) {
      this.wellBeingIndicators[9].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[9].new1 > this.wellBeingIndicators[9].old) {
      this.wellBeingIndicators[9].classForBorder = "progress_wraper green_brder"
    }

    this.wellBeingIndicators[10].old = event.oldTraits.wisdom;
    this.wellBeingIndicators[10].new1 = event.newTraits.wisdom;

    if (this.wellBeingIndicators[10].new1 < this.wellBeingIndicators[10].old) {
      this.wellBeingIndicators[10].classForBorder = "progress_wraper red_brder"
    }
    else if (this.wellBeingIndicators[10].new1 > this.wellBeingIndicators[10].old) {
      this.wellBeingIndicators[10].classForBorder = "progress_wraper green_brder"
    }
  }

  changeGraph(key) {
    this.wellBeingIndicatorService.selectedIndicator = key;
    this.generateGraph();
  }
  close() {
    this.modalWindowShowHide.showWellBeingIndicatorWindow = false;
  }

}
