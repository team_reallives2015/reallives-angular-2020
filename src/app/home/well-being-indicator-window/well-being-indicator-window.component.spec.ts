import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WellBeingIndicatorWindowComponent } from './well-being-indicator-window.component';

describe('WellBeingIndicatorWindowComponent', () => {
  let component: WellBeingIndicatorWindowComponent;
  let fixture: ComponentFixture<WellBeingIndicatorWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WellBeingIndicatorWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WellBeingIndicatorWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
