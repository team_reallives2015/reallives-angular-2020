import { Component, HostListener } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
export let browserRefresh = false;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[]
  
})
 
export class AppComponent {
  title = 'angular-project2020';
  state = "language"
  subscription: Subscription;


  constructor(private router: Router) {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}