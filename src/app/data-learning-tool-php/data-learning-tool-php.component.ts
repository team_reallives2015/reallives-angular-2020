import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../shared/common.service';
import { PhpToolService } from './php-tool.service';

@Component({
  selector: 'app-data-learning-tool-php',
  templateUrl: './data-learning-tool-php.component.html',
  styleUrls: ['./data-learning-tool-php.component.css']
})
export class DataLearningToolPhpComponent implements OnInit {
  token;
  constructor(public translate: TranslateService,
    public router: Router,
    public commonService: CommonService,
    public phpToolService: PhpToolService,
    public activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.phpToolService.flagForPhpToolClick = true;
    this.phpToolService.flowFromCountry = false;
    this.token = (this.activatedRouter.snapshot.queryParamMap.get('token') || 0);
    // this.token = "63ad1fb893312510221887f1"
    localStorage.setItem('token', this.token);
    this.router.navigate(['/language']);
  }

}
