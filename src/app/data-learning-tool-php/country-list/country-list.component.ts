import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { SdgToolServiceService } from 'src/app/sdg-data-learning-tool/sdg-tool-service.service';
import { CommonService } from 'src/app/shared/common.service';
import { PhpToolService } from '../php-tool.service';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})

export class CountryListComponent implements OnInit {
  countryList: any;
  sameCountrySelectionFlag: boolean = false;
  msgText;
  bornCountryRadioButtonValue: any = null
  RegisterCountryRadioButtonValue: any = null
  disableFlag: boolean = false;
  registerSelctedCountryObject = [];
  bornSelectedCountryObject = [];
  bornCountry: any;
  registerCountry: any;
  country: any;
  constructor(public phpToolService: PhpToolService,
    public gameSummary: GameSummeryService,
    public commonService: CommonService,
    public router: Router,
    public translate: TranslateService,
    public SdgToolService: SdgToolServiceService) { }

  ngOnInit(): void {
    this.phpToolService.selectedBornCountryObject = [];
    this.phpToolService.selectedRegisterCountryObject = [];
    this.phpToolService.selectedBornCountryObject = null;
    this.phpToolService.selectedRegisterCountryObject = null;
    this.phpToolService.getCountryList().subscribe(
      res => {
        this.phpToolService.countryList = res;
        this.country = res;
        this.countryList = JSON.parse(JSON.stringify(this.phpToolService.countryList));
        this.setCountryName();
      }
    )

  }

  allRadioButtonValue(event: any) {
    if (event.target.name == "country_born") {
      this.bornCountryRadioButtonValue = event.target.value;
      this.bornCountryRadioButtonValue = parseInt(this.bornCountryRadioButtonValue)
      if (this.phpToolService.selectedRegisterCountryObject !== null) {
        if (this.phpToolService.selectedRegisterCountryObject.countryid === this.bornCountryRadioButtonValue) {
          for (let i = 0; i < this.countryList.length; i++) {
            if (this.bornCountryRadioButtonValue === this.countryList[i].countryid) {
              this.phpToolService.selectedBornCountryObject = this.countryList[i];
              this.checkDisableFlag();
              this.setCountryName();
              break;
            }
          }
          this.sameCountrySelectionFlag = true
          this.msgText = this.translate.instant('gameLabel.Can_not_select_same_country')
        }
        else {
          for (let i = 0; i < this.countryList.length; i++) {
            if (this.bornCountryRadioButtonValue === this.countryList[i].countryid) {
              this.phpToolService.selectedBornCountryObject = this.countryList[i];
              this.checkDisableFlag();
              this.setCountryName();
              break;
            }
          }
        }
      }
      else {
        for (let i = 0; i < this.countryList.length; i++) {
          if (this.bornCountryRadioButtonValue === this.countryList[i].countryid) {
            this.phpToolService.selectedBornCountryObject = this.countryList[i];
            this.checkDisableFlag();
            this.setCountryName();
            break;
          }
        }
      }
    }
    if (event.target.name == "country_register") {
      this.RegisterCountryRadioButtonValue = event.target.value;
      this.RegisterCountryRadioButtonValue = parseInt(this.RegisterCountryRadioButtonValue)
      if (this.phpToolService.selectedBornCountryObject !== null) {
        if (this.phpToolService.selectedBornCountryObject.countryid === this.RegisterCountryRadioButtonValue) {
          this.sameCountrySelectionFlag = true
          this.msgText = this.translate.instant('gameLabel.Can_not_select_same_country')
          for (let i = 0; i < this.countryList.length; i++) {
            if (this.RegisterCountryRadioButtonValue === this.countryList[i].countryid) {
              this.phpToolService.selectedRegisterCountryObject = this.countryList[i];
              this.checkDisableFlag();
              this.setCountryName();
              break;
            }
          }
        }
        else {
          for (let i = 0; i < this.countryList.length; i++) {
            if (this.RegisterCountryRadioButtonValue === this.countryList[i].countryid) {
              this.phpToolService.selectedRegisterCountryObject = this.countryList[i];
              this.checkDisableFlag();
              this.setCountryName();
              break;
            }
          }
        }
      }
      else {
        for (let i = 0; i < this.countryList.length; i++) {
          if (this.RegisterCountryRadioButtonValue === this.countryList[i].countryid) {
            this.phpToolService.selectedRegisterCountryObject = this.countryList[i];
            this.checkDisableFlag();
            this.setCountryName();
            break;
          }
        }
      }
    }

  }


  generate() {
    if (this.phpToolService.flowFromCountry && !this.SdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/learningTool']);
    }
    else if (this.phpToolService.flagForPhpToolClick && !this.SdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/learningTool']);
    }
    else if (this.phpToolService.flowFromSdg && !this.SdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/sdgCountryPages']);
    }
    else if (this.phpToolService.flowFromSdg && this.SdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/sdgStmt']);
    }
    else if (this.phpToolService.flowFromCountryDisaparity) {
      this.router.navigate(['/countryDisparityPages']);
    }
    else if (this.phpToolService.folwFromCountryGroup) {
      this.router.navigate(['/countryGroupLearningToolPhp']);
    }
  }



  shuffleCountry() {
    this.commonService.randomArrayShuffle(this.countryList);
  }

  Atoz() {
    this.countryList = JSON.parse(JSON.stringify(this.country));
  }

  checkDisableFlag() {
    if (this.phpToolService.selectedBornCountryObject === null || this.phpToolService.selectedRegisterCountryObject === null) {
      this.disableFlag = false;
    }

    else if (this.bornCountryRadioButtonValue !== null || this.RegisterCountryRadioButtonValue !== null) {
      if (this.bornCountryRadioButtonValue === this.RegisterCountryRadioButtonValue) {
        this.disableFlag = false;
      }
      else {
        this.disableFlag = true;
      }
    }
    else {
      this.disableFlag = true;
    }

    // if (this.phpToolService.selectedBornCountryObject !== null && this.phpToolService.selectedRegisterCountryObject !== null)
    //   if (this.phpToolService.selectedBornCountryObject.countryid === this.phpToolService.selectedRegisterCountryObject.countryid) {
    //     this.disableFlag = false;
    //   }
    //   else {
    //     this.disableFlag = true;
    //   }
  }


  setCountryName() {
    if (this.phpToolService.selectedBornCountryObject === null) {
      this.bornCountry = "None";
    }
    else {
      this.bornCountry = this.phpToolService.selectedBornCountryObject.country;
    }


    if (this.phpToolService.selectedRegisterCountryObject === null) {
      this.registerCountry = "None";
    }
    else {
      this.registerCountry = this.phpToolService.selectedRegisterCountryObject.country;
    }

  }


  closePage() {
    this.router.navigate(['/phpToolPages'])
  }

  backClick() {
    if (this.phpToolService.flowFromCountry && this.phpToolService.flagForPhpToolClick) {
      this.router.navigate(['/phpToolSummary'])
    }
    else if (this.phpToolService.flagForPhpToolClick && this.phpToolService.flagForPhpToolClick) {
      this.router.navigate(['/phpToolSummary'])
    }
    else if (this.phpToolService.flowFromCountryDisaparity) {
      this.router.navigate(['/countryDisparityLearningToolSummary'])
    }
    else if (this.SdgToolService.flowFromCountrySdg || this.SdgToolService.flowFromSdgStmt || this.SdgToolService.flowFromWorldSdg) {
      this.router.navigate(['/sdgCountrySelection'])

    }
  }
  clickToExitButton() {
    if (this.gameSummary.countryGroupTool || this.gameSummary.countryDisaparityTool || this.gameSummary.countryLearningTool) {
      this.router.navigate(['/action-page']);
      this.gameSummary.countryGroupTool = false;
      this.gameSummary.countryDisaparityTool = false;
      this.gameSummary.countryLearningTool = false;
      this.gameSummary.countrySdgTool = false;
    }
    else if (this.gameSummary.countrySdgTool) {
      this.router.navigate(['/sdgSummary']);
      this.gameSummary.countryGroupTool = false;
      this.gameSummary.countryDisaparityTool = false;
      this.gameSummary.countryLearningTool = false;
    }
    else {
      this.commonService.close();
    }
  }

  okClick() {
    this.sameCountrySelectionFlag = false;
  }


}
