import { Component, OnInit } from '@angular/core';
import { PhpToolService } from '../php-tool.service';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldIndiaLow";
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { SdgToolServiceService } from 'src/app/sdg-data-learning-tool/sdg-tool-service.service';
import { TranslateService } from '@ngx-translate/core';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
@Component({
  selector: 'app-country-list-map',
  templateUrl: './country-list-map.component.html',
  styleUrls: ['./country-list-map.component.css']
})
export class CountryListMapComponent implements OnInit {
  countryList: any;
  sameCountrySelectionFlag: boolean = false;
  disableFlag: boolean = false
  bornCountry: any;
  registerCountry: any;
  flagNoDb: boolean = false;
  flagNoDbR: boolean = false;
  msgText = this.translate.instant('gameLabel.Selected_country_not_available_in_realLives_database');
  constructor(public phpToolService: PhpToolService,
    public translate: TranslateService,
    public router: Router,
    public gameSummary: GameSummeryService,
    public commonService: CommonService,
    public sdgToolService: SdgToolServiceService) { }

  ngOnInit(): void {
    this.phpToolService.selectedBornCountryObject = null;
    this.phpToolService.selectedRegisterCountryObject = null;
    this.phpToolService.getCountryList().subscribe(
      res => {
        this.phpToolService.countryList = res;
        this.countryList = this.phpToolService.countryList;
        this.setBornCountryMap();
        this.setRegisterCountryMap();
        this.setCountryName();
      })
  }


  setBornCountryMap() {
    let colourCode
    let selectedObj: any;
    let cnt = 0;
    let map = am4core.create("chartdiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.events.on("hit", function (ev) {
      // zoom to an object
      let country: any[] = [];
      // ev.target.series.chart.zoomToMapObject(ev.target);
      selectedObj = ev.target.dataItem.dataContext;
      if (this.phpToolService.selectedRegisterCountryObject !== null) {
        if (this.phpToolService.selectedRegisterCountryObject.Code === selectedObj["id"]) {
          this.sameCountrySelectionFlag = true;
          this.msgText = this.translate.instant('gameLabel.Can_not_select_same_country');
          for (let i = 0; i < this.countryList.length; i++) {
            if (this.countryList[i].Code === this.phpToolService.selectedRegisterCountryObject.Code) {
              cnt = -1;
              this.phpToolService.selectedBornCountryObject = this.countryList[i]
              this.checkDisableFlag();
              this.setCountryName();
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#00FF00")

              })
              this.checkDisableFlag();
              this.setCountryName()
            }
            else {
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#00FF00")
              })
            }
            {

            }
          }
        }
        else {
          for (let i = 0; i < this.countryList.length; i++) {
            if (this.countryList[i].Code === selectedObj["id"]) {
              cnt = -1;
              this.phpToolService.selectedBornCountryObject = this.countryList[i]
              this.checkDisableFlag();
              this.setCountryName();
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#0000FF")
              })
            }
            else {
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#00FF00")
              })
            }
            {

            }
          }
        }
      }
      else {
        for (let i = 0; i < this.countryList.length; i++) {
          if (this.countryList[i].Code === selectedObj["id"]) {
            cnt = -1;
            this.phpToolService.selectedBornCountryObject = this.countryList[i]
            this.checkDisableFlag();
            this.setCountryName();
            country.push({
              "id": this.countryList[i].Code,
              "name": this.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#0000FF")
            })
          }
          else {
            country.push({
              "id": this.countryList[i].Code,
              "name": this.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#00FF00")
            })
          }
          {

          }
        }
      }
      if (cnt === 0) {
        this.flagNoDb = true;
      }
      else {
        this.flagNoDb = false;
      }
      polygonSeries.data = country
      polygonTemplate.propertyFields.fill = "fill";

    }, this);


    let county1 = [];
    colourCode = am4core.color("#00FF00")
    for (let i = 0; i < this.countryList.length; i++) {
      county1.push({
        "id": this.countryList[i].Code,
        "name": this.countryList[i].country,
        "value": 100,
        "fill": colourCode
      })
    }
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }
  setRegisterCountryMap() {
    let cnt = 0;
    let colourCode
    let selectedObj: any;
    let map = am4core.create("regDiv", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    let polygonSeries = new am4maps.MapPolygonSeries();
    polygonSeries.useGeodata = true;
    map.series.push(polygonSeries);
    am4core.options.autoDispose = true;
    map.projection = new am4maps.projections.Mercator();

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    map.homeZoomLevel = 1;
    // Create hover state and set alternative fill color
    let hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color(polygonTemplate.propertyFields.fill);
    polygonSeries.exclude = ["AQ"];
    polygonTemplate.events.on("hit", function (ev) {
      // zoom to an object
      let country: any[] = [];
      // ev.target.series.chart.zoomToMapObject(ev.target);
      selectedObj = ev.target.dataItem.dataContext;
      if (this.phpToolService.selectedBornCountryObject !== null) {
        if (this.phpToolService.selectedBornCountryObject.Code === selectedObj["id"]) {
          this.sameCountrySelectionFlag = true;
          cnt = -1;
          this.msgText = this.translate.instant('gameLabel.Can_not_select_same_country');
          for (let i = 0; i < this.countryList.length; i++) {
            if (this.countryList[i].Code === this.phpToolService.selectedBornCountryObject.Code) {
              cnt = -1;
              this.phpToolService.selectedRegisterCountryObject = this.countryList[i]
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#FFA500")
              })
              this.checkDisableFlag();
              this.setCountryName();
            }
            else {
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#FFA500")
              })
            }
          }
        }
        else {
          for (let i = 0; i < this.countryList.length; i++) {

            if (this.countryList[i].Code === selectedObj["id"]) {
              cnt = -1;
              this.phpToolService.selectedRegisterCountryObject = this.countryList[i]
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#0000FF")
              })
              this.checkDisableFlag();
              this.setCountryName();
            }
            else {
              country.push({
                "id": this.countryList[i].Code,
                "name": this.countryList[i].country,
                "value": 100,
                "fill": am4core.color("#FFA500")
              })
            }
          }
        }
      }
      else {
        for (let i = 0; i < this.countryList.length; i++) {

          if (this.countryList[i].Code === selectedObj["id"]) {
            cnt = -1;
            this.phpToolService.selectedRegisterCountryObject = this.countryList[i]
            country.push({
              "id": this.countryList[i].Code,
              "name": this.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#0000FF")
            })
            this.checkDisableFlag();
            this.setCountryName();
          }
          else {
            country.push({
              "id": this.countryList[i].Code,
              "name": this.countryList[i].country,
              "value": 100,
              "fill": am4core.color("#FFA500")
            })
          }
        }
      }
      if (cnt === 0) {
        this.flagNoDbR = true;
      }
      else {
        this.flagNoDbR = false;
      }
      polygonSeries.data = country
      polygonTemplate.propertyFields.fill = "fill";
    }, this);


    let county1 = [];
    colourCode = am4core.color("#FFA500")
    for (let i = 0; i < this.countryList.length; i++) {
      county1.push({
        "id": this.countryList[i].Code,
        "name": this.countryList[i].country,
        "value": 100,
        "fill": colourCode
      })
    }
    polygonSeries.data = county1
    polygonTemplate.propertyFields.fill = "fill";
  }
  clickListButton() {
    this.router.navigate(['/countryList']);
  }
  clickNextButton() {
    if (this.phpToolService.flowFromCountry && !this.sdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/learningTool']);
    }
    if (this.phpToolService.flagForPhpToolClick && !this.sdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/learningTool']);
    }
    else if (this.phpToolService.flowFromSdg && !this.sdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/sdgCountryPages']);
    }
    else if (this.phpToolService.flowFromSdg && this.sdgToolService.flowFromSdgStmt) {
      this.router.navigate(['/sdgStmt']);
    }
    else if (this.phpToolService.flowFromCountryDisaparity) {
      this.router.navigate(['/countryDisparityPages']);
    }
    else if (this.phpToolService.folwFromCountryGroup) {
      this.router.navigate(['/countryGroupLearningToolPhp']);
    }
  }


  checkDisableFlag() {
    if ((this.phpToolService.selectedBornCountryObject === null || this.phpToolService.selectedRegisterCountryObject === null)) {
      this.disableFlag = false;
    }
    else {
      this.disableFlag = true;
    }

    if (this.phpToolService.selectedBornCountryObject !== null && this.phpToolService.selectedRegisterCountryObject !== null)
      if ((this.phpToolService.selectedBornCountryObject.Code === this.phpToolService.selectedRegisterCountryObject.Code)) {
        this.disableFlag = false;

      }
      else {
        this.disableFlag = true;
      }
  }

  setCountryName() {
    if (this.phpToolService.selectedBornCountryObject === null) {
      this.bornCountry = "None";
    }
    else {
      this.bornCountry = this.phpToolService.selectedBornCountryObject.country;
    }


    if (this.phpToolService.selectedBornCountryObject === null || this.phpToolService.selectedRegisterCountryObject === null) {
      this.registerCountry = "None";
    }
    else {
      this.registerCountry = this.phpToolService.selectedRegisterCountryObject.country;
    }

  }

  clickOk() {
    this.flagNoDb = false;
  }


  clickOkR() {
    this.flagNoDbR = false;
  }

  clickToExitButton() {
    if (this.gameSummary.countryGroupTool || this.gameSummary.countryDisaparityTool || this.gameSummary.countryLearningTool) {
      this.router.navigate(['/action-page']);
      this.gameSummary.countryGroupTool = false;
      this.gameSummary.countryDisaparityTool = false;
      this.gameSummary.countryLearningTool = false;
      // this.gameSummary.countrySdgTool = false;
    }
    else if (this.gameSummary.countrySdgTool) {
      this.router.navigate(['/sdgSummary']);
      this.gameSummary.countryGroupTool = false;
      this.gameSummary.countryDisaparityTool = false;
      this.gameSummary.countryLearningTool = false;
    }
    else {
      this.commonService.close();

    }

  }
  okClick() {
    this.sameCountrySelectionFlag = false
  }

  backClick() {
    if (this.phpToolService.flowFromCountry && this.phpToolService.flagForPhpToolClick) {
      this.router.navigate(['/phpToolSummary'])
    }
    else if (this.phpToolService.flagForPhpToolClick && this.phpToolService.flagForPhpToolClick) {
      this.router.navigate(['/phpToolSummary'])
    }
    else if (this.phpToolService.flowFromCountryDisaparity) {
      this.router.navigate(['/countryDisparityLearningToolSummary'])
    }
    else if (this.sdgToolService.flowFromCountrySdg || this.sdgToolService.flowFromSdgStmt || this.sdgToolService.flowFromWorldSdg) {
      this.router.navigate(['/sdgCountrySelection'])

    }
  }

}
