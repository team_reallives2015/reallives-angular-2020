import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryListMapComponent } from './country-list-map.component';

describe('CountryListMapComponent', () => {
  let component: CountryListMapComponent;
  let fixture: ComponentFixture<CountryListMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryListMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryListMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
