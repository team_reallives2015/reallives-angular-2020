import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataLearningToolPhpComponent } from './data-learning-tool-php.component';

describe('DataLearningToolPhpComponent', () => {
  let component: DataLearningToolPhpComponent;
  let fixture: ComponentFixture<DataLearningToolPhpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataLearningToolPhpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataLearningToolPhpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
