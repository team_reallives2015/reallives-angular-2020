import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolSummaryComponent } from './tool-summary.component';

describe('ToolSummaryComponent', () => {
  let component: ToolSummaryComponent;
  let fixture: ComponentFixture<ToolSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToolSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
