import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameSummeryService } from 'src/app/game-summery/game-summery.service';
import { CommonService } from 'src/app/shared/common.service';
import { PhpToolService } from '../php-tool.service';

@Component({
  selector: 'app-tool-summary',
  templateUrl: './tool-summary.component.html',
  styleUrls: ['./tool-summary.component.css']
})
export class ToolSummaryComponent implements OnInit {

  constructor(public phpToolService: PhpToolService,
    public router: Router,
    public gameSummary: GameSummeryService,
    public commonService: CommonService) { }

  ngOnInit(): void {
    // this.phpToolService.flowFromCountry = true
    this.phpToolService.flagForPhpToolClick = true;
    this.phpToolService.getCountryList().subscribe(
      res => {
        this.phpToolService.countryList = res;
      }
    )
  }

  clickdataLearningButtonMap() {
    this.router.navigate(['/countryMap'])
  }

  clickdataLearningButtonList() {
    this.router.navigate(['/countryList'])
  }

  clickToExitButton() {
    if (this.gameSummary.countryLearningTool) {
      this.gameSummary.countryLearningTool = false;
      this.phpToolService.flowFromCountry = false;
      this.router.navigate(['/action-page']);
    }
    else {
      this.phpToolService.flowFromCountry = false;
      this.commonService.close();
    }
  }

}
